/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bca.jni;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * 
 * @author LiemLT
 */
@SuppressLint("CommitPrefEdits")
public class MPreferences {

	SharedPreferences prefs;
	private SharedPreferences.Editor editor;

	public MPreferences(Activity activity) {
		prefs = activity.getSharedPreferences("gemchip", android.content.Context.MODE_PRIVATE);
		this.editor = prefs.edit();
	}

	public MPreferences(Context context) {
		prefs = context.getSharedPreferences("gemchip", android.content.Context.MODE_PRIVATE);
		this.editor = prefs.edit();
	}

	public boolean getBooleanValue(String strKey, boolean bDefault) {
		return prefs.getBoolean(strKey, bDefault);
	}

	public void setValue(String strKey, boolean bValue) {
		editor.putBoolean(strKey, bValue);
		editor.commit();
	}

	public String getStringValue(String strKey, String strDefault) {
		return prefs.getString(strKey, strDefault);
	}

	public void setValue(String strKey, String strValue) {
		editor.putString(strKey, strValue);
		editor.commit();
	}

	public int getIntValue(String strKey, int iDefault) {
		return prefs.getInt(strKey, iDefault);
	}

	public void setValue(String strKey, int iValue) {
		editor.putInt(strKey, iValue);
		editor.commit();
	}
}