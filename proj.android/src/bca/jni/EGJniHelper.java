package bca.jni;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Handler;
import android.os.Looper;
import android.content.pm.PackageManager.NameNotFoundException;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;


public class EGJniHelper {
	
	public static Context _context = null;
	
	public EGJniHelper(Context context) {
		_context = context;
	}

	
	public static void quitGame() {
		
	}
	
	public static String checkIsActive() {
		return "1";
	}
	
	public static void showTopOnline() {
		
	}
	
	public static void insertTopScore(int nScore) {

	}
	
	public static void showAdmob() {
		
	}
	
	public static void showTopUp(int pType, int pTypeCharging) {
		Log.e("tuanvu", "show topup");
	}
	
	public static void showToast(final String content) {
		//Toast.makeText(_context	, content, Toast.LENGTH_LONG).show();
		
		 Handler handler = new Handler(Looper.getMainLooper());
	        handler.post(new Runnable() {

	            @Override
	            public void run() {
	                Toast.makeText(_context, content, Toast.LENGTH_LONG).show();
	            }
	        });
	}
	
	public static void hideAdmob() {
		
	}
	
	public static void showPopup() {
		//AdBuddiz.showAd((Activity) _context);
	}
	
	public static void logOutFacebook() {
		//AdBuddiz.showAd((Activity) _context);
	}
	
	public static String checkShowTopUp1st() {
		return "1";
	}
	
	public static String checkShowTopUp() {
		return "1";
	}
	
	public static String getRechargeID() {
		return "1";
	}
	
	public static void setGravityAdBotCenter() {
		
	}
	
	public static void setGravityAdBotLeft() {
		
	}
	
	public static void moreGame() {
		
	}
	
	public static String gettp() {
		// TODO Auto-generated method stub
		return String.valueOf(System.currentTimeMillis());
	}

	public static String getDeviceID() {
		TelephonyManager telephonyManager = (TelephonyManager) _context.getSystemService(Context.TELEPHONY_SERVICE);
		String android_id = Secure.getString(_context.getContentResolver(), Secure.ANDROID_ID);
		String devicesID = telephonyManager.getDeviceId() != null ? telephonyManager.getDeviceId() : android_id;
		return devicesID;
	}

	public static String getCountry() {
		try {
			TelephonyManager tm = (TelephonyManager) _context.getSystemService(Context.TELEPHONY_SERVICE);

			if (tm.getSimState() == TelephonyManager.SIM_STATE_READY) {
				String strSimCountry = tm.getSimCountryIso();

				if (strSimCountry != null && strSimCountry.length() > 0) {
					String strNetworkCountry = tm.getNetworkCountryIso();
					tm.getSimState();

					if (strNetworkCountry != null && strNetworkCountry.length() > 0) {
						return strNetworkCountry;
					}
				}

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return "unknow";
	}

	public static String getPackage() {
		return _context.getPackageName();
	}

	public static String getTelco() {
		try {
			TelephonyManager tm = (TelephonyManager) _context.getSystemService(Context.TELEPHONY_SERVICE);

			if (tm.getSimState() == TelephonyManager.SIM_STATE_READY) {
				String strSimCountry = tm.getSimCountryIso();

				if (strSimCountry != null && strSimCountry.length() > 0) {
					String strNetworkCountry = tm.getNetworkCountryIso();
					tm.getSimState();

					if (strNetworkCountry != null && strNetworkCountry.length() > 0) {
						String strNetworkOperatorName = tm.getNetworkOperatorName();
						if (strNetworkOperatorName != null && strNetworkOperatorName.length() > 0) {
							return strNetworkOperatorName;
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return "other";
	}

	public static String getCPID() {
		MPreferences _cStore = new MPreferences(_context);
		String strCpId = _cStore.getStringValue("cpid", "1");

		if (strCpId == null || strCpId.length() <= 0) {
			strCpId = "1";
		}

		return strCpId;
	}

	public static String getChargeID() {
		return "1";
	}

	public static String getMCC() {
		try {
			TelephonyManager tm = (TelephonyManager) _context.getSystemService(Context.TELEPHONY_SERVICE);

			if (tm.getSimState() == TelephonyManager.SIM_STATE_READY) {
				String strSimCountry = tm.getSimCountryIso();

				if (strSimCountry != null && strSimCountry.length() > 0) {
					String strNetworkCountry = tm.getNetworkCountryIso();
					tm.getSimState();

					if (strNetworkCountry != null && strNetworkCountry.length() > 0) {
						String strNetworkOperator = tm.getNetworkOperator();
						if (strNetworkOperator != null && strNetworkOperator.length() > 0) {
							return strNetworkOperator;
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return "unknow";
	}

	// Dung de dat mac dinh so dien thoai sau khi login
	// cho nguoi dung cap nhat So dien thoai
	public static String getPhoneNumber() {
		TelephonyManager TelephonyMgr = (TelephonyManager) _context.getSystemService(Context.TELEPHONY_SERVICE);

		if (TelephonyMgr.getLine1Number() != null) {
			return TelephonyMgr.getLine1Number();
		}

		return "";
	}

	public static String gethk() {
		final PackageManager packageManager = _context.getPackageManager();

		try {
			PackageInfo pi = packageManager.getPackageInfo(_context.getPackageName(), PackageManager.GET_SIGNATURES);
			final Signature[] arrSignatures = pi.signatures;

			for (final Signature sig : arrSignatures) {
				MessageDigest md;
				try {
					md = MessageDigest.getInstance("SHA");
					md.update(sig.toByteArray());

					byte[] hash = md.digest();
					StringBuffer hexString = new StringBuffer();

					for (int i = 0; i < hash.length; i++) {
						String hex = Integer.toHexString(0xff & hash[i]);
						if (hex.length() == 1)
							hexString.append('0');
						hexString.append(hex);
					}

					return hexString.toString();

				} catch (NoSuchAlgorithmException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		} catch (NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return "uk";
	}

	@SuppressWarnings("unused")
	public static String gethm() {
		final PackageManager packageManager = _context.getPackageManager();

		try {
			PackageInfo pi = packageManager.getPackageInfo(_context.getPackageName(), PackageManager.GET_SIGNATURES);
			final Signature[] arrSignatures = pi.signatures;

			for (final Signature sig : arrSignatures) {
				try {
					StringBuffer strMd5 = new StringBuffer();
					MessageDigest md5 = MessageDigest.getInstance("MD5");
					byte[] hashMD5 = md5.digest();

					for (int i = 0; i < hashMD5.length; i++) {
						if ((0xff & hashMD5[i]) < 0x10) {
							strMd5.append("0" + Integer.toHexString((0xFF & hashMD5[i])));
						} else {
							strMd5.append(Integer.toHexString(0xFF & hashMD5[i]));
						}
					}

					return strMd5.toString();

				} catch (NoSuchAlgorithmException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		} catch (NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return "uk";
	}

	public static String gethc() {
		final PackageManager packageManager = _context.getPackageManager();

		try {
			PackageInfo pi = packageManager.getPackageInfo(_context.getPackageName(), PackageManager.GET_SIGNATURES);
			final Signature[] arrSignatures = pi.signatures;

			for (final Signature sig : arrSignatures) {
				return String.valueOf(sig.hashCode());
			}
		} catch (NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return "uk";
	}

	public static String getpk() {
		return _context.getPackageName();
	}
}