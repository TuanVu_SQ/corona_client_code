LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := mplib_static
LOCAL_MODULE_FILENAME := mplib
ifeq ($(TARGET_ARCH_ABI), x86)
    LOCAL_SRC_FILES := x86/libmplib.a
endif
ifeq ($(TARGET_ARCH_ABI), armeabi)
    LOCAL_SRC_FILES := armeabi/libmplib.a
endif
ifeq ($(TARGET_ARCH_ABI), armeabi-v7a)
    LOCAL_SRC_FILES := armeabi-v7a/libmplib.a
endif
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)
$(call import-add-path,$(LOCAL_PATH)/../../Classes/common)
$(call import-add-path,$(LOCAL_PATH)/../../Classes/libs)
$(call import-add-path,$(LOCAL_PATH)/../../Classes/object)
$(call import-add-path,$(LOCAL_PATH)/../../Classes/message)
$(call import-add-path,$(LOCAL_PATH)/../../Classes/scene)
$(call import-add-path,$(LOCAL_PATH)/../../Classes/mpclient)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp
MY_CPP_LIST := $(wildcard $(LOCAL_PATH)/hellocpp/*.cpp)
MY_CPP_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/*.cpp)
MY_CPP_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/common/*.cpp)
MY_CPP_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/libs/*.cpp)
MY_CPP_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/object/*.cpp)
MY_CPP_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/scene/*.cpp)
MY_CPP_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/message/*.cpp)
MY_CPP_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/mpclient/*.cpp)

LOCAL_SRC_FILES := $(MY_CPP_LIST:$(LOCAL_PATH)/%=%)

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/common
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/message
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/libs
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/object
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/scene
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/mpclient

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cocos2dx_static mplib_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
LOCAL_ALLOW_UNDEFINED_SYMBOLS := true
