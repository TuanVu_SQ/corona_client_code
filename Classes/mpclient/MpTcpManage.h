#ifndef __MPTCP_MANAGER_H_INCLUDED__
#define __MPTCP_MANAGER_H_INCLUDED__

#include "MpConfig.h"
#include "cocos2d.h"

class MpTcpManager : cocos2d::Ref {
public:
	static MpTcpManager* create();

public:
	void sendMessage(MpMessage* msg, bool isWatingResponse = false);

private:
	void _connect();

public:
	void update(float = 0);

private:
	std::vector<MpMessage*> _listMessengeSend;
	MpClient* _mpClient;
	bool _isWatingResponse;
	WClock* _timeDelay;
	WClock* _timeOut;

private:
	MpTcpManager();
	~MpTcpManager();
};

#endif


