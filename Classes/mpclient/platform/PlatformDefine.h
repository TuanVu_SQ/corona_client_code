#ifndef __PLATFORM_DEFINE_H__
#define __PLATFORM_DEFINE_H__

#define MP_PLATFORM_UNKNOWN            0
#define MP_PLATFORM_IOS                1
#define MP_PLATFORM_ANDROID            2
#define MP_PLATFORM_WIN32              3
#define MP_PLATFORM_MARMALADE          4
#define MP_PLATFORM_LINUX              5
#define MP_PLATFORM_BADA               6
#define MP_PLATFORM_BLACKBERRY         7
#define MP_PLATFORM_MAC                8
#define MP_PLATFORM_NACL               9
#define MP_PLATFORM_EMSCRIPTEN        10
#define MP_PLATFORM_TIZEN             11
#define MP_PLATFORM_QT5               12
#define MP_PLATFORM_WP8               13
#define MP_PLATFORM_WINRT             14


#if WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP
	#undef MP_TARGET_PLATFORM
	#define MP_TARGET_PLATFORM MP_PLATFORM_WINRT
#elif WINAPI_FAMILY == WINAPI_FAMILY_APP
	#undef MP_TARGET_PLATFORM
	#define MP_TARGET_PLATFORM MP_PLATFORM_WIN32
#endif


#if defined(MP_TARGET_OS_MAC) || defined(__APPLE__)
	#undef  MP_TARGET_PLATFORM
	#define MP_TARGET_PLATFORM         MP_PLATFORM_MAC
#endif

// iphone
#if defined(MP_TARGET_OS_IPHONE)
#undef  MP_TARGET_PLATFORM
#define MP_TARGET_PLATFORM         MP_PLATFORM_IOS
#endif

// android
#if defined(ANDROID)
	#undef  MP_TARGET_PLATFORM
	#define MP_TARGET_PLATFORM         MP_PLATFORM_ANDROID
#endif

// win32
#if defined(_WIN32) && defined(_WINDOWS)
	#undef  MP_TARGET_PLATFORM
	#define MP_TARGET_PLATFORM         MP_PLATFORM_WIN32
#endif

// linux
#if defined(LINUX) && !defined(__APPLE__)
	#undef  MP_TARGET_PLATFORM
	#define MP_TARGET_PLATFORM         MP_PLATFORM_LINUX
#endif

// marmalade
#if defined(MARMALADE)
#undef  MP_TARGET_PLATFORM
#define MP_TARGET_PLATFORM         MP_PLATFORM_MARMALADE
#endif

// bada
#if defined(SHP)
#undef  MP_TARGET_PLATFORM
#define MP_TARGET_PLATFORM         MP_PLATFORM_BADA
#endif

// qnx
#if defined(__QNX__)
#undef  MP_TARGET_PLATFORM
#define MP_TARGET_PLATFORM     MP_PLATFORM_BLACKBERRY
#endif

// native client
#if defined(__native_client__)
#undef  MP_TARGET_PLATFORM
#define MP_TARGET_PLATFORM     MP_PLATFORM_NACL
#endif

// Emscripten
#if defined(EMSCRIPTEN)
#undef  MP_TARGET_PLATFORM
#define MP_TARGET_PLATFORM     MP_PLATFORM_EMSCRIPTEN
#endif

// tizen
#if defined(TIZEN)
#undef  MP_TARGET_PLATFORM
#define MP_TARGET_PLATFORM     MP_PLATFORM_TIZEN
#endif

// qt5
#if defined(MP_TARGET_QT5)
#undef  MP_TARGET_PLATFORM
#define MP_TARGET_PLATFORM     MP_PLATFORM_QT5
#endif

// WinRT (Windows Store App)
#if defined(WINRT)
#undef  MP_TARGET_PLATFORM
#define MP_TARGET_PLATFORM			MP_PLATFORM_WINRT
#endif

// WP8 (Windows Phone 8 App)
#if defined(WP8)
#undef  MP_TARGET_PLATFORM
#define MP_TARGET_PLATFORM			MP_PLATFORM_WP8
#endif


#if MP_TARGET_PLATFORM == MP_PLATFORM_WIN32

#if defined(MP_STATIC_LIB)
#define MP_DLL_API
#else
#if defined(MP_DYNAMIC_LIB)
#define MP_DLL_API	__declspec(dllexport)
#define MP_EXPIMP_TEMPLATE
#else         /* use a DLL library */
#define MP_DLL_API	__declspec(dllimport)
#define MP_EXPIMP_TEMPLATE extern
#endif  
#endif

#elif MP_TARGET_PLATFORM == MP_PLATFORM_WINRT

#if defined(MP_STATIC_LIB)
#define MP_DLL_API
#else
#if defined(MP_DYNAMIC_LIB)
#define MP_DLL_API	__declspec(dllexport)
#define MP_EXPIMP_TEMPLATE
#else         /* use a DLL library */
#define MP_DLL_API	__declspec(dllimport)
#define MP_EXPIMP_TEMPLATE extern
#endif  
#endif

#elif MP_TARGET_PLATFORM == MP_PLATFORM_ANDROID
#elif MP_TARGET_PLATFORM == MP_PLATFORM_IOS
#define MP_DLL_API
#define MP_EXPIMP_TEMPLATE
#endif

#if MP_TARGET_PLATFORM == MP_PLATFORM_LINUX
#define MP_DLL_API
#endif


#if MP_TARGET_PLATFORM == MP_PLATFORM_ANDROID
#define MP_DLL_API
#define MP_EXPIMP_TEMPLATE
#endif

#if defined(__GNUC__) && ((__GNUC__ >= 4) || ((__GNUC__ == 3) && (__GNUC_MINOR__ >= 1)))
#define MP_DEPRECATED_ATTRIBUTE __attribute__((deprecated))
#elif _MSC_VER >= 1400 //vs 2005 or higher
#define MP_DEPRECATED_ATTRIBUTE __declspec(deprecated) 
#else
#define MP_DEPRECATED_ATTRIBUTE
#endif

#endif