#include "MpManager.h"

MpManager::MpManager() {

}

MpManager::~MpManager() {
	clear();
}

static MpManager* _mpManager = nullptr;
MpManager* MpManager::getInstance() {
	if (_mpManager == nullptr)
		_mpManager = new MpManager();
	return _mpManager;
}

void MpManager::push_back(MpMessage* msg) 
{
	_lstMsgStack.push_back(msg);
}

void MpManager::clear() {
	while (_lstMsgStack.size() > 0) 
	{
		MpMessage* msg = _lstMsgStack.back();
		if (msg) 
		{
			delete msg;
			msg = NULL;
			_lstMsgStack.pop_back();
		}
	}
}

MpMessage* MpManager::getMessenger() 
{
	MpMessage* msg = NULL;
	if (_lstMsgStack.size() > 0) 
	{
		msg = _lstMsgStack.at(0);
		_lstMsgStack.erase(_lstMsgStack.begin() + 0);
	}
	
	return msg;
}
