#include "MpClientManager.h"
#include "MpManager.h"
#include "libs/EGScene.h"
#include "mpmessagerequest.h"

MpClientManager::MpClientManager() 
{
	_mpClient = std::make_shared<MpClient>();
	_isReady = false;
	_host = "";
	_port = 0;
	_isWatingResponse = false;
	_isStart = false;
	_isEnable = false;

	_timeDelay = new WClock();
	_timeResponse = new WClock();
	_timeAutoSendPing = new WClock();
	_timeAutoStatusMpClient = new WClock();
}

MpClientManager::~MpClientManager() 
{
	//delete _mpClient;
	_mpClient = nullptr;
	delete _timeDelay;
	_timeDelay = nullptr;
	delete _timeResponse;
	_timeResponse = nullptr;
	delete _timeAutoSendPing;
	_timeAutoSendPing = nullptr;
	delete _timeAutoStatusMpClient;
	_timeAutoStatusMpClient = nullptr;
}

static MpClientManager* _mpclientmanager = nullptr;
static MpClientManager* _mpclientRankmanager = nullptr;
static MpClientManager* _mpclientNormalmanager = nullptr;
static int mCurrentType = 0;
MpClientManager* MpClientManager::getInstanceOnetime(int pType) 
{
	if (pType == 0)
	{
		if (_mpclientmanager == nullptr) 
		{
			_mpclientmanager = new MpClientManager();
			Director::getInstance()->getScheduler()->scheduleUpdate(_mpclientmanager, 0, false);
		}
		return _mpclientmanager;
	}
	else if (pType == 2){
		if (_mpclientRankmanager == nullptr) {
			_mpclientRankmanager = new MpClientManager();
			Director::getInstance()->getScheduler()->scheduleUpdate(_mpclientRankmanager, 0, false);
		}
		return _mpclientRankmanager;
	}
	else{
		if (_mpclientNormalmanager == nullptr) {
			_mpclientNormalmanager = new MpClientManager();
			Director::getInstance()->getScheduler()->scheduleUpdate(_mpclientNormalmanager, 0, false);
		}
		return _mpclientNormalmanager;
	}
}

void MpClientManager::createMpClientInstances(){
	//delete _mpClient;
	//_mpClient = new MpClient();
	
}

MpClientManager* MpClientManager::getInstance(int pType)
{
	if (pType != -1){
		mCurrentType = pType;
	}
	if (mCurrentType == 0)
	{
		if (_mpclientmanager == nullptr) {
			_mpclientmanager = new MpClientManager();
			Director::getInstance()->getScheduler()->scheduleUpdate(_mpclientmanager, 0, false);
		}
		return _mpclientmanager;
	}
	else if (mCurrentType == 2)
	{
		if (_mpclientRankmanager == nullptr) {
			_mpclientRankmanager = new MpClientManager();
			Director::getInstance()->getScheduler()->scheduleUpdate(_mpclientRankmanager, 0, false);
		}
		return _mpclientRankmanager;
	}
	else{
		if (_mpclientNormalmanager == nullptr) 
		{
			_mpclientNormalmanager = new MpClientManager();
			Director::getInstance()->getScheduler()->scheduleUpdate(_mpclientNormalmanager, 0, false);
		}
		return _mpclientNormalmanager;
	}

}

void MpClientManager::detroyInstance() {
	Director::getInstance()->getScheduler()->unscheduleAllForTarget(_mpclientmanager);
	delete _mpclientmanager;
	_mpclientmanager = nullptr;
}

void MpClientManager::connect(std::string host, uint16_t port, bool reconnect) {
	unsigned short portValue = port;
	char hostIp[200] = { 0 };
	_convertIp(host.c_str(), hostIp);
	this->connect(hostIp, portValue, reconnect);
}

void MpClientManager::connect(const char* host, unsigned short port, bool reconnect) {
	if (!strlen(host) || port <= 0) return;
	if (reconnect || !(!strcmp(host, _host) && (port == _port))) {
		_connect(host, port);
	}
}

void MpClientManager::sendMessage(MpMessage* msg, bool isWatingResponse)
{	
	_listMessengeSend.push_back(msg);
	_isWatingResponse = isWatingResponse;
	_timeAutoSendPing->start();
	_timeResponse->start();
}

void MpClientManager::close() {
	while (_listMessengeSend.size() > 0) {
		MpMessage* msg = _listMessengeSend[0];
		if (msg) {
			_mpClient->sendMessage(msg);
			_listMessengeSend.erase(_listMessengeSend.begin() + 0);
			delete msg;
		}
	}
	//delete _mpClient;
	_mpClient = std::make_shared<MpClient>();
	_isReady = false;
	_host = "";
	_port = 0;
	_isStart = false;
	_isWatingResponse = false;
}

void MpClientManager::clearOldMsg() {
	while (_listMessengeSend.size() > 0) {
		MpMessage* msg = _listMessengeSend[0];
		if (msg) {
			_listMessengeSend.erase(_listMessengeSend.begin() + 0);
			delete msg;
		}
	}
}

void MpClientManager::disconnect(bool isShowing) {
	if (_mpClient->isReady()) {
		_isReady = false;
		_host = "";
		_port = 0;
		_isStart = false;
		_isWatingResponse = false;
		while (_listMessengeSend.size() > 0) {
			MpMessage* msg = _listMessengeSend.back();
			_listMessengeSend.pop_back();
			delete msg;
		}
		if (isShowing) {
		/*	TopLayer::getInstance()->notificationInClient(ERR_LOCAL_CONNECT_FAILED, [=]{
				auto sceneNow = Director::getInstance()->getRunningScene();
				if (sceneNow) {
					if (sceneNow->getTag() != 1)
						Director::getInstance()->replaceScene(BaseScene::getInstance());
				}
				else Director::getInstance()->replaceScene(BaseScene::getInstance());
				BaseScene::getInstance()->replaceScene(SceneType::TYPE_LOGIN);
			});*/
		}
	}
}

bool MpClientManager::isReady()
{
	return  _isReady;
}

void MpClientManager::_connect(const char* host, unsigned short port) {
	_host = host;
	_port = port;
	_isStart = true;

	_mpClient->connect(_host, _port);
	_isReady = _mpClient->isReady();
	_timeDelay->start();
	_timeResponse->start();
	_timeAutoSendPing->start();
	_timeAutoStatusMpClient->start();
}

void MpClientManager::_autoSendRequestPing() 
{	
	/*MpMessageRequest* pMsg = new MpMessageRequest(MP_MSG_PING);
	pMsg->setTokenId(UserInfo::getInstance()->getTokenId());
	this->sendMessage(msg);*/
	return;
}

void MpClientManager::_autoCheckStatusMpClient() {
	if (_timeAutoStatusMpClient->getDelayMilisecond() >= CONST_DELAY_READY_STATUS) {
		_timeAutoStatusMpClient->start();
		_isReady = _mpClient->isReady();
	}
}

void MpClientManager::setResponed() {
	_isWatingResponse = false;
}

void MpClientManager::update(float) {
	if (!_isEnable) {
		_timeDelay->start();
		_timeResponse->start();
		_timeAutoSendPing->start();
		_timeAutoStatusMpClient->start();
		return;
	}
	if (!_isStart) return;
	//if (_timeDelay->getDelayMilisecond() < ) return;
	//_timeDelay->start();
	MpMessage* msgHeader = _mpClient->getMessage();

	if (_isReady) {
		if (msgHeader != NULL) {
			MpManager::getInstance()->push_back(msgHeader);
			setResponed();
			//TopLayer::getInstance()->resetClient();
		}
		else {
			if (_mpClient->getState() == MpClient::MpStateNetworkError) {
				_autoCheckStatusMpClient();
				return;
			}
		}

		while (_listMessengeSend.size() > 0) {
			MpMessage* msg = _listMessengeSend[0];
			if (msg) {
				if (_mpClient->isReady() && _mpClient->sendMessage(msg)) {
					_listMessengeSend.erase(_listMessengeSend.begin() + 0);
					delete msg;
				}
				else {
					_autoCheckStatusMpClient();
					break;
				}
			}
			else _listMessengeSend.erase(_listMessengeSend.begin() + 0);
		}

		if (_isWatingResponse) {
			if (_timeResponse->getDelay() >= CONST_TIME_DELAY_TIMEOUT) {
				disconnect();
				if (Director::getInstance()->getRunningScene()){
					((EGScene*)Director::getInstance()->getRunningScene())->setErrorCode(TEXT_ERR_NOCONNECTION);
				}
				return;
			}
		}
		if (_timeAutoSendPing->getDelay() >= CONST_TIME_PING_TIMEOUT) {
			_timeAutoSendPing->start();
			_autoSendRequestPing();
		}
	}
	else {
		_autoCheckStatusMpClient();
		if (msgHeader == NULL) {
			if (_mpClient->getState() == MpClient::MpStateNetworkError) {
				disconnect();
				return;
			}
		}

		if (_timeResponse->getDelay() >= CONST_TIME_DELAY_TIMEOUT) {
			disconnect();
			return;
		}
	}
}
