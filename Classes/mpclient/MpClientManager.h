#ifndef __MPCLIENT_MANAGER_H_INCLUDED__
#define __MPCLIENT_MANAGER_H_INCLUDED__

#include "MpConfig.h"
#include "cocos2d.h"
#include "libs/WClock.h"
static std::vector<std::string> pLstData;
class MpClientManager 
{
public:
	static MpClientManager* getInstance(int pType  = -1);
	static MpClientManager* getInstanceOnetime(int pType );
	static std::vector<std::string> getVectorFromParam(std::string parom0)
	{
		const uint8_t *pParam = (const uint8_t *)parom0.data();
		const uint8_t *pStart = (const uint8_t *) parom0.data();

		pLstData.clear();
		
		while (pStart < pParam + parom0.size()) 
		{
			size_t pLen = *pStart;
			uint8_t *data = new uint8_t[pLen];
			memcpy(data, (uint8_t*)pStart + 1, pLen);
			pStart += (pLen + 1);

			std::string pStrData = std::string((const char*)data, pLen);
			pLstData.push_back(pStrData);
		}
		return pLstData;
	}

	static std::vector<std::vector<std::string> > getVectorInVectorMsgByTag(MpMessage* pResponse, unsigned int pTag)
	{
		std::vector<std::vector<std::string>> pLstBigData;
		std::string pListFriend = pResponse->getString(pTag);
		//char *pParam = param0->paramData->buf;
		const uint8_t *pParam = (const uint8_t *)pListFriend.data();
		const uint8_t *pStart = (const uint8_t *)pListFriend.data();
		while (pStart < pParam + pListFriend.size())
		{
			size_t pLen = *pStart;
			uint8_t *data = new uint8_t[pLen];
			memcpy(data, (uint8_t*)pStart + 1, pLen);
			pStart += (pLen + 1);

			std::string param1 =(const char*) data;
			std::vector<std::string> _lstData = getVectorFromParam(param1);

			pLstBigData.push_back(_lstData);
		}
		return pLstBigData;
	}

	void detroyInstance();

public:
	void connect(std::string host, uint16_t port, bool reconnect = false);
	void connect(const char* host, unsigned short port, bool reconnect = false);
	void sendMessage(MpMessage* msg, bool isWatingResponse = false);
	void close();
	void disconnect(bool isShowing = true);
	bool isReady();
	void setResponed();
	void createMpClientInstances();
	void clearOldMsg();

private:
	void _connect(const char* host, unsigned short port);
	void _autoSendRequestPing();
	void _autoCheckStatusMpClient();

public:
	void update(float = 0);

private:
	CC_SYNTHESIZE(bool, _isEnable, MpClientEnable);

private:
	std::vector<MpMessage*> _listMessengeSend;
	//MpClient* _mpClient;
	std::shared_ptr<MpClient> _mpClient;
	bool _isReady;
	bool _isStart;
	const char* _host;
	unsigned short _port;
	bool _isWatingResponse;

private:
	WClock* _timeDelay, *_timeResponse, *_timeAutoSendPing;
	WClock* _timeAutoStatusMpClient;

private:
	MpClientManager();
	~MpClientManager();
};

#endif
