#ifndef __MPCONFIG_H_INCLUDED__
#define __MPCONFIG_H_INCLUDED__ 

//#include "m2w_libs/WLibrary.h"
//#include "layer/TopLayer.h"
//#include "layer/ToastLayer.h"
//#include "scene/BaseScene.h"
#include "cocos2d.h"
#include "libs/WClock.h"
#include "mplib.h"
#include "MpManager.h"
//#include "message/IncludeMessage.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 || CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
#include <winsock2.h>
#include <ws2tcpip.h>

#pragma comment (lib, "Ws2_32.lib")
#else 
#include<sys/socket.h>
#include<netdb.h> 
#include<arpa/inet.h>
#endif

#define CONST_TIME_DELAY_TIMEOUT 30
#define CONST_TIME_PING_TIMEOUT 20
#define CONST_TIME_DIALOG_TIMEOUT 30
#define CONST_DELAY_READY_STATUS 1.f

#pragma warning (disable : 4101)

static int _convertIp(const char* hostname, char* ip) {
	int sockfd;
	struct addrinfo hints, *servinfo, *p;
	struct sockaddr_in *h;
	int rv;

#ifdef _WIN32
	// Initialize Winsock
	WSADATA wsaData;
	int iResult;

	DWORD dwRetval;

	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		return 1;
	}

#endif
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC; // use AF_INET6 to force IPv6
	hints.ai_socktype = SOCK_STREAM;

	if ((rv = getaddrinfo(hostname, NULL, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// loop through all the results and connect to the first we can
	for (p = servinfo; p != NULL; p = p->ai_next)
	{
		if (p->ai_family == AF_INET)
		{
			h = (struct sockaddr_in *) p->ai_addr;
			strcpy(ip, inet_ntoa(h->sin_addr));
			break;
		}
	}

	freeaddrinfo(servinfo); // all done with this structure
	return 0;
}
#endif

