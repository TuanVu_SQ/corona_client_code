#pragma once

#include "MpMessage.h"
//#pragma warning (disable : 4251)
#include <string>
// Export template to disable waning 4251
//MP_EXPIMP_TEMPLATE template class MP_DLL_API std::allocator<char>;
//MP_EXPIMP_TEMPLATE template struct MP_DLL_API std::char_traits<char>;
//MP_EXPIMP_TEMPLATE template class MP_DLL_API std::basic_string<char, struct std::char_traits<char>, class std::allocator<char> >;

namespace mp
{
	namespace network { class TcpClient; }
	namespace protocol
	{	
		class MP_DLL_API MpClient
		{

		public:
			enum MpState
			{
				MpStateTimeout = 0,
				MpStateNetworkError,
			};

		public:
			static MpClient* create();
			static void destroy(MpClient *&);

		public:
			MpClient();
			~MpClient();

			MpState connect(const char *server, unsigned short port);
			MpState getState() const;
			bool isReady() const;
			bool sendMessage(const MpMessage *pMsg) const;
			void setTimeout(size_t timeout);
			void setKeepalive(size_t keepaliveTime);
			MpMessage *getMessage();

		private:
			MpClient( const MpClient &) = delete;
			MpClient& operator=(const MpClient &) = delete;

		private:
			std::string m_strServer;
			unsigned short m_nPort;
			size_t m_nTimeout;
			size_t m_nKeepaliveTime;
			mp::network::TcpClient *m_pTcpClient;
			MpState m_nState;
			char *_pBuf;
			int  _size;
			int  _pos;
		};
	}
}


