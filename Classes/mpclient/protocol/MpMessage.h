#pragma once
#include <string>
#include <stdint.h>
#include "../platform/PlatformDefine.h"

namespace mp
{
	namespace protocol
	{		
		class MP_DLL_API MpMessage
		{
		public:			
			MpMessage(uint32_t tag);			
			virtual ~MpMessage();

			static MpMessage* create(char *&buffer, int &len);

			uint32_t getType() const;

			void addUInt64(uint32_t tag, uint64_t number);		//compatible with server
			void addUInt32(uint32_t tag, uint32_t number);		//compatible with server	
			void addUInt16(uint32_t tag, uint16_t number);		//compatible with server
			void addUInt8(uint32_t tag, uint8_t number);		//compatible with server			
			void addInt(uint32_t tag, uint64_t number);

			bool addString(uint32_t tag, const std::string &buffer);						
			
			bool getRawData(char *buffer, int & len) const;

			bool getString(uint32_t tag, std::string & data) const;
			std::string getString(uint32_t tag, const std::string &def = "") const;
			
			uint64_t getInt(uint32_t tag) const;

			MP_DEPRECATED_ATTRIBUTE bool getUInt32(uint32_t tag, uint32_t &number) const;	// using getInt instead
			MP_DEPRECATED_ATTRIBUTE bool getUInt16(uint32_t tag, uint16_t &number) const;	// using getInt instead
			MP_DEPRECATED_ATTRIBUTE bool getUInt8(uint32_t tag, uint8_t &number) const;		// using getInt instead
			MP_DEPRECATED_ATTRIBUTE void addComponent(uint32_t tag, const uint8_t* buffer, uint32_t len);
			MP_DEPRECATED_ATTRIBUTE bool getDataByTag(uint32_t tag, std::string& data) const;

		protected:
			MP_DEPRECATED_ATTRIBUTE MpMessage();
			
		private:
			MpMessage(void*);			
			MpMessage(const MpMessage&) = delete;
			MpMessage &operator=(const MpMessage&) = delete;

		private:
			struct Ajnomoto;
			Ajnomoto *p;			
		};
	}
}


