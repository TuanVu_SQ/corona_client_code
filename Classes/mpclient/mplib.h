#ifndef __MP_LIB_H_INCLUDED__
#define __MP_LIB_H_INCLUDED__ 

#include "platform/PlatformDefine.h"
#include "protocol/MpClient.h"

#include "message/MessageParam.h"
#include "message/MessageType.h"

using namespace mp;
using namespace protocol;
#endif