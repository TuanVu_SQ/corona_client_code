#include "MpTcpManage.h"
//#include "common/ClientInfo.h"

MpTcpManager::MpTcpManager() {
	_mpClient = new MpClient();
	_timeDelay = new WClock();
	_timeOut = new WClock();

	_isWatingResponse = false;
}

MpTcpManager::~MpTcpManager() {
	delete _mpClient;
	_mpClient = nullptr;
	delete _timeDelay;
	_timeDelay = nullptr;
}

MpTcpManager* MpTcpManager::create() {
	MpTcpManager* mptcp = new MpTcpManager();
	mptcp->_connect();
	return mptcp;
}

void MpTcpManager::_connect() {
	char hostIp[200] = { 0 };
	//_convertIp(configuration::Config::getInstance()->getHostIp(), hostIp);
	//_mpClient->connect(hostIp, configuration::Config::getInstance()->getHostPort());
}

void MpTcpManager::sendMessage(MpMessage* msg, bool isWatingResponse) {
	_listMessengeSend.push_back(msg);
	_isWatingResponse = isWatingResponse;

	Director::getInstance()->getScheduler()->scheduleUpdate(this, 0, false);
}

void MpTcpManager::update(float dt) {
	if (_timeDelay->getDelayMilisecond() < 0.2f) return;
	_timeDelay->start();

	if (_timeOut->getDelayMilisecond() > 15) {
		Director::getInstance()->getScheduler()->unscheduleAllForTarget(this);
		this->release();
		return;
	}

	while (_listMessengeSend.size() > 0) {
		MpMessage* msg = _listMessengeSend[0];
		if (msg) {
			if (_mpClient->sendMessage(msg)) {
				_listMessengeSend.erase(_listMessengeSend.begin() + 0);
				delete msg;
			}
			else {
				_mpClient->isReady();
				return;
			}
		}
		else _listMessengeSend.erase(_listMessengeSend.begin() + 0);
	}

	MpMessage* msgHeader = _mpClient->getMessage();

	if (msgHeader != NULL) {
		MpManager::getInstance()->push_back(msgHeader);
		_isWatingResponse = false;
	}

	if (!_isWatingResponse) {
		Director::getInstance()->getScheduler()->unscheduleAllForTarget(this);
		this->release();
	}
}
