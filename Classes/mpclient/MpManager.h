#ifndef __MP_MANAGER_H_INCLUDED__
#define __MP_MANAGER_H_INCLUDED__

#include "MpConfig.h"

class MpManager {
public:
	static MpManager* getInstance();

public:
	void push_back(MpMessage* msg);
	void clear();
	MpMessage* getMessenger();

private:
	std::vector<MpMessage*> _lstMsgStack;

private:
	MpManager();
	~MpManager();
};

#endif
