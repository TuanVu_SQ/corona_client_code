#include "ChooseRoomLayer.h"

ChooseRoomLayer* ChooseRoomLayer::createLayer(){
	ChooseRoomLayer* layer = new ChooseRoomLayer();
	layer->initLayer();
	layer->autorelease();
	return layer;
}
void ChooseRoomLayer::initLayer(){
	mCacheListRoom = NULL;
	Size _size = Director::getInstance()->getWinSize();
	LayerColor::initWithColor(Color4B(0, 178, 191, 100), _size.width, _size.height-62);
	tableView = TableView::create(this, Size(700, 350));
	tableView->setDirection(ScrollView::Direction::VERTICAL);
	tableView->setPosition(Vec2(35,10));
	tableView->setDelegate(this);
	tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	this->addChild(tableView);
	tableView->reloadData();
}
void ChooseRoomLayer::tableCellTouched(TableView* table, TableViewCell* cell)
{
}
Size ChooseRoomLayer::tableCellSizeForIndex(TableView *table, ssize_t idx)
{
	return Size(700, 180);
}

TableViewCell* ChooseRoomLayer::tableCellAtIndex(TableView *table, ssize_t idx)
{
	auto string = String::createWithFormat("%ld", idx);
	TableViewCell *cell = table->dequeueCell();
	if (!cell) {
		cell = new TableViewCell();
		cell->autorelease();
		int pMaxRow = std::min((int)idx * 4 + 4, (int)mCacheListRoom->size()) - idx * 4;
		for (int i = 0; i < pMaxRow; i++){
			auto spriteRoom = EGButtonSprite::createWithSpriteFrameName(IMG_ROOM_SPRITE);
			spriteRoom->setButtonType(EGButton2FrameDark);
			spriteRoom->setBindingParrentTouch(false);
			
			spriteRoom->setPosition(Vec2(95 + i * 175, 110));
			cell->addChild(spriteRoom);
			spriteRoom->setTag(i+1);
			Sprite* boxBetMoney = Sprite::createWithSpriteFrameName(IMG_BOX_INFO_BETMONEY);
			spriteRoom->addChild(boxBetMoney);
			boxBetMoney->setPosition(Vec2(spriteRoom->getContentSize().width/2,-25));
			Sprite* iconCoin = Sprite::createWithSpriteFrameName(IMG_ICON_BETMONEY);
			boxBetMoney->addChild(iconCoin);
			iconCoin->setPosition(Vec2(0, boxBetMoney->getContentSize().height / 2));
			/*for (int j = 3; j >= 0; j--){
				int index = 3 - j;
				if (index > 1){
					index++;
				}
				Vec2 pPosition = EGSupport::getPointInCircle(spriteRoom->getContentSize().width / 2, spriteRoom->getContentSize().height / 2, spriteRoom->getContentSize().width / 2 + 15, -80 + 40 * index);
				Sprite* pStatus = Sprite::createWithSpriteFrameName(String::createWithFormat(IMG_STATUS_SLOT, 1)->getCString());
				spriteRoom->addChild(pStatus);
				pStatus->setPosition(pPosition);
				pStatus->setTag(3 + index);
			}*/
			Sprite* labelRoom = Sprite::createWithSpriteFrameName(IMG_LABEL_ROOM);
			spriteRoom->addChild(labelRoom);
			labelRoom->setPosition(Vec2(spriteRoom->getContentSize().width / 2, spriteRoom->getContentSize().height / 2 + 5));

			Label* numRoom = Label::createWithBMFont(IMG_NUM_ROOM, String::createWithFormat("%d", idx * 4 + i)->getCString());
			spriteRoom->addChild(numRoom);
			numRoom->setAlignment(kCCTextAlignmentCenter);
			numRoom->setPosition(Vec2(spriteRoom->getContentSize().width / 2, spriteRoom->getContentSize().height / 2 - 18));
			numRoom->setTag(1);
		
			EGNumberSprite* numBetMoney = EGNumberSprite::createWithSpriteFrameName(IMG_NUM_BETMONEY);
			numBetMoney->setNumber(10000000, true);
			spriteRoom->addChild(numBetMoney);
			numBetMoney->setPosition(Vec2(50, boxBetMoney->getPositionY()));
			numBetMoney->setTag(2);
		}
		reloadInfoRoom(cell, idx);
	}
	else
	{
		reloadInfoRoom(cell, idx);

	}
	return cell;
}
void ChooseRoomLayer::reloadInfoRoom(TableViewCell *cell,int idx){
	int pMaxRow = std::min((int)idx * 4 + 4, (int)mCacheListRoom->size()) - idx * 4;
	for (int i = 0; i < 4; i++){
		
	
		auto spriteRoom = cell->getChildByTag(i + 1);
		if (spriteRoom){
			if (i < pMaxRow){
				spriteRoom->setVisible(true);
				ClientTableInfo pInfoRoom = mCacheListRoom->at(idx * 4 + i);
				((EGButtonSprite*)spriteRoom)->setOnTouch(CC_CALLBACK_0(ChooseRoomLayer::chooseRoom, this, idx * 4 + i));
				spriteRoom->setUserObject(CCInteger::create(idx * 4 + i));
				((Label*)spriteRoom->getChildByTag(1))->setString(__String::createWithFormat("%d", idx * 4 + i + 1)->getCString());
				EGNumberSprite* numBetMoney = ((EGNumberSprite*)spriteRoom->getChildByTag(2));
				numBetMoney->setNumber(pInfoRoom.betMoney);
				int mMaxPlayer = pInfoRoom.maxPlayer;
				int pTypeRoom = 1;
				if (pInfoRoom.team1 == 0){
					if (pInfoRoom.totalPlayer > 0){
						pTypeRoom = 0;
					}
				}
				for (int index = 0; index < mMaxPlayer; index++){
					Sprite* pStatus = (Sprite*)spriteRoom->getChildByTag(3 + index);
					Vec2 pPosition = EGSupport::getPointInCircle(spriteRoom->getContentSize().width / 2, spriteRoom->getContentSize().height / 2, spriteRoom->getContentSize().width / 2 + 15, -(mMaxPlayer - 1) * 40 / 2 + index * 40);
					if (!pStatus){
						pStatus = Sprite::createWithSpriteFrameName(String::createWithFormat(IMG_STATUS_SLOT, 1)->getCString());
						spriteRoom->addChild(pStatus);

					}
					pStatus->setTag(3 + index);
					pStatus->setPosition(pPosition);

					pStatus->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(String::createWithFormat(IMG_STATUS_SLOT, 1)->getCString()));
					if (pTypeRoom == 0){
						int pPLayerCount = pInfoRoom.totalPlayer;
						if (index < pPLayerCount){
							pStatus->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(String::createWithFormat(IMG_STATUS_SLOT, 2)->getCString()));
						}
					}
					else{
						int pPlayerCount1 = pInfoRoom.team1;
						int pPlayerCount2 = pInfoRoom.totalPlayer - pInfoRoom.team1;
						if (index < mMaxPlayer / 2 && index >= mMaxPlayer / 2 - pPlayerCount1){
							pStatus->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(String::createWithFormat(IMG_STATUS_SLOT, 3)->getCString()));
						}
						else if (index < mMaxPlayer / 2 + pPlayerCount2 && index >= mMaxPlayer / 2) {
							pStatus->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(String::createWithFormat(IMG_STATUS_SLOT, 4)->getCString()));
						}
					}
				}
			}
			else{
				spriteRoom->setVisible(false);
			}
		}
	}
}
ssize_t ChooseRoomLayer::numberOfCellsInTableView(TableView *table)
{
	if (mCacheListRoom){
		if (mCacheListRoom->size() % 4 != 0){
			return (mCacheListRoom->size() / 4) + 1;
		}
		else{
			return (mCacheListRoom->size() / 4);
		}
	}
	return 0;
}
void ChooseRoomLayer::loadListRoom(std::vector<ClientTableInfo>* pCacheListRoom){
	mCacheListRoom = pCacheListRoom;

	tableView->reloadData();
}