#include "LayerNotice.h"
#include "message/MessageType.h"
#include "message/MessageParam.h"
#include "common/GameInfo.h"
#include "MpClientManager.h"
#include "message/mppingmessagerequest.h"

std::string mErrorCode;
LayerNotice* LayerNotice::createLayer(){
	LayerNotice* layer = new LayerNotice();
	layer->initLayer();
	layer->autorelease();
	return layer;
}

void LayerNotice::initLayer(){
	Node::init();
	mDelayTime = 0;
	schedule(schedule_selector(LayerNotice::updateNotice), 0);
}

void LayerNotice::connect(){
	MpClientManager::getInstance()->connect(GameInfo::loginServerAddress, GameInfo::loginServerPort);
}
void senMessage(MpMessage* msg, bool isWatingResponse){
	MpClientManager::getInstance()->sendMessage(msg, isWatingResponse);
}
void LayerNotice::showToast(std::string pToast) {
	if (mLstToast.size() != 0)
		mLstToast.push_back(pToast);
	else {
		mLstToast.push_back(pToast);
		showToastReal(pToast);
	}
}
void LayerNotice::showToast(std::string pToast, float pWidth, Vec2 pPosition){
	if (mLstToast.size() != 0)
		mLstToast.push_back(pToast);
	else {
		mLstToast.push_back(pToast);
		showToastReal(pToast, pWidth, pPosition);
	}
}

void LayerNotice::showNextToast() {
	mLstToast.erase(mLstToast.begin());
	if (mLstToast.size() != 0)
		showToastReal(mLstToast.at(0));
}
void LayerNotice::showToastReal(std::string pToast, float pWidth, Vec2 pPosition) {
	if (mLstToast.size() > 0){
		LayerColor* pLayer = LayerColor::create(Color4B(0, 0, 0, 200), pWidth, 40);
		this->addChild(pLayer);
		Label* pLbl = Label::createWithBMFont("font_vn.fnt", pToast, TextHAlignment::CENTER, pWidth - 6);
		pLayer->addChild(pLbl);
		Label* pLblTest = Label::createWithBMFont("font_vn.fnt", pToast);
		if (pWidth > pLblTest->getContentSize().width){
			pWidth = pLblTest->getContentSize().width + 20;
		}
		pLayer->setContentSize(Size(pWidth, pLbl->getContentSize().height + 6));
		pLbl->setPosition(Vec2(pLayer->getContentSize().width / 2, pLayer->getContentSize().height / 2));
		pLayer->setPosition(Vec2(pPosition.x - pLayer->getContentSize().width / 2, pPosition.y - pLayer->getContentSize().height / 2));
		float pXStart = 800 + pLbl->getContentSize().width / 2;
		float pXEnd = -pLbl->getContentSize().width / 2;
		float pDistances = pXStart - pXEnd;
		mLstToast.erase(mLstToast.begin() + 0);
		if (mLstToast.size() > 0){
			pLbl->runAction(Sequence::createWithTwoActions(DelayTime::create(4), CallFunc::create([=] {
				showToastReal(mLstToast.at(0), pWidth, pPosition);
				pLbl->removeFromParentAndCleanup(true);
				pLayer->removeFromParentAndCleanup(true);
			})));
		}
		else{
			pLbl->runAction(Sequence::createWithTwoActions(DelayTime::create(4), CallFunc::create([=] {
				pLbl->removeFromParentAndCleanup(true);
				pLayer->removeFromParentAndCleanup(true);
			})));
		}
	}
}
void LayerNotice::showToastReal(std::string pToast, float pWidth, Vec2 pPosition, float pDuration) {

	LayerColor* pLayer = LayerColor::create(Color4B(0, 0, 0, 200), pWidth, 40);
	pLayer->setTag(999);
	this->addChild(pLayer);

	Label* pLbl = Label::createWithBMFont("font_vn.fnt", pToast, TextHAlignment::CENTER, pWidth - 6);

	pLayer->addChild(pLbl);
	Label* pLblTest = Label::createWithBMFont("font_vn.fnt", pToast);
	if (pWidth > pLblTest->getContentSize().width){
		pWidth = pLblTest->getContentSize().width + 20;
	}
	pLayer->setContentSize(Size(pWidth, pLbl->getContentSize().height + 6));
	pLbl->setPosition(Vec2(pLayer->getContentSize().width / 2, pLayer->getContentSize().height / 2));
	pLayer->setPosition(Vec2(pPosition.x - pLayer->getContentSize().width / 2, pPosition.y - pLayer->getContentSize().height / 2));
	float pXStart = 800 + pLbl->getContentSize().width / 2;
	float pXEnd = -pLbl->getContentSize().width / 2;
	float pDistances = pXStart - pXEnd;

}
void LayerNotice::showToastReal(std::string pToast) {
	LayerColor* pLayer = LayerColor::create(Color4B(0, 0, 0, 200), 800, 40);
	pLayer->setPosition(Vec2(0, 480 - 40));
	this->addChild(pLayer);

	Label* pLbl = Label::createWithBMFont("font_vn.fnt", pToast);
	pLbl->setPosition(Vec2(800 + pLbl->getContentSize().width / 2, 20));
	pLayer->addChild(pLbl);

	float pXStart = 800 + pLbl->getContentSize().width / 2;
	float pXEnd = -pLbl->getContentSize().width / 2;
	float pDistances = pXStart - pXEnd;
	pLbl->runAction(Sequence::createWithTwoActions(MoveTo::create(pDistances / 100, Vec2(pXEnd, pLbl->getPositionY())), CallFunc::create([=] {
		showNextToast();
		pLbl->removeFromParentAndCleanup(true);
		pLayer->removeFromParentAndCleanup(true);
	})));
}
void LayerNotice::updateNotice(float delta)
{
	if (GameInfo::mUserInfo.mTokenID == "") return;
	mDelayTime += delta;
	if (mDelayTime >= 10)
	{
		mDelayTime = 0;
		MpMessageRequest *pRequest = new MpMessageRequest(MP_MSG_GETBALANCE);
		pRequest->setTokenId(GameInfo::mUserInfo.mTokenID);
		MpClientManager::getInstanceOnetime(0)->sendMessage(pRequest);
	}
	//while (mTcpNotice != NULL && mTcpNotice->getMsgSize() > 0) {
	//	MsgHeader* pMsg = NULL;
	//	mTcpNotice->getMsgAutoResponse(pMsg);
	//	if (pMsg == NULL || pMsg->msgType == MP_NONE) {
	//		if (pMsg){
	//			FreeMsg(pMsg);
	//			pMsg = NULL;
	//		}
	//		continue;
	//	}
	//	MpMessage * response = new MpMessage(*pMsg);
	//	if (response->getMsg()->msgType == MP_MSG_SERVER_NOTICE) {
	//		std::string pContent = response->getString(MP_CONTENT);
	//		int pCount = response->getInt(MP_PARAM_NOTICE_TIMES);
	//		int pState = response->getInt(MP_PARAM_NOTICE_GROUP);
	//		if (pState == mState || mState == NG_ALL){
	//			for (int i = 0; i < pCount; i++){
	//				showToast(pContent);
	//			}
	//		}
	//	}
	//}
}
void LayerNotice::setCurrentState(int pState){
	mState = pState;
}

void LayerNotice::resetTimeCount()
{
	mDelayTime = 0;
}