#ifndef __SHOPSCENE_H_INCLUDED__
#define __SHOPSCENE_H_ICNLUDED__

#include "cocos2d.h"
#include "EGButtonSprite.h"
#include "EGNumberSprite.h"
#include "EGScene.h"
#include "Loading.h"
#include "ObjItem.h"
#include "ObjItemDetails.h"
#include "BoxNotice.h"
#include "BoxNoticeManager.h"
#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "extensions\cocos-ext.h"
#else
#include "cocos-ext.h"
#endif

USING_NS_CC;


class ShopScene :public EGScene, public TableViewDataSource, public TableViewDelegate {
public:
	static Scene* scene();

	void onBackPressed();

	void buyItem(int pItemID, int pCounter);
private:
	virtual ~ShopScene();
	EGSprite* mBackground, *mbackgroundTab;
	EGButtonSprite* mBtnBack, *mBtnAvatar, *mBtnWeapon, *mBtnItem;
	Loading* mLoading;
	BoxNotice* mBoxNotice;
	int mCurrentPage, mCurrentType;
	ObjItemDetails* mBoxObjItemDetails;
	WXmlReader* pXml;

	EGSprite* mSpriteAvatar;
	Label* mLblName, *mLblBalance;
	ScratchCard itemReward;

	//CREATE_FUNC(ShopScene);
	void initScene();
	void reloadDatas();
	void update(float dt);

private:
	Node* _nodeReward, *_nodeAvatar;
	std::vector<EGSprite*> _lstRadioButton;
	TableView* mViewAvatar, *mViewReward;
	std::vector<ScratchCard> lstRewardBuffer;

private:
	virtual void scrollViewDidScroll(ScrollView* view) {};
	virtual void scrollViewDidZoom(ScrollView* view) {};
	virtual void tableCellTouched(TableView* table, TableViewCell* cell) {};
	virtual Size tableCellSizeForIndex(TableView *table, ssize_t idx);
	virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(TableView *table);

private:
	void btnAvatarClicked();
	void btnLogoPhoneClicked(uint8_t idx);
	void rewardItemClicked(ScratchCard item);
	void showItemDetails(int pItemID, int pExType, int pType);

	void loadLstCardReward();
	void sendRewardMessage(ScratchCard item);
	void executeReward();
};

#endif