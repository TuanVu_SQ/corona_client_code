#ifndef __FRIENDSCENE_H_INCLUDED__
#define __FRIENDSCENE_H_INCLUDED__

#include "cocos2d.h"
#include "EGScene.h"
#include "EGButtonSprite.h"
#include "BoxNotice.h"
#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "extensions\cocos-ext.h"
#else
#include "cocos-ext.h"
#endif
#include "WXmlReader.h"
#include "Loading.h"
#include "ObjFriend.h"
#include "BoxInfo.h"
#include "BoxInfoGame.h"

USING_NS_CC;
using namespace extension;

class FriendScene :public EGScene {
public:
	static Scene* scene();

	void onBackPressed();
private:
	~FriendScene();

	Sprite* mBackground;
	EGButtonSprite* mBtnAddFriend;
	BoxNotice* mBoxNotice;
	Loading* mLoading;
	WXmlReader* mXml;
	BoxInfo* mBoxInfo;

	EditBox* mEdt_Search;
	EGButtonSprite* mBtnSearch;

	Sprite* mTitle;
	EGButtonSprite* mBtnBack;

	Vector<ObjFriend*> mLstFriendObj;
	Vector<Sprite*> mLstButtonPage;
	int mCurrentPage;

	//CREATE_FUNC(FriendScene);
	void initScene();

	void showInfo(std::string pUsername);
	void acceptFriend(std::string pUsername);
	void rejectFriend(std::string pUsername);

	void callDialogAddFriend();
	void addFriend();
	void searchFriend();

	void loadFriend(int pPage);
	void update(float dt);

	BoxInfoGame * mBoxInfoGame;
};

#endif