#include "MailScene.h"
#include "MessageParam.h"
#include "MessageType.h"

std::string mErrorCode_MailScene = "";
uint8_t __TAB = 0;
std::vector<StructMail> _lstMailInbox, _lstMailOutbox;
std::vector<std::string> _lstReceipt;

typedef struct MailInfo_s
{
	uint8_t nType;
	uint8_t nPage;
}MailInfo;

typedef struct MailInfoSend_s
{
	std::string reciever;
	std::string subject;
	std::string content;
}MailInfoSend;

typedef struct MailInfoReaded_s
{
	std::string sender;
	uint32_t time;
}MailInfoReaded;
int mTypeLoad;
int mPageLoad;
void pthread_LoadMail(void* _void) {
	MailInfo* info = (MailInfo*)_void;
	mTypeLoad = info->nType;
	mPageLoad = info->nPage;

	MpMessage* message = new MpMessage((uint32_t)MP_MSG_GET_MAIL);
	message->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
	message->addInt(MP_PAGE, info->nPage);
	message->addInt(MP_PARAM_MAILBOX, info->nType);
	//client_loadMail->sendMsg(message.getMsg());
	MpClientManager::getInstance()->sendMessage(message,true);
	delete info;
}

void pthread_sendMail(void* _void) {
	MailInfoSend* info = (MailInfoSend*)_void;
	
	MpMessage* message = new MpMessage((uint32_t)MP_MSG_SEND_MAIL);
	message->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
	message->addString(MP_PARAM_RECEIVER, info->reciever);
	message->addString(MP_CONTENT, info->content);
	message->addString(MP_PARAM_SUBJECT, info->subject);
	MpClientManager::getInstance()->sendMessage(message,true);
	//client_sendMail->sendMsg(message.getMsg());
	delete info;

}

void pthread_readMail(void* _void) {
	MailInfoReaded* info = (MailInfoReaded*)_void;
	MpMessage *message = new MpMessage((uint32_t)MP_MSG_READ_MAIL);
	message->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
	message->addString(MP_PARAM_SENDER, info->sender);
	message->addInt(MP_PARAM_TIME, info->time);
	//client_readMail->sendMsg(message.getMsg());
	MpClientManager::getInstance()->sendMessage(message);

	delete info;
}

void pthread_deleteMail(void* _void) {
	StructMail* info = (StructMail*)_void;
	MpMessage *message = new MpMessage((uint32_t)MP_MSG_DEL_MAIL);
	message->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
	message->addInt(MP_PARAM_TIME, info->created);
	if (__TAB == 0) //inbox
		message->addString(MP_PARAM_SENDER, info->sender);
	else if (__TAB == 1)
		message->addString(MP_PARAM_RECEIVER, info->receiver);
	message->addInt(MP_PARAM_MAILBOX, __TAB);
	//client_deleteMail->sendMsg(message.getMsg());
	MpClientManager::getInstance()->sendMessage(message);

	delete info;
}

MailScene::MailScene() {
	for (int i = 0; i < 3; i++){
		mLayerMail[i] = NULL;
	}
	mCurrentTab = 0;
	mCurrentPage = 0;
	_nCurrentInboxPage = 0;
	_nCurrentOutboxPage = 0;
	mTableViewCell = NULL;
	boxMail = NULL;
}

MailScene::~MailScene() {

}

MailScene* MailScene::scene() {
	auto scene = new MailScene();
	scene->initScene();
	scene->autorelease();
	return scene;
}

void MailScene::initScene() {
	GameInfo::mUserInfo.stateScene = 0;
	_mXmlreader = WXmlReader::create();
	_mXmlreader->load(KEY_XML_FILE);
	mErrorCode_MailScene = "";

	_nCurrentInboxPage = 0;
	_nCurrentOutboxPage = 0;

	auto sprBackground = EGSprite::create("background_choosetype.png");
	sprBackground->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(sprBackground);

	auto mTitleBg = EGSprite::createWithFile("kv_info_bg.png");
	mTitleBg->setScale(0.7f);
	mTitleBg->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT - mTitleBg->getContentSize().height / 2 ));
	sprBackground->addChild(mTitleBg);

	auto mLblTitle = Label::createWithBMFont(FONT_BMF_NAME, "Email");
	mLblTitle->setScale(1.2f);
	mLblTitle->setPosition(mTitleBg->getContentSize() / 2);
	mTitleBg->addChild(mLblTitle);

	boxMail = BoxMail::createBoxMail();
	boxMail->setPosition(Vec2(0, 0));
	sprBackground->addChild(boxMail, 20);
	boxMail->setVisible(false);
	boxMail->setPosition(Vec2(CLIENT_WIDTH, 0));
	boxMail->setFuncCallBack(CC_CALLBACK_0(MailScene::callBackForBoxMail, this));
	boxMail->setFuncShowNotice(CC_CALLBACK_1(MailScene::showNotice, this));
	boxMail->setFuncSendMail(CC_CALLBACK_3(MailScene::sendMail, this));
	boxMail->setFuncDeleteMail(CC_CALLBACK_1(MailScene::deleteMail, this));

	mBoxNotice = BoxNotice::create();
	mBoxNotice->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBoxNotice, 30);

	mLoading = Loading::create();
	mLoading->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mLoading, 30);

	auto btnBack = EGButtonSprite::createWithFile("button-back-cr (1).png", EGButton2FrameDark);
	btnBack->setPosition(Vec2(btnBack->getContentSize().width / 2 + 10, CLIENT_HEIGHT - btnBack->getContentSize().height / 2 - 10));
	btnBack->setOnTouch([=]{
		//WAudioControl::getInstance()->playEffectSound("snd_button_clicked.mp3");
		if (_funcBack)
			_funcBack();
	});
	sprBackground->addChild(btnBack);

	_btnMessOut = EGButtonSprite::createWithFile("btn_mail_out_1.png", EGButton2FrameDark);
	_btnMessOut->setPosition(Vec2(CLIENT_WIDTH / 2, _btnMessOut->getContentSize().height / 2 + 5));
	_btnMessOut->setOnTouch(CC_CALLBACK_0(MailScene::chooseTab, this, 1, 0));
	sprBackground->addChild(_btnMessOut, 1);

	_btnMessIn = EGButtonSprite::createWithFile("btn_mail_in_0.png", EGButton2FrameDark);
	_btnMessIn->setAnchorPoint(Vec2(1, 0.5f));
	_btnMessIn->setPosition(Vec2(_btnMessOut->getPositionX1() - 30, _btnMessOut->getPositionY()));
	_btnMessIn->setOnTouch(CC_CALLBACK_0(MailScene::chooseTab, this, 0, 0));
	sprBackground->addChild(_btnMessIn, 1);

	_btnMessWrite = EGButtonSprite::createWithFile("btn_mail_create_0.png", EGButton2FrameDark);
	_btnMessWrite->setAnchorPoint(Vec2(0, 0.5f));
	_btnMessWrite->setPosition(Vec2(_btnMessOut->getPositionX2() + 30, _btnMessOut->getPositionY()));
	_btnMessWrite->setOnTouch(CC_CALLBACK_0(MailScene::chooseTab, this, 2, 0));
	sprBackground->addChild(_btnMessWrite, 1);

	mTableSize = Size(CLIENT_WIDTH - 20, 340);
	mTableView = TableView::create(this, mTableSize);
	mTableView->setDelegate(this);
	mTableView->setDirection(ScrollView::Direction::VERTICAL);
	mTableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	mTableView->setPosition(Vec2(10, _btnMessOut->getContentSize().height + 15));
	sprBackground->addChild(mTableView);

	chooseTab(0, 0);
	this->scheduleUpdate();
}

void MailScene::onKeyBack() {
	//WAudioControl::getInstance()->playEffectSound("snd_button_clicked.mp3");
	if (_funcBack)
		_funcBack();
}

TableViewCell* MailScene::tableCellAtIndex(TableView *table, ssize_t idx) {
	TableViewCell* pCell = table->dequeueCell();
	if (pCell)
	{
		pCell->removeAllChildrenWithCleanup(true);
		pCell->removeFromParentAndCleanup(true);
	}
	pCell = new TableViewCell();

	auto element = MailView::createMailView();
	element->setButtonType(EGButtonScrollViewItem);
	element->setPosition(Vec2(400, 32));
	pCell->addChild(element);
	if (mCurrentTab == 0){
		if (idx == _lstMailInbox.size()){
			StructMail str;
			element->setInfoMaill(2, str);
			element->setOnTouch(CC_CALLBACK_0(MailScene::loadMail, this, 0, _nCurrentInboxPage));
		}
		else{
			element->setInfoMaill(0, _lstMailInbox[idx]);
			element->setOnTouch(CC_CALLBACK_0(MailScene::showBoxMail, this, 0, element, idx));
		}
	}
	else if (mCurrentTab == 1){
		if (idx == _lstMailOutbox.size()){
			StructMail str;
			element->setInfoMaill(2, str);
			element->setOnTouch(CC_CALLBACK_0(MailScene::loadMail, this, 1, _nCurrentOutboxPage));
		}
		else{
			element->setInfoMaill(1, _lstMailOutbox[idx]);
			element->setOnTouch(CC_CALLBACK_0(MailScene::showBoxMail, this, 1, element, 0));
		}
	}

	return pCell;
}

ssize_t MailScene::numberOfCellsInTableView(TableView *table) {
	if (mCurrentTab == 0){
		if (_lstMailInbox.size() == 0)
			return 0;
		else
			return _lstMailInbox.size() + 1;
	}
	else if (mCurrentTab == 1){
		if (_lstMailOutbox.size() == 0)
			return 0;
		else
			return _lstMailOutbox.size() + 1;
	}
	return 0;
}

Size MailScene::tableCellSizeForIndex(TableView *table, ssize_t idx) {
	return Size(CLIENT_WIDTH - 20, 75);
}

void MailScene::addNewMailToOutbox(StructMail pInfoMail){
	_lstMailOutbox.push_back(pInfoMail);
	chooseTab(1, 0);
}

void MailScene::onReload(){
	float pHeight = 0;
	float pOldY = 0;
	for (int i = mLstObject.size() - 1; i >= 0; i--) {
		pHeight += mLstHeight.at(i);
		Node* pNode = mLstObject.at(i);
		pNode->setPosition(Vec2(pNode->getContentSize().width / 2, pOldY + mLstHeight.at(i) / 2));
		pOldY += mLstHeight.at(i) + 5;
	}
	mTableView->reloadData();
	if (mCurrentPage > 0){
		float pContentOffSetHeight = pHeight - mTableSize.height;
		if (pContentOffSetHeight > 0)
			mTableView->setContentOffset(Vec2(mTableView->getContentOffset().x, 0));
	}
}

void MailScene::clearData(){
	mLstObject.clear();
	mLstHeight.clear();
	if (mLayerMail[mCurrentTab]){
		mLayerMail[mCurrentTab]->removeAllChildrenWithCleanup(true);
	}
	onReload();
}

void MailScene::chooseTab(int index, int pPage){
	//WAudioControl::getInstance()->playEffectSound("snd_button_clicked.mp3");
	//clearData();
	mCurrentTab = index;
	_nCurrentInboxPage = 0;
	_nCurrentOutboxPage = 0;
	__TAB = mCurrentTab;

	if (index == 0)
		loadMail(0, 0);
	else if (index == 1)
		loadMail(1, 0);
	else if (index == 2){
		mTableView->reloadData();
		showBoxMail(2);
	}

	_btnMessIn->setTexture("btn_mail_in_0.png");
	_btnMessOut->setTexture("btn_mail_out_0.png");
	_btnMessWrite->setTexture("btn_mail_create_0.png");
	switch (index)
	{
	case 0:_btnMessIn->setTexture("btn_mail_in_1.png"); break;
	case 1:_btnMessOut->setTexture("btn_mail_out_1.png"); break;
	case 2:_btnMessWrite->setTexture("btn_mail_create_1.png"); break;
	}
}

void MailScene::showBoxMail(uint8_t type, MailView* mailView, uint8_t idx){
	boxMail->setBoxType(type);
	if (type < 2)
		boxMail->showMessage(type, mailView->getInfo());
	if (type == 0){
		if (mailView->getInfo().status == 0){
			readMail(mailView->getInfo().sender, mailView->getInfo().created);
			mailView->changeReadState();
			_lstMailInbox[idx].status = 1;
		}
	}
	boxMail->setPosition(0, 0);
	boxMail->setVisible(true);

}
void MailScene::deleteMail(StructMail str){
	StructMail* strmail = new StructMail();
	*strmail = str;
	 pthread_deleteMail( strmail);
}

void MailScene::readMail(std::string name, uint32_t time)
{

	MailInfoReaded* str = new MailInfoReaded();
	str->sender = name;
	str->time = time;
	pthread_readMail( str);
}

void MailScene::sendMail(const std::string& name, const std::string& title, const std::string& content){
	mLoading->show();
	MailInfoSend* str = new MailInfoSend();
	str->reciever = name;
	str->subject = title;
	str->content = content;
	pthread_sendMail( str);

}
void MailScene::replyMess(StructMail pMail){
	/*chooseTab(2, 0);
	pMail.nStatus = 1;
	pMail.strContent = "";
	showBoxMail(pMail);*/
}
void MailScene::hideDialog(Node* pNode, int pTab){
	/*pNode->setPosition(Vec2(-_sizePhone.width / 2, -_sizePhone.height / 2));
	pNode->setVisible(false);
	if (pTab != mCurrentTab && pTab >= 0){
	chooseTab(pTab, 0);
	}*/
}
void MailScene::mailView(MailView* view){
	/*WAudioControl::getInstance()->playEffectSound("snd_button_clicked.mp3");
	StructMail pMailInfo = view->getInfo();
	showBoxMail(pMailInfo);
	pMailInfo.nStatus = 1;
	view->setInfoMaill(pMailInfo);*/
}
void MailScene::addStatusCount(int pStatus){
	/*if (!viewStatusCount->getParent()){
		mLayerMail[mCurrentTab]->addChild(viewStatusCount);
		viewStatusCount->setPosition(mLayerMail[mCurrentTab]->getContentSize() / 2);
		}
		mLstObject.push_back(viewStatusCount);
		mLstHeight.push_back(viewStatusCount->getContentSize().height);
		Label* label = (Label*)viewStatusCount->getChildren().at(0);
		label->setString(WXmlReader::getInstance()->getNodeTextByTagName(__String::createWithFormat("txt_status_count_mail%d", pStatus)->getCString()).c_str());
		if (pStatus == 1){
		viewStatusCount->registerTouch();
		label->setColor(Color3B::YELLOW);
		}
		else{
		viewStatusCount->unregisterTouch();
		label->setColor(Color3B::GRAY);
		}
		onReload();*/
}
int MailScene::getCurrentPage(){
	return mCurrentPage;
}

void MailScene::enableCreateMail(bool pBool){
	/*if (pBool){
		_btnTabSendMess->registerTouch();
		}
		else{
		_btnTabSendMess->unregisterTouch();
		}*/
}

void MailScene::callBackForBoxMail()
{
	if (mCurrentTab == 2)
		chooseTab(0, _nCurrentInboxPage);
}

void MailScene::update(float dt)
{
	MpMessage* pMsg = MpManager::getInstance()->getMessenger();
	if (pMsg){
		switch (pMsg->getType())
		{
		case MP_MSG_DEL_MAIL_ACK:
		{
									MpMessage *msg = pMsg;
									uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);

									if (pErrorCode != 0) {
										mErrorCode_MailScene = __String::createWithFormat("%d", pErrorCode)->getCString();
									}
									else {
										/*std::string data = msg.getString(MP_PARAM_EMAIL);
										std::vector<StructMail> lstitem;
										lstitem = StructMail::from_string(data);*/
										mErrorCode_MailScene = TEXT_SUCCESS_GETINFO_DELETE_MAIL;
									}
		}
			break;
		case MP_MSG_READ_MAIL_ACK:
		{
									 MpMessage* msg = pMsg;
									 uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);

									 if (pErrorCode != 0) {
										 mErrorCode_MailScene = __String::createWithFormat("%d", pErrorCode)->getCString();
									 }
									 else {
										 /*std::string data = msg.getString(MP_PARAM_EMAIL);
										 std::vector<StructMail> lstitem;
										 lstitem = StructMail::from_string(data);*/
										 //mErrorCode_MailScene = TEXT_SUCCESS_GETINFO_SEND_MAIL;
									 }
		}
			break;
		case MP_MSG_GET_MAIL_ACK:
		{
									MpMessage *msg = pMsg;
									uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);

									if (pErrorCode != 0) {
										mErrorCode_MailScene = __String::createWithFormat("%d", pErrorCode)->getCString();
									}
									else {
										std::string data = msg->getString(MP_PARAM_EMAIL);
										std::vector<StructMail> lstitem;
										lstitem = StructMail::from_string(data);

										if (mTypeLoad == 0){
											if (lstitem.size() > 0){
												_lstMailInbox = lstitem;
												mErrorCode_MailScene = TEXT_SUCCESS_GETINFO_LOADMAIL;
											}
											else{
												if (mTypeLoad == 0)
													_lstMailInbox = lstitem;
												mErrorCode_MailScene = "txt_mail_load_mail_empty";
											}
										}
										else if (mTypeLoad == 1){
											if (lstitem.size() > 0){
												_lstMailOutbox = lstitem;
												mErrorCode_MailScene = TEXT_SUCCESS_GETINFO_LOADMAIL;
											}
											else{
												if (mPageLoad == 0)
													_lstMailOutbox = lstitem;
												mErrorCode_MailScene = "txt_mail_load_mail_empty";
											}
										}

									}
		}
			break;
		case MP_MSG_SEND_MAIL_ACK:
		{
									 MpMessage *msg = pMsg;
									 uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);

									 _lstReceipt.clear();
									 std::string strData = msg->getString(MP_PARAM_RECEIVER);
									 const char* ptr = strData.data();
									 const char* end = ptr + strData.size();
									 std::string userName;
									 while (ptr < end)
									 {
										 userName = ptr;
										 _lstReceipt.push_back(userName);
										 ptr += userName.size() + 1;
									 }

									 if (pErrorCode != 0) {
										 mErrorCode_MailScene = "send_mail_error";
									 }
									 else {
										 mErrorCode_MailScene = TEXT_SUCCESS_GETINFO_SEND_MAIL;
									 }
		}
			break;
		default:
			break;
		}
delete pMsg;
pMsg = NULL;
	}
	if (getErrorCode() != ""){
		mErrorCode_MailScene = getErrorCode();
		setErrorCode("");
	}
	if (mErrorCode_MailScene != "") {
		mLoading->hide();
		if (mErrorCode_MailScene == TEXT_SUCCESS_GETINFO_LOADMAIL){
			mTableView->reloadData();
			if (__TAB == 0)
				_nCurrentInboxPage++;
			else if (__TAB == 1)
				_nCurrentOutboxPage++;
		}
		else if (mErrorCode_MailScene == "send_mail_error"){
			std::string listName = "";
			for (uint8_t i = 0; i < _lstReceipt.size(); ++i){
				if (i < _lstReceipt.size() - 1)
					listName += _lstReceipt[i] + ", ";
				else
					listName += _lstReceipt[i];
			}

			BoxNoticeManager::getBoxNotice(mBoxNotice,
				_mXmlreader->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				__String::createWithFormat(_mXmlreader->getNodeTextByTagName("txt_acc_not_exists").c_str(), listName.c_str())->getCString());
			mBoxNotice->setVisible(true);
		}
		else if (mErrorCode_MailScene == TEXT_SUCCESS_GETINFO_SEND_MAIL){

			boxMail->setVisible(false);
			boxMail->setPosition(Vec2(CLIENT_WIDTH, 0));
			chooseTab(0, 0);

			if (_lstReceipt.size() > 0){
				std::string listName = "";
				for (uint8_t i = 0; i < _lstReceipt.size(); ++i){
					if (i < _lstReceipt.size() - 1)
						listName += _lstReceipt[i] + ", ";
					else
						listName += _lstReceipt[i];
				}

				BoxNoticeManager::getBoxNotice(mBoxNotice,
					_mXmlreader->getNodeTextByTagName(TEXT_NOTICE).c_str(),
					__String::createWithFormat(_mXmlreader->getNodeTextByTagName("txt_acc_not_exists").c_str(), listName.c_str())->getCString());
				mBoxNotice->setVisible(true);
			}
			else
				boxMail->resetEditText();
		}
		else if (mErrorCode_MailScene == TEXT_SUCCESS_GETINFO_DELETE_MAIL){
			boxMail->setVisible(false);
			boxMail->setPosition(Vec2(CLIENT_WIDTH, 0));
			if (__TAB == 0)
				chooseTab(0, 0);
			else if (__TAB == 1)
				chooseTab(1, 0);
		}
		else{
			if (mErrorCode_MailScene == "txt_mail_load_mail_empty"){
				mTableView->reloadData();
				if (__TAB == 0){
					if (_nCurrentInboxPage == 0) return;
				}
				else if (__TAB == 1){
					if (_nCurrentOutboxPage == 0) return;
				}
				else if (__TAB == 2) return;
				BoxNoticeManager::getBoxNotice(mBoxNotice,
					_mXmlreader->getNodeTextByTagName(TEXT_NOTICE).c_str(),
					_mXmlreader->getNodeTextByTagName(mErrorCode_MailScene.c_str()).c_str());
				mBoxNotice->setVisible(true);
			}
			else{
				BoxNoticeManager::getBoxNotice(mBoxNotice,
					_mXmlreader->getNodeTextByTagName(TEXT_NOTICE).c_str(),
					_mXmlreader->getNodeTextByTagName(mErrorCode_MailScene.c_str()).c_str());
				mBoxNotice->setVisible(true);
			}
		}
	}
	mErrorCode_MailScene = "";

}

void MailScene::showNotice(const std::string& notice)
{
	mErrorCode_MailScene = notice;
}

void MailScene::loadMail(uint8_t nType, uint8_t nPage)
{
	mLoading->show();
	MailInfo* mailinfo = new MailInfo();
	mailinfo->nType = nType;
	if (nType == 0)
		mailinfo->nPage = _nCurrentInboxPage;
	else
		mailinfo->nPage = _nCurrentOutboxPage;
	 pthread_LoadMail( mailinfo);
}