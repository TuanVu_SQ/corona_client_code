#ifndef __FIRSTSCENE_H_INCLUDED__
#define __FIRSTSCENE_H_INCLUDED__

#include "cocos2d.h"
#include "EGScene.h"

USING_NS_CC;

class FirstScene : public EGScene {
public:
	static Scene* scene();

	void onBackPressed();
private:
	Sprite* mBackground;

	LayerColor* mDarkBg;
	Sprite* mLoadBar;
	ProgressTimer* mLoad;
	Sprite* mLoadObject;

	void initScene();
	void loadData();
	void loadMusic();
	void loadDataDone();
};

#endif