#ifndef __HOMESCENEOFF_H_INCLUDED__
#define __HOMESCENEOFF_H_INCLUDED__
#include "cocos2d.h"
#include "EGButtonSprite.h"
#include "EGScene.h"
using namespace cocos2d;

class HomeSceneOff : public EGScene {
public:
	static Scene* scene();
	virtual bool init();
	void turnMusic();
	void updateGame(float pSec);
	void showButton();
	void gotoGame(int pType);
	void onBackPressed();
	bool isExitGame();

private:
	void updateBubble(float dt);
private:
	EGSprite* buttonMusic, *buttonPlay, *buttonHistory, *buttonShop, *buttonGameOnline;
	Sprite * mKhobau,*mSand,*mLogo;
	float mSpeed;
	std::vector<Node*> mArrayButton;
	Sprite* mBackGround;
};

#endif