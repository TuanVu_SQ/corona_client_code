#ifndef __GAMESCENE_H_INCLUDED__
#define __GAMESCENE_H_INCLUDED__
#include "cocos2d.h"
#include "GameInfo.h"
#include "Gun.h"
//#include "ChooseRoomScene.h"
#include "Loading.h"
#include "BoxResize.h"
#include "Fish.h"
#include "pthread_mobi.h"
#include "BoxNotice.h"
#include "EGScene.h"
#include "GunInfo.h"
#include "TaskGame.h"
#include "BoxTopUp.h"
#include "BoxTopUpCard.h"
#include "BoxChat.h"
#include "BoxInfoGame.h"
#include "ClientPlayerInfo.h"
#include "EGNumberSprite.h"
#include "mpmessagerequest.h"


#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "network/HttpClient.h"
#else
#include "HttpClient.h"
#endif
#include "RippleSprite.h"

//#include "ChooseRoomScene.h"

using namespace cocos2d;
using namespace cocos2d::network;

class GameScene : public EGScene, public EditBoxDelegate {
public:
	uint32_t mFireMoney;
	void onBackPressed();
	void reloadMoneyOff();
	virtual void onSMSResultCallback(int pType, int pMoney)
	{
		if (getInfoMyselft()){
			if (pType == 1){
				getInfoMyselft()->balance += pMoney;
				UserDefault::getInstance()->setStringForKey(KEY_MONEY, __String::createWithFormat("%d", getInfoMyselft()->balance)->getCString());
			}
			else{
				mCountSo += pMoney;
				UserDefault::getInstance()->setStringForKey(KEY_SO, __String::createWithFormat("%d", getInfoMyselft()->balance)->getCString());
			}
			updateMoneyShop();
		}
	};
	static Scene *createScene(void *pInfoRoom);
	static Scene *createSceneWithShop(void *pInfoRoom);
	static Scene *createSceneWithDiary(void *pInfoRoom);
	void initScene(void *pInfoRoom);
	bool checkInRoom(std::string pUserName);
	void choosePosition(int index);
	void fireBullet(std::string pUserName,int pTypeBullet, int pTypeGun, Vec2 pPositionFire, bool isVisible);
	Sprite* createBullet(int pTypeBullet,int pTypeGun, Vec2 pPositionStart, Vec2 pPositionFire, std::string pUserName);
	void checkBullet(Sprite* pBullet);
	Sprite* obtainBullet(int pTypeBullet,int pTypeGun);
	void refresh();
	void updateSetUp();
	void showBoxOption();
	void shootLocation(Vec2 pPointFire);
	void addPLayer(ClientPlayerInfoEX pPlayer);
	void createFish(ClientFishInfo *pInfo);
	Fish* obtainFish(int pType);
	void doItem(std::string pUserName, int index, std::vector<uint8_t> pListFish, Vec2 pPosition);
	void doItemRequest(int index, std::vector<uint8_t> pListFish, Vec2 pPosition);
	void chooseItem(int index);
	void updateGame(float pSec);
	void setFireMoney(uint32_t pFireMoney);
	bool checkContain(PointArray* pArrayPoint, Point pPointCheck);
	bool checkRectCollide(Rect _rect1, Rect _rect2, float pRotation1, float pRotation2,bool iRotate);
	bool checkRectCollide(Rect _rect1, Rect _rect2, float pRotation1, float pRotation2)
	{
		return checkRectCollide(_rect1, _rect2, pRotation1, pRotation2, false);
	};
	void setRemainTime(float remainTime);
	void leftGame();
	void showTopUpSo();
	void outGame();
	void clearData();
	void reStart();
	void addComponentDefault(MpMessageRequest* pRequest);
	void readyStartGame();
	void resortScore();
	void createBoxResult(std::vector<GamePlayerResult> pRankList, int pType, int pResult);
	ClientPlayerInfoEX* getInfoMyselft();
	void createBoxCreate();
	void startGame();
	void changeTypeRoom(int pType,int pBetMoney);
	void showBoxCreateRoom();
	void closeBoxCreateRoom();
	void showBoxChooseType();
	void showBoxBetMoney();
	void closeBoxBetMoney();
	void closeBoxChooseType();
	//void chooseBetMoney(int pIndex);
	//void chooseTypeRoom(int pIndex, __String* pText);
	void chooseTypeRoom(int pIndex);
	void setUpGame();
	void updateForOffLineGame(float pSec);
	void playSound(const char* pSoundEffect);
	void chooseTabShop(int index);
	void chooseItemShop(int index);
	void updateMoneyShop();
	void showBoxShop();
	void closeBoxShop();
	void refreshItemSp();
	virtual void pauseGame();
	virtual void reumeGame(){};
	void updateMoneyOff(float pSec);
	void detachTrap();
	void upgradeGun(int pTypeUpgrade);
	Label* addTextforButton(Sprite* pButton, std::string pString);
	void gift(Vec2 pSartPoint, Vec2 pEndPoint, float pScore, bool pHide);
	ClientPlayerInfoEX* getPlayerByName(std::string pName);
	bool checkTaskGame(int id, int pType, int quantity);
	void showBoxTopUp();
	void startEventMaiden();
	void checkResult(float pRotation);
	void showResultMaiden(int pIndex);
	void growMoney(int pMoney);
	void showBoxMaiDen();
	void closeBoxMaiDen();
	void startEventShark();
	void mixTreasure();
	void showBoxShark();
	void closeBoxShark();
	void startChooseTreasure();
	void chooseTreasure(EGSprite* button);
	void updateTaskBox();
	void showBoxQuest();
	void closeBoxQuest();
	void completeQuest();
	void showQuaMan();
	void checkChangeMap();
	void changeMap();
	void pauseGameOff();
	void resumeGameOff();
	void pauseAllGame();
	void resumeAllGame();
	void bonusMap(int pType);
	void laser(float pRotation);
	void checkLaser(Node *pNode);
	bool checkOutLine(Point pPointStart, Point pPointEnd, Point pPointCheck);
	void addFishDead(int pIndex);
	bool checkExistFish(int pType);
	void gift10minutes(uint16_t pTime, uint16_t pMoney);
	void gift10minutesRequest();
	void pauseSprite(Node* pNode);
	void resumeSprite(Node* pNode);
	void giftOff(Ref* pObject);
	void touchGun(int index);
	void addFriend(std::string pUserName);
	void kickPlayer(std::string pUserName);
	void onSendText(std::string pContent);
	void chooseBetMoney(int index);
	void onSendEmotion(std::string pEmotionType, int pEmotionID);
	void bonusResult(std::vector<GamePlayerResult> pRankPlayer);
	void updateOutGame(float pSec);
	void showBoxInvite();
	void detachLaser();
	void showBoxTopUpFirstTime();
	void openBoxX(BoxResize *pBox);
	void chooseXMoney(int index);
	void sendXFactor(int index);
	void earthQuake(int rate,int pAngle);
	void explore(Vec2 pPosition, int pType);
	void showBonusSpecial(int index, int pScore);
	bool endsWith(std::string str, std::string suffix);
protected:
	BoxNotice* mBoxNotice;
private:
	Node* mLaser;
	int mTypeRechargeWP;
	
	bool isOpenPopup,isDirty,isFirsTimeSetUp;
	BoxTopUp* mBoxTopUp;
	BoxTopUpCard* mBoxTopUpCard;
	BoxInfoGame *mBoxInfoGame;
	BoxResize* mBoxXMoney[4];
	void rechargeBySMS(int pIndex);
	void showTopUpByCard();
	void recharge(std::string pSerial, std::string pNumber, std::string pTelco, std::string pUsername);
	void checkReadyCreateRoom();
	void detachLayerResult(LayerColor* layer);
	void createBoxTeam(Sprite* pBox,bool pFlip);
	BoxChat* boxChat;
	std::vector<Vec2*>* mListFishDead;
	LayerColor* mBoxShop, *mBoxEvenMaiDen, *mBoxEvenShark, *mBoxQuest;
	Node* mBoxCreateRoom;
	EGSprite* mTreasure;
	Label* mTimeTreasure;
	BoxResize* mBoxChoosetype, *mBoxChooseBet;
	Sprite* mRangeItem;
	Sprite* mBonusSpecialFish[4];
	Layer* mMainLayer;
	//BoxInvite *mBoxInvite;
	Vector<GunPlayer*> mListGun;
	Vector<Sprite*> mListBullet;
	Vector<Fish*> mListFish;
	std::vector<Vec3>* mListInfoGun;
	EGButtonSprite* mButtonStartReady;
	Sprite* mBoxteam1, *mBoxteam2,*mLoadDingWaitFish;
	EGSprite* mButtonTopup;
	TaskGame* mTaskGame;
	Sprite* mElectric;
	//std::vector<MpMessageResponse*> *mListRepose;
	ProgressTimer* mBackGround;
	RippleSprite* mBackGroudRiple;
	Label*  mLabelMoneyOff,*mLabelCountReCoin,*mLabelSoOff;
	int mCountShoot,mCountChange;
	void* mInfoRoom;
	Loading * mMainLoading;
	bool isStartGame, isChangeMap, isBonus,isChangedsetUp,iGift10Minute,iTouchGun;
	Vec2* mPointTrap;
	ProgressTimer* barPower;
	float mTime,mCountReady,mDelaySmallFishOff,mDelayOutGame,mDelayEarthQuake;
	EGNumberSprite* numStage;
	ProgressTimer* barStage;
	LayerColor* layerResultUp;
	float mDelayShoot, mDelayOffline, mDelayPing, mDeLayUpdateMoneyOff, mDelayRecoin, mLengtLaser, mCountDownGift,mDelayPingBalance,mDelayConnect,mDelayChangeMap;
	Label *mTimeLabel;
	int  mChooseType, mCurrentPower, mChooseBetMoney, pScoreTeam1, pScoreTeam2, mMoneyTeam1, mMoneyTeam2, mIndexTabShop, mCountSo, mIDFish, mItemChoosed, mLevel, mCurrentTask, mCurrentExp, mLimitExp, mCountRecoin, isFirstTime;
	virtual bool ccTouchBegans(Touch* touch, Event* event);
	virtual void ccTouchMove(Touch* touch, Event* event);
	virtual void ccTouchUp(Touch* touch, Event* event);
	void onHttpRequestCompleted(HttpClient *sender, HttpResponse *response);
	virtual void editBoxReturn(EditBox* editBox);
	virtual void editBoxEditingDidBegin(EditBox* editBox);
	EGButtonSprite* buttonOption, *buttonInvite;

private:
	void showNotice(const std::string& content);
	void showLoading(bool bLoading);

private:
	void getSmsInfo();
	void getVisaInfo();
	void getRechargeRate();
	void recharge_clone(std::string pSerial, std::string pNumber, std::string pTelco);
};

#endif