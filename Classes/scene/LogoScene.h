#ifndef __LOGOSCENE_H_INCLUDED__
#define __LOGOSCENE_H_INCLUDED__

#include "cocos2d.h"
#include "EGScene.h"

USING_NS_CC;

class LogoScene : public EGScene {
public:
	static Scene* scene();
private:
	Sprite* mBackground;

	void initScene();
};

#endif