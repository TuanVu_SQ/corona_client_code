#ifndef __BONUSSCENE_H_INCLUDED__
#define __BONUSSCENE_H_INCLUDED__

#include "cocos2d.h"
#include "EGScene.h"
#include "EGButtonSprite.h"
#include "BoxNotice.h"

USING_NS_CC;

class BonusScene :public EGScene {
public:
	static BonusScene* scene();

	void onBackPressed();
private:
	Sprite* mBackground, *mTitle;
	EGButtonSprite** mButton;
	EGButtonSprite* mBtnBack;
	BoxNotice* mBoxNotice;

	void initScene();
	void excuteBonus(int pIndex);
};

#endif