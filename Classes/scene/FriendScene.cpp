#include "FriendScene.h"
#include "GameInfo.h"
#include "BoxNoticeManager.h"
#include "MainScene.h"
#include "pthread_mobi.h"

#include "EGJniHelper.h"
#include "ChooseRoomScene.h"
#include "HomeScene.h"
#include "mpgetinfoplayergameresponse.h"
#include "MpRegisterRequest.h"


std::string mErrorCode_FriendScene;
std::vector<std::vector<std::string>*>* mCacheFriend; // Friend cache
ClientPlayerInfoEX* mPlayerInfoFriend;
Scene* FriendScene::scene()
{
	FriendScene* pScene = new FriendScene();
	pScene->initScene();
	pScene->registerBackPressed();
	pScene->autorelease();
	return pScene;
}

FriendScene::~FriendScene() {
	mErrorCode_FriendScene = "";
	for (int i = mLstFriendObj.size() - 1; i >= 0; i--) {
		ObjFriend* pObjFriend = mLstFriendObj.at(i);
		mLstFriendObj.erase(i);
		pObjFriend->removeAllChildrenWithCleanup(true);
		pObjFriend->removeFromParentAndCleanup(true);
	}
	if (mCacheFriend) {
		for (int i = mCacheFriend->size() - 1; i >= 0; i--) {
			std::vector<std::string>* pLstFriend = mCacheFriend->at(i);
			if (pLstFriend) {
				pLstFriend->clear();
			}
		}
		mCacheFriend->clear();
	}
}

void* pthread_loadFriend(void* _void) {
	MpMessage* pRequest = new MpMessage(MP_MSG_GET_FRIEND);
	pRequest->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID.c_str());

	MpClientManager::getInstance()->sendMessage(pRequest, true);

	pthread_exit(NULL);
	return NULL;
}

void FriendScene::initScene() {
	GameInfo::mUserInfo.stateScene = 0;
	mErrorCode_FriendScene = "";
	mXml = WXmlReader::create();
	mXml->load(KEY_XML_FILE);

	//mBackground = Sprite::createWithSpriteFrameName(IMG_BACKGROUND_FRIEND);
	mBackground = Sprite::create("background_choosetype.png");
	mBackground->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBackground);

	mBtnAddFriend = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_ADD, EGButton2FrameDark);
	mBtnAddFriend->setOnTouch(CC_CALLBACK_0(FriendScene::callDialogAddFriend, this));
	mBtnAddFriend->setPosition(Vec2(CLIENT_WIDTH - mBtnAddFriend->getContentSize().width / 2 - 10, CLIENT_HEIGHT - mBtnAddFriend->getContentSize().height / 2 - 20));
	mBackground->addChild(mBtnAddFriend);

	mEdt_Search = EditBox::create(Size(491, 60), Scale9Sprite::createWithSpriteFrameName(IMG_EDITTEXT_LONG));
	mEdt_Search->setPosition(Vec2(mBtnAddFriend->getPositionX1() - mEdt_Search->getContentSize().width / 2 + 2, mBtnAddFriend->getPositionY()));
	mEdt_Search->setFontName(FONT);
	mEdt_Search->setFontSize(20);
	//mEdt_Search->setMargins(20, 20);
	mEdt_Search->setInputFlag(EditBox::InputFlag::SENSITIVE);
	mEdt_Search->setInputMode(EditBox::InputMode::SINGLE_LINE);
	mEdt_Search->setFontColor(Color3B::WHITE);
	mEdt_Search->setPlaceHolder(mXml->getNodeTextByTagName(TEXT_FINDFRIEND).c_str());
	mEdt_Search->setPlaceholderFontColor(Color3B::GRAY);
	mEdt_Search->setMaxLength(REQ_MAXLENGHT_USERNAME);
	mEdt_Search->setReturnType(EditBox::KeyboardReturnType::DONE);
	mBackground->addChild(mEdt_Search);
	mEdt_Search->setZOrder(10);

	mBtnSearch = EGButtonSprite::createWithSpriteFrameName(IMG_FIND, EGButton2FrameDark);
	mBtnSearch->setOnTouch(CC_CALLBACK_0(FriendScene::searchFriend, this));
	mBtnSearch->setPosition(Vec2(mEdt_Search->getPositionX() + mEdt_Search->getContentSize().width / 2 - mBtnSearch->getContentSize().width / 2 - 10, mEdt_Search->getPositionY()));
	mBackground->addChild(mBtnSearch);
	mBtnSearch->setZOrder(10);

	mTitle = Sprite::createWithSpriteFrameName(IMG_TITLE_FRIEND);
	mTitle->setPosition(Vec2(mEdt_Search->getPositionX() - 491 / 2 - mTitle->getContentSize().width / 2 - 2, mEdt_Search->getPositionY()));
	mBackground->addChild(mTitle);

	mBtnBack = EGButtonSprite::createWithFile("button-back-cr (1).png", EGButton2FrameDark);
	mBtnBack->setOnTouch(CC_CALLBACK_0(FriendScene::onBackPressed, this));
	mBtnBack->setPosition(Vec2(10 + mBtnBack->getContentSize().width / 2, mTitle->getPositionY()));
	mBackground->addChild(mBtnBack);
	mBtnBack->setZOrder(10);

	mBoxInfo = BoxInfo::create(false);
	mBoxInfo->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	mBackground->addChild(mBoxInfo, 1);
	mBoxInfo->setZOrder(10);

	mBoxNotice = BoxNotice::create();
	mBoxNotice->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	mBackground->addChild(mBoxNotice, 1);
	mBoxNotice->setZOrder(10);

	mLoading = Loading::create();
	mLoading->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	mBackground->addChild(mLoading, 1);


	pthread_t t1;
	mCurrentPage = 1;

	mLoading->show();
	pthread_create(&t1, NULL, &pthread_loadFriend, NULL);

	mBoxInfoGame = BoxInfoGame::createBoxInfo();
	this->addChild(mBoxInfoGame);
	mBoxInfoGame->closeBox();
	mBoxInfoGame->setType(2);
	mBoxInfo->setZOrder(10);

	this->scheduleUpdate();
}

void FriendScene::onBackPressed() {
	if (mLoading->isVisible()) {
		return;
	}
	else if (mBoxInfo->isVisible()) {
		mBoxInfo->setVisible(false);
	}
	else if (mBoxNotice->isVisible()) {
		mBoxNotice->setVisible(false);
	}
	else {
		Scene* pScene = MainScene::scene(0);
		TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
		Director::getInstance()->replaceScene(pTrans);
	}
}

void FriendScene::callDialogAddFriend() {
	BoxNoticeManager::getBoxInput(mBoxNotice, mXml->getNodeTextByTagName(TEXT_ADDFRIEND_TITLE).c_str(), mXml->getNodeTextByTagName(TEXT_ADDFRIEND_CONTENT).c_str(),
		mXml->getNodeTextByTagName(TEXT_ADDFRIEND_HOLDER).c_str(), "", mXml->getNodeTextByTagName(TEXT_SEND).c_str(), CC_CALLBACK_0(FriendScene::addFriend, this));

	mBoxNotice->setVisible(true);
}

void* pthread_addFriend(void* _void) {
	MpMessage *pRequest = new MpMessage(MP_MSG_ADD_FRIEND);
	std::string pStrFriendName = ((__String*)_void)->getCString();
	pRequest->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
	pRequest->addString(MP_PARAM_USERNAME, pStrFriendName);
	MpClientManager::getInstance()->sendMessage(pRequest);


	pthread_exit(NULL);
	return NULL;
}

void FriendScene::addFriend() {
	std::string pFriendName = mBoxNotice->mEdt_Input->getText();
	if (pFriendName.length() < 6){
		mErrorCode_FriendScene = __String::createWithFormat("%d", 166257)->getCString();
		return;
	}
		
	pthread_t t1;
	pthread_create(&t1, NULL, &pthread_addFriend, __String::create(pFriendName));

	mBoxNotice->setVisible(false);
}

void FriendScene::searchFriend() {
	if (mBtnSearch->getFileName() == IMG_FIND) {
		std::string pUsername = mEdt_Search->getText();
		bool isErr = false;

		if (pUsername.length() > REQ_MAXLENGHT_USERNAME) {
			isErr = true;
		}
		//else if (pUsername.find_first_not_of("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890") != std::string::npos) {
		else if (pUsername.find_first_not_of("0123456789") != std::string::npos) {
			isErr = true;
		}

		if (isErr) {
			BoxNoticeManager::getBoxNotice(mBoxNotice, mXml->getNodeTextByTagName(TEXT_NOTICE).c_str(), mXml->getNodeTextByTagName(TEXT_ERR_USERNAME_INVALID).c_str());
			mBoxNotice->setVisible(true);
		}
		else {
			for (int i = 0; i < pUsername.length(); i++) {
				pUsername[i] = std::tolower(pUsername[i]);
			}
			for (int i = mCacheFriend->size() - 1; i >= 0; i--) {
				std::string pName = mCacheFriend->at(i)->at(1);
				for (int j = 0; j < pName.length(); j++) {
					pName[j] = std::tolower(pName[j]);
				}
				if (pName.find(pUsername)) {
					mCacheFriend->erase(mCacheFriend->begin() + i);
				}
			}

			loadFriend(1);
			mBtnSearch->setTexture(IMG_X);
		}
	}
	else {
		pthread_t t1;
		mCurrentPage = 1;

		mLoading->show();
		pthread_create(&t1, NULL, &pthread_loadFriend, NULL);

		mBtnSearch->setTexture(IMG_FIND);
		mEdt_Search->setText("");
	}
}

void FriendScene::loadFriend(int pPage) {
	mCurrentPage = pPage;
	mErrorCode_FriendScene = TEXT_SUCCESS_CHOOSEPAGE_FRIEND;
}

void FriendScene::update(float dt) {
	MpMessage* pMsg = MpManager::getInstance()->getMessenger();
	if (pMsg) {
		switch (pMsg->getType())
		{
		case MP_MSG_GET_ACC_INFO_ACK:
		{
			MpGetInfoPlayerGameResponse *pResponse = (MpGetInfoPlayerGameResponse*)pMsg;

			unsigned pErrorCode = pResponse->getErrorCode();
			if (pErrorCode != 0) 
			{
				mErrorCode_FriendScene = __String::createWithFormat("%d", pErrorCode)->getCString();
			}
			else 
			{
				pResponse->getPlayerInfo(*mPlayerInfoFriend);
				mErrorCode_FriendScene = TEXT_GETINFO_SUCCES;
			}
		}break;
		case MP_MSG_ADD_FRIEND_ACK:
		{
			MpMessage* pResponse = pMsg;
			unsigned pErrorCode = pResponse->getInt(MP_PARAM_ERROR_CODE);
			
			if (pErrorCode != 0) 
			{
				mErrorCode_FriendScene = __String::createWithFormat("%d", pErrorCode)->getCString();
				//mErrorCode_FriendScene = "ket ban khong thanh cong";
			}
			else 
			{
				mErrorCode_FriendScene = TEXT_SUCCESS_ADDFRIEND;
			}
		}
		break;
		case MP_MSG_GET_FRIEND_ACK:
		{
			std::vector<std::vector<std::string>>* pLstFriend = NULL;
			MpMessage *pResponse = pMsg;
			mCacheFriend = new std::vector<std::vector<std::string>*>();
			std::vector<std::vector<std::string>> pLstFriendReceived = MpClientManager::getVectorInVectorMsgByTag(pResponse, MP_LISTFRIEND);

			log("reveiced %d friends", pLstFriendReceived.size());
			for (int i = 0; i < pLstFriendReceived.size(); i++) {
				std::vector<std::string>* pFriend = new std::vector<std::string>();
				std::vector<std::string> pFriendReceived = pLstFriendReceived.at(i);
				pFriend->push_back(pFriendReceived.at(0));
				pFriend->push_back(pFriendReceived.at(1));
				pFriend->push_back(pFriendReceived.at(2));
				pFriend->push_back(pFriendReceived.at(3));
				pFriend->push_back(pFriendReceived.at(4));
				pFriend->push_back(pFriendReceived.at(5));
				mCacheFriend->push_back(pFriend);
			}

			mErrorCode_FriendScene = TEXT_SUCCESS_LOADFRIEND;
		}
		break;
		default:
			break;
		}
		delete pMsg;
		pMsg = NULL;
	}
	if (getErrorCode() != "") {
		mErrorCode_FriendScene = getErrorCode();
		setErrorCode("");
	}
	if (mErrorCode_FriendScene != "") {
		mLoading->hide();
		if (mErrorCode_FriendScene == TEXT_SUCCESS_LOADFRIEND) {
			loadFriend(1);
		}
		else if (mErrorCode_FriendScene == TEXT_GETINFO_SUCCES) {
			mBoxInfoGame->showBox(mPlayerInfoFriend, mPlayerInfoFriend);
		}
		else if (mErrorCode_FriendScene == TEXT_ERR_NOCONNECTION) {
			WXmlReader* pXml = WXmlReader::create();
			pXml->load(KEY_XML_FILE);
			BoxNoticeManager::getBoxNotice(mBoxNotice,
				pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				pXml->getNodeTextByTagName(mErrorCode_FriendScene).c_str(),
				[=] {
				Scene* _scene = HomeScene::scene();
				Director::getInstance()->replaceScene(_scene);
			});
			mBoxNotice->setVisible(true);
		}
		else if (mErrorCode_FriendScene != TEXT_SUCCESS_CHOOSEPAGE_FRIEND) {
			WXmlReader* pXml = WXmlReader::create();
			pXml->load(KEY_XML_FILE);
			BoxNoticeManager::getBoxNotice(mBoxNotice,
				pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				pXml->getNodeTextByTagName(mErrorCode_FriendScene).c_str());
			mBoxNotice->setVisible(true);
		}

		if (mErrorCode_FriendScene == TEXT_SUCCESS_CHOOSEPAGE_FRIEND) {
			int pTotalPage = mCacheFriend->size() / 4;
			if (mCacheFriend->size() % 4 != 0) {
				pTotalPage++;
			}
			if (mCurrentPage > pTotalPage) {
				mErrorCode_FriendScene = "";
				return;
			}

			// clear old data
			for (int i = mLstFriendObj.size() - 1; i >= 0; i--) {
				ObjFriend* pObjFriend = mLstFriendObj.at(i);
				mLstFriendObj.erase(i);
				pObjFriend->removeAllChildrenWithCleanup(true);
				pObjFriend->removeFromParentAndCleanup(true);
			}
			//for (; mCurrentPage <= pTotalPage; mCurrentPage++){
			for (int i = (mCurrentPage - 1) * 4; i < (mCurrentPage - 1) * 4 + 4; i++) {
				if (i >= mCacheFriend->size()) {
					break;
				}
				std::string pStatusFriend = mCacheFriend->at(i)->at(5);
				std::string pName = mCacheFriend->at(i)->at(1);
				unsigned int pAvatarID = atoi(mCacheFriend->at(i)->at(0).c_str());
				ObjFriend* pObjFriend;
				if (pStatusFriend == "0") {
					pObjFriend = ObjFriend::create(pAvatarID, pName, CC_CALLBACK_0(FriendScene::acceptFriend, this, pName),
						CC_CALLBACK_0(FriendScene::rejectFriend, this, pName), CC_CALLBACK_0(FriendScene::showInfo, this, pName));
				}
				else {
					unsigned long pBalance = atol(mCacheFriend->at(i)->at(2).c_str());
					unsigned int pLevel = atoi(mCacheFriend->at(i)->at(3).c_str());
					pObjFriend = ObjFriend::create(pAvatarID, pName, pBalance, pLevel, atoi(mCacheFriend->at(i)->at(4).c_str()), CC_CALLBACK_0(FriendScene::showInfo, this, pName));
					//pObjFriend->setOnUp(CC_CALLBACK_0(FriendScene::showInfo, this,pName));
					//pObjFriend->registerTouch(true);
					pObjFriend->setOnTouch(CC_CALLBACK_0(FriendScene::showInfo, this, pName));
					pObjFriend->setButtonType(EGButtonScrollViewItem);
				}

				pObjFriend->setPosition(Vec2(mBackground->getContentSize().width / 2, 350 - (pObjFriend->getContentSize().height + 5)*(i - (mCurrentPage - 1) * 4)));
				mBackground->addChild(pObjFriend);
				mLstFriendObj.pushBack(pObjFriend);
			}
			//}

			for (int i = mLstButtonPage.size() - 1; i >= 0; i--) {
				Sprite* pButton = mLstButtonPage.at(i);
				pButton->removeFromParentAndCleanup(true);
			}
			mLstButtonPage.clear();

			if (mCurrentPage != 0) {
				EGButtonSprite* pBtnFirstPage = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_PAGE_TEXT);
				std::string pStrPage = __String::createWithFormat("Paj %d/%d", mCurrentPage, pTotalPage)->getCString();
				pBtnFirstPage->setText(pStrPage.c_str(), FONT, 15);
				pBtnFirstPage->setPosition(Vec2((mBackground->getContentSize().width - 716) / 2 + pBtnFirstPage->getContentSize().width / 2, 60));
				mBackground->addChild(pBtnFirstPage);
				mLstButtonPage.pushBack(pBtnFirstPage);

				float pStartX = pBtnFirstPage->getPositionX2();

				int pMaxCounterNumberRight = mCurrentPage + 3;
				if (mCurrentPage < 4) {
					pMaxCounterNumberRight += 4 - mCurrentPage;
				}
				for (int i = mCurrentPage - 3; i <= pMaxCounterNumberRight && i <= pTotalPage; i++) {
					if (i > 0) {
						EGButtonSprite* pBtnPage;
						if (i == mCurrentPage) {
							pBtnPage = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_PAGE_CHOOSING);
							pBtnPage->unregisterTouch();
						}
						else {
							pBtnPage = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_PAGE_NUMBER, EGButton2FrameDark);
						}
						if (i == mCurrentPage - 3 || i == pMaxCounterNumberRight) {
							if (i == 1 || i == pTotalPage) {
								pBtnPage->setText(__String::createWithFormat("%d", i)->getCString(), FONT, 15);
							}
							else {
								pBtnPage->setText("...", FONT, 15);
								pBtnPage->unregisterTouch();
							}
						}
						else {
							pBtnPage->setText(__String::createWithFormat("%d", i)->getCString(), FONT, 15);
						}
						pBtnPage->setOnTouch(CC_CALLBACK_0(FriendScene::loadFriend, this, i));
						pBtnPage->setPosition(Vec2(pStartX + pBtnPage->getContentSize().width / 2 + 10, pBtnFirstPage->getPositionY()));
						pStartX = pBtnPage->getPositionX2();
						mBackground->addChild(pBtnPage);
						mLstButtonPage.pushBack(pBtnPage);
					}
				}
			}
		}
		mErrorCode_FriendScene = "";
	}
}

void FriendScene::showInfo(std::string pUsername) {
	mBoxInfoGame->showBox(NULL, NULL);
	mPlayerInfoFriend = new ClientPlayerInfoEX();
	mPlayerInfoFriend->username = pUsername;
	mLoading->show();

	MpMessageRequest * request = new MpMessageRequest(MP_MSG_GET_ACC_INFO);
	request->setTokenId(GameInfo::mUserInfo.mTokenID);
	std::string pUserName = mPlayerInfoFriend->username;
	for (int i = 0; i < pUserName.length(); i++) {
		pUserName[i] = std::tolower(pUserName[i]);
	}
	request->addString(MP_PARAM_USERNAME, pUserName);
	MpClientManager::getInstance()->sendMessage(request, true);

}

void FriendScene::acceptFriend(std::string pUsername) {
	MpMessageRequest *pReq = new MpMessageRequest(MP_MSG_ANSWER_FRIEND);
	pReq->addInt(MP_ANSWER, MP_ACCEPT);
	pReq->addString(MP_PARAM_USERNAME, pUsername);
	pReq->setTokenId(GameInfo::mUserInfo.mTokenID);
	MpClientManager::getInstance()->sendMessage(pReq);

	// remove this user from list
	std::vector<std::string>* pFriend = NULL;
	for (int i = 0; i < mCacheFriend->size(); i++) {
		if (mCacheFriend->at(i)->at(1) == pUsername) {
			pFriend = mCacheFriend->at(i);
			pFriend->at(5) = "1";
			mCacheFriend->erase(i + mCacheFriend->begin());
			break;
		}
	}

	if (pFriend) {
		for (int i = 0; i < mCacheFriend->size(); i++) {
			if (mCacheFriend->at(i)->at(5) == "1") {
				mCacheFriend->insert(i + mCacheFriend->begin(), pFriend);
				pFriend = NULL;
				break;
			}
		}
	}
	if (pFriend) {
		mCacheFriend->push_back(pFriend);
		pFriend = NULL;
	}

	// count again totalpage
	int pTotalPage = mCacheFriend->size() / 4;
	if (mCacheFriend->size() % 4 != 0) {
		pTotalPage++;
	}
	// check if currentpage > totlapage -> load currentpage -1 ( if currentpage 1 >= 0 )
	if (mCurrentPage > pTotalPage) {
		mCurrentPage--;
		if (mCurrentPage < 0) {
			mCurrentPage = 0;
		}
	}

	loadFriend(mCurrentPage);
}

void FriendScene::rejectFriend(std::string pUsername)
{
	MpMessageRequest *pReq = new MpMessageRequest(MP_MSG_ANSWER_FRIEND);
	pReq->addInt(MP_ANSWER, MP_DENIED);
	pReq->addString(MP_PARAM_USERNAME, pUsername);
	pReq->setTokenId(GameInfo::mUserInfo.mTokenID);
	MpClientManager::getInstance()->sendMessage(pReq);

	// remove this user from list
	for (int i = 0; i < mCacheFriend->size(); i++) {
		std::vector<std::string>* pFriend = mCacheFriend->at(i);
		if (pFriend->at(1) == pUsername) {
			mCacheFriend->erase(mCacheFriend->begin() + i);
			break;
		}
	}
	// count again totalpage
	int pTotalPage = mCacheFriend->size() / 4;
	if (mCacheFriend->size() % 4 != 0) {
		pTotalPage++;
	}
	// check if currentpage > totlapage -> load currentpage -1 ( if currentpage 1 >= 0 )
	if (mCurrentPage > pTotalPage) {
		mCurrentPage--;
		if (mCurrentPage < 0) {
			mCurrentPage = 0;
		}
	}

	loadFriend(mCurrentPage);
}