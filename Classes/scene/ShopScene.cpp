#include "ShopScene.h"
#include "GameInfo.h"
#include "GunInfo.h"
#include "MainScene.h"
#include "MessageParam.h"
#include "MessageType.h"
#include "message/mpbuyitemmessageresponse.h"

std::string mErrorCode_ShopScene;
std::vector<ScratchCard> lstReward;

unsigned int BuildErrorCode(unsigned int CI, unsigned int CEC)
{
	unsigned int nCec = CEC;
	unsigned int nCi = 100 + CI;

	if (CEC > 1000000)
		return 1000;            //UNKNOWN ERROR CODE

	while (nCec)
	{
		nCec = nCec / 10;
		nCi = nCi * 10;
	}
	return nCi + CEC;
}

void pthread_loadReward(void* _void) {
	MpMessage *message = new MpMessage((uint32_t)MP_MSG_GET_XCHANGE_SC_CFG);
	message->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
	//message->addString(MP_PARAM_TELCO_ID)
	//client_loadReward->sendMsg(message.getMsg());
	MpClientManager::getInstance()->sendMessage(message, true);
}

void pthread_takeReward(void* _void) {
	ScratchCard* item = (ScratchCard*)_void;

	MpMessage *message = new MpMessage((uint32_t)MP_MSG_XCHANGE_SCRATCH_CARD);
	message->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
	message->addInt(MP_PARAM_TELCO_ID, item->telcoId);
	message->addInt(MP_PARAM_VALUE, item->value);
	//client_takeReward->sendMsg(message.getMsg());
	MpClientManager::getInstance()->sendMessage(message, true);


	delete item;
}

Scene* ShopScene::scene() {
	ShopScene* pScene = new ShopScene();
	pScene->registerBackPressed();
	pScene->initScene();
	pScene->autorelease();
	return pScene;
}

ShopScene::~ShopScene() {
}

void ShopScene::initScene() {
	GameInfo::mUserInfo.stateScene = 0;
	pXml = WXmlReader::create();
	pXml->load(KEY_XML_FILE);

	mBackground = EGSprite::createWithFile("background_choosetype.png");
	mBackground->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBackground);

	auto mTitleBg = EGSprite::createWithFile("kv_info_bg.png");
	mTitleBg->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT - mTitleBg->getContentSize().height/2));
	this->addChild(mTitleBg);
	mTitleBg->setScale(0.7f);

	auto mLblTitle = Label::createWithBMFont(FONT_BMF_NAME, "BOUTIK");
	mLblTitle->setScale(1.2f);
	mLblTitle->setPosition(mTitleBg->getContentSize()/2);
	mTitleBg->addChild(mLblTitle);

	mbackgroundTab = EGSprite::createWithFile("background_shopavatar_tab.png");
	mbackgroundTab->setPosition(Vec2(CLIENT_WIDTH / 2, mbackgroundTab->getContentSize().height / 2 + 5));
	this->addChild(mbackgroundTab);

	mBtnBack = EGButtonSprite::createWithFile("button-back-cr (1).png", EGButton2FrameDark);
	mBtnBack->registerTouch(true);
	mBtnBack->setOnTouch(CC_CALLBACK_0(ShopScene::onBackPressed, this));
	mBtnBack->setPosition(Vec2(7 + mBtnBack->getContentSize().width / 2, CLIENT_HEIGHT - mBtnBack->getContentSize().height / 2 - 10));
	mbackgroundTab->addChild(mBtnBack, 20);

	mBtnAvatar = EGButtonSprite::createWithFile("btn_avatar_shop.png", EGButton2FrameDark);
	mBtnAvatar->registerTouch(true);
	mBtnAvatar->setOnTouch(CC_CALLBACK_0(ShopScene::btnAvatarClicked, this));
	mBtnAvatar->setPosition(Vec2(CLIENT_WIDTH - mBtnAvatar->getContentSize().width / 2 - 25, 375));
	mbackgroundTab->addChild(mBtnAvatar, 2);

	/*mBtnReward = EGButtonSprite::createWithFile("btn_reward_shop.png", EGButton2FrameDark);
	mBtnReward->registerTouch(true);
	mBtnReward->setOnTouch(CC_CALLBACK_0(ShopScene::btnRewardClicked, this));
	mBtnReward->setPosition(Vec2(mBtnAvatar->getPositionX() - mBtnReward->getContentSize().width - 44, mBtnAvatar->getPositionY()));
	mbackgroundTab->addChild(mBtnReward, 2);*/

	mSpriteAvatar = EGSprite::createWithSpriteFrameName(__String::createWithFormat("avatar (%d).png", GameInfo::mUserInfo.mAvatarID)->getCString());
	mSpriteAvatar->setPosition(Vec2(mSpriteAvatar->getContentSize().width / 2 + 10, 380));
	mBackground->addChild(mSpriteAvatar);

	std::string pMoney = __String::createWithFormat("%ld", GameInfo::mUserInfo.mBalance)->getCString();
	pMoney = EGSupport::addDotMoney(pMoney);
	pMoney += " xu";
	mLblBalance = Label::createWithBMFont(FONT_BMF_MONEY, pMoney);
	mLblBalance->setPosition(Vec2(mSpriteAvatar->getContentSize().width + 20 + mLblBalance->getContentSize().width*mLblBalance->getScaleX() / 2, mSpriteAvatar->getPositionY1() + mLblBalance->getContentSize().height / 2));
	mBackground->addChild(mLblBalance);

	mLblName = Label::createWithBMFont(FONT_BMF_NAME, GameInfo::mUserInfo.mFullname);
	mLblName->setPosition(Vec2(mSpriteAvatar->getContentSize().width + 20 + mLblName->getContentSize().width*mLblName->getScaleX() / 2, mLblBalance->getPositionY() + 30));
	mBackground->addChild(mLblName);

	mLoading = Loading::create();

	mBoxNotice = BoxNotice::create();

	mBoxObjItemDetails = ObjItemDetails::create(true, mLoading, mBoxNotice, CC_CALLBACK_0(ShopScene::reloadDatas, this));
	mBoxObjItemDetails->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBoxObjItemDetails);

	mBoxNotice->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBoxNotice);

	mLoading->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mLoading);

	_nodeReward = Node::create();
	_nodeReward->setPosition(10, 10);
	mbackgroundTab->addChild(_nodeReward);

	auto bg_logoPhone = EGSprite::createWithFile("background_logo_phone_shop.png");
	bg_logoPhone->setPosition(Vec2(20 + bg_logoPhone->getContentSize().width / 2, mbackgroundTab->getContentSize().height / 2 - 30));
	_nodeReward->addChild(bg_logoPhone);

	for (uint8_t i = 0; i < 3; ++i)
	{
		std::string filename = __String::createWithFormat("logo_phone_%d.png", i + 1)->getCString();
		auto logo = EGButtonSprite::createWithFile(filename.c_str(), EGButton2FrameDark);
		logo->setPosition(Vec2(logo->getContentSize().width / 2 + 8, bg_logoPhone->getContentSize().height + 35 - (i + 1)*logo->getContentSize().height*1.05f));
		logo->setOnTouch(CC_CALLBACK_0(ShopScene::btnLogoPhoneClicked, this, i + 1));
		bg_logoPhone->addChild(logo);

		auto radioBg = EGSprite::createWithFile("radio_shop_bg.png");
		radioBg->setPosition(Vec2(logo->getPositionX2() + 25, logo->getPositionY()));
		bg_logoPhone->addChild(radioBg);

		auto radioRoot = EGSprite::createWithFile("radio_shop_root.png");
		i == 0 ? radioRoot->setVisible(true) : radioRoot->setVisible(false);
		radioRoot->setTag(i + 1);
		radioRoot->setPosition(radioBg->getPosition());
		bg_logoPhone->addChild(radioRoot);
		_lstRadioButton.push_back(radioRoot);
	}

	_nodeAvatar = Node::create();
	_nodeAvatar->setPosition(CLIENT_WIDTH, 10);
	mbackgroundTab->addChild(_nodeAvatar);

	mViewAvatar = TableView::create(this, Size(780, 320));
	mViewAvatar->setTag(2);
	mViewAvatar->setPosition(Vec2(10, 0));
	mViewAvatar->setDirection(ScrollView::Direction::VERTICAL);
	mViewAvatar->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	mViewAvatar->setDelegate(this);
	_nodeAvatar->addChild(mViewAvatar);
	//mViewAvatar->reloadData();

	mViewReward = TableView::create(this, Size(550, 317));
	mViewReward->setTag(1);
	mViewReward->setPosition(Vec2(bg_logoPhone->getPositionX2() + 20, 0));
	mViewReward->setDirection(ScrollView::Direction::VERTICAL);
	mViewReward->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	mViewReward->setDelegate(this);
	_nodeReward->addChild(mViewReward);
	//mViewReward->reloadData();

	scheduleUpdate();
	btnAvatarClicked();
}

void ShopScene::buyItem(int pItemID, int pCounter) {

}

void ShopScene::reloadDatas() {
	mLblName->setString(GameInfo::mUserInfo.mUsername);
	mLblName->setPosition(Vec2(20 + mLblName->getContentSize().width*mLblName->getScaleX() / 2, 320));
	mLblName->setPosition(Vec2(mSpriteAvatar->getContentSize().width + 20 + mLblName->getContentSize().width*mLblName->getScaleX() / 2, mLblBalance->getPositionY() + 30));

	std::string pMoney = __String::createWithFormat("%ld", GameInfo::mUserInfo.mBalance)->getCString();
	pMoney = EGSupport::addDotMoney(pMoney);
	pMoney += " xu";
	mLblBalance->setString(pMoney);
	mLblBalance->setPosition(Vec2(20 + mLblBalance->getContentSize().width*mLblBalance->getScaleX() / 2, mLblName->getPositionY() - mLblBalance->getContentSize().height - 5));
	mLblBalance->setPosition(Vec2(mSpriteAvatar->getContentSize().width + 20 + mLblBalance->getContentSize().width*mLblBalance->getScaleX() / 2, mSpriteAvatar->getPositionY1() + mLblBalance->getContentSize().height / 2));


	mSpriteAvatar->setTexture(__String::createWithFormat("avatar (%d).png", GameInfo::mUserInfo.mAvatarID)->getCString());
	//mSpriteAvatar->setPosition(Vec2(mSpriteAvatar->getContentSize().width / 2 + 20, mLblName->getPositionY() + mSpriteAvatar->getContentSize().height));
}

void ShopScene::update(float dt) {
	MpMessage* pMsg = MpManager::getInstance()->getMessenger();
	if (pMsg){
		switch (pMsg->getType())
		{
		case MP_MSG_BUY_ITEM_ACK:
		{
			MpBuyItemMessageResponse* pResponse = (MpBuyItemMessageResponse*)pMsg;
			if (pResponse->getErrorCode() != 0) {
				mBoxObjItemDetails->mErrorcode_ObjItemDetails = __String::createWithFormat("%d", pResponse->getErrorCode())->getCString();
			}
			else {
				mBoxObjItemDetails->mErrorcode_ObjItemDetails = TEXT_SUCCESS_BUYITEM;
				GameInfo::mUserInfo.mAvatarID = mBoxObjItemDetails->getItemId();
				mSpriteAvatar->setTexture(__String::createWithFormat("avatar (%d).png", GameInfo::mUserInfo.mAvatarID)->getCString());
			}
		}
		break;
		case MP_MSG_XCHANGE_SCRATCH_CARD_ACK:
		{
			MpMessage *msg = pMsg;
			uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);

			if (pErrorCode != 0) {
				if (pErrorCode == BuildErrorCode(MP_MSG_XCHANGE_SCRATCH_CARD, MP_PARAM_BALANCE + 1)){
					WXmlReader* pXml = WXmlReader::create();
					pXml->load(KEY_XML_FILE);
					uint32_t balance = msg->getInt(MP_PARAM_BALANCE + 1);
					mErrorCode_ShopScene = __String::createWithFormat(pXml->getNodeTextByTagName("txt_reward_over_amount_pernmiss").c_str(), EGSupport::convertMoneyAndAddDot(balance).c_str())->getCString();
				}
				else
					mErrorCode_ShopScene = "txt_take_recharge_fail";
			}
			else {
				mErrorCode_ShopScene = "take_recharge_success";
			}
		}
		break;
		case MP_MSG_GET_XCHANGE_SC_CFG_ACK:
		{
			MpMessage *msg = pMsg;
			uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);

			if (pErrorCode != 0) {
				mErrorCode_ShopScene = __String::createWithFormat("%d", pErrorCode)->getCString();
			}
			else {
				std::string data = msg->getString(MP_PARAM_SCRATCH_CARD_INFO);
				lstReward = ScratchCard::from_string(data);
				mErrorCode_ShopScene = "load_rewaard_success";
			}
		}
		break;
		default:
			break;
		}
		delete pMsg;
		pMsg = NULL;
	}
	if (getErrorCode() != ""){
		mErrorCode_ShopScene = getErrorCode();
		setErrorCode("");
	}
	if (mErrorCode_ShopScene != "") {
		mLoading->hide();
		if (mErrorCode_ShopScene == "load_rewaard_success"){
			btnLogoPhoneClicked(1);
		}
		else if (mErrorCode_ShopScene == "take_recharge_success"){
			executeReward();
		}
		else if (mErrorCode_ShopScene == "take_recharge_fail"){
			BoxNoticeManager::getBoxNotice(mBoxNotice,
				pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				pXml->getNodeTextByTagName("txt_take_recharge_fail").c_str());
			mBoxNotice->setVisible(true);
		}
		else{
			BoxNoticeManager::getBoxNotice(mBoxNotice,
				pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				pXml->getNodeTextByTagName(mErrorCode_ShopScene).c_str());
			mBoxNotice->setVisible(true);
		}
	}
	mErrorCode_ShopScene = "";
}

void ShopScene::showItemDetails(int pItemID, int pExType, int pType) {
	mBoxObjItemDetails->show(pItemID, pExType, pType, NULL, 1);
}

void ShopScene::onBackPressed() {
	if (mLoading->isVisible()) {
		return;
	}
	else if (mBoxNotice->isVisible()) {
		mBoxNotice->setVisible(false);
	}
	else if (mBoxObjItemDetails->isVisible()) {
		mBoxObjItemDetails->setVisible(false);
	}
	else {
		Scene* pScene = MainScene::scene(0);
		TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
		Director::getInstance()->replaceScene(pTrans);
	}
}

void ShopScene::btnAvatarClicked()
{
	mbackgroundTab->setTexture("background_shopavatar_tab.png");
	_nodeAvatar->setPosition(10, 10);
	_nodeReward->setPosition(CLIENT_WIDTH, 10);
	mViewAvatar->reloadData();
}

void ShopScene::btnLogoPhoneClicked(uint8_t idx)
{
	for (uint8_t i = 0; i < _lstRadioButton.size(); ++i){
		if (_lstRadioButton[i]->getTag() == idx)
			_lstRadioButton[i]->setVisible(true);
		else
			_lstRadioButton[i]->setVisible(false);
	}

	//handle view content
	lstRewardBuffer.clear();
	for (uint8_t i = 0; i < lstReward.size(); ++i)
	{
		if (lstReward[i].telcoId == idx)
			lstRewardBuffer.push_back(lstReward[i]);
	}
	mViewReward->reloadData();

}

void ShopScene::rewardItemClicked(ScratchCard item)
{
	uint32_t balance = GameInfo::mUserInfo.mBalance;
	if (balance < item.balance)
	{
		// recharge
		BoxNoticeManager::getBoxNotice(mBoxNotice,
			pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
			__String::createWithFormat(pXml->getNodeTextByTagName("txt_reward_not_enought_balance").c_str(), EGSupport::convertMoneyAndAddDot(item.value).c_str())->getCString());
		mBoxNotice->setVisible(true);
	}
	else{
		// send request reward
		BoxNoticeManager::getBoxConfirm(mBoxNotice,
			pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
			__String::createWithFormat(pXml->getNodeTextByTagName("txt_reward_confirm").c_str(), EGSupport::convertMoneyAndAddDot(item.value).c_str(), EGSupport::convertMoneyAndAddDot(item.balance).c_str())->getCString(),
			CC_CALLBACK_0(ShopScene::sendRewardMessage, this, item));
		mBoxNotice->setVisible(true);
	}

}

Size ShopScene::tableCellSizeForIndex(TableView *table, ssize_t idx) {
	if (table->getTag() == 1)
		return Size(155, 160);
	else if (table->getTag() == 2)
		return Size(100, 190);
	return Size::ZERO;
}

TableViewCell* ShopScene::tableCellAtIndex(TableView *table, ssize_t idx) {
	TableViewCell* tableViewCell = table->dequeueCell();
	if (tableViewCell) {
		tableViewCell->removeAllChildrenWithCleanup(true);
		tableViewCell->removeFromParentAndCleanup(true);
	}
	tableViewCell = TableViewCell::create();

	if (table->getTag() == 1){
		//create cell for card
		std::string filename = "";
		for (uint8_t i = 0; i < 3; ++i){
			if (idx + idx * 2 + i >= lstRewardBuffer.size()) break;
			filename = __String::createWithFormat("doithuong_%d_%d.png", lstRewardBuffer[0].telcoId, lstRewardBuffer[idx + idx * 2 + i].value / 1000)->getCString();
			auto img = EGButtonSprite::createWithFile(filename.c_str());
			img->setPosition(Vec2((i + 1)*img->getContentSize().width*1.27f - 70, 55 + img->getContentSize().height / 2));
			img->setButtonType(EGButtonScrollViewItem);
			tableViewCell->addChild(img);
			img->setOnTouch(CC_CALLBACK_0(ShopScene::rewardItemClicked, this, lstRewardBuffer[idx + idx * 2 + i]));

			auto bgMoney = EGSprite::createWithFile("img_charge_item_money_bg.png");
			bgMoney->setPosition(Vec2(img->getPositionX(), 15 + bgMoney->getContentSize().height / 2));
			tableViewCell->addChild(bgMoney);

			auto lbMoney = Label::createWithBMFont(FONT_BMF_MONEY, EGSupport::convertMoneyAndAddDot(lstRewardBuffer[idx + idx * 2 + i].balance) + " xu");
			lbMoney->setColor(Color3B::YELLOW);
			lbMoney->setPosition(bgMoney->getContentSize() / 2);
			bgMoney->addChild(lbMoney);
		}
	}
	else if (table->getTag() == 2){
		//create cell for avatar
		for (uint8_t i = 0; i < 4; ++i){
			auto avatar = ObjItem::create(idx + 3 * idx + i + 2, 1, 0, true);
			avatar->setButtonType(EGButtonScrollViewItem);
			avatar->setPosition(Vec2((i + 1)*avatar->getContentSize().width*1.09f - 100, 10 + avatar->getContentSize().height / 2));
			tableViewCell->addChild(avatar);
			avatar->setOnTouch(CC_CALLBACK_0(ShopScene::showItemDetails, this, idx + 3 * idx + i + 2, 1, 0));
		}
	}

	return tableViewCell;
}

ssize_t ShopScene::numberOfCellsInTableView(TableView *table) {
	if (table->getTag() == 1){
		if (lstRewardBuffer.size() % 3 != 0)
			return lstRewardBuffer.size() / 3 + 1;
		else
			return lstRewardBuffer.size() / 3;
	}
	else if (table->getTag() == 2)
		return 12;
	return 0;

}

void ShopScene::sendRewardMessage(ScratchCard item)
{
	itemReward = item;
	mLoading->show();
	mBoxNotice->setVisible(false);
	ScratchCard* str = new ScratchCard();
	*str = item;
	pthread_takeReward(str);
}

void ShopScene::executeReward()
{
	mLoading->hide();
	BoxNoticeManager::getBoxNotice(mBoxNotice,
		pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
		pXml->getNodeTextByTagName("txt_reward_success").c_str());
	mBoxNotice->setVisible(true);

	GameInfo::mUserInfo.mBalance = GameInfo::mUserInfo.mBalance - itemReward.balance;

	std::string pMoney = __String::createWithFormat("%ld", GameInfo::mUserInfo.mBalance)->getCString();
	pMoney = EGSupport::addDotMoney(pMoney);
	pMoney += " xu";
	mLblBalance->setString(pMoney);
}

void ShopScene::loadLstCardReward()
{
	mLoading->show();
	pthread_loadReward(NULL);
}

