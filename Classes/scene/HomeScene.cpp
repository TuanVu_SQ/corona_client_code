﻿#include "HomeScene.h"
//#include "tcpclient.h"
#include "GameInfo.h"
#include "ExceptionGlobal.h"

#include "MessageParam.h"
#include "MpLoginRequest.h"
#include "MpLoginResponse.h"

#include "BoxNoticeManager.h"

#include "MainScene.h"
#include "EGJniHelper.h"
#include "HomeSceneOff.h"
#include "WHelper.h"
#include "MessageType.h"
#include "WHelper.h"
#include "libs/WClock.h"
#include "MpJoinTableMessageResponse.h"
#include "ChooseRoomScene.h"
#include "libs/WAudioControl.h"


#include "md5.h"
#include "FirstScene.h"
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "unistd.h"
#endif

#define TAG_SECURITY 0x210
#define TAG_HM 0x211
#define TAG_HK 0x212
#define TAG_HC 0x213
#define TAG_PN 0x214
#define TAG_TS 0x215
#define TAG_DEVICENAME 0x216

static roomStruct* mStrucReconect;
//char IP_SERVER_ACCOUNTd[128];
std::string mErrorCode_HomeScene;
std::string mErrorCode_ReConnect;
std::string mErrorDescription;
UserInfo GameInfo::mUserInfo;
void* pthread_reconect(void* _void);

void pthread_loadBetMoneyHome(void* _void) {
	std::vector<uint32_t> vtBetMoney;
	BoxTopUp* boxTopUp = (BoxTopUp*)_void;
	std::string ip;
	int port;
	for (int i = 0; i < GameInfo::mUserInfo.lstGameDomain.size(); i++) 
	{
		if (GameInfo::mUserInfo.lstGameDomain.at(i).gameId == 1) 
		{
			ip = GameInfo::mUserInfo.lstGameDomain.at(i).domain;
			port = GameInfo::mUserInfo.lstGameDomain.at(i).port;
			break;
		}
	}

	MpClientManager::getInstance(2)->setMpClientEnable(true);
	MpClientManager::getInstance(2)->connect(ip.c_str(), port);
	MpMessage *messag = new MpMessage((uint32_t)MP_MSG_GET_LIST_BETMONEY);
	messag->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
	MpClientManager::getInstance()->sendMessage(messag, true);
}
void HomeScene::onLoginFacebook(int pResult, std::string pAccessToken) {
	if (pResult == 0) {
		mErrorCode_HomeScene = "errcode_login_facebook_fall";
	}
	else {
		// login with facebook
		// step 1: get access token
		// step 2: login with access token and default passwords
		// step 3: ( in server - check access token already have account ? )
		// YES : Login successfully
		// NO : ( in server - register for this access token ) and Login successfully
	}
}

void HomeScene::goGameOffline() {
	roomStruct *mInfoRoom = new roomStruct();
	//mInfoRoom->mTcp = new TcpClient();
	mInfoRoom->mTableID = 0;
	mInfoRoom->pTypeRoom = 0;
	mInfoRoom->mIDRoom = 0;
	mInfoRoom->pRoomLevelstr = "0";
	std::vector<ClientPlayerInfoEX> *mPlayerList = new std::vector<ClientPlayerInfoEX>();
	ClientPlayerInfoEX player;
	player.username = "";
	player.balance = atof(UserDefault::getInstance()->getStringForKey(KEY_MONEY, "500").c_str());
	player.score = 0;
	player.itemId = 101;
	mPlayerList->push_back(player);
	mInfoRoom->mListPlayer = mPlayerList;
	mInfoRoom->mGameID = 2;
	mInfoRoom->pMaxPlayer = 1;
	mInfoRoom->iCreater = false;
	mInfoRoom->mBackGroundID = 1 + rand() % 14;
	Scene* _scene = GameScene::createScene(mInfoRoom);
	Director::getInstance()->replaceScene(_scene);

}
Scene* HomeScene::scene() {
	HomeScene* pScene = new HomeScene();
	GameInfo::mUserInfo.stateScene = 0;
	pScene->registerBackPressed();
	pScene->initScene();
	pScene->autorelease();

	return pScene;
}

#include "BoxTopUpCard.h"

#include "AvatarInfo.h"
#include "GunInfo.h"
#include "ItemInfo.h"

void HomeScene::initScene() {

	isConnect = false;
	MpClientManager::getInstance(2)->close();
	MpClientManager::getInstance(1)->close();
	MpClientManager::getInstance(0)->close();
	GameInfo::mUserInfo.mIsKeepAlive = false;
	mErrorCode_HomeScene = "";
	GameInfo::mUserInfo.mUsername = "";
	GameInfo::mUserInfo.mLevelRoom = -1;
	GameInfo::mUserInfo.mTabChoose = 0;
	mStrucReconect = new roomStruct();
	mXml = WXmlReader::create();
	mXml->load(KEY_XML_FILE);
	mErrorCode_ReConnect = "";
	// ------------------------------------------------------------------ BACKGROUND ----------------------------------------------------
	mBackground = RippleSprite::create("img_background_login.png");
	//mBackground->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBackground);
	//mBackground->autoRipple();

	// ------------------------------------------------------------------ TITLE ----------------------------------------------------
	mTitle = Sprite::create("logo-home-off.png");
	mTitle->setPosition(Vec2(CLIENT_WIDTH - mTitle->getContentSize().width / 2 - 20, CLIENT_HEIGHT - mTitle->getContentSize().height / 2 - 10));
	mBackground->addChild(mTitle, 2);

	auto character = Sprite::create("kv_charracter.png");
	character->setPosition(Vec2(character->getContentSize().width/2 + 30, character->getContentSize().height / 2 + 20));
	mBackground->addChild(character, 2);

	// ------------------------------------------------------------------ EDITTEXT ----------------------------------------------------
	Scale9Sprite* pEdtSprite_Username = Scale9Sprite::create(IMG_EDITTEXT);
	mEdt_Username = EditBox::create(Size(312, 53), pEdtSprite_Username);
	mEdt_Username->setPosition(Vec2(mTitle->getPositionX(), mTitle->getPositionY() - 100));
	mEdt_Username->setFontName(FONT);
	mEdt_Username->setFontSize(20);
	//mEdt_Username->setMargins(20, 20);
	mEdt_Username->setInputFlag(EditBox::InputFlag::SENSITIVE);
	mEdt_Username->setInputMode(EditBox::InputMode::SINGLE_LINE);
	mEdt_Username->setFontColor(Color3B::WHITE);
	mEdt_Username->setPlaceHolder(mXml->getNodeTextByTagName(TEXT_EDT_USERNAME).c_str());
	mEdt_Username->setPlaceholderFontColor(Color3B::GRAY);
	mEdt_Username->setMaxLength(REQ_MAXLENGHT_USERNAME);
	mEdt_Username->setReturnType(EditBox::KeyboardReturnType::DONE);
	mEdt_Username->setText(UserDefault::getInstance()->getStringForKey(KEY_USERNAME, "").c_str());
	mEdt_Username->setDelegate(this);
	mBackground->addChild(mEdt_Username, 2);

	Scale9Sprite* pEdtSprite_Passwords = Scale9Sprite::create(IMG_EDITTEXT);
	mEdt_Passwords = EditBox::create(Size(312, 53), pEdtSprite_Passwords);
	mEdt_Passwords->setPosition(Vec2(mEdt_Username->getPositionX(), mEdt_Username->getPositionY() - mEdt_Username->getContentSize().height - 10));
	mEdt_Passwords->setFontName(FONT);
	mEdt_Passwords->setFontSize(20);
	//mEdt_Passwords->setMargins(20, 20);
	mEdt_Passwords->setInputFlag(EditBox::InputFlag::PASSWORD);
	mEdt_Passwords->setInputMode(EditBox::InputMode::SINGLE_LINE);
	mEdt_Passwords->setFontColor(Color3B::WHITE);
	mEdt_Passwords->setPlaceHolder(mXml->getNodeTextByTagName("insert_passwords").c_str());
	mEdt_Passwords->setPlaceholderFontColor(Color3B::GRAY);
	mEdt_Passwords->setMaxLength(REQ_MAXLENGHT_PASSWORDS);
	mEdt_Passwords->setReturnType(EditBox::KeyboardReturnType::DONE);
	mEdt_Passwords->setText(UserDefault::getInstance()->getStringForKey(KEY_PASSWORDS, "").c_str());
	mEdt_Passwords->setDelegate(this);
	mBackground->addChild(mEdt_Passwords, 2);

	// ------------------------------------------------------------------ CHECKBOX - FORGETPASS ----------------------------------------------------
	mBtn_BoxCheck = EGButtonSprite::createWithFile("kv_tick_bg.png");
	mBtn_BoxCheck->setOnTouch(CC_CALLBACK_0(HomeScene::checkBox, this));
	mBtn_BoxCheck->setPosition(Vec2(mEdt_Username->getPositionX() - mEdt_Username->getContentSize().width / 2
		+ mBtn_BoxCheck->getContentSize().width / 2, mEdt_Passwords->getPositionY() - mEdt_Passwords->getContentSize().height / 2
		- mBtn_BoxCheck->getContentSize().height / 2 - 10));
	mBackground->addChild(mBtn_BoxCheck, 2);

	mCheck = Sprite::create("kv_tich.png");
	mCheck->setPosition(Vec2(mBtn_BoxCheck->getContentSize().width / 2, mBtn_BoxCheck->getContentSize().height / 2));
	mCheck->setVisible(UserDefault::getInstance()->getBoolForKey(KEY_REMEMBER, true));
	mBtn_BoxCheck->addChild(mCheck);

	Sprite* pTextRemember = Sprite::createWithSpriteFrameName(IMG_REMEMBERPASSWORDS);
	pTextRemember->setPosition(Vec2(mBtn_BoxCheck->getContentSize().width + pTextRemember->getContentSize().width / 2 + 10,
		mBtn_BoxCheck->getContentSize().height / 2));
	mBtn_BoxCheck->addChild(pTextRemember);

	mBtn_ForgetPass = EGButtonSprite::createWithSpriteFrameName(IMG_FORGETPASS);
	//mBtn_ForgetPass->setVisible(false);
	mBtn_ForgetPass->setPosition(Vec2(mEdt_Username->getPositionX() + mEdt_Username->getContentSize().width / 2
		- mBtn_ForgetPass->getContentSize().width / 2, mBtn_BoxCheck->getPositionY()));
	mBackground->addChild(mBtn_ForgetPass, 2);
	mBtn_ForgetPass->setOnTouch(CC_CALLBACK_0(HomeScene::forgetPasswords, this));

	// ------------------------------------------------------------------ EDITTEXT RE_PASSWORDS ----------------------------------------------------
	Scale9Sprite* pEdtSprite_RePasswords = Scale9Sprite::create(IMG_EDITTEXT);
	mEdt_RePasswords = EditBox::create(Size(312, 53), pEdtSprite_RePasswords);
	mEdt_RePasswords->setPosition(Vec2(mEdt_Username->getPositionX(), mEdt_Passwords->getPositionY() - mEdt_RePasswords->getContentSize().height - 10));
	mEdt_RePasswords->setFontName(FONT);
	mEdt_RePasswords->setFontSize(20);
	//mEdt_RePasswords->setMargins(20, 20);
	mEdt_RePasswords->setInputFlag(EditBox::InputFlag::PASSWORD);
	mEdt_RePasswords->setFontColor(Color3B::WHITE);
	mEdt_RePasswords->setPlaceHolder(mXml->getNodeTextByTagName("re_insert_passwords").c_str());
	mEdt_RePasswords->setPlaceholderFontColor(Color3B::GRAY);
	mEdt_RePasswords->setMaxLength(REQ_MAXLENGHT_PASSWORDS);
	mEdt_RePasswords->setReturnType(EditBox::KeyboardReturnType::DONE);
	mEdt_RePasswords->setVisible(false);
	mEdt_RePasswords->setDelegate(this);
	mBackground->addChild(mEdt_RePasswords, 2);

	// ------------------------------------------------------------------ BUTTON ----------------------------------------------------
	mBtn_Login = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_LOGIN, EGButton2FrameDark);
	mBtn_Login->setOnTouch(CC_CALLBACK_0(HomeScene::login, this));
	mBtn_Login->setPosition(Vec2(mEdt_Username->getPositionX() - mEdt_Username->getContentSize().width / 2
		+ mBtn_Login->getContentSize().width / 2, mBtn_BoxCheck->getPositionY() - mBtn_BoxCheck->getContentSize().height / 2
		- mBtn_Login->getContentSize().height / 2 - 10));
	mBackground->addChild(mBtn_Login, 2);

	mBtn_Register = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_REGISTER, EGButton2FrameDark);
	mBtn_Register->setOnTouch(CC_CALLBACK_0(HomeScene::registerAccount, this));
	mBtn_Register->setPosition(Vec2(mEdt_Username->getPositionX() + mEdt_Username->getContentSize().width / 2
		- mBtn_Register->getContentSize().width / 2, mBtn_Login->getPositionY()));
	mBackground->addChild(mBtn_Register, 2);

	EGButtonSprite* buttonloginByGuest = EGButtonSprite::createWithFile("bt-login-by-guest.png");
	buttonloginByGuest->setVisible(false);
	mBackground->addChild(buttonloginByGuest, 2);
	buttonloginByGuest->setPosition(Vec2(mEdt_Username->getPosition().x, mBtn_Login->getPositionY() - 10 - buttonloginByGuest->getContentSize().height));
	buttonloginByGuest->setButtonType(EGButton2FrameDark);
	buttonloginByGuest->setOnTouch(CC_CALLBACK_0(HomeScene::facebookPress, this));

#if(CC_TARGET_PLATFORM != CC_PLATFORM_WP8 && CC_TARGET_PLATFORM != CC_PLATFORM_WINRT)
	/*mBtnLoginFb = EGButtonSprite::createWithFile("btnfblogin.png", EGButton2FrameDark);
	mBtnLoginFb->setPosition(Vec2(mEdt_Username->getPositionX(), mBtn_Login->getPositionY() - mBtn_Login->getContentSize().height / 2
	- mBtnLoginFb->getContentSize().height / 2 - 10));
	mBtnLoginFb->setOnTouch([=] {
	EGJniHelper::loginFacebook();
	});
	mBackground->addChild(mBtnLoginFb);*/
#endif

	mBtn_Skip = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_SKIP, EGButton2FrameDark);
	mBtn_Skip->setVisible(false);
	mBtn_Skip->setOnTouch(CC_CALLBACK_0(HomeScene::onBackPressed, this));
	mBtn_Skip->setPosition(mBtn_Login->getPosition());
	mBackground->addChild(mBtn_Skip, 2);

	/*EGButtonSprite* buttonOffline = EGButtonSprite::createWithSpriteFrameName("new_playoffline.png");
	mBackground->addChild(buttonOffline, 2);
	buttonOffline->setButtonType(EGButton2FrameDark);
	buttonOffline->registerTouch(true);
	buttonOffline->setPosition(Vec2(8 + buttonOffline->getContentSize().width / 2, 8 + buttonOffline->getContentSize().height / 2));
	buttonOffline->setOnTouch(CC_CALLBACK_0(HomeScene::goGameOffline, this));*/
	mBoxNotice = BoxNotice::create();
	mBoxNotice->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBoxNotice);

	std::string fileMusic = "nhac1.png";
	if (!UserDefault::getInstance()->getBoolForKey(KEY_MUSIC, true)) {
		fileMusic = "nhac2.png";
		CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0);
		//WAudioControl::getInstance()->setVolumeBackground(0);
	}

	mBtnMusic = EGButtonSprite::createWithFile(fileMusic.c_str());
	mBtnMusic->setPosition(Vec2(mBtnMusic->getContentSize().width / 2 + 10, CLIENT_HEIGHT - mBtnMusic->getContentSize().height / 2 - 10));
	mBtnMusic->setOnTouch(CC_CALLBACK_0(HomeScene::musicConfig, this));
	mBackground->addChild(mBtnMusic, 2);

	std::string fileSound = "tieng1.png";
	if (!UserDefault::getInstance()->getBoolForKey(KEY_SOUND, true)) {
		fileSound = "tieng2.png";
	}

	mBtnSound = EGButtonSprite::createWithFile(fileSound.c_str());
	mBtnSound->setPosition(Vec2(mBtnMusic->getPositionX2() + mBtnSound->getContentSize().width / 2 + 10, mBtnMusic->getPositionY()));
	mBtnSound->setOnTouch(CC_CALLBACK_0(HomeScene::soundConfig, this));
	mBackground->addChild(mBtnSound, 2);

	/*mBtnBack = EGButtonSprite::createWithSpriteFrameName("btnback.png", EGButton2FrameDark);
	mBtnBack->setOnTouch(CC_CALLBACK_0(HomeScene::onBackPressed, this));
	mBtnBack->setPosition(Vec2(mBtnBack->getContentSize().width / 2 + 10, mBtnBack->getContentSize().height / 2 + 10));
	mBackground->addChild(mBtnBack);*/

	mLoading = Loading::create();
	mLoading->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mLoading);

	if (UserDefault::getInstance()->getBoolForKey("firsttime_install", true)) {
		UserDefault::getInstance()->setBoolForKey("firsttime_install", false);
		mBtn_Register->performClick();
	}
	//this->schedule(schedule_selector(HomeScene::updateBubble), 2);
	this->scheduleUpdate();
	//requestDetectSub();

	boxResetPass = BoxResetPass::create();
	boxResetPass->setPosition(Vec2(CLIENT_WIDTH/2, CLIENT_HEIGHT/2));
	this->addChild(boxResetPass);
	boxResetPass->setFuncSendPhoneNumber(CC_CALLBACK_1(HomeScene::requestGetOtp, this));
	boxResetPass->setFuncSendOTP(CC_CALLBACK_2(HomeScene::sendOtpToResetPass, this));
	boxResetPass->setFuncResendOTP(CC_CALLBACK_0(HomeScene::resendOTP, this));

	MpClientManager::getInstance()->clearOldMsg();
	MpClientManager::getInstance()->createMpClientInstances();
	MpClientManager::getInstance(0)->setMpClientEnable(true);
	MpClientManager::getInstance(0)->connect(GameInfo::loginServerAddress, GameInfo::loginServerPort);

	
	
}

void HomeScene::soundConfig() {
	bool isSoundOn = UserDefault::getInstance()->getBoolForKey(KEY_SOUND, true);
	if (isSoundOn) {
		mBtnSound->setTexture("tieng2.png");
	}
	else {
		mBtnSound->setTexture("tieng1.png");
	}
	UserDefault::getInstance()->setBoolForKey(KEY_SOUND, !isSoundOn);
}

void HomeScene::musicConfig() {
	bool isMusicOn = UserDefault::getInstance()->getBoolForKey(KEY_MUSIC, true);
	if (isMusicOn) {
		mBtnMusic->setTexture("nhac2.png");
		CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0);
		//WAudioControl::getInstance()->setVolumeBackground(0);
	}
	else {
		mBtnMusic->setTexture("nhac1.png");
		CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(1);
		//WAudioControl::getInstance()->setVolumeBackground(1);
	}
	UserDefault::getInstance()->setBoolForKey(KEY_MUSIC, !isMusicOn);
}

void HomeScene::editBoxReturn(EditBox* editBox) {
#if(CC_TARGET_PLATFORM == CC_PLATFORM_WP8 || CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
#else
	if (editBox == mEdt_Username) {
		mEdt_Passwords->touchDownAction(NULL, cocos2d::ui::Widget::TouchEventType::ENDED);
	}
	else if (editBox == mEdt_Passwords) {
		if (mEdt_RePasswords->isVisible()) {
			mEdt_RePasswords->touchDownAction(NULL, cocos2d::ui::Widget::TouchEventType::ENDED);
		}
		else {
			if (mEdt_Passwords->getText() != "" && mEdt_Username->getText() != "")
				mBtn_Login->performClick();
		}
	}
	else if (editBox == mEdt_RePasswords) {
		if (mEdt_Passwords->getText() != "" && mEdt_Username->getText() != "" && mEdt_RePasswords->getText() != "")
			mBtn_Register->performClick();
	}
#endif
}

void HomeScene::checkBox() {
	mCheck->setVisible(!mCheck->isVisible());
}

struct LoginStruct {
public:
	Sprite* mCheck;
	std::string pUsername;
	std::string pPasswords, pCPID;
	std::string pDeviceID;
	bool loginByGuest;
	Loading* mLoading;
	uint8_t mChargeID;
};



void HomeScene::login() {
	UserDefault::getInstance()->setBoolForKey(KEY_REMEMBER, mCheck->isVisible());
	std::string pUsername = mEdt_Username->getText();
	std::string pPasswords = mEdt_Passwords->getText();
	if (!mCheck->isVisible()) {
		UserDefault::getInstance()->setStringForKey(KEY_USERNAME, "");
		UserDefault::getInstance()->setStringForKey(KEY_PASSWORDS, "");
	}
	else {
		UserDefault::getInstance()->setStringForKey(KEY_USERNAME, pUsername);
		UserDefault::getInstance()->setStringForKey(KEY_PASSWORDS, pPasswords);
	}
	const char* pCPID = EGJniHelper::getCPID();
	uint8_t pChargeID = atoi(EGJniHelper::getRechargeID());

	/*if (pUsername.length() < REQ_MINLENGHT_USERNAME ||
		pUsername.length() > REQ_MAXLENGHT_USERNAME)
	{
		mErrorCode_HomeScene = TEXT_ERR_USERNAME_INVALID;
		return;
	}*/
	/*else if (pUsername.find_first_not_of("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890") != std::string::npos)
	{
		mErrorCode_HomeScene = TEXT_ERR_USERNAME_INVALID;
		return;
	}
	else if (pUsername.substr(0, 1).find_first_not_of("01234567890") == std::string::npos) {
		mErrorCode_HomeScene = TEXT_ERR_USERNAME_INVALID;
		return;
	}*/
	/*else if (strlen(pPasswords.c_str()) < REQ_MINLENGHT_PASSWORDS ||
		strlen(pPasswords.c_str()) > REQ_MAXLENGHT_PASSWORDS)
	{
		mErrorCode_HomeScene = TEXT_ERR_PASS_INVALID;
		return;
	}*/

	mLoading->show();
	GameInfo::mUserInfo.mPassWord = pPasswords;
	pPasswords = md5(pPasswords);
	LoginStruct *pLoginStruct = new LoginStruct();
	pLoginStruct->mCheck = mCheck;
	pLoginStruct->pUsername = pUsername;
	pLoginStruct->pPasswords = pPasswords;
	pLoginStruct->pDeviceID = EGJniHelper::getDeviceID();
	pLoginStruct->pCPID = pCPID;
	pLoginStruct->mLoading = mLoading;
	pLoginStruct->mChargeID = pChargeID;
	pLoginStruct->loginByGuest = false;
	loginProcess(pLoginStruct);


}
void HomeScene::loginProcess(void* pVoid) {
	LoginStruct *pLoginStruct = (LoginStruct*)pVoid;
	MpLoginRequest *pRequest = NULL;
	if (GameInfo::mUserInfo.mPassWord == "") {
		pRequest = new MpLoginRequest(MP_MSG_FB_LOGIN);
		pRequest->setTokenId(pLoginStruct->pPasswords.c_str());
	}
	else {
		pRequest = new MpLoginRequest();
	}
	pRequest->addString(MP_PARAM_FACEBOOK_ID, pLoginStruct->pUsername.c_str());
	pRequest->setUsername(pLoginStruct->pUsername.c_str());
	pRequest->setPassword(pLoginStruct->pPasswords.c_str());
	pRequest->setCpId(pLoginStruct->pCPID.c_str());
	pRequest->setVersion(VERSION);
	pRequest->setGameGroup(GROUP_ID);
	pRequest->addString(MP_PARAM_DEVICE_ID, pLoginStruct->pDeviceID.c_str());
	std::string pOS = GameInfo::os_prefix;
	pRequest->setOs(pOS.c_str());
	std::string pKey = pLoginStruct->pUsername + "89f4f96a62c7a49b204921033b68f80c";
	if (pLoginStruct->loginByGuest) {
		pKey = pLoginStruct->pDeviceID + "89f4f96a62c7a49b204921033b68f80c";
	}
	for (int i = 0; i < pKey.length(); i++) {
		pKey[i] = std::tolower(pKey[i]);
	}
	pRequest->setKey(pKey.c_str());
	pRequest->addString(TAG_HM, WHelper::getInstance()->getHM());
	pRequest->addString(TAG_HK, WHelper::getInstance()->getHK());
	pRequest->addString(TAG_HC, WHelper::getInstance()->getHC());
	pRequest->addString(TAG_PN, WHelper::getInstance()->getPK());
	std::string timeStamp = WHelper::getInstance()->getTP();
	pRequest->addString(TAG_TS, timeStamp);
	pRequest->addString(TAG_SECURITY, WDeviceInfo::getSecurityString(timeStamp));
	pRequest->addString(TAG_DEVICENAME, WHelper::getInstance()->getDeviceName());
	pRequest->addString(MP_PARAM_COUNTRY, WHelper::getInstance()->getCountry());
	//CCLOG(WHelper::getInstance()->getCountry().c_str());
	/*if (!isConnect) {
		MpClientManager::getInstance()->setMpClientEnable(true);
		MpClientManager::getInstance()->connect(GameInfo::loginServerAddress, GameInfo::loginServerPort);

		isConnect = true;
	}*/
	
	MpClientManager::getInstance(0)->sendMessage(pRequest, true);
	isConnect = true;

}

struct RegisterStruct {
public:
	std::string pUsername, pPasswords;
	const char *pCPID, *pEmail;
	std::string pDeviceID;
	Loading* mLoading;
};

void* pthread_register(void* _void) {
	RegisterStruct* pRegisterStruct = (RegisterStruct*)_void;

	MpLoginRequest *pRequest = new MpLoginRequest(MP_MSG_REGISTER);
	pRequest->setUsername(pRegisterStruct->pUsername);
	pRequest->setPassword(pRegisterStruct->pPasswords);
	pRequest->setCpId(pRegisterStruct->pCPID);
	pRequest->addString(MP_PARAM_DEVICE_ID, pRegisterStruct->pDeviceID);
	pRequest->addString(MP_PARAM_EMAIL, pRegisterStruct->pEmail);

	pRequest->addString(TAG_HM, WHelper::getInstance()->getHM());
	pRequest->addString(TAG_HK, WHelper::getInstance()->getHK());
	pRequest->addString(TAG_HC, WHelper::getInstance()->getHC());
	pRequest->addString(TAG_PN, WHelper::getInstance()->getPK());
	std::string timeStamp = WHelper::getInstance()->getTP();
	pRequest->addString(TAG_TS, timeStamp);
	pRequest->addString(TAG_SECURITY, WDeviceInfo::getSecurityString(timeStamp));
	pRequest->addString(TAG_DEVICENAME, WHelper::getInstance()->getDeviceName());
	MpClientManager::getInstance(0)->sendMessage(pRequest, true);


	delete pRegisterStruct;
	pRegisterStruct = NULL;

	//pthread_exit(NULL);
	return NULL;
}
void HomeScene::facebookPress() {
	WHelper::getInstance()->_facebookCallback = CC_CALLBACK_2(HomeScene::loginFacebook, this);
	WHelper::getInstance()->loginFacebook();
	//loginFacebook("084748387", "linhtb")/*;*/
}

void HomeScene::loginFacebook(std::string facebookID, std::string tokenFacebook) {
	mLoading->show();
	const char* pCPID = EGJniHelper::getCPID();
	uint8_t pChargeID = atoi(EGJniHelper::getRechargeID());
	LoginStruct *pLoginStruct = new LoginStruct();
	pLoginStruct->mCheck = mCheck;
	pLoginStruct->pDeviceID = EGJniHelper::getDeviceID();
	pLoginStruct->pCPID = pCPID;
	pLoginStruct->mLoading = mLoading;
	pLoginStruct->mChargeID = pChargeID;
	pLoginStruct->loginByGuest = true;
	pLoginStruct->pUsername = facebookID;
	pLoginStruct->pPasswords = tokenFacebook;
	GameInfo::mUserInfo.mPassWord = "";
	loginProcess(pLoginStruct);
	//WHelper::getInstance()->_facebookCallback = nullptr;
}
void HomeScene::registerAccount() {
	if (mEdt_RePasswords->isVisible()) {
		std::string pUsername = mEdt_Username->getText();
		std::string pPasswords = mEdt_Passwords->getText();
		std::string pRePasswords = mEdt_RePasswords->getText();
		const char* pCPID = EGJniHelper::getCPID();
		const char* pEmail = "M2W_DEMO_EMAIL@gmail.com";

		/*if (pUsername.length() < REQ_MINLENGHT_USERNAME ||
			pUsername.length() > REQ_MAXLENGHT_USERNAME)
		{
			mErrorCode_HomeScene = TEXT_ERR_USERNAME_INVALID;
			return;
		}*/
		//else if (pUsername.find_first_not_of("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890") != std::string::npos)
		if (pUsername.find_first_not_of("0123456789+") != std::string::npos) 
		{
			mErrorCode_HomeScene = TEXT_ERR_USERNAME_INVALID;
			return;
		}
		/*else if (pUsername.substr(0, 1).find_first_not_of("01234567890") == std::string::npos) {
			mErrorCode_HomeScene = TEXT_ERR_USERNAME_INVALID;
			return;
		}*/
		else if (pPasswords != pRePasswords) {
			mErrorCode_HomeScene = TEXT_PASS_NOTSAME_INVALID;
			return;
		}
		else if (pPasswords.length() < REQ_MINLENGHT_PASSWORDS ||
			pPasswords.length() > REQ_MAXLENGHT_PASSWORDS)
		{
			mErrorCode_HomeScene = TEXT_ERR_PASS_INVALID;
			return;
		}

		mLoading->show();

		pPasswords = md5(pPasswords);
		RegisterStruct *pRegisterStruct = new RegisterStruct();
		pRegisterStruct->mLoading = mLoading;
		pRegisterStruct->pCPID = pCPID;
		pRegisterStruct->pDeviceID = EGJniHelper::getDeviceID();
		log("%s", pRegisterStruct->pDeviceID.c_str());
		pRegisterStruct->pEmail = pEmail;
		pRegisterStruct->pPasswords = pPasswords;
		pRegisterStruct->pUsername = pUsername;
		
		if (!isConnect) {
			MpClientManager::getInstance(0)->connect(GameInfo::loginServerAddress, GameInfo::loginServerPort);
			MpClientManager::getInstance(0)->setMpClientEnable(true);
			isConnect = true;
		}
		pthread_register(pRegisterStruct);
		/*pthread_t t1;
		pthread_create(&t1, NULL, &pthread_register, pRegisterStruct);*/
	}
	else {
		mBtn_BoxCheck->setVisible(false);
		mBtn_ForgetPass->setVisible(false);

		mEdt_RePasswords->setVisible(true);

		mBtn_Skip->setVisible(true);
		mBtn_Login->setVisible(false);

		mEdt_Username->setText("");
		mEdt_Passwords->setText("");
		mEdt_RePasswords->setText("");
	}
}

void HomeScene::forgetPasswords() {
	boxResetPass->setPosition(Vec2(CLIENT_WIDTH/2, CLIENT_HEIGHT/2));
	boxResetPass->setVisible(true);
}

void HomeScene::onBackPressed() {
	if (mLoading->isVisible()) {
		return;
	}
	else if (mEdt_RePasswords->isVisible()) {
		mBtn_BoxCheck->setVisible(true);
		//mBtn_ForgetPass->setVisible(true);

		mEdt_RePasswords->setVisible(false);

		mBtn_Skip->setVisible(false);
		mBtn_Login->setVisible(true);

		mEdt_Username->setText(UserDefault::getInstance()->getStringForKey(KEY_USERNAME, "").c_str());
		mEdt_Passwords->setText("");
		mEdt_RePasswords->setText("");
	}
	else {
		Scene* _scene = HomeSceneOff::scene();
		Director::getInstance()->replaceScene(_scene);
	}
}

void HomeScene::update(float dt)
{
	MpMessage * _mess = MpManager::getInstance()->getMessenger();
	if (_mess) 
	{
		switch (_mess->getType())
		{		
		case MP_MSG_REGISTER_ACK:
		{
			MpMessageResponse *pResponse = (MpMessageResponse*)_mess;
			unsigned pErrorCode = pResponse->getErrorCode();
			std::string des = pResponse->getErrorDesciption();
			if (pErrorCode != 0) 
			{
				mErrorCode_HomeScene = des;
			}
			else 
			{
				mErrorCode_HomeScene = TEXT_SUCCESS_REGISTER;
			}
			mLoading->hide();
		}
		break;

		case MP_MSG_GET_LIST_BETMONEY_ACK:
		{
			MpMessage *msg = _mess;
			uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);
			std::vector<uint32_t> vtBetMoney;
			if (pErrorCode != 0) {
				mErrorCode_HomeScene = __String::createWithFormat("%d", pErrorCode)->getCString();
			}
			else {
				std::string data = msg->getString(MP_PARAM_BET_MONEY);
				{
					const uint8_t* ptr = (const uint8_t*)data.data();
					const uint8_t* end = (const uint8_t*)data.data() + data.size();
					while (ptr < end)
					{
						uint32_t n = *((uint32_t*)ptr);
						n = ntohl(n);
						ptr += 4;
						vtBetMoney.push_back(n);
					}
				}
				MainScene::setVectorMoney(vtBetMoney);
				mErrorCode_HomeScene = TEXT_GETBETMONEY_SUCCESS;
			}
		}
		break;

		case MP_MSG_LOGIN_ACK:
			if (_mess == NULL) 
			{
				mErrorCode_HomeScene = TEXT_ERR_TIMEOUT;
				mLoading->hide();
			}
			else 
			{
				MpLoginResponse* pLoginResponse = static_cast<MpLoginResponse*>(_mess);
				if (!pLoginResponse) 
				{
					CCLOG("BAD CAST");
					exit(1);
				}
				unsigned int pErrorCode = pLoginResponse->getErrorCode(); //102262
				if (pErrorCode != 0) 
				{
					GameInfo::mUserInfo.mUrlUpdate = pLoginResponse->getErrorDesciption();

					if (pErrorCode == 102263 || pErrorCode == 124263) {
						mErrorDescription = pLoginResponse->getErrorDesciption();

						GameInfo::mUserInfo.mUsername = pLoginResponse->getFullname();
						for (int i = 0; i < GameInfo::mUserInfo.mUsername.length(); i++) {
							GameInfo::mUserInfo.mUsername[i] = std::tolower(GameInfo::mUserInfo.mUsername[i]);
						}
						GameInfo::mUserInfo.mFullname = pLoginResponse->getFullname();
						GameInfo::mUserInfo.mEmail = pLoginResponse->getEmail();
						GameInfo::mUserInfo.mBirthday = pLoginResponse->getBirthday();
						GameInfo::mUserInfo.mBalance = pLoginResponse->getBalance();
						GameInfo::mUserInfo.mGender = pLoginResponse->getGender();
						GameInfo::mUserInfo.mLevel = pLoginResponse->getLevel();
						GameInfo::mUserInfo.mTokenID = pLoginResponse->getTokenId();
						GameInfo::mUserInfo.mAvatarID = pLoginResponse->getAvatarId();
						GameInfo::mUserInfo.isBonus = pLoginResponse->getInt(MP_PARAM_BONUS);
						GameInfo::mUserInfo.isCanEnableSms = pLoginResponse->getInt(MP_PARAM_ENABLE_SMS);
						//GameInfo::mUserInfo.isCanEnableSms = false;
						std::vector<MpLoginResponse::GameAddress> lstGameAdress = pLoginResponse->getGameAddress();
						GameInfo::mUserInfo.lstGameDomain = lstGameAdress;
						pLoginResponse->getListByTagRate(MP_PARAM_DAME_GUN, GameInfo::DAME_GUN);
						pLoginResponse->getListByTagRate(MP_PARAM_DAME_GROW_BY_GUN, GameInfo::DAME_GROW_BY_GUN);
						pLoginResponse->getListByTagRate(MP_PARAM_DAME_GROW_BY_FISH, GameInfo::DAME_GROW_BY_FISH);
						GameInfo::DAME_GROW_BY_FISH.insert(GameInfo::DAME_GROW_BY_FISH.begin(), 0);
						pLoginResponse->getListByTagRate(MP_PARAM_GROW_RATE_DEAD_BY_GUN, GameInfo::GROW_RATE_DEAD_BY_GUN);
						pLoginResponse->getListByTagRate(MP_PARAM_GROW_RATE_DEAD, GameInfo::GROW_RATE_DEAD);
						GameInfo::GROW_RATE_DEAD.insert(GameInfo::GROW_RATE_DEAD.begin(), 0);
						pLoginResponse->getListByTagRate(MP_PARAM_FISH_RATE_DEAD, GameInfo::FISH_RATE_DEAD);
						GameInfo::FISH_RATE_DEAD.insert(GameInfo::FISH_RATE_DEAD.begin(), 0);
						//pLoginResponse

						GameInfo::mUserInfo.mCPID = EGJniHelper::getCPID();
						GameInfo::mUserInfo.mDeviceID = EGJniHelper::getDeviceID();
						// reconnect only


						GameInfo::mUserInfo.mServiceNumber = "";
						GameInfo::mUserInfo.mSyntax = "";
						GameInfo::mUserInfo.mChargeID = atoi(EGJniHelper::getRechargeID());

						uint32_t tableID = pLoginResponse->getInt(MP_PARAM_TABLE_ID);
						if (tableID != 0) 
						{
							GameInfo::mUserInfo.mTableID = tableID;
							uint8_t index = pLoginResponse->getInt(MP_PARAM_SEVER_ID);
							if (index < lstGameAdress.size()) {
								GameInfo::mUserInfo.domainReconect = lstGameAdress.at(index);
								GameInfo::mUserInfo.mGameID = GameInfo::mUserInfo.domainReconect.gameId;

							}

							mErrorCode_HomeScene = "RECONECT";
						}
						else 
						{
							mLoading->hide();
							mErrorCode_HomeScene = "102263";
						}
						delete pLoginResponse;
						pLoginResponse = NULL;
					}
					else if (pErrorCode == 102310){ 
						std::string fullname = _mess->getString(MP_PARAM_FULLNAME);
						fullname = pLoginResponse->getFullname();
						GameInfo::mUserInfo.mUsername = pLoginResponse->getFullname();
						for (int i = 0; i < GameInfo::mUserInfo.mUsername.length(); i++)
						{
							GameInfo::mUserInfo.mUsername[i] = std::tolower(GameInfo::mUserInfo.mUsername[i]);
						}
						GameInfo::mUserInfo.mFullname = pLoginResponse->getFullname();
						GameInfo::mUserInfo.mEmail = pLoginResponse->getEmail();
						GameInfo::mUserInfo.mBirthday = pLoginResponse->getBirthday();
						GameInfo::mUserInfo.mBalance = pLoginResponse->getBalance();
						GameInfo::mUserInfo.mGender = pLoginResponse->getGender();
						GameInfo::mUserInfo.mLevel = pLoginResponse->getLevel();
						GameInfo::mUserInfo.mTokenID = pLoginResponse->getTokenId();
						GameInfo::mUserInfo.mAvatarID = pLoginResponse->getAvatarId();
						GameInfo::mUserInfo.mCPID = EGJniHelper::getCPID();
						GameInfo::mUserInfo.mDeviceID = EGJniHelper::getDeviceID();
						GameInfo::mUserInfo.isBonus = pLoginResponse->getInt(MP_PARAM_BONUS);
						GameInfo::mUserInfo.isCanEnableSms = pLoginResponse->getInt(MP_PARAM_ENABLE_SMS);
						std::vector<MpLoginResponse::GameAddress> lstGameAdress = pLoginResponse->getGameAddress();
						GameInfo::mUserInfo.lstGameDomain = lstGameAdress;
						pLoginResponse->getListByTagRate(MP_PARAM_DAME_GUN, GameInfo::DAME_GUN);
						pLoginResponse->getListByTagRate(MP_PARAM_DAME_GROW_BY_GUN, GameInfo::DAME_GROW_BY_GUN);
						pLoginResponse->getListByTagRate(MP_PARAM_DAME_GROW_BY_FISH, GameInfo::DAME_GROW_BY_FISH);
						GameInfo::DAME_GROW_BY_FISH.insert(GameInfo::DAME_GROW_BY_FISH.begin(), 0);
						pLoginResponse->getListByTagRate(MP_PARAM_GROW_RATE_DEAD_BY_GUN, GameInfo::GROW_RATE_DEAD_BY_GUN);
						pLoginResponse->getListByTagRate(MP_PARAM_GROW_RATE_DEAD, GameInfo::GROW_RATE_DEAD);
						GameInfo::GROW_RATE_DEAD.insert(GameInfo::GROW_RATE_DEAD.begin(), 0);
						pLoginResponse->getListByTagRate(MP_PARAM_FISH_RATE_DEAD, GameInfo::FISH_RATE_DEAD);
						GameInfo::FISH_RATE_DEAD.insert(GameInfo::FISH_RATE_DEAD.begin(), 0);
						GameInfo::mUserInfo.mServiceNumber = "";
						GameInfo::mUserInfo.mSyntax = "";
						GameInfo::mUserInfo.mChargeID = atoi(EGJniHelper::getRechargeID());
						mErrorCode_HomeScene = TEXT_ACTIVE_REQUIRE;
					}
					else 
					{
						mErrorCode_HomeScene = __String::createWithFormat("%d", pErrorCode)->getCString();
						mLoading->hide();
					}
				}
				else 
				{

					//if (pLoginStruct->mCheck->isVisible()) {
					//	/*UserDefault::getInstance()->setStringForKey(KEY_USERNAME, pLoginStruct->pUsername);
					//	UserDefault::getInstance()->setStringForKey(KEY_PASSWORDS, pLoginStruct->pPasswords);*/
					//}
					std::string fullname = _mess->getString(MP_PARAM_FULLNAME);
					fullname = pLoginResponse->getFullname();
					GameInfo::mUserInfo.mUsername = pLoginResponse->getFullname();
					for (int i = 0; i < GameInfo::mUserInfo.mUsername.length(); i++) 
					{
						GameInfo::mUserInfo.mUsername[i] = std::tolower(GameInfo::mUserInfo.mUsername[i]);
					}
					GameInfo::mUserInfo.mFullname = pLoginResponse->getFullname();
					GameInfo::mUserInfo.mEmail = pLoginResponse->getEmail();
					GameInfo::mUserInfo.mBirthday = pLoginResponse->getBirthday();
					GameInfo::mUserInfo.mBalance = pLoginResponse->getBalance();
					GameInfo::mUserInfo.mGender = pLoginResponse->getGender();
					GameInfo::mUserInfo.mLevel = pLoginResponse->getLevel();
					GameInfo::mUserInfo.mTokenID = pLoginResponse->getTokenId();
					GameInfo::mUserInfo.mAvatarID = pLoginResponse->getAvatarId();
					GameInfo::mUserInfo.mCPID = EGJniHelper::getCPID();
					GameInfo::mUserInfo.mDeviceID = EGJniHelper::getDeviceID();
					GameInfo::mUserInfo.isBonus = pLoginResponse->getInt(MP_PARAM_BONUS);
					GameInfo::mUserInfo.isCanEnableSms = pLoginResponse->getInt(MP_PARAM_ENABLE_SMS);
					std::vector<MpLoginResponse::GameAddress> lstGameAdress = pLoginResponse->getGameAddress();
					GameInfo::mUserInfo.lstGameDomain = lstGameAdress;
					pLoginResponse->getListByTagRate(MP_PARAM_DAME_GUN, GameInfo::DAME_GUN);
					pLoginResponse->getListByTagRate(MP_PARAM_DAME_GROW_BY_GUN, GameInfo::DAME_GROW_BY_GUN);
					pLoginResponse->getListByTagRate(MP_PARAM_DAME_GROW_BY_FISH, GameInfo::DAME_GROW_BY_FISH);
					GameInfo::DAME_GROW_BY_FISH.insert(GameInfo::DAME_GROW_BY_FISH.begin(), 0);
					pLoginResponse->getListByTagRate(MP_PARAM_GROW_RATE_DEAD_BY_GUN, GameInfo::GROW_RATE_DEAD_BY_GUN);
					pLoginResponse->getListByTagRate(MP_PARAM_GROW_RATE_DEAD, GameInfo::GROW_RATE_DEAD);
					GameInfo::GROW_RATE_DEAD.insert(GameInfo::GROW_RATE_DEAD.begin(), 0);
					pLoginResponse->getListByTagRate(MP_PARAM_FISH_RATE_DEAD, GameInfo::FISH_RATE_DEAD);
					GameInfo::FISH_RATE_DEAD.insert(GameInfo::FISH_RATE_DEAD.begin(), 0);
					// reconnect only

					GameInfo::mUserInfo.mServiceNumber = "";
					GameInfo::mUserInfo.mSyntax = "";
					GameInfo::mUserInfo.mChargeID = atoi(EGJniHelper::getRechargeID());


					uint32_t tableID = pLoginResponse->getInt(MP_PARAM_TABLE_ID);
					if (tableID != 0)
					{
						GameInfo::mUserInfo.mTableID = tableID;
						uint8_t index = pLoginResponse->getInt(MP_PARAM_SEVER_ID);
						if (index < lstGameAdress.size()) {
							GameInfo::mUserInfo.domainReconect = lstGameAdress.at(index);
							GameInfo::mUserInfo.mGameID = GameInfo::mUserInfo.domainReconect.gameId;
						}
						mErrorCode_HomeScene = "RECONECT";
					}
					else 
					{
						mErrorCode_HomeScene = TEXT_SUCCESS_LOGIN;
					}
				}
			}
			break;
			case MP_MSG_RESET_PASSWORD_ACK:
				executeRequestOTP(_mess);
				break;
			case MP_MSG_RESET_PASSWORD_VERIFY_ACK:
				executeResetPass(_mess);
				break;

		default:
			break;
		}
		delete _mess;
		_mess = NULL;
	}
	if (getErrorCode() != "") {
		mErrorCode_HomeScene = getErrorCode();
		setErrorCode("");
	}
	if (mErrorCode_HomeScene != "") {
		if (mErrorCode_HomeScene == TEXT_SUCCESS_REGISTER) {
			mBtn_BoxCheck->setVisible(true);
			//mBtn_ForgetPass->setVisible(true);

			mEdt_RePasswords->setVisible(false);

			mBtn_Skip->setVisible(false);
			mBtn_Login->setVisible(true);

			mEdt_RePasswords->setText("");
		}
		else if (mErrorCode_HomeScene == TEXT_SUCCESS_LOGIN) {
			this->unscheduleAllSelectors();
			unscheduleUpdate();
			mErrorCode_HomeScene = "";
			if (!GameInfo::mUserInfo.isReConnect) {
				Scene* pScene = MainScene::scene(0);
				TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
				Director::getInstance()->replaceScene(pTrans);
			}
			else 
			{
				Scene* pScene = ChooseRoomScene::createScene(GameInfo::mUserInfo.mGameID);
				TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
				Director::getInstance()->replaceScene(pScene);
			}
			return;
		}
		else if (mErrorCode_HomeScene == TEXT_ACTIVE_REQUIRE) {
			this->unscheduleAllSelectors();
			unscheduleUpdate();
			mErrorCode_HomeScene = "";
			Scene* pScene = MainScene::scene(1);
			TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
			Director::getInstance()->replaceScene(pTrans);
			return;
		}
		else if (mErrorCode_HomeScene == TEXT_NEW_UPDATEINSERVER) {
			BoxNoticeManager::getBoxConfirm(mBoxNotice,
				mXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				mXml->getNodeTextByTagName(mErrorCode_HomeScene.c_str()).c_str(),
				[=] {
				EGJniHelper::openLink(GameInfo::mUserInfo.mUrlUpdate.c_str());
				EGJniHelper::showDialogQuit();
			});
			mBoxNotice->setVisible(true);
			mErrorCode_HomeScene = "";

			return;
		}
		else if (mErrorCode_HomeScene == "102263" || mErrorCode_HomeScene == "124263") {
			this->unscheduleAllSelectors();
			if (!GameInfo::mUserInfo.isReConnect) {
				BoxNoticeManager::getBoxConfirm(mBoxNotice, mXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
					mXml->getNodeTextByTagName(mErrorCode_HomeScene), [=] {
					EGJniHelper::openLink(mErrorDescription.c_str());
				}, [=] {
					Scene* pScene = MainScene::scene(0);
					TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
					Director::getInstance()->replaceScene(pTrans);
				});
				mBoxNotice->setVisible(true);
			}
			else {
				Scene* pScene = ChooseRoomScene::createScene(GameInfo::mUserInfo.mGameID);
				TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
				Director::getInstance()->replaceScene(pTrans);
			}
			return;
		}
		else if (mErrorCode_HomeScene == "RECONECT") {
			GameInfo::mUserInfo.isReConnect = true;
			pthread_loadBetMoneyHome(NULL);
			mErrorCode_HomeScene = "";
		}
		else if (mErrorCode_HomeScene == TEXT_GETBETMONEY_SUCCESS) 
		{
			unscheduleAllSelectors();
			Scene* pScene = ChooseRoomScene::createScene(GameInfo::mUserInfo.mGameID);
			TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
			Director::getInstance()->replaceScene(pTrans);
			mErrorCode_HomeScene = "";
		}
		if (mErrorCode_HomeScene != "") {
			BoxNoticeManager::getBoxNotice(mBoxNotice,
				mXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				mXml->getNodeTextByTagName(mErrorCode_HomeScene.c_str()).c_str());
			mBoxNotice->setVisible(true);
		}
		mErrorCode_HomeScene = "";
	}
	if (mErrorCode_ReConnect != "") {
		Scene* _scene = HomeScene::scene();
		Director::getInstance()->replaceScene(_scene);
	}
}

void HomeScene::reconect() {
	schedule(schedule_selector(HomeScene::updateJoinTable), 0);
	pthread_t t1;
	pthread_create(&t1, NULL, &pthread_reconect, mStrucReconect);
}

void* pthread_reconect(void* _void)
{
	//roomStruct* pIDRoom = (roomStruct*)_void;
	//int pConnection = pIDRoom->mTcp->connectToServer(pIDRoom->mTcp->getIP().c_str(), pIDRoom->mTcp->getPort());
	//if (pConnection == 0) {
	//	GameInfo::mUserInfo.mLstTcpTimeout.push_back(&pIDRoom->mTcp);
	//	MpJoinTableMessageRequest* pRequest = new MpJoinTableMessageRequest();
	//	pRequest->setTokenId(GameInfo::mUserInfo.mTokenID.c_str());
	//	pRequest->setRoomId(pIDRoom->mIDRoom);
	//	pRequest->setGameId(pIDRoom->mGameID);
	//	pRequest->setTableId(pIDRoom->mTableID);
	//	pRequest->setGameLevel(atoi(pIDRoom->pRoomLevelstr.c_str()));
	//	//pIDRoom->mTcp->sendMsg(pRequest->getMsg());

	//	delete pRequest;
	//	pRequest = NULL;

	//	pIDRoom->mTcp->startListener();
	//	pIDRoom->mTcp->receiveMessageHandler();

	//	pthread_mutex_lock(&GameInfo::mUserInfo.pthreadlock);
	//	if (pIDRoom->mTcp){
	//		if (pIDRoom->mTcp->getReasonKill() == KILL_BY_SEVER || pIDRoom->mTcp->getReasonKill() == KILL_BY_TIME_OUT) {
	//			mErrorCode_ReConnect = "fail_connect";
	//		}
	//		delete pIDRoom->mTcp;
	//		pIDRoom->mTcp = NULL;
	//	}
	//	pthread_mutex_unlock(&GameInfo::mUserInfo.pthreadlock);


	//}
	//else {
	//	delete pIDRoom->mTcp;
	//	pIDRoom->mTcp = NULL;

	//	mErrorCode_HomeScene = TEXT_ERR_NOCONNECTION;
	//	pIDRoom->pLoading->hide();
	//	delete pIDRoom;
	//}

	pthread_exit(NULL);
	return NULL;
}

#define MP_NONE 99999999

void HomeScene::updateJoinTable(float pSec) {
	//MsgHeader* pMsg = NULL;
	//bool isSuccess = false;
	////pthread_mutex_lock(&GameInfo::mUserInfo.pthreadlock);
	////if (mStrucReconect->mTcp) {
	////	isSuccess = mStrucReconect->mTcp->getMsgAutoResponse(pMsg);
	////}

	//pthread_mutex_unlock(&GameInfo::mUserInfo.pthreadlock);
	//if (isSuccess && (!pMsg || pMsg->msgType == MP_NONE)) {
	//	//mErrorCode_ReConnect = "fail_reconnect";
	//}
	//else if (pMsg && pMsg->msgType != MP_NONE)
	//{
	//	MpMessageResponse * response = new MpMessageResponse(*pMsg);
	//	if (response->getType() == MP_MSG_JOIN_TABLE_ACK) 
	//	{
	//		pthread_mutex_lock(&GameInfo::mUserInfo.pthreadlock);
	//		for (int i = 0; i < GameInfo::mUserInfo.mLstTcpTimeout.size(); i++) {
	//			if (mStrucReconect->mTcp == (*GameInfo::mUserInfo.mLstTcpTimeout.at(i))) {
	//				GameInfo::mUserInfo.mLstTcpTimeout.erase(GameInfo::mUserInfo.mLstTcpTimeout.begin() + i);
	//			}
	//		}
	//		pthread_mutex_unlock(&GameInfo::mUserInfo.pthreadlock);
	//		MpJoinTableMessageResponse *pResponse = (MpJoinTableMessageResponse*)response;
	//		FreeMsg(pMsg);
	//		pMsg = NULL;
	//		unsigned  pErrorCode = pResponse->getErrorCode();
	//		if (pErrorCode != 0) {
	//			//CCLOG(__String::createWithFormat("code code %d", pErrorCode)->getCString());
	//			mErrorCode_HomeScene = __String::createWithFormat("%d", pErrorCode)->getCString();
	//			mStrucReconect->pLoading->hide();
	//		}
	//		else 
	//		{
	//			pthread_mutex_lock(&GameInfo::mUserInfo.pthreadlock);
	//			if (mStrucReconect->mTcp) {
	//				mStrucReconect->mTcp->start();
	//			}
	//			else {
	//				mErrorCode_ReConnect = "fail_reconnect";
	//				return;
	//			}
	//			pthread_mutex_unlock(&GameInfo::mUserInfo.pthreadlock);
	//			std::vector<ClientPlayerInfo> pListPlayer;
	//			std::vector<ReadyPlayer> pListReady;
	//			pResponse->getListReadyPlayer(pListReady);
	//			pResponse->getListPlayer(pListPlayer);
	//			pResponse->getListItem(mStrucReconect->mListItemSp);
	//			pResponse->getListFishPrice(mStrucReconect->mListPrice);
	//			pResponse->getDefaultBetMoney(mStrucReconect->pBetMoneyDefault);
	//			std::string data;
	//			pResponse->getString(MP_PARAM_FISH_MONEY, data);

	//			mStrucReconect->mDurationReadyHost = 30;
	//			mStrucReconect->mDurationReadyNormal = 30;
	//			uint16_t pDuration;
	//			pResponse->getGameDuration(pDuration);
	//			mStrucReconect->mDuration = pDuration;
	//			mStrucReconect->pMaxPlayer = 4;
	//			unsigned int pMinBetMoney;
	//			pResponse->getMinBetMoney(pMinBetMoney);
	//			if (mStrucReconect->mListBetMoney &&mStrucReconect->mListBetMoney->size() > 0) {
	//				mStrucReconect->mListBetMoney->clear();
	//			}
	//			for (int i = 0; i < 3; i++) {
	//				mStrucReconect->mListBetMoney->push_back(pMinBetMoney * LIST_BET_RATE[i]);
	//			}
	//			mStrucReconect->pMinMoney = pMinBetMoney;
	//			mStrucReconect->mRatio = pResponse->getRatioLevel();
	//			mStrucReconect->mBackGroundID = pResponse->getBackgroundId();
	//			int len = 0;
	//			for (int i = 0; i < pListPlayer.size(); i++) {

	//				int index = i;
	//				ClientPlayerInfo playerInfo = pListPlayer.at(index);
	//				ClientPlayerInfoEX playerInfoGame;
	//				playerInfoGame.balance = playerInfo.balance;
	//				playerInfoGame.balanceDefault = playerInfo.balance;
	//				playerInfoGame.host = playerInfo.host;
	//				playerInfoGame.level = playerInfo.level;
	//				playerInfoGame.score = *((uint32_t*)(data.data() + len));
	//				playerInfoGame.score = ntohl(playerInfoGame.score);
	//				len += 4;
	//				playerInfoGame.position = playerInfo.position;
	//				playerInfoGame.username = playerInfo.username;
	//				playerInfoGame.disallowKick = playerInfo.disallowKick;
	//				for (int j = 0; j < pListReady.size(); j++) 
	//				{
	//					ReadyPlayer  readyPlayer = pListReady.at(j);
	//					if (readyPlayer.position == playerInfoGame.position) {
	//						playerInfoGame.isReady = readyPlayer.status;
	//						break;
	//					}
	//				}
	//				playerInfoGame.itemId = playerInfo.itemId;
	//				if (playerInfo.itemId == 0) {
	//					playerInfoGame.itemId = 101;
	//				}
	//				mStrucReconect->mListPlayer->push_back(playerInfoGame);
	//			}
	//			std::string strListFactor = pResponse->getString(MP_PARAM_LIST_XFACTOR, strListFactor);
	//			for (int i = 0; i < strListFactor.length(); i++) {
	//				mStrucReconect->mListXFactor.push_back(strListFactor[i]);
	//			}
	//			std::string strXFactor = pResponse->getString(MP_PARAM_XFACTOR, strXFactor);
	//			if (strXFactor.length() == mStrucReconect->mListPlayer->size()) {
	//				for (size_t i = 0; i < mStrucReconect->mListPlayer->size(); i++) {
	//					mStrucReconect->mListPlayer->at(i).indexX = strXFactor[i];
	//				}
	//			}
	//			pResponse = NULL;
	//			createGame();
	//			mErrorCode_HomeScene = "";

	//		}

	//		//this->unschedule(schedule_selector(ChooseRoomScene::updateJoinTable));
	//	}
	//}
	//if (pMsg) {
	//	FreeMsg(pMsg);
	//	pMsg = NULL;
	//}
}
void HomeScene::createGame() 
{
	GameInfo::mUserInfo.mTabChoose = mStrucReconect->mIDRoom;
	GameInfo::mUserInfo.mLevelRoom = atoi(mStrucReconect->pRoomLevelstr.c_str());
	Scene *_scene = GameScene::createScene(mStrucReconect);
	Director::getInstance()->replaceScene(_scene);
}

void HomeScene::updateBubble(float dt)
{
	auto bubble = EGSprite::createWithFile("img_bubble.png");
	uint32_t posX = rand() % 800;
	bubble->setPosition(Vec2(posX, rand() % 200 - 30));
	bubble->setScale((rand() % 6 + 2)*0.1f);
	mBackground->addChild(bubble, rand()%4);
	bubble->runAction(Sequence::createWithTwoActions(MoveTo::create(rand() % 5 + 2, Vec2(posX, 550)), CallFunc::create([=]{bubble->removeFromParentAndCleanup(true); })));
}

void HomeScene::requestDetectSub(){
	HttpRequest* request = new (std::nothrow) HttpRequest();
	request->setUrl("http://mps.natcom.com.ht:9181/MPS/mobile.html?PRO=MOCOM&SER=FISHING_MONEY&SUB=FISHING_MONEY_DAILY&DATA=urlencode($data_encrypted)&SIG=$signature");
	request->setRequestType(HttpRequest::Type::GET);
	request->setResponseCallback(CC_CALLBACK_2(HomeScene::requestDectectSubComplete, this));
	HttpClient::getInstance()->sendImmediate(request);
	request->release();
}

void HomeScene::requestDectectSubComplete(cocos2d::network::HttpClient* sender, cocos2d::network::HttpResponse* response){
	if (!response)
		return;

	if (response->isSucceed()) {
		// Dump the data
		std::vector<char>* buffer = response->getResponseData();
		std::string strResult(buffer->begin(), buffer->end());
		// do something
	}
	else {
		// login not by detect subscriber
	}

}

void HomeScene::requestGetOtp(std::string phone){
	MpMessageRequest* request = new MpMessageRequest(MP_MSG_RESET_PASSWORD_REQ);
	request->addString(MP_PARAM_TAG_1, phone);
	MpClientManager::getInstance()->sendMessage(request, true);
}

void HomeScene::sendOtpToResetPass(std::string phone, std::string otp){
	MpMessageRequest* request = new MpMessageRequest(MP_MSG_RESET_PASSWORD_VERIFY_REQ);
	//request->setTokenId(GameInfo::mUserInfo.mTokenID);
	request->addString(MP_PARAM_TAG_1, phone);
	request->addString(MP_PARAM_TAG_2, otp);
	request->addString(MP_PARAM_TAG_3, _trandId);
	MpClientManager::getInstance()->sendMessage(request, true);
}

void HomeScene::resendOTP(){
	MpMessageRequest* request = new MpMessageRequest(MP_MSG_ACTIVE_ACCOUNT_REQ);
	request->setTokenId(GameInfo::mUserInfo.mTokenID);
	MpClientManager::getInstance()->sendMessage(request, true);
}

void HomeScene::executeResetPass(MpMessage* pMsg){
	MpMessage* msg = pMsg;
	uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);
	boxResetPass->showDescription("");

	if (pErrorCode != 0) {
		std::string des = msg->getString(MP_PARAM_ERROR_DESCRIPTION);
		string content = "";
		if (des.length() == 0)
			content = __String::createWithFormat("%d", pErrorCode)->getCString();
		else
			content = des;
		boxResetPass->showDescription(content);
	}
	else {

		WXmlReader* pXml = WXmlReader::create();
		pXml->load(KEY_XML_FILE);
		boxResetPass->showDescription(pXml->getNodeTextByTagName("txt_box_repass_success").c_str());
		boxResetPass->ChangePassSuccess();
		/*BoxNoticeManager::getBoxNotice(mBoxNotice,
			"Notifikasyon",
			pXml->getNodeTextByTagName("txt_box_repass_success").c_str(), 
			[=]{
				boxResetPass->setVisible(false);
				boxResetPass->setPosition(Vec2(0, 1000));
			});*/
	}
}

void HomeScene::executeResendOTP(MpMessage* pMsg){
	MpMessage* msg = pMsg;
	uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);
	boxResetPass->showDescription("");
	if (pErrorCode != 0) {
		std::string des = msg->getString(MP_PARAM_ERROR_DESCRIPTION);
		string content = "";
		if (des.length() == 0)
			content = __String::createWithFormat("%d", pErrorCode)->getCString();
		else
			content = des;
		boxResetPass->showDescription(content);
	}
	else {
		//show notice resend
		WXmlReader* pXml = WXmlReader::create();
		pXml->load(KEY_XML_FILE);
		boxResetPass->showDescription(pXml->getNodeTextByTagName("txt_resend_otp_success").c_str());
	}
}

void HomeScene::executeRequestOTP(MpMessage* pMsg){
	boxResetPass->showDescription("");
	MpMessage* msg = pMsg;
	uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);

	if (pErrorCode != 0) {
		std::string des = msg->getString(MP_PARAM_ERROR_DESCRIPTION);
		string content = "";
		if (des.length() == 0)
			content = __String::createWithFormat("%d", pErrorCode)->getCString();
		else
			content = des;
		boxResetPass->showDescription(content);
	}
	else {
		//show notice resend
		WXmlReader* pXml = WXmlReader::create();
		pXml->load(KEY_XML_FILE);
		boxResetPass->showDescription(pXml->getNodeTextByTagName("txt_resend_otp_success").c_str());

		_trandId = msg->getString(MP_PARAM_TAG_TRANSID);
		boxResetPass->showOtpConfirm();
	}
}