#include "MainScene.h"
#include "GameInfo.h"
#include "FriendScene.h"
#include "TopScene.h"
#include "ShopScene.h"
#include "BonusScene.h"
#include "EGSupport.h"
#include "HomeScene.h"
#include "BoxNoticeManager.h"
#include "MessageType.h"
#include "ChooseRoomScene.h"
#include "mprechargebycardmessagerequest.h"
#include "mpgetinfoplayergameresponse.h"

#include "ClientPlayerInfo.h"
#include "mpgetinfopurchasemessagerequest.h"
#include "mpgetinfopurchasemessageresponse.h"
#include "mploadsubscriptioninfo.h"
#include "mprechargebycardmessagerequest.h"
#include "mprechargebycardmessageresponse.h"
#include "pthread_mobi.h"
#include "message/mpgetinfosmsmessagerequest.h"
#include "message/mpgetinfosmsmessageresponse.h"
#include "md5.h"

#include "EGJniHelper.h"
#include "WXmlReader.h"
#include "scene/MailScene.h"
#include "object/BoxActiveAccount.h"
#include "object/BoxAds.h"

#define CREATE_TID(g,l,t) (((g)<<24) | ((l) << 16) | ((t) & 0xff)) // for client

static std::vector<uint32_t> vtBetMoney;

std::string mNewPassword = "";
std::string mErrorCode_MainScene;
std::string mErrorCode_UpdateAcount;
ClientPlayerInfoEX* mPlayerInfoMyself;
std::vector<StructTelcoInfo> lstTelcoInfo;
int mType;

struct RechargeStruct {
public:
	std::string mTelco, mSerrial, mNumber, mUsername;
	Loading* mLoading;
};

std::vector<uint32_t>& MainScene::getVectorMoney(){
	return vtBetMoney;
}
void MainScene::setVectorMoney(std::vector<uint32_t> pVector){
	vtBetMoney = pVector;
}

struct AccountStruct {
public:
	std::string pUsername;
	std::string pPasswords;
	std::string pNewPasswords;
	Loading* mLoading;
};
Scene* MainScene::scene(int _type) {
	mType = _type;
	MainScene* pScene = new MainScene();
	pScene->registerBackPressed();
	pScene->initScene(false);
	pScene->autorelease();
	
	return pScene;
}

Scene* MainScene::sceneWithRecharge() {
	MainScene* pScene = new MainScene();
	pScene->registerBackPressed();
	pScene->initScene(false);
	pScene->autorelease();
	pScene->mBtnRecharge->performClick();

	return pScene;
}

Scene* MainScene::sceneWithUpdateNotice() {
	MainScene* pScene = new MainScene();
	pScene->registerBackPressed();
	pScene->initScene(true);
	pScene->autorelease();

	return pScene;
}

void MainScene::sendChangePass(std::string pOldPass, std::string pNewPass, std::string pReNewPass){

	AccountStruct* pInfo = new AccountStruct();
	std::string pUsername =GameInfo::mUserInfo.mUsername;
	std::string pPasswords = pNewPass;
	std::string pRePasswords = pReNewPass;
	const char* pCPID = EGJniHelper::getCPID();
	const char* pEmail = "M2W_DEMO_EMAIL@gmail.com";
	if (pOldPass.length() < REQ_MINLENGHT_PASSWORDS ||
		pOldPass.length() > REQ_MAXLENGHT_PASSWORDS){
		mErrorCode_MainScene = TEXT_ERR_PASS_INVALID;
		return;
	}
	 if (pPasswords != pRePasswords) {
		mErrorCode_MainScene = TEXT_PASS_NOTSAME_INVALID;
		return;
	}
	else if (pPasswords.length() < REQ_MINLENGHT_PASSWORDS ||
		pPasswords.length() > REQ_MAXLENGHT_PASSWORDS)
	{
		mErrorCode_MainScene = TEXT_ERR_PASS_INVALID;
		return;
	}
	pInfo->pNewPasswords = pNewPass;
	mNewPassword = pNewPass;
	pInfo->pPasswords = pOldPass;
	pInfo->mLoading = mLoading;
	mLoading->show();
	pUsername = "";
	if (pInfo->pUsername != ""){
		MpMessageRequest* request = new MpMessageRequest(MP_CHANGE_ACC_INFO);
		request->setTokenId(GameInfo::mUserInfo.mTokenID);
		pInfo->pPasswords = md5(pInfo->pPasswords);
		request->addString(MP_PARAM_USERNAME, pInfo->pUsername.data());
		request->addString(MP_PARAM_PASSWORD, pInfo->pPasswords.data());
		//mTcp->sendMsg(request->getMsg());
		MpClientManager::getInstance()->sendMessage(request,true);
	}
	else{
		MpMessageRequest* request = new MpMessageRequest(MP_MSG_CHANGE_PASSWORD);
		request->setTokenId(GameInfo::mUserInfo.mTokenID);
		std::string pPassWord = md5(pInfo->pPasswords);
		std::string pNewPasswordMd5 = md5(pInfo->pNewPasswords);
		request->addString(MP_PARAM_PASSWORD, pPassWord);
		request->addString(MP_PARAM_NEW_PASSWORD, pNewPasswordMd5);
		//mTcp->sendMsg(request->getMsg());
		MpClientManager::getInstance()->sendMessage(request,true);
	}

}


void MainScene::excuteChangeInfo(MpMessage* pMsg){
	mLoading->hide();
	MpMessageResponse* pResponse = (MpMessageResponse*)pMsg;
	unsigned pErrorCode = pResponse->getErrorCode();
	if (pErrorCode != 0){
		mErrorCode_MainScene = __String::createWithFormat("%d", pErrorCode)->getCString();
		mErrorCode_UpdateAcount = "1";

	}
	else{
	
		if (pMsg->getType() == MP_MSG_CHANGE_PASSWORD_ACK){
			GameInfo::mUserInfo.mPassWord = mNewPassword;
			mErrorCode_MainScene = TEXT_CHANGE_PASSWORD_SUCCES;
		}
		else{
			mErrorCode_MainScene = TEXT_CHANGE_ACCOUNT_INFO_SUCCES;
		}
	}

}
void MainScene::showInfo(std::string pUserName){
	mBoxInfoGame->showBox(NULL, NULL);
	mPlayerInfoMyself = new ClientPlayerInfoEX();
	
	MpMessageRequest* request = new MpMessageRequest(MP_MSG_GET_ACC_INFO);
	request->setTokenId(GameInfo::mUserInfo.mTokenID);
	for (int i = 0; i < pUserName.length(); i++){
		pUserName[i] = std::tolower(pUserName[i]);
	}
	request->addString(MP_PARAM_USERNAME, pUserName);
	MpClientManager::getInstance()->sendMessage(request, true);
	mLoading->show();
}
void MainScene::excuteGetInfoMyselft(MpMessage* pMsg)
{
	MpGetInfoPlayerGameResponse  *pResponse =(MpGetInfoPlayerGameResponse*)pMsg;
	unsigned pErrorCode = pResponse->getErrorCode();
	if (pErrorCode != 0){
		mErrorCode_MainScene = __String::createWithFormat("%d", pErrorCode)->getCString();

	}
	else{
		pResponse->getPlayerInfo(*mPlayerInfoMyself);
		mErrorCode_MainScene = TEXT_GETINFO_SUCCES;
	}
}

void MainScene::initScene(bool isNotice)
{
	MpClientManager::getInstance(2)->close();
	MpClientManager::getInstance(1)->close();
	MpClientManager::getInstance(0);
	GameInfo::mUserInfo.stateScene = 1;
	GameInfo::mUserInfo.mIsKeepAlive = true;
	mErrorCode_MainScene = "";
	mErrorCode_UpdateAcount = "";
	GameInfo::mUserInfo.mTabChoose = 0;
	GameInfo::mUserInfo.mLevelRoom = -1;
	mBackground = Sprite::create("background_choosetype.png");
	mBackground->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBackground);


	mBtnPlayRank = EGButtonSprite::createWithFile("kv_jweanliy_button.png", EGButton2FrameDark);
	//mBtnPlayRank->unregisterTouch();
	mBtnPlayRank->setOnTouch([=]{
		mLoading->show();
		vtBetMoney.clear();
		GameInfo::mUserInfo.mGameID = 1;
		std::string ip;
		int port;
		for (int i = 0; i < GameInfo::mUserInfo.lstGameDomain.size(); i++){
			if (GameInfo::mUserInfo.lstGameDomain.at(i).gameId == 1){
				ip = GameInfo::mUserInfo.lstGameDomain.at(i).domain;
				port = GameInfo::mUserInfo.lstGameDomain.at(i).port;
			}
		}
		//MpClientManager::getInstance()->close();
		MpClientManager::getInstance(2)->setMpClientEnable(true);
		MpClientManager::getInstance(2)->connect(ip.c_str(), port, true);
		MpMessage *message = new MpMessage(MP_MSG_GET_LIST_BETMONEY);
		message->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
		MpClientManager::getInstance()->sendMessage(message, true);
	});
	mBtnPlayRank->setPosition(Vec2(CLIENT_WIDTH - mBtnPlayRank->getContentSize().width / 2 - 50, 60 + mBtnPlayRank->getContentSize().height / 2));
	mBackground->addChild(mBtnPlayRank);

	mBtnAccInfo = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_INFO, EGButton2FrameDark);
	mBtnAccInfo->setPosition(Vec2(mBtnAccInfo->getContentSize().width / 2, mBtnAccInfo->getContentSize().height / 2));
	mBackground->addChild(mBtnAccInfo);
	mBtnAccInfo->setButtonType(EGButton2FrameDark);
	mBtnAccInfo->setOnTouch(CC_CALLBACK_0(MainScene::showInfo, this, GameInfo::mUserInfo.mUsername));

	mBtnRecharge = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_RECHARGE, EGButton2FrameDark);
	mBtnRecharge->setOnTouch(CC_CALLBACK_0(MainScene::showTopUp, this));
	mBtnRecharge->setPosition(Vec2(mBtnAccInfo->getPositionX2() + mBtnRecharge->getContentSize().width / 2, mBtnAccInfo->getPositionY()));
	mBackground->addChild(mBtnRecharge);

	mBtnTop = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_TOP, EGButton2FrameDark);
	mBtnTop->setOnTouch(CC_CALLBACK_0(MainScene::gotoTopScene, this));
	mBtnTop->setPosition(Vec2(mBtnRecharge->getPositionX2() + mBtnTop->getContentSize().width / 2, mBtnAccInfo->getPositionY()));
	mBackground->addChild(mBtnTop);

	mBtnBonus = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_BONUS, EGButton2FrameDark);
	mBtnBonus->setOnTouch(CC_CALLBACK_0(MainScene::showBonus, this));
	mBtnBonus->setPosition(Vec2(mBtnTop->getPositionX2() + mBtnBonus->getContentSize().width / 2, mBtnAccInfo->getPositionY()));
	mBackground->addChild(mBtnBonus);

	mBtnShop = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_SHOP, EGButton2FrameDark);
	mBtnShop->setOnTouch(CC_CALLBACK_0(MainScene::gotoShopScene, this));
	mBtnShop->setPosition(Vec2(mBtnBonus->getPositionX2() + mBtnShop->getContentSize().width / 2, mBtnAccInfo->getPositionY()));
	mBackground->addChild(mBtnShop);

	mBtnMore = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_MORE, EGButton2FrameDark);
	mBtnMore->setOnTouch(CC_CALLBACK_0(MainScene::showMore, this));
	mBtnMore->setPosition(Vec2(mBtnShop->getPositionX2() + mBtnMore->getContentSize().width / 2, mBtnAccInfo->getPositionY()));
	mBackground->addChild(mBtnMore, 2);

	mBtnFriend = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_FRIEND, EGButton2FrameDark);
	mBtnFriend->setOnTouch(CC_CALLBACK_0(MainScene::gotoFriendScene, this));
	mBtnFriend->unregisterTouch();
	mBtnFriend->setPosition(mBtnMore->getPosition());
	mBackground->addChild(mBtnFriend);

	mBtnInvetory = EGButtonSprite::createWithSpriteFrameName("btn_inventory.png", EGButton2FrameDark);
	mBtnInvetory->setOnTouch(CC_CALLBACK_0(MainScene::gotoInventory, this));
	mBtnInvetory->unregisterTouch();
	mBtnInvetory->runAction(Hide::create());
	mBtnInvetory->setPosition(mBtnMore->getPosition());
	mBackground->addChild(mBtnInvetory);

	mBtnNews = EGButtonSprite::createWithSpriteFrameName("btn_news.png", EGButton2FrameDark);
	mBtnNews->setVisible(false);
	mBtnNews->setOnTouch(CC_CALLBACK_0(MainScene::gotoNews, this));
	mBtnNews->unregisterTouch();
	mBtnNews->setPosition(mBtnMore->getPosition());
	mBackground->addChild(mBtnNews);

	mBtnMail = EGButtonSprite::createWithFile("btn_mail_main.png", EGButton2FrameDark);
	mBtnMail->setOnTouch(CC_CALLBACK_0(MainScene::gotoMailScene, this));
	mBtnMail->unregisterTouch();
	mBtnMail->setPosition(mBtnMore->getPosition());
	mBackground->addChild(mBtnMail);

	// * -------------- VIEW ACCOUNT INFO ( Avatar - Name - Balance - Level(?) ) --------------------------------
	mSpriteAvatar = Sprite::createWithSpriteFrameName(__String::createWithFormat("avatar (%d).png", GameInfo::mUserInfo.mAvatarID)->getCString());
	mSpriteAvatar->setPosition(Vec2(mSpriteAvatar->getContentSize().width / 2 + 5, mBtnAccInfo->getContentSize().height / 2));
	mBtnAccInfo->addChild(mSpriteAvatar);

	mLblName = Label::createWithBMFont(FONT_BMF_NAME, GameInfo::mUserInfo.mFullname);
	mLblName->setPosition(Vec2(mSpriteAvatar->getContentSize().width / 2 + mSpriteAvatar->getPositionX()
		+ 5 + mLblName->getContentSize().width*mLblName->getScaleX() / 2, mSpriteAvatar->getContentSize().height / 2 + mSpriteAvatar->getPositionY()
		- mLblName->getContentSize().height*mLblName->getScaleY() / 2 - 10));
	mBtnAccInfo->addChild(mLblName);

	std::string pMoney = __String::createWithFormat("%ld", GameInfo::mUserInfo.mBalance)->getCString();
	pMoney = EGSupport::addDotMoney(pMoney);
	pMoney += " xu";
	mLblBalance = Label::createWithBMFont(FONT_BMF_MONEY, pMoney);
	mLblBalance->setPosition(Vec2(mSpriteAvatar->getContentSize().width / 2 + mSpriteAvatar->getPositionX()
		+ 5 + mLblBalance->getContentSize().width*mLblBalance->getScaleX() / 2, -mSpriteAvatar->getContentSize().height / 2 + mSpriteAvatar->getPositionY()
		+ mLblBalance->getContentSize().height*mLblBalance->getScaleY() / 2 + 5));
	mBtnAccInfo->addChild(mLblBalance);

	mTitle = Sprite::create("logo-home-off.png");
	mTitle->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT - mTitle->getContentSize().height / 2 - 20));
	mBackground->addChild(mTitle);

	mBtnPlayNormal = EGButtonSprite::createWithFile("kv_nomal_button.png", EGButton2FrameDark);
	mBtnPlayNormal->setOnTouch(CC_CALLBACK_0(MainScene::gotoNormalGame, this));
	mBtnPlayNormal->setPosition(Vec2(mBtnPlayNormal->getContentSize().width / 2 + 50, 60 + mBtnPlayNormal->getContentSize().height / 2));
	mBackground->addChild(mBtnPlayNormal);

	if (mType == 1){
		mBoxActive = BoxActiveAccount::create();
		mBoxActive->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
		mBoxActive->setVisible(true);
		this->addChild(mBoxActive, 111);
		mBoxActive->setFuncSendOTP(CC_CALLBACK_1(MainScene::sendOtpToActiveAccount, this));
		mBoxActive->setFuncResendOTP(CC_CALLBACK_0(MainScene::resendOTP, this));
		resendOTP();
	}
	else{
		auto _boxAds = BoxAds::create();
		this->addChild(_boxAds, 1000);
		_boxAds->showBoxAds();
	}

	CCLOG("SH1");

	mBtnBack = EGButtonSprite::createWithFile("button-back-cr (1).png", EGButton2FrameDark);
	mBtnBack->setOnTouch([=] {
		WXmlReader* pXml = WXmlReader::create();
		pXml->load(KEY_XML_FILE);
		BoxNoticeManager::getBoxConfirm(mBoxNotice,
			pXml->getNodeTextByTagName("title_quitgame").c_str(),
			pXml->getNodeTextByTagName("content_quitgame").c_str(),
			CC_CALLBACK_0(MainScene::logout, this));
		mBoxNotice->setVisible(true);
	});
	mBtnBack->setPosition(Vec2(CLIENT_WIDTH - mBtnBack->getContentSize().width / 2 - 10, CLIENT_HEIGHT - mBtnBack->getContentSize().height / 2 - 10));
	mBackground->addChild(mBtnBack);

	mGiftCode = EGButtonSprite::createWithSpriteFrameName("giftcode1.png", EGButton2FrameDark);
	mGiftCode->setOnTouch(CC_CALLBACK_0(MainScene::showGiftcode, this));
	mGiftCode->setPosition(Vec2(mGiftCode->getContentSize().width / 2 + 10, 100));
	mBackground->addChild(mGiftCode);
	if (GameInfo::mUserInfo.isBonus == 0){
		mGiftCode->runAction(Hide::create());
	}

	Animation* pAnimation = Animation::create();
	pAnimation->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName("giftcode1.png"));
	pAnimation->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName("giftcode2.png"));
	pAnimation->setDelayPerUnit(0.5f);
	mGiftCode->runAction(RepeatForever::create(Animate::create(pAnimation)));

	/*mBoxNew = BoxNew::create();
	this->addChild(mBoxNew);*/

	mBoxTopUp = BoxTopUp::create(CC_CALLBACK_0(MainScene::showTopUpByCard, this), CC_CALLBACK_1(MainScene::rechargeBySMS, this));
	mBoxTopUp->close();
	this->addChild(mBoxTopUp);
	mBoxTopUp->setFuncShowNotice(CC_CALLBACK_1(MainScene::showNotice, this));
	mBoxTopUp->setFuncShowLoading(CC_CALLBACK_1(MainScene::showLoading, this));
	mBoxTopUp->setFuncGetSmsInfo(CC_CALLBACK_0(MainScene::getSmsInfo, this));
	mBoxTopUp->setFuncGetSubInfo(CC_CALLBACK_0(MainScene::getSubInfo, this));
	mBoxTopUp->setFuncRecharge(CC_CALLBACK_3(MainScene::recharge, this));
	mBoxTopUp->_tab = 2;

	/*mBoxTopUpCard = BoxTopUpCard::create(CC_CALLBACK_4(MainScene::recharge, this));
	mBoxTopUpCard->setVisible(false);
	this->addChild(mBoxTopUpCard);*/

	mBoxNotice = BoxNotice::create();
	mBoxNotice->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBoxNotice, 10);
	CCLOG("SH2");
	EGButtonSprite* buttonShowUpdate = EGButtonSprite::createWithFile("bt-show-update-acount.png");
	mBackground->addChild(buttonShowUpdate);
	buttonShowUpdate->setPosition(Vec2(mBtnAccInfo->getContentSize().width - buttonShowUpdate->getContentSize().width / 2, mBtnAccInfo->getContentSize().height / 2));
	buttonShowUpdate->setButtonType(EGButton2FrameDark);
	buttonShowUpdate->setVisible(false);
	buttonShowUpdate->setOnTouch([=]{
		mEdit_Name->setText("");
		mEditPassWord1->setText("");
		mEditPassWord2->setText("");
		mLayerBoxUpdateAcount->setVisible(true);
		if (mLayerBoxUpdateAcount->getChildren().size() > 0){
			mLayerBoxUpdateAcount->getChildren().at(0)->setVisible(true);
		}
	});
	CallFunc *callUpdate = CallFunc::create([=]{
		mEdit_Name->setText("");
		mLayerBoxUpdateAcount->setVisible(true);
		if (mLayerBoxUpdateAcount->getChildren().size() > 0){
			mLayerBoxUpdateAcount->getChildren().at(0)->setVisible(true);
		}
	});
	CCLOG("userName %s ", GameInfo::mUserInfo.mUsername.c_str());
	if (GameInfo::mUserInfo.mUsername == ""){
		runAction(callUpdate);
	}

	mLayerBoxUpdateAcount = LayerColor::create(Color4B(0, 0, 0, 188));
	this->addChild(mLayerBoxUpdateAcount);
	mLayerBoxUpdateAcount->setVisible(false);
	EGSprite* boxUpdateAcount = EGSprite::createWithFile("box-update-account.png");
	boxUpdateAcount->setVisible(false);
	boxUpdateAcount->setTag(1);
	mLayerBoxUpdateAcount->addChild(boxUpdateAcount);
	boxUpdateAcount->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	boxUpdateAcount->setBindingRect(false);
	boxUpdateAcount->registerTouch(true);
	Sprite* pTitle = Sprite::create("title-box-update-account.png");
	boxUpdateAcount->addChild(pTitle);
	pTitle->setPosition(Vec2(boxUpdateAcount->getContentSize().width / 2, boxUpdateAcount->getContentSize().height));
	WXmlReader* mXml = WXmlReader::create();
	CCLOG("SH3");
	for (int i = 0; i < 1; i++){
		Scale9Sprite* pEdtSprite_Passwords = Scale9Sprite::createWithSpriteFrameName(IMG_EDITTEXT);
		EditBox* mEdt = EditBox::create(Size(312, 53), pEdtSprite_Passwords);
		mEdt->setPosition(Vec2(boxUpdateAcount->getContentSize().width / 2, 160 - i*(pEdtSprite_Passwords->getContentSize().height + 10)));
		mEdt->setFontName(FONT);
		mEdt->setFontSize(20);
		//mEdt_Passwords->setMargins(20, 20);

		mEdt->setFontColor(Color3B::WHITE);
		if (i > 0){
			mEdt->setInputFlag(EditBox::InputFlag::PASSWORD);
			mEdt->setPlaceHolder(mXml->getNodeTextByTagName("insert_passwords").c_str());
			if (i == 1){
				mEditPassWord1 = mEdt;
			}
			else{
				mEditPassWord2 = mEdt;
			}
		}
		else{
			mEdt->setInputFlag(EditBox::InputFlag::SENSITIVE);
			mEdt->setPlaceHolder(mXml->getNodeTextByTagName(TEXT_EDT_USERNAME).c_str());
			mEdit_Name = mEdt;
		}
		mEdt->setPlaceholderFontColor(Color3B::GRAY);
		mEdt->setMaxLength(REQ_MAXLENGHT_PASSWORDS);
		mEdt->setReturnType(EditBox::KeyboardReturnType::DONE);
		mEdt->setDelegate(this);
		boxUpdateAcount->addChild(mEdt);
	}
	EGButtonSprite* buttonUpdate1 = EGButtonSprite::createWithFile("bt-box-update-account1.png");
	boxUpdateAcount->addChild(buttonUpdate1);
	buttonUpdate1->setButtonType(EGButton2FrameDark);
	buttonUpdate1->setPosition(Vec2(boxUpdateAcount->getContentSize().width / 2, 72));


	//EGButtonSprite* buttonUpdate2 = EGButtonSprite::createWithFile("bt-box-update-account2.png");
	//boxUpdateAcount->addChild(buttonUpdate2);
	//buttonUpdate2->setButtonType(EGButton2FrameDark);
	//buttonUpdate2->setPosition(Vec2(boxUpdateAcount->getContentSize().width / 2 + 5 + buttonUpdate2->getContentSize().width / 2, 72));
	//buttonUpdate2->setOnTouch([=]{
	//	mLayerBoxUpdateAcount->setVisible(false);
	//	boxUpdateAcount->setVisible(false);
	//});

	mLoading = Loading::create();
	mLoading->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mLoading, 1);
	CCLOG("SH4");
	buttonUpdate1->setOnTouch([=]{
		AccountStruct* pInfo = new AccountStruct();
		std::string pUsername = mEdit_Name->getText();
		std::string pPasswords = "132456";
		std::string pRePasswords = "132456";
		const char* pCPID = EGJniHelper::getCPID();
		const char* pEmail = "M2W_DEMO_EMAIL@gmail.com";

		if (pUsername.length() < REQ_MINLENGHT_USERNAME ||
			pUsername.length() > REQ_MAXLENGHT_USERNAME)
		{
			mErrorCode_MainScene = TEXT_ERR_USERNAME_INVALID;
			return;
		}
		else if (pUsername.find_first_not_of("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890") != std::string::npos)
		{
			mErrorCode_MainScene = TEXT_ERR_USERNAME_INVALID;
			return;
		}
		else if (pUsername.substr(0, 1).find_first_not_of("01234567890") == std::string::npos) {
			mErrorCode_MainScene = TEXT_ERR_USERNAME_INVALID;
			return;
		}
		else if (pPasswords != pRePasswords) {
			mErrorCode_MainScene = TEXT_PASS_NOTSAME_INVALID;
			return;
		}
		else if (pPasswords.length() < REQ_MINLENGHT_PASSWORDS ||
			pPasswords.length() > REQ_MAXLENGHT_PASSWORDS)
		{
			mErrorCode_MainScene = TEXT_ERR_PASS_INVALID;
			return;
		}
		pInfo->mLoading = mLoading;
		mLoading->show();

		////pInfo->pPasswords = mEditPassWord1->getText();
		pInfo->pUsername = mEdit_Name->getText();
		mNewPassword = pInfo->pNewPasswords;
		if (pInfo->pUsername != ""){
			MpMessageRequest* request = new MpMessageRequest(MP_CHANGE_ACC_INFO);
			request->setTokenId(GameInfo::mUserInfo.mTokenID);
			pInfo->pPasswords = md5(pInfo->pPasswords);
			request->addString(MP_PARAM_USERNAME, pInfo->pUsername.data());
			request->addString(MP_PARAM_PASSWORD, pInfo->pPasswords.data());
			//mTcp->sendMsg(request->getMsg());
			MpClientManager::getInstance()->sendMessage(request, true);
		}
		else{
			MpMessageRequest* request = new MpMessageRequest(MP_MSG_CHANGE_PASSWORD);
			request->setTokenId(GameInfo::mUserInfo.mTokenID);
			std::string pPassWord = md5(pInfo->pPasswords);
			std::string pNewPasswordMd5 = md5(pInfo->pNewPasswords);
			request->addString(MP_PARAM_PASSWORD, pPassWord);
			request->addString(MP_PARAM_NEW_PASSWORD, pNewPasswordMd5);
			//mTcp->sendMsg(request->getMsg());
			MpClientManager::getInstance()->sendMessage(request, true);
		}
	});

	mBtnSmall = NULL;
	mBtnNormal = NULL;
	mBtnLarge = NULL;
	mBtnHuge = NULL;

	if (isNotice) {
		WXmlReader* pXml = WXmlReader::create();
		pXml->load(KEY_XML_FILE);

		BoxNoticeManager::getBoxConfirm(mBoxNotice,
			pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
			pXml->getNodeTextByTagName("102263").c_str(),
			[=] {
			EGJniHelper::openLink(GameInfo::mUserInfo.mUrlUpdate.c_str());
			//EGJniHelper::showDialogQuit();
		});
		mBoxNotice->setVisible(true);
	}
	mBoxInfoGame = BoxInfoGame::createBoxInfo();
	this->addChild(mBoxInfoGame);
	mBoxInfoGame->closeBox();
	mBoxInfoGame->setType(2);
	mBoxInfoGame->setFuncChangePass(CC_CALLBACK_3(MainScene::sendChangePass, this));
	this->scheduleUpdate();
	MpMessageRequest* pRequest = new MpMessageRequest(MP_MSG_GETBALANCE);
	pRequest->setTokenId(GameInfo::mUserInfo.mTokenID);
	MpClientManager::getInstance(0)->sendMessage(pRequest);

	//mBoxTopUp->btnCardClicked();

	
}
void MainScene::excuteGetBalance(MpMessage* pMsg)
{
	int pErrorCode = pMsg->getInt(MP_PARAM_ERROR_CODE);

	if (pErrorCode != 0) {
		mErrorCode_MainScene = __String::createWithFormat("%d", pErrorCode)->getCString();
	}
	else {
		GameInfo::mUserInfo.mBalance = pMsg->getInt(MP_PARAM_BALANCE);		 
		mErrorCode_MainScene = TEXT_BALANCE;
	}
}


void MainScene::showGiftcode() {
	WXmlReader* pXmlReader = WXmlReader::create();
	pXmlReader->load(KEY_XML_FILE);

	mBoxNotice->mEdt_Input->setMaxLength(10);
	BoxNoticeManager::getBoxInput(mBoxNotice, pXmlReader->getNodeTextByTagName("giftcode_title").c_str(), pXmlReader->getNodeTextByTagName("giftcode_content").c_str(),
		pXmlReader->getNodeTextByTagName("giftcode_holder").c_str(), "", pXmlReader->getNodeTextByTagName(TEXT_SEND).c_str(), CC_CALLBACK_0(MainScene::sendGiftcode, this));
	mBoxNotice->setVisible(true);
}


void pthread_sendGiftCode(void* _void) 
{
	/*__String* pStrGiftcode = (__String*)_void;
	std::string pGiftcode = pStrGiftcode->getCString();
		MpGiftCodeMessageRequest* pRequest = new MpGiftCodeMessageRequest();
		pRequest->setTokenId(GameInfo::mUserInfo.mTokenID);
		pRequest->setGiftCode(pGiftcode);
		MpClientManager::getInstance()->sendMessage(pRequest);*/
}

void MainScene::sendGiftcode() {
	std::string pGiftcode = mBoxNotice->mEdt_Input->getText();
	if (pGiftcode.length() != 10) {
		mErrorCode_MainScene = TEXT_GIFTCODE_FALL;
	}
	else {
		mLoading->show();
		pthread_sendGiftCode( __String::create(pGiftcode.c_str()));
	}
	mBoxNotice->setVisible(false);
}

void MainScene::refreshPlayerInfo() {
	this->stopAllActions();
	mLoading->hide();
	std::string pMoney = __String::createWithFormat("%ld", GameInfo::mUserInfo.mBalance)->getCString();
	pMoney = EGSupport::addDotMoney(pMoney);
	pMoney += " xu"; 
	mLblBalance->setString(pMoney);
	mLblBalance->setPosition(Vec2(mSpriteAvatar->getContentSize().width / 2 + mSpriteAvatar->getPositionX()
		+ 5 + mLblBalance->getContentSize().width*mLblBalance->getScaleX() / 2, -mSpriteAvatar->getContentSize().height / 2 + mSpriteAvatar->getPositionY()
		+ mLblBalance->getContentSize().height*mLblBalance->getScaleY() / 2 + 5));
}

void MainScene::showBonus() {
	if (GameInfo::mUserInfo.isBonus == 1){
		Scene* pScene = BonusScene::scene();
		TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
		Director::getInstance()->replaceScene(pTrans);
	}
	else{
		WXmlReader* pXml = WXmlReader::create();
		pXml->load(KEY_XML_FILE);
		BoxNoticeManager::getBoxNotice(mBoxNotice,
			pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
			pXml->getNodeTextByTagName("feature_comingsoon").c_str());
		mBoxNotice->setVisible(true);
	}
}

void MainScene::gotoNormalGame() {
	GameInfo::mUserInfo.mGameID = 0;
	vtBetMoney.clear();
	MpClientManager::getInstance(1);
	MpClientManager::getInstance(1)->setMpClientEnable(true);
	std::string ip;
	int port;
	for (int i = 0; i < GameInfo::mUserInfo.lstGameDomain.size(); i++)
	{
		if (GameInfo::mUserInfo.lstGameDomain.at(i).gameId == 0){
			ip = GameInfo::mUserInfo.lstGameDomain.at(i).domain;
			port = GameInfo::mUserInfo.lstGameDomain.at(i).port;
		}
	}
	GameInfo::mUserInfo.setIpNormal(ip);
	GameInfo::mUserInfo.setPortNormal(port);
	MpClientManager::getInstance(1)->connect(ip.c_str(), port, false);
	Scene* _scene = ChooseRoomScene::createScene(0);
	Director::getInstance()->replaceScene(_scene);

	//add new
	/*GameInfo::mUserInfo.mGameID = 0;
	GameInfo::mUserInfo.mLevelRoom = 0;
	if (!mLoading->isVisible()) {
		mLoading->show();
		roomStruct * mInfoRoom;
		mLoading->setSize(Size(CLIENT_WIDTH, CLIENT_HEIGHT));
		mLoading->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
		uint8_t tableid = CREATE_TID(0, 0, 0xff);
		if (GameInfo::mUserInfo.isReConnect) {
			GameInfo::mUserInfo.isReConnect = false;
			tableid = 0;
		}
		MpMessageRequest* pRequest = new MpMessageRequest(MP_MSG_JOIN_TABLE);
		std::string pToken = GameInfo::mUserInfo.mTokenID;
		pRequest->setTokenId(pToken.c_str());
		pRequest->addInt(MP_PARAM_TABLE_ID, tableid);
		MpClientManager::getInstance(1)->sendMessage(pRequest, true);
	}*/

}

void MainScene::gotoRankGame() {
	GameInfo::mUserInfo.mGameID = 1;
	GameInfo::mUserInfo.mLevelRoom = 1;
	Scene* _scene = ChooseRoomScene::createScene(1);
	Director::getInstance()->replaceScene(_scene);
	((ChooseRoomScene*)_scene)->updateListBetMoney(vtBetMoney);
}

void MainScene::logout() {
	EGJniHelper::logOutFacebook();
	Scene* pScene = HomeScene::scene();
	TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
	Director::getInstance()->replaceScene(pTrans);
	MpClientManager::getInstance()->disconnect();
}

void MainScene::onBackPressed() {
	/*if (mBoxNew->isVisible()) {
		mBoxNew->setVisible(false);
	} else*/ if (!mBoxNotice->isVisible()) {
		WXmlReader* pXml = WXmlReader::create();
		pXml->load(KEY_XML_FILE);
		BoxNoticeManager::getBoxConfirm(mBoxNotice,
			pXml->getNodeTextByTagName("title_quitgame").c_str(),
			pXml->getNodeTextByTagName("content_quitgame").c_str(),
			CC_CALLBACK_0(MainScene::logout, this));
		mBoxNotice->setVisible(true);
	}
}

void MainScene::showMore() {
	mBtnMore->unregisterTouch();
	mBtnFriend->unregisterTouch();
	mBtnInvetory->unregisterTouch();
	mBtnNews->unregisterTouch();
	mBtnMail->unregisterTouch();
	mBtnInvetory->setVisible(false);

	if (mBtnFriend->getPositionY() == mBtnMore->getPositionY()) {
		mBtnFriend->runAction(MoveTo::create(0.5f, Vec2(mBtnMore->getPositionX(), mBtnMore->getPositionY2() + mBtnFriend->getContentSize().height / 2)));
		mBtnInvetory->runAction(MoveTo::create(0.5f, Vec2(mBtnMore->getPositionX(), mBtnMore->getPositionY2() + mBtnFriend->getContentSize().height 
			+ mBtnInvetory->getContentSize().height / 2)));
		mBtnNews->runAction(MoveTo::create(0.5f, Vec2(mBtnMore->getPositionX(), mBtnMore->getPositionY2() + mBtnFriend->getContentSize().height
			+ mBtnInvetory->getContentSize().height + mBtnNews->getContentSize().height / 2)));
		mBtnMail->runAction(MoveTo::create(0.5f, Vec2(mBtnMore->getPositionX(), mBtnMore->getPositionY2() + mBtnFriend->getContentSize().height
			  + mBtnNews->getContentSize().height / 2)));

		mBtnFriend->runAction(Sequence::createWithTwoActions(DelayTime::create(0.5f), CallFunc::create(CC_CALLBACK_0(EGButtonSprite::_registerTouch, mBtnFriend))));
		mBtnInvetory->runAction(Sequence::createWithTwoActions(DelayTime::create(0.5f), CallFunc::create(CC_CALLBACK_0(EGButtonSprite::_registerTouch, mBtnInvetory))));
		mBtnNews->runAction(Sequence::createWithTwoActions(DelayTime::create(0.5f), CallFunc::create(CC_CALLBACK_0(EGButtonSprite::_registerTouch, mBtnNews))));
		mBtnMail->runAction(Sequence::createWithTwoActions(DelayTime::create(0.5f), CallFunc::create(CC_CALLBACK_0(EGButtonSprite::_registerTouch, mBtnMail))));
	}
	else {
		mBtnFriend->runAction(MoveTo::create(0.5f, mBtnMore->getPosition()));
		mBtnInvetory->runAction(MoveTo::create(0.5f, mBtnMore->getPosition()));
		mBtnNews->runAction(MoveTo::create(0.5f, mBtnMore->getPosition()));
		mBtnMail->runAction(MoveTo::create(0.5f, mBtnMore->getPosition()));
	}

	mBtnMore->runAction(Sequence::createWithTwoActions(DelayTime::create(0.5f), CallFunc::create(CC_CALLBACK_0(EGButtonSprite::_registerTouch, mBtnMore))));
}
void MainScene::getSmsInfo()
{
	MpGetInfoPurchaseMessageRequest* pRequest = new MpGetInfoPurchaseMessageRequest();
	pRequest->setTokenId(GameInfo::mUserInfo.mTokenID.c_str());
	//pRequest->setChargeID(GameInfo::mUserInfo.mChargeID);
	pRequest->setChargeID(1);
	//pRequest->setCountry(EGJniHelper::getCountry());
	pRequest->setCountry("ht");
	std::string pOS = GameInfo::os_prefix;
	pRequest->setOS(pOS);
	pRequest->setProductCode(EGJniHelper::getPackage());
	MpClientManager::getInstance()->sendMessage(pRequest, true);
	mLoading->show();
}

void MainScene::getSubInfo()
{
	MpMessage* message = new MpMessage((uint32_t)MP_MSG_GET_SUBSCRIPTION);
	message->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
	MpClientManager::getInstance()->sendMessage(message);
}

void MainScene::excuteGetSMSInfo(MpMessage* pMsg){
	mLoading->hide();
	MpGetInfoPurchaseMessageResponse* pResponse = (MpGetInfoPurchaseMessageResponse*)pMsg;

	int pErrorCode = pResponse->getErrorCode();
	std::string des = pResponse->getErrorDesciption();

	if (pErrorCode != 0) {
		if (des.length() == 0)
			mErrorCode_MainScene = __String::createWithFormat("%d", pErrorCode)->getCString();
		else
			mErrorCode_MainScene = des;
	}
	else {
		GameInfo::mUserInfo.mLstRechargeInfo.clear();
		GameInfo::mUserInfo.mLstRechageVISAInfo.clear();
		pResponse->getVtRecharge(GameInfo::mUserInfo.mLstRechargeInfo);
		pResponse->getVtRechargeIAP(GameInfo::mUserInfo.mLstRechageVISAInfo);
		GameInfo::mUserInfo.isCanRecharge = (pResponse->getEnable() == 1);

		GameInfo::mUserInfo.mLstExchange.clear();
		GameInfo::mUserInfo.mLstSub_new.clear();
		GameInfo::mUserInfo.mLstSmsInfo_new.clear();
		GameInfo::mUserInfo.mLstSmsInfo_new.clear();
		GameInfo::mUserInfo.mLstSub_new.clear();
		pResponse->getVtSubcribe(GameInfo::mUserInfo.mLstSub_new);
		pResponse->getVtRecharge(GameInfo::mUserInfo.mLstSmsInfo_new);
		pResponse->getVtExchange(GameInfo::mUserInfo.mLstExchange);
		mBoxTopUp->setLstRechargeSMS(GameInfo::mUserInfo.mLstSmsInfo_new);
		mBoxTopUp->setLstSubInfo(GameInfo::mUserInfo.mLstSub_new);
		mBoxTopUp->setLstExchangeInfo(GameInfo::mUserInfo.mLstExchange);

		std::string strTelcoArr = pResponse->getString(MP_PARAM_MAX_TELCO_BALANCE);
		StructTelcoInfo telcoInfo;
		size_t len = 0;
		lstTelcoInfo.clear();
		/*while (len < strTelcoArr.size())
		{
			telcoInfo.telcoName = "";
			while (strTelcoArr[len] != 0)
			{
				telcoInfo.telcoName += strTelcoArr[len];
				len++;
			}
			len++;

			telcoInfo.maxBalance = *((uint32_t*)(strTelcoArr.data() + len));
			telcoInfo.maxBalance = ntohl(telcoInfo.maxBalance);
			len += 4;

			lstTelcoInfo.push_back(telcoInfo);
		}*/

		telcoInfo.telcoName = "natcom";
		telcoInfo.maxBalance = 0;
		lstTelcoInfo.push_back(telcoInfo);

		mErrorCode_MainScene = TEXT_SUCCESS_GETINFOSMS;
	}
}

void MainScene::excuteGetSubInfo(MpMessage* pMsg){
	mLoading->hide();
	MpLoadSubscriptionInfoResponse* pResponse = (MpLoadSubscriptionInfoResponse*)pMsg;

	int pErrorCode = pResponse->getErrorCode();

	if (pErrorCode != 0) {
		mErrorCode_MainScene = __String::createWithFormat("%d", pErrorCode)->getCString();
	}
	else {
		GameInfo::mUserInfo.mLstSubInfo = pResponse->get();
		//mBoxTopUp->setLstSubInfo(GameInfo::mUserInfo.mLstSubInfo);

		mErrorCode_MainScene = TEXT_SUCCESS_GETINFOSUB;
	}
}

void MainScene::update(float dt) {
	MpMessage* msg = MpManager::getInstance()->getMessenger();
	if (msg){
		switch (msg->getType())
		{
		case MP_MSG_GET_ACC_INFO_ACK:
			excuteGetInfoMyselft(msg);
			break;
		case MP_MSG_GETBALANCE_ACK:
			excuteGetBalance(msg);
			break;
		case MP_MSG_GET_LIST_BETMONEY_ACK:
			excuteloadBetMoney(msg);
			break;
		case MP_MSG_CHANGE_PASSWORD_ACK:
			excuteChangeInfo(msg);
			break;
		case MP_CHANGE_ACC_INFO_ACK:
			excuteChangeInfo(msg);
			break;
		case MP_MSG_GETINFO_PURCHASE_ACK:
			excuteGetSMSInfo(msg);
			break;
		case MP_MSG_GET_SUBSCRIPTION_ACK:
			excuteGetSubInfo(msg);
			break;
		case MP_MSG_RECHARGE:
			excuteRecharge(msg);
			break;
		case MP_MSG_GET_SCRATCH_CARD_INFO_ACK:
			excutegetRechargeRate(msg);
			break;
		case MP_MSG_JOIN_TABLE_ACK:
			executeJoinTable(msg);
			break;
		case MP_MSG_ACTIVE_ACCOUNT_VERIFY_ACK:
			executeActiveAccount(msg);
			break;
		case MP_MSG_ACTIVE_ACCOUNT_ACK:
			executeResendOTP(msg);
			break;
		case MP_MSG_CHARGE_ACK:
		{
			uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);

			if (pErrorCode != 0) {
				std::string des = msg->getString(MP_PARAM_ERROR_DESCRIPTION);
				string content = "";
				if (des.length() == 0)
					content = __String::createWithFormat("%d", pErrorCode)->getCString();
				else
					content = des;
				mBoxTopUp->GetBoxSms()->showDescription(des);
			}
			else {
				string _transId = msg->getString(MP_PARAM_TAG_TRANSID);
				mBoxTopUp->GetBoxSms()->ShowOtpToCharge(_transId);
			}
		}
			break;
		case MP_MSG_CHARGE_VERIFY_ACK:
		{
			uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);

			if (pErrorCode != 0) {
				std::string des = msg->getString(MP_PARAM_ERROR_DESCRIPTION);
				string content = "";
				if (des.length() == 0)
					content = __String::createWithFormat("%d", pErrorCode)->getCString();
				else
					content = des;
				mBoxTopUp->GetBoxSms()->showDescription(content);
			}
			else {
				mBoxTopUp->GetBoxSms()->Reset();
				mBoxTopUp->GetBoxSms()->setVisible(false);
				mBoxTopUp->GetBoxSms()->setPosition(Vec2(1300,0));
			}
		}
		break;
		case MP_MSG_EXCHANGE_ACK:
		{
			uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);

			if (pErrorCode != 0) {
				std::string des = msg->getString(MP_PARAM_ERROR_DESCRIPTION);
				string content = "";
				if (des.length() == 0)
					content = __String::createWithFormat("%d", pErrorCode)->getCString();
				else
					content = des;
				mBoxTopUp->GetBoxSms()->showDescription(content);
			}
			else {
				string _transId = msg->getString(MP_PARAM_TAG_TRANSID);
				mBoxTopUp->GetBoxSms()->ShowOtpToCharge(_transId);
			}
		}
		break;
		case MP_MSG_EXCHANGE_VERIFY_ACK:
		{
			uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);

			if (pErrorCode != 0) {
				std::string des = msg->getString(MP_PARAM_ERROR_DESCRIPTION);
				string content = "";
				if (des.length() == 0)
					content = __String::createWithFormat("%d", pErrorCode)->getCString();
				else
					content = des;
				mBoxTopUp->GetBoxSms()->showDescription(des);
			}
			else {
				mBoxTopUp->GetBoxSms()->Reset();
				mBoxTopUp->GetBoxSms()->setVisible(false);
				mBoxTopUp->GetBoxSms()->setPosition(Vec2(1300, 0));
			}
		}
		break;
		case MP_MSG_GIFTCODE_ACK:
		{
			MpMessageResponse* pResponse = (MpMessageResponse*)msg;

			int pErrorCode = pResponse->getErrorCode();

			if (pErrorCode != 0) {
				mErrorCode_MainScene = __String::createWithFormat("%d", pErrorCode)->getCString();
			}
			else {
				//pResponse->getBalance(GameInfo::mUserInfo.mBalance);
				GameInfo::mUserInfo.mBalance = pResponse->getInt(MP_PARAM_BALANCE);
				mErrorCode_MainScene = TEXT_GIFTCODE_SUCCESS;
			}
		}
			break;
		default:
			break;
		}
		delete msg;
		msg = NULL;
	}
	if (getErrorCode() != ""){
		mErrorCode_MainScene = getErrorCode();
		setErrorCode("");
	}
	if (mErrorCode_MainScene != "") {
		mLoading->hide();
		refreshPlayerInfo();
		if (mErrorCode_MainScene == TEXT_SUCCESS_GETINFOSMS) 
		{
			mErrorCode_MainScene = "";
			if (mBoxTopUp->_tab == 1){
				mBoxTopUp->setLstSmsInfoTelco(lstTelcoInfo);
				mBoxTopUp->createTabsSmsInfo();
			}
			else if (mBoxTopUp->_tab == 2){
				mBoxTopUp->showSubPackage();
			}
			else if (mBoxTopUp->_tab == 3)
				mBoxTopUp->reloadData(3);
			mBoxTopUp->setVisible(true);
		}	
		else if (mErrorCode_MainScene == TEXT_SUCCESS_GETINFOSUB)
		{
			mErrorCode_MainScene = "";
			mBoxTopUp->showSubPackage();
			mBoxTopUp->setVisible(true);
		}
		else if (mErrorCode_MainScene == TEXT_GETINFO_SUCCES) {
			if (mPlayerInfoMyself->username == GameInfo::mUserInfo.mUsername){
				GameInfo::mUserInfo.mBalance = mPlayerInfoMyself->balance;
			}
			mBoxInfoGame->showBox(mPlayerInfoMyself, mPlayerInfoMyself);
			mErrorCode_MainScene = "";
		}
		else if (mErrorCode_MainScene == TEXT_GETBETMONEY_SUCCESS) {
			mLoading->hide();
			gotoRankGame();
			mErrorCode_MainScene = "";
		}
		else if (mErrorCode_MainScene == TEXT_ACTIVE_ACCOUNT_SUCCESS) {
			mLoading->hide();
			mErrorCode_MainScene = "";
			//mBoxActive->setVisible(false);
			//mBoxActive->setPosition(Vec2(0, 1000));
		}
		else if (mErrorCode_MainScene == TEXT_SUCCESS_GETINFO_RECHARGERATE) {
			mBoxTopUp->setVisible(true);
			mBoxTopUp->showRechargePromotion(1);
			mErrorCode_MainScene = "";
			//mBoxInfoGame->showBox(mPlayerInfoMyself, mPlayerInfoMyself);
		}
		else if (mErrorCode_MainScene == TEXT_CHANGE_ACCOUNT_INFO_SUCCES) {
			if (GameInfo::mUserInfo.mPassWord != ""){
				UserDefault::getInstance()->setStringForKey(KEY_PASSWORDS, mEditPassWord1->getText());
				UserDefault::getInstance()->setStringForKey(KEY_USERNAME, mEdit_Name->getText());
			}
			GameInfo::mUserInfo.mFullname = mEdit_Name->getText();
			std::string pUserName = GameInfo::mUserInfo.mFullname;
			for (size_t i = 0; i < pUserName.size(); i++){
				pUserName[i] = std::tolower(pUserName[i]);
			}
			GameInfo::mUserInfo.mUsername = pUserName;
			mLblName->setString(GameInfo::mUserInfo.mFullname);
			mLblName->setPosition(Vec2(mSpriteAvatar->getContentSize().width / 2 + mSpriteAvatar->getPositionX()
				+ 5 + mLblName->getContentSize().width*mLblName->getScaleX() / 2, mSpriteAvatar->getContentSize().height / 2 + mSpriteAvatar->getPositionY()
				- mLblName->getContentSize().height*mLblName->getScaleY() / 2 - 10));
			mLayerBoxUpdateAcount->runAction(Hide::create());
			mLayerBoxUpdateAcount->setVisible(false);
		}
		else if (mErrorCode_MainScene == TEXT_CHANGE_PASSWORD_SUCCES) {
			UserDefault::getInstance()->setStringForKey(KEY_PASSWORDS, GameInfo::mUserInfo.mPassWord.c_str());
			mBoxInfoGame->closeBox();
		}
		else if (mErrorCode_MainScene == TEXT_BALANCE) {
			std::string pMoney = __String::createWithFormat("%ld", GameInfo::mUserInfo.mBalance)->getCString();
			pMoney = EGSupport::addDotMoney(pMoney);
			pMoney += " xu";
			mLblBalance->setString(pMoney.c_str());
			mErrorCode_MainScene = "";
		}
		else {
			WXmlReader* pXml = WXmlReader::create();
			pXml->load(KEY_XML_FILE);
			if (mErrorCode_UpdateAcount != ""){
				BoxNoticeManager::getBoxNotice(mBoxNotice,
					pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
					pXml->getNodeTextByTagName("105257").c_str());
				mEdit_Name->setText("");
				mBoxNotice->setVisible(true);
			}
			else{
				WXmlReader* pXml = WXmlReader::create();
				pXml->load(KEY_XML_FILE);

				BoxNoticeManager::getBoxNotice(mBoxNotice,
					pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
					pXml->getNodeTextByTagName(mErrorCode_MainScene.c_str()).c_str());
				mBoxNotice->setVisible(true);
			}
		}
	

		if (mErrorCode_MainScene == "105257"){
			mLayerBoxUpdateAcount->setVisible(false);
			mLayerBoxUpdateAcount->getChildByTag(1)->setVisible(false);
		}
		if (mErrorCode_MainScene != ""){
			WXmlReader* pXml = WXmlReader::create();
			pXml->load(KEY_XML_FILE);
			BoxNoticeManager::getBoxNotice(mBoxNotice,
				pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				pXml->getNodeTextByTagName(mErrorCode_MainScene).c_str());
			mEdit_Name->setText("");
			mBoxNotice->setVisible(true);
		}
	
		mErrorCode_MainScene = "";
	}
}

void MainScene::showTopUpByCard() {
	mBoxTopUp->close();
	//mBoxTopUpCard->setVisible(true);
}

void MainScene::rechargeBySMS(int pIndex) {
	mLoading->show();
	//mBoxTopUp->close();
	this->runAction(Sequence::createWithTwoActions(DelayTime::create(10),CallFunc::create(CC_CALLBACK_0(Loading::hide, mLoading))));
}

#include "mprechargebycardmessageresponse.h"

void MainScene::recharge(std::string pSerial, std::string pNumber, std::string pTelco) {

	mLoading->show();
	this->runAction(Sequence::createWithTwoActions(DelayTime::create(10), CallFunc::create(CC_CALLBACK_0(Loading::hide, mLoading))));

	RechargeStruct* pRechargeStruct = new RechargeStruct();
	pRechargeStruct->mTelco = pTelco;
	pRechargeStruct->mNumber = pNumber;
	pRechargeStruct->mSerrial = pSerial;
	pRechargeStruct->mUsername = GameInfo::mUserInfo.mUsername;
	pRechargeStruct->mLoading = mLoading;

	MpMessage* message = new MpMessage((uint32_t)MP_MSG_RECHARGE);
	message->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
	message->addInt(MP_PARAM_CHARGE_ID, GameInfo::mUserInfo.mChargeID);
	message->addString(MP_PARAM_TELCO_ID, pRechargeStruct->mTelco);
	message->addString(MP_SERRIAL, pRechargeStruct->mSerrial);
	message->addString(MP_PARAM_PASSWORD, pRechargeStruct->mNumber);
	for (int i = 0; i < pRechargeStruct->mUsername.length(); i++) {
		pRechargeStruct->mUsername[i] = std::tolower(pRechargeStruct->mUsername[i]);
	}
	message->addString(MP_PARAM_USERNAME, pRechargeStruct->mUsername);
	MpClientManager::getInstance()->sendMessage(message);
}
void MainScene::excuteRecharge(MpMessage* pMsg){
	MpRechargeByCardMessageResponse* pResponse =(MpRechargeByCardMessageResponse*)pMsg;
	int pErrcode = pResponse->getErrorCode();
	if (pErrcode != 0) {
		mErrorCode_MainScene = TEXT_ERR_USERNAME_NOTEXIST;
	}
	mLoading->hide();
	mErrorCode_MainScene = "txt_recharge_success_mainscene";
}



void MainScene::gotoInventory() {
	//Scene* pScene = InventoryScene::scene();
	//TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
	//Director::getInstance()->replaceScene(pTrans);
}

void MainScene::gotoNews() {
	/*mBoxNew->setVisible(true);
	mBoxNew->show();*/
}

void MainScene::gotoFriendScene() {
	unscheduleAllSelectors();
	unscheduleUpdate();
	Scene* pScene = FriendScene::scene();
	TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
	Director::getInstance()->replaceScene(pTrans);
}

void MainScene::gotoTopScene() {
	//Scene* pScene = TopScene::scene();
	unscheduleAllSelectors();
	unscheduleUpdate();
	Scene* pScene = TopScene::scene();
	TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
	Director::getInstance()->replaceScene(pTrans);
}

void MainScene::gotoMailScene() {
	//Scene* pScene = TopScene::scene();
	unscheduleAllSelectors();
	unscheduleUpdate();
	Scene* pScene = MailScene::scene();
	((MailScene*)pScene)->setFuncBack([=]{
		Scene* pScenes = MainScene::scene(0);
		TransitionFade* pTranss = TransitionFade::create(0.5f, pScenes);
		Director::getInstance()->replaceScene(pTranss);
	});
	TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
	Director::getInstance()->replaceScene(pTrans);
}


void MainScene::gotoShopScene() {
	unscheduleAllSelectors();
	unscheduleUpdate();
	Scene* pScene = ShopScene::scene();
	//Scene* pScene = MailScene::scene();
	//TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
	Director::getInstance()->replaceScene(pScene);
}

void MainScene::showNotice(const std::string& content)
{
	mErrorCode_MainScene = content;
}

void MainScene::showLoading(bool bLoading)
{
	if (bLoading)
		mLoading->show();
	else
		mLoading->hide();
}

void MainScene::showTopUp() {
	//bool state = GameInfo::mUserInfo.isCanEnableSms;
	//if (state){
	//	/*if (mBoxTopUp->getLstSubInfo.size() == 0) {
	//		mLoading->show();
	//		getSubInfo();
	//	}l
	//	else {
	//		mBoxTopUp->setVisible(true);
	//		mBoxTopUp->setPosition(0, 0);
	//	}*/
	//}
	//else{
	//	mBoxTopUp->setVisible(true);
	//	mBoxTopUp->setPosition(0, 0);
	//	mBoxTopUp->showBoxLockRecharge();
	//}

	if (mBoxTopUp->getLstSubInfo().size() == 0) {
		mLoading->show();
		getSmsInfo();
	}
	else {
		mBoxTopUp->setVisible(true);
		mBoxTopUp->setPosition(0, 0);
	}
}
void MainScene::excutegetRechargeRate(MpMessage* pMsg){
	MpMessage* msg = (MpMessage*)pMsg;
	uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);

	if (pErrorCode != 0) {
		mErrorCode_MainScene = __String::createWithFormat("%d", pErrorCode)->getCString();
	}
	else {
		std::string data = msg->getString(MP_PARAM_SCRATCH_CARD_INFO);
		std::vector<ScratchCard> lstitem;
		lstitem = ScratchCard::from_string(data);
		mBoxTopUp->setLstRate(lstitem);
		mErrorCode_MainScene = TEXT_SUCCESS_GETINFO_RECHARGERATE;
	}
}

void MainScene::excuteloadBetMoney(MpMessage* pMsg){
	MpMessage* msg = pMsg;
	uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);

	if (pErrorCode != 0) {
		mErrorCode_MainScene = __String::createWithFormat("%d", pErrorCode)->getCString();
	}
	else {
		std::string data = msg->getString(MP_PARAM_BET_MONEY);
		{
			const uint8_t* ptr = (const uint8_t*)data.data();
			const uint8_t* end = (const uint8_t*)data.data() + data.size();
			while (ptr < end)
			{
				uint32_t n = *((uint32_t*)ptr);
				n = ntohl(n);
				ptr += 4;
				vtBetMoney.push_back(n);
			}
		}
		mErrorCode_MainScene = TEXT_GETBETMONEY_SUCCESS;
	}
}



void MainScene::executeJoinTable(MpMessage* pMsg) {
	mLoading->hide();
	roomStruct* pInfo = new roomStruct();
	std::vector<ClientItemInfo> mListItemSp;
	std::vector<uint16_t> mListPrice;
	std::vector<uint16_t> mListXFactor;
	pInfo->mListPlayer = new std::vector<ClientPlayerInfoEX>();
	pInfo->mListItemSp = mListItemSp;
	pInfo->mListBetMoney = new std::vector<uint32_t>();
	pInfo->mListPrice = mListPrice;
	pInfo->mListXFactor = mListXFactor;
	pInfo->pLoading = mLoading;
	pInfo->mGameID = GameInfo::mUserInfo.mGameID;
	pInfo->pTypeRoom = GameInfo::mUserInfo.mLevelRoom;

	MpJoinTableMessageResponse *pResponse = (MpJoinTableMessageResponse*)pMsg;

	unsigned  pErrorCode = pResponse->getErrorCode();
	if (pErrorCode != 0) {
		mErrorCode_MainScene = __String::createWithFormat("%d", pErrorCode)->getCString();
		mLoading->hide();
	}
	else {
		std::vector<ClientPlayerInfo> pListPlayer;
		std::vector<ReadyPlayer> pListReady;
		pResponse->getListReadyPlayer(pListReady);
		pResponse->getListPlayer(pListPlayer);
		pResponse->getListItem(pInfo->mListItemSp);
		pResponse->getListFishPrice(pInfo->mListPrice);
		pInfo->mTableID = pResponse->getInt(MP_PARAM_TABLE_ID);
		pInfo->mDurationReadyNormal = 30;
		pInfo->mDurationReadyHost = 30;
		pResponse->getDefaultBetMoney(pInfo->pBetMoneyDefault);
		uint16_t pDuration;
		uint32_t pFireMoney = pResponse->getInt(MP_PARAM_FIRE_MONEY);
		pResponse->getGameDuration(pDuration);
		std::string strListFactor; pResponse->getString(MP_PARAM_LIST_XFACTOR, strListFactor);
		for (int i = 0; i < strListFactor.length(); i++) {
			pInfo->mListXFactor.push_back(strListFactor[i]);
		}
		pInfo->mDuration = pDuration;
		uint32_t pMinBetMoney;
		pMinBetMoney = pResponse->getInt(MP_PARAM_BET_MONEY);
		std::string strXFactor; pResponse->getString(MP_PARAM_XFACTOR, strListFactor);

		if (pInfo->mListBetMoney &&pInfo->mListBetMoney->size() > 0){
			pInfo->mListBetMoney->clear();
		}
		for (int i = 0; i < 3; i++){
			pInfo->mListBetMoney->push_back(pMinBetMoney *LIST_BET_RATE[i]);
		}
		pInfo->pBetMoney = pMinBetMoney;

		pInfo->mRatio = pResponse->getRatioLevel();
		pInfo->mBackGroundID = pResponse->getBackgroundId();
		std::string data;
		int len = 0;
		pResponse->getString(MP_PARAM_FISH_MONEY, data);
		for (int i = pListPlayer.size() - 1; i >= 0; i--) {
			int index = pListPlayer.size() - 1 - i;
			ClientPlayerInfo playerInfo = pListPlayer.at(index);
			ClientPlayerInfoEX playerInfoGame;
			playerInfoGame.balance = playerInfo.balance;
			playerInfoGame.balanceDefault = playerInfo.balance;
			playerInfoGame.host = playerInfo.host;
			playerInfoGame.level = playerInfo.level;
			playerInfoGame.position = playerInfo.position;
			playerInfoGame.score = *((uint32_t*)(data.data() + len));
			playerInfoGame.score = ntohl(playerInfoGame.score);
			len += 4;
			playerInfoGame.username = playerInfo.username;
			playerInfoGame.disallowKick = playerInfo.disallowKick;
			for (int j = 0; j < pListReady.size(); j++) {
				ReadyPlayer  readyPlayer = pListReady.at(j);
				if (readyPlayer.position == playerInfoGame.position) {
					playerInfoGame.isReady = readyPlayer.status;
					break;
				}
			}

			playerInfoGame.itemId = playerInfo.itemId;
			if (playerInfo.itemId == 0) {
				playerInfoGame.itemId = 101;
			}
			pInfo->mListPlayer->push_back(playerInfoGame);
		}
		if (strXFactor.length() == pInfo->mListPlayer->size()) {
			for (size_t i = 0; i < pInfo->mListPlayer->size(); i++) {
				pInfo->mListPlayer->at(i).indexX = strXFactor[i];
			}
		}
		GameScene *scene = ((GameScene*)createGame(pInfo));

		Scene *_scene = GameScene::createScene(pInfo);
		TransitionFade* pTrans = TransitionFade::create(0.5f, _scene);
		Director::getInstance()->replaceScene(pTrans);

		if (pInfo->mGameID == 1) {
			scene->setRemainTime(pResponse->getInt(MP_REMAIN_TIME));
			scene->setFireMoney(pFireMoney);
		}
		mErrorCode_MainScene = "";


		this->unscheduleAllSelectors();
	}
}

Scene* MainScene::createGame(void* pInfo) {
	unscheduleAllSelectors();
	unscheduleUpdate();
	Scene *_scene = GameScene::createScene(pInfo);
	TransitionFade* pTrans = TransitionFade::create(0.5f, _scene);
	Director::getInstance()->replaceScene(pTrans);
	return _scene;
}

void MainScene::sendOtpToActiveAccount(std::string otp){
	MpMessageRequest* request = new MpMessageRequest(MP_MSG_ACTIVE_ACCOUNT_VERIFY_REQ);
	request->setTokenId(GameInfo::mUserInfo.mTokenID);
	//request->addString(MP_PARAM_TAG_1, GameInfo::mUserInfo.mFullname);
	request->addString(MP_PARAM_TAG_2, otp);
	request->addString(MP_PARAM_TAG_3, _transId);
	MpClientManager::getInstance()->sendMessage(request, true);
}

void MainScene::resendOTP(){
	MpMessageRequest* request = new MpMessageRequest(MP_MSG_ACTIVE_ACCOUNT_REQ);
	request->setTokenId(GameInfo::mUserInfo.mTokenID);
	MpClientManager::getInstance()->sendMessage(request, true);
}

void MainScene::executeActiveAccount(MpMessage* pMsg){
	MpMessage* msg = pMsg;
	uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);

	if (pErrorCode != 0) {
		std::string des = msg->getString(MP_PARAM_ERROR_DESCRIPTION);
		string content = "";
		if (des.length() == 0)
			content = __String::createWithFormat("%d", pErrorCode)->getCString();
		else
			content = des;
		mBoxActive->showDescription(content);
		
	}
	else {

		WXmlReader* pXml = WXmlReader::create();
		pXml->load(KEY_XML_FILE);

		mBoxActive->showDescription(pXml->getNodeTextByTagName("txt_verify_otp_success").c_str());
		mBoxActive->successActive();
	}
}

void MainScene::executeResendOTP(MpMessage* pMsg){
	MpMessage* msg = pMsg;
	uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);

	if (pErrorCode != 0) {
		std::string des = msg->getString(MP_PARAM_ERROR_DESCRIPTION);
		string content = "";
		if (des.length() == 0)
			content = __String::createWithFormat("%d", pErrorCode)->getCString();
		else
			content = des;
		mBoxActive->showDescription(content);
	}
	else {
		//show notice resend
		WXmlReader* pXml = WXmlReader::create();
		pXml->load(KEY_XML_FILE);
		_transId = msg->getString(MP_PARAM_TAG_TRANSID);
		mBoxActive->showDescription(pXml->getNodeTextByTagName("txt_resend_otp_success").c_str());
	}
}
