#pragma once

#include "cocos2d.h"
#include "EGSprite.h"
#include "EGButtonSprite.h"
#include "EGScene.h"
#include "BoxNotice.h"
#include "object/BoxNoticeManager.h"
#include "Loading.h"
#include "ClientPlayerInfo.h"
#include "object/BoxMail.h"
#include "GameInfo.h"
#include "WXmlReader.h"
#include "object/MailView.h"

#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "extensions\cocos-ext.h"
#else
#include "cocos-ext.h"
#endif

class MailScene : public EGScene, public TableViewDataSource, public TableViewDelegate{
public:
	static MailScene* scene();

public:
	void initScene();
	void onKeyBack();
	void addNewMailToOutbox(StructMail pInfoMail);
	int getCurrentPage();
	void addStatusCount(int pStatus);
	void chooseTab(int index, int pPage);
	void showBoxMail(uint8_t type, MailView* mailView = NULL, uint8_t idx = 0);
	void enableCreateMail(bool pBool);

private:
	void clearData();
	void onReload();
	void btnBackClicked();
	void mailView(MailView* view);
	void _setTextForButton(Node* sender, std::string strContent);
	void replyMess(StructMail pMail);
	void sendMail(const std::string& name, const std::string& title, const std::string& content);
	void readMail(std::string name, uint32_t time);
	void deleteMail(StructMail str);
	void hideDialog(Node* pNode, int pTab);
	void loadMail(uint8_t nType, uint8_t nPage); // 0:inbox, 1:outbox

private:
	TableView* mTableView;
	TableViewCell* mTableViewCell;
	Node* mLayerMail[3];
	Size mTableSize;
	int mCurrentTab, mCurrentPage;
	std::vector<float> mLstHeight;
	std::vector<Node*> mLstObject;
	EGButtonSprite*  _btnMessIn, *_btnMessOut, *_btnMessWrite;
	BoxMail* boxMail;
	BoxNotice* mBoxNotice;
	Loading* mLoading;
	WXmlReader* _mXmlreader;
	EGButtonSprite* viewStatusCount;
	uint8_t _nCurrentInboxPage, _nCurrentOutboxPage;

private:
	void getInbox();
	void getOutbox();
	void callBackForBoxMail(); 
	void update(float dt);
	void showNotice(const std::string& notice);

private:
	//CC_SYNTHESIZE(std::vector<StructMail>, , ListMailInbox);
	//CC_SYNTHESIZE(std::vector<StructMail>, , ListMailOutbox);

	CC_SYNTHESIZE(std::function<void()>, _funcBack, FuncBack);
	CC_SYNTHESIZE(std::function<void(int pType, int pPage)>, _funcLoadMail, FuncLoadMail);
	CC_SYNTHESIZE(std::function<void(int pMailID)>, _funcDeleteMail, FuncDeleteMal);
	CC_SYNTHESIZE(std::function<void(int pMailID)>, _funcReadMail, FuncReadMal);
	CC_SYNTHESIZE(std::function<void(StructMail pMailInfo)>, _funcSendMail, FuncSendMal);
	virtual void scrollViewDidScroll(ScrollView* view) {};
	virtual void scrollViewDidZoom(ScrollView* view) {};
	virtual void tableCellTouched(TableView* table, TableViewCell* cell) {};
	virtual Size tableCellSizeForIndex(TableView *table, ssize_t idx);
	virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(TableView *table);
private:
	MailScene();
	~MailScene();
};