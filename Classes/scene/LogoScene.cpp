#include "LogoScene.h"
#include "GameInfo.h"
#include "HomeScene.h"
#include "FirstScene.h"

Scene* LogoScene::scene() {
	LogoScene* pScene = new LogoScene();
	pScene->initScene();
	pScene->autorelease();

	return pScene;
}

void LogoScene::initScene() {
	GameInfo::mUserInfo.stateScene = 0;
	mBackground = Sprite::create("logo.png");
	mBackground->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	mBackground->setOpacity(0);
	this->addChild(mBackground);

	mBackground->runAction(Sequence::create(DelayTime::create(0.1f),FadeTo::create(1,255),DelayTime::create(2),
		CallFunc::create([=] {
		Director::getInstance()->replaceScene(TransitionFade::create(0.5f, FirstScene::scene()));
	}), NULL));
}