#include "ChooseRoomScene.h"
#include "pthread_mobi.h"
#include "MessageType.h"
#include "MainScene.h"
#include "MpLoginRequest.h"
#include "MpLoginResponse.h"
#include "MpMessageRequest.h"
#include "md5.h"
#include "EGJniHelper.h"
#include "MpLoadRoomMessageResponse.h"

#define CREATE_TID(g,l,r,t) (((g)<<24) | ((l) << 16)| ((r) << 8) | ((t) & 0xff)) // for client
std::string mErrorCode_ChooseRoom;
std::string mErrorCode_JoinRoom;
std::string mErrorCode_JoinRoom1;
std::string mIP[2];
uint16_t mPort[2];
roomStruct* mInviteInfo;
bool iChoosePlace;
bool iCreateGame;
bool isExitByUser = false;

struct ReconectStruct {
public:
	std::string pUsername;
	std::string pPasswords, pCPID;
	std::string pDeviceID;
	Loading* mLoading;
	uint8_t mChargeID;
};
#define SSTR(x) dynamic_cast< std::ostringstream & >((std::ostringstream() << std::dec << (x))).str()
void* pthread_loadRoom(void* _void);
void* pthread_loadTable(void* _void);
void* pthread_reconect_chooseScene(void *_void);
void pthread_playNow(void* _void) {
	loadRoomStruct* pStructRoom = (loadRoomStruct*)_void;
	roomStruct* pIDRoom = (roomStruct*)_void;
	MpMessageRequest* pRequest = new MpMessageRequest(MP_MSG_QUICK_JOIN_TABLE);
	std::string pToken = GameInfo::mUserInfo.mTokenID;
	pRequest->setTokenId(pToken.c_str());
	unsigned tableid;
	tableid = CREATE_TID(pIDRoom->mGameID, atoi(pIDRoom->pRoomLevelstr.c_str()), 0, 0xff);
	pRequest->addInt(MP_PARAM_TABLE_ID, tableid);
	pRequest->addInt(MP_PARAM_BALANCE, GameInfo::mUserInfo.mBalance);
	//pIDRoom->mTcp->sendMsg(pRequest->getMsg());
	MpClientManager::getInstance(2)->sendMessage(pRequest);
}


Scene* ChooseRoomScene::createScene(int pGameID)
{
	ChooseRoomScene* pScene = new ChooseRoomScene();
	GameInfo::mUserInfo.stateScene = 0;
	pScene->initScene(pGameID);
	pScene->autorelease();
	pScene->registerBackPressed();
	return pScene;
}

Scene* ChooseRoomScene::createSceneFromGame(int pGameID)
{
	ChooseRoomScene* pScene = new ChooseRoomScene();
	pScene->initScene(pGameID);
	GameInfo::mUserInfo.iFromGame = true;
	pScene->autorelease();
	pScene->registerBackPressed();
	return pScene;
}
void ChooseRoomScene::initScene(int pGameID)
{
	//GameInfo::mUserInfo.mLstTcpTimeout.clear();
	roomLoadStruct = nullptr;
	m_isResume = false;
	_isLoadTableContinue = false;
	mErrorCode_JoinRoom1 = "";
	GameInfo::mUserInfo.iFromGame = false;
	mInviteInfo = NULL;
	mInfoRoom = new roomStruct();
	GameInfo::mUserInfo.mIsKeepAlive = true;
	mGameID = pGameID;
	iCreateGame = false;
	iChoosePlace = false;
	GameInfo::mUserInfo.iStage = false;
	mCurrentStage = 0;
	mLoading = NULL;
	mCountTab = 0;
	mRoomID = -1;
	mErrorCode_JoinRoom = "";
	Size _size = Director::getInstance()->getWinSize();
	mMainLayer = Layer::create();
	this->addChild(mMainLayer);
	pTest = true;
	mListMoneyBet = new std::vector<uint32_t>();
	mCacheListRoom = new std::vector<ClientTableInfo>();
	mListRoom = new std::vector<uint8_t>();
	mPlayerList = new std::vector<ClientPlayerInfoEX>();
	Sprite* mBackGround = Sprite::create("background_choosetype.png");
	mMainLayer->addChild(mBackGround);
	mLayerPlace = LayerColor::create();
	this->addChild(mLayerPlace);
	mBackGround->setAnchorPoint(Vec2(0, 0));
	mBarTitle = Sprite::create("kv_chonman_title.png");
	
	mMainLayer->addChild(mBarTitle);
	EGButtonSprite* buttonBack = EGButtonSprite::createWithFile(IMG_BUTTON_BACK_CR1);
	mBackGround->addChild(buttonBack);
	buttonBack->setButtonType(EGButton2FrameDark);
	buttonBack->setOnTouch(CC_CALLBACK_0(ChooseRoomScene::backStage, this));
	buttonBack->setPosition(Vec2(buttonBack->getContentSize().width / 2 + 20, CLIENT_HEIGHT - buttonBack->getContentSize().height/2 - 30));

	auto mLblTitle = Label::createWithBMFont(FONT_BMF_NAME, "JWET NOMAL");
	mLblTitle->setScale(1.2);
	mLblTitle->setPosition(Vec2(CLIENT_WIDTH/2, buttonBack->getPositionY()));
	mMainLayer->addChild(mLblTitle);

	if (mGameID == 1) {
		mLblTitle->setString("JWE AN LIY");
	}

	EGButtonSprite* buttonCreateRoom = EGButtonSprite::createWithFile("kv_info_bg.png");
	mBarTitle->addChild(buttonCreateRoom);
	buttonCreateRoom->setButtonType(EGButton2FrameDark);
	buttonCreateRoom->setScale(0.6);
	buttonCreateRoom->setPosition(Vec2(mBarTitle->getContentSize().width - 10, mBarTitle->getContentSize().height / 2 + 20));
	buttonCreateRoom->setOnTouch(CC_CALLBACK_0(ChooseRoomScene::playNow, this));

	auto mLbPlayNow = Label::createWithBMFont(FONT_BMF_NAME, "Jwe kounyea");
	mLbPlayNow->setPosition(Vec2(buttonCreateRoom->getContentSize() / 2));
	buttonCreateRoom->addChild(mLbPlayNow);

	if (mGameID == 0) {
		buttonCreateRoom->setVisible(false);
	}

	mBarTitle->setPosition(Vec2(CLIENT_WIDTH / 2, buttonBack->getPositionY() - 20));
	//mBarTitle->runAction(MoveBy::create(0.2f, Vec2(0, -mBarTitle->getContentSize().height)));
	string area = "Europy";
	for (int i = 3; i >= 0; i--) {
		int index = 3 - i;
		if (index == 0)
			area = "Africa";
		else if (index == 1)
			area = "Americas";
		else if (index == 2)
			area = "Australia";
		else
			area = "Europy";


		EGButtonSprite* parent = EGButtonSprite::createWithFile("kv_chonman_asia_bg.png");
		parent->setPosition(Vec2(652 / 4 * (4-i), mBarTitle->getPositionY() - mBarTitle->getContentSize().height/2 - parent->getContentSize().height/2));
		parent->setTag(index + 10);
		mLayerPlace->addChild(parent);

		EGButtonSprite* buttonChoosePlace = EGButtonSprite::createWithFile(__String::createWithFormat("choose_place%d_1.png", index + 1)->getCString(), EGButton2FrameDark);
		mLayerPlace->addChild(buttonChoosePlace);
		buttonChoosePlace->setPosition(Vec2(parent->getPositionX(), parent->getPositionY() + 15));

		buttonChoosePlace->setTag(index + 1);
		buttonChoosePlace->setOnTouch(CC_CALLBACK_0(ChooseRoomScene::choosePlace, this, index + 1));
		parent->setOnTouch(CC_CALLBACK_0(ChooseRoomScene::choosePlace, this, index + 1));

		auto mLblName = Label::createWithBMFont(FONT_BMF_NAME, area);
		mLblName->setPosition(Vec2(parent->getContentSize().width/2, 30));
		parent->addChild(mLblName);
	}

	_boxMoneyGame = Sprite::create("box_game_money.png");
	mLayerPlace->addChild(_boxMoneyGame);
	_boxMoneyGame->setPosition(Vec2(_size.width / 2, 30 + _boxMoneyGame->getContentSize().height / 2));
	_boxMoneyGame->setVisible(false);

	mLayerChooseRoom = ChooseRoomLayer::createLayer();
	mLayerChooseRoom->setFuntionChooseRoom(CC_CALLBACK_1(ChooseRoomScene::chooseRoom, this));
	this->addChild(mLayerChooseRoom);
	updateStage(0);

	mBoxNotice = BoxNotice::create();
	mBoxNotice->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBoxNotice, 1);

	mLoadingMain = Loading::create();
	this->addChild(mLoadingMain);
	mLoadingMain->setZOrder(10);
	mLoadingMain->setPosition(Vec2(_size.width / 2, _size.height / 2));
//	mInfoRoom->mTcp = NULL;

	for (size_t i = 0; i < GameInfo::mUserInfo.lstGameDomain.size(); i++) {
		if (GameInfo::mUserInfo.lstGameDomain.at(i).gameId < 2) {
			mIP[GameInfo::mUserInfo.lstGameDomain.at(i).gameId] = GameInfo::mUserInfo.lstGameDomain.at(i).domain;
			mPort[GameInfo::mUserInfo.lstGameDomain.at(i).gameId] = GameInfo::mUserInfo.lstGameDomain.at(i).port;
		}
	}
	if (mGameID == 0){
		MpClientManager::getInstance(1);
		MpClientManager::getInstance(1)->setMpClientEnable(true);
		
		std::string ip;
		int port;
		for (int i = 0; i < GameInfo::mUserInfo.lstGameDomain.size(); i++)
		{
			if (GameInfo::mUserInfo.lstGameDomain.at(i).gameId == 0){
				ip = GameInfo::mUserInfo.lstGameDomain.at(i).domain;
				port = GameInfo::mUserInfo.lstGameDomain.at(i).port;
			}
		}
		MpClientManager::getInstance(1)->connect(ip.c_str(), port, false);
	}
	else{
		MpClientManager::getInstance(2);
		MpClientManager::getInstance(2)->setMpClientEnable(true);
		std::string ip;
		int port;
		for (int i = 0; i < GameInfo::mUserInfo.lstGameDomain.size(); i++)
		{
			if (GameInfo::mUserInfo.lstGameDomain.at(i).gameId == 1){
				ip = GameInfo::mUserInfo.lstGameDomain.at(i).domain;
				port = GameInfo::mUserInfo.lstGameDomain.at(i).port;
			}
		}
		MpClientManager::getInstance(2)->connect(ip.c_str(), port, false);
	}
	//schedule(schedule_selector(ChooseRoomScene::updateInfoTable));

	runAction(CallFunc::create([=]{if (GameInfo::mUserInfo.isReConnect)
	{
		mLoadingMain->show();
		GameInfo::mUserInfo.isReConnect = false;
		runAction(Sequence::createWithTwoActions(DelayTime::create(1), CallFunc::create([=] {
			mPlayerList->clear();
			mLoadingMain->setSize(Size(CLIENT_WIDTH, CLIENT_HEIGHT));
			mLoadingMain->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
			mInfoRoom->mTableID = GameInfo::mUserInfo.mTableID;
			mInfoRoom->pTypeRoom = 0;
			mInfoRoom->mIDRoom = GameInfo::mUserInfo.mRoomID;
			mInfoRoom->pRoomLevelstr = GameInfo::mUserInfo.mRoomLevel;
			mInfoRoom->pLoading = mLoadingMain;
			mInfoRoom->mListPlayer = mPlayerList;
			mInfoRoom->mListBetMoney = new std::vector<uint32_t>();
			mInfoRoom->mGameID = mGameID;
			mInfoRoom->pMaxPlayer = 4;
			mInfoRoom->iCreater = false;
			MpMessageRequest* pRequest = new MpMessageRequest(MP_MSG_JOIN_TABLE);
			std::string pToken = GameInfo::mUserInfo.mTokenID;
			pRequest->setTokenId(pToken);
			unsigned tableid = mInfoRoom->mTableID;
			pRequest->addString(MP_PARAM_USERNAME, GameInfo::mUserInfo.mUsername);
			pRequest->addInt(MP_PARAM_TABLE_ID, tableid);
			MpClientManager::getInstance(2)->sendMessage(pRequest, true);
			//schedule(schedule_selector(ChooseRoomScene::updateJoinTable), 0);
			//pthread_t t1;
			/*	pthread_create(&t1, NULL, &pthread_joinRoom, mInfoRoom);
			schedule(schedule_selector(ChooseRoomScene::updateJoinTable), 2.0f);*/
		})));
	}}));
	replay();
	this->scheduleUpdate();
	if (MainScene::getVectorMoney().size() > 0) {
		updateListBetMoney(MainScene::getVectorMoney());
	}
	/*if (mGameID == 0) {
		choosePlace(1);
	}*/

}
void ChooseRoomScene::playNow() {
	if (!mLoadingMain->isVisible()) {
		mPlayerList->clear();
		mLoadingMain->setSize(Size(CLIENT_WIDTH, CLIENT_HEIGHT));
		mLoadingMain->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
		//mInfoRoom->mTcp = new TcpClient();
		mInfoRoom->pTypeRoom = 0;
		if (mCacheListRoom->size() > 0) {
			if (mCacheListRoom->at(mLayerChooseRoom->getIDChoosed()).team1 > 0) {
				mInfoRoom->pTypeRoom = 1;
			}
			mInfoRoom->pBetMoney = mLayerChooseRoom->getInfoRoom().betMoney;
			mInfoRoom->pMaxPlayer = mLayerChooseRoom->getInfoRoom().maxPlayer;
			if (mLayerChooseRoom->getInfoRoom().team1 == 0) {
				if (mLayerChooseRoom->getInfoRoom().totalPlayer > 0) {
					mInfoRoom->pTypeRoom = 0;
				}
			}
		}
		mInfoRoom->mIDRoom = mRoomID;
		mInfoRoom->pRoomLevelstr = pRoomLevelstr;
		mInfoRoom->pLoading = mLoadingMain;
		mInfoRoom->mListPlayer = mPlayerList;

		mInfoRoom->mGameID = mGameID;
		mInfoRoom->mListBetMoney = mListMoneyBet;


		mInfoRoom->iCreater = false;
		mLoadingMain->show();
		pthread_playNow(mInfoRoom);
		//schedule(schedule_selector(ChooseRoomScene::updateJoinTable), 0);
	}
}

Label* ChooseRoomScene::addTextforButton(Sprite* pButton, std::string pString) {
	pButton->removeAllChildrenWithCleanup(true);
	Label *label = Label::createWithBMFont(FONT_ARIAL_FNT, pString.c_str());
	pButton->addChild(label);
	label->setPosition(Vec2(pButton->getContentSize().width / 2, pButton->getContentSize().height / 2));
	label->setTag(1);
	label->setUserObject(new __String(pString));
	return label;
}

void ChooseRoomScene::updateListBetMoney(std::vector<uint32_t> vtBetMoney) {
	//mInfoRoom->mListBetMoney = vtBetMoney;
	_boxMoneyGame->setVisible(true);
	for (int i = 0; i < 4; i++) {
		if (mLayerPlace->getChildByTag(i + 1)) {
			mLayerPlace->getChildByTag(i + 1)->setVisible(false);
		}
		if (mLayerPlace->getChildByTag(i + 10)) {
			mLayerPlace->getChildByTag(i + 10)->setVisible(false);
		}
	}
	for (size_t i = 0; i < _boxMoneyGame->getChildrenCount(); i++) {
		_boxMoneyGame->getChildren().at(i)->setOpacity(0);
		_boxMoneyGame->getChildren().at(i)->stopAllActions();
		if (_boxMoneyGame->getChildren().at(i)->getChildrenCount() > 0) {
			_boxMoneyGame->getChildren().at(i)->getChildByTag(1)->setOpacity(0);
			_boxMoneyGame->getChildren().at(i)->getChildByTag(1)->stopAllActions();
		}
	}
	for (size_t i = 0; i < vtBetMoney.size(); i++) {
		EGButtonSprite* buttonJoin = (EGButtonSprite*)_boxMoneyGame->getChildByTag(i + 1);
		if (!buttonJoin) {
			buttonJoin = EGButtonSprite::createWithFile("button_join_by_money.png");
			buttonJoin->setTag(i + 1);
			_boxMoneyGame->addChild(buttonJoin);
		}
		buttonJoin->setOnTouch(CC_CALLBACK_0(ChooseRoomScene::chooseRoom, this, i));
		buttonJoin->setPosition(Vec2(90 + (10 + buttonJoin->getContentSize().width)*(i % 4), _boxMoneyGame->getContentSize().height - 143 - (20 + buttonJoin->getContentSize().height)*(i / 4)));
		addTextforButton(buttonJoin, __String::createWithFormat("%d", vtBetMoney.at(i))->getCString());
		if (buttonJoin->getChildrenCount() > 0) {
			buttonJoin->getChildByTag(1)->setOpacity(0);
			buttonJoin->getChildByTag(1)->stopAllActions();
		}
		buttonJoin->runAction(Sequence::create(DelayTime::create(0.1f*i), FadeIn::create(0.2f), NULL));
		buttonJoin->getChildByTag(1)->runAction(Sequence::create(DelayTime::create(0.1f*i), FadeIn::create(0.2f), NULL));
	}
}

void ChooseRoomScene::choosePlace(int index) {

	mRoomID = index-1;
	
	_isLoadTableContinue = true;
	setEnableButtonChoosePlace(false);
	pRoomLevelstr = __String::createWithFormat("%d", index - 1)->getCString();
	Sprite* pSprite = ((Sprite*)mLayerPlace->getChildByTag(index));
	if (mGameID == 1) 
	{
		pSprite->getChildren().at(0)->setVisible(true);
		pSprite->stopActionByTag(2);
		Action* action = ScaleTo::create(0.2f, 1.05f);
		action->setTag(2);
		pSprite->runAction(action);
		pSprite->setTexture(Director::getInstance()->getTextureCache()->addImage(__String::createWithFormat("choose_place%d_2.png", index)->getCString()));

	}
	else {
		chooseRoom(0);
	}
	if (mLoading){
		mLoading->removeFromParentAndCleanup(true);
	}
	mLoading = Sprite::create("loading_1.png");
	mLoading->setVisible(true);
	Vector<SpriteFrame*> _list;
	for (int i = 0; i < 4; i++){
		std::string filename = __String::createWithFormat("loading_%d.png", i + 1)->getCString();
		SpriteFrameCache *cache = SpriteFrameCache::getInstance();
		SpriteFrame *spriteFrame = cache->getSpriteFrameByName(filename);
		auto sprite = Sprite::create(filename);
		auto frame = SpriteFrame::create(filename, sprite->getTextureRect());
		cache->addSpriteFrame(frame, filename);
		Director::getInstance()->getTextureCache()->removeTextureForKey(filename);
		_list.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(filename));
	}
	Animate* pAnimation = Animate::create(Animation::createWithSpriteFrames(_list, 0.1f));
	mLoading->runAction(RepeatForever::create(pAnimation));
	pSprite->addChild(mLoading);
	mListRoom->clear();
	mLoading->setPosition(Vec2(pSprite->getContentSize().width / 2, pSprite->getContentSize().height / 2));
	mLoading->setScale(0.6f);
	roomLoadStruct = new loadRoomStruct();
	roomLoadStruct->mCacheListTable = mCacheListRoom;
	roomLoadStruct->pSpriteLoading = mLoading;
	roomLoadStruct->pButtonChoose = pSprite;
	roomLoadStruct->mListBetMoney = mListMoneyBet;
	roomLoadStruct->mListRoom = mListRoom;
	roomLoadStruct->mRoomID = mRoomID;
	//roomStruc->mRoomServers = mRoomServers;


	__String* pGameIdstr = __String::createWithFormat("%d", mGameID);
	roomLoadStruct->mRoomLevel = pRoomLevelstr;
	roomLoadStruct->mGameID = pGameIdstr->getCString();
	mCacheListRoom->clear();
	mListMoneyBet->clear();
	mCountTab = 0;
	roomLoadStruct->mRoomID = index - 1;
	_isLoadTableContinue = true;

	/*pthread_t t1;
	pthread_create(&t1, NULL, &pthread_loadRoom, roomLoadStruct);*/

}
void ChooseRoomScene::setEnableButtonChoosePlace(bool pBool) {
	/*for (int i = 4; i >= 1; i--) {
		EGButtonSprite* pSprite = (EGButtonSprite*)((Sprite*)mLayerPlace->getChildByTag(i));
		pSprite->stopAllActions();
		pSprite->stopActionByTag(2);
		Action *action = ScaleTo::create(0.2f, 1);
		action->setTag(2);
		pSprite->runAction(action);
		pSprite->setTexture(Director::getInstance()->getTextureCache()->addImage(__String::createWithFormat("choose_place%d_1.png", i)->getCString()));
	}*/
}

void* pthread_loadTable(void* _void)
{
	//loadRoomStruct* pStructRoom = (loadRoomStruct*)_void;
	//int pConnection = 0;
	//bool iFirstTime = true;
	//pConnection = client_loadTable->connectToServer(IP_SERVER_ACCOUNT, PORT_SERVER_ACCOUNT);
	//iFirstTime = true;
	//isExitByUser = false;
	//if (pConnection == 0) {
	//	MpLoadTablesMessageRequest * pRequest = new MpLoadTablesMessageRequest();
	//	pRequest->setTokenId(GameInfo::mUserInfo.mTokenID);
	//	pRequest->setGameLevel(atoi(pStructRoom->mRoomLevel.c_str()));
	//	pRequest->setGameId(atoi(pStructRoom->mGameID.c_str()));
	//	pRequest->setRoomId(pStructRoom->mRoomID);
	//	//client_loadTable->sendMsg(pRequest->getMsg());
	//	GameInfo::mUserInfo.mLstTcpTimeout.push_back(&client_loadTable);

	//	delete pRequest;
	//	pRequest = NULL;
	//	if (iFirstTime){
	//		client_loadTable->startListener();
	//		client_loadTable->receiveMessageHandler();
	//		pthread_mutex_lock(&GameInfo::mUserInfo.pthreadlock);
	//		if (client_loadTable){
	//			if (client_loadTable->getReasonKill() == KILL_BY_SEVER || client_loadTable->getReasonKill() == KILL_BY_TIME_OUT) {
	//				mErrorCode_JoinRoom1 = KILL_BY_SEVER;
	//			}
	//			delete client_loadTable;
	//			client_loadTable = NULL;
	//		}
	//		pthread_mutex_unlock(&GameInfo::mUserInfo.pthreadlock);
	//		/*	if (!iCreateGame  && pStructRoom && pStructRoom->pLoading){
	//				pStructRoom->pLoading->hide();
	//				}*/
	//	}
	//}
	//else {
	//	delete client_loadTable;
	//	client_loadTable = NULL;

	//	mErrorCode_ChooseRoom = TEXT_ERR_NOCONNECTION;
	//	//pStructRoom->pLoading->hide();
	//	((Loading*)pStructRoom->pSpriteLoading)->hide();
	//	delete pStructRoom;
	//}
	//pthread_exit(NULL);
	//return NULL;

	loadRoomStruct* pStructRoom = (loadRoomStruct*)_void;
	MpMessageRequest* request = new MpMessageRequest(MP_LOAD_TABLES);
	request->setTokenId(GameInfo::mUserInfo.mTokenID);
	/*request->addUInt16(MP_GAME_ID, atoi(pStructRoom->mGameID.c_str()));
	request->addUInt8(MP_GAME_LVL, atoi(pStructRoom->mRoomLevel.c_str()));
	request->addUInt8(MP_ROOM_ID, atoi(pStructRoom->mRoomLevel.c_str()));*/
	request->addInt(MP_PARAM_TABLE_ID, CREATE_TID(atoi(pStructRoom->mGameID.c_str()), 0, atoi(pStructRoom->mRoomLevel.c_str()), 0xff));
	MpClientManager::getInstance(1)->sendMessage(request, true);
	//delete pStructRoom;
	pthread_exit(NULL);
	return NULL;
}


void* pthread_loadRoom(void* _void)
{
	//loadRoomStruct* pStructRoom = (loadRoomStruct*)_void;
	//client_loadRoom = new TcpClient();
	//int pConnection = client_loadRoom->connectToServer(mIP[atoi(pStructRoom->mGameID.c_str())].c_str(), mPort[atoi(pStructRoom->mGameID.c_str())]);
	//if (pConnection == 0) {

	//	MpLoadRoomMessageRequest* pRequest = new MpLoadRoomMessageRequest();
	//	pRequest->setTokenId(GameInfo::mUserInfo.mTokenID);
	//	pRequest->setGameLevel(atoi(pStructRoom->mRoomLevel.c_str()));
	//	pRequest->setGameId(atoi(pStructRoom->mGameID.c_str()));
	//	//client_loadRoom->sendMsg(pRequest->getMsg());
	//	GameInfo::mUserInfo.mLstTcpTimeout.push_back(&client_loadRoom);
	//	delete pRequest;
	//	pRequest = NULL;

	//	MsgHeader_t *pMsg = client_loadRoom->getMsgResponse();

	//	pthread_mutex_lock(&GameInfo::mUserInfo.pthreadlock);
	//	if (client_loadRoom){
	//		client_loadRoom->close();
	//		delete client_loadRoom;
	//		client_loadRoom = NULL;
	//	}
	//	pthread_mutex_unlock(&GameInfo::mUserInfo.pthreadlock);

	//	if (pMsg){
	//		MpLoadRoomMessageResponse *pResponse = new MpLoadRoomMessageResponse(*pMsg);

	//		FreeMsg(pMsg);
	//		pMsg = NULL;
	//		unsigned pErrorCode = pResponse->getErrorCode();
	//		if (pErrorCode != 0){
	//			mErrorCode_ChooseRoom = __String::createWithFormat("%d", pErrorCode)->getCString();

	//		}
	//		else{

	//			pResponse->getRoomStates(*pStructRoom->mListRoom);
	//			pResponse->getBetMoney(*pStructRoom->mListBetMoney);
	//			delete pResponse;
	//			pResponse = NULL;

	//			mErrorCode_ChooseRoom = TEXT_SUCCESS_LOADROOM;
	//		}
	//	}
	//	else{
	//		mErrorCode_ChooseRoom = TEXT_ERR_NOCONNECTION;
	//	}
	//}
	//else {
	//	mErrorCode_ChooseRoom = TEXT_ERR_NOCONNECTION;
	//}


	//pStructRoom->pSpriteLoading->setVisible(false);
	//pStructRoom->pSpriteLoading->stopAllActions();
	//pStructRoom->pButtonChoose->getChildren().at(0)->setVisible(false);
	//delete pStructRoom;
	//iChoosePlace = false;
	//pthread_exit(NULL);
	//return NULL;
	loadRoomStruct* pStructRoom = (loadRoomStruct*)_void;
	//MpMessageRequest* request = new MpMessageRequest(MP_LOAD_ROOM);
	MpMessageRequest* request = new MpMessageRequest(MP_MSG_JOIN_TABLE);
	request->setTokenId(GameInfo::mUserInfo.mTokenID); 
	request->addUInt16(MP_GAME_ID, atoi(pStructRoom->mGameID.c_str()));
	request->addUInt8(MP_GAME_LVL, atoi(pStructRoom->mRoomLevel.c_str()));
	request->addInt(MP_PARAM_TABLE_ID, CREATE_TID(atoi(pStructRoom->mGameID.c_str()), atoi(pStructRoom->mRoomLevel.c_str()), pStructRoom->mRoomID, 0xff));
	MpClientManager::getInstance(1)->sendMessage(request, true);
	//delete pStructRoom;
	pthread_exit(NULL);
	return NULL;
}

#include "BoxNoticeManager.h"

void ChooseRoomScene::update(float pSec) {

	MpMessage* msg = MpManager::getInstance()->getMessenger();
	if (msg){
		switch (msg->getType())
		{
		case MP_LOAD_ROOM_ACK:
			executeLoadRoom(msg);
			break;
		case MP_LOAD_TABLES_ACK:
			executeLoadTable(msg);
			break;
		case MP_MSG_JOIN_TABLE_ACK:
			executeJoinTable(msg);
			break;
		default:
			break;
		}
		delete msg;
		msg = NULL;
	}

	if (getErrorCode() != "") {
		mErrorCode_ChooseRoom = getErrorCode();
		setErrorCode("");
	}
	if (mErrorCode_ChooseRoom != "") {
		if (mErrorCode_ChooseRoom == TEXT_EER_LOADROOM || mErrorCode_ChooseRoom == TEXT_ERR_NOCONNECTION) {
			setEnableButtonChoosePlace(true);
			WXmlReader* pXml = WXmlReader::create();
			pXml->load(KEY_XML_FILE);

			BoxNoticeManager::getBoxNotice(mBoxNotice,
				pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				pXml->getNodeTextByTagName(mErrorCode_ChooseRoom.c_str()).c_str(), [=] {
				Scene*_scene = HomeScene::scene();
				Director::getInstance()->replaceScene(_scene);
			});
			mBoxNotice->setVisible(true);
			mErrorCode_ChooseRoom = "";
		}
		else if (mErrorCode_ChooseRoom == TEXT_SUCCESS_LOADROOM) {
			mErrorCode_ChooseRoom = "";
			loadListRoom();
			nextStageChooseRoom();
			GameInfo::mUserInfo.mLevelRoom = atoi(pRoomLevelstr.c_str());
			int indexTab = GameInfo::mUserInfo.mTabChoose;
			chooseTab(indexTab, mCountTab);
			return;
		}
		else if (mErrorCode_ChooseRoom == TEXT_SUCCESS_LOADTABLE) {
			mErrorCode_ChooseRoom = "";
			loadListTable();
			return;
		}
		else if (mErrorCode_ChooseRoom == TEXT_SUCCESS_JOINROOM) {
			mErrorCode_ChooseRoom = "";
			createGame(mInfoRoom);
			return;
		}
		else if (mErrorCode_ChooseRoom == TEXT_SUCCESS_LOGIN) {
			mErrorCode_ChooseRoom = "";
			replay();
			return;
		}
		else if (mErrorCode_ChooseRoom == TEXT_SUCCESS_PLAYNOW) {
			joinInvite();
			mErrorCode_ChooseRoom = "";
		}
		else {
			WXmlReader* pXml = WXmlReader::create();
			pXml->load(KEY_XML_FILE);
			if (mErrorCode_ChooseRoom != "314") {
				BoxNoticeManager::getBoxNotice(mBoxNotice,
					pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
					pXml->getNodeTextByTagName(mErrorCode_ChooseRoom).c_str(), [=] {

					Scene* _scene = HomeScene::scene();
					Director::sharedDirector()->getInstance()->replaceScene(_scene);

				});
			}
			else {
				if (mErrorCode_ChooseRoom.substr(0, 5).c_str() == "20578") {

					BoxNoticeManager::getBoxNotice(mBoxNotice,
						pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
						pXml->getNodeTextByTagName("20578").c_str(), [=] {

						mBoxNotice->setVisible(false);

					});
				}
				else {
					BoxNoticeManager::getBoxNotice(mBoxNotice,
						pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
						pXml->getNodeTextByTagName(mErrorCode_ChooseRoom).c_str(), [=] {

						mBoxNotice->setVisible(false);

					});
				}
			}
			mBoxNotice->setVisible(true);
			mErrorCode_ChooseRoom = "";
		}


	}
	if (mErrorCode_JoinRoom != "") {
		if (mErrorCode_JoinRoom == "294257") {
			WXmlReader* pXml = WXmlReader::create();
			pXml->load(KEY_XML_FILE);

			BoxNoticeManager::getBoxNotice(mBoxNotice,
				pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				pXml->getNodeTextByTagName("error_double_login").c_str(), nullptr, [=] {
				Scene* _scene = HomeScene::scene();
				Director::sharedDirector()->getInstance()->replaceScene(_scene);
			});
			mBoxNotice->setVisible(true);
			mErrorCode_JoinRoom = "";
		}
		else {
			WXmlReader* pXml = WXmlReader::create();
			pXml->load(KEY_XML_FILE);
			if (mErrorCode_JoinRoom == "294267") {
				BoxNoticeManager::getBoxConfirm(mBoxNotice,
					pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
					pXml->getNodeTextByTagName(mErrorCode_JoinRoom.c_str()).c_str(), [=] {
					Scene* _scene = MainScene::sceneWithRecharge();
					Director::sharedDirector()->getInstance()->replaceScene(_scene);
				}, [=] {
					mBoxNotice->setVisible(false);
					//schedule(schedule_selector(ChooseRoomScene::updateInfoTable));
					mRoomID = -1;
					replay();
					mBoxNotice->setVisible(false);
				});

			}
			else {
				if (mErrorCode_JoinRoom.substr(0, 5) == "20578") {
					BoxNoticeManager::getBoxNotice(mBoxNotice,
						pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
						pXml->getNodeTextByTagName("20578").c_str());
					mBoxNotice->setVisible(false);
				}
				else {
					BoxNoticeManager::getBoxNotice(mBoxNotice,
						pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
						pXml->getNodeTextByTagName(mErrorCode_JoinRoom.c_str()).c_str(), [=] {
						//schedule(schedule_selector(ChooseRoomScene::updateInfoTable));
						mRoomID = -1;
						replay();
						mBoxNotice->setVisible(false);
					});
				}
			}
			mBoxNotice->setVisible(true);
			mErrorCode_JoinRoom = "";
		}
	}
	if (mErrorCode_JoinRoom1 != "") {
		if (mBoxNotice->isVisible()) {
			mLoadingMain->setVisible(false);
		}
		else {
			WXmlReader* pXml = WXmlReader::create();
			pXml->load(KEY_XML_FILE);
			BoxNoticeManager::getBoxNotice(mBoxNotice,
				pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				pXml->getNodeTextByTagName("content_lostconnect").c_str(),
				[=] {
				Scene* _scene = HomeScene::scene();
				Director::getInstance()->replaceScene(_scene);
			});
			mBoxNotice->setVisible(true);
			mLoadingMain->setVisible(false);
		}
		mErrorCode_JoinRoom1 = "";
	}
}
void ChooseRoomScene::nextStageChooseRoom() {
	mCurrentStage = 1;
	updateStage(0.25f);
}
void ChooseRoomScene::backStage() {
	_isLoadTableContinue = false;
	if (mLoadingMain->isVisible() || (mLoading && mLoading->isVisible())) {
		return;
	}
	if (mCurrentStage == 1) {
		mRoomID = -1;
		GameInfo::mUserInfo.mTabChoose = 0;
		mCurrentStage = 0;
		updateStage(0.25f);
		GameInfo::mUserInfo.iStage = true;
		/*client_loadTable->setReasonKill(KILL_BY_CLIENT);
		client_loadTable->close();*/
		isExitByUser = true;
		MpMessageRequest * request = new MpMessageRequest(MP_LEAVE_ROOM);
		request->addInt(MP_GAME_ID, mGameID);
		request->setTokenId(GameInfo::mUserInfo.mTokenID);
		//TcpClient* client = new TcpClient();
		//int pConnect = client->connectToServer(IP_SERVER_ACCOUNT, PORT_SERVER_ACCOUNT);
		//if (pConnect == 0){
		//	//client->sendMsg(request->getMsg());

		//	//std::this_thread::sleep_for(std::chrono::seconds(1));

		//}
		//delete client;
		delete request;
	}
	else {
		Director::getInstance()->replaceScene(TransitionFade::create(0.25f, MainScene::scene(0)));
	}
}
void ChooseRoomScene::onBackPressed() {
	backStage();
}
void ChooseRoomScene::loadListRoom() {
	for (int i = 0; i < mCountTab; i++) {
		EGButtonSprite* buttonTab = (EGButtonSprite*)mLayerChooseRoom->getChildByTag(TAG_TAB_CHOOSE_ROOM + i);
		buttonTab->removeAllChildrenWithCleanup(true);
		buttonTab->removeFromParentAndCleanup(true);
	}
	createTab(mListRoom->size());
	mLayerChooseRoom->setListBetMoney(mListMoneyBet);
	//mBoxChooseBet->clearAllButton();
	//mLayerChooseRoom->loadListRoom(mCacheListRoom);
	//for (int i = mListMoneyBet->size() - 1; i >= 0; i--){
	//	int index = mListMoneyBet->size() - 1 - i;
	//	EGButtonSprite* buttonChoose = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_CHOOSE_CR2);
	//	mBoxChooseBet->addButton(buttonChoose);

	//	mBoxChooseBet->addChild(buttonChoose);
	//	buttonChoose->setButtonType(EGButton2FrameDark);
	//	buttonChoose->setOnTouch(CC_CALLBACK_0(ChooseRoomScene::chooseBetMoney, this, index + 1));

	//	addTextforButton(buttonChoose, mListMoneyBet->at(index).c_str());
	//}
}
void ChooseRoomScene::loadListTable() {
	mLayerChooseRoom->loadListRoom(mCacheListRoom);
}
void ChooseRoomScene::updateStage(float pDuration) {
	Size _size = Director::sharedDirector()->getWinSize();
	mLayerPlace->runAction(MoveTo::create(pDuration, Vec2(mCurrentStage*_size.width, 0)));
	mLayerChooseRoom->runAction(MoveTo::create(pDuration, Vec2((mCurrentStage - 1)*_size.width, 0)));
	setEnableButtonChoosePlace(true);
}
Scene* ChooseRoomScene::createGame(void* pInfo) {
	iCreateGame = true;
	unscheduleAllSelectors();
	unscheduleUpdate();
	Scene *_scene = GameScene::createScene(pInfo);
	//runAction(Sequence::createWithTwoActions(DelayTime::create(0.5f), CallFunc::create([=]{

	TransitionFade* pTrans = TransitionFade::create(0.5f, _scene);
	Director::getInstance()->replaceScene(pTrans);
	//})));

	return _scene;
}
void ChooseRoomScene::chooseRoom(int index) 
{
	if (!mLoadingMain->isVisible()) {
		mPlayerList->clear();
		mLoadingMain->setSize(Size(CLIENT_WIDTH, CLIENT_HEIGHT));
		mLoadingMain->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
		//mInfoRoom->mTcp = new TcpClient();
		mInfoRoom->mTableID = index;
		mInfoRoom->pTypeRoom = 0;
		if (mCacheListRoom->size() > 0) {
			if (mCacheListRoom->at(mLayerChooseRoom->getIDChoosed()).team1 > 0) {
				mInfoRoom->pTypeRoom = 1;
			}
			mInfoRoom->pBetMoney = mLayerChooseRoom->getInfoRoom().betMoney;
			mInfoRoom->pMaxPlayer = mLayerChooseRoom->getInfoRoom().maxPlayer;
			if (mLayerChooseRoom->getInfoRoom().team1 == 0) {
				if (mLayerChooseRoom->getInfoRoom().totalPlayer > 0) {
					mInfoRoom->pTypeRoom = 0;
				}
			}
		}
		mInfoRoom->mIDRoom = mRoomID;
		mInfoRoom->pRoomLevelstr = pRoomLevelstr;
		mInfoRoom->pLoading = mLoadingMain;
		mInfoRoom->mListPlayer = mPlayerList;

		mInfoRoom->mGameID = mGameID;
		mInfoRoom->mListBetMoney = mListMoneyBet;


		mInfoRoom->iCreater = false;
		mLoadingMain->show();
		MpMessageRequest* pRequest = new MpMessageRequest(MP_MSG_JOIN_TABLE);
		std::string pToken = GameInfo::mUserInfo.mTokenID;
		pRequest->setTokenId(pToken.c_str());
		unsigned tableid;
		if (mInfoRoom->mGameID != 1) {
			tableid = CREATE_TID(mInfoRoom->mGameID, atoi(mInfoRoom->pRoomLevelstr.c_str()), mRoomID, index);
		}
		else {
			tableid = CREATE_TID(mInfoRoom->mGameID, mInfoRoom->mTableID, 0, 0xff);
		}
		if (GameInfo::mUserInfo.isReConnect) {
			GameInfo::mUserInfo.isReConnect = false;
			tableid = mInfoRoom->mTableID;
		}
		pRequest->addString(MP_PARAM_USERNAME, GameInfo::mUserInfo.mUsername);
		pRequest->addInt(MP_PARAM_TABLE_ID, tableid);
		if (mInfoRoom->mGameID == 0)
			MpClientManager::getInstance(1)->sendMessage(pRequest, true);
		else
			MpClientManager::getInstance(2)->sendMessage(pRequest, true);
		//schedule(schedule_selector(ChooseRoomScene::updateJoinTable), 0);
		_isLoadTableContinue = false;
	}
}
void ChooseRoomScene::chooseTab(int index, int pCountTab) 
{
	this->stopActionByTag(100);
	if (m_isResume || mRoomID != index) {
		if (index < 0) {
			index = 0;
		}
		m_isResume = false;
		for (int i = pCountTab - 1; i >= 0; i--) {
			int index = pCountTab - 1 - i;
			EGSprite* pTab = (EGSprite*)mLayerChooseRoom->getChildByTag(TAG_TAB_CHOOSE_ROOM + index);
			pTab->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_TAB_CHOOSE_ROOM, 1)->getCString()));
		}
		mLoadingMain->setSize(Size(CLIENT_WIDTH, 360));
		mLoadingMain->setPosition(Vec2(CLIENT_WIDTH / 2, 360 / 2));
		mLoadingMain->show();
		mCacheListRoom->clear();
		EGSprite* pButtonTab = (EGSprite*)mLayerChooseRoom->getChildByTag(TAG_TAB_CHOOSE_ROOM + index);
		pButtonTab->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_TAB_CHOOSE_ROOM, 2)->getCString()));
		if (roomLoadStruct)
			delete roomLoadStruct;
		roomLoadStruct = new loadRoomStruct();

		mRoomID = index;
		roomLoadStruct->mRoomID = index;
		/*client_loadTable = new TcpClient();
		roomStruct->mTcp = client_loadTable;*/
		roomLoadStruct->mGameID = String::createWithFormat("%d", mGameID)->getCString();
		roomLoadStruct->mRoomLevel = pRoomLevelstr.c_str();
		roomLoadStruct->mCacheListTable = mCacheListRoom;
		roomLoadStruct->pSpriteLoading = mLoadingMain;
		roomLoadStruct->mListBetMoney = mListMoneyBet;
		GameInfo::mUserInfo.mTabChoose = index;
		//pthread_t t1;
		//pthread_create(&t1, NULL, &pthread_loadTable, roomLoadStruct);
		reloadTableRequest(1);

	}
}
void ChooseRoomScene::joinInvite() {
	//unschedule(schedule_selector(ChooseRoomScene::updateInfoTable));

	mPlayerList->clear();
	mLoadingMain->setSize(Size(CLIENT_WIDTH, CLIENT_HEIGHT));
	mLoadingMain->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	mInviteInfo->pTypeRoom = 0;
	mInviteInfo->pLoading = mLoadingMain;
	mInviteInfo->mListPlayer = mPlayerList;
	mInviteInfo->mListBetMoney = mListMoneyBet;
	mInviteInfo->pMaxPlayer = 4;
	mInviteInfo->iCreater = false;
	mLoadingMain->show();
	MpMessageRequest* pRequest = new MpMessageRequest(MP_MSG_QUICK_JOIN_TABLE);
	std::string pToken = GameInfo::mUserInfo.mTokenID;
	pRequest->setTokenId(pToken);
	unsigned tableid;
	if (mInviteInfo->mGameID != 1) {
		tableid = CREATE_TID(mInviteInfo->mGameID, atoi(mInviteInfo->pRoomLevelstr.c_str()), 0xff, 0xff);
	}
	else {
		tableid = CREATE_TID(mInviteInfo->mGameID, mInviteInfo->mTableID, 0xff, 0xff);
	}
	if (GameInfo::mUserInfo.isReConnect) {
		GameInfo::mUserInfo.isReConnect = false;
		tableid = mInviteInfo->mTableID;
	}
	pRequest->addString(MP_PARAM_USERNAME, GameInfo::mUserInfo.mUsername);
	pRequest->addInt(MP_PARAM_TABLE_ID, tableid);
	MpClientManager::getInstance(1)->sendMessage(pRequest, true);
	//schedule(schedule_selector(ChooseRoomScene::updateJoinTable), 0);
}
void ChooseRoomScene::updateInfoTable(float pSec) 
{
	
	/*{
		if (response->getMsg()->msgType == MP_LOAD_TABLES_NTFY){
			MpLoadTablesMessageResponse *pResponse = (MpLoadTablesMessageResponse*)response;
			FreeMsg(pMsg);
			pMsg = NULL;
			std::vector<ClientTableInfo>  pListRoom;
			pResponse->getTableInfos(pListRoom);
			for (int i = 0; i < mCacheListRoom->size(); i++){
				bool iExist = false;
				for (int j = 0; j < pListRoom.size(); j++){
					ClientTableInfo* pInfo = &pListRoom.at(j);
					if (pInfo->id == i){
						mCacheListRoom->at(i) = *pInfo;
						iExist = true;
					}
				}
			}
			mErrorCode_ChooseRoom = TEXT_SUCCESS_LOADTABLE;
		}
	}*/
}
void ChooseRoomScene::updateJoinTable(float pSec) {
	roomStruct* pInfo = mInfoRoom;
	MpMessage* pMsg = MpManager::getInstance()->getMessenger();
	if (pMsg) {
		switch (pMsg->getType())
		{
		case MP_MSG_JOIN_TABLE_ACK:
			MpJoinTableMessageResponse *pResponse = (MpJoinTableMessageResponse*)pMsg;

			unsigned  pErrorCode = pResponse->getErrorCode();
			if (pErrorCode != 0) {
				mErrorCode_JoinRoom = __String::createWithFormat("%d", pErrorCode)->getCString();
				mLoadingMain->hide();
			}
			else {
				std::vector<ClientPlayerInfo> pListPlayer;
				std::vector<ReadyPlayer> pListReady;
				pResponse->getListReadyPlayer(pListReady);
				pResponse->getListPlayer(pListPlayer);
				pResponse->getListItem(pInfo->mListItemSp);
				pResponse->getListFishPrice(pInfo->mListPrice);
				pInfo->mTableID = pResponse->getInt(MP_PARAM_TABLE_ID);
				pInfo->mDurationReadyNormal = 30;
				pInfo->mDurationReadyHost = 30;
				pResponse->getDefaultBetMoney(pInfo->pBetMoneyDefault);
				uint16_t pDuration;
				uint32_t pFireMoney = pResponse->getInt(MP_PARAM_FIRE_MONEY);
				pResponse->getGameDuration(pDuration);
				std::string strListFactor; pResponse->getString(MP_PARAM_LIST_XFACTOR, strListFactor);
				for (int i = 0; i < strListFactor.length(); i++) {
					pInfo->mListXFactor.push_back(strListFactor[i]);
				}
				pInfo->mDuration = pDuration;
				uint32_t pMinBetMoney;
				pMinBetMoney = pResponse->getInt(MP_PARAM_BET_MONEY);
				std::string strXFactor; pResponse->getString(MP_PARAM_XFACTOR, strListFactor);

				if (pInfo->mListBetMoney && pInfo->mListBetMoney->size() > 0)
					pInfo->mListBetMoney->clear();
				
				for (int i = 0; i < 3; i++){
				pInfo->mListBetMoney->push_back(pMinBetMoney *LIST_BET_RATE[i]);
				}
				pInfo->pBetMoney = pMinBetMoney;

				pInfo->mRatio = pResponse->getRatioLevel();
				pInfo->mBackGroundID = pResponse->getBackgroundId();
				std::string data;
				int len = 0;
				pResponse->getString(MP_PARAM_FISH_MONEY, data);
				for (int i = pListPlayer.size() - 1; i >= 0; i--) {
					int index = pListPlayer.size() - 1 - i;
					ClientPlayerInfo playerInfo = pListPlayer.at(index);
					ClientPlayerInfoEX playerInfoGame;
					playerInfoGame.balance = playerInfo.balance;
					playerInfoGame.balanceDefault = playerInfo.balance;
					playerInfoGame.host = playerInfo.host;
					playerInfoGame.level = playerInfo.level;
					playerInfoGame.position = playerInfo.position;
					playerInfoGame.score = *((uint32_t*)(data.data() + len));
					playerInfoGame.score = ntohl(playerInfoGame.score);
					len += 4;
					playerInfoGame.username = playerInfo.username;
					playerInfoGame.disallowKick = playerInfo.disallowKick;
					for (int j = 0; j < pListReady.size(); j++) {
						ReadyPlayer  readyPlayer = pListReady.at(j);
						if (readyPlayer.position == playerInfoGame.position) {
							playerInfoGame.isReady = readyPlayer.status;
							break;
						}
					}

					playerInfoGame.itemId = playerInfo.itemId;
					if (playerInfo.itemId == 0) {
						playerInfoGame.itemId = 101;
					}
					pInfo->mListPlayer->push_back(playerInfoGame);
				}
				if (strXFactor.length() == pInfo->mListPlayer->size()) {
					for (size_t i = 0; i < pInfo->mListPlayer->size(); i++) {
						pInfo->mListPlayer->at(i).indexX = strXFactor[i];
					}
				}
				GameScene *scene = ((GameScene*)createGame(pInfo));
				if (pInfo->mGameID == 1) {
					scene->setRemainTime(pResponse->getInt(MP_REMAIN_TIME));
					scene->setFireMoney(pFireMoney);
				}
				mErrorCode_ChooseRoom = "";


				this->unscheduleAllSelectors();
			}
			break;
		}
		delete pMsg;
		pMsg = NULL;
	}

}
void* pthread_reconect_chooseScene(void *_void) 
{
	//ReconectStruct* pLoginStruct = (ReconectStruct*)_void;
	//client_login_chooseScene = new TcpClient();
	//int pConnect = client_login_chooseScene->connectToServer(IP_SERVER_ACCOUNT, PORT_SERVER_ACCOUNT);
	//if (pConnect == 0) {
	//	MpLoginRequest *pRequest = NULL;
	//	if (GameInfo::mUserInfo.mPassWord == "") {
	//		pRequest = new MpLoginRequest(MP_GUEST_LOGIN);
	//	}
	//	else {
	//		pRequest = new MpLoginRequest();
	//	}
	//	pRequest->setUsername(pLoginStruct->pUsername.c_str());
	//	pRequest->setPassword(pLoginStruct->pPasswords.c_str());
	//	pRequest->setCpId(pLoginStruct->pCPID.c_str());
	//	pRequest->setVersion(VERSION);
	//	pRequest->setGameGroup(GROUP_ID);
	//	pRequest->addString(MP_PARAM_DEVICE_ID, pLoginStruct->pDeviceID.c_str());
	//	std::string pOS = os_prefix;
	//	pRequest->setOs(pOS.c_str());
	//	std::string pKey = pLoginStruct->pUsername + "89f4f96a62c7a49b204921033b68f80c";
	//	if (GameInfo::mUserInfo.mPassWord == "") {
	//		pKey = pLoginStruct->pDeviceID + "89f4f96a62c7a49b204921033b68f80c";
	//	}
	//	for (int i = 0; i < pKey.length(); i++) {
	//		pKey[i] = std::tolower(pKey[i]);
	//	}
	//	pRequest->setKey(pKey);
	//	//client_login_chooseScene->sendMsg(pRequest->getMsg());

	//	delete pRequest;
	//	pRequest = NULL;

	//	GameInfo::mUserInfo.mLstTcpTimeout.push_back(&client_login_chooseScene);
	//	MsgHeader *pMsg = client_login_chooseScene->getMsgResponse();
	//	pthread_mutex_lock(&GameInfo::mUserInfo.pthreadlock);
	//	if (client_login_chooseScene) {
	//		client_login_chooseScene->close();
	//	}
	//	pthread_mutex_unlock(&GameInfo::mUserInfo.pthreadlock);

	//	if (pMsg == NULL) {
	//		mErrorCode_ChooseRoom = TEXT_ERR_TIMEOUT;
	//	}
	//	else 
	//	{
	//		MpLoginResponse* pLoginResponse = (MpLoginResponse*)new MpMessageResponse(*pMsg);

	//		FreeMsg(pMsg);
	//		pMsg = NULL;

	//		unsigned int pErrorCode = pLoginResponse->getErrorCode(); //102262
	//		if (pErrorCode != 0) {
	//			GameInfo::mUserInfo.mUrlUpdate = pLoginResponse->getErrorCode();

	//			if (pErrorCode == 102263) {
	//				//if (pLoginStruct->mCheck->isVisible()) {
	//				//	/*UserDefault::getInstance()->setStringForKey(KEY_USERNAME, pLoginStruct->pUsername);
	//				//	UserDefault::getInstance()->setStringForKey(KEY_PASSWORDS, pLoginStruct->pPasswords);*/
	//				//}

	//				GameInfo::mUserInfo.mUsername = pLoginStruct->pUsername.c_str();
	//				for (int i = 0; i < GameInfo::mUserInfo.mUsername.length(); i++) {
	//					GameInfo::mUserInfo.mUsername[i] = std::tolower(GameInfo::mUserInfo.mUsername[i]);
	//				}
	//				GameInfo::mUserInfo.mFullname = pLoginResponse->getFullname();
	//				GameInfo::mUserInfo.mEmail = pLoginResponse->getEmail();
	//				GameInfo::mUserInfo.mBirthday = pLoginResponse->getBirthday();
	//				GameInfo::mUserInfo.mBalance = pLoginResponse->getBalance();
	//				GameInfo::mUserInfo.mGender = pLoginResponse->getGender();
	//				GameInfo::mUserInfo.mLevel = pLoginResponse->getLevel();
	//				GameInfo::mUserInfo.mTokenID = pLoginResponse->getTokenId();
	//				GameInfo::mUserInfo.mAvatarID = pLoginResponse->getAvatarId();
	//				// reconnect only


	//				GameInfo::mUserInfo.mServiceNumber = "";
	//				GameInfo::mUserInfo.mSyntax = "";
	//				GameInfo::mUserInfo.mChargeID = pLoginStruct->mChargeID;

	//				pLoginStruct->mLoading->hide();
	//				mErrorCode_ChooseRoom = TEXT_SUCCESS_LOGIN;
	//				delete pLoginResponse;
	//				pLoginResponse = NULL;
	//			}
	//			else {
	//				delete pLoginResponse;
	//				pLoginResponse = NULL;

	//				mErrorCode_ChooseRoom = __String::createWithFormat("%d", pErrorCode)->getCString();
	//			}
	//		}
	//		else {
	//			//if (pLoginStruct->mCheck->isVisible()) {
	//			//	/*UserDefault::getInstance()->setStringForKey(KEY_USERNAME, pLoginStruct->pUsername);
	//			//	UserDefault::getInstance()->setStringForKey(KEY_PASSWORDS, pLoginStruct->pPasswords);*/
	//			//}

	//			GameInfo::mUserInfo.mUsername = pLoginStruct->pUsername.c_str();
	//			for (int i = 0; i < GameInfo::mUserInfo.mUsername.length(); i++) {
	//				GameInfo::mUserInfo.mUsername[i] = std::tolower(GameInfo::mUserInfo.mUsername[i]);
	//			}
	//			GameInfo::mUserInfo.mFullname = pLoginResponse->getFullname();
	//			GameInfo::mUserInfo.mEmail = pLoginResponse->getEmail();
	//			GameInfo::mUserInfo.mBirthday = pLoginResponse->getBirthday();
	//			GameInfo::mUserInfo.mBalance = pLoginResponse->getBalance();
	//			GameInfo::mUserInfo.mGender = pLoginResponse->getGender();
	//			GameInfo::mUserInfo.mLevel = pLoginResponse->getLevel();
	//			GameInfo::mUserInfo.mTokenID = pLoginResponse->getTokenId();
	//			GameInfo::mUserInfo.mAvatarID = pLoginResponse->getAvatarId();
	//			// reconnect only

	//			GameInfo::mUserInfo.mServiceNumber = "";
	//			GameInfo::mUserInfo.mSyntax = "";
	//			GameInfo::mUserInfo.mChargeID = pLoginStruct->mChargeID;



	//			mErrorCode_ChooseRoom = TEXT_SUCCESS_LOGIN;
	//			pLoginStruct->mLoading->hide();
	//			delete pLoginResponse;
	//			pLoginResponse = NULL;
	//		}
	//	}
	//}
	//else {
	//	delete client_login_chooseScene;
	//	client_login_chooseScene = NULL;

	//	mErrorCode_ChooseRoom = TEXT_ERR_NOCONNECTION;
	//}

	//pLoginStruct->mLoading->hide();

	//delete pLoginStruct;
	//pLoginStruct = NULL;

	pthread_exit(NULL);
	return NULL;
}
void ChooseRoomScene::pauseGame() {

}
void ChooseRoomScene::resumeGame() {
	//Scene* pScene = HomeScene::scene();
	//TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
	//Director::getInstance()->replaceScene(pTrans);
}

void ChooseRoomScene::executeLoadRoom(MpMessage* pMsg)
{
	mListRoom->clear();
	std::string content = "";
	pMsg->getString(MP_PARAM_LIST_ROOM, content);
	uint8_t *ptr = (uint8_t*) content.data();
	for (uint8_t i = 0; i < content.size(); ++i){
		mListRoom->push_back(content[i]);
	}
	mErrorCode_ChooseRoom = TEXT_SUCCESS_LOADROOM;

	//reloadTableRequest(1);
}

void ChooseRoomScene::replay() {
	//int pRoomLevel = GameInfo::mUserInfo.mLevelRoom;
	//if (pRoomLevel >= 0) {
	//	choosePlace(pRoomLevel + 1);
	//}
}

void ChooseRoomScene::reloadTableRequest(float dt)
{
	if (!_isLoadTableContinue) return;
	MpMessageRequest* request = new MpMessageRequest(MP_LOAD_TABLES);
	request->setTokenId(GameInfo::mUserInfo.mTokenID);
	request->addInt(MP_PARAM_TABLE_ID, CREATE_TID(atoi(roomLoadStruct->mGameID.c_str()), atoi(roomLoadStruct->mRoomLevel.c_str()), roomLoadStruct->mRoomID, 0xff));
	MpClientManager::getInstance(1)->sendMessage(request, true);
}

void ChooseRoomScene::executeLoadTable(MpMessage* pMsg)
{
	std::vector<ClientTableInfo>  pListRoom;
	std::string content = "";
	pMsg->getString(MP_PARAM_LIST_TABLE, content);
	uint8_t *ptr = (uint8_t*)content.data();
	ClientTableInfo tableInfo;
	for (uint8_t i = 0; i < content.size(); ++i){
		tableInfo.id = i;
		tableInfo.totalPlayer = content[i];
		pListRoom.push_back(tableInfo);
	}
	
	mCacheListRoom->clear();
	for (int j = 0; j < pListRoom.size(); j++){
		ClientTableInfo* pInfo = &pListRoom.at(j);
		mCacheListRoom->push_back(*pInfo);
	}
	mErrorCode_ChooseRoom = TEXT_SUCCESS_LOADTABLE;
	mLoadingMain->hide();
	mLoading->setVisible(false);

	//this->schedule(schedule_selector(ChooseRoomScene::reloadTableRequest, 5));
	auto action = Sequence::createWithTwoActions(DelayTime::create(5), CallFunc::create([=]{reloadTableRequest(1); }));
	action->setTag(100);
	this->runAction(action);
}

void ChooseRoomScene::executeJoinTable(MpMessage* pMsg)
{
	MpJoinTableMessageResponse *pResponse = (MpJoinTableMessageResponse*)pMsg;
	roomStruct* pInfo = mInfoRoom;
	unsigned  pErrorCode = pResponse->getErrorCode();
	if (pErrorCode != 0) {
		mErrorCode_JoinRoom = __String::createWithFormat("%d", pErrorCode)->getCString();
		mLoadingMain->hide();
	}
	else {
		std::vector<ClientPlayerInfo> pListPlayer;
		std::vector<ReadyPlayer> pListReady;
		pResponse->getListReadyPlayer(pListReady);
		pResponse->getListPlayer(pListPlayer);
		pResponse->getListItem(pInfo->mListItemSp);
		pResponse->getListFishPrice(pInfo->mListPrice);
		pInfo->mTableID = pResponse->getInt(MP_PARAM_TABLE_ID);
		pInfo->mDurationReadyNormal = 30;
		pInfo->mDurationReadyHost = 30;
		pResponse->getDefaultBetMoney(pInfo->pBetMoneyDefault);
		uint16_t pDuration;
		uint32_t pFireMoney = pResponse->getInt(MP_PARAM_FIRE_MONEY);
		pResponse->getGameDuration(pDuration);
		std::string strListFactor; pResponse->getString(MP_PARAM_LIST_XFACTOR, strListFactor);
		for (int i = 0; i < strListFactor.length(); i++) {
			pInfo->mListXFactor.push_back(strListFactor[i]);
		}
		pInfo->mDuration = pDuration;
		uint32_t pMinBetMoney;
		pMinBetMoney = pResponse->getInt(MP_PARAM_BET_MONEY);
		std::string strXFactor; pResponse->getString(MP_PARAM_XFACTOR, strListFactor);

		if (pInfo->mListBetMoney && pInfo->mListBetMoney->size() > 0)
			pInfo->mListBetMoney->clear();

		for (int i = 0; i < 3; i++){
			pInfo->mListBetMoney->push_back(pMinBetMoney *LIST_BET_RATE[i]);
		}
		pInfo->pBetMoney = pMinBetMoney;

		pInfo->mRatio = pResponse->getRatioLevel();
		pInfo->mBackGroundID = pResponse->getBackgroundId();
		std::string data;
		int len = 0;
		pResponse->getString(MP_PARAM_FISH_MONEY, data);
		for (int i = pListPlayer.size() - 1; i >= 0; i--) {
			int index = pListPlayer.size() - 1 - i;
			ClientPlayerInfo playerInfo = pListPlayer.at(index);
			ClientPlayerInfoEX playerInfoGame;
			playerInfoGame.balance = playerInfo.balance;
			playerInfoGame.balanceDefault = playerInfo.balance;
			playerInfoGame.host = playerInfo.host;
			playerInfoGame.level = playerInfo.level;
			playerInfoGame.position = playerInfo.position;
			playerInfoGame.score = *((uint32_t*)(data.data() + len));
			playerInfoGame.score = ntohl(playerInfoGame.score);
			len += 4;
			playerInfoGame.username = playerInfo.username;
			playerInfoGame.disallowKick = playerInfo.disallowKick;
			for (int j = 0; j < pListReady.size(); j++) {
				ReadyPlayer  readyPlayer = pListReady.at(j);
				if (readyPlayer.position == playerInfoGame.position) {
					playerInfoGame.isReady = readyPlayer.status;
					break;
				}
			}

			playerInfoGame.itemId = playerInfo.itemId;
			if (playerInfo.itemId == 0) {
				playerInfoGame.itemId = 101;
			}
			pInfo->mListPlayer->push_back(playerInfoGame);
		}
		if (strXFactor.length() == pInfo->mListPlayer->size()) {
			for (size_t i = 0; i < pInfo->mListPlayer->size(); i++) {
				pInfo->mListPlayer->at(i).indexX = strXFactor[i];
			}
		}
		GameScene *scene = ((GameScene*)createGame(pInfo));
		if (pInfo->mGameID == 1) {
			scene->setRemainTime(pResponse->getInt(MP_REMAIN_TIME));
			scene->setFireMoney(pFireMoney);
		}
		mErrorCode_ChooseRoom = "";
	}
}

void ChooseRoomScene::createTab(int pCountTab){
	for (int i = 5; i >= 0; i--){
		int index = 5 - i;
		if (index < pCountTab){
			mCountTab++;
			std::string pFileTab = __String::createWithFormat(IMG_TAB_CHOOSE_ROOM, 1)->getCString();
			EGSprite* pTab = (EGSprite*)mLayerChooseRoom->getChildByTag(TAG_TAB_CHOOSE_ROOM + index);
			Sprite* pTitleTab = NULL;
			EGNumberSprite * numIndexTab = NULL;
			if (!pTab)	{
				pTab = EGSprite::createWithSpriteFrameName(pFileTab.c_str());
				mLayerChooseRoom->addChild(pTab);

			}

			pTab->registerTouch(true);
			pTab->setTag(TAG_TAB_CHOOSE_ROOM + index);
			pTab->setPosition(Vec2(pTab->getContentSize().width / 2 + index* pTab->getContentSize().width, 395));
			int pCount = pCountTab;
			if (pCountTab > 6){
				pCount = 6;
			}
			pTab->setOnTouch(CC_CALLBACK_0(ChooseRoomScene::chooseTab, this, index, pCount));
			pTitleTab = (EGSprite*)pTab->getChildByTag(1);
			numIndexTab = (EGNumberSprite*)pTab->getChildByTag(2);
			if (!pTitleTab){
				pTitleTab = Sprite::createWithSpriteFrameName(__String::createWithFormat(IMG_TAB_TITLE, 1 + atoi(pRoomLevelstr.c_str()))->getCString());
				pTab->addChild(pTitleTab);
				numIndexTab = EGNumberSprite::createWithSpriteFrameName(IMG_NUM_TAB);
				pTab->addChild(numIndexTab);
			}
			pTitleTab->setTag(1);
			pTitleTab->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_TAB_TITLE, 1 + atoi(pRoomLevelstr.c_str()))->getCString()));
			numIndexTab->setTag(2);
			numIndexTab->setNumber(index + 1);
			pTitleTab->setPosition(Vec2(pTab->getContentSize().width / 2 - (pTitleTab->getContentSize().width + 5 + numIndexTab->getContentSize().width) / 2 + pTitleTab->getContentSize().width / 2, pTab->getContentSize().height / 2));
			numIndexTab->setPosition(Vec2(pTitleTab->getPositionX() + pTitleTab->getContentSize().width / 2 + 5 + numIndexTab->getContentSize().width / 2, pTab->getContentSize().height / 2));
		}
		else{
			EGSprite* pTab = (EGSprite*)mLayerChooseRoom->getChildByTag(TAG_TAB_CHOOSE_ROOM + index);
			if (pTab){
				pTab->setVisible(false);
			}
		}
	}
}