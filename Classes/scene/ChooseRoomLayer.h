#ifndef __CHOOSEROOMLAYER_H_INCLUDED__
#define __CHOOSEROOMLAYER_H_INCLUDED__
#include "cocos2d.h"
#include "GameInfo.h"
#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "extensions\cocos-ext.h"
#else
#include "cocos-ext.h"
#endif
#include "EGNumberSprite.h"
#include "EGButtonSprite.h"
#include "ClientPlayerInfo.h"


using namespace cocos2d;
using namespace extension;

class ChooseRoomLayer : public LayerColor, public cocos2d::extension::TableViewDataSource, public cocos2d::extension::TableViewDelegate {
public:
	static ChooseRoomLayer* createLayer();
	void initLayer();
	void loadListRoom(std::vector<ClientTableInfo>* pCacheListRoom);
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view) {};
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view) {}
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx);
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);
	void reloadInfoRoom(TableViewCell *cell, int idx);
	void setFuntionChooseRoom(std::function<void(uint8_t)> pFunc){
		mFuntionChooseRoom = pFunc;
	}
	void chooseRoom(int id){
		mID = id;
		/*CallFunc* callChoose = CallFunc::create(mFuntionChooseRoom(id));	
		callChoose->execute();*/
		mFuntionChooseRoom(id);
	}
	ClientTableInfo getInfoRoom(){
		return mCacheListRoom->at(mID);
	}
	int getIDChoosed(){ return mID; }
	void setListBetMoney(std::vector<uint32_t>* pListBetMoney){
		mListBetMoney = pListBetMoney;
	}
private:
	std::vector<uint32_t>* mListBetMoney;
	std::function<void(uint8_t)> mFuntionChooseRoom;
	std::vector<ClientTableInfo>* mCacheListRoom;
	Vector<Sprite*> mListRoomSprite;
	TableView* tableView;
	int mQuantity;
	int mID;
};
#endif