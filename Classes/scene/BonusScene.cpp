#include "BonusScene.h"
#include "GameInfo.h"
#include "WXmlReader.h"
#include "MainScene.h"
#include "LuckyBonusScene.h"
#include "BoxNoticeManager.h"

BonusScene* BonusScene::scene() {
	BonusScene* pScene = new BonusScene();
	pScene->initScene();
	pScene->autorelease();
	pScene->registerBackPressed();

	return pScene;
}

void BonusScene::initScene() {
	GameInfo::mUserInfo.stateScene = 0;
	mBackground = Sprite::createWithSpriteFrameName("background_friend.png");
	mBackground->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBackground);

	mBtnBack = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_BACK, EGButton2FrameDark);
	mBtnBack->setOnTouch(CC_CALLBACK_0(BonusScene::onBackPressed, this));
	mBtnBack->setPosition(Vec2(10 + mBtnBack->getContentSize().width / 2, CLIENT_HEIGHT - mBtnBack->getContentSize().height / 2 - 10));
	mBackground->addChild(mBtnBack);
	 
	mTitle = Sprite::createWithSpriteFrameName("title_bonus.png");
	mTitle->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT - mTitle->getContentSize().height / 2 - 20));
	this->addChild(mTitle);

	mButton = new EGButtonSprite*[3];

	WXmlReader* xmlReader = WXmlReader::create();
	xmlReader->load(KEY_XML_FILE);

	for (int i = 0; i < 3; i++) {
		mButton[i] = EGButtonSprite::createWithSpriteFrameName("rec_friendbg.png", EGButton2FrameDark);
		mButton[i]->setOnTouch(CC_CALLBACK_0(BonusScene::excuteBonus, this, i));
		mButton[i]->setPosition(Vec2(CLIENT_WIDTH / 2, 90 + (mButton[i]->getContentSize().height + 10)*(3-i)));
		mBackground->addChild(mButton[i]);

		Sprite* pIcon = Sprite::createWithSpriteFrameName(__String::createWithFormat("icon_bonus%d.png", i + 1)->getCString());
		pIcon->setPosition(Vec2(pIcon->getContentSize().width + 10, mButton[i]->getContentSize().height / 2));
		if (i == 3) {
			pIcon->setPositionX(pIcon->getPositionX() - 20);
		}
		mButton[i]->addChild(pIcon);

		Label* pLabel = Label::createWithBMFont(FONT_WHITE_STROKE_BLACK, xmlReader->getNodeTextByTagName(__String::createWithFormat("text_bonus_%d", i + 1)->getCString()));
		pLabel->setScale(1.3f);
		pLabel->setPosition(Vec2(150 + pLabel->getContentSize().width / 2, mButton[i]->getContentSize().height / 2));
		mButton[i]->addChild(pLabel);
	}

	Sprite* pImgTreasure = Sprite::createWithSpriteFrameName("img_treasure.png");
	pImgTreasure->setPosition(Vec2(CLIENT_WIDTH - pImgTreasure->getContentSize().width + 50, pImgTreasure->getContentSize().height - 30));
	this->addChild(pImgTreasure);

	mBoxNotice = BoxNotice::create();
	mBoxNotice->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBoxNotice, 10);
}

void BonusScene::onBackPressed() {
	Scene* pScene = MainScene::scene(0);
	TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
	Director::getInstance()->replaceScene(pTrans);
}

void BonusScene::excuteBonus(int pIndex) {
	if (pIndex == 2) {
		Scene* pScene = LuckyBonusScene::scene();
		TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
		Director::getInstance()->replaceScene(pTrans);
	}
	else {
		WXmlReader* pXml = WXmlReader::create();
		pXml->load(KEY_XML_FILE);
		BoxNoticeManager::getBoxNotice(mBoxNotice,
			pXml->getNodeTextByTagName("notice").c_str(),
			pXml->getNodeTextByTagName("feature_comingsoon").c_str());
		mBoxNotice->setVisible(true);
	}
}