#include "HomeSceneOff.h"
#include "HomeScene.h"
#include "GameScene.h"
#include "EGJniHelper.h"
#include "ChooseRoomScene.h"
#include "libs/WAudioControl.h"


Scene* HomeSceneOff::scene(){
	HomeSceneOff* pScene = new HomeSceneOff();
	pScene->init();
	pScene->autorelease();
	return pScene;
}
bool HomeSceneOff::init(){
	//WAudioControl::getInstance()->enableAudio(true);
	//WAudioControl::getInstance()->enableNewEngine(true);

	GameInfo::mUserInfo.stateScene = 0;
	/*bool pRet = false;
	do{*/
		Size _size = Director::sharedDirector()->getWinSize();
		//CC_BREAK_IF(!Scene::init());
		registerBackPressed();
		mBackGround = Sprite::create("bg-home-off.png");
		mBackGround->setAnchorPoint(ccp(0, 0));
		this->addChild(mBackGround);
		mKhobau = Sprite::create("treasure-off.png");
		mBackGround->addChild(mKhobau);
		mSpeed = 100;
		mKhobau->setPosition(ccp(_size.width / 2 + 10, _size.height));
		mKhobau->setAnchorPoint(ccp(0.5f, 0));
		mSand = Sprite::create(__String::createWithFormat("sand-off (%d).png", 1)->getCString());
		mBackGround->addChild(mSand);
		mSand->setPosition(ccp(_size.width / 2 + 20, 120));

		buttonGameOnline = EGSprite::createWithFile("bt-game-online.png");
		buttonGameOnline->setPosition(Vec2(710, 68));
		mBackGround->addChild(buttonGameOnline, 5);
		mArrayButton.push_back(buttonGameOnline);
		//buttonGameOnline->setVisible(false);
		buttonGameOnline->setOnTouch([=]{
			Scene* _scene = HomeScene::scene();
			Director::getInstance()->replaceScene(_scene);
		});

		buttonHistory = EGSprite::createWithFile("bt-diary-off.png");
		buttonHistory->setPosition(Vec2(530, 115));
		//buttonHistory->setPosition(Vec2(650, 115));
		mBackGround->addChild(buttonHistory, 5);
		mArrayButton.push_back(buttonHistory);
		buttonHistory->setOnTouch([=]{
			gotoGame(1);
		});

		buttonPlay = EGSprite::createWithFile("bt-play-off.png");
		buttonPlay->setPosition(Vec2(360, 80));
		//buttonPlay->setPosition(Vec2(400, 80));
		mBackGround->addChild(buttonPlay, 5);
		mArrayButton.push_back(buttonPlay);
		buttonPlay->setOnTouch([=]{
			gotoGame(0);
		});

		buttonShop = EGSprite::createWithFile("bt-shop-off.png");
		buttonShop->setPosition(Vec2(214, 160));
		//buttonShop->setPosition(Vec2(150, 160));
		mBackGround->addChild(buttonShop, 5);
		mArrayButton.push_back(buttonShop);
		buttonShop->setOnTouch([=]{
			gotoGame(2);
		});


		buttonMusic = EGSprite::createWithFile("bt-music-off1.png");
		buttonMusic->setPosition(Vec2(33, 410));
		mBackGround->addChild(buttonMusic, 5);
		mArrayButton.push_back(buttonMusic);
		buttonMusic->setOnTouch([=]{
			turnMusic();
		});

		mLogo = Sprite::create("logo-home-off.png");
		mLogo->setOpacity(0);
		this->addChild(mLogo);
		mLogo->setPosition(ccp(_size.width / 2 - 70, _size.height - 10 - mLogo->getContentSize().height / 2));
		for (int i = 0; i < mArrayButton.size(); i++){
			EGSprite* _button = (EGSprite*)mArrayButton.at(i);
			_button->setScale(0);
		}
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("anchor_fall.mp3");
		schedule(schedule_selector(HomeSceneOff::updateGame));
		CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
		if (!UserDefault::sharedUserDefault()->getBoolForKey(KEY_SOUND, true)){
			buttonMusic->setTexture("bt-music-off2.png");
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
			//WAudioControl::getInstance()->stopAll();
		}
		else{
			//CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(MUSIC_BACKGROUND, true);
			WAudioControl::getInstance()->playBackgroundMusic(MUSIC_BACKGROUND);
		}
		/*pRet = true;
	} while (0);*/
	//return pRet;

		this->schedule(schedule_selector(HomeSceneOff::updateBubble), 2);


	return true;
}
void HomeSceneOff::gotoGame(int pType){
	roomStruct *mInfoRoom = new roomStruct();
	//mInfoRoom->mTcp = new TcpClient();
	mInfoRoom->mTableID = 0;
	mInfoRoom->pTypeRoom = 0;
	mInfoRoom->mIDRoom = 0;
	mInfoRoom->pRoomLevelstr = "0";
	std::vector<ClientPlayerInfoEX> *mPlayerList = new std::vector<ClientPlayerInfoEX>();
	ClientPlayerInfoEX player;
	player.username = "";
	player.balance = atof(UserDefault::getInstance()->getStringForKey(KEY_MONEY, "500").c_str());
	player.score = 0;
	player.itemId = 101;
	mPlayerList->push_back(player);
	mInfoRoom->mListPlayer = mPlayerList;
	mInfoRoom->mGameID = 2;
	mInfoRoom->pMaxPlayer = 1;
	mInfoRoom->iCreater = false;
	mInfoRoom->mBackGroundID = 1 + rand() % 14;
	if (pType == 0){
		Scene* _scene = GameScene::createScene(mInfoRoom);
		Director::getInstance()->replaceScene(_scene);
	}
	else if (pType == 1){
		Scene* _scene = GameScene::createSceneWithDiary(mInfoRoom);
		Director::getInstance()->replaceScene(_scene);
	}
	else if (pType == 2){
		Scene* _scene = GameScene::createSceneWithShop(mInfoRoom);
		Director::getInstance()->replaceScene(_scene);
	}
}
void HomeSceneOff::turnMusic(){
	if (UserDefault::sharedUserDefault()->getBoolForKey(KEY_SOUND, true)){
		UserDefault::sharedUserDefault()->setBoolForKey(KEY_SOUND, false);
		UserDefault::sharedUserDefault()->setBoolForKey(KEY_MUSIC, false);
		buttonMusic->setTexture("bt-music-off2.png");
		CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
		//WAudioControl::getInstance()->stopAll();
	}
	else{
		UserDefault::sharedUserDefault()->setBoolForKey(KEY_SOUND, true);
		UserDefault::sharedUserDefault()->setBoolForKey(KEY_MUSIC, true);
		buttonMusic->setTexture("bt-music-off1.png");
	}
	//playSound(SOUND_ENTER);
}
void HomeSceneOff::updateGame(float pSec){
	float pGiatoc = 700;
	mSpeed += pGiatoc*pSec;
	if (mKhobau->getPositionY() <= 110){
		mSpeed = 100;
		if (mSand->getTag() != -2){
			Animation* pAnimationArray = Animation::create();
			for (int i = 0; i < 5; i++){
				pAnimationArray->addSpriteFrameWithFileName(__String::createWithFormat("sand-off (%d).png", i+1)->getCString());
			}
			pAnimationArray->setDelayPerUnit(0.1f);
			mSand->runAction(Animate::create(pAnimationArray));
			mSand->setTag(-2);
		}
	}
	if (rand() % 3 == 1){
		auto bubble = EGSprite::createWithFile("img_bubble.png");
		uint32_t posX = rand() % 160 + 333;
		bubble->setPosition(Vec2(posX, mKhobau->getPositionY() + 30));
		bubble->setScale((rand() % 4 + 2)*0.1f);
		mBackGround->addChild(bubble, rand() % 10 - 5);
		bubble->runAction(Sequence::create(
			MoveTo::create(rand() % 5 + 2, Vec2(posX, 550)),
			CallFunc::create([=]{bubble->removeFromParentAndCleanup(true); }), NULL));
	}

	mKhobau->setPositionY(mKhobau->getPositionY() - mSpeed*pSec);
	if (mKhobau->getPositionY() <= 80){
		unscheduleAllSelectors();
		this->schedule(schedule_selector(HomeSceneOff::updateBubble), 2);
		mLogo->runAction(Sequence::create(FadeIn::create(0.5f), CallFunc::create(CC_CALLBACK_0(HomeSceneOff::showButton,this)), NULL));
		/*for (uint8_t i = 0; i < 8; ++i){
			auto bubble = EGSprite::createWithFile("img_bubble.png");
			bubble->setVisible(false);
			uint32_t posX = rand() % 170 + 10;
			bubble->setPosition(Vec2(posX, rand()%150));
			bubble->setScale((rand() % 4 + 2)*0.1f);
			mKhobau->addChild(bubble, rand() % 10 - 5);
			bubble->runAction(Sequence::create(DelayTime::create(rand()%20*0.1f), 
				CallFunc::create([=]{bubble->setVisible(true); }),
				MoveTo::create(rand() % 5 + 2, Vec2(posX, 550)), 
				CallFunc::create([=]{bubble->removeFromParentAndCleanup(true); }), NULL));
		}*/
		/*if (UserDefault::getInstance()-> getBoolForKey(KEY_SOUND, true)){
			if (UserDefault::sharedUserDefault()->getBoolForKey(KEY_SOUND, true)){
				CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(MUSIC_BACKGROUND, true);
			}
		}*/
	}
}
void HomeSceneOff::showButton(){
	for (int i = 0; i  < mArrayButton.size(); i++){
		EGSprite* _button = (EGSprite*)mArrayButton.at(i);
		_button->setScale(0);
		_button->registerTouch(true);
		_button->runAction(Sequence::create(DelayTime::create(i*0.1f), ScaleTo::create(0.2f, 1), NULL));
		_button->runAction(RepeatForever::create(Sequence::create(MoveBy::create(1 + i*0.1f, ccp(0, 10)), MoveBy::create(1 + i*0.1f, ccp(0, -10)), NULL)));
	}
}
void HomeSceneOff::onBackPressed() {
	EGJniHelper::showDialogQuit();
}

bool HomeSceneOff::isExitGame() {
	return false;
}

void HomeSceneOff::updateBubble(float dt)
{
	auto bubble = EGSprite::createWithFile("img_bubble.png");
	uint32_t posX = rand() % 800;
	bubble->setPosition(Vec2(posX, rand()%200 - 30));
	bubble->setScale((rand()%6 + 2)*0.1f);
	mBackGround->addChild(bubble, 1);
	bubble->runAction(Sequence::createWithTwoActions(MoveTo::create(rand() % 5 + 2, Vec2(posX, 550)), CallFunc::create([=]{bubble->removeFromParentAndCleanup(true); })));
}