#include "LuckyBonusScene.h"
#include "GameInfo.h"
#include "BonusScene.h"
#include "BoxNoticeManager.h"

#include "mpcheckluckymessageresponse.h"
#include "pthread_mobi.h"
#include "mpsendluckymessagerequest.h"

#include "HomeScene.h"

std::string mErrorCode_LuckyBonusScene;
int mPercentageLucky;
bool isDrawLucky;
string oldErrCode;

Scene* LuckyBonusScene::scene() {
	LuckyBonusScene* pScene = new LuckyBonusScene();
	pScene->initScene();
	pScene->autorelease();
	pScene->registerBackPressed();

	return pScene;
}

void LuckyBonusScene::initScene() {
	GameInfo::mUserInfo.stateScene = 0;
	mErrorCode_LuckyBonusScene = "";
	mXml = WXmlReader::create();
	mXml->load(KEY_XML_FILE);
	mCircleTurn = 0;
	isDrawLucky = false;
	isDrawing = false;
	oldErrCode = "";

	mBackground = EGSprite::createWithSpriteFrameName("backgroundlucky.png");
	mBackground->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBackground);

	mBtnBack = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_BACK, EGButton2FrameDark);
	mBtnBack->setOnTouch(CC_CALLBACK_0(LuckyBonusScene::onBackPressed, this));
	mBtnBack->setPosition(Vec2(10 + mBtnBack->getContentSize().width / 2, CLIENT_HEIGHT - mBtnBack->getContentSize().height / 2 - 10));
	mBackground->addChild(mBtnBack);

	mLuckyPrice = EGSprite::createWithSpriteFrameName("luckyprice.png");
	mLuckyPrice->setPosition(Vec2(20 + mLuckyPrice->getContentSize().width / 2, CLIENT_HEIGHT / 2 - 20));
	mBackground->addChild(mLuckyPrice);

	mLuckyCirle = EGSprite::createWithSpriteFrameName("luckybar.png");
	mLuckyCirle->setPosition(Vec2(CLIENT_WIDTH - mLuckyCirle->getContentSize().width / 2 - 20, CLIENT_HEIGHT / 2 - 10));
	mBackground->addChild(mLuckyCirle);

	mLuckyBar = EGSprite::createWithSpriteFrameName("luckybarborder.png");
	mLuckyBar->setPosition(mLuckyCirle->getPosition());
	mBackground->addChild(mLuckyBar);

	mBtnStartLucky = EGButtonSprite::createWithSpriteFrameName("btnstartlucky.png", EGButton2FrameDark);
	mBtnStartLucky->setOnTouch(CC_CALLBACK_0(LuckyBonusScene::startLucky, this));
	mBtnStartLucky->setPosition(Vec2(CLIENT_WIDTH - mBtnStartLucky->getContentSize().width / 2 - 30, mBtnStartLucky->getContentSize().height / 2 + 40));
	mBackground->addChild(mBtnStartLucky);

	mPriceReceive = EGSprite::createWithSpriteFrameName("100.png");
	mPriceReceive->setPosition(Vec2(mLuckyPrice->getContentSize().width / 2, mLuckyPrice->getContentSize().height - mPriceReceive->getContentSize().height / 2 - 20));
	mPriceReceive->runAction(RepeatForever::create(Sequence::createWithTwoActions(ScaleTo::create(0.5f, 1.2f), ScaleTo::create(0.5f, 0.8f))));
	mPriceReceive->setVisible(false);
	mLuckyPrice->addChild(mPriceReceive);

	mBoxNotice = BoxNotice::create();
	mBoxNotice->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	mBackground->addChild(mBoxNotice, 1);

	mLoading = Loading::create();
	mLoading->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	mBackground->addChild(mLoading, 1);

	this->scheduleUpdate();
}

#include "EGJniHelper.h"

void* pthread_checkLucky(void* _void) 
{
	__String* pStr_DeviceID = (__String*)(_void);
	std::string pDeviceID = pStr_DeviceID->getCString();
	MpClient client;

	client.connect(GameInfo::loginServerAddress.c_str(), GameInfo::loginServerPort);
	int cnt = 0;
	while (client.isReady() == false && cnt < 10)
	{
		SLEEP(1);
		++cnt;
	}

	if (cnt >= 10)
	{
		mErrorCode_LuckyBonusScene = TEXT_ERR_NOCONNECTION;
		return NULL;
	}

	MpMessageRequest request(MP_MSG_LUCKY_CHECK);
	request.setTokenId(GameInfo::mUserInfo.mTokenID.c_str());
	request.addString(MP_PARAM_DEVICE_ID, pDeviceID);

	client.sendMessage(&request);
	cnt = 0;
	MpMessage *pRes = NULL;
	while (((pRes = client.getMessage()) = NULL) && (cnt < 10))
	{
		SLEEP(1);
		++cnt;
	}

	if (cnt >= 10 || pRes == NULL)
	{
		mErrorCode_LuckyBonusScene = TEXT_ERR_NOCONNECTION;
		return NULL;
	}

	MpCheckLuckyMessageResponse *pResponse = (MpCheckLuckyMessageResponse *)pRes;
	stringstream ss;
	ss << pResponse->getErrorCode();
	mErrorCode_LuckyBonusScene = ss.str();

	if (mErrorCode_LuckyBonusScene.size() == 6 && mErrorCode_LuckyBonusScene.substr(3, 3) == "256")
	{
		mErrorCode_LuckyBonusScene = TEXT_ERR_NOCONNECTION;
	}else 
	{
		uint8_t pStatus;
		pResponse->getResult(pStatus);
		if (pStatus == 1)
		{
			mErrorCode_LuckyBonusScene = TEXT_CHECKLUCKY_FALL;
		}
		else
		{
			mPercentageLucky = pResponse->getPercentageResult();
			mErrorCode_LuckyBonusScene = TEXT_CHECKLUCKY_OK;
		}			
	}
	delete pRes;
	pthread_exit(0);
	return NULL;
}

void LuckyBonusScene::startLucky() {
	mLoading->show();
	
	if(isDrawLucky) {
		mErrorCode_LuckyBonusScene = oldErrCode;
	} else {
		pthread_t t1;
		__String *pStr_DeviceID = __String::create(EGJniHelper::getDeviceID());
		pthread_create(&t1, NULL, pthread_checkLucky, pStr_DeviceID);
	}
}

void LuckyBonusScene::runLucky() {
	int pRotate = mPercentageLucky;

	int mBonusMoney = 0;

	if (pRotate > 0 && pRotate < 11) {
		mBonusMoney = 500;
	}
	else if (pRotate > 11 && pRotate < 115) {
		mBonusMoney = 100;
	}
	else if (pRotate > 115 && pRotate < 152)  {
		mBonusMoney = 300;
	}
	else if (pRotate > 152 && pRotate < 236) {
		mBonusMoney = 180;
	}
	else if (pRotate > 236 && pRotate < 258) {
		mBonusMoney = 400;
	}
	else {
		mBonusMoney = 150;
	}

	mLuckyCirle->runAction(Sequence::createWithTwoActions(EaseExponentialOut::create(RotateBy::create(10, -pRotate + 360 * 5)),
		CallFunc::create(CC_CALLBACK_0(EGSprite::setVisible, mPriceReceive, true))));

	GameInfo::mUserInfo.mBalance += mBonusMoney;
	mPriceReceive->setTexture(__String::createWithFormat("%d.png", mBonusMoney)->getCString());
	mPriceReceive->setPosition(Vec2(mLuckyPrice->getContentSize().width / 2, mLuckyPrice->getContentSize().height - mPriceReceive->getContentSize().height / 2 - 20));
	//mPriceReceive->setVisible(true);
	mBtnStartLucky->setVisible(false);
}

void LuckyBonusScene::update(float dt) {
	if (mErrorCode_LuckyBonusScene != "") {
		if (mErrorCode_LuckyBonusScene == TEXT_CHECKLUCKY_OK) {
			runLucky();
		}
		else if (mErrorCode_LuckyBonusScene == TEXT_CHECKLUCKY_FALL) {
			BoxNoticeManager::getBoxNotice(mBoxNotice, mXml->getNodeTextByTagName("title_lucky_cantplay").c_str(), mXml->getNodeTextByTagName("content_lucky_cantplay").c_str());
			mBoxNotice->setVisible(true);
		}
		else if (mErrorCode_LuckyBonusScene == TEXT_ERR_NOCONNECTION) {
			WXmlReader* pXml = WXmlReader::create();
			pXml->load(KEY_XML_FILE);
			BoxNoticeManager::getBoxNotice(mBoxNotice,
				pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				pXml->getNodeTextByTagName(mErrorCode_LuckyBonusScene).c_str(),
				[=] {
				Scene* _scene = HomeScene::scene();
				Director::getInstance()->replaceScene(_scene);
			});
			mBoxNotice->setVisible(true);
		}
		else {
			BoxNoticeManager::getBoxNotice(mBoxNotice,
				mXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				mXml->getNodeTextByTagName(mErrorCode_LuckyBonusScene.c_str()).c_str());
			mBoxNotice->setVisible(true);
		}
		mLoading->hide();
		mErrorCode_LuckyBonusScene = "";
	}
}

void LuckyBonusScene::onBackPressed() {
	Scene* pScene = BonusScene::scene();
	TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
	Director::getInstance()->replaceScene(pTrans);
}