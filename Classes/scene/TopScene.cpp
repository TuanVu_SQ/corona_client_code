#include "TopScene.h"
#include "GameInfo.h"
#include "MainScene.h"
#include "pthread_mobi.h"
#include "mpmessagerequest.h"
#include "mploadtopmessageresponse.h"
#include "mpgetinfoplayergameresponse.h"
#include "GameInfo.h"

#include "BoxNoticeManager.h"
#include "EGJniHelper.h"


std::string mErrorCode_TopScene;

vector<TopInfo> m_cacheTop_Balance;
vector<TopInfo> m_cacheTop_Level;
vector<TopInfo> m_cacheTop_Fish;
std::vector<TopInfo> mCacheTop;
ClientPlayerInfoEX* mPlayerInfoTop;
int mTypeLoadTop = 0;
Scene* TopScene::scene() {
	TopScene* pScene = new TopScene();
	pScene->initScene();
	pScene->registerBackPressed();
	pScene->autorelease();

	return pScene;
}

TopScene::~TopScene() {
	m_cacheTop_Balance.clear();
	m_cacheTop_Level.clear();
	m_cacheTop_Fish.clear();
	mCacheTop.clear();
}

void TopScene::initScene() {
	GameInfo::mUserInfo.stateScene = 0;
	_topType = -1;
	mXml = WXmlReader::create();
	mXml->load(KEY_XML_FILE);
	mPlayerInfoTop = NULL;
	mBackground = Sprite::create("background_choosetype.png");
	mBackground->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBackground);

	//mBorderTop = EGSprite::createWithSpriteFrameName(IMG_BORDER_TOP_LEVEL);
	mBorderTop = EGSprite::createWithFile("kv_banthuong_bg2.png");
	mBorderTop->setScale(1.1f);
	mBorderTop->setPosition(Vec2(CLIENT_WIDTH / 2, mBorderTop->getContentSize().height/2));
	mBackground->addChild(mBorderTop);

	/*auto bgNew = EGSprite::createWithFile("kv_banthuong_bg2.png");
	bgNew->setScaleX(1.18);
	bgNew->setScaleY(1.2);
	bgNew->setPosition(Vec2(CLIENT_WIDTH / 2, bgNew->getContentSize().height/2));
	mBackground->addChild(bgNew);*/

	mBtnTopBalance = EGButtonSprite::createWithFile("kv_banthuong_on_button.png", EGButton2FrameDark);
	mBtnTopBalance->setOnTouch(CC_CALLBACK_0(TopScene::loadTopBalance, this));
	mBtnTopBalance->setPosition(Vec2(484, 410));
	mBackground->addChild(mBtnTopBalance);
	mBtnTopBalance->setZOrder(10);

	auto mLbBalance = Label::createWithBMFont(FONT_BMF_NAME, "Moun Rich");
	mLbBalance->setTag(1);
	mLbBalance->setScale(0.7f);
	mLbBalance->setPosition(mBtnTopBalance->getContentSize() / 2);
	mBtnTopBalance->addChild(mLbBalance);

	mBtnTopLevel = EGButtonSprite::createWithFile("kv_banthuong_off_button.png", EGButton2FrameDark);
	mBtnTopLevel->setOnTouch(CC_CALLBACK_0(TopScene::loadTopLevel, this));
	mBtnTopLevel->setPosition(Vec2(695, 410));
	mBackground->addChild(mBtnTopLevel);
	mBtnTopLevel->setZOrder(10);

	auto mLbLevel = Label::createWithBMFont(FONT_BMF_NAME, "Eksperyans");
	mLbLevel->setScale(0.7f);
	mLbLevel->setTag(1);
	mLbLevel->setPosition(mBtnTopLevel->getContentSize() / 2);
	mBtnTopLevel->addChild(mLbLevel);

	mBtnTopFish = EGButtonSprite::createWithFile("kv_banthuong_off_button.png", EGButton2FrameDark);
	mBtnTopFish->setOnTouch(CC_CALLBACK_0(TopScene::loadTopFish, this));
	mBtnTopFish->setPosition(Vec2(273, 410));
	mBackground->addChild(mBtnTopFish);
	mBtnTopFish->setZOrder(10);

	auto mLbFish = Label::createWithBMFont(FONT_BMF_NAME, "Viris Global");
	mLbFish->setScale(0.7f);
	mLbFish->setTag(1);
	mLbFish->setPosition(mBtnTopFish->getContentSize() / 2);
	mBtnTopFish->addChild(mLbFish);

	mBtnBack = EGButtonSprite::createWithFile("button-back-cr (1).png", EGButton2FrameDark);
	mBtnBack->setOnTouch(CC_CALLBACK_0(TopScene::onBackPressed, this));
	mBtnBack->setPosition(Vec2(10 + mBtnBack->getContentSize().width / 2, 440));
	mBackground->addChild(mBtnBack);

	mTableView = TableView::create(this, Size(mBorderTop->getContentSize().width, mBorderTop->getContentSize().height - 75));
	mTableView->setDirection(ScrollView::Direction::VERTICAL);
	mTableView->setPosition(Vec2(mBorderTop->getPositionX() - mBorderTop->getContentSize().width / 2, 70));
	mTableView->setDelegate(this);
	mBackground->addChild(mTableView);

	mBoxNotice = BoxNotice::create();
	mBoxNotice->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBoxNotice);

	mLoading = Loading::create();
	mLoading->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mLoading);

	mBtnTopBalance->performClick();

	mBoxInfoGame = BoxInfoGame::createBoxInfo();
	this->addChild(mBoxInfoGame);
	mBoxInfoGame->closeBox();
	mBoxInfoGame->setType(2);
	mBoxInfoGame->setLocalZOrder(10);
	scheduleUpdate();
}

void TopScene::tableCellTouched(TableView* table, TableViewCell* cell) {
}

Size TopScene::tableCellSizeForIndex(TableView *table, ssize_t idx) {
	return Size(716, 76);
}

TableViewCell* TopScene::tableCellAtIndex(TableView *table, ssize_t i) {
	mTableCell = table->dequeueCell();

	if (!mTableCell) {
		mTableCell = new TableViewCell();
	}
	else {
		mTableCell->removeAllChildrenWithCleanup(true);
		mTableCell->removeFromParentAndCleanup(true);

		mTableCell = new TableViewCell();
	}

	if (mCacheTop.size() > 0) {
		float pHeightObject = 0;
		std::string pName = mCacheTop.at(mCacheTop.size() - 1 - i).username;
		unsigned long pBalance = mCacheTop.at(mCacheTop.size() - 1 - i).balance;
		unsigned int pLevel = mCacheTop.at(mCacheTop.size() - 1 - i).level;
		unsigned int pAvatarID = mCacheTop.at(mCacheTop.size() - 1 - i).avatarId;
		int pStatus = mCacheTop.at(mCacheTop.size() - 1 - i).status;
		bool isHardPlay = false;
		//if (mBorderTop->getFileName() == "border_top_fish.png")
		if (_topType == 0)
			isHardPlay = true;
		ObjFriend* pObjFriend = ObjFriend::create(pAvatarID, pName, pBalance, pLevel, pStatus, CC_CALLBACK_0(TopScene::showInfo, this, pName), isHardPlay);
		//pObjFriend->setOnUp(CC_CALLBACK_0(TopScene::showInfoDetail, this, pName));
		//pObjFriend->registerTouch(false);
		pObjFriend->setOnTouch(CC_CALLBACK_0(TopScene::showInfoDetail, this, pName));
		pObjFriend->setButtonType(EGButtonScrollViewItem);
		//if (mBorderTop->getFileName() == "border_top_fish.png") {
		if (_topType == 0){
			pObjFriend->setTypeTop(2);
		}
		if (mCacheTop.size() - 1 - i == 0) {
			pHeightObject = pObjFriend->getContentSize().height;
		}
		pObjFriend->setPosition(Vec2(mBorderTop->getContentSize().width / 2, pObjFriend->getContentSize().height / 2));
		mTableCell->addChild(pObjFriend);
		//mLstPlayer.push_back(pObjFriend);
	}

	/*mTableView->setContentOffset(Vec2(0, mBorderTop->getContentSize().height - 100 - pHeightObject - 20));
	mTableCell->setContentSize(Size(0, pHeightObject*mCacheTop->size()));*/
	return mTableCell;
}

ssize_t TopScene::numberOfCellsInTableView(TableView *table) {
	return mCacheTop.size();
}

void pthread_loadTop(void* _void) {
	__Integer* pType = (__Integer*)_void;
	int _pType = pType->getValue();
	mTypeLoadTop = _pType;
	MpMessageRequest* pRequest = new MpMessageRequest(MP_MSG_GET_TOP);
	pRequest->setTokenId(GameInfo::mUserInfo.mTokenID.c_str());
	pRequest->addInt(MP_PARAM_TOP_TYPE, _pType);
	//pRequest->init(NULL, GameInfo::mUserInfo.mTokenID.c_str(), pType->getValue());

	//client_loadtop->sendMsg(pRequest->getMsg());
	MpClientManager::getInstance()->sendMessage(pRequest, true);
	log("send message load top");


	//delete pType;
}

void TopScene::showInfoDetail(std::string pUserName) 
{
	mBoxInfoGame->showBox(NULL, NULL);
	mPlayerInfoTop = new ClientPlayerInfoEX();
	mPlayerInfoTop->username = pUserName;
	MpMessageRequest* request = new MpMessageRequest(MP_MSG_GET_ACC_INFO);
	request->setTokenId(GameInfo::mUserInfo.mTokenID);
	for (int i = 0; i < pUserName.length(); i++) {
		pUserName[i] = std::tolower(pUserName[i]);
	}
	request->addString(MP_PARAM_USERNAME, pUserName);
	MpClientManager::getInstance()->sendMessage(request, true);
}

void TopScene::loadTopBalance() {
	/*if (mBorderTop->getFileName() != IMG_BORDER_TOP_BALANCE) {
		mBorderTop->setTexture(IMG_BORDER_TOP_BALANCE);*/
	
	if (_topType == 1) return;
		_topType = 1;
		mLoading->show();

		mBtnTopBalance->setTexture("kv_banthuong_on_button.png");
		mBtnTopFish->setTexture("kv_banthuong_off_button.png");
		mBtnTopLevel->setTexture("kv_banthuong_off_button.png");

		mBtnTopBalance->getChildByTag(1)->setPositionY(mBtnTopBalance->getContentSize().height/2 + 6);
		mBtnTopFish->getChildByTag(1)->setPositionY(mBtnTopBalance->getContentSize().height / 2);
		mBtnTopLevel->getChildByTag(1)->setPositionY(mBtnTopBalance->getContentSize().height / 2);

		if (m_cacheTop_Balance.size() != 0) {
			mCacheTop = m_cacheTop_Balance;
			mTableView->reloadData();
			mLoading->hide();
		}
		else {
			pthread_loadTop(__Integer::create(0));
		}
	//}
}
void TopScene::loadTopLevel() {
	/*if (!mLoading->isVisible() && mBorderTop->getFileName() != IMG_BORDER_TOP_LEVEL) {
		mBorderTop->setTexture(IMG_BORDER_TOP_LEVEL);*/

	if (_topType == 2) return;
	_topType = 2;
		mLoading->show();

		mBtnTopBalance->setTexture("kv_banthuong_off_button.png");
		mBtnTopFish->setTexture("kv_banthuong_off_button.png");
		mBtnTopLevel->setTexture("kv_banthuong_on_button.png");

		mBtnTopBalance->getChildByTag(1)->setPositionY(mBtnTopBalance->getContentSize().height / 2);
		mBtnTopFish->getChildByTag(1)->setPositionY(mBtnTopBalance->getContentSize().height / 2);
		mBtnTopLevel->getChildByTag(1)->setPositionY(mBtnTopBalance->getContentSize().height / 2 + 6);

		if (m_cacheTop_Level.size() != 0) {
			mCacheTop = m_cacheTop_Level;
			mTableView->reloadData();
			mLoading->hide();
		}
		else {
			pthread_t t1;
			pthread_loadTop(__Integer::create(1)); 
		}
	//}
}

void TopScene::loadTopFish() {
	/*if (mBorderTop->getFileName() != "border_top_fish.png") {
		mBorderTop->setTexture("border_top_fish.png");*/
	if (_topType == 0) return;
	_topType = 0;
		mLoading->show();

		mBtnTopBalance->setTexture("kv_banthuong_off_button.png");
		mBtnTopFish->setTexture("kv_banthuong_on_button.png");
		mBtnTopLevel->setTexture("kv_banthuong_off_button.png");

		mBtnTopBalance->getChildByTag(1)->setPositionY(mBtnTopBalance->getContentSize().height / 2);
		mBtnTopFish->getChildByTag(1)->setPositionY(mBtnTopBalance->getContentSize().height / 2 + 6);
		mBtnTopLevel->getChildByTag(1)->setPositionY(mBtnTopBalance->getContentSize().height / 2);

		if (m_cacheTop_Fish.size() != 0) {
			mCacheTop = m_cacheTop_Fish;
			mTableView->reloadData();
			mLoading->hide();
		}
		else {
			pthread_loadTop(__Integer::create(3));
		}
	//}
}

void TopScene::update(float dt) {
	MpMessage* pMsg = MpManager::getInstance()->getMessenger();
	if (pMsg) {
		switch (pMsg->getType())
		{
		case MP_MSG_GET_ACC_INFO_ACK:
		{
			MpGetInfoPlayerGameResponse  *pResponse = (MpGetInfoPlayerGameResponse*)pMsg;

			unsigned pErrorCode = pResponse->getErrorCode();
			if (pErrorCode != 0) {
				mErrorCode_TopScene = __String::createWithFormat("%d", pErrorCode)->getCString();

			}
			else {
				pResponse->getPlayerInfo(*mPlayerInfoTop);
				mErrorCode_TopScene = TEXT_GETINFO_SUCCES;
			}
		}
		break;
		case MP_MSG_GET_TOP_ACK:
		{
			MpLoadTopMessageResponse* pResponse = (MpLoadTopMessageResponse*)pMsg;

			stringstream ss;
			ss << pResponse->getErrorCode();

			if (ss.str() != "0") {
				mErrorCode_TopScene = ss.str();
			}

			if (mErrorCode_TopScene.size() == 6 && mErrorCode_TopScene.substr(3, 3) == "256") {
				mErrorCode_TopScene = TEXT_ERR_NOCONNECTION;
			}
			else {

				mCacheTop.clear();

				pResponse->getTop(mCacheTop);

				for (int i = 0; i < mCacheTop.size(); i++) {
					TopInfo topInfo = mCacheTop[i];
					log("username: %s", topInfo.username.c_str());
					log("avatarid: %d", topInfo.avatarId);
					log("balance: %d", topInfo.balance);
					log("level: %d", topInfo.level);
					log("status: %d", topInfo.status);
				}
				CCLOG("%d sizes", (int)mCacheTop.size());
				if (mTypeLoadTop == 0) {
					m_cacheTop_Balance = mCacheTop;
				}
				else if (mTypeLoadTop == 1) {
					m_cacheTop_Level = mCacheTop;
				}
				else {
					m_cacheTop_Fish = mCacheTop;
				}

				mErrorCode_TopScene = TEXT_SUCCESS_LOADTOP;
			}
		}
		break;
		default:
			break;
		}
		delete pMsg;
		pMsg = NULL;
	}
	if (getErrorCode() != "") {
		mErrorCode_TopScene = getErrorCode();
		setErrorCode("");
	}
	if (mErrorCode_TopScene != "") {
		if (mErrorCode_TopScene == TEXT_SUCCESS_LOADTOP) {
			mTableView->reloadData();
		}
		else if (mErrorCode_TopScene == TEXT_GETINFO_SUCCES) {
			mBoxInfoGame->showBox(mPlayerInfoTop, mPlayerInfoTop);
			mBoxInfoGame->setType(1);
		}
		else if (mErrorCode_TopScene == TEXT_ERR_NOCONNECTION) {
			WXmlReader* pXml = WXmlReader::create();
			pXml->load(KEY_XML_FILE);
			BoxNoticeManager::getBoxNotice(mBoxNotice,
				pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				pXml->getNodeTextByTagName(mErrorCode_TopScene).c_str(),
				[=] {
				Scene* _scene = HomeScene::scene();
				Director::getInstance()->replaceScene(_scene);
			});
			mBoxNotice->setVisible(true);
		}
		else {
			BoxNoticeManager::getBoxNotice(mBoxNotice,
				mXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				mXml->getNodeTextByTagName(mErrorCode_TopScene.c_str()).c_str());
			mBoxNotice->setVisible(true);
		}
		mLoading->hide();
		mErrorCode_TopScene = "";
	}
}

void TopScene::showInfo(std::string pUsername) {

}

void TopScene::onBackPressed() {
	if (mLoading->isVisible()) {
		return;
	}
	else {
		Scene* pScene = MainScene::scene(0);
		TransitionFade* pTrans = TransitionFade::create(0.5f, pScene);
		Director::getInstance()->replaceScene(pTrans);
	}
}