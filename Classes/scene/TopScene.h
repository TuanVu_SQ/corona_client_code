#ifndef __TOPSCENE_H_INLCUDED__
#define __TOPSCENE_H_INCLUDED__

#include "cocos2d.h"
#include "EGSprite.h"
#include "EGButtonSprite.h"
#include "ObjFriend.h"
#include "Loading.h"
#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "extensions\cocos-ext.h"
#else
#include "cocos-ext.h"
#endif
#include "BoxNotice.h"
#include "WXmlReader.h"
#include "EGScene.h"
#include "BoxInfoGame.h"

USING_NS_CC;
using namespace extension;

class TopScene :public EGScene, public TableViewDataSource, public TableViewDelegate{
public:
	static Scene* scene();
	void onBackPressed();

private:
	int _topType;

private:
	WXmlReader* mXml;
	virtual ~TopScene();
	Sprite* mBackground;
	EGSprite* mBorderTop;
	EGButtonSprite* mBtnTopBalance;
	EGButtonSprite* mBtnTopLevel;
	EGButtonSprite* mBtnTopFish;
	EGButtonSprite* mBtnBack;
	Loading* mLoading;
	TableView* mTableView;
	TableViewCell* mTableCell;

	BoxNotice* mBoxNotice;
	BoxInfoGame * mBoxInfoGame;

	virtual void scrollViewDidScroll(ScrollView* view) {};
	virtual void scrollViewDidZoom(ScrollView* view) {};
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	virtual Size tableCellSizeForIndex(TableView *table, ssize_t idx);
	virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	std::vector<ObjFriend*> mLstPlayer;

	//CREATE_FUNC(TopScene);
	void initScene();

	void loadTopBalance();
	void loadTopLevel();
	void loadTopFish();
	void update(float dt);
	void showInfoDetail(std::string pUserName);

	void showInfo(std::string pUsername);
	
};

#endif