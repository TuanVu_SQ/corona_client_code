#include "GameScene.h"
#include "ChooseRoomScene.h"
#include "JsonParser.h"
#include "MessageType.h"
#include "mplefttablemessagerequest.h"
#include "mpjointablemessagenotify.h"
#include "mpplayerreadymessagenotify.h"
#include "mpplayerreadymessagerequest.h"
#include "mpfirebulletmessagenotify.h"
#include "mpfirebulletmessagerequest.h"
//#include "mpuseitemmessagenotify.h"
//#include "mpuseitemmessagerequest.h"
#include "mpdeadfishmessagenotify.h"
#include "mpdeadfishmessagerequest.h"
#include "mpendgamemessagenotify.h"
#include "mplistfishesnotify.h"
#include "HomeScene.h"
#include "BoxNoticeManager.h"
#include "mpchatemotionmessagerequest.h"
#include "mpchatmessagerequest.h"
#include "mprechargebycardmessagerequest.h"
#include "mprechargebycardmessageresponse.h"
#include "mpgetinfopurchasemessagerequest.h"
#include "mpgetinfopurchasemessageresponse.h"
#include "mpgetinfoplayergameresponse.h"
#include "MpSwapPositionConfirm.h"
#include "MpSwapPositionIndication.h"
#include "MpSwapPositionNotify.h"
#include "MpSwapPositionRequest.h"
#include "MpSwapPositionResponse.h"
#include "MpKickPlayerNotify.h"
#include "MpKickPlayerRequest.h"
#include "HomeSceneOff.h"
#include "MainScene.h"
#include "libs/WAudioControl.h"

#include "EGJniHelper.h"


ClientPlayerInfoEX* mPlayerShow = NULL;
std::string mErrorCode_Game_New;
std::string mErrorCode_Game;
std::vector<StructTelcoInfo> lstTelcoInfo_game;
//std::vector<TopInfo> mCacheTop;
#define SSTR(x) dynamic_cast< std::ostringstream & >((std::ostringstream() << std::dec << (x))).str()

void pthread_getInfo(void* _void) 
{
	do{
		MpMessageRequest* request = new MpMessageRequest(MP_MSG_GET_ACC_INFO);
		request->setTokenId(GameInfo::mUserInfo.mTokenID);
		std::string pUserName = mPlayerShow->username;
		for (int i = 0; i < pUserName.length(); i++)
		{
			pUserName[i] = std::tolower(pUserName[i]);
		}
		request->addString(MP_PARAM_USERNAME, pUserName);
		MpClientManager::getInstance(0)->sendMessage(request);
		MpClientManager::getInstance(GameInfo::mUserInfo.mGameID + 1);

	} while (0);
}

struct RechargeStruct_game {
public:
	std::string mTelco, mSerrial, mNumber, mUsername;
	Loading* mLoading;
};

void GameScene::onBackPressed() {
	if (!mBoxNotice->isVisible()) {
		WXmlReader* pXml = WXmlReader::create();
		pXml->load(KEY_XML_FILE);
		std::string pContent = pXml->getNodeTextByTagName("content_quitroom").c_str();
		if (((roomStruct*)mInfoRoom)->mGameID == 2){
			pContent = pXml->getNodeTextByTagName("content_quitgame").c_str();
		}
		if (isStartGame && ((roomStruct*)mInfoRoom)->mGameID == 1){
			//pContent = " \n" + pContent;
			if (((roomStruct*)mInfoRoom)->pTypeRoom == 1){
				pContent = __String::createWithFormat(pXml->getNodeTextByTagName("warning_quitgame").c_str(), EGSupport::addDotMoney(((roomStruct*)mInfoRoom)->pBetMoney * 3).c_str())->getCString() + pContent;
			}
			else{
				pContent = pXml->getNodeTextByTagName("warning_quitgame1").c_str() + pContent;
			}
		}
		BoxNoticeManager::getBoxConfirm(mBoxNotice,
			pXml->getNodeTextByTagName("title_quitgame").c_str(),
			pContent.c_str(),
			CC_CALLBACK_0(GameScene::leftGame, this));
		mBoxNotice->setVisible(true);
	}
}

void GameScene::reloadMoneyOff()
{
	if (((roomStruct*)mInfoRoom)->mGameID == 2){
		UserDefault::getInstance()->setStringForKey(KEY_MONEY, __String::createWithFormat("%d", getPlayerByName("")->balance)->getCString());
		UserDefault::getInstance()->setIntegerForKey(KEY_SO, mCountSo);
	}
}

Scene* GameScene::createScene(void *pInfoRoom)
{
	GameScene* pScene = new GameScene();
	GameInfo::mUserInfo.stateScene = 0;
	pScene->initScene(pInfoRoom);
	pScene->autorelease();
	pScene->registerBackPressed();
	return pScene;
}
Scene* GameScene::createSceneWithShop(void *pInfoRoom){
	GameScene* pScene = new GameScene();
	pScene->initScene(pInfoRoom);
	pScene->showBoxShop();
	pScene->autorelease();
	pScene->registerBackPressed();
	return pScene;

}
Scene* GameScene::createSceneWithDiary(void *pInfoRoom){
	GameScene* pScene = new GameScene();
	pScene->initScene(pInfoRoom);
	pScene->showBoxShop();
	pScene->chooseTabShop(2);
	pScene->autorelease();
	pScene->registerBackPressed();
	return pScene;
}
void GameScene::initScene(void *pInfoRoom)
{
	isOpenPopup = false;
	GameInfo::mUserInfo.mIsKeepAlive = false;
	mRangeItem = NULL;
	mBoxteam1 = NULL;
	mBoxteam2 = NULL;
	isFirsTimeSetUp = true;
	mButtonStartReady = NULL;
	mLoadDingWaitFish = NULL;
	layerResultUp = NULL;
	mPointTrap = NULL;
	mDelayOffline = 0;
	mDelayRecoin = 10;
	mDelayEarthQuake = 0;
	mDelaySmallFishOff = 0.1f;
	mCountChange = 0;
	mDelayChangeMap = 300;
	mTaskGame = NULL;
	mLaser = NULL;
	mDelayConnect = DELAY_CONNECT;
	mMoneyTeam1 = 0;
	mDeLayUpdateMoneyOff = 0;
	mCountShoot = 0;
	mDelayOutGame = 11;
	mCountDownGift = 600;
	iTouchGun = false;
	isDirty = false;
	mCountSo = UserDefault::getInstance()->getIntegerForKey(KEY_SO, 0);
	mMoneyTeam2 = 0;
	mDelayPing = 0;
	mIDFish = 1;
	mCurrentPower = 0;
	isChangeMap = false;
	isChangedsetUp = false;
	isStartGame = false;
	mDelayOffline = 0;
	mDelayPingBalance = 0;
	isBonus = false;
	pScoreTeam1 = 0;
	pScoreTeam2 = 0;
	iGift10Minute = false;
	isFirstTime = 0;
	for (int i = 0; i < 4; i++){
		mBonusSpecialFish[i] = NULL;
	}

	JsonParser::getInstance()->parseJsonFile("map-fish");
	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(GameScene::ccTouchBegans, this);
	listener->onTouchMoved = CC_CALLBACK_2(GameScene::ccTouchMove, this);
	listener->onTouchEnded = CC_CALLBACK_2(GameScene::ccTouchUp, this);
	//listener->setSwallowTouches(true);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	roomStruct * roomInfo = (roomStruct*)pInfoRoom;
	roomInfo->pMaxPlayer = 4;
	WXmlReader* pXml = WXmlReader::create();
	if (roomInfo->mGameID == 1){
		showToast(__String::createWithFormat(pXml->getNodeTextByTagName("notice_bet_money").c_str(), EGSupport::addDotMoney(roomInfo->pBetMoney).c_str())->getCString(), 600, Vec2(400, 160));
	}
	mInfoRoom = pInfoRoom;
	
	Size _size = Director::getInstance()->getWinSize();
	mMainLayer = Layer::create();
	this->addChild(mMainLayer);
	CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	if (UserDefault::getInstance()->getBoolForKey(KEY_MUSIC, true)){
		//CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(MUSIC_BACKGROUND, true);
		CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(1);
		WAudioControl::getInstance()->playBackgroundMusic(KEY_MUSIC, true);
		WAudioControl::getInstance()->setVolumeBackground(1);
	}

	mBackGround = ProgressTimer::create(Sprite::create(__String::createWithFormat(IMG_BACK_GROUND_GAME, roomInfo->mBackGroundID)->getCString()));
	mBackGround->setPosition(ccp(_size.width / 2, _size.height / 2));
	mMainLayer->addChild(mBackGround);
	mBackGround->setType(kCCProgressTimerTypeBar);
	mBackGround->setMidpoint(ccp(1, 0));
	mBackGround->setBarChangeRate(ccp(1, 0));
	mBackGround->setPercentage(100);
	mBackGround->setScale(1.1f);
	if (roomInfo->mGameID != 2){
		mBackGroudRiple = RippleSprite::create(__String::createWithFormat(IMG_BACK_GROUND_GAME, roomInfo->mBackGroundID)->getCString());
		mBackGround->addChild(mBackGroudRiple);
		mBackGroudRiple->setPosition(Vec2(400, 240));
	}
	//Sprite* pBlankBlack = Sprite::createWithSpriteFrameName(IMG_BOX_INFO_PLAYER_GAME);
	//pBlankBlack->setAnchorPoint(Vec2(1, 0));
	//pBlankBlack->setZOrder(9);
	//pBlankBlack->setTag(100);
	//pBlankBlack->setPosition(Vec2(_size.width, 0));
	//mMainLayer->addChild(pBlankBlack);
	//pBlankBlack = Sprite::createWithSpriteFrameName(IMG_BOX_INFO_PLAYER_GAME);
	//pBlankBlack->setZOrder(9);
	//pBlankBlack->setTag(101);
	//pBlankBlack->setAnchorPoint(Vec2(1, 1));
	//pBlankBlack->setPosition(Vec2(_size.width, _size.height));
	//mMainLayer->addChild(pBlankBlack);
	for (int i = 0; i < 4; i++){
		GunPlayer* gun = GunPlayer::createGun(((roomStruct*)mInfoRoom)->mGameID);
		gun->setZOrder(8);
		gun->setOnTouch(CC_CALLBACK_0(GameScene::touchGun, this, i));
		mMainLayer->addChild(gun);
		gun->setPosition(Vec2((i / 2) * 800 + ((i / 2)*-2 + 1)*(220 + (i % 2) * 360), (i / 2) * 480));
		if (roomInfo->mGameID == 2 && i == 0){
			gun->setPosition(Vec2(CLIENT_WIDTH / 2, 0));
		}
		else if (roomInfo->mGameID == 2){
			gun->setVisible(false);
		}
		if (i > 1){
			gun->flipY(true);
		}
		else{
			gun->flipY(false);
		}

		mBoxXMoney[i] = BoxResize::createBoxResize(IMG_TOP_BOX_SP, IMG_MID_BOX_SP, IMG_TOP_BOX_SP);
		mBoxXMoney[i]->setAnchor(Vec2(0.5f, 0));

		mBoxXMoney[i]->setDistance(10);

		mBoxXMoney[i]->setZOrder(9);
		if (i <= 1){
			/*for (int j = roomInfo->mListXFactor.size() - 1; j >= 0; j--){
				EGButtonSprite* buttonXMoney = EGButtonSprite::createWithFile("rate_battle_box1.png");
				mBoxXMoney[i]->addButton(buttonXMoney, roomInfo->mListXFactor[j]);
				buttonXMoney->registerTouch();
				buttonXMoney->setOnTouch(CC_CALLBACK_0(GameScene::chooseXMoney, this, j));
				Sprite* sprite = Sprite::create(__String::createWithFormat("rate_battle_label%d.png", roomInfo->mListXFactor[j])->getCString());
				buttonXMoney->addChild(sprite);
				sprite->setPosition(buttonXMoney->getContentSize() / 2);
			}*/
		}
		else{
			/*for (int j = 0; j < roomInfo->mListXFactor.size(); j++){
				EGButtonSprite* buttonXMoney = EGButtonSprite::createWithFile("rate_battle_box1.png");
				mBoxXMoney[i]->addButton(buttonXMoney, roomInfo->mListXFactor[j]);
				buttonXMoney->registerTouch();
				buttonXMoney->setOnTouch(CC_CALLBACK_0(GameScene::chooseXMoney, this, j));
				Sprite* sprite = Sprite::create(__String::createWithFormat("rate_battle_label%d.png", roomInfo->mListXFactor[j])->getCString());
				buttonXMoney->addChild(sprite);
				sprite->setPosition(buttonXMoney->getContentSize() / 2);
			}*/
		}

		mBoxXMoney[i]->visibleObject(1, false);
		EGButtonSprite* buttonShowX = EGButtonSprite::createWithFile("rate_battle_box1.png");
		mMainLayer->addChild(buttonShowX);
		buttonShowX->setPosition(Vec2((i / 2) * 800 + ((i / 2)*-2 + 1)*(220 + (i % 2) * 360) + 130, 25 + (i / 2) * 420));
		buttonShowX->setOnTouch(CC_CALLBACK_0(GameScene::openBoxX, this, mBoxXMoney[i]));
		buttonShowX->setZOrder(10);
		buttonShowX->registerTouch();
		mBoxXMoney[i]->setTag(1);
		mBoxXMoney[i]->setDistance(buttonShowX->getContentSize().height + 2);
		mBoxXMoney[i]->setOffsetY(buttonShowX->getContentSize().height / 2 + 2);
		mBoxXMoney[i]->setPosition(buttonShowX->getContentSize() / 2);
		buttonShowX->addChild(mBoxXMoney[i]);
		mBoxXMoney[i]->setZOrder(-1);
		Sprite* sprite = Sprite::create("rate_battle_label1.png");
		buttonShowX->addChild(sprite);
		sprite->setTag(10);
		sprite->setPosition(buttonShowX->getContentSize() / 2);
		if (i > 1){
			mBoxXMoney[i]->setAnchor(Vec2(0.5f, 1));
			mBoxXMoney[i]->setOffsetY(-buttonShowX->getContentSize().height / 2 - 2);
		}
		mBoxXMoney[i]->visibleStroke(false);
		gun->setButtonXMoney(buttonShowX);
		if (roomInfo){
			for (int j = roomInfo->mListPlayer->size() - 1; j >= 0; j--){
				if (roomInfo->mListPlayer->at(j).position == i){
					roomInfo->mListPlayer->at(j).durationReady = roomInfo->mDurationReadyNormal;
					if (roomInfo->mListPlayer->at(j).host){
						roomInfo->mListPlayer->at(j).durationReady = roomInfo->mDurationReadyHost;
					}
					gun->setUserInfo(&roomInfo->mListPlayer->at(j));
				}
			}
			gun->setEnableChoose(true);
		}
		mListGun.pushBack(gun);
		if (roomInfo->mGameID == 0){
			gun->showInfoMoney(false);
		}
	}

	BoxResize* boxOptionSp = BoxResize::createBoxResize(IMG_TOP_BOX_SP, IMG_MID_BOX_SP, IMG_TOP_BOX_SP);
	boxOptionSp->setHeight(190);
	boxOptionSp->setAnchor(Vec2(0.5f, 0));
	boxOptionSp->setOffsetY(20);
	boxOptionSp->setDistance(70);
	boxOptionSp->setZOrder(9);
	mMainLayer->addChild(boxOptionSp);
	boxOptionSp->setPosition(Vec2(_size.width - 30, 15));
	boxOptionSp->setTag(100);
	if (roomInfo->mGameID != 1){
		if (roomInfo->mListItemSp.size() <= 0){
			for (int i = 0; i < 2; i++){
				ClientItemInfo pInfoItem;
				pInfoItem.id = 200 + i;
				pInfoItem.quantity = 0;
				roomInfo->mListItemSp.push_back(pInfoItem);
			}
		}
		for (int i = 1; i >= 0; i--){
			int pType;
			if (i < roomInfo->mListItemSp.size()){
				pType = roomInfo->mListItemSp.at(i).id;
				pType -= 200;

			}
			else{
				pType = roomInfo->mListItemSp.at(0).id;
				pType -= 200;
				pType = abs(pType - 1);
				ClientItemInfo pInfoItem;
				pInfoItem.id = pType;
				pInfoItem.quantity = 0;
				roomInfo->mListItemSp.push_back(pInfoItem);
			}
			EGButtonSprite *buttonSp = EGButtonSprite::createWithSpriteFrameName(__String::createWithFormat(IMG_ITEM_SP, pType + 1)->getCString());
			buttonSp->setOnTouch(CC_CALLBACK_0(GameScene::chooseItem, this, pType));
			boxOptionSp->addButton(buttonSp);
			buttonSp->setTag(i + 1);
			Label * labelText = Label::createWithBMFont(FONT_NUM_SP, __String::createWithFormat(IMG_ITEM_SP, 0)->getCString());
			buttonSp->addChild(labelText);
			labelText->setTag(1);
			labelText->setPosition(Vec2(buttonSp->getContentSize().width - 10, 10));
			buttonSp->unregisterTouch();
			labelText->setString(__String::createWithFormat("%d", roomInfo->mListItemSp.at(i).quantity)->getCString());
			buttonSp->registerTouch(false);
		}
	}
	mButtonStartReady = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_START);
	mMainLayer->addChild(mButtonStartReady);
	mButtonStartReady->setPosition(Vec2(_size.width / 2, _size.height / 2));
	mButtonStartReady->setVisible(false);
	mButtonStartReady->setOnTouch(CC_CALLBACK_0(GameScene::readyStartGame, this));
	EGButtonSprite *pButtonOption = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_OPTION_GAME);
	mMainLayer->addChild(pButtonOption);
	pButtonOption->setZOrder(9);
	pButtonOption->setTag(101);

	pButtonOption->setPosition(Vec2(boxOptionSp->getPositionX(), boxOptionSp->getPositionY() + 9));
	pButtonOption->setButtonType(EGButton2FrameDark);
	pButtonOption->setOnTouch(CC_CALLBACK_0(GameScene::showBoxOption, this));
	boxOptionSp->closeBox();
	boxOptionSp->setVisible(false);
	pButtonOption->setVisible(false);
	EGButtonSprite *buttonBack = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_BACK_GAME);
	buttonBack->setZOrder(9);
	buttonBack->setScale(0.9f);
	mMainLayer->addChild(buttonBack);
	buttonBack->setPosition(Vec2(_size.width - buttonBack->getContentSize().width / 2 - 3, _size.height - buttonBack->getContentSize().height / 2));
	buttonBack->setButtonType(EGButton2FrameDark);
	buttonBack->setOnTouch([=] {
		if (!mBoxNotice->isVisible()) {
			WXmlReader* pXml = WXmlReader::create();
			pXml->load(KEY_XML_FILE);
			std::string pContent = pXml->getNodeTextByTagName("content_quitroom").c_str();

			if (isStartGame && ((roomStruct*)mInfoRoom)->mGameID == 1){
				//pContent = " \n" + pContent;
				if (((roomStruct*)mInfoRoom)->pTypeRoom == 1){
					pContent = __String::createWithFormat(pXml->getNodeTextByTagName("warning_quitgame").c_str(), EGSupport::addDotMoney(((roomStruct*)mInfoRoom)->pBetMoney * 3).c_str())->getCString() + pContent;
				}
				else{
					pContent = pXml->getNodeTextByTagName("warning_quitgame1").c_str() + pContent;
				}
			}
			BoxNoticeManager::getBoxConfirm(mBoxNotice,
				pXml->getNodeTextByTagName("title_quitgame").c_str(),
				pContent.c_str(),
				CC_CALLBACK_0(GameScene::leftGame, this));
			mBoxNotice->setVisible(true);
		}
	});
	buttonInvite = EGButtonSprite::createWithSpriteFrameName("button-invite-game.png");
	buttonInvite->setScale(0.9f);
	buttonInvite->setZOrder(9);
	mMainLayer->addChild(buttonInvite);
	buttonInvite->setPosition(Vec2(_size.width - 3 * buttonBack->getContentSize().width / 2 - 6, _size.height - buttonBack->getContentSize().height / 2));
	buttonInvite->setButtonType(EGButton2FrameDark);
	buttonInvite->setOnTouch(CC_CALLBACK_0(GameScene::showBoxInvite, this));
	buttonInvite->setVisible(false);
	Sprite* pClockBox = Sprite::createWithSpriteFrameName(IMG_CLOCK);
	this->addChild(pClockBox);
	if (roomInfo->mGameID != 1){
		pClockBox->setPosition(Vec2(pClockBox->getContentSize().width / 2, CLIENT_HEIGHT / 2));
	}
	else{
		pClockBox->setPosition(Vec2(CLIENT_WIDTH - pClockBox->getContentSize().width / 2, _size.height - 60 - pClockBox->getContentSize().height / 2));
	}
	mTimeLabel = Label::createWithBMFont(FONT_DIGITAL, "00:00");
	pClockBox->addChild(mTimeLabel);
	mTimeLabel->setPosition(Vec2(pClockBox->getContentSize().width / 2 + 16, pClockBox->getContentSize().height / 2));
	if (roomInfo->mGameID != 2){
		boxChat = BoxChat::create(GameInfo::mUserInfo.mFullname,
			CC_CALLBACK_1(GameScene::onSendText, this), CC_CALLBACK_2(GameScene::onSendEmotion, this));
		mMainLayer->addChild(boxChat);
		boxChat->setZOrder(9);
	}
	if (roomInfo->mGameID == 0){
		mTimeLabel->setVisible(false);
		buttonInvite->setVisible(false);
		pClockBox->setVisible(false);
		startGame();

	}
	else if (roomInfo->mGameID == 2){
		if (roomInfo->mGameID == 2 && getInfoMyselft()){
			getInfoMyselft()->balance = atol(UserDefault::getInstance()->getStringForKey(KEY_MONEY, "500").c_str());
			getInfoMyselft()->balanceDefault = atol(UserDefault::getInstance()->getStringForKey(KEY_MONEY, "500").c_str());
		}
		mCountRecoin = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_ITEM_SP, 4)->getCString());
		mListInfoGun = new std::vector<Vec3>();
		for (int i = 0; i <= 9; i++){
			int pUpgradeDame = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_DAME_GUN, i + 1)->getCString());
			int pUgradeSpeed = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_SPEED_GUN, i + 1)->getCString());
			mListInfoGun->push_back(Vec3(i + 1, pUpgradeDame, pUgradeSpeed));
		}
		mListFishDead = new std::vector<Vec2*>;
		mLevel = UserDefault::getInstance()->getIntegerForKey(KEY_LEVEL, 1);
		mCurrentTask = UserDefault::getInstance()->getIntegerForKey(KEY_CURRENT_TASK, 0);
		mLimitExp = 0;
		mCurrentExp = 0;
		for (int i = 0; i < mLevel; i++){
			mLimitExp += 200 * (i + 1);
		}
		mTaskGame = TaskGame::getTask(mCurrentTask);
		mListGun.at(0)->visibeInfo(false);
		mTimeLabel->setVisible(false);
		buttonInvite->setVisible(false);
		pClockBox->setVisible(false);
		startGame();
		Sprite* pBar = Sprite::createWithSpriteFrameName(IMG_BAR_INFO_GAME);
		mMainLayer->addChild(pBar);
		pBar->setAnchorPoint(Vec2(0, 0));
		pBar->setZOrder(7);
		mMainLayer->getChildByTag(100)->setVisible(false);
		mMainLayer->getChildByTag(101)->setVisible(false);
		buttonBack->setVisible(false);
		EGButtonSprite* pButtonBack = EGButtonSprite::createWithSpriteFrameName(IMG_BT_BACK_GAME_OFF);
		pButtonBack->setAnchorPoint(Vec2(0, 1));
		mMainLayer->addChild(pButtonBack);
		pButtonBack->setPosition(Vec2(0, CLIENT_HEIGHT));
		pButtonBack->setButtonType(EGButton2FrameDark);
		pButtonBack->setOnTouch([=] {
			WXmlReader* pXml = WXmlReader::create();
			pXml->load(KEY_XML_FILE);
			BoxNoticeManager::getBoxConfirm(mBoxNotice,
				pXml->getNodeTextByTagName("title_quitgame").c_str(),
				pXml->getNodeTextByTagName("content_quitgame_off").c_str(),
				CC_CALLBACK_0(GameScene::leftGame, this));
			mBoxNotice->setVisible(true);
		});
		pButtonBack->setZOrder(7);
		Sprite* pBoxStatusGame = Sprite::createWithSpriteFrameName(IMG_BT_STATUS_GAME);
		mMainLayer->addChild(pBoxStatusGame);
		pBoxStatusGame->setAnchorPoint(Vec2(1, 1));
		pBoxStatusGame->setPosition(Vec2(CLIENT_WIDTH, CLIENT_HEIGHT));
		pBoxStatusGame->setZOrder(7);
		for (int i = 0; i < 3; i++){
			EGSprite* btSupport = EGSprite::createWithSpriteFrameName(IMG_BUBBLE);
			mMainLayer->addChild(btSupport);
			btSupport->setPosition(Vec2(CLIENT_WIDTH / 2 - btSupport->getContentSize().width - 10 + i*(btSupport->getContentSize().width + 10), CLIENT_HEIGHT - 40));
			btSupport->setZOrder(7);
			btSupport->setTag(200 + i);
			btSupport->unregisterTouch();
			btSupport->registerTouch(false);
			btSupport->setOnTouch(CC_CALLBACK_0(GameScene::chooseItem, this, i));

			Sprite* icon = Sprite::createWithSpriteFrameName(__String::createWithFormat(IMG_ICON_SP, i + 1)->getCString());
			btSupport->addChild(icon);
			icon->setPosition(Vec2(btSupport->getContentSize().width / 2, btSupport->getContentSize().height / 2));

			Label* pNumbCountItem = Label::createWithBMFont("font-item-off-scene.fnt", "");
			btSupport->addChild(pNumbCountItem);
			int pCount = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_ITEM_SP, i + 1)->getCString(), 3);
			pNumbCountItem->setString(__String::createWithFormat("%d", pCount)->getCString());
			pNumbCountItem->setPosition(Vec2(btSupport->getContentSize().width - 5, 5));
			pNumbCountItem->setTag(1);
			if (i < roomInfo->mListItemSp.size()){
				roomInfo->mListItemSp.at(i).id = i;

				roomInfo->mListItemSp.at(i).quantity = pCount;
			}
			else{
				ClientItemInfo itemInfo;
				itemInfo.id = i;
				itemInfo.quantity = 0;
				roomInfo->mListItemSp.push_back(itemInfo);
			}
		}
		EGButtonSprite* buttonShowShop = EGButtonSprite::createWithSpriteFrameName(IMG_BT_SHOWSHOP);
		mMainLayer->addChild(buttonShowShop);
		buttonShowShop->setZOrder(7);
		buttonShowShop->setPosition(Vec2(CLIENT_WIDTH / 2 - 160, CLIENT_HEIGHT - buttonShowShop->getContentSize().height / 2));
		buttonShowShop->setButtonType(EGButton2FrameDark);
		buttonShowShop->setOnTouch(CC_CALLBACK_0(GameScene::showBoxShop, this));
		EGButtonSprite * buttonShowQuest = EGButtonSprite::createWithSpriteFrameName(IMG_BT_SHOWQUEST);
		mMainLayer->addChild(buttonShowQuest);
		buttonShowQuest->setPosition(Vec2(CLIENT_WIDTH / 2 + 160, CLIENT_HEIGHT - buttonShowQuest->getContentSize().height / 2));
		buttonShowQuest->setZOrder(7);
		buttonShowQuest->setButtonType(EGButton2FrameDark);
		buttonShowQuest->setOnTouch(CC_CALLBACK_0(GameScene::showBoxQuest, this));
		Sprite* boxMoney = Sprite::createWithSpriteFrameName(IMG_BOX_MONEY_OFF);
		pBar->addChild(boxMoney);
		boxMoney->setAnchorPoint(Vec2(0, 0));
		boxMoney->setPositionY(5);
		Sprite* boxSo = Sprite::createWithSpriteFrameName(IMG_BOX_SO_OFF);
		pBar->addChild(boxSo);
		boxSo->setAnchorPoint(Vec2(1, 0));
		boxSo->setPosition(Vec2(CLIENT_WIDTH - 3, 5));
		Sprite* strokePower = Sprite::createWithSpriteFrameName(IMG_STROKE_POWER);
		strokePower->setAnchorPoint(Vec2(0, 0));
		strokePower->setPosition(Vec2(CLIENT_WIDTH / 2 + 160, 1));
		pBar->addChild(strokePower);
		barPower = ProgressTimer::create(Sprite::createWithSpriteFrameName(IMG_BAR_POWER));
		strokePower->addChild(barPower);
		barPower->setBarChangeRate(Vec2(1, 0));
		barPower->setMidpoint(Vec2(0, 0));
		barPower->setType(kCCProgressTimerTypeBar);
		barPower->setPercentage(0);
		barPower->setPosition(Vec2(strokePower->getContentSize().width / 2, strokePower->getContentSize().height / 2));
		mLabelMoneyOff = Label::createWithBMFont(FONT_MONEY_GAME, "1000000");
		mLabelMoneyOff->setAlignment(kCCTextAlignmentLeft);
		mLabelMoneyOff->setAnchorPoint(Vec2(0, 0.5f));
		boxMoney->addChild(mLabelMoneyOff);
		mLabelMoneyOff->setPosition(Vec2(35, boxMoney->getContentSize().height / 2 - 2));
		mLabelCountReCoin = Label::createWithBMFont(FONT_MONEY_GAME, "10");
		mLabelCountReCoin->setAlignment(kCCTextAlignmentCenter);
		boxMoney->addChild(mLabelCountReCoin);
		mLabelCountReCoin->setPosition(Vec2(180, boxMoney->getContentSize().height / 2 - 2));
		mLabelSoOff = Label::createWithBMFont(FONT_MONEY_GAME, "10");
		mLabelSoOff->setAlignment(kCCTextAlignmentLeft);
		mLabelSoOff->setAnchorPoint(Vec2(0, 0.5f));
		boxSo->addChild(mLabelSoOff);
		mLabelSoOff->setPosition(Vec2(50, boxMoney->getContentSize().height / 2 - 2));
		Sprite* pStrokeStage = Sprite::createWithSpriteFrameName(IMG_STROKE_STAGE);
		pBoxStatusGame->addChild(pStrokeStage);
		pStrokeStage->setPosition(Vec2(84, pBoxStatusGame->getContentSize().height / 2 + 3));
		barStage = ProgressTimer::create(Sprite::createWithSpriteFrameName(IMG_BAR_STAGE));
		pStrokeStage->addChild(barStage);
		barStage->setBarChangeRate(Vec2(0, 1));
		barStage->setMidpoint(Vec2(0, 0));
		barStage->setType(kCCProgressTimerTypeBar);
		barStage->setPercentage(50);
		barStage->setPosition(Vec2(pStrokeStage->getContentSize().width / 2, pStrokeStage->getContentSize().height / 2));
		numStage = EGNumberSprite::createWithSpriteFrameName(IMG_NUM_TAB);
		pStrokeStage->addChild(numStage);
		numStage->setDistanceSpace(-10);
		numStage->setNumber(mLevel);
		if (mLevel > 9){
			numStage->setPosition(Vec2(pStrokeStage->getContentSize().width / 2 + 10, pStrokeStage->getContentSize().height / 2));
		}
		else{
			numStage->setPosition(Vec2(pStrokeStage->getContentSize().width / 2, pStrokeStage->getContentSize().height / 2));
		}

		mBoxShop = LayerColor::create(ccc4(0, 0, 0, 155));
		mMainLayer->addChild(mBoxShop);
		mBoxShop->setZOrder(10);
		EGSprite *boxShop1 = EGSprite::createWithSpriteFrameName(IMG_BOX_SHOP_OFF);
		mBoxShop->addChild(boxShop1);
		boxShop1->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2 - 10));
		//boxShop->setZOrder(1);
		boxShop1->registerTouch(true);
		boxShop1->setBindingRect(false);
		EGSprite *boxShop = EGSprite::createWithSpriteFrameName(IMG_BOX_SHOP_OFF);
		mBoxShop->addChild(boxShop);
		boxShop->setTag(1);
		boxShop->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2 - 10));
		boxShop->setZOrder(1);

		EGButtonSprite* buttonClose = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_CLOSE_CR);
		boxShop->addChild(buttonClose);
		buttonClose->setPosition(Vec2(boxShop->getContentSize().width - 5, boxShop->getContentSize().height - 5));
		buttonClose->setButtonType(EGButton2FrameDark);
		buttonClose->setOnTouch(CC_CALLBACK_0(GameScene::closeBoxShop, this));
		WXmlReader* xml = WXmlReader::create();
		xml->load(KEY_XML_FILE);
		for (int i = 0; i < 3; i++){
			EGSprite* buttonTab = EGSprite::createWithSpriteFrameName(__String::createWithFormat(IMG_BUTTON_TAB_SHOP_OFF, i + 1, 1)->getCString());
			mBoxShop->addChild(buttonTab);
			buttonTab->setTag(i + 2);
			buttonTab->registerTouch(true);
			buttonTab->setOnTouch(CC_CALLBACK_0(GameScene::chooseTabShop, this, i));
			buttonTab->setPosition(Vec2(CLIENT_WIDTH / 2 - buttonTab->getContentSize().width + i* buttonTab->getContentSize().width, CLIENT_HEIGHT / 2 + boxShop->getContentSize().height / 2 + 10));
			Node* pLayerShop = Node::create();
			pLayerShop->setTag(i + 1);
			boxShop->addChild(pLayerShop);
			if (i < 2){
				if (i == 0){
					for (int i = 0; i < 9; i++){
						EGButtonSprite* buttonChooseWeaponShop = EGButtonSprite::createWithSpriteFrameName(__String::createWithFormat(IMG_BUTTON_CHOOSE_WEAPON_OFF, i + 1)->getCString());
						buttonChooseWeaponShop->setPosition(Vec2(90 + i % 3 * (buttonChooseWeaponShop->getContentSize().width + 5), boxShop->getContentSize().height - 70 - (i / 3) * (buttonChooseWeaponShop->getContentSize().height + 5)));
						pLayerShop->addChild(buttonChooseWeaponShop);
						buttonChooseWeaponShop->setButtonType(EGButton2FrameDark);
						buttonChooseWeaponShop->setTag(i + 1);
						buttonChooseWeaponShop->setOnTouch(CC_CALLBACK_0(GameScene::chooseItemShop, this, i));

					}
					Sprite* subBox = Sprite::createWithSpriteFrameName(IMG_SUB_BOX_SHOP_OFF);
					pLayerShop->addChild(subBox);
					subBox->setPosition(Vec2(boxShop->getContentSize().width / 2 + subBox->getContentSize().width / 2, boxShop->getContentSize().height / 2));
					Sprite* pItem = Sprite::create();
					pLayerShop->addChild(pItem);
					pItem->setPosition(Vec2(subBox->getPositionX(), subBox->getPositionY() + 60));
					pItem->setScale(1.4f);
					pItem->setTag(11);
					EGButtonSprite* buttonBuyShopDame = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_BUY_COIN);
					pLayerShop->addChild(buttonBuyShopDame);
					buttonBuyShopDame->setPosition(Vec2(subBox->getPositionX() + 62, subBox->getPositionY() - buttonBuyShopDame->getContentSize().height / 2 - 20));
					buttonBuyShopDame->setButtonType(EGButton2FrameDark);
					buttonBuyShopDame->setOnTouch(CC_CALLBACK_0(GameScene::upgradeGun, this, 0));
					Label* pLabelDameShop = Label::createWithBMFont(FONT_SHOP_OFF, xml->getNodeTextByTagName("label-dame-shop"));
					pLayerShop->addChild(pLabelDameShop);
					pLabelDameShop->setPosition(subBox->getPositionX() - 90, subBox->getPositionY() - buttonBuyShopDame->getContentSize().height / 2 - 20);
					pLabelDameShop->setAlignment(kCCTextAlignmentCenter);
					pLabelDameShop->setTag(12);
					Label* pLabelPriceDame = Label::createWithBMFont(FONT_SHOP_OFF, xml->getNodeTextByTagName("label-upgrade-shop"));
					pLayerShop->addChild(pLabelPriceDame);
					pLabelPriceDame->setPosition(subBox->getPositionX() + 76, subBox->getPositionY() - buttonBuyShopDame->getContentSize().height / 2 - 20);
					pLabelPriceDame->setAlignment(kCCTextAlignmentLeft);
					pLabelPriceDame->setScale(0.8f);
					pLabelPriceDame->setTag(13);
					EGButtonSprite* buttonBuyShopSpeed = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_BUY_COIN);
					pLayerShop->addChild(buttonBuyShopSpeed);
					buttonBuyShopSpeed->setPosition(Vec2(subBox->getPositionX() + 62, subBox->getPositionY() - buttonBuyShopSpeed->getContentSize().height - 60));
					buttonBuyShopSpeed->setButtonType(EGButton2FrameDark);
					buttonBuyShopSpeed->setOnTouch(CC_CALLBACK_0(GameScene::upgradeGun, this, 1));
					Label* pLabelSpeedShop = Label::createWithBMFont(FONT_SHOP_OFF, xml->getNodeTextByTagName("label-speed-shop"));
					pLayerShop->addChild(pLabelSpeedShop);
					pLabelSpeedShop->setPosition(subBox->getPositionX() - 90, subBox->getPositionY() - buttonBuyShopSpeed->getContentSize().height - 60);
					pLabelSpeedShop->setAlignment(kCCTextAlignmentCenter);
					pLabelSpeedShop->setTag(14);
					Label* pLabelPriceSpeed = Label::createWithBMFont(FONT_SHOP_OFF, xml->getNodeTextByTagName("label-upgrade-shop"));
					pLayerShop->addChild(pLabelPriceSpeed);
					pLabelPriceSpeed->setPosition(subBox->getPositionX() + 76, subBox->getPositionY() - buttonBuyShopSpeed->getContentSize().height - 60);
					pLabelPriceSpeed->setAlignment(kCCTextAlignmentLeft);
					pLabelPriceSpeed->setScale(0.8f);
					pLabelPriceSpeed->setTag(15);
				}
				else{
					for (int i = 0; i < 4; i++){
						EGButtonSprite* buttonChoose = EGButtonSprite::createWithSpriteFrameName(__String::createWithFormat(IMG_BT_CHOOSE_ITEM_SP_OFF, i + 1)->getCString());
						pLayerShop->addChild(buttonChoose);
						buttonChoose->setPosition(Vec2(180, boxShop->getContentSize().height - 60 - i * (buttonChoose->getContentSize().height - 5)));
						buttonChoose->setButtonType(EGButton2FrameDark);
						buttonChoose->setTag(i + 1);
						buttonChoose->setOnTouch(CC_CALLBACK_0(GameScene::chooseItemShop, this, i));
					}
					Sprite* subBox = Sprite::createWithSpriteFrameName(IMG_SUB_BOX_SHOP_OFF);
					pLayerShop->addChild(subBox);
					subBox->setPosition(Vec2(boxShop->getContentSize().width / 2 + subBox->getContentSize().width / 2, boxShop->getContentSize().height / 2));
					Sprite* pItem = Sprite::create();
					pLayerShop->addChild(pItem);
					pItem->setPosition(Vec2(subBox->getPositionX(), subBox->getPositionY() + 60));
					pItem->setScale(1.4f);
					pItem->setTag(11);
					Label* pDescriptionItem = Label::createWithBMFont("font-arial-shop.fnt", xml->getNodeTextByTagName("description-item1"));
					pLayerShop->addChild(pDescriptionItem);
					pDescriptionItem->setPosition(subBox->getPositionX(), subBox->getPositionY() - 20);
					pDescriptionItem->setAlignment(kCCTextAlignmentCenter);
					pDescriptionItem->setTag(12);
					EGButtonSprite* buttonBuyShopSo = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_BUY_SO);
					pLayerShop->addChild(buttonBuyShopSo);
					buttonBuyShopSo->setPosition(Vec2(subBox->getPositionX(), 70));
					buttonBuyShopSo->setButtonType(EGButton2FrameDark);
					buttonBuyShopSo->setOnTouch(CC_CALLBACK_0(GameScene::upgradeGun, this, 0));

					Label* labelPrice = Label::createWithBMFont(FONT_SHOP_OFF, "300 Nang Cap");
					labelPrice->setScale(0.8f);
					pLayerShop->addChild(labelPrice);
					labelPrice->setPosition(buttonBuyShopSo->getPositionX() + 20, buttonBuyShopSo->getPositionY());
					labelPrice->setAlignment(kCCTextAlignmentLeft);
					labelPrice->setTag(13);
				}
				Sprite* boxMoneyShop = Sprite::createWithSpriteFrameName(IMG_BOX_MONEY_SHOP);
				if (i == 1){
					boxMoneyShop->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(IMG_BOX_SO_SHOP));
				}
				pLayerShop->addChild(boxMoneyShop);
				boxMoneyShop->setPosition(Vec2(40 + boxMoneyShop->getContentSize().width / 2, 60));

				EGButtonSprite* buttonTopUp = EGButtonSprite::createWithSpriteFrameName(__String::createWithFormat(IMG_BUTTON_TOPUP_SHOP, i + 1)->getCString());
				pLayerShop->addChild(buttonTopUp);
				buttonTopUp->setPosition(Vec2(boxMoneyShop->getPositionX() + 150, boxMoneyShop->getPositionY()));
				buttonTopUp->setButtonType(EGButton2FrameDark);

				if (i == 0){
					buttonTopUp->setOnTouch([=] {
						showBoxTopUp();
					});
				}
				else if (i == 1){
					buttonTopUp->setOnTouch([=] {
						showTopUpSo();
					});
				}

				Label* labelMoneyCoin = Label::createWithBMFont(FONT_SHOP_OFF, "1000");
				pLayerShop->addChild(labelMoneyCoin);
				labelMoneyCoin->setColor(ccc3(255, 255, 0));
				labelMoneyCoin->setPosition(Vec2(boxMoneyShop->getPositionX() + 10, boxMoneyShop->getPositionY() - 5));
				labelMoneyCoin->setTag(100);
			}
			else {
				Sprite* pFishDiary = Sprite::createWithSpriteFrameName(IMG_DIARY_FISH);
				pLayerShop->addChild(pFishDiary);
				pFishDiary->setPosition(Vec2(boxShop->getContentSize().width / 2 - 20, boxShop->getContentSize().height / 2));
				for (int i = 0; i < 20; i++){
					Label* labelCount = Label::createWithBMFont(FONT_SHOP_OFF, "0");
					pLayerShop->addChild(labelCount);
					labelCount->setAlignment(kCCTextAlignmentLeft);
					labelCount->setPosition(Vec2(135 + 170 * (i / 5), boxShop->getContentSize().height - 70 - 63 * (i % 5)));
					labelCount->setTag(i + 1);
				}
			}
		}
		chooseTabShop(0);
		//showBoxShop();
		closeBoxShop();
		mBoxQuest = LayerColor::create(ccc4(0, 0, 0, 155));
		mMainLayer->addChild(mBoxQuest);
		mBoxQuest->setZOrder(12);
		EGSprite* boxQuest = EGSprite::createWithSpriteFrameName(IMG_BOX_QUEST);
		mBoxQuest->addChild(boxQuest);
		boxQuest->setTag(200);
		boxQuest->setPosition(Vec2(CLIENT_WIDTH / 2 + 30, CLIENT_HEIGHT / 2));
		closeBoxQuest();
		boxQuest->registerTouch(true);
		boxQuest->setBindingRect(false);
		Sprite* pBoxQuantityQuest = Sprite::createWithSpriteFrameName("box-quantity-quest.png");
		boxQuest->addChild(pBoxQuantityQuest);
		pBoxQuantityQuest->setPosition(Vec2(50, 30));
		Label* pQuantityLabel = Label::createWithBMFont("font-num-room.fnt", "0");
		boxQuest->addChild(pQuantityLabel);
		pQuantityLabel->setPosition(pBoxQuantityQuest->getPosition());
		pQuantityLabel->setPositionY(pQuantityLabel->getPositionY() - 5);
		pQuantityLabel->setTag(100);
		EGButtonSprite* buttonCloseQuest = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_CLOSE_CR);
		mBoxQuest->addChild(buttonCloseQuest);
		buttonCloseQuest->setPosition(Vec2(boxQuest->getPositionX() + boxQuest->getContentSize().width / 2 - 100, boxQuest->getPositionY() + boxQuest->getContentSize().height / 2 - 60));
		buttonCloseQuest->setButtonType(EGButton2FrameDark);
		buttonCloseQuest->setOnTouch(CC_CALLBACK_0(GameScene::closeBoxQuest, this));
		//mBoxQuest->setVisible(false);

		mBoxEvenMaiDen = LayerColor::create(ccc4(0, 0, 0, 155));
		mMainLayer->addChild(mBoxEvenMaiDen);
		mBoxEvenMaiDen->setZOrder(11);
		EGSprite* boxMaiDen = EGSprite::createWithSpriteFrameName(IMG_BOX_MAIDEN);
		mBoxEvenMaiDen->addChild(boxMaiDen);
		boxMaiDen->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
		boxMaiDen->setTag(1);
		boxMaiDen->registerTouch(true);
		boxMaiDen->setBindingRect(false);
		Sprite* menu = Sprite::createWithSpriteFrameName(IMG_MENU_MAIDEN);
		boxMaiDen->addChild(menu);
		menu->setPosition(Vec2(boxMaiDen->getContentSize().width / 2 - 30, boxMaiDen->getContentSize().height / 2 - 20));
		Sprite* circle = Sprite::createWithSpriteFrameName(IMG_CIRCLE_MAIDEN);
		circle->setTag(1);
		boxMaiDen->addChild(circle);
		circle->setPosition(Vec2(boxMaiDen->getContentSize().width - 30 - circle->getContentSize().width / 2, boxMaiDen->getContentSize().height / 2 - 20));
		Sprite* circleArrow = Sprite::createWithSpriteFrameName(IMG_CIRCLE_ARROW_MAIDEN);
		boxMaiDen->addChild(circleArrow);
		circleArrow->setPosition(Vec2(boxMaiDen->getContentSize().width - 30 - circle->getContentSize().width / 2, boxMaiDen->getContentSize().height / 2 - 20));
		EGButtonSprite* buttonQuay = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_QUAY_MAIDEN);
		buttonQuay->setButtonType(EGButton2FrameDark);
		buttonQuay->setPosition(Vec2(menu->getPositionX(), 60));
		boxMaiDen->addChild(buttonQuay);
		buttonQuay->setTag(2);
		buttonQuay->setOnTouch(CC_CALLBACK_0(GameScene::startEventMaiden, this));


		mBoxEvenShark = LayerColor::create(ccc4(0, 0, 0, 155));
		mMainLayer->addChild(mBoxEvenShark);
		mBoxEvenShark->setZOrder(11);
		EGSprite* boxShark = EGSprite::createWithSpriteFrameName(IMG_BOX_SHARK);
		mBoxEvenShark->addChild(boxShark);
		boxShark->setPosition(Vec2(CLIENT_WIDTH / 2 + 20, CLIENT_HEIGHT / 2));
		boxShark->setTag(1);
		boxShark->registerTouch(true);
		boxShark->setBindingRect(false);
		int pGiftTreasure = rand() % 4;
		for (int i = 0; i < 4; i++){
			EGSprite* pTreasure = EGSprite::createWithSpriteFrameName(__String::createWithFormat(IMG_TREASURE, 1)->getCString());
			boxShark->addChild(pTreasure);
			pTreasure->setPosition(Vec2(130 + (i / 2) * 150, 130 + (i % 2) * 140));
			if (i == pGiftTreasure){
				pTreasure->setUserData(new int());
				pTreasure->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_TREASURE, 2)->getCString()));
			}
			pTreasure->setScale(0.6f);
			pTreasure->setTag(i + 1);
			pTreasure->setOnTouch(CC_CALLBACK_0(GameScene::chooseTreasure, this, pTreasure));
		}
		closeBoxShark();
		mElectric = Sprite::create();
		Animation* pArrayElectric = Animation::create();
		for (int i = 16; i >= 0; i--){
			int index = 17 - i;
			pArrayElectric->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_ELECTRIC, index)->getCString()));
		}
		pArrayElectric->setDelayPerUnit(0.2f);
		mElectric->runAction(RepeatForever::create(Animate::create(pArrayElectric)));
		mElectric->setAnchorPoint(ccp(0.5f, 0));
		mElectric->setPosition(ccp(_size.width / 2 - 5, 30));
		mMainLayer->addChild(mElectric);
		mElectric->setZOrder(4);
		mElectric->setVisible(false);
		mElectric->runAction(Hide::create());
		time_t t = time(0);
		struct tm * now = localtime(&t);
		std::string mCurentDate = __String::createWithFormat("%d/%d/%d", now->tm_mday, now->tm_mon + 1, now->tm_year + 1900)->getCString();
		std::string pLasteDate = UserDefault::sharedUserDefault()->getStringForKey("key-datetime", mCurentDate.c_str());
		if (pLasteDate != mCurentDate){

			int pLastIndex = pLasteDate.length() - 1;
			int pTime = 0;
			int pType = 0;
			for (int i = pLasteDate.length() - 1; i >= 0; i--){
				std::string pStr = pLasteDate.substr(i, 1);
				if (pStr == "/" || pType == 2){
					if (pType == 0){
						pTime += 24 * 365 * (now->tm_year + 1900 - atoi(pLasteDate.substr(i + 1, pLastIndex - i).c_str()));
						pType = 1;
						pLastIndex = i;
					}
					else if (pType == 1) {
						pTime += 24 * 30 * (now->tm_mon + 1 - atoi(pLasteDate.substr(i + 1, pLastIndex - i).c_str()));
						pType = 2;
						pLastIndex = i;
					}
					else if (pType == 2){
						i = -1;
						pTime += 24 * (now->tm_mday - atoi(pLasteDate.substr(i + 1, pLastIndex - i).c_str()));
					}
				}
			}
			if (pTime > 999){
				pTime = 999;
			}
			if (pTime < 0){
				pTime = 0;
			}
			EGSprite* pBox = EGSprite::createWithSpriteFrameName("box-free-money-off.png");
			pBox->registerTouch(true);
			pBox->setBindingRect(false);
			pBox->setUserData(new int(pTime));
			pBox->setOnTouch(CC_CALLBACK_0(GameScene::giftOff, this, pBox));
			mMainLayer->addChild(pBox);
			pBox->setZOrder(9);
			pBox->setPosition(ccp(_size.width / 2, _size.height / 2));
			EGNumberSprite* numGift = EGNumberSprite::createWithSpriteFrameName("big-num");
			numGift->setNumber(pTime);
			numGift->setDistanceSpace(-30);
			numGift->setScale(1.5f);
			pBox->addChild(numGift);
			numGift->setPosition(ccp(pBox->getContentSize().width / 2, pBox->getContentSize().height / 2 - 25));
		}
		UserDefault::getInstance()->setStringForKey("key-datetime", mCurentDate.c_str());



		growMoney(0);
		closeBoxMaiDen();
		if (!UserDefault::getInstance()->getBoolForKey("KEY_GUIDE", false)){
			EGButtonSprite* pBoxRemider = EGButtonSprite::createWithSpriteFrameName("box-remider.png");
			pBoxRemider->setZOrder(9);
			mMainLayer->addChild(pBoxRemider);
			pBoxRemider->setPosition(ccp(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
			pBoxRemider->registerTouch(true);
			pBoxRemider->setBindingRect(false);
			pBoxRemider->setOnUp(CC_CALLBACK_0(EGSprite::performRelease, pBoxRemider));
			UserDefault::getInstance()->setBoolForKey("KEY_GUIDE", true);
		}
		//changeMap();
		//bonusMap(0);
		refreshItemSp();
	}
	mTreasure = EGSprite::createWithSpriteFrameName(__String::createWithFormat("treasure (%d).png", 1)->getCString());
	mTreasure->setZOrder(5);
	mMainLayer->addChild(mTreasure);
	if (roomInfo->mGameID != 1){
		mTreasure->setPosition(ccp(_size.width - mTreasure->getContentSize().width / 2 + 30, _size.height - 60 - mTreasure->getContentSize().height / 2));
	}
	else{
		mTreasure->setPosition(Vec2(1200, 1200));
		//mBoxInvite = BoxInvite::createBox();
		//this->addChild(mBoxInvite);
		//mBoxInvite->setInfoGame(pInfoRoom);
		//mBoxInvite->closeBox();
		buttonOption = EGButtonSprite::createWithSpriteFrameName("button-option-game.png");
		mMainLayer->addChild(buttonOption);
		buttonOption->setPosition(Vec2(CLIENT_WIDTH - 3 - buttonOption->getContentSize().width / 2, 2 + buttonOption->getContentSize().height / 2));
		buttonOption->setButtonType(EGButton2FrameDark);
		buttonOption->setOnTouch(CC_CALLBACK_0(GameScene::showBoxCreateRoom, this));
		buttonOption->setZOrder(9);
	}
	mTreasure->setVisible(false);
	mTreasure->setUserData(new int());
	mTreasure->setOnTouch(CC_CALLBACK_0(GameScene::gift10minutesRequest, this));
	mTimeTreasure = Label::createWithBMFont(FONT_DIGITAL, "00:00");
	mMainLayer->addChild(mTimeTreasure);
	mTimeTreasure->setZOrder(5);
	mTimeTreasure->setVisible(false);
	mTimeTreasure->setPosition(ccp(mTreasure->getPositionX() - 4, mTreasure->getPositionY() - mTreasure->getContentSize().height / 2 - mTimeTreasure->getContentSize().height / 2 + 20));
	mTimeTreasure->setColor(Color3B(255, 255, 0));
	if (roomInfo->mGameID != 2){
		gift10minutesRequest();
		iGift10Minute = false;
	}
	mErrorCode_Game = TEXT_SUCCESS_CHOOSEPOSITION;

	createBoxCreate();
	mLoadDingWaitFish = Sprite::create("view_mail_length.png");
	this->addChild(mLoadDingWaitFish);
	mLoadDingWaitFish->setZOrder(9);
	mLoadDingWaitFish->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	mLoadDingWaitFish->setVisible(true);
	
	auto lbWait = Label::createWithBMFont("font-info-game.fnt", "Tanpri tann pwochen pwason an");
	//lbWait->setScale(1.2f);
	lbWait->setPosition(Vec2(mLoadDingWaitFish->getContentSize().width / 2, mLoadDingWaitFish->getContentSize().height/2));
	//lbWait->setColor(Color3B::ORANGE);
	mLoadDingWaitFish->addChild(lbWait);

	if (roomInfo->mGameID == 1 || roomInfo->mGameID == 2){
		mLoadDingWaitFish->pauseSchedulerAndActions();
		mLoadDingWaitFish->setVisible(false);
	}


	mBoxNotice = BoxNotice::create();
	mBoxNotice->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBoxNotice, 11);
	roomStruct* _pInfoRoom = (roomStruct*)mInfoRoom;
	_pInfoRoom->mBoxNotice = mBoxNotice;
	mButtonTopup = EGSprite::createWithSpriteFrameName("button-topup-game1.png");
	mMainLayer->addChild(mButtonTopup);
	mButtonTopup->setPosition(Vec2(mButtonTopup->getContentSize().width / 2, pClockBox->getPositionY() + pClockBox->getContentSize().height / 2 + mButtonTopup->getContentSize().height / 2));
	Animation* animation = Animation::create();
	for (int i = 0; i < 2; i++){
		animation->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat("button-topup-game%d.png", i + 1)->getCString()));

	}
	animation->setDelayPerUnit(0.4f);
	mButtonTopup->runAction(RepeatForever::create(Animate::create(animation)));
	mButtonTopup->setVisible(false);
	mButtonTopup->registerTouch(true);
	mButtonTopup->setOnTouch(CC_CALLBACK_0(GameScene::showBoxTopUp, this));
	mButtonTopup->setZOrder(7);

	mBoxInfoGame = BoxInfoGame::createBoxInfo();
	this->addChild(mBoxInfoGame);
	mBoxInfoGame->closeBox();



	mBoxTopUp = BoxTopUp::create(CC_CALLBACK_0(GameScene::showTopUpByCard, this), CC_CALLBACK_1(GameScene::rechargeBySMS, this));
	mBoxTopUp->close();
	this->addChild(mBoxTopUp);
	mBoxTopUp->setFuncShowNotice(CC_CALLBACK_1(GameScene::showNotice, this));
	mBoxTopUp->setFuncShowLoading(CC_CALLBACK_1(GameScene::showLoading, this));
	mBoxTopUp->setFuncGetSmsInfo(CC_CALLBACK_0(GameScene::getSmsInfo, this));
	mBoxTopUp->setFuncRecharge(CC_CALLBACK_3(GameScene::recharge_clone, this));

	mBoxTopUpCard = BoxTopUpCard::create(CC_CALLBACK_4(GameScene::recharge, this));
	mBoxTopUpCard->setVisible(false);
	this->addChild(mBoxTopUpCard);

	mMainLoading = Loading::create();
	mMainLoading->setPosition(Vec2(_size.width / 2, _size.height / 2));
	this->addChild(mMainLoading);
	mMainLoading->hide();
	schedule(schedule_selector(GameScene::updateGame));
	/*changeTypeRoom(roomInfo->pTypeRoom, roomInfo->pBetMoney);
	refresh();*/
	if (roomInfo->mGameID == 2){
		schedule(schedule_selector(GameScene::updateForOffLineGame));
		refresh();
	}
	else if (roomInfo->mGameID == 0){
		refresh();
	}
	if (roomInfo->mGameID == 0){
		MpMessage* message = new MpMessage((uint32_t)MP_MSG_GET_SCRATCH_CARD_INFO);
		message->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
		MpClientManager::getInstance(0)->sendMessage(message);
		MpClientManager::getInstance(GameInfo::mUserInfo.mGameID + 1);
	}

}

void GameScene::onSendText(std::string pContent) {
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	MpChatMessageRequest * request = new MpChatMessageRequest();
	request->setContent(pContent);
	request->setUsername(GameInfo::mUserInfo.mFullname);
	request->setTokenId(GameInfo::mUserInfo.mTokenID);
	request->addInt(MP_PARAM_TABLE_ID, pInfoRoom->mTableID);
	MpClientManager::getInstance()->sendMessage(request);
}

void GameScene::chooseXMoney(int index){
	roomStruct * pInfo = (roomStruct*)mInfoRoom;
	ClientPlayerInfoEX * myseft = getInfoMyselft();
	mBoxXMoney[(myseft->position)]->closeBox();
	if (myseft){
		BoxResize* box = mBoxXMoney[(myseft->position)];
		for (size_t j = 0; j < pInfo->mListXFactor.size(); j++){
			if (j == index){
				box->visibleObject(pInfo->mListXFactor[j], false);
			}
			else{
				box->visibleObject(pInfo->mListXFactor[j], true);
			}

		}
		Sprite* buttonXMoney = (Sprite*)box->getParent();
		((Sprite*)buttonXMoney->getChildByTag(10))->setTexture(__String::createWithFormat("rate_battle_label%d.png", pInfo->mListXFactor[index])->getCString());
		myseft->indexX = pInfo->mListXFactor[index];
		sendXFactor(pInfo->mListXFactor[index]);
	}
}
void GameScene::sendXFactor(int index){
	mLoadDingWaitFish->setVisible(true);
	mLoadDingWaitFish->resumeSchedulerAndActions();
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	MpMessageRequest * request = new MpMessageRequest(MP_MSG_CHANGE_XFACTOR);
	request->setTokenId(GameInfo::mUserInfo.mTokenID);
	request->addInt(MP_PARAM_TABLE_ID, pInfoRoom->mTableID);
	request->addInt(MP_PARAM_XFACTOR, index);
	MpClientManager::getInstance()->sendMessage(request);
}

void GameScene::onSendEmotion(std::string pEmotionType, int pEmotionID){
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	MpChatEmotionMessageRequest * request = new MpChatEmotionMessageRequest();
	request->setEmotionType(pEmotionType);
	request->setEmotionID(pEmotionID);
	request->setUsername(GameInfo::mUserInfo.mFullname);
	request->setTokenId(GameInfo::mUserInfo.mTokenID);
	request->addInt(MP_PARAM_TABLE_ID, pInfoRoom->mTableID);
	MpClientManager::getInstance()->sendMessage(request);
}

void GameScene::giftOff(Ref* pObject){
	EGSprite* pBox = (EGSprite*)pObject;
	ClientPlayerInfoEX* myselft = getInfoMyselft();
	if (myselft){
		myselft->balance += *(int*)pBox->getUserData();
	}

	gift(pBox->getPosition(), Vec2(CLIENT_WIDTH / 2, 0), *(int*)pBox->getUserData(), false);
	pBox->performRelease();
}
void GameScene::pauseGameOff(){
	unschedule(schedule_selector(GameScene::updateForOffLineGame));
	CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
	//WAudioControl::getInstance()->pauseAll();
}
void GameScene::resumeGameOff(){
	schedule(schedule_selector(GameScene::updateForOffLineGame));
	mDelayOffline = 0;
	CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
	//WAudioControl::getInstance()->resumeAll();
}
void GameScene::pauseAllGame(){
	pauseSchedulerAndActions();
	mMainLayer->pauseSchedulerAndActions();
	for (int i = 0; i < mListFish.size(); i++){
		Fish* pFish = mListFish.at(i);
		pauseSprite(pFish);
	}
	registerBackPressed();
}
void GameScene::pauseSprite(Node* pNode){
	pNode->pauseSchedulerAndActions();
	if (pNode->getChildrenCount() >0){

		for (int i = 0; i < pNode->getChildrenCount(); i++){
			pauseSprite(pNode->getChildren().at(i));
		}
	}
}
void GameScene::resumeSprite(Node* pNode){
	pNode->resumeSchedulerAndActions();
	if (pNode->getChildrenCount() >0){

		for (int i = 0; i < pNode->getChildrenCount(); i++){
			resumeSprite(pNode->getChildren().at(i));
		}
	}
}
void GameScene::resumeAllGame(){
	resumeSchedulerAndActions();
	mMainLayer->resumeSchedulerAndActions();
	for (int i = 0; i < mListFish.size(); i++){
		Fish* pFish = mListFish.at(i);
		resumeSprite(pFish);
	}
	CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
	//WAudioControl::getInstance()->resumeAll();
}
void GameScene::showBoxQuest(){
	mBoxQuest->setVisible(true);
	Sprite* boxQuest = (Sprite*)mBoxQuest->getChildByTag(200);
	boxQuest->setVisible(true);
	Label* labelQuantity = (Label*)boxQuest->getChildByTag(100);
	labelQuantity->setString(__String::createWithFormat("%d", mCurrentTask)->getCString());
	updateTaskBox();
	pauseAllGame();
}
void GameScene::closeBoxQuest(){
	mBoxQuest->setVisible(false);
	mBoxQuest->getChildByTag(200)->setVisible(false);
	resumeAllGame();
}
bool GameScene::checkTaskGame(int id, int pType, int quantity){
	roomStruct* pInfo = (roomStruct*)mInfoRoom;
	if (mTaskGame&& pInfo->mGameID == 2){
		bool isComeplete = true;
		Vector<TaskTarget*>* mArrayTask = mTaskGame->getArrayTask();
		for (int i = 0; i < mArrayTask->size(); i++){
			TaskTarget* task = mArrayTask->at(i);
			if (task->getType() == pType && task->getID() == id){
				task->setCurrentQuantity(task->getCurrentQuantity() + quantity);
			}
			if (task->getCurrentQuantity() < task->getQuantity()){
				isComeplete = false;
			}
		}
		if (isComeplete){
			completeQuest();
			mCurrentTask++;
			UserDefault::getInstance()->setIntegerForKey(KEY_CURRENT_TASK, mCurrentTask);
			for (int i = 0; i < mArrayTask->size(); i++){
				TaskTarget* task = mArrayTask->at(i);
				mArrayTask->eraseObject(task);
				delete task;
			}
			delete mArrayTask;
			delete mTaskGame;
			mTaskGame = NULL;
			mTaskGame = TaskGame::getTask(mCurrentTask);

		}
		return isComeplete;
	}
	return false;
}
void GameScene::updateTaskBox(){
	if (mTaskGame){
		WXmlReader *xml = WXmlReader::create();
		xml->load(KEY_XML_FILE);
		float pHeight = 0;
		Vector<TaskTarget*>* mArrayTask = mTaskGame->getArrayTask();
		for (int i = 0; i < mArrayTask->size(); i++){
			Label* pLabelQuest = (Label*)mBoxQuest->getChildByTag(i * 3 + 1);
			TaskTarget* task = mArrayTask->at(i);
			if (!pLabelQuest){
				pLabelQuest = Label::createWithBMFont("font-arial-shop.fnt", "");
				mBoxQuest->addChild(pLabelQuest);
			}
			if (task->getType() == 1){
				pLabelQuest->setString(xml->getNodeTextByTagName("label-catch"));
			}
			else{
				pLabelQuest->setString(xml->getNodeTextByTagName("label-use"));
			}
			pLabelQuest->setTag(i * 3 + 1);
			pLabelQuest->setAlignment(kCCTextAlignmentLeft);
			pLabelQuest->setAnchorPoint(Vec2(0, 1));
			pLabelQuest->setPosition(Vec2(CLIENT_WIDTH / 2 - 190, CLIENT_HEIGHT / 2 + 40 - pHeight));
			pHeight += pLabelQuest->getContentSize().height + 15;

			Sprite* pIcon = (Sprite*)mBoxQuest->getChildByTag(i * 3 + 2);
			if (!pIcon){
				pIcon = Sprite::create();
				pLabelQuest->getParent()->addChild(pIcon);
			}
			else{
				pIcon->removeAllChildrenWithCleanup(true);
			}
			if (task->getType() == 1){
				pIcon->setTexture((__String::createWithFormat(IMG_ICON_FISH, task->getID())->getCString()));
			}
			else if (task->getType() == 2){
				pIcon->setTexture(nullptr);
				pIcon->setTextureRect(Rect::ZERO);
				Label* labelInfo = Label::createWithBMFont("font-arial-shop.fnt", "");
				pIcon->addChild(labelInfo);
				labelInfo->setString(xml->getNodeTextByTagName(__String::createWithFormat("label-item-%d", task->getID())->getCString()));
				labelInfo->setAnchorPoint(Vec2(0, 0.5f));
			}
			else if (task->getType() == 3){
				pIcon->setTexture(nullptr);
				pIcon->setTextureRect(Rect::ZERO);
				Label* labelInfo = Label::createWithBMFont("font-arial-shop.fnt", "");
				pIcon->addChild(labelInfo);
				labelInfo->setString(xml->getNodeTextByTagName("label-laser"));
				labelInfo->setAnchorPoint(Vec2(0, 0.5f));
			}
			else if (task->getType() == 4){
				pIcon->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName("icon-coin.png"));
			}
			pIcon->setTag(i * 3 + 2);
			pIcon->setPosition(Vec2(pLabelQuest->getPositionX() + pLabelQuest->getContentSize().width + pIcon->getContentSize().height, pLabelQuest->getPositionY() - pLabelQuest->getContentSize().height / 2));

			Label* labelCount = (Label*)mBoxQuest->getChildByTag(i * 3 + 3);
			if (!labelCount){
				labelCount = Label::createWithBMFont("font-arial-shop.fnt", "10/10");
				pLabelQuest->getParent()->addChild(labelCount);
			}
			labelCount->setTag(i * 3 + 3);
			labelCount->setPosition(Vec2(CLIENT_WIDTH / 2 + 60, pLabelQuest->getPositionY() - pLabelQuest->getContentSize().height / 2));
			labelCount->setString(__String::createWithFormat("%d/%d", task->getCurrentQuantity(), task->getQuantity())->getCString());

			Label* labelBonus = (Label*)mBoxQuest->getChildByTag(100);
			if (!labelBonus){
				labelBonus = Label::createWithBMFont("font-arial-shop.fnt", "Thuong : 3 coin");
				pLabelQuest->getParent()->addChild(labelBonus);
			}
			labelBonus->setTag(100);
			labelBonus->setPosition(Vec2(CLIENT_WIDTH / 2 + 60, CLIENT_HEIGHT / 2 - 100));
			std::string pStringBonus = xml->getNodeTextByTagName("label-bonus") + __String::createWithFormat("%d", mTaskGame->getBonus())->getCString();
			labelBonus->setString(pStringBonus.c_str());

			Sprite* pSpriteBonus = (Sprite*)mBoxQuest->getChildByTag(101);
			if (!pSpriteBonus){
				pSpriteBonus = Sprite::create();
				pLabelQuest->getParent()->addChild(pSpriteBonus);
			}
			if (mTaskGame->isCoin()){
				pSpriteBonus->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName("icon-coin.png"));
			}
			else{
				pSpriteBonus->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName("icon-so.png"));
			}
			pSpriteBonus->setTag(101);
			pSpriteBonus->setScale(0.8f);
			pSpriteBonus->setPosition(Vec2(labelBonus->getPositionX() + labelBonus->getContentSize().width / 2 + pSpriteBonus->getContentSize().width / 2, labelBonus->getPositionY() - 3));
		}
	}
}
void GameScene::startEventShark(){
	EGSprite* boxShark = (EGSprite*)mBoxEvenShark->getChildByTag(1);
	for (int i = 0; i < 4; i++){
		EGSprite* pTreasure = (EGSprite*)boxShark->getChildByTag(i + 1);
		pTreasure->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_TREASURE, 0)->getCString()));
		pTreasure->setScale(1);
	}
	boxShark->runAction(Sequence::create(DelayTime::create(1), Repeat::create(Sequence::create(DelayTime::create(0.2f), CallFunc::create(CC_CALLBACK_0(GameScene::mixTreasure, this)), NULL), 25), CallFunc::create(CC_CALLBACK_0(GameScene::startChooseTreasure, this)), NULL));
}
void GameScene::startChooseTreasure(){
	EGSprite* boxShark = (EGSprite*)mBoxEvenShark->getChildByTag(1);
	for (int i = 0; i < 4; i++){
		EGSprite* pTreasure = (EGSprite*)boxShark->getChildByTag(i + 1);
		pTreasure->registerTouch(true);
	}

}
void GameScene::mixTreasure(){
	EGSprite* boxShark = (EGSprite*)mBoxEvenShark->getChildByTag(1);
	for (int i = 0; i < 4; i++){
		for (int j = i; j < 3; j++){
			if (rand() % 2 == 1){
				EGSprite* pTreaure = (EGSprite*)boxShark->getChildByTag(i + 1);
				((EGSprite*)boxShark->getChildByTag(i + 2))->setTag(i + 1);
				pTreaure->setTag(i + 2);
			}
		}
	}
	for (int i = 0; i < 4; i++){
		EGSprite* pTreasure = (EGSprite*)boxShark->getChildByTag(i + 1);
		pTreasure->stopAllActions();
		pTreasure->runAction(MoveTo::create(0.2f, Vec2(130 + (i / 2) * 150, 130 + (i % 2) * 140)));
	}
}
void GameScene::chooseTreasure(EGSprite* button){
	EGSprite* boxShark = (EGSprite*)mBoxEvenShark->getChildByTag(1);
	EGSprite* pGiftText = EGSprite::createWithSpriteFrameName(__String::createWithFormat(IMG_GIFT_SHARK, 1)->getCString());
	if (button->getUserData()){
		pGiftText->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_GIFT_SHARK, 2)->getCString()));
		if (getInfoMyselft()){
			getInfoMyselft()->balance += 600;
		}
	}
	else{
		if (getInfoMyselft()){
			getInfoMyselft()->balance += 200;
		}
	}
	mBoxEvenShark->addChild(pGiftText);
	pGiftText->setOpacity(155);
	pGiftText->runAction(FadeTo::create(0.5f, 255));
	pGiftText->runAction(Sequence::create(MoveBy::create(2, Vec2(0, 50)), CallFunc::create(CC_CALLBACK_0(EGSprite::performRelease, pGiftText)), NULL));
	pGiftText->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	for (int i = 0; i < 4; i++){
		EGSprite* pTreasure = (EGSprite*)boxShark->getChildByTag(i + 1);
		pTreasure->unregisterTouch();
		if (pTreasure->getUserData()){
			pTreasure->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_TREASURE, 2)->getCString()));
		}
		else{
			pTreasure->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_TREASURE, 1)->getCString()));
		}
		pTreasure->setScale(0.7f);
	}
	mBoxEvenShark->runAction(Sequence::create(DelayTime::create(3), CallFunc::create(CC_CALLBACK_0(GameScene::closeBoxShark, this)), NULL));
	refresh();
}
void GameScene::startEventMaiden(){
	EGSprite* boxMaiden = (EGSprite*)mBoxEvenMaiDen->getChildByTag(1);
	Sprite* circle = (Sprite*)boxMaiden->getChildByTag(1);
	boxMaiden->getChildByTag(2)->setVisible(false);
	float pRotation = rand() % 360;
	circle->setRotation(0);
	circle->runAction(Sequence::create(EaseOut::create(RotateBy::create(8, 360 * 12 + pRotation), 1), CallFunc::create(CC_CALLBACK_0(GameScene::checkResult, this, pRotation)), NULL));
}
void GameScene::showBoxMaiDen(){
	if (!mBoxEvenShark->isVisible()){
		if (!mBoxEvenMaiDen->isVisible()){
			playSound(SOUND_CATCH_MAIDEN);
			mBoxEvenMaiDen->setVisible(true);
			EGSprite* boxMaiden = (EGSprite*)mBoxEvenMaiDen->getChildByTag(1);
			boxMaiden->setVisible(true);
			boxMaiden->getChildByTag(2)->setVisible(true);
			pauseAllGame();
		}
	}
	else {
		mBoxEvenMaiDen->setUserData(new int());
	}
}
void GameScene::closeBoxMaiDen(){
	mBoxEvenMaiDen->setVisible(false);
	EGSprite* boxMaiden = (EGSprite*)mBoxEvenMaiDen->getChildByTag(1);
	boxMaiden->setVisible(false);
	resumeAllGame();
	if (mBoxEvenShark->getUserData()){
		void* pData = mBoxEvenShark->getUserData();
		delete pData;
		mBoxEvenShark->setUserData(NULL);
		showBoxShark();
	}
}
void GameScene::showBoxShark(){
	if (!mBoxEvenMaiDen->isVisible()){
		if (!mBoxEvenShark->isVisible()){
			//playSound(SOUND_TREASURE);
			mBoxEvenShark->setVisible(true);
			EGSprite* boxMaiden = (EGSprite*)mBoxEvenShark->getChildByTag(1);
			boxMaiden->setVisible(true);
			mBoxEvenShark->runAction(Sequence::create(DelayTime::create(2), CallFunc::create(CC_CALLBACK_0(GameScene::startEventShark, this)), NULL));
			pauseAllGame();
		}
	}
	else{
		mBoxEvenShark->setUserData(new int());
	}
}
void GameScene::closeBoxShark(){
	mBoxEvenShark->setVisible(false);
	EGSprite* boxMaiden = (EGSprite*)mBoxEvenShark->getChildByTag(1);
	boxMaiden->setVisible(false);
	resumeAllGame();
	if (mBoxEvenMaiDen->getUserData()){
		void* pData = mBoxEvenMaiDen->getUserData();
		delete pData;
		mBoxEvenMaiDen->setUserData(NULL);
		showBoxMaiDen();
	}
}
void GameScene::checkResult(float pRotation){
	float pTotal = 0;
	for (int i = 0; i < 6; i++){
		pTotal += LIST_GIFT[i].x;
		if ((360 - pRotation) < pTotal){
			showResultMaiden(i);
			if (getInfoMyselft()){
				getInfoMyselft()->balance += LIST_GIFT[i].y;
			}
			refresh();
			break;
		}
	}
}
void GameScene::showResultMaiden(int pResult){
	EGSprite* pGiftSprite = EGSprite::createWithSpriteFrameName(__String::createWithFormat(IMG_GIFT_MAIDEN, pResult + 1)->getCString());
	mBoxEvenMaiDen->addChild(pGiftSprite);
	pGiftSprite->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	pGiftSprite->runAction(Sequence::create(MoveBy::create(3, Vec2(0, 50)), CallFunc::create(CC_CALLBACK_0(EGSprite::performRelease, pGiftSprite)), CallFunc::create(CC_CALLBACK_0(GameScene::closeBoxMaiDen, this)), NULL));
}



void GameScene::chooseTabShop(int index){
	bool isFake = false;
	if (index == -1) {
		index = 0;
		isFake = true;
	}
	updateMoneyShop();
	Sprite* boxShop = (Sprite*)mBoxShop->getChildByTag(1);
	for (int i = 0; i < 3; i++){
		LayerColor* layer = (LayerColor*)boxShop->getChildByTag(i + 1);
		layer->setVisible(false);
	}
	LayerColor* layerShop = (LayerColor*)boxShop->getChildByTag(index + 1);
	layerShop->setVisible(true);
	for (int i = 0; i < 3; i++){
		EGSprite* buttonTab = (EGSprite*)mBoxShop->getChildByTag(i + 2);
		buttonTab->setPosition(Vec2(CLIENT_WIDTH / 2 - buttonTab->getContentSize().width + i* buttonTab->getContentSize().width, boxShop->getPositionY() + boxShop->getContentSize().height / 2 + 10));
		buttonTab->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_BUTTON_TAB_SHOP_OFF, i + 1, 1)->getCString()));
	}
	EGSprite* pButtonTab = (EGSprite*)mBoxShop->getChildByTag(index + 2);
	pButtonTab->setPositionY(pButtonTab->getPositionY() + 10);
	pButtonTab->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_BUTTON_TAB_SHOP_OFF, index + 1, 2)->getCString()));
	mIndexTabShop = index;
	if (index == 0){
		if (!isFake)
			chooseItemShop(0);
		for (int i = 0; i < 9; i++){
			EGButtonSprite* buttonTab = (EGButtonSprite*)layerShop->getChildByTag(i + 1);
			buttonTab->unregisterTouch();
			buttonTab->setOpacity(255);
			if (i == 8){
				int pUpgradeDame = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_DAME_GUN, 8)->getCString(), 0);
				int pUpgradeSpeed = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_SPEED_GUN, 8)->getCString(), 0);
				if (pUpgradeDame >0 || pUpgradeSpeed > 0){
					buttonTab->registerTouch(true);
				}
				else{
					buttonTab->setOpacity(155);
				}
			}
			else{
				buttonTab->registerTouch(true);
			}
		}
	}
	else if (index == 1){
		chooseItemShop(0);
	}
	else{
		for (int i = mListFishDead->size() - 1; i >= 0; i--){
			Vec2* pInfo = mListFishDead->at(i);
			int pCount = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_DIARY_FISH, (int)pInfo->x)->getCString(), 0);
			UserDefault::getInstance()->setIntegerForKey(__String::createWithFormat(KEY_DIARY_FISH, (int)pInfo->x)->getCString(), pCount + pInfo->y);
			mListFishDead->pop_back();
			delete pInfo;
		}
		for (int i = 0; i < 20; i++){
			int pCount = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_DIARY_FISH, i + 1)->getCString(), 0);
			Label* label = (Label*)layerShop->getChildByTag(i + 1);
			label->setString(__String::createWithFormat("%d", pCount)->getCString());
		}
	}
}
void GameScene::chooseItemShop(int index){
	Sprite* boxShop = (Sprite*)mBoxShop->getChildByTag(1);
	LayerColor* layerShop = (LayerColor*)boxShop->getChildByTag(mIndexTabShop + 1);
	WXmlReader * xml = WXmlReader::create();
	mItemChoosed = index;
	if (mIndexTabShop == 0){
		Sprite* pItem = (Sprite*)layerShop->getChildByTag(11);
		pItem->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat("gun-off (%d).png", index + 1)->getCString()));
		Label* label = (Label*)layerShop->getChildByTag(12);
		int pDefault = 1;
		if (index > 6){
			pDefault = 0;
		}
		int pUpgradeDame = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_DAME_GUN, index + 1)->getCString(), pDefault);
		int pUpgradeSpeed = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_SPEED_GUN, index + 1)->getCString(), pDefault);
		std::string pLabelDame = xml->getNodeTextByTagName("label-dame-shop") + ": " + __String::createWithFormat("%d/%d", pUpgradeDame, 20)->getCString();
		label->setString(pLabelDame);
		Label* labelSpeed = (Label*)layerShop->getChildByTag(14);
		std::string pLabelSpeed = xml->getNodeTextByTagName("label-speed-shop") + ": " + __String::createWithFormat("%d/%d", pUpgradeSpeed, 20)->getCString();
		labelSpeed->setString(pLabelSpeed);
		Label* labelPriceUpgradeDAme = (Label*)layerShop->getChildByTag(13);
		int pPriceFirst = 300 + index * 300;
		std::string pStringUpdagradeDame = __String::createWithFormat("%d ", pPriceFirst + pPriceFirst*(pUpgradeDame - 1) / 2)->getCString() + xml->getNodeTextByTagName("label-upgrade-shop");
		labelPriceUpgradeDAme->setString(pStringUpdagradeDame);
		Label* labelPriceUpgradeSpeed = (Label*)layerShop->getChildByTag(15);
		std::string pStringUpdagradeSpeed = __String::createWithFormat("%d ", pPriceFirst + pPriceFirst*(pUpgradeSpeed - 1) / 2)->getCString() + xml->getNodeTextByTagName("label-upgrade-shop");
		labelPriceUpgradeSpeed->setString(pStringUpdagradeSpeed);
	}
	else if (mIndexTabShop == 1){
		Sprite* pItem = (Sprite*)layerShop->getChildByTag(11);
		pItem->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_ITEM_SP_SHOP, index + 1)->getCString()));
		int pDefault = 3;
		if (index == 3){
			pDefault = 0;
		}
		int pCountItem = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_ITEM_SP, index + 1)->getCString(), pDefault);
		Label* label = (Label*)layerShop->getChildByTag(12);
		std::string pLabelDame = xml->getNodeTextByTagName(__String::createWithFormat("description-item%d", index + 1)->getCString());
		label->setScale(0.9f);
		if (index < 3){
			pLabelDame += __String::createWithFormat("%d", pCountItem)->getCString();
		}
		else{
			pLabelDame += __String::createWithFormat("%d", 100 + pCountItem * 50)->getCString();
		}
		label->setString(pLabelDame);
		Label* labelPrice = (Label*)layerShop->getChildByTag(13);
		int pPrice[] = { 20, 8, 8, 10 };
		std::string pStringUpdagradeDame = "Mua";
		pStringUpdagradeDame = __String::createWithFormat("%d ", pPrice[index])->getCString() + pStringUpdagradeDame;
		labelPrice->setString(pStringUpdagradeDame);
	}
}
void GameScene::upgradeGun(int pTypeUpgrade){
	if (mIndexTabShop == 0){
		ClientPlayerInfoEX* myselft = getInfoMyselft();
		if (myselft){
			uint32_t pMoney = myselft->balance;
			int pDefault = 1;
			if (mItemChoosed > 6){
				pDefault = 0;
			}
			int pUpgradeDame = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_DAME_GUN, mItemChoosed + 1)->getCString(), pDefault);
			int pUpgradeSpeed = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_SPEED_GUN, mItemChoosed + 1)->getCString(), pDefault);
			if (pTypeUpgrade == 0){
				if (pUpgradeDame == 20)
					return;
				int pPriceFirst = 300 + mItemChoosed * 300;
				if (pMoney >= pPriceFirst + (pUpgradeDame - 1)*pPriceFirst / 2){
					UserDefault::getInstance()->setIntegerForKey(__String::createWithFormat(KEY_DAME_GUN, mItemChoosed + 1)->getCString(), pUpgradeDame + 1);
					myselft->balance -= pPriceFirst + (pUpgradeDame - 1)*pPriceFirst / 2;
					UserDefault::getInstance()->setStringForKey(KEY_MONEY, __String::createWithFormat("%d", myselft->balance)->getCString());
					checkTaskGame(1, 4, pPriceFirst + (pUpgradeDame - 1)*pPriceFirst / 2);
				}
				else {
					showBoxTopUp();
				}
			}
			else{
				if (pUpgradeSpeed == 20)
					return;
				int pPriceFirst = 300 + mItemChoosed * 300;
				if (pMoney >= pPriceFirst + (pUpgradeSpeed - 1)*pPriceFirst / 2) {
					UserDefault::getInstance()->setIntegerForKey(__String::createWithFormat(KEY_SPEED_GUN, mItemChoosed + 1)->getCString(), pUpgradeSpeed + 1);
					checkTaskGame(1, 4, pPriceFirst + (pUpgradeSpeed - 1)*pPriceFirst / 2);
					myselft->balance -= pPriceFirst + (pUpgradeSpeed - 1)*pPriceFirst / 2;
					UserDefault::getInstance()->setStringForKey(KEY_MONEY, __String::createWithFormat("%d", myselft->balance)->getCString());
				}
				else {
					showBoxTopUp();
				}
			}
		}
		chooseTabShop(-1);
		chooseItemShop(mItemChoosed);
	}
	else{
		int pPrice[] = { 20, 8, 8, 10 };
		if (mCountSo >= pPrice[mItemChoosed]){
			int pCountItem = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_ITEM_SP, mItemChoosed + 1)->getCString(), 3);
			UserDefault::getInstance()->setIntegerForKey(__String::createWithFormat(KEY_ITEM_SP, mItemChoosed + 1)->getCString(), pCountItem + 1);
			mCountSo -= pPrice[mItemChoosed];
			refreshItemSp();
			chooseItemShop(mItemChoosed);
		}
		else {
			showTopUpSo();
		}
	}
	updateMoneyShop();
}

void GameScene::growMoney(int pMoney){
	if (pMoney >= 0){
		ClientPlayerInfoEX* myselft = getInfoMyselft();
		if (myselft){
			myselft->balance += pMoney;
		}
		mCurrentExp += pMoney;
		mCurrentPower += pMoney;
		if (mCurrentPower > 5000){
			playSound(SOUND_MAX_POWER);
			mElectric->setVisible(true);
			mElectric->runAction(Show::create());
			mElectric->runAction(Sequence::createWithTwoActions(DelayTime::create(1), CallFunc::create(CC_CALLBACK_0(Sprite::setUserData, mElectric, new int()))));
			mListGun.at(0)->active(false);
			mListGun.at(0)->active(true);
			mCurrentPower = 0;
		}
		barPower->setPercentage((float)mCurrentPower * 100.0f / 5000.0f);
		if (mCurrentExp >= mLimitExp){
			mLevel += 1;
			UserDefault::sharedUserDefault()->setIntegerForKey(KEY_LEVEL, mLevel);
			mCurrentExp = 0;
			numStage->setNumber(mLevel);
			if (mLevel > 9){
				numStage->setPosition(Vec2(numStage->getParent()->getContentSize().width / 2 + 20 - numStage->getContentSize().width / 2, numStage->getParent()->getContentSize().height / 2));
			}
			else{
				numStage->setPosition(Vec2(numStage->getParent()->getContentSize().width / 2, numStage->getParent()->getContentSize().height / 2));
			}
			showQuaMan();
			mLimitExp = 0;
			for (int i = 0; i < mLevel; i++){
				mLimitExp += 200 * (i + 1);
			}
			mCountSo += 3;
			UserDefault::getInstance()->setIntegerForKey(KEY_SO, mCountSo);
			/*RISESprite *pCoin = RISESprite::createWithSpriteFrameName(ICON_XO);
			CCArray* pArrayAnimation = CCArray::create();
			mFlower = CCParticleFlower::create();
			mFlower->setEmissionRate(20);
			mFlower->setStartColor(ccc4f(1, 1, 0, 1));
			mFlower->setStartColorVar(ccc4f(1, 1, 0, 1));
			mFlower->setEndColor(ccc4f(1, 1, 0, 1));
			mFlower->setEndColorVar(ccc4f(1, 1, 0, 1));
			mFlower->setLifeVar(0.5f);
			mFlower->setLife(0.5f);
			mFlower->setTexture(CCTextureCache::sharedTextureCache()->addImage(STAR));
			this->addChild(mFlower);
			Point pDestiny = ccp(790, 10);
			float pDuration = ccpDistance(ccp(400, 240), pDestiny) / 200.0f;
			mFlower->runAction(Sequence::create(MoveTo::create(pDuration, pDestiny), DelayTime::create(0.5f), Hide::create(), CallFunc::create(mFlower, callfunc_selector(CCParticleFlower::removeFromParent)), CallFunc::create(this, callfunc_selector(GameScene::loadInfoGame)), CallFunc::create(mFlower, callfunc_selector(CCParticleFlower::cleanup)), NULL));
			mFlower->setStartColor(ccc4f(0, 0, 0, 1));
			mFlower->setPosition(ccp(400, 240));
			mFlower->setZOrder(1);
			pCoin->runAction(RepeatForever::create(Animate::create(CCAnimation::createWithSpriteFrames(pArrayAnimation, 0.1f))));
			pCoin->runAction(Sequence::create(MoveTo::create(pDuration, pDestiny), CallFunc::create(pCoin, callfunc_selector(RISESprite::detachSelf)), CCCallFuncO::create(mMoneySprite, callfuncO_selector(NumberSprite::countTo), CCFloat::create(mMoney)), CCCallFuncO::create(this, callfuncO_selector(GameScene::playSound), __String::create(SOUND_COIN)), NULL));
			this->addChild(pCoin);
			pCoin->setPosition(ccp(400, 240));
			pCoin->setScale(0.5f + (rand() % 2)* 0.1f);
			pCoin->setZOrder(1);*/
		}

		barStage->setPercentage(mCurrentExp * 100 / mLimitExp);
	}
	refresh();
}
void GameScene::completeQuest(){
	Size _size = Director::sharedDirector()->getWinSize();
	//playSound(SOUND_TREASURE);
	EGSprite *pComplete = EGSprite::createWithSpriteFrameName("complete-task.png");
	mMainLayer->addChild(pComplete);
	pComplete->setZOrder(10);
	pComplete->setPosition(ccp(_size.width / 2, _size.height - pComplete->getContentSize().height / 2));
	Sprite* pIconBonus = Sprite::createWithSpriteFrameName("icon-so.png");
	if (mTaskGame->isCoin()){
		pIconBonus->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat("icon-coin.png", 1)->getCString()));
		if (getInfoMyselft()){
			getInfoMyselft()->balance += mTaskGame->getBonus();
		}
	}
	else{
		mCountSo += mTaskGame->getBonus();
		UserDefault::getInstance()->setIntegerForKey(KEY_SO, mCountSo);
	}
	pComplete->addChild(pIconBonus);
	pIconBonus->setScale(0.6f);
	pIconBonus->setPosition(ccp(260, 58));
	Label* label = Label::createWithBMFont(FONT_ARIAL_FNT, FONT_ARIAL_FNT);
	label->setString(__String::createWithFormat("x %d", mTaskGame->getBonus())->getCString());
	label->setAnchorPoint(ccp(0, 0.5f));
	label->setColor(ccc3(0, 0, 0));
	label->setPosition(ccp(pIconBonus->getPositionX() + pIconBonus->getContentSize().height*0.9f / 2, pIconBonus->getPositionY()));
	pComplete->addChild(label);
	pComplete->runAction(Sequence::create(MoveTo::create(1, ccp(_size.width / 2, _size.height - 100)), DelayTime::create(2), MoveTo::create(1.5f, ccp(_size.width / 2, _size.height + pComplete->getContentSize().height / 2)), CallFunc::create(pComplete, callfunc_selector(EGSprite::performRelease)), NULL));

}
void GameScene::showQuaMan(){
	Size _size = Director::sharedDirector()->getWinSize();
	EGSprite* pTextQuaMan = EGSprite::createWithSpriteFrameName(IMG_LEVEL_COMPLETE);
	pTextQuaMan->setPosition(ccp(_size.width / 2, _size.height / 2));
	pTextQuaMan->setZOrder(9);
	mMainLayer->addChild(pTextQuaMan);
	pTextQuaMan->runAction(MoveBy::create(4.5f, ccp(0, 200)));
	pTextQuaMan->runAction(Sequence::create(DelayTime::create(4), FadeOut::create(0.5f), CallFunc::create(CC_CALLBACK_0(EGSprite::performRelease, pTextQuaMan)), NULL));
	Sprite* pGift = Sprite::createWithSpriteFrameName("icon-so.png");
	pTextQuaMan->addChild(pGift);
	pGift->setPosition(ccp(pTextQuaMan->getContentSize().width / 2 - 50, -pGift->getContentSize().height / 2));
	Sprite* num = Sprite::createWithSpriteFrameName(__String::createWithFormat(IMG_NUM_COMPLETE_LEVEL, 3)->getCString());
	pTextQuaMan->addChild(num);
	num->setPosition(ccp(pGift->getPositionX() + pGift->getContentSize().width / 2 + 10 + num->getContentSize().width / 2, pGift->getPositionY()));
}
void GameScene::checkChangeMap(){
	bool pExist = false;
	for (int i = mListFish.size() - 1; i >= 0; i--){
		Fish *_fish = (Fish*)mListFish.at(i);
		if (_fish->isVisible()){
			pExist = true;
		}
	}
	if (!pExist){
		if (isChangeMap){
			changeMap();
			isChangeMap = false;
		}
		if (isBonus){
			resumeGameOff();
			isBonus = false;
		}
	}
}
void GameScene::changeMap(){
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	Size _size = Director::sharedDirector()->getWinSize();
	pInfoRoom->mBackGroundID = 1 + rand() % 14;
	//mBackGround->setPercentage(99);
	mBackGround->runAction(Sequence::create(DelayTime::create(5), CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, mBackGround, true)), NULL));
	mBackGround = ProgressTimer::create(Sprite::create(__String::createWithFormat(IMG_BACK_GROUND_GAME, pInfoRoom->mBackGroundID)->getCString()));
	mBackGround->setPosition(ccp(_size.width / 2, _size.height / 2));
	EGSprite* pLine = EGSprite::createWithSpriteFrameName(__String::createWithFormat(IMG_LINE_CHANGEMAP, 1)->getCString());
	Sprite * pChildLine = Sprite::createWithSpriteFrameName(__String::createWithFormat(IMG_LINE_CHANGEMAP, 1)->getCString());
	Animation* pArrayAnimation = Animation::create();
	for (int i = 6; i >= 1; i--){
		pArrayAnimation->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_LINE_CHANGEMAP, i)->getCString()));
	}
	pArrayAnimation->setDelayPerUnit(0.2f);
	//pLine->addChild(pChildLine);
	pLine->setScaleY(2);
	pChildLine->setAnchorPoint(ccp(0, 0));
	pChildLine->setPosition(ccp(0, pLine->getContentSize().height));
	pLine->runAction(RepeatForever::create(Animate::create(pArrayAnimation)));
	pChildLine->runAction(RepeatForever::create(Animate::create(pArrayAnimation)));
	pLine->setAnchorPoint(ccp(0, 0));
	pLine->setPosition(ccp(_size.width - pLine->getContentSize().width / 2, 0));
	if (mCountChange >= 6){
		mCountChange = 0;
		pLine->runAction(Sequence::create(MoveTo::create(5, ccp(-pLine->getContentSize().width / 2, 0)), CallFunc::create(CC_CALLBACK_0(GameScene::bonusMap, this, 0)), CallFunc::create(CC_CALLBACK_0(EGSprite::performRelease, pLine)), NULL));
	}
	else{
		pLine->runAction(Sequence::create(MoveTo::create(5, ccp(-pLine->getContentSize().width / 2, 0)), CallFunc::create(CC_CALLBACK_0(GameScene::resumeGameOff, this)), CallFunc::create(CC_CALLBACK_0(EGSprite::performRelease, pLine)), NULL));
	}
	mMainLayer->addChild(mBackGround);
	mBackGround->setType(kCCProgressTimerTypeBar);
	mBackGround->setMidpoint(ccp(1, 0));
	mBackGround->setBarChangeRate(ccp(1, 0));
	mBackGround->setPercentage(0);
	mBackGround->runAction(ProgressTo::create(5, 100));
	pLine->setZOrder(6);
	mMainLayer->addChild(pLine);
	reloadMoneyOff();
}

void GameScene::updateMoneyShop(){
	Sprite* boxShop = (Sprite*)mBoxShop->getChildByTag(1);
	if (getInfoMyselft()){
		uint32_t pMoney = getInfoMyselft()->balance;
		for (int i = 0; i < 3; i++){
			LayerColor* layerShop = (LayerColor*)boxShop->getChildByTag(i + 1);
			Label* label = (Label*)layerShop->getChildByTag(100);
			if (i < 2){
				if (i == 0){
					label->setString(__String::createWithFormat("%d", pMoney)->getCString());
				}
				else{
					label->setString(__String::createWithFormat("%d", mCountSo)->getCString());
				}
			}
		}
	}
	updateMoneyOff(0);
}
void GameScene::updateMoneyOff(float pSec){
	mDeLayUpdateMoneyOff += pSec;
	ClientPlayerInfoEX* myself = getInfoMyselft();
	if (myself){
		uint32_t pMoney = myself->balance;
		roomStruct* pRoomInfo = (roomStruct*)mInfoRoom;
		if (mDeLayUpdateMoneyOff > 10){
			mDeLayUpdateMoneyOff = 0;
			if (pRoomInfo->mGameID == 2){
				/*UserDefault::getInstance()->setStringForKey(KEY_MONEY, __String::createWithFormat("%d", getPlayerByName("")->balance)->getCString());
				UserDefault::getInstance()->setIntegerForKey(KEY_SO, mCountSo);*/
				for (int i = mListFishDead->size() - 1; i >= 0; i--){
					Vec2* pInfo = mListFishDead->at(i);
					int pCount = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_DIARY_FISH, (int)pInfo->x)->getCString(), 0);
					UserDefault::getInstance()->setIntegerForKey(__String::createWithFormat(KEY_DIARY_FISH, (int)pInfo->x)->getCString(), pCount + pInfo->y);
					mListFishDead->pop_back();
					delete pInfo;
				}
			}
		}
		mLabelMoneyOff->setString(__String::createWithFormat("%d", pMoney)->getCString());
		mLabelSoOff->setString(__String::createWithFormat("%d", mCountSo)->getCString());
		if (pMoney < 100 + 50 * mCountRecoin){
			mDelayRecoin -= pSec;
			if (mDelayRecoin <= 0){
				mDelayRecoin = 10;
				myself->balance += 2;
			}
			mLabelCountReCoin->setString(__String::createWithFormat("%d", (int)mDelayRecoin)->getCString());
		}
	}
}
void GameScene::showBoxShop(){

	mBoxShop->setVisible(true);
	EGSprite* boxShop = (EGSprite*)mBoxShop->getChildByTag(1);
	boxShop->setVisible(true);
	//for (int i = 0; i < 3; i++){
	//	EGSprite* buttonTab = (EGSprite*) mBoxShop->getChildByTag(i + 2);
	//	buttonTab->setVisible(true);
	///*	buttonTab->registerTouch(true);*/
	//}
	chooseTabShop(0);
	pauseAllGame();
}
void GameScene::closeBoxShop(){
	mBoxShop->setVisible(false);
	EGSprite* boxShop = (EGSprite*)mBoxShop->getChildByTag(1);
	boxShop->setVisible(false);
	for (int i = 0; i < 3; i++){
		LayerColor* layer = (LayerColor*)boxShop->getChildByTag(i + 1);
		layer->setVisible(false);
	}
	resumeAllGame();
	//boxShop->setZOrder(2);
	//for (int i = 0; i < 3; i++){
	//	EGSprite* buttonTab = (EGSprite*)mBoxShop->getChildByTag(i + 2);
	//	buttonTab->setZOrder(1);
	//}

}
void GameScene::refreshItemSp(){

	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	pInfoRoom->mListItemSp.clear();
	for (int i = 0; i < 3; i++){
		EGSprite* buttonSuport = (EGSprite*)mMainLayer->getChildByTag(200 + i);
		Label* pNumb = (Label*)buttonSuport->getChildByTag(1);
		int pCountItem = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_ITEM_SP, i + 1)->getCString(), 3);
		pNumb->setString(__String::createWithFormat("%d", pCountItem)->getCString());

		ClientItemInfo infoItem;
		infoItem.id = 200 + i;
		infoItem.quantity = pCountItem;
		pInfoRoom->mListItemSp.push_back(infoItem);
	}
}
void GameScene::laser(float pRotation){
	if (mElectric->getUserData()){
		checkTaskGame(1, 3, 1);
		playSound(SOUND_LASER_SHOOT);
		void* pData = mElectric->getUserData();
		delete pData;
		mElectric->setUserData(NULL);
		mElectric->setVisible(false);
		mElectric->runAction(Hide::create());
		//mListGun.at(0)->active(false);
		mLaser = Node::create();
		float pHeight = 0;
		for (int i = 7; i >= 0; i--){
			int index = 7 - i;
			Sprite *_laser = Sprite::create();
			const char* pFrame = IMG_START_LASER;
			if (index != 0){
				pFrame = IMG_LASER;
			}
			_laser->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(pFrame, 1)->getCString()));
			_laser->setAnchorPoint(ccp(0.5f, 0));
			Animation* pArrayAnimation = Animation::create();
			for (int j = 1; j >= 0; j--){
				pArrayAnimation->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(pFrame, 2 - j)->getCString()));
			}
			pArrayAnimation->setDelayPerUnit(0);
			if (i == 0){
				_laser->runAction(Sequence::create(Animate::create(pArrayAnimation), DelayTime::create(0.5f), Hide::create(), CallFunc::create(CC_CALLBACK_0(GameScene::detachLaser, this)), NULL));
			}
			mLaser->addChild(_laser);

			_laser->setPosition(ccp(0, pHeight));
			pHeight += _laser->getContentSize().height - 1;
		}
		mLengtLaser = pHeight;
		mMainLayer->addChild(mLaser);
		mLaser->setPosition(Vec2(mListGun.at(0)->getPositionX(), 60));
		mLaser->setRotation(pRotation);
		mLaser->setZOrder(5);
	}
}
void GameScene::detachLaser(){

	mLaser->removeAllChildrenWithCleanup(true);
	mLaser->removeFromParentAndCleanup(true);
	mLaser = NULL;
}
void GameScene::checkLaser(Node* _laser){
	Vec2 pPointLaser = EGSupport::getPointInCircle(_laser->getPositionX(), _laser->getPositionY(), mLengtLaser / 2, _laser->getRotation());
	Rect _rectlaser = CCRectMake(pPointLaser.x - 20, pPointLaser.y - mLengtLaser / 2, 40, mLengtLaser);
	Rect _rectlaser1 = CCRectMake(pPointLaser.x - 10, pPointLaser.y - mLengtLaser / 2, 20, mLengtLaser);
	Rect _rectlaser2 = CCRectMake(pPointLaser.x - 5, pPointLaser.y - mLengtLaser / 2, 10, mLengtLaser);
	for (int i = mListFish.size() - 1; i >= 0; i--){
		Fish* fish = (Fish*)mListFish.at(i);
		Rect _rectfish = fish->rect();
		if (!fish->isDead() && fish->isVisible()){
			if (checkRectCollide(_rectfish, _rectlaser, fish->getRotation(), _laser->getRotation(), true)
				|| checkRectCollide(_rectfish, _rectlaser1, fish->getRotation(), _laser->getRotation(), true)
				|| checkRectCollide(_rectfish, _rectlaser2, fish->getRotation(), _laser->getRotation(), true)){
				fish->dead();
				gift(fish->getPosition(), Vec2(400, 0), fish->getObject()->getMoney(), false);
				checkTaskGame(fish->getType(), 1, 1);
				if (fish->getType() == 18 || fish->getType() == 17){
					showBoxShark();
				}
				else if (fish->getType() == 19 || fish->getType() == 20){
					showBoxMaiDen();
				}
				addFishDead(fish->getType());
			}
		}
	}
}
void GameScene::addFishDead(int pIndex){
	bool iExist = false;
	for (int i = 0; i < mListFishDead->size(); i++){
		Vec2* pInfoFish = mListFishDead->at(i);
		if (pIndex == pInfoFish->x){
			pInfoFish->y++;
			iExist = true;
		}
	}
	if (!iExist){
		mListFishDead->push_back(new Vec2(pIndex, 1));
	}
}


void GameScene::showBoxTopUp(){
	mButtonTopup->setVisible(false);
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	if (pInfoRoom->mGameID != 2 && pInfoRoom->mGameID != 1){
		bool state = GameInfo::mUserInfo.isCanEnableSms;
		if (state){
			//if (GameInfo::mUserInfo.mLstRechargeInfo.size() != 0) {
			//	mBoxTopUp->setInfo(GameInfo::mUserInfo.mLstRechargeInfo, GameInfo::mUserInfo.isCanRecharge);
			//	mBoxTopUp->setInfoVISA(GameInfo::mUserInfo.mLstRechageVISAInfo);
			//	mBoxTopUp->setVisible(true);
			//}
			//else {
			//	mMainLoading->show();
			//	BoxTopUp* boxTopUp = mBoxTopUp;
			//	/*MpGetInfoPurchaseMessageRequest* pRequest = new MpGetInfoPurchaseMessageRequest();
			//	pRequest->setTokenId(GameInfo::mUserInfo.mTokenID.c_str());
			//	pRequest->setChargeID(GameInfo::mUserInfo.mChargeID);
			//	pRequest->setCountry(EGJniHelper::getCountry());
			//	std::string pOS = GameInfo::os_prefix;
			//	pRequest->setOS(pOS);
			//	pRequest->setProductCode(EGJniHelper::getPackage());
			//	MpClientManager::getInstance(0)->sendMessage(pRequest);*/
			//	MpMessage* message = new MpMessage((uint32_t)MP_MSG_GET_SCRATCH_CARD_INFO);
			//	message->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
			//	MpClientManager::getInstance(0)->sendMessage(message);
			//	MpClientManager::getInstance(GameInfo::mUserInfo.mGameID + 1);
			//}

				mMainLoading->show();
				BoxTopUp* boxTopUp = mBoxTopUp;
				MpGetInfoPurchaseMessageRequest* pRequest = new MpGetInfoPurchaseMessageRequest();
				pRequest->setTokenId(GameInfo::mUserInfo.mTokenID.c_str());
				pRequest->setChargeID(GameInfo::mUserInfo.mChargeID);
				pRequest->setCountry(EGJniHelper::getCountry());
				std::string pOS = GameInfo::os_prefix;
				pRequest->setOS(pOS);
				pRequest->setProductCode(EGJniHelper::getPackage());
				MpClientManager::getInstance(0)->sendMessage(pRequest);

			/*MpMessage* message = new MpMessage((uint32_t)MP_MSG_GET_SCRATCH_CARD_INFO);
			message->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
			MpClientManager::getInstance(0)->sendMessage(message);
			MpClientManager::getInstance(GameInfo::mUserInfo.mGameID + 1);*/
		}
		else{
			mBoxTopUp->setVisible(true);
			mBoxTopUp->setPosition(0, 0);
			mBoxTopUp->showBoxLockRecharge();
		}

	}
	else if (pInfoRoom->mGameID == 2) {
		if (EGJniHelper::checkShowTopUp()){
			/*mBoxTopUpCard->setVisible(true);
			mBoxTopUpCard->mEdt_Username->setVisible(false);*/
			mTypeRechargeWP = 1;
			EGJniHelper::showTopUp(1, mTypeRechargeWP);
		}
	}
}
void GameScene::showBoxTopUpFirstTime(){
	if (EGJniHelper::checkShowTopUp1st()){
		/*mBoxTopUpCard->setVisible(true);
		mBoxTopUpCard->mEdt_Username->setVisible(false);*/
		mTypeRechargeWP = 1;
		EGJniHelper::showTopUp(1, mTypeRechargeWP);
	}
}
void GameScene::showTopUpSo(){
	if (EGJniHelper::checkShowTopUp()){
		/*mBoxTopUpCard->setVisible(true);
		mBoxTopUpCard->mEdt_Username->setVisible(false);*/
		mTypeRechargeWP = 2;
		EGJniHelper::showTopUp(1, mTypeRechargeWP);
	}
}

void GameScene::showTopUpByCard() {
	mBoxTopUp->close();
	//mBoxTopUpCard->setVisible(true);
	EGJniHelper::showTopUp(1, mTypeRechargeWP);
}

void GameScene::rechargeBySMS(int pIndex) {
	mBoxTopUp->close();
}

void GameScene::recharge(std::string pSerial, std::string pNumber, std::string pTelco, std::string pUsername) {
	if (pSerial.length() < 6) {
		mErrorCode_Game = TEXT_ERR_RECHARGE_SERIAL;
		return;
	}
	if (pNumber.length() < 12) {
		mErrorCode_Game = TEXT_ERR_RECHARGE_NUMBER;
		return;
	}
	mBoxTopUpCard->setVisible(false);


//	HttpRequest* request = new (std::nothrow) HttpRequest();
//	request->setUrl("http://game3k.net:8989/banca/CardCharger?");
//	request->setRequestType(HttpRequest::Type::POST);
//	request->setResponseCallback(CC_CALLBACK_2(GameScene::onHttpRequestCompleted, this));
//	std::string postData;
//	// write the post data
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID )
//	postData = __String::createWithFormat(
//		"os=%s&card_serial=%s&card_code=%s&devices=%s&code=%s&provider=%s",
//		"a", pSerial.c_str(), pNumber.c_str(), EGJniHelper::getDeviceID(), "BCOL", pTelco.c_str())->getCString();
//#endif
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS )
//	postData = __String::createWithFormat(
//		"os=%s&card_serial=%s&card_code=%s&devices=%s&code=%s&provider=%s",
//		"i", pSerial.c_str(), pNumber.c_str(), EGJniHelper::getDeviceID(), "BCOL", pTelco.c_str())->getCString();
//#endif	
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 )
//	postData = __String::createWithFormat(
//		"os=%s&card_serial=%s&card_code=%s&devices=%s&code=%s&provider=%s",
//		"w", pSerial.c_str(), pNumber.c_str(), EGJniHelper::getDeviceID().c_str(), "BCOL", pTelco.c_str())->getCString();
//#endif
//
//	request->setRequestData(postData.c_str(), strlen(postData.c_str()));
//	HttpClient::getInstance()->send(request);

	EGJniHelper::showTopUp(1, mTypeRechargeWP);
	//mMainLoading->show();

}

void GameScene::onHttpRequestCompleted(HttpClient *sender, HttpResponse *response) {
	if (!response)
		return;

	std::vector<char> *buffer = response->getResponseData();
	std::string pResult;
	log("Http Test, dump data: ");
	for (unsigned int i = 0; i < buffer->size(); i++)
	{
		pResult += (*buffer)[i];
	}


	WXmlReader* pXml = WXmlReader::create();
	pXml->load("string.xml");
	if (pResult != "0") {
		long iCardValue = atol(pResult.c_str());
		float iCoin1K = 100;
		if (mTypeRechargeWP != 1){
			iCoin1K = 5;
		}
		float bonusPercent = 2.5f;

		if (iCardValue >= 500000) {
			bonusPercent = 5.0f;
		}
		else if (iCardValue >= 300000) {
			bonusPercent = 4.0f;
		}
		else if (iCardValue >= 100000) {
			bonusPercent = 3.0f;
		}

		float fCoinValue = bonusPercent  *iCoin1K*  iCardValue / 1000;
		long pMoney = fCoinValue;

		if (mTypeRechargeWP == 1) { // nap vang
			if (getInfoMyselft()){
				getInfoMyselft()->balance += pMoney;
			}
		}
		else { // nap so
			mCountSo += pMoney;
		}
		updateMoneyShop();
		std::string pStrToast = pXml->getNodeTextByTagName("rechargewp_success");
		pStrToast += " " + pResult + " ";
		if (mTypeRechargeWP == 1) { // nap vang
			pStrToast += pXml->getNodeTextByTagName("rechargewp_coin");
		}
		else { // nap so
			pStrToast += pXml->getNodeTextByTagName("rechargewp_gold");
		}
		pStrToast += pXml->getNodeTextByTagName("rechargewp_success2");
		showToast(pStrToast.c_str());
	}
	else {
		showToast(pXml->getNodeTextByTagName("rechargewp_fall"));
		mBoxTopUp->close();
		//mBoxTopUpCard->setVisible(true);
		EGJniHelper::showTopUp(1, mTypeRechargeWP);
	}

	if (mMainLoading)
		mMainLoading->hide();
}

void GameScene::changeTypeRoom(int pType, int pBetMoney){
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	for (int i = mListGun.size() - 1; i >= 0; i--){
		mListGun.at(i)->visibeInfo(!pType);
		mListGun.at(i)->refresh();
	}
	if (pType == 0){
		if (mBoxteam1){
			mBoxteam1->setVisible(false);
			mBoxteam2->setVisible(false);
		}
	}
	else
	{

		if (!mBoxteam1){
			mBoxteam1 = Sprite::createWithSpriteFrameName(IMG_BOX_TEAM1);
			mMainLayer->addChild(mBoxteam1);
			createBoxTeam(mBoxteam1, false);
			mBoxteam1->setZOrder(6);
		}
		mBoxteam1->setPosition(CLIENT_WIDTH / 2, -mBoxteam1->getContentSize().height / 2);
		if (!mBoxteam2){
			mBoxteam2 = Sprite::createWithSpriteFrameName(IMG_BOX_TEAM2);
			mMainLayer->addChild(mBoxteam2);
			createBoxTeam(mBoxteam2, true);
			mBoxteam2->setZOrder(6);
		}
		mBoxteam2->setPosition(CLIENT_WIDTH / 2, CLIENT_HEIGHT + mBoxteam2->getContentSize().height / 2);
		mBoxteam1->stopAllActions();
		mBoxteam2->stopAllActions();
		mBoxteam1->setVisible(true);
		mBoxteam2->setVisible(true);
		mBoxteam1->runAction(MoveBy::create(0.25f, Vec2(0, mBoxteam1->getContentSize().height)));
		mBoxteam2->runAction(MoveBy::create(0.25f, Vec2(0, -mBoxteam2->getContentSize().height)));
	}
	if (isDirty&&!isFirsTimeSetUp){
		pInfoRoom->pTypeRoom = pType;
		pInfoRoom->pBetMoney = pBetMoney;
		for (int i = pInfoRoom->mListPlayer->size() - 1; i >= 0; i--){
			pInfoRoom->mListPlayer->at(i).isReady = false;
		}
		reStart();
	}
	if (isDirty){
		isDirty = false;
	}
}
void GameScene::createBoxTeam(Sprite* pBox, bool pFlip){
	Sprite* pIconMoney = Sprite::create(IMG_ICON_MONEY_GAME);
	pBox->addChild(pIconMoney);

	Label* pMoney = Label::createWithBMFont(FONT_MONEY_GAME, "10000");

	pBox->addChild(pMoney);
	pMoney->setAlignment(kCCTextAlignmentLeft);
	pMoney->setTag(1);
	Sprite* pIconMoneyTaken = Sprite::create(IMG_ICON_LEVEL_GAME);
	pBox->addChild(pIconMoneyTaken);

	Label* pMoneyTaken = Label::createWithBMFont(FONT_MONEY_GAME, "0");

	pBox->addChild(pMoneyTaken);
	pMoneyTaken->setTag(2);
	pMoneyTaken->setAlignment(kCCTextAlignmentLeft);
	if (pFlip){
		pIconMoney->setPosition(Vec2(pBox->getContentSize().width / 2 - 40, pBox->getContentSize().height / 2 + 20));
		pMoney->setPosition(Vec2(pIconMoney->getPositionX() + pIconMoney->getContentSize().width + 8, pBox->getContentSize().height / 2 + 20));
		pIconMoneyTaken->setPosition(Vec2(pBox->getContentSize().width / 2 - 40, pBox->getContentSize().height / 2 - 10));
		pMoneyTaken->setPosition(Vec2(pIconMoneyTaken->getPositionX() + pIconMoneyTaken->getContentSize().width + 8, pBox->getContentSize().height / 2 - 10));
	}
	else{
		pIconMoney->setPosition(Vec2(pBox->getContentSize().width / 2 - 40, pBox->getContentSize().height / 2 + 10));
		pMoney->setPosition(Vec2(pIconMoney->getPositionX() + pIconMoney->getContentSize().width + 8, pBox->getContentSize().height / 2 + 10));
		pIconMoneyTaken->setPosition(Vec2(pBox->getContentSize().width / 2 - 40, pBox->getContentSize().height / 2 - 20));
		pMoneyTaken->setPosition(Vec2(pIconMoneyTaken->getPositionX() + pIconMoneyTaken->getContentSize().width + 8, pBox->getContentSize().height / 2 - 20));
	}

}
void GameScene::showBoxChooseType(){
	if (!mBoxChoosetype->isVisible()){
		closeBoxBetMoney();
		Size _size = Director::sharedDirector()->getWinSize();
		Sprite* pArrow = (Sprite*)mBoxCreateRoom->getChildByTag(10);
		pArrow->setPosition(Vec2(_size.width / 2 + 130, _size.height / 2 + 90));
		pArrow->setVisible(true);
		pArrow->setOpacity(100);
		pArrow->setFlippedX(false);
		pArrow->runAction(FadeTo::create(0.2f, 255));
		pArrow->runAction(MoveBy::create(0.2f, Vec2(5, 0)));
		mBoxChoosetype->stopAllActions();
		mBoxChoosetype->setPosition(Vec2(_size.width / 2, _size.height / 2 + 90));
		mBoxChoosetype->setVisible(true);
		mBoxChoosetype->runAction(Sequence::create(MoveTo::create(0.3f, Vec2(_size.width / 2 + 260, mBoxChoosetype->getPositionY())), CallFunc::create(CC_CALLBACK_0(BoxResize::showBox, mBoxChoosetype)), NULL));
	}
	else{
		closeBoxChooseType();
	}

}
void GameScene::showBoxBetMoney(){
	if (!mBoxChooseBet->isVisible()){
		closeBoxChooseType();
		Size _size = Director::sharedDirector()->getWinSize();
		Sprite* pArrow = (Sprite*)mBoxCreateRoom->getChildByTag(10);
		pArrow->setPosition(Vec2(_size.width / 2 + 130, _size.height / 2 + 20));
		pArrow->setVisible(true);
		pArrow->setOpacity(100);
		pArrow->setFlippedX(false);
		pArrow->runAction(FadeTo::create(0.2f, 255));
		pArrow->runAction(MoveBy::create(0.2f, Vec2(5, 0)));
		mBoxChooseBet->stopAllActions();
		mBoxChooseBet->setPosition(Vec2(_size.width / 2, _size.height / 2 + 20));
		mBoxChooseBet->setVisible(true);
		mBoxChooseBet->runAction(Sequence::create(MoveTo::create(0.3f, Vec2(_size.width / 2 + 260, mBoxChooseBet->getPositionY())), CallFunc::create(CC_CALLBACK_0(BoxResize::showBox, mBoxChooseBet)), NULL));
	}
	else{
		closeBoxBetMoney();
	}

}
void GameScene::closeBoxBetMoney(){
	mBoxChooseBet->setVisible(false);
	mBoxChooseBet->closeBox();
	Sprite* pArrow = (Sprite*)mBoxCreateRoom->getChildByTag(10);
	pArrow->setVisible(false);

}
void GameScene::closeBoxChooseType(){
	mBoxChoosetype->setVisible(false);
	mBoxChoosetype->closeBox();
	Sprite* pArrow = (Sprite*)mBoxCreateRoom->getChildByTag(10);
	pArrow->setVisible(false);

}
//void GameScene::chooseBetMoney(int pIndex){
//	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
//	if (pIndex != mChooseBetMoney){
//		isChangedsetUp = true;
//	}
//	mChooseBetMoney = pIndex;
//	closeBoxBetMoney();
//	closeBoxChooseType();
//	Sprite* box = (Sprite*)mBoxCreateRoom->getChildByTag(1);
//	Sprite* buttonBetMoney = (Sprite*)box->getChildByTag(2);
//	Label *labelText = (Label*)buttonBetMoney->getChildByTag(1);
//	std::string  pBalance = String::createWithFormat("%d", pInfoRoom->mListBetMoney->at(pIndex - 1))->getCString();
//	labelText->setString(EGSupport::addDotMoney(pBalance).c_str());
//	EditBox *editText = (EditBox*)box->getChildByTag(4);
//	editText->setText(EGSupport::addDotMoney(pBalance).c_str());
//	checkReadyCreateRoom();
//}
//void GameScene::chooseTypeRoom(int pIndex, __String* pText){
//	if (mChooseType != pIndex){
//		isChangedsetUp = true;
//	}
//	mChooseType = pIndex;
//	closeBoxBetMoney();
//	closeBoxChooseType();
//	Sprite* box = (Sprite*)mBoxCreateRoom->getChildByTag(1);
//	Sprite* buttonBetMoney = (Sprite*)box->getChildByTag(1);
//	Label *labelText = (Label*)buttonBetMoney->getChildByTag(1);
//	labelText->setString(pText->getCString());
//	checkReadyCreateRoom();
//}
void GameScene::chooseTypeRoom(int pIndex){
	if (mChooseType != pIndex){
		isChangedsetUp = true;
	}
	mChooseType = pIndex;
	Sprite* box = (Sprite*)mBoxCreateRoom->getChildByTag(1);
	for (int i = 0; i < 2; i++){
		EGButtonSprite* button = (EGButtonSprite*)box->getChildByTag(4 + i);
		std::string pFrame = __String::createWithFormat(IMG_BUTTON_CHOOSE_SOLO, 1)->getCString();
		if (i == 1){
			pFrame = __String::createWithFormat(IMG_BUTTON_CHOOSE_TEAM, 1)->getCString();
		}
		button->setSpriteFrame(pFrame.c_str());
		button->removeAllChildrenWithCleanup(true);
	}
	EGButtonSprite* button = (EGButtonSprite*)box->getChildByTag(4 + pIndex);
	std::string pFrame = __String::createWithFormat(IMG_BUTTON_CHOOSE_SOLO, 2)->getCString();
	if (pIndex == 1){
		pFrame = __String::createWithFormat(IMG_BUTTON_CHOOSE_TEAM, 2)->getCString();
	}
	button->setSpriteFrame(pFrame.c_str());
	Sprite* pCheck = Sprite::createWithSpriteFrameName("check-choose-game.png");
	button->addChild(pCheck);
	pCheck->setPosition(Vec2(button->getContentSize().width - 10, button->getContentSize().height / 2));
	checkReadyCreateRoom();
}
void GameScene::checkReadyCreateRoom(){
	Sprite* box = (Sprite*)mBoxCreateRoom->getChildByTag(1);
	EGButtonSprite* buttonAccpet = (EGButtonSprite*)box->getChildByTag(11);
	if (isChangedsetUp &&getInfoMyselft() && getInfoMyselft()->host&& mChooseType >= 0){
		if (buttonAccpet->getOpacity() < 255){
			buttonAccpet->registerTouch(true);
		}
		buttonAccpet->setOpacity(255);
	}
	else{
		if (buttonAccpet->getOpacity() == 255){
			buttonAccpet->unregisterTouch();
		}
		buttonAccpet->setOpacity(177);
	}
}
void GameScene::showBoxCreateRoom(){
	mChooseBetMoney = -1;
	mBoxCreateRoom->setVisible(true);
	Sprite* box = (Sprite*)mBoxCreateRoom->getChildByTag(1);
	EditBox *edit = (EditBox*)box->getChildByTag(10);
	if (getInfoMyselft()){
		if (!getInfoMyselft()->host){
			edit->setEnabled(false);
		}
		else{
			edit->setEnabled(true);
		}
	}
	else{
		edit->setEnabled(false);
	}
	for (int i = 1; i < 6; i++){

		EGSprite* buttonBetMoney = (EGSprite*)box->getChildByTag(i);
		buttonBetMoney->unregisterTouch();
		buttonBetMoney->setOpacity(155);
		if (getInfoMyselft()){
			if (!getInfoMyselft()->host){
			}
			else{
				//labelText->setString(((__String*)labelText->getUserObject())->getCString());
				buttonBetMoney->setOpacity(255);
				buttonBetMoney->registerTouch(true);
			}
		}
	}

	checkReadyCreateRoom();
	updateSetUp();
}
void GameScene::closeBoxCreateRoom(){
	mBoxCreateRoom->setVisible(false);
}
void GameScene::chooseBetMoney(int index){
	if (index != mChooseBetMoney){
		isChangedsetUp = true;
	}
	mChooseBetMoney = index;
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	EGSprite* box = (EGSprite*)mBoxCreateRoom->getChildByTag(1);
	EditBox* edit = (EditBox*)box->getChildByTag(10);
	////edit->setText(EGSupport::addDotMoney(__String::createWithFormat("%d", pInfoRoom->mListBetMoney->at(index))->getCString()).c_str());
	////for (int i = 0; i < pInfoRoom->mListBetMoney->size(); i++){
	////	Sprite* pCheckBox = (Sprite*)box->getChildByTag(i + 1);
	////	pCheckBox->removeAllChildrenWithCleanup(true);
	////}
	Sprite* pCheckBox = (Sprite*)box->getChildByTag(index + 1);
	Sprite* pCheck = Sprite::createWithSpriteFrameName(IMG_CHECK_BOX_BET);
	pCheck->setPosition(Vec2(pCheckBox->getContentSize().width / 2, pCheckBox->getContentSize().height / 2));
	pCheckBox->addChild(pCheck);
	checkReadyCreateRoom();
}
void GameScene::createBoxCreate(){
	WXmlReader* xml = WXmlReader::create();
	xml->load(KEY_XML_FILE);
	Size _size = Director::getInstance()->getWinSize();
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	mBoxCreateRoom = LayerColor::create(Color4B(0, 0, 0, 155));
	mBoxCreateRoom->setVisible(false);
	this->addChild(mBoxCreateRoom);
	mBoxCreateRoom->setZOrder(10);
	if (pInfoRoom->mGameID == 1){
		EGSprite* box = EGSprite::createWithSpriteFrameName(IMG_BOX_CREATE_ROOM);
		box->setZOrder(1);
		box->setTag(1);
		box->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
		box->registerTouch(true);
		mBoxCreateRoom->addChild(box);
		box->setPosition(Vec2(_size.width / 2, _size.height / 2));
			for (int i = 0; i < pInfoRoom->mListBetMoney->size(); i++){
				EGSprite* pStroke = EGSprite::createWithSpriteFrameName(IMG_CHECK_BOX_BET_STROKE);
				box->addChild(pStroke);
				pStroke->setPosition(Vec2(74 + 96 * i, 95));
				pStroke->setTag(i + 1);
				pStroke->setOnTouch(CC_CALLBACK_0(GameScene::chooseBetMoney, this, i));
				std::string  pNum = __String::createWithFormat("%d", pInfoRoom->mListBetMoney->at(i))->getCString();
				Label* pBetMoney = Label::createWithBMFont("font-option-game.fnt", EGSupport::shortCutNumText(pNum, 5, true).c_str());
				box->addChild(pBetMoney);
				pBetMoney->setPosition(Vec2(pStroke->getPositionX(), pStroke->getPositionY() + 30));
				}
		EGSprite* buttonChooseSolo = EGSprite::createWithSpriteFrameName(__String::createWithFormat(IMG_BUTTON_CHOOSE_SOLO, 1)->getCString());
		box->addChild(buttonChooseSolo);
		buttonChooseSolo->setPosition(Vec2(box->getContentSize().width / 2, 290));
		buttonChooseSolo->setOnTouch(CC_CALLBACK_0(GameScene::chooseTypeRoom, this, 0));
		buttonChooseSolo->setTag(4);
		EGSprite* buttonChooseTeam = EGSprite::createWithSpriteFrameName(__String::createWithFormat(IMG_BUTTON_CHOOSE_TEAM, 1)->getCString());
		box->addChild(buttonChooseTeam);
		buttonChooseTeam->setPosition(Vec2(box->getContentSize().width / 2, 290 - buttonChooseTeam->getContentSize().height - 5));
		buttonChooseTeam->setOnTouch(CC_CALLBACK_0(GameScene::chooseTypeRoom, this, 1));
		buttonChooseTeam->setTag(5);
		Scale9Sprite* pEdtSprite_Username = Scale9Sprite::createWithSpriteFrameName("editbox-bet.png");
		EditBox * mEdt_Username = EditBox::create(Size(140, 50), pEdtSprite_Username);
		mEdt_Username->setPosition(Vec2(219, 182));
		mEdt_Username->setFontName(FONT);
		mEdt_Username->setFontSize(20);
		mEdt_Username->setFontColor(Color3B::YELLOW);
		EventKeyboard* pEvent;
		mEdt_Username->setDelegate(this);
		mEdt_Username->setInputFlag(EditBox::InputFlag::SENSITIVE);
		mEdt_Username->setInputMode(EditBox::InputMode::NUMERIC);
		mEdt_Username->setFontColor(Color3B::WHITE);
		mEdt_Username->setPlaceHolder(xml->getNodeTextByTagName(TEXT_LABEL_BET_MONEY).c_str());
		mEdt_Username->setPlaceholderFontColor(Color3B::GRAY);
		mEdt_Username->setMaxLength(REQ_MAXLENGHT_USERNAME);
		mEdt_Username->setReturnType(EditBox::KeyboardReturnType::DONE);
		mEdt_Username->setTag(10);
		box->addChild(mEdt_Username);
		EGButtonSprite* buttonAccept = EGButtonSprite::createWithSpriteFrameName("button-accept.png");
		box->addChild(buttonAccept);
		buttonAccept->setButtonType(EGButton2FrameDark);
		buttonAccept->setPosition(Vec2(box->getContentSize().width / 2, 30));
		buttonAccept->setTag(11);
		buttonAccept->setOnTouch(CC_CALLBACK_0(GameScene::setUpGame, this));
		EGButtonSprite* closeButton = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_CLOSE_CR);
		box->addChild(closeButton);
		closeButton->setButtonType(EGButton2FrameDark);
		closeButton->setPosition(Vec2(box->getContentSize().width - 30, box->getContentSize().height - 30));
		closeButton->setOnTouch(CC_CALLBACK_0(GameScene::closeBoxCreateRoom, this));
	}
	//
	//mBoxCreateRoom = Node::create();
	//mBoxCreateRoom->setVisible(false);
	//this->addChild(mBoxCreateRoom);
	//mBoxCreateRoom->setZOrder(10);
	//EGSprite* box = EGSprite::createWithSpriteFrameName(IMG_BOX_CREATE_ROOM);
	//box->setZOrder(1);
	//box->setTag(1);
	//box->registerTouch(true);
	//mBoxCreateRoom->addChild(box);
	//box->setPosition(Vec2(_size.width / 2, _size.height / 2));
	//Sprite* pTitle = Sprite::createWithSpriteFrameName(IMG_TITLE_CREATE_ROOM);
	//box->addChild(pTitle);
	//pTitle->setPosition(Vec2(box->getContentSize().width / 2, box->getContentSize().height));
	//EGButtonSprite* buttonChoose1 = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_CHOOSE_CR1);
	//box->addChild(buttonChoose1);
	//buttonChoose1->setButtonType(EGButton2FrameDark);
	//buttonChoose1->setPosition(Vec2(box->getContentSize().width / 2, box->getContentSize().height / 2 + 90));
	//buttonChoose1->setOnTouch(CC_CALLBACK_0(GameScene::showBoxChooseType, this));
	//buttonChoose1->setTag(1);
	//addTextforButton(buttonChoose1, xml->getNodeTextByTagName(TEXT_TYPE_ROOM0));

	//EGButtonSprite* buttonChoose2 = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_CHOOSE_CR1);
	//box->addChild(buttonChoose2);
	//buttonChoose2->setButtonType(EGButton2FrameDark);
	//buttonChoose2->setPosition(Vec2(box->getContentSize().width / 2, box->getContentSize().height / 2 + 20));
	//buttonChoose2->setOnTouch(CC_CALLBACK_0(GameScene::showBoxBetMoney, this));
	//buttonChoose2->setTag(2);
	//addTextforButton(buttonChoose2, xml->getNodeTextByTagName(TEXT_LABEL_BET_MONEY));

	//Scale9Sprite* pEdtSprite_Username = Scale9Sprite::createWithSpriteFrameName("edit-box-bet.png");
	//EditBox * mEdt_Username = EditBox::create(Size(240, 53), pEdtSprite_Username);
	//mEdt_Username->setPosition(Vec2(box->getContentSize().width / 2, box->getContentSize().height / 2 - 50));
	//mEdt_Username->setFontName(FONT);
	//mEdt_Username->setFontSize(20);
	//EventKeyboard* pEvent;
	//mEdt_Username->setDelegate(this);
	//mEdt_Username->setInputFlag(EditBox::InputFlag::SENSITIVE);
	//mEdt_Username->setInputMode(EditBox::InputMode::NUMERIC);
	//mEdt_Username->setFontColor(Color3B::WHITE);
	//mEdt_Username->setPlaceHolder(xml->getNodeTextByTagName(TEXT_LABEL_BET_MONEY).c_str());
	//mEdt_Username->setPlaceholderFontColor(Color3B::GRAY);
	//mEdt_Username->setMaxLength(REQ_MAXLENGHT_USERNAME);
	//mEdt_Username->setReturnType(EditBox::KeyboardReturnType::DONE);
	//mEdt_Username->setTag(4);
	//box->addChild(mEdt_Username);

	//EGButtonSprite* buttonAccept = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_ACCEPT_CR);
	//box->addChild(buttonAccept);
	//buttonAccept->setButtonType(EGButton2FrameDark);
	//buttonAccept->setPosition(Vec2(box->getContentSize().width / 2, box->getContentSize().height / 2 - 110));
	//buttonAccept->setTag(3);
	//buttonAccept->setOnTouch(CC_CALLBACK_0(GameScene::setUpGame, this));

	//EGButtonSprite* closeButton = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_CLOSE_CR);
	//box->addChild(closeButton);
	//closeButton->setButtonType(EGButton2FrameDark);
	//closeButton->setPosition(Vec2(box->getContentSize().width - 10, box->getContentSize().height - 10));
	//closeButton->setOnTouch(CC_CALLBACK_0(GameScene::closeBoxCreateRoom, this));

	//Sprite* pArrow = Sprite::createWithSpriteFrameName(IMG_ARROW_SHOW_BOX);
	//mBoxCreateRoom->addChild(pArrow);
	//pArrow->setPosition(Vec2(0, 0));
	//pArrow->setVisible(false);
	//pArrow->setTag(10);
	//pArrow->setZOrder(1);

	//mBoxChoosetype = BoxResize::createBoxResize(IMG_BOX_RESIZE_TOP, IMG_BOX_RESIZE_MID, IMG_BOX_RESIZE_TOP);
	//mBoxCreateRoom->addChild(mBoxChoosetype);
	//mBoxChoosetype->setPosition(Vec2(_size.width / 2, _size.height / 2));



	//__String *pStringTypeRoom[] = {
	//	String::create(xml->getNodeTextByTagName(TEXT_TYPE_ROOM1)),
	//	String::create(xml->getNodeTextByTagName(TEXT_TYPE_ROOM2)) };
	//for (int i = 0; i < 2; i++){
	//	EGButtonSprite* buttonChoose = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_CHOOSE_CR2);
	//	mBoxChoosetype->addButton(buttonChoose);
	//	buttonChoose->setButtonType(EGButton2FrameDark);
	//	pStringTypeRoom[i]->retain();
	//	buttonChoose->setOnTouch(CC_CALLBACK_0(GameScene::chooseTypeRoom, this, i, pStringTypeRoom[i]));
	//	addTextforButton(buttonChoose, pStringTypeRoom[i]->getCString());
	//}
	//mBoxChoosetype->closeBox();
	//mBoxChoosetype->setVisible(false);
	//mBoxChooseBet = BoxResize::createBoxResize(IMG_BOX_RESIZE_TOP, IMG_BOX_RESIZE_MID, IMG_BOX_RESIZE_TOP);
	//mBoxCreateRoom->addChild(mBoxChooseBet);
	//mBoxChooseBet->setPosition(Vec2(_size.width / 2, _size.height / 2));
	//mBoxChooseBet->closeBox();
	//mBoxChooseBet->setVisible(false);

	//mBoxChooseBet->clearAllButton();
	//roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	//if (pInfoRoom->mGameID < 2){
	//	for (int i = pInfoRoom->mListBetMoney->size() - 1; i >= 0; i--){
	//		int index = pInfoRoom->mListBetMoney->size() - 1 - i;
	//		EGButtonSprite* buttonChoose = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_CHOOSE_CR2);
	//		mBoxChooseBet->addButton(buttonChoose);
	//		buttonChoose->setButtonType(EGButton2FrameDark);
	//		buttonChoose->setOnTouch(CC_CALLBACK_0(GameScene::chooseBetMoney, this, index + 1));
	//		std::string pBetMoney = String::createWithFormat("%d", pInfoRoom->mListBetMoney->at(index))->getCString();
	//		addTextforButton(buttonChoose, EGSupport::addDotMoney(pBetMoney).c_str());
	//	}
	//}

}
void GameScene::editBoxEditingDidBegin(EditBox* editBox){
#if(CC_TARGET_PLATFORM == CC_PLATFORM_WP8 || CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
#else
	std::string pStrMoney = editBox->getText();
	for (int i = pStrMoney.length() - 1; i >= 0; i--){
		std::string pDot = ".";
		if (pStrMoney.at(i) == pDot.at(0)){
			pStrMoney.erase(pStrMoney.begin() + i);
		}
	}
	editBox->setText(pStrMoney.c_str());
#endif
}
void GameScene::editBoxReturn(EditBox* editBox) {
#if(CC_TARGET_PLATFORM == CC_PLATFORM_WP8 || CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
#else
	std::string pStrMoney = editBox->getText();
	for (int i = pStrMoney.length() - 1; i >= 0; i--){
		std::string pDot = ".";
		if (pStrMoney.at(i) == pDot.at(0)){
			pStrMoney.erase(pStrMoney.begin() + i);
		}
	}
	editBox->setText(EGSupport::addDotMoney(pStrMoney).c_str());
	isChangedsetUp = true;
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	Sprite* pCheckStroke = NULL;

	EGSprite* box = (EGSprite*)mBoxCreateRoom->getChildByTag(1);
	////for (int i = 0; i < pInfoRoom->mListBetMoney->size(); i++){
	////	Sprite* pCheckBox = (Sprite*)box->getChildByTag(i + 1);
	////	pCheckBox->removeAllChildrenWithCleanup(true);
	////	if (atoi(pStrMoney.c_str()) == pInfoRoom->mListBetMoney->at(i)){
	////		pCheckStroke = pCheckBox;
	////	}
	////}
	if (pCheckStroke){
		Sprite* pCheck = Sprite::createWithSpriteFrameName(IMG_CHECK_BOX_BET);
		pCheck->setPosition(Vec2(pCheckStroke->getContentSize().width / 2, pCheckStroke->getContentSize().height / 2));
		pCheckStroke->addChild(pCheck);
	}
	checkReadyCreateRoom();
#endif
}
void GameScene::setUpGame()
{
	isChangedsetUp = false;
	Sprite* box = (Sprite*)mBoxCreateRoom->getChildByTag(1);
	EditBox* editText = (EditBox*)box->getChildByTag(10);
	roomStruct* roomInfo = (roomStruct*)mInfoRoom;
	WXmlReader* pXml = WXmlReader::create();
	std::string pBetMoney = editText->getText();
	for (int i = pBetMoney.length() - 1; i >= 0; i--){
		std::string pDot = ".";
		if (pBetMoney.at(i) == pDot.at(0)){
			pBetMoney.erase(pBetMoney.begin() + i);
		}
	}
	if (atol(pBetMoney.c_str()) > GameInfo::mUserInfo.mBalance){
		showToast(pXml->getNodeTextByTagName("not_enough_money_to_create"), 600, Vec2(400, 160));
	}
	else if (atol(pBetMoney.c_str()) < roomInfo->pMinMoney){
		std::string pStrMoney = __String::createWithFormat("%d", (int)roomInfo->pMinMoney)->getCString();
		showToast(__String::createWithFormat(pXml->getNodeTextByTagName("betmoney_too_low").c_str(), EGSupport::addDotMoney(pStrMoney).c_str())->getCString(), 600, Vec2(400, 160));
	}
	else
	{
		/*MpSetupGameMessageRequest* pRequest = new MpSetupGameMessageRequest();
		pRequest->setBetMoney(atol(pBetMoney.c_str()));
		pRequest->setGameType(mChooseType);
		addComponentDefault(pRequest);*/
	}
	closeBoxCreateRoom();

}
Label* GameScene::addTextforButton(Sprite* pButton, std::string pString){
	Label *label = Label::createWithBMFont(FONT_ARIAL_FNT, pString.c_str());
	pButton->addChild(label);
	label->setPosition(Vec2(pButton->getContentSize().width / 2, pButton->getContentSize().height / 2));
	label->setTag(1);
	label->setUserObject(new __String(pString));
	return label;
}
void GameScene::detachLayerResult(LayerColor* layer){
	if (layer == layerResultUp){
		layerResultUp = NULL;
	}
	layer->stopAllActions();
	runAction(CallFunc::create(CC_CALLBACK_0(LayerColor::removeAllChildrenWithCleanup, layer, true)));
	runAction(CallFunc::create(CC_CALLBACK_0(LayerColor::removeFromParentAndCleanup, layer, true)));
	reStart();

}
void GameScene::createBoxResult(std::vector<GamePlayerResult> pRankList, int pType, int pResult){
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	Size pSize = Director::sharedDirector()->getWinSize();
	std::string pUserNAme = "";
	for (int i = 0; i < pRankList.size(); i++){
		pUserNAme += pRankList.at(i).Username;
	}
	//showToast(__String::createWithFormat("end game : %s", pUserNAme.c_str())->getCString(), 600, Vec2(400, 240));
	layerResultUp = LayerColor::create(ccc4(0, 0, 0, 180));
	this->addChild(layerResultUp);
	layerResultUp->setContentSize(Size(CLIENT_WIDTH, CLIENT_HEIGHT / 2));
	layerResultUp->setPosition(Vec2(0, CLIENT_HEIGHT / 2 + 200));
	layerResultUp->setOpacity(0);
	LayerColor* layerResultDown = LayerColor::create(ccc4(0, 0, 0, 180));
	layerResultDown->setContentSize(Size(CLIENT_WIDTH, CLIENT_HEIGHT / 2));
	layerResultDown->setPosition(Vec2(0, -200));
	layerResultDown->setOpacity(0);
	this->addChild(layerResultDown);
	int pMoveBy = 150;
	if (pType == 0){
		pMoveBy = 200;
		for (int i = 0; i < pRankList.size(); i++){
			GamePlayerResult playerResult = pRankList.at(i);
			ClientPlayerInfoEX* player = getPlayerByName(playerResult.Username);
			if (player){
				int index = player->position;
				int pTypeAchieve = i + 1;
				if (playerResult.WinMoney == 0){
					pTypeAchieve = 0;
				}
				Sprite* pAchieve = Sprite::createWithSpriteFrameName(__String::createWithFormat("result_achieve (%d).png", pTypeAchieve)->getCString());
				pAchieve->setPosition(Vec2((index / 2) * 800 + ((index / 2)*-2 + 1)*(220 + (index % 2) * 360), ((index / 2) + 1) * 80));
				pAchieve->runAction(Sequence::create(DelayTime::create(5), MoveBy::create(0.2f, Vec2(0, ((index / 2) * 2 - 1) * 50)), NULL));
				if (index < 2){
					layerResultDown->addChild(pAchieve);
				}
				else{
					layerResultUp->addChild(pAchieve);
				}
			}
		}
	}
	else{
		int* pResult = new int();
		for (int i = 0; i < pRankList.size(); i++){
			ClientPlayerInfoEX* player = getPlayerByName(pRankList.at(i).Username);
			if (player && player->position >= 0 && player->position < 4){
				int position = player->position / 2;
				pResult[position] = 2;
				if (player && pRankList.at(i).WinMoney >0){
					pResult[position] = 1;
				}
				else  if (player && pRankList.at(i).WinMoney == 0){
					pResult[position] = 0;
				}
			}
		}
		if (pResult[0] == 1 && pResult[1] == 1){
			pResult[0] = 3;
			pResult[1] = 3;
		}
		for (int i = 0; i < 2; i++){
			Sprite* pAchieve = Sprite::createWithSpriteFrameName(__String::createWithFormat("result_achieve_team (%d).png", pResult[i])->getCString());
			pAchieve->setPosition(Vec2(CLIENT_WIDTH / 2, 120));
			if (i < 1){
				layerResultDown->addChild(pAchieve);
			}
			else{
				layerResultUp->addChild(pAchieve);
			}
		}
	}
	layerResultUp->runAction(Sequence::create(FadeTo::create(0.2f, 180), DelayTime::create(5), CallFunc::create(CC_CALLBACK_0(GameScene::reStart, this)), CallFunc::create(CC_CALLBACK_0(GameScene::refresh, this)), CallFunc::create(CC_CALLBACK_0(Layer::setOpacity, layerResultUp, 0)), NULL));
	layerResultDown->runAction(Sequence::create(FadeTo::create(0.2f, 180), DelayTime::create(5), CallFunc::create(CC_CALLBACK_0(GameScene::bonusResult, this, pRankList)), CallFunc::create(CC_CALLBACK_0(Layer::setOpacity, layerResultDown, 0)), NULL));
	layerResultUp->runAction(Sequence::create(MoveBy::create(0.2f, Vec2(0, -pMoveBy)), DelayTime::create(10), CallFunc::create(CC_CALLBACK_0(GameScene::detachLayerResult, this, layerResultUp)), NULL));
	layerResultDown->runAction(Sequence::create(MoveBy::create(0.2f, Vec2(0, pMoveBy)), DelayTime::create(10), CallFunc::create(CC_CALLBACK_0(GameScene::detachLayerResult, this, layerResultDown)), NULL));
	/*LayerColor* layerBG = LayerColor::create(ccc4(0, 0, 0, 180));
	this->addChild(layerBG);
	layerBG->runAction(Sequence::create(DelayTime::create(10), CallFunc::create(CC_CALLBACK_0(GameScene::reStart, this)), CallFunc::create(CC_CALLBACK_0(GameScene::detachLayerResult, this, layerBG)), NULL));
	EGSprite* mBoxResult = EGSprite::createWithSpriteFrameName(IMG_BOX_RESULT);
	layerBG->addChild(mBoxResult);
	mBoxResult->setPosition(Vec2(pSize.width / 2, pSize.height / 2));
	mBoxResult->registerTouch(true);
	mBoxResult->setScale(0);
	mBoxResult->runAction(EaseElasticOut::create(ScaleTo::create(0.5f, 1)));
	EGButtonSprite*buttonClose = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_CLOSE_CR);
	mBoxResult->addChild(buttonClose);
	buttonClose->setPosition(Vec2(mBoxResult->getContentSize().width - 5, mBoxResult->getContentSize().height - 5));
	buttonClose->setButtonType(EGButton2FrameDark);
	buttonClose->setOnTouch(CC_CALLBACK_0(GameScene::detachLayerResult, this, layerBG));
	Sprite* pTitleResult = Sprite::createWithSpriteFrameName(String::createWithFormat(IMG_TITLE_RESULT, abs(pResult - 2))->getCString());
	mBoxResult->addChild(pTitleResult);
	pTitleResult->setPosition(Vec2(mBoxResult->getContentSize().width / 2, mBoxResult->getContentSize().height - 5));
	Sprite * pSTTlabel = Sprite::createWithSpriteFrameName(IMG_STT_LABEL);
	mBoxResult->addChild(pSTTlabel);
	pSTTlabel->setPosition(Vec2(62, 328));
	Sprite * pName = Sprite::createWithSpriteFrameName(IMG_NAME_LABEL);
	mBoxResult->addChild(pName);
	pName->setPosition(Vec2(mBoxResult->getContentSize().width / 2, 328));
	Sprite * pScore = Sprite::createWithSpriteFrameName(IMG_SCORE_LABEL);
	mBoxResult->addChild(pScore);
	pScore->setPosition(Vec2(508, 328));
	for (int i = 0; i < pRankList.size(); i++){
	ClientPlayerInfoEX* mySelf = getInfoMyselft();
	if (mySelf){
	std::string pMyName = mySelf->username;
	for (int j = 0; j < pMyName.length(); j++){
	pMyName[j] = std::tolower(pMyName[j]);
	}
	if (pMyName == pRankList.at(i).Username){
	GameInfo::mUserInfo.mBalance += pRankList.at(i).WinMoney;
	break;
	}
	}
	}

	if (pInfoRoom->pTypeRoom == 0){

	for (int i = 0; i < 4; i++){
	Sprite* pBoxInfo = Sprite::createWithSpriteFrameName(IMG_BOX_INFO_RESULT_SMALL);
	mBoxResult->addChild(pBoxInfo);
	pBoxInfo->setPosition(Vec2(mBoxResult->getContentSize().width / 2, 269 - i* (pBoxInfo->getContentSize().height + 7)));
	if (i < pRankList.size()){
	Label* pLabelName = Label::createWithBMFont(FONT_BMF_NAME, pRankList.at(i).Username.c_str());
	pBoxInfo->addChild(pLabelName);
	pLabelName->setPosition(Vec2(pBoxInfo->getContentSize().width / 2, pBoxInfo->getContentSize().height / 2));
	std::string pAdd = "%d";
	if ((int)pRankList.at(i).WinMoney >0){
	pAdd = "+" + pAdd;
	}
	Label *pLabel = Label::createWithBMFont("font-info-game.fnt", String::createWithFormat(pAdd.c_str(), (int)pRankList.at(i).WinMoney)->getCString());
	pLabel->setAlignment(kCCTextAlignmentRight);
	pLabel->setAnchorPoint(Vec2(1, 0.5f));
	pBoxInfo->addChild(pLabel);
	pLabel->setPosition(Vec2(pBoxInfo->getContentSize().width - 12, pBoxInfo->getContentSize().height / 2));
	Label *pLabelExp = Label::createWithBMFont(FONT_BMF_NAME, String::createWithFormat("+%d Exp", pRankList.at(i).Exp)->getCString());
	pBoxInfo->addChild(pLabelExp);
	pLabelExp->setPosition(Vec2(pBoxInfo->getContentSize().width - 140, pBoxInfo->getContentSize().height / 2));
	pLabelExp->setRotation(-30);
	if (i == 0){
	pLabelName->setColor(ccc3(255, 255, 0));
	pLabel->setColor(ccc3(255, 255, 0));
	pLabelExp->setColor(Color3B::GREEN);
	}

	}
	}
	Sprite* pSTT = Sprite::createWithSpriteFrameName(IMG_STT);
	mBoxResult->addChild(pSTT);
	pSTT->setPosition(Vec2(62, mBoxResult->getContentSize().height / 2 - 20));
	}
	else{
	for (int i = 0; i < 4; i++){
	if (i == 0 || i == 2){
	Sprite* pBoxInfo = Sprite::createWithSpriteFrameName(IMG_BOX_INFO_RESULT_BIG);
	mBoxResult->addChild(pBoxInfo);
	pBoxInfo->setPosition(Vec2(mBoxResult->getContentSize().width / 2, 240 - (i / 2)* (pBoxInfo->getContentSize().height + 7)));
	Sprite* pResult = Sprite::createWithSpriteFrameName(__String::createWithFormat(IMG_TITLE_RESULT, (i / 2) + 1)->getCString());
	mBoxResult->addChild(pResult);
	pResult->setPosition(Vec2(140, 240 - (i / 2) * 120));
	pResult->setScale(0.6f);
	pResult->setRotation(-45);
	}
	Sprite* pBoxInfo = Sprite::createWithSpriteFrameName(IMG_BOX_INFO_RESULT_SMALL);
	pBoxInfo->setOpacity(0);
	mBoxResult->addChild(pBoxInfo);
	pBoxInfo->setPosition(Vec2(mBoxResult->getContentSize().width / 2, 269 - i* (pBoxInfo->getContentSize().height + 7)));
	if (i < pRankList.size()){
	Label* pLabelName = Label::createWithBMFont(FONT_BMF_NAME, pRankList.at(i).Username.c_str());
	pBoxInfo->addChild(pLabelName);
	pLabelName->setPosition(Vec2(pBoxInfo->getContentSize().width / 2, pBoxInfo->getContentSize().height / 2));
	std::string pAdd = "%d";
	if ((int)pRankList.at(i).WinMoney >0){
	pAdd = "+" + pAdd;
	}
	Label* pLabel = Label::createWithBMFont("font-info-game.fnt", String::createWithFormat(pAdd.c_str(), (int)pRankList.at(i).WinMoney)->getCString());
	pLabel->setAlignment(kCCTextAlignmentRight);
	pLabel->setAnchorPoint(Vec2(1, 0.5f));
	pBoxInfo->addChild(pLabel);
	pLabel->setPosition(Vec2(pBoxInfo->getContentSize().width - 12, pBoxInfo->getContentSize().height / 2));
	Label *pLabelExp = Label::createWithBMFont(FONT_BMF_NAME, String::createWithFormat("+%d Exp", pRankList.at(i).Exp)->getCString());
	pBoxInfo->addChild(pLabelExp);
	pLabelExp->setPosition(Vec2(pBoxInfo->getContentSize().width - 140, pBoxInfo->getContentSize().height / 2));
	pLabelExp->setRotation(-30);
	if (i < 2){
	pLabelName->setColor(ccc3(255, 255, 0));
	pLabel->setColor(ccc3(255, 255, 0));
	pLabelExp->setColor(Color3B::GREEN);
	}
	}

	}*/

	//}
}
void GameScene::bonusResult(std::vector<GamePlayerResult> pRankList){
	for (int i = 0; i < pRankList.size(); i++){
		ClientPlayerInfoEX* player = getPlayerByName(pRankList.at(i).Username);
		if (player && player->position< 4){
			std::string pString = __String::createWithFormat("%d", pRankList.at(i).WinMoney)->getCString();
			pString = EGSupport::addDotMoney(pString);
			if (pRankList.at(i).WinMoney >0){
				pString = "+" + pString;
			}
			int pSide = 1;
			if (player->position >= 2){
				pSide = -1;
			}
			Node* pNode = Node::create();
			this->addChild(pNode);
			pNode->setPosition(Vec2(mListGun.at(player->position)->getPosition().x, mListGun.at(player->position)->getPosition().y + pSide * 110));
			Label* labelGold = Label::createWithBMFont("font-bonus-result.fnt", pString.c_str());
			pNode->addChild(labelGold);
			if (pRankList.at(i).WinMoney > 0){
				labelGold->setColor(Color3B::YELLOW);
			}
			else{
				labelGold->setColor(Color3B::GRAY);
			}
			if (player == getInfoMyselft()){
				GameInfo::mUserInfo.mBalance += pRankList.at(i).WinMoney;
			}
			Sprite* pGold = Sprite::createWithSpriteFrameName("coin.png");
			pGold->setPosition(Vec2(-labelGold->getContentSize().width / 2 - pGold->getContentSize().width / 2, 5));
			pNode->addChild(pGold);
			pGold->setScale(0.8f);
			Label* labelExp = Label::createWithBMFont("font-bonus-result.fnt", __String::createWithFormat("+%d Exp", pRankList.at(i).Exp)->getCString());
			pNode->addChild(labelExp);
			labelExp->setScale(0.78);
			labelExp->setPosition(Vec2(labelGold->getPositionX(), labelGold->getPositionY() - 20));
			labelExp->setColor(Color3B::GREEN);
			pNode->runAction(Sequence::create(MoveBy::create(5, Vec2(0, pSide * 50)), CallFunc::create(CC_CALLBACK_0(Node::removeAllChildrenWithCleanup, pNode, true)), CallFunc::create(CC_CALLBACK_0(Node::removeFromParentAndCleanup, pNode, true)), NULL));
		}
	}
}
void GameScene::resortScore(){
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	pScoreTeam1 = 0;
	pScoreTeam2 = 0;
	for (int i = pInfoRoom->mListPlayer->size() - 1; i >= 0; i--){
		if (pInfoRoom->pTypeRoom == 0){
			for (int j = i; j >= 1; j--){
				if (pInfoRoom->mListPlayer->at(i).score < pInfoRoom->mListPlayer->at(j).score)
				{
					//std::swap(pInfoRoom->mListPlayer->begin() + i, pInfoRoom->mListPlayer->begin() + j);
					ClientPlayerInfoEX tmp = (*pInfoRoom->mListPlayer)[i];
					(*pInfoRoom->mListPlayer)[i] = (*pInfoRoom->mListPlayer)[j];
					(*pInfoRoom->mListPlayer)[j] = tmp;
				}
			}
		}
		else{
			if (pInfoRoom->mListPlayer->at(i).position < 2){
				pScoreTeam1 = pInfoRoom->mListPlayer->at(i).score;
			}
			else{
				pScoreTeam2 = pInfoRoom->mListPlayer->at(i).score;
			}
		}
	}
	int pTeamWin = 0;
	if (min(pScoreTeam1, pScoreTeam2) != pScoreTeam1){
		pTeamWin = 1;
	}
	for (int i = 0; i < pInfoRoom->mListPlayer->size(); i--){
		ClientPlayerInfoEX player = pInfoRoom->mListPlayer->at(i);
		if (player.position / 2 != pTeamWin){
			for (int j = i + 1; j < pInfoRoom->mListPlayer->size(); j--){
				if (pInfoRoom->mListPlayer->at(j).position / 2 == pTeamWin)
				{
					//std::swap(pInfoRoom->mListPlayer->begin() + i, pInfoRoom->mListPlayer->begin() + j);
					ClientPlayerInfoEX tmp = (*pInfoRoom->mListPlayer)[i];
					(*pInfoRoom->mListPlayer)[i] = (*pInfoRoom->mListPlayer)[j];
					(*pInfoRoom->mListPlayer)[j] = tmp;
				}
			}
		}
	}
}
ClientPlayerInfoEX* GameScene::getInfoMyselft(){
	ClientPlayerInfoEX *mInfoMySelf = NULL;
	roomStruct* roomInfo = (roomStruct*)mInfoRoom;
	if (!roomInfo->mListPlayer)
		return NULL;
	if (roomInfo){
		for (int i = 0; i < roomInfo->mListPlayer->size(); i++){
			std::string pName = roomInfo->mListPlayer->at(i).username;
			for (int i = 0; i < pName.length(); i++) {
				pName[i] = std::tolower(pName[i]);
			}
			if (GameInfo::mUserInfo.mUsername == pName){

				mInfoMySelf = &roomInfo->mListPlayer->at(i);
			}
		}
	}
	return mInfoMySelf;
}
void GameScene::doItemRequest(int index, std::vector<uint8_t> pListFish, Vec2 pPosition){
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	if (pInfoRoom->mListItemSp.at(index).quantity > 0){
		if (pInfoRoom->mGameID != 2){
			int a = 1;
			/*MpUseItemMessageRequest* request = new MpUseItemMessageRequest();
			request->setItemId(index + 200);
			request->setListFishes(pListFish);
			request->setPosition(MpPoint(pPosition.x, pPosition.y));
			addComponentDefault(request);*/
		}
		else{
			if (getInfoMyselft()){
				doItem(getInfoMyselft()->username, index, pListFish, pPosition);
			}
			UserDefault::getInstance()->setIntegerForKey(__String::createWithFormat(KEY_ITEM_SP, index + 1)->getCString(), pInfoRoom->mListItemSp.at(index).quantity);
			refreshItemSp();
		}

	}
}
void GameScene::addComponentDefault(MpMessageRequest* pRequest)
{
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	pRequest->setTokenId(GameInfo::mUserInfo.mTokenID.c_str());
	pRequest->addInt(MP_PARAM_TABLE_ID, pInfoRoom->mTableID);
	MpClientManager::getInstance()->sendMessage(pRequest);	
}

void GameScene::doItem(std::string pUserName, int index, std::vector<uint8_t> pListFish, Vec2 pPosition){
	roomStruct *infoRoom = (roomStruct*)mInfoRoom;
	checkTaskGame(index + 1, 2, 1);
	EGSprite* pEffect = EGSprite::createWithSpriteFrameName(__String::createWithFormat(IMG_EFFECT_ITEM_SP, index + 1, 1)->getCString());
	int pCountFrame = 6;
	if (index == 2){
		pCountFrame = 1;
	}
	Animation* _animaionEffect = Animation::create();
	for (int i = 0; i < pCountFrame; i++){
		_animaionEffect->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_EFFECT_ITEM_SP, index + 1, i + 1)->getCString()));
	}
	_animaionEffect->setDelayPerUnit(0.1f);
	mMainLayer->addChild(pEffect);
	ActionInterval* actionEffect;
	if (index != 2){
		actionEffect = Animate::create(_animaionEffect);
	}
	else{
		actionEffect = Repeat::create(Sequence::create(ScaleTo::create(0.4f, 1.2f), ScaleTo::create(0.4f, 1), NULL), 20);
	}
	pEffect->runAction(Sequence::create(actionEffect, CallFunc::create(CC_CALLBACK_0(EGSprite::performRelease, pEffect)), NULL));
	pEffect->setPosition(pPosition);
	playSound(__String::createWithFormat(SOUND_ITEM_SUOPORT, index + 1)->getCString());
	if (index == 2){
		mPointTrap = new Vec2(pPosition);
		runAction(Sequence::create(DelayTime::create(16), CallFunc::create(CC_CALLBACK_0(GameScene::detachTrap, this)), NULL));
	}
	ClientPlayerInfoEX* myselft = getInfoMyselft();
	if (myselft){
		for (int j = pListFish.size() - 1; j >= 0; j--){
			uint8_t pID = pListFish.at(j);
			for (int i = mListFish.size() - 1; i >= 0; i--){
				Fish * _fish = mListFish.at(i);
				if (!_fish->isDead()){
					if (atoi(_fish->getId().c_str()) == pID){
						if (index == 0){
							if (infoRoom->mGameID == 2){
								_fish->dead();
								gift(_fish->getPosition(), Vec2(400, 0), _fish->getObject()->getMoney(), false);
								checkTaskGame(_fish->getType(), 1, 1);
								if (_fish->getType() == 18 || _fish->getType() == 17){
									showBoxShark();
								}
								else if (_fish->getType() == 19 || _fish->getType() == 20){
									showBoxMaiDen();
								}
								addFishDead(_fish->getType());
							}
						}
						else if (index == 1){
							_fish->stun(7);
							EGSprite* pEffect = EGSprite::createWithSpriteFrameName(__String::createWithFormat(IMG_EFFECT_STUN, 1)->getCString());
							_fish->addChild(pEffect);
							Animation* _animation = Animation::create();
							for (int i = 0; i < 3; i++){
								_animation->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_EFFECT_STUN, i + 1)->getCString()));
							}
							_animation->setDelayPerUnit(0.2f);
							pEffect->runAction(RepeatForever::create(Animate::create(_animation)));
							pEffect->runAction(Sequence::create(DelayTime::create(7), CallFunc::create(CC_CALLBACK_0(EGSprite::performRelease, pEffect)), NULL));
						}
					}
				}
			}
		}


		if (pUserName == myselft->username && infoRoom->mGameID != 2){
			int pCount = infoRoom->mListItemSp.at(index).quantity;
			infoRoom->mListItemSp.at(index).quantity = pCount - 1;
			for (int i = 1; i >= 0; i--){
				BoxResize* boxOption = (BoxResize*)mMainLayer->getChildByTag(100);
				EGButtonSprite* itemSp = (EGButtonSprite*)boxOption->getChildByTag(i + 1);
				Label* pLabel = (Label*)itemSp->getChildByTag(1);
				pLabel->setString(__String::createWithFormat("%d", infoRoom->mListItemSp.at(i).quantity)->getCString());
			}
		}
		else if (infoRoom->mGameID == 2){
			infoRoom->mListItemSp.at(index).quantity--;
		}
	}
}
void GameScene::detachTrap(){
	delete mPointTrap;
	mPointTrap = NULL;
	//for (int i = 0; i < mListFish.size(); i++){
	//	Fish* pFish = mListFish.at(i);
	//	if (!pFish->isDead() && !pFish->isTrap()){
	//		pFish->unTrap();
	//	}
	//}
}
void GameScene::chooseItem(int index){
	roomStruct *pInfoRoom = (roomStruct*)mInfoRoom;
	if (isStartGame){
		if (pInfoRoom->mListItemSp.at(index).quantity > 0){
			if (mRangeItem){
				mRangeItem->removeAllChildrenWithCleanup(true);
				mRangeItem->removeFromParentAndCleanup(true);
				mRangeItem = NULL;
			}
			mRangeItem = Sprite::createWithSpriteFrameName(IMG_RANGE_ITEM);
			mMainLayer->addChild(mRangeItem);
			mRangeItem->setUserData(new int(index));
			mRangeItem->setVisible(false);
			Sprite* pIcon = Sprite::createWithSpriteFrameName(__String::createWithFormat(IMG_ITEM_SP, index + 1)->getCString());
			mRangeItem->addChild(pIcon);
			pIcon->setPosition(Vec2(mRangeItem->getContentSize().width / 2, mRangeItem->getContentSize().height / 2));
		}
		if (pInfoRoom->mGameID != 2){
			showBoxOption();
		}
	}
}
void GameScene::openBoxX(BoxResize *pBox){
	if (!pBox->isOpen()){
		pBox->showBox();
	}
	else{
		pBox->closeBox();
	}
}
void GameScene::showBoxOption() {
	BoxResize* boxOptionSp = (BoxResize*)mMainLayer->getChildByTag(100);
	EGButtonSprite *buttonOption = (EGButtonSprite*)mMainLayer->getChildByTag(101);
	buttonOption->stopAllActions();
	if (!boxOptionSp->isOpen()){
		buttonOption->runAction(RotateBy::create(0.2f, 360));
		boxOptionSp->showBox();
	}
	else{
		buttonOption->runAction(RotateBy::create(0.2f, -360));
		boxOptionSp->closeBox();
	}

}
bool GameScene::checkInRoom(std::string pUserName){
	roomStruct * roomInfo = (roomStruct*)mInfoRoom;
	for (int i = roomInfo->mListPlayer->size() - 1; i >= 0; i--){
		if (roomInfo->mListPlayer->at(i).username == pUserName){
			return true;
		}
	}
	return false;
}
void GameScene::kickPlayer(std::string pUserName){
	roomStruct* pInfo = (roomStruct*)mInfoRoom;
	MpKickPlayerRequest *request = new MpKickPlayerRequest();
	request->setTokenId(GameInfo::mUserInfo.mTokenID);
	request->setUsername(pUserName);
	request->addInt(MP_PARAM_TABLE_ID, pInfo->mTableID);
	MpClientManager::getInstance(2)->sendMessage(request);
	if (mBoxInfoGame){
		mBoxInfoGame->closeBox();
	}
}
void GameScene::touchGun(int index){
	if (!layerResultUp || layerResultUp->getOpacity() == 0)
	{
		if (!mListGun.at(index)->getUserInf() && getInfoMyselft() && getInfoMyselft()->position >= 0 && getInfoMyselft()->position < 4){
			choosePosition(index);
			iTouchGun = true;
		}
		else if (mListGun.at(index)->getUserInf() && (!isStartGame || (getInfoMyselft() && (getInfoMyselft()->position < 0 || getInfoMyselft()->position > 3)))){
			mMainLoading->show();
			mBoxInfoGame->showBox(NULL, NULL);
			mBoxInfoGame->setOnTouchChangePosition(CC_CALLBACK_0(GameScene::choosePosition, this, index));
			mPlayerShow = new ClientPlayerInfoEX();
			mPlayerShow->username = mListGun.at(index)->getUserInf()->username;
			pthread_getInfo( mMainLoading);
			iTouchGun = true;
		}
	}
}

void GameScene::addFriend(std::string pUserName){
	if (mBoxInfoGame){
		mBoxInfoGame->closeBox();
	}
	WXmlReader *mXml = WXmlReader::create();
	MpMessage *pRequest = new MpMessage(MP_MSG_ADD_FRIEND);
	std::string pFriendName = pUserName;
	for (int i = 0; i < pFriendName.length(); i++) {
		pFriendName[i] = std::tolower(pFriendName[i]);
	}
	pRequest->addString(MP_PARAM_USERNAME, pFriendName.c_str());
	pRequest->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID.c_str());
	MpClientManager::getInstance(0)->sendMessage(pRequest);
	MpClientManager::getInstance(GameInfo::mUserInfo.mGameID + 1);
	std::this_thread::sleep_for(std::chrono::seconds(1));
	EGJniHelper::showToast(mXml->getNodeTextByTagName("success_addfriend").c_str());
}
void GameScene::choosePosition(int index){
	MpSwapPositionRequest* pRequest = new MpSwapPositionRequest();
	pRequest->setTokenId(GameInfo::mUserInfo.mTokenID.c_str());
	PlayerPosition playerPos;
	ClientPlayerInfoEX* player = mListGun.at(index)->getUserInf();
	if (player){
		playerPos.username = player->username;
	}
	else{
		playerPos.username = "";
	}
	playerPos.position = index;
	for (int i = 0; i < playerPos.username.length(); i++){
		playerPos.username[i] = std::tolower(playerPos.username[i]);
	}
	pRequest->setPlayerPosition(playerPos);
	pRequest->addInt(MP_PARAM_TABLE_ID, ((roomStruct*)mInfoRoom)->mTableID);
	MpClientManager::getInstance(2)->sendMessage(pRequest);

	if (mBoxInfoGame){
		mBoxInfoGame->closeBox();
	}

}

void GameScene::refresh(){
	roomStruct * roomInfo = (roomStruct*)mInfoRoom;

	if (roomInfo == NULL || roomInfo->mListPlayer == NULL)
		return;
	for (int i = mListGun.size() - 1; i >= 0; i--){
		if (i < roomInfo->pMaxPlayer){
			bool pBool = false;
			for (int j = roomInfo->mListPlayer->size() - 1; j >= 0; j--){
				if (roomInfo->mListPlayer->at(j).position == i){
					mListGun.at(roomInfo->mListPlayer->at(j).position)->visibeChangeGun(false);
					if (roomInfo->mListPlayer->at(j).position < mListGun.size()){
						if (getInfoMyselft() && roomInfo->mListPlayer->at(j).username == getInfoMyselft()->username){
							mListGun.at(roomInfo->mListPlayer->at(j).position)->visibeChangeGun(true);
						}
						
						mListGun.at(roomInfo->mListPlayer->at(j).position)->setUserInfo(&roomInfo->mListPlayer->at(j));
					}
					pBool = true;
					break;
				}
			}
			if (!pBool){
				mListGun.at(i)->setUserInfo(NULL);
			}
		}

	}
	if (roomInfo->pTypeRoom == 1 && mBoxteam1 && mBoxteam2){
		((Label*)mBoxteam1->getChildByTag(1))->setString(__String::createWithFormat("%d", mMoneyTeam1)->getCString());
		((Label*)mBoxteam1->getChildByTag(2))->setString(__String::createWithFormat("%d", pScoreTeam1)->getCString());
		((Label*)mBoxteam2->getChildByTag(1))->setString(__String::createWithFormat("%d", mMoneyTeam2)->getCString());
		((Label*)mBoxteam2->getChildByTag(2))->setString(__String::createWithFormat("%d", pScoreTeam2)->getCString());
	}
}

void GameScene::addPLayer(ClientPlayerInfoEX pPLayer){
	roomStruct * roomInfo = (roomStruct*)mInfoRoom;
	ClientPlayerInfoEX* playerUser = getPlayerByName(pPLayer.username);
	if (playerUser){
		*playerUser = pPLayer;
	}
	else{
		roomInfo->mListPlayer->push_back(pPLayer);
	}

	refresh();
}
void GameScene::fireBullet(std::string pUserName, int pTypeBullet, int pTypeGun, Vec2 pPositionFire, bool isVisible){

	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	GunPlayer* pGun = NULL;
	ClientPlayerInfoEX* player = getPlayerByName(pUserName);
	if (player){
		if (player->position < 4){
			pGun = mListGun.at(player->position);
			//pGun->setCrrentGun(pTypeGun);
			pGun->updateType();
		}
	}
	if (pGun){
		bool isShoot = true;
		if (!isVisible){
			if (pInfoRoom->mGameID == 2){
				if (pInfoRoom->pTypeRoom == 0){
					if (player){
						if (player->balance >= pTypeGun){
							player->balance -= (pTypeGun);
						}
						else{
							isShoot = false;
						}
					}
				}
				refresh();
			}
		}
		pGun->fireTo(pPositionFire);
		if (isShoot){

			Sprite* bullet = createBullet(pTypeBullet, pTypeGun, pGun->getPointFire(), pPositionFire, pUserName);
			std::string pName = pUserName;
			for (int i = 0; i < pName.length(); i++) {
				pName[i] = std::tolower(pName[i]);
			}
			if (!isVisible && GameInfo::mUserInfo.mUsername == pName){
				bullet->setOpacity(0);
			}
		}
	}


}
Sprite* GameScene::createBullet(int pTypeBullet, int pTypeGun, Vec2 pPositionStart, Vec2 pPositionFire, std::string pUserName){
	Sprite* bullet = obtainBullet(pTypeBullet, pTypeGun);
	roomStruct* pInfo = (roomStruct*)mInfoRoom;
	bullet->setZOrder(1);
	bullet->setPosition(pPositionStart);
	float pDistance = ccpDistance(pPositionStart, pPositionFire);
	bullet->setUserObject(new __String(pUserName.c_str()));
	float pSpeed = 400;
	if (pInfo->mGameID == 2){
		int pUgradeSpeed = mListInfoGun->at(pTypeGun - 1).z;
		pSpeed += (pUgradeSpeed - 1) * 30;
	}
	bullet->runAction(Sequence::createWithTwoActions(MoveTo::create(pDistance / pSpeed, pPositionFire), CallFunc::create(CC_CALLBACK_0(GameScene::checkBullet, this, bullet))));
	bullet->setRotation(EGSupport::getDirectionByRotate(pPositionStart.x, pPositionStart.y, pPositionFire.x, pPositionFire.y));
	return bullet;
}
void GameScene::checkBullet(Sprite* pBullet){
	int pType = *(int*)pBullet->getUserData();
	int pTypeFrame = (pType / 3) + 1;
	roomStruct* pInfo = (roomStruct*)mInfoRoom;
	__String * pName = (__String*)pBullet->getUserObject();
	std::string _name = pName->getCString();
	GunPlayer* gun = NULL;
	if (getPlayerByName(_name)){
		if (getPlayerByName(_name)->position < 4){
			gun = mListGun.at(getPlayerByName(_name)->position);
		}
	}
	if (!gun){
		pBullet->setVisible(false);
		pBullet->runAction(Hide::create());
		return;
	}
	std::vector<uint8_t> pListFishDead;
	std::vector<Fish*> pListFishDeadSprite;
	if (pTypeFrame > 2){
		pTypeFrame = 2;
	}
	pBullet->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_SNARE, pTypeFrame)->getCString()));
	pBullet->setScale(0.5f);


	int  pRandom = rand() % 1000;
	float pScale = 0.7f + (pType)*0.05f - 0.2f;
	pBullet->runAction(Sequence::create(EaseElasticOut::create(ScaleTo::create(0.25f, pScale + 0.2f)), FadeTo::create(0.5f, 0), CallFunc::create(CC_CALLBACK_0(Sprite::setVisible, pBullet, false)), Hide::create(), NULL));
	pBullet->setZOrder(5);
	int pUgradeGun = 0;
	if (pInfo->mGameID == 2){
		pUgradeGun = mListInfoGun->at(pType - 1).y;
	}
	if (mListFish.size() > 0){
		for (int i = mListFish.size() - 1; i >= 0; i--){
			Fish* _fish = mListFish.at(i);
			if (!_fish->isDead() && pBullet->getOpacity() == 0){
				Rect _rectBullet = Rect(pBullet->getPositionX() - pBullet->getContentSize().width*pScale / 2, pBullet->getPositionY() - pBullet->getContentSize().height*pScale / 2, pBullet->getContentSize().width*pScale, pBullet->getContentSize().height*pScale);
				if (checkRectCollide(_fish->rect(), _rectBullet, _fish->getRotation(), 0)){
					if (_fish->getType() == 0){
						pListFishDead.push_back(atoi(_fish->getId().c_str()));
					}
					int pLimit = 1000 - (GameInfo::DAME_GUN[pType - 1] + pUgradeGun + _fish->getDameGun()) * 10;
					_fish->setDameGun(_fish->getDameGun() + GameInfo::DAME_GROW_BY_GUN[pType - 1] * GameInfo::DAME_GROW_BY_FISH[_fish->getType()]);

					if (pRandom > pLimit){
						pRandom = rand() % 1000;
						int  pRandom1 = rand() % 1000;
						int pLimit = 1000 - GameInfo::FISH_RATE_DEAD[_fish->getType()] * 10 - _fish->getDameMage()*(float)pType*(GameInfo::DAME_GUN[pType - 1] - 5 + pUgradeGun - 2) / 100;
						if (pRandom1 > pLimit){
							if (pInfo->mGameID != 2){
								pListFishDead.push_back(atoi(_fish->getId().c_str()));
							}
							else{
								pListFishDeadSprite.push_back(_fish);
							}
						}
						else{
							if (_fish->getType() < 6){
								_fish->scaleSpeed(1, 2);
							}
						}
					}
					_fish->hit(GameInfo::GROW_RATE_DEAD[_fish->getType()] * GameInfo::GROW_RATE_DEAD_BY_GUN[pType - 1]);
				}
			}
		}
	}
	if (pInfo->mGameID != 2){
		if (pListFishDead.size() > 0){
			CCLOG("chet_client");
			std::string pName = _name;
			for (int i = 0; i < pName.length(); i++) {
				pName[i] = std::tolower(pName[i]);
			}
			if (pName == GameInfo::mUserInfo.mUsername){
				MpDeadFishMessageRequest * pRequest = new MpDeadFishMessageRequest();
				pRequest->setListFishes(pListFishDead);
				addComponentDefault(pRequest);
			}
		}
	}
	else{
		for (int i = pListFishDeadSprite.size() - 1; i >= 0; i--){
			Fish* pFish = pListFishDeadSprite.at(i);
			//gift(pFish->getPosition(), Vec2(CLIENT_WIDTH / 2, 0), pFish->getObject()->getMoney()*pInfo->mRatio + (float)pFish->getObject()->getMoney()*(float)pInfo->mRatio *GunInfo::getExp(getInfoMyselft()->itemId) / 100.0f, false);
			gift(pFish->getPosition(), Vec2(CLIENT_WIDTH / 2, 0), pFish->getObject()->getMoney(), false);
			playSound(SOUND_COIN);
			if (pFish->getType() > 17){
				playSound(SOUND_CATCH_MAIDEN);
			}
			pFish->dead();
			checkTaskGame(pFish->getType(), 1, 1);
			if (pFish->getType() == 18 || pFish->getType() == 17){
				showBoxShark();
			}
			else if (pFish->getType() == 19 || pFish->getType() == 20){
				showBoxMaiDen();
			}
			addFishDead(pFish->getType());
		}
		refresh();
	}

	pBullet->setUserObject(NULL);
	delete pName;
}
Sprite* GameScene::obtainBullet(int pTypeBullet, int pTypeGun){
	Sprite* bullet = NULL;
	roomStruct* pInfo = (roomStruct*)mInfoRoom;
	for (int i = mListBullet.size() - 1; i >= 0; i--){
		Sprite *pBullet = mListBullet.at(i);
		if (!pBullet->isVisible()){
			bullet = pBullet;
			break;
		}
	}
	if (!bullet){
		if (pInfo->mGameID != 2){
			bullet = Sprite::create(__String::createWithFormat(IMG_BULLET, pTypeBullet)->getCString());
		}
		else{
			bullet = Sprite::create(__String::createWithFormat(IMG_BULLET, pTypeBullet)->getCString());
		}
		mMainLayer->addChild(bullet);
		mListBullet.pushBack(bullet);
	}
	if (pInfo->mGameID != 2){
		bullet->setTexture(__String::createWithFormat(IMG_BULLET, pTypeBullet)->getCString());
	}
	else{
		bullet->setTexture((__String::createWithFormat(IMG_BULLET, pTypeBullet)->getCString()));
	}
	bullet->setOpacity(255);
	bullet->setScale(0.85f + 0.02*pTypeGun);
	bullet->runAction(Show::create());
	bullet->setVisible(true);
	if (bullet->getUserData()){
		delete bullet->getUserData();
		bullet->setUserData(NULL);
	}
	bullet->setUserData(new int(pTypeGun));
	return bullet;
}

Fish* GameScene::obtainFish(int pType){
	Fish * _fish = NULL;
	roomStruct* pInfo = (roomStruct*)mInfoRoom;
	for (int i = mListFish.size() - 1; i >= 0; i--){
		Fish* pFish = mListFish.at(i);

		if (!pFish->isVisible() && pFish->isDead()){
			_fish = pFish;
			break;
		}
	}
	if (!_fish){
		_fish = Fish::createFish(IMG_FISH_GAME);
		mMainLayer->addChild(_fish);
		mListFish.pushBack(_fish);
		_fish->setUserData(NULL);
	}
	_fish->setVisible(true);
	_fish->runAction(Show::create());
	if (_fish->getUserData()){
		std::vector<Vec2*>* pList = (std::vector<Vec2*>*)_fish->getUserData();
		for (int i = pList->size() - 1; i >= 0; i--){
			int index = pList->size() - 1 - i;
			Vec2* pVec = pList->at(index);
			delete pVec;
			pList->pop_back();
		}
		delete pList;
	}
	_fish->setTypeGame(pInfo->mGameID);
	_fish->stopAllActions();
	_fish->reset();
	_fish->setUserData(NULL);
	_fish->setType(pType);
	if (pInfo->mListPrice.size() > 0){
		_fish->getObject()->setPrice(pInfo->mListPrice.at(pType));
	}
	return _fish;
}
void GameScene::shootLocation(Vec2 pPointFire){
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;

	MpFireBulletMessageRequest* pRequest = new MpFireBulletMessageRequest();
	pRequest->setBulletPosition(MpPoint(pPointFire.x, pPointFire.y));
	if (getInfoMyselft()){
		if (mListGun.at(getInfoMyselft()->position < 4)){
			pRequest->setGunLevel(mListGun.at(getInfoMyselft()->position)->getCurrentGun());
		}
	}
	addComponentDefault(pRequest);
}
void GameScene::explore(Vec2 pPosition, int pType){
	mBackGroudRiple->addRipple(pPosition, RIPPLE_TYPE_WATER, 3.0f);
	Animation* animation = Animation::create();
	pType = 2;
	int pCountFrame = 8;
	if (pType != 1){
		pCountFrame = 6;
	}
	for (int i = 0; i < pCountFrame; i++){
		animation->addSpriteFrameWithFile(__String::createWithFormat("effect_explore%d_%d.png", pType, i + 1)->getCString());
	}
	animation->setDelayPerUnit(0.1f);
	Sprite* pEffect = Sprite::create();
	mMainLayer->addChild(pEffect);
	pEffect->setPosition(pPosition);
	pEffect->runAction(Animate::create(animation));
	pEffect->setLocalZOrder(3);
	pEffect->runAction(Sequence::create(DelayTime::create(0.5f), ScaleTo::create(1.2f, 3), CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParentAndCleanup, pEffect, true)), NULL));

	//Animation* animation1 = Animation::create();
	//for (int i = 0; i < 6; i++){
	//	animation1->addSpriteFrameWithFile(__String::createWithFormat("effect_explore2_%d.png", i + 1)->getCString());
	//}
	//animation1->setDelayPerUnit(0.1f);
	//Sprite* pEffect1 = Sprite::create();
	//mMainLayer->addChild(pEffect1);
	//pEffect1->setPosition(pPosition);
	//pEffect1->runAction(Sequence::create(Animate::create(animation1), CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParentAndCleanup, pEffect1, true)), NULL));
	mDelayEarthQuake = 0.05f;
	earthQuake(5, 80);
}
bool GameScene::ccTouchBegans(Touch* touch, Event* event){
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	if (!mRangeItem && isStartGame && mDelayShoot <= 0 && iGift10Minute == false){
		for (int i = mListGun.size() - 1; i >= 0; i--){
			GunPlayer* gun = mListGun.at(i);
			gun->open(false);
		}

		if (pInfoRoom->mGameID != 2){
			if (!iTouchGun){

				bool pShoot = false;
				ClientPlayerInfoEX* player = getInfoMyselft();
				if (pInfoRoom->pTypeRoom == 0){

					if (player &&player->position < 4 && player->balance >= mListGun.at(player->position)->getCurrentGun()*pInfoRoom->mRatio){
						pShoot = true;
					}
				}
				else{
					if (player){
						if (player->position < 2){
							if (mMoneyTeam1 >= mListGun.at(player->position)->getCurrentGun()*pInfoRoom->mRatio){
								pShoot = true;
							}
						}
						else if (player->position < 4){
							if (mMoneyTeam2 >= mListGun.at(player->position)->getCurrentGun()*pInfoRoom->mRatio){
								pShoot = true;
							}
						}
					}
				}
				if (pShoot){
					shootLocation(touch->getLocation());
					if (getInfoMyselft()){
						if (getInfoMyselft()->position < 4){
							int pTypeBullet = mListGun.at(getInfoMyselft()->position)->getCurrentGun() / 3;
							if (pTypeBullet > 2){
								pTypeBullet = 2;
							}
							fireBullet(GameInfo::mUserInfo.mUsername, pTypeBullet + 1, mListGun.at(getInfoMyselft()->position)->getCurrentGun(), touch->getLocation(), true);
						}
					}
					playSound(SOUND_SHOOT);
					mDelayPing = 0;
					GameInfo::mLayerNotice->resetTimeCount();
				}
				else{
					if (isOpenPopup){
						isOpenPopup = false;
						showBoxTopUp();
					}
					else{
						if (pInfoRoom->mGameID != 1){
							mButtonTopup->setVisible(true);
						}
					}
				}
			}
		}
		else{
			if (!mElectric->isVisible()){
				bool isShoot = true;
				if (pInfoRoom->mGameID == 2){
					if (pInfoRoom->pTypeRoom == 0){
						if (getPlayerByName(GameInfo::mUserInfo.mUsername) && getInfoMyselft()){
							if (getInfoMyselft()->position < 4 && getPlayerByName(GameInfo::mUserInfo.mUsername)->balance >= mListGun.at(getInfoMyselft()->position)->getCurrentGun()){
								//getPlayerByName(GameInfo::mUserInfo.mUsername)->balance -= (mListGun.at(getInfoMyselft()->position)->getCurrentGun());
							}
							else{
								isShoot = false;
							}
						}
					}
					refresh();
				}
				if (isShoot&&getInfoMyselft()){
					if (getInfoMyselft()->position < 4){
						int pTypeBullet = mListGun.at(getInfoMyselft()->position)->getCurrentGun() / 3;
						if (pTypeBullet > 2){
							pTypeBullet = 2;
						}
						fireBullet(GameInfo::mUserInfo.mUsername, pTypeBullet + 1, mListGun.at(getInfoMyselft()->position)->getCurrentGun(), touch->getLocation(), true);
						fireBullet(GameInfo::mUserInfo.mUsername, pTypeBullet + 1, mListGun.at(getInfoMyselft()->position)->getCurrentGun(), touch->getLocation(), false);
						checkTaskGame(1, 4, mListGun.at(getInfoMyselft()->position)->getCurrentGun());
					}

					playSound(SOUND_SHOOT);
				}
				else{
					if (!UserDefault::getInstance()->getBoolForKey("first-time-topup-off", false)){
						showBoxTopUpFirstTime();
					}
					else{
						mButtonTopup->setVisible(true);
					}

				}
			}
			else{
				laser(EGSupport::getDirectionByRotate(CLIENT_WIDTH / 2, 60, touch->getLocation().x, touch->getLocation().y));
				mListGun.at(0)->fireTo(touch->getLocation());
			}
		}

		mDelayShoot = 0.1f;
	}
	if (iGift10Minute){
		iGift10Minute = false;
	}
	if (iTouchGun){
		iTouchGun = false;
	}
	return true;
}
void GameScene::ccTouchMove(Touch* touch, Event* event){
	if (mRangeItem&& isStartGame){
		mRangeItem->setVisible(true);
		mRangeItem->setPosition(touch->getLocation());
	}
}
void GameScene::ccTouchUp(Touch* touch, Event* event){
	if (mRangeItem&& isStartGame){
		std::vector<uint8_t> pListFishEffect;
		for (int i = mListFish.size() - 1; i >= 0; i--){
			Fish* pFish = mListFish.at(i);
			if (ccpDistance(pFish->getPosition(), touch->getLocation()) <= mRangeItem->getContentSize().width / 2){
				pListFishEffect.push_back(atoi(pFish->getId().c_str()));
			}
		}
		int* pIndex = (int*)mRangeItem->getUserData();
		mRangeItem->setUserData(NULL);
		mRangeItem->removeAllChildrenWithCleanup(true);
		mRangeItem->removeFromParentAndCleanup(true);
		bool pBool = false;
		for (int i = 0; i < 3; i++){
			EGSprite* buttonSuport = (EGSprite*)mMainLayer->getChildByTag(200 + i);
			Rect pRect = Rect(buttonSuport->getPositionX() - buttonSuport->getContentSize().width / 2, buttonSuport->getPositionY() - buttonSuport->getContentSize().height / 2, buttonSuport->getContentSize().width, buttonSuport->getContentSize().height);
			if (pRect.containsPoint(touch->getLocation())){
				pBool = true;
			}
		}
		if (!pBool){
			doItemRequest(*pIndex, pListFishEffect, touch->getLocation());
		}
		delete pIndex;
		mRangeItem = NULL;

	}
}
void GameScene::gift10minutesRequest()
{
	//roomStruct* pInfo = (roomStruct*)mInfoRoom;
	//if (pInfo->mGameID != 2){
	//	MpGiftOnlineMessageRequest * request = new MpGiftOnlineMessageRequest();
	//	request->setTokenId(GameInfo::mUserInfo.mTokenID);
	//	request->addInt(MP_PARAM_TABLE_ID, ((roomStruct*)mInfoRoom)->mTableID);
	//	MpClientManager::getInstance()->sendMessage(request);
	//}
	//else{
	//	gift10minutes(600, 50);
	//}
	//iGift10Minute = true;
}
void GameScene::gift10minutes(uint16_t pTime, uint16_t pMoney){
	if (pMoney > 0 && getInfoMyselft()){
		getInfoMyselft()->balance += pMoney;
		if (getInfoMyselft()->position <4){
			gift(mTreasure->getPosition(), mListGun.at(getInfoMyselft()->position)->getPosition(), pMoney, false);
		}
	}
	mCountDownGift = pTime;
	mTreasure->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat("treasure (%d).png", 1)->getCString()));
	if (mCountDownGift > 0){
		mTreasure->setUserData(new int());
		mTreasure->unregisterTouch();
		mTimeTreasure->setVisible(true);
	}
}
void GameScene::updateGame(float pSec){

	// NEW
	if (mLaser && mLaser->isVisible()){
		checkLaser(mLaser);
	}
	if (getErrorCode() != ""){
		mErrorCode_Game_New = getErrorCode();
		setErrorCode("");
	}
	if (mErrorCode_Game_New != "") {
		mMainLoading->hide();
		if (mErrorCode_Game_New == TEXT_SUCCESS_GETINFOSMS) {
			mBoxTopUp->setPosition(Vec2(0, 0));
			mBoxTopUp->setVisible(true);
			mBoxTopUp->setLstSmsInfoTelco(lstTelcoInfo_game);
			mBoxTopUp->createTabsSmsInfo();
			/*if (mBoxTopUp->_tab == 1){
				mBoxTopUp->setLstSmsInfoTelco(lstTelcoInfo_game);
				mBoxTopUp->createTabsSmsInfo();
				mBoxTopUp->reloadData(1);
				}
				else if (mBoxTopUp->_tab == 3)
				mBoxTopUp->reloadData(3);*/
			mBoxTopUp->reloadData(1);
		}
		else if (mErrorCode_Game_New == TEXT_SUCCESS_GETINFO_RECHARGERATE) {
			++isFirstTime;
			if (isFirstTime > 1)
				mBoxTopUp->showRechargePromotion(1);
			//mBoxInfoGame->showBox(mPlayerInfoMyself, mPlayerInfoMyself);
		}
		else {
			WXmlReader* pXml = WXmlReader::create();
			pXml->load(KEY_XML_FILE);

			BoxNoticeManager::getBoxNotice(mBoxNotice,
				pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				pXml->getNodeTextByTagName(mErrorCode_Game_New.c_str()).c_str());
			mBoxNotice->setVisible(true);
		}
		mErrorCode_Game_New = "";
	}
	/*if (((roomStruct*)mInfoRoom)->mGameID == 1 && mBoxInvite->isVisible()){
		mBoxInvite->updateLoad(pSec);
	}*/

	if (mErrorCode_Game == TEXT_GETINFO_SUCCES){
		mBoxInfoGame->setOnTouchAddFriend(CC_CALLBACK_0(GameScene::addFriend, this, mPlayerShow->username));
		mBoxInfoGame->setOnTouchKickPlayer(CC_CALLBACK_0(GameScene::kickPlayer, this, mPlayerShow->username));
		mBoxInfoGame->showBox(mPlayerShow, getInfoMyselft());
		mBoxInfoGame->setType(1);
		delete mPlayerShow;
		mPlayerShow = NULL;
		mErrorCode_Game = "";
	}
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	if (pInfoRoom->mGameID == 1){
		int pCountReady = 0;
		for (int i = 0; i < pInfoRoom->mListPlayer->size(); i++){
			ClientPlayerInfoEX* playerInfo = &pInfoRoom->mListPlayer->at(i);
			if (playerInfo->isReady){
				pCountReady++;
			}
		}
		for (int i = 0; i < pInfoRoom->mListPlayer->size(); i++){
			ClientPlayerInfoEX* playerInfo = &pInfoRoom->mListPlayer->at(i);
			if (playerInfo->position < 4){
				if (!playerInfo->isReady && !isStartGame){
					playerInfo->durationReady -= pSec;
					if (playerInfo->durationReady <= 0){
						mListGun.at(playerInfo->position)->stopCount();
					}
					else{
						if (!playerInfo->host){
							mListGun.at(playerInfo->position)->countTime(playerInfo->durationReady / pInfoRoom->mDurationReadyNormal);
						}
						else{
							if (pCountReady == pInfoRoom->mListPlayer->size() - 1 && pInfoRoom->mListPlayer->size() > 1 && (pInfoRoom->pTypeRoom == 0 || pInfoRoom->mListPlayer->size() == 4)){
								mListGun.at(playerInfo->position)->countTime(playerInfo->durationReady / pInfoRoom->mDurationReadyHost);
							}
							else{
								mListGun.at(playerInfo->position)->stopCount();
								playerInfo->durationReady = pInfoRoom->mDurationReadyHost;
							}
						}
					}
				}
				else{
					mListGun.at(playerInfo->position)->stopCount();
					playerInfo->durationReady = pInfoRoom->mDurationReadyNormal;
					if (playerInfo->host){
						playerInfo->durationReady = pInfoRoom->mDurationReadyHost;
					}
				}
			}
		}
	}
	if (mTreasure->getUserData()){
		mCountDownGift -= pSec;
		int pTimeDown = mCountDownGift;
		std::string pStrMinute = __String::createWithFormat("%d", pTimeDown / 60)->getCString();
		if (pStrMinute.length() < 2){
			pStrMinute = "0" + pStrMinute;
		}
		std::string pStringSecond = __String::createWithFormat("%d", pTimeDown % 60)->getCString();
		if (pStringSecond.length() < 2){
			pStringSecond = "0" + pStringSecond;
		}
		std::string pStringTime = pStrMinute + ":" + pStringSecond;
		mTimeTreasure->setString(pStringTime.c_str());
		mTimeTreasure->setColor(ccc3(255, 255, 0));
		if (mCountDownGift < 0){
			mTreasure->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName("treasure (3).png"));
			mCountDownGift = 600;
			delete mTreasure->getUserData();
			mTreasure->setUserData(NULL);
			mTreasure->registerTouch(true);
			mTimeTreasure->setVisible(false);
		}
	}
	if (pInfoRoom->mGameID == 2){

		updateMoneyOff(pSec);
		if (isChangeMap || isBonus){
			checkChangeMap();
		}
	}
	else{
		mDelayPingBalance += pSec;
		if (mDelayPingBalance > 10)
		{
			MpMessageRequest* request = new MpMessageRequest(MP_MSG_GETBALANCE);
			request->setTokenId(GameInfo::mUserInfo.mTokenID);
			request->addInt(MP_PARAM_TABLE_ID, ((roomStruct*)mInfoRoom)->mTableID);
			MpClientManager::getInstance()->sendMessage(request);
			mDelayPingBalance = 0;
		}
	}
	if (mPointTrap){
		for (int i = 0; i < mListFish.size(); i++){
			Fish* pFish = mListFish.at(i);
			if (!pFish->isDead() && pFish->getType() < 10){
				if (ccpDistance(pFish->getPosition(), *mPointTrap) <= 200 && !pFish->isTrap()){
					pFish->trap(*mPointTrap);
					mMainLayer->runAction(Sequence::create(DelayTime::create(16), CallFunc::create(CC_CALLBACK_0(Fish::unTrap, pFish)), NULL));
				}
			}
		}
	}
	bool pShoot = false;
	ClientPlayerInfoEX* player = getInfoMyselft();
	if (pInfoRoom->pTypeRoom == 0 && player){
		if (player->position < 4){
			if (player->balance >= mListGun.at(player->position)->getCurrentGun()*pInfoRoom->mRatio){
				pShoot = true;
			}
		}
	}
	else{
		pShoot = true;
	}
	if (!pShoot){
		mDelayPing += pSec;
		if (mDelayPing >= 10){
			mDelayPing = 0;
			MpMessageRequest* request = new MpMessageRequest(MP_MSG_GETBALANCE);
			request->setTokenId(GameInfo::mUserInfo.mTokenID);
			request->addInt(MP_PARAM_TABLE_ID, ((roomStruct*)mInfoRoom)->mTableID);
			MpClientManager::getInstance()->sendMessage(request);
		}
	}
	if (mDelayEarthQuake == 0){
		for (int i = mListFish.size() - 1; i >= 0; i--){
			Fish* _fish = mListFish.at(i);
			if (!_fish->isDead()){
				FishObject* pObjectFish = _fish->getObject();
				Vec2 pVec = ccpForAngle(_fish->getRotation());
				_fish->move(pSec);
			}
		}
	}
	else{
		mDelayEarthQuake -= pSec;
		if (mDelayEarthQuake < 0){
			for (int i = mListFish.size() - 1; i >= 0; i--){
				Fish* _fish = mListFish.at(i);
				if (!_fish->isDead()){
					FishObject* pObjectFish = _fish->getObject();
					Vec2 pVec = ccpForAngle(_fish->getRotation());
					_fish->move(-mDelayEarthQuake);
				}
			}
			mDelayEarthQuake = 0;
		}
	}
	if (!isStartGame){
		if (pInfoRoom->mGameID == 1){
			buttonOption->setVisible(false);
		}
	}
	if (isStartGame){
		if (pInfoRoom->mGameID == 1){
			buttonInvite->setVisible(false);
			buttonOption->setVisible(false);
		}
		if (mDelayShoot > 0){
			mDelayShoot -= pSec;
		}
		if (mTime > 0){
			mTime -= pSec;
			if (mTime < 0){
				mTime = 0;
			}
			if (mTime >= 0){
				int pTime = mTime;
				std::string pFist = String::createWithFormat("%d", pTime / 60)->getCString();
				if (pFist.length() == 1){
					pFist = "0" + pFist;
				}
				std::string pLast = String::createWithFormat("%d", pTime % 60)->getCString();
				if (pLast.length() == 1){
					pLast = "0" + pLast;
				}
				mTimeLabel->setString(String::createWithFormat("%s:%s", pFist.c_str(), pLast.c_str())->getCString());
			}
		}

	}
	else if (!isStartGame && !getInfoMyselft()){
		if (mTime > 0){
			mTime -= pSec;
			if (mTime < 0){
				mTime = 0;
			}
			if (mTime >= 0){
				int pTime = mTime;
				std::string pFist = String::createWithFormat("%d", pTime / 60)->getCString();
				if (pFist.length() == 1){
					pFist = "0" + pFist;
				}
				std::string pLast = String::createWithFormat("%d", pTime % 60)->getCString();
				if (pLast.length() == 1){
					pLast = "0" + pLast;
				}
				mTimeLabel->setString(String::createWithFormat("%s:%s", pFist.c_str(), pLast.c_str())->getCString());
			}
		}
	}

	MpMessage * response = MpManager::getInstance()->getMessenger();
	do {
		mDelayConnect = DELAY_CONNECT;
		int index = 0;
		if (response){
			if (response->getType() == MP_MSG_GETINFO_SMS_ACK) {
				mMainLoading->hide();
				MpGetInfoPurchaseMessageResponse* pResponse = (MpGetInfoPurchaseMessageResponse*)response;
				pResponse->getVtRecharge(GameInfo::mUserInfo.mLstRechargeInfo);
				GameInfo::mUserInfo.isCanRecharge = (pResponse->getEnable() == 1);
				mBoxTopUp->setVisible(true);
				mBoxTopUp->setInfo(GameInfo::mUserInfo.mLstRechargeInfo, GameInfo::mUserInfo.isCanRecharge);
				mBoxTopUp->setInfoVISA(GameInfo::mUserInfo.mLstRechageVISAInfo);
			}
			else if (response->getType() == MP_MSG_GET_SCRATCH_CARD_INFO_ACK){
				MpMessage *msg = (MpMessage*)response;
				uint32_t pErrorCode = msg->getInt(MP_PARAM_ERROR_CODE);

				if (pErrorCode != 0) {
					mErrorCode_Game_New = __String::createWithFormat("%d", pErrorCode)->getCString();
				}
				else {
					std::string data = msg->getString(MP_PARAM_SCRATCH_CARD_INFO);
					std::vector<ScratchCard> lstitem;
					lstitem = ScratchCard::from_string(data);
					mBoxTopUp->setLstRate(lstitem);
					mErrorCode_Game_New = TEXT_SUCCESS_GETINFO_RECHARGERATE;
				}

			}
			else if (response->getType() == MP_MSG_GETINFO_PURCHASE_ACK){
				MpGetInfoPurchaseMessageResponse* pResponse = (MpGetInfoPurchaseMessageResponse*)response;

				int pErrorCode = pResponse->getErrorCode();

				if (pErrorCode != 0) {
					mErrorCode_Game_New = __String::createWithFormat("%d", pErrorCode)->getCString();
				}
				else {
					GameInfo::mUserInfo.mLstRechargeInfo.clear();
					GameInfo::mUserInfo.mLstRechageVISAInfo.clear();
					pResponse->getVtRecharge(GameInfo::mUserInfo.mLstRechargeInfo);
					pResponse->getVtRechargeIAP(GameInfo::mUserInfo.mLstRechageVISAInfo);
					GameInfo::mUserInfo.isCanRecharge = (pResponse->getEnable() == 1);
					mBoxTopUp->setLstRechargeSMS(GameInfo::mUserInfo.mLstRechargeInfo);
					mBoxTopUp->setLstRechargeVISA(GameInfo::mUserInfo.mLstRechageVISAInfo);

					std::string strTelcoArr = pResponse->getString(MP_PARAM_MAX_TELCO_BALANCE);
					StructTelcoInfo telcoInfo;
					size_t len = 0;
					lstTelcoInfo_game.clear();
					while (len < strTelcoArr.size())
					{
						telcoInfo.telcoName = "";
						while (strTelcoArr[len] != 0)
						{
							telcoInfo.telcoName += strTelcoArr[len];
							len++;
						}
						len++;

						telcoInfo.maxBalance = *((uint32_t*)(strTelcoArr.data() + len));
						telcoInfo.maxBalance = ntohl(telcoInfo.maxBalance);
						len += 4;

						lstTelcoInfo_game.push_back(telcoInfo);
					}

					mErrorCode_Game_New = TEXT_SUCCESS_GETINFOSMS;
				}


			}
			else if (response->getType() == MP_MSG_RECHARGE){
				MpRechargeByCardMessageResponse* pResponse = (MpRechargeByCardMessageResponse*)response;
				int pErrcode = pResponse->getErrorCode();
				if (pErrcode != 0) {
					mErrorCode_Game_New = TEXT_ERR_USERNAME_NOTEXIST;
				}
				else{
					mErrorCode_Game_New = "txt_recharge_success_mainscene";
				}
				mMainLoading->hide();

			}
			else if (response->getType() == MP_MSG_GET_ACC_INFO_ACK)
			{
				mMainLoading->hide();
				MpGetInfoPlayerGameResponse  *pResponse = (MpGetInfoPlayerGameResponse*)response;
				unsigned pErrorCode = pResponse->getErrorCode();
				if (pErrorCode != 0){
					mErrorCode_Game = __String::createWithFormat("%d", pErrorCode)->getCString();
				}
				else{
					pResponse->getPlayerInfo(*mPlayerShow);

					mErrorCode_Game = TEXT_GETINFO_SUCCES;
				}
			}
			else if (response->getType() == MP_MSG_GETBALANCE_ACK) 
			{
				if (getInfoMyselft() && (pInfoRoom->mGameID != 1))
				{
					//pResponse->getBalance();
					getInfoMyselft()->balance = response->getInt(MP_PARAM_BALANCE);
					if (pInfoRoom->mGameID == 0)
					{
						GameInfo::mUserInfo.mBalance = getInfoMyselft()->balance;
					}
				}
				refresh();
				mButtonTopup->setVisible(false);
			}
			else if (response->getType() == MP_MSG_CHANGE_XFACTOR_NTFY){
				std::string pUserName = response->getString(MP_PARAM_USERNAME);
				ClientPlayerInfoEX * player = getPlayerByName(pUserName);
				mLoadDingWaitFish->setVisible(false);
				mLoadDingWaitFish->pauseSchedulerAndActions();
				if (player){
					player->indexX = response->getInt(MP_PARAM_XFACTOR);

					if (getInfoMyselft() == player){
						for (size_t i = 0; i < mListFish.size(); i++){
							if (mListFish.at(i)->isVisible() && !mListFish.at(i)->isDead()){
								mListFish.at(i)->setDame(mListFish.at(i)->getDame()*mListGun.at(player->position)->mXFactor / player->indexX);
							}
						}
					}
					mListGun.at(player->position)->mXFactor = player->indexX;
					mListGun.at(player->position)->setUserInfo(player);
				}
			}
			/*else if (response->getType() == MP_MSG_GIFT_ONLINE_NTFY){
				MpGiftOnlineMessageResponse* pResponse = (MpGiftOnlineMessageResponse*)response;
				int pTime = pResponse->getTime();
				int pMoney = pResponse->getGiftMoney();
				gift10minutes(pTime, pMoney);
				mTreasure->unregisterTouch();
			}*/
			else if (response->getType() == MP_MSG_KICK_PLAYER_NTFY){

				MpKickPlayerNotify* pResponse = (MpKickPlayerNotify*)response;
				uint32_t pErrorCode = pResponse->getErrorCode();
				std::string pUserName;
				pUserName = pResponse->getUsername();
				if (pErrorCode == 0){
					ClientPlayerInfoEX * player = getPlayerByName(pUserName);
					if (player){
						for (int i = 0; i < pInfoRoom->mListPlayer->size(); i++){
							if (&pInfoRoom->mListPlayer->at(i) == player){
								pInfoRoom->mListPlayer->erase(pInfoRoom->mListPlayer->begin() + i);
							}
						}
					}
					reStart();
					refresh();
				}
				else{
					if (pErrorCode == MP_PARAM_ITEM_ID){
						WXmlReader* pXml = WXmlReader::create();
						if (getInfoMyselft() && getInfoMyselft()->host){
							showToast(String::createWithFormat(pXml->getNodeTextByTagName("denied_kick_request").c_str(), pUserName.c_str())->getCString(), 600, Vec2(400, 160));
						}
						else{
							showToast(pXml->getNodeTextByTagName("protected_kick_request").c_str(), 600, Vec2(400, 160));
						}
					}
				}
			}
			else if (response->getType() == MP_MSG_SWAP_POSITION_CNF){
				MpSwapPositionConfirm* pResponse = (MpSwapPositionConfirm*)response;
				if (pResponse->getErrorCode() != 0){

					WXmlReader* pXml = WXmlReader::create();
					pXml->load(KEY_XML_FILE);
					BoxNoticeManager::getBoxNotice(mBoxNotice,
						pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
						pXml->getNodeTextByTagName("response_swap_position").c_str());
					mBoxNotice->setVisible(true);
				}
			}
			else if (response->getType() == MP_MSG_SWAP_POSITION_NTFY){
				MpSwapPositionNotify* pResponse = (MpSwapPositionNotify*)response;
				std::vector<PlayerPosition> pListPlayer;
				pResponse->getListPlayerPosition(pListPlayer);
				for (int i = 0; i < pListPlayer.size(); i++){
					PlayerPosition  player = pListPlayer.at(i);
					if (getPlayerByName(player.username)){
						getPlayerByName(player.username)->position = player.position;
					}
				}
				reStart();
				refresh();
			}
			else if (response->getType() == MP_MSG_SWAP_POSITION_IND){
				if (!isStartGame){
					MpSwapPositionIndication* pResponse = (MpSwapPositionIndication*)response;
					WXmlReader* pXml = WXmlReader::create();
					pXml->load(KEY_XML_FILE);
					std::vector<PlayerPosition> pListPosition;
					pResponse->getListPlayerPosition(pListPosition);

					ClientPlayerInfoEX* player = NULL;
					player = getPlayerByName(pListPosition.at(0).username);
					if (player && player->position == pListPosition.at(0).position && getInfoMyselft() && getInfoMyselft()->position == pListPosition.at(1).position){
						BoxNoticeManager::getBoxConfirm(mBoxNotice,
							pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
							__String::createWithFormat(
							pXml->getNodeTextByTagName("ask_swap_position").c_str(), player->username.c_str())->getCString(),
							[=] {
							MpSwapPositionResponse *request = new MpSwapPositionResponse();
							request->setListPlayerPosition(pListPosition);
							request->setTokenId(GameInfo::mUserInfo.mTokenID.c_str());
							request->addInt(MP_PARAM_TABLE_ID, ((roomStruct*)mInfoRoom)->mTableID);
							MpClientManager::getInstance(2)->sendMessage(request);
							mBoxNotice->setVisible(false);
						}, [=] {
							MpSwapPositionResponse *request = new MpSwapPositionResponse();
							request->setTokenId(GameInfo::mUserInfo.mTokenID.c_str());
							request->addInt(MP_PARAM_TABLE_ID, ((roomStruct*)mInfoRoom)->mTableID);
							MpClientManager::getInstance(2)->sendMessage(request);
							mBoxNotice->setVisible(false);
						});
						mBoxNotice->setVisible(true);
					}
					else{
						MpSwapPositionResponse *request = new MpSwapPositionResponse();
						request->setTokenId(GameInfo::mUserInfo.mTokenID.c_str());
						request->addInt(MP_PARAM_TABLE_ID, ((roomStruct*)mInfoRoom)->mTableID);
						MpClientManager::getInstance(2)->sendMessage(request);
						mBoxNotice->setVisible(false);
					}
				}

			}
			else if (response->getType() == MP_PLAYER_READY_NTFY){
				MpPlayerReadyMessageNotify * pResponse = (MpPlayerReadyMessageNotify*)response;
				std::string pUserName;
				pResponse->getUsername(pUserName);
				ClientPlayerInfoEX *player = getPlayerByName(pUserName);
				if (player){
					if (!player->host){
						if (player->position < 4){
							GunPlayer* gun = mListGun.at(player->position);
							gun->ready();
						}
						player->isReady = true;
					}
					else{
						startGame();
					}
				}
				reStart();
			}
			else if (response->getType() == MP_JOIN_TABLE_NTFY){
				MpJoinTableMessageNotify * pResponse = (MpJoinTableMessageNotify*)response;
				std::vector<ClientPlayerInfo> pListPlayer;
				pResponse->getListPlayer(pListPlayer);
				ClientPlayerInfo pInfo = pListPlayer.at(0);
				if (pInfo.itemId == 0){
					pInfo.itemId = 101;
				}
				ClientPlayerInfoEX playerInfo = ClientPlayerInfoEX(pInfo.username.c_str(), pInfo.level, pInfo.balance, pInfo.position, pInfo.disallowKick, pInfo.itemId, pInfo.host, false, 7);
				if (pInfoRoom->mGameID == 1){
					playerInfo.balance = mFireMoney;
					playerInfo.balanceDefault = mFireMoney;
				}
				playerInfo.indexX = 1;
				playerInfo.durationReady = pInfoRoom->mDurationReadyNormal;
				addPLayer(playerInfo);
				if (pInfoRoom->mGameID == 1){
					updateSetUp();
				}
				reStart();
				refresh();
			}
			else if (response->getType() == MP_MSG_CHAT_TEXT){
				MpChatMessageRequest * pResponse = (MpChatMessageRequest*)response;
				if (pResponse->getUsername() != GameInfo::mUserInfo.mFullname){
					boxChat->updateChat(pResponse->getUsername(), pResponse->getContent());
				}
				ClientPlayerInfoEX* player = getPlayerByName(pResponse->getUsername());
				if (player){
					if (player->position < 4){
						GunPlayer* gun = mListGun.at(player->position);
						if (gun){
							gun->showChatText(pResponse->getContent());
						}
					}
				}
			}
			else if (response->getType() == MP_MSG_CHAT_EMOTION){
				MpChatEmotionMessageRequest * pResponse = (MpChatEmotionMessageRequest*)response;
				if (pResponse->getUsername() != GameInfo::mUserInfo.mFullname){
					boxChat->updateChatWithEmotion(pResponse->getUsername(), pResponse->getEmotionType(), pResponse->getEmotionID());
				}
				ClientPlayerInfoEX* player = getPlayerByName(pResponse->getUsername());
				if (player){
					if (player->position < 4){
						GunPlayer* gun = mListGun.at(player->position);
						if (gun){
							gun->showChatEmo(pResponse->getEmotionID());
						}
					}
				}
			}
			else if (response->getType() == MP_END_GAME_NTFY){
				MpEndGameMessageNotify * pResponse = (MpEndGameMessageNotify*)response;
				Node* pNode = this->getChildByTag(999);
				if (pNode){
					pNode->removeAllChildrenWithCleanup(true);
					pNode->removeFromParentAndCleanup(true);
				}
				mTime = pInfoRoom->mDuration;
				for (int i = 0; i < pInfoRoom->mListPlayer->size(); i++){
					pInfoRoom->mListPlayer->at(i).isReady = false;
				}
				std::vector<GamePlayerResult> rank;
				pResponse->getGameResult(rank);
				stopAllActions();
				for (int i = mListFish.size() - 1; i >= 0; i--){
					mListFish.at(i)->destroy();
				}
				for (int i = mListGun.size() - 1; i >= 0; i--){
					GunPlayer* gun = mListGun.at(i);
					gun->startGame(false);
				}
				int pResult = 0;
				for (int i = 0; i < rank.size(); i++){
					GamePlayerResult pWinner = rank.at(i);
					std::string pName = pWinner.Username;
					for (int i = 0; i < pName.length(); i++) {
						pName[i] = std::tolower(pName[i]);
					}
					if (GameInfo::mUserInfo.mUsername == pName){
						if (pWinner.WinMoney >0){
							pResult = 1;
						}
						break;
					}
				}
				isStartGame = false;
				createBoxResult(rank, pInfoRoom->pTypeRoom, pResult);
				pScoreTeam1 = 0;
				pScoreTeam2 = 0;
				//mBoxInvite->refresh();
				buttonOption->setVisible(false);
			}
			else if (response->getType() == MP_LEFT_TABLE_NTFY){
				MpLeftTableMessageNotify * pResponse = (MpLeftTableMessageNotify*)response;
				std::string pUserName;
				pResponse->getUsername(pUserName);
				std::string pName = GameInfo::mUserInfo.mUsername;
				for (int i = 0; i < pName.length(); i++){
					pName[i] = std::tolower(pName[i]);
				}
				if (pUserName == pName){
					CCLOG("out game");
					std::string pReason;
					WXmlReader* pXml = WXmlReader::create();
					pXml->load(KEY_XML_FILE);
					stopAllActions();
					unscheduleAllSelectors();
					uint32_t errorCode = pResponse->getInt(MP_PARAM_ERROR_CODE);
					if (errorCode != 0){
						schedule(schedule_selector(GameScene::updateOutGame));
						mDelayOutGame = 10;
						pReason = pXml->getNodeTextByTagName(__String::createWithFormat("%d", (int)errorCode)->getCString()).c_str();
						BoxNoticeManager::getBoxNotice(mBoxNotice,
							pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
							pReason.c_str(),
							[=] {
							leftGame();
						});
					}
					else {
						if (pInfoRoom->pTypeRoom == 1){
							pReason = pXml->getNodeTextByTagName("error_double_login").c_str();
							BoxNoticeManager::getBoxNotice(mBoxNotice,
								pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
								pReason.c_str(), nullptr,
								[=] {
								outGame();
							});
						}
						else{
							pReason = pXml->getNodeTextByTagName("error_double_login").c_str();
							BoxNoticeManager::getBoxNotice(mBoxNotice,
								pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
								pReason.c_str(), nullptr,
								[=] {
								outGame();
							});
						}
					}


					mBoxNotice->setVisible(true);
				}

				for (int i = pInfoRoom->mListPlayer->size() - 1; i >= 0; i--){
					std::string pName = pInfoRoom->mListPlayer->at(i).username;
					for (int i = 0; i < pName.length(); i++){
						pName[i] = std::tolower(pName[i]);
					}
					if (pName == pUserName){
						if (pInfoRoom->mListPlayer->at(i).host){
							for (int j = pInfoRoom->mListPlayer->size() - 1; j >= 0; j--){
								pInfoRoom->mListPlayer->at(j).isReady = false;
							}
						}
						if ((getInfoMyselft() && pInfoRoom->mListPlayer->at(i).username != getInfoMyselft()->username) || !getInfoMyselft()){
							pInfoRoom->mListPlayer->erase(pInfoRoom->mListPlayer->begin() + i);
						}
						break;
					}
				}

				if (pInfoRoom->mGameID == 1){
					std::string pNameHost;
					if (pResponse->getHostname(pNameHost)){
						if (getPlayerByName(pNameHost)){
							getPlayerByName(pNameHost)->host = true;
							getPlayerByName(pNameHost)->durationReady = pInfoRoom->mDurationReadyHost;
						}
					}
				}
				reStart();
				refresh();
			}
			else if (response->getType() == MP_MSG_FIRE_BULLET_NTFY){
				MpFireBulletMessageNotify * pResponse = (MpFireBulletMessageNotify*)response;
				std::string pUserName;
				uint8_t pGunLevel;
				uint32_t pMoney;
				MpPoint pPoint = MpPoint(0, 0);
				pResponse->getUsername(pUserName);
				pResponse->getGunLevel(pGunLevel);
				pResponse->getBulletPosition(pPoint);
				pResponse->getFireMoney(pMoney);
				if (getPlayerByName(pUserName)){
					if (pInfoRoom->pTypeRoom == 0){
						getPlayerByName(pUserName)->balance = pMoney;
					}
					else{
						if (getPlayerByName(pUserName)->position < 2){
							mMoneyTeam1 = pMoney;
						}
						else if (getPlayerByName(pUserName)->position < 4){
							mMoneyTeam2 = pMoney;
						}
					}
					mListGun.at(getPlayerByName(pUserName)->position)->setCrrentGun(pGunLevel);
					/*roomStruct * roomInfo = (roomStruct*)mInfoRoom;
					std::vector<ClientPlayerInfoEX>* mListPlayer = roomInfo->mListPlayer;
					for (uint8_t i = 0; i < mListPlayer->size(); ++i){		
						if (!pUserName.compare(mListPlayer->at(i).username)){
							roomInfo->mListPlayer->at(i).gunType = pGunLevel;
							break;
						}
					}*/
				}
				int pTypeBullet = pGunLevel / 3;
				if (pTypeBullet > 2){
					pTypeBullet = 2;
				}
				fireBullet(pUserName, pTypeBullet + 1, pGunLevel, Vec2(pPoint.x, pPoint.y), false);
				refresh();
			}
			else if (response->getType() == MP_USE_ITEM_NTFY){
				int a = 1;
				/*MpUseItemMessageNotify * pResponse = (MpUseItemMessageNotify*)response;
				std::string pUserName;
				uint8_t pGunLevel;
				std::vector<uint8_t> pListfish;
				MpPoint pPoint = MpPoint(0, 0);
				pResponse->getUsername(pUserName);
				pResponse->getItemId(pGunLevel);
				pResponse->getPosition(pPoint);
				pResponse->getListFishes(pListfish);
				doItem(pUserName, pGunLevel - 200, pListfish, Vec2(pPoint.x, pPoint.y));
				if (pGunLevel == 200){
					MpDeadFishMessageRequest* requestDeadFish = new MpDeadFishMessageRequest();
					requestDeadFish->setListFishes(pListfish);
					addComponentDefault(requestDeadFish);
				}*/
			}
			else if (response->getType() == MP_LISH_FISH_NTFY){
				mLoadDingWaitFish->setVisible(false);
				mLoadDingWaitFish->pauseSchedulerAndActions();
				MpListFishesNotify * pResponse = (MpListFishesNotify*)response;
				std::string  strListFish;
				strListFish = pResponse->getString(MP_PARAM_LIST_FISH);
				std::string strListfishSpecial;
				strListfishSpecial = pResponse->getString(MP_PARAM_LIST_SPECIAL_FISH);

				std::vector<int> listIdFishSpecial;
				for (int i = 0; i < strListfishSpecial.length(); i++){
					listIdFishSpecial.push_back((uint8_t)strListfishSpecial[i]);
				}
				std::vector<ClientFishInfo> pListFish = ClientFishInfo::from_string(strListFish);
				float pDelay = 0;
				int oldType = 0;
				int pType = pListFish.at(pListFish.size() - 1).fishType;
				for (int i = pListFish.size() - 1; i >= 0; i--){
					ClientFishInfo* pFishInfo = new ClientFishInfo(pListFish.at(i));
					bool iExist = false;
					for (size_t j = 0; j < listIdFishSpecial.size(); j++){
						if (listIdFishSpecial[j] == pFishInfo->fishId){
							iExist = true;
							break;
						}
					}
					if (iExist){
						pFishInfo->isSpecial = true;
					}
					pDelay = pFishInfo->time;
					pDelay = pDelay / 10;
					if (pFishInfo->fishType == 0){
						pDelay += 2 + (rand() % 10)*0.1f;
					}
					/*	if (pFishInfo->fishType != oldType){
							float delay = pFishInfo->time;
							delay = delay / 10;
							pDelay += delay;
							oldType = pFishInfo->fishType;
							}
							if (oldType == 0){
							pDelay = 0;
							}*/
					if (pFishInfo->health > 0){
						pFishInfo->health += 150;
					}
					CallFunc *call = CallFunc::create(CC_CALLBACK_0(GameScene::createFish, this, pFishInfo));
					runAction(Sequence::createWithTwoActions(DelayTime::create(pDelay), call));
				}
			}
			else if (response->getType() == MP_MSG_GET_ACC_INFO_ACK){
				MpGetInfoPlayerGameResponse  *pResponse = (MpGetInfoPlayerGameResponse*)response;

				unsigned pErrorCode = pResponse->getErrorCode();
				if (pErrorCode != 0){
					mErrorCode_Game = __String::createWithFormat("%d", pErrorCode)->getCString();

				}
				else{
					pResponse->getPlayerInfo(*mPlayerShow);
					mErrorCode_Game = TEXT_GETINFO_SUCCES;
				}
				mMainLoading->hide();
			}
			else if (response->getType() == MP_DEAD_FISH_NTFY){
				CCLOG("chet_sever");
				MpDeadFishMessageNotify * pResponse = (MpDeadFishMessageNotify*)response;
				std::vector<uint8_t> pListFish;
				pResponse->getListFish(pListFish);
				std::string pUserName;
				uint32_t pMoney;
				uint32_t pScore;
				pResponse->getUsername(pUserName);
				pScore = pResponse->getInt(MP_PARAM_FISH_MONEY);
				uint32_t moneySpecialFish = 0;
				moneySpecialFish = pResponse->getInt(MP_PARAM_SPECIAL_FISH_MONEY);
				if (getPlayerByName(pUserName)){
					if (moneySpecialFish){
						showBonusSpecial(getPlayerByName(pUserName)->position, moneySpecialFish);
					}
				}
				if (pInfoRoom->pTypeRoom == 0){
					if (pScore > 0){
						pInfoRoom->mListPrice.at(0) = pScore;
					}
				}
				ClientPlayerInfoEX *player = getPlayerByName(pUserName);
				if (!player)
					return;
				for (int i = pListFish.size() - 1; i >= 0; i--){
					uint8_t pID = pListFish.at(i);
					for (int j = mListFish.size() - 1; j >= 0; j--){
						Fish* pFish = mListFish.at(j);
						if (atoi(pFish->getId().c_str()) == pID){
							if (!pFish->isDead()){
								ClientPlayerInfoEX* myselft = getInfoMyselft();
								if (pFish->getType() == 0){
									pFish->getObject()->setPrice(pInfoRoom->mListPrice.at(0));
								}
								if (myselft){
									player->balance += pFish->getObject()->getMoney()*myselft->indexX;
									player->score += pFish->getObject()->getMoney();
									Vec2 pPositionFishDead = pFish->getPosition();

									std::string pName = myselft->username;
									for (int i = 0; i < pName.length(); i++){
										pName[i] = std::tolower(pName[i]);
									}
									if (player->position < 4){
										if (pUserName == pName){
											if (pFish->getType() > 17){
												playSound(SOUND_CATCH_MAIDEN);
											}
											float pBonus = (float)pFish->getObject()->getMoney() *GunInfo::getExp(player->itemId) / 100.0f;
											if (pInfoRoom->mGameID == 1){
												pBonus = 0;
											}
											if (!pFish->getObject()->isSpecial()){
												gift(pPositionFishDead, mListGun.at(player->position)->getPosition(), pFish->getObject()->getMoney()*myselft->indexX + pBonus, false);
											}
											else{
												gift(pPositionFishDead, mListGun.at(player->position)->getPosition(), pFish->getObject()->getMoney()*myselft->indexX + pBonus, true);
											}
										}
										else{
											gift(pPositionFishDead, mListGun.at(player->position)->getPosition(), pFish->getObject()->getMoney() + (float)pFish->getObject()->getMoney() *GunInfo::getExp(player->itemId) / 100.0f, true);
										}
									}
								}
								pFish->dead();

								if (pFish->getObject()->isSpecial()){
									if (pFish->getType() < 19){
										explore(pFish->getPosition(), 1);
									}
									else{
										explore(pFish->getPosition(), 2);
									}
									pFish->stopAllActions();
									pFish->getBody()->stopAllActions();
									pFish->destroy();
								}
							}
						}
					}
				}
				ClientPlayerInfoEX* myselft = getInfoMyselft();
				if (myselft && pUserName == myselft->username){
					playSound(SOUND_COIN);
				}

				pResponse->getFireMoney(pMoney);
				player->score = pScore;
				player->balance = pMoney;
				if (pInfoRoom->pTypeRoom == 1) {
					for (int i = 0; i < pInfoRoom->mListPlayer->size(); i++){
						if (pInfoRoom->mListPlayer->at(i).position / 2 == player->position / 2){
							pInfoRoom->mListPlayer->at(i).balance = pMoney;
							pInfoRoom->mListPlayer->at(i).score = pScore;
						}
					}
					if (player->position < 2){
						pScoreTeam1 = pScore;
						mMoneyTeam1 = pMoney;
					}
					else{
						pScoreTeam2 = pScore;
						mMoneyTeam2 = pMoney;
					}
				}

				refresh();
			}
			/*else if (response->getType() == MP_SERVER_NOTICE) {
				MpServerNoticeMessageNotify* msResponse = (MpServerNoticeMessageNotify*)response;
				std::string strNotice = msResponse->getNotice();
				if (strNotice != "")
					showToast(strNotice);
			}*/
			else if (response->getType() == MP_SETUP_GAME_NTFY)
			{
			/*	MpSetupGameMessageNotify * pResponse = (MpSetupGameMessageNotify*)response;
				uint32_t indexBet;
				uint8_t gameType;
				uint16_t remainTime;
				pResponse->getFireMoney(mFireMoney);
				pResponse->getBetMoney(indexBet);
				pResponse->getGameType(gameType);
				pInfoRoom->pBetMoney = indexBet;
				pInfoRoom->pTypeRoom = gameType;
				mChooseType = gameType;
				pResponse->getRemainTime(remainTime);
				WXmlReader* pXml = WXmlReader::create();
				std::string pStrMoney = __String::createWithFormat("%d", (int)indexBet)->getCString();
				mTime = remainTime;
				if (remainTime < pInfoRoom->mDuration && !getInfoMyselft()){
					isStartGame = false;
					mButtonStartReady->setVisible(false);
					showToastReal(pXml->getNodeTextByTagName("waiting_play").c_str(), 600, Vec2(400, 160), remainTime);
				}
				else{
					if (remainTime < pInfoRoom->mDuration){
						isStartGame = true;
						mButtonStartReady->setVisible(false);

					}
					showToast(__String::createWithFormat(pXml->getNodeTextByTagName("notice_bet_money").c_str(), EGSupport::addDotMoney(pStrMoney).c_str())->getCString(), 600, Vec2(400, 160));
				}
				for (int i = mListGun.size() - 1; i >= 0; i--){
					mListGun.at(i)->startGame(isStartGame);
				}
				isDirty = true;
				updateSetUp();
				reStart();
				refresh();
				isFirsTimeSetUp = false;*/
			}

			//mListRepose->erase(mListRepose->begin() + index);
		}
		if (response){
			delete response;
			response = NULL;
		}
		response = MpManager::getInstance()->getMessenger();
	} while (response);
	//mDelayConnect -= pSec;
	//if (mDelayConnect <= 0){
	//	if (pInfoRoom->mGameID != 2){
	//		/*WXmlReader* pXml = WXmlReader::create();
	//		pXml->load(KEY_XML_FILE);
	//		BoxNoticeManager::getBoxNotice(mBoxNotice,
	//		pXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
	//		pXml->getNodeTextByTagName("content_lostconnect").c_str(),
	//		[=] {
	//		outGame();
	//		});
	//		mBoxNotice->setVisible(true);*/
	//	}
	//}
}
void GameScene::setRemainTime(float remainTime){
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	WXmlReader* pXml = WXmlReader::create();
	mTime = remainTime;
	if (remainTime < pInfoRoom->mDuration && !getInfoMyselft()){
		isStartGame = false;
		mButtonStartReady->setVisible(false);
		showToastReal(pXml->getNodeTextByTagName("waiting_play").c_str(), 600, Vec2(400, 160), remainTime);
	}
	else{
		if (remainTime < pInfoRoom->mDuration){
			isStartGame = true;
			mButtonStartReady->setVisible(false);

		}
	}
	for (int i = mListGun.size() - 1; i >= 0; i--){
		mListGun.at(i)->startGame(isStartGame);
	}
	isDirty = true;
	//updateSetUp();
	reStart();
	refresh();
	isFirsTimeSetUp = false;
}
void GameScene::updateOutGame(float pSec){
	if (mDelayOutGame < 11 && mDelayOutGame>0){
		mDelayOutGame -= pSec;
	}
}
void GameScene::updateSetUp(){
	WXmlReader* xml = WXmlReader::create();
	xml->load(KEY_XML_FILE);
	roomStruct* pInfo = (roomStruct*)mInfoRoom;
	for (int i = 0; i < pInfo->mListPlayer->size(); i++){
		ClientPlayerInfoEX* player = &pInfo->mListPlayer->at(i);
		player->balanceDefault = mFireMoney;
	}
	Sprite* box = (Sprite*)mBoxCreateRoom->getChildByTag(1);
	EditBox *editText = (EditBox*)box->getChildByTag(10);
	std::string pBetMoney = String::createWithFormat("%d", pInfo->pBetMoney)->getCString();
	editText->setText(EGSupport::addDotMoney(pBetMoney).c_str());
	for (int i = 0; i < 3; i++){
		Sprite* pStroke = (Sprite*)box->getChildByTag(1 + i);
		pStroke->removeAllChildrenWithCleanup(true);
		if (i < pInfo->mListBetMoney->size()){
			if (pInfo->mListBetMoney->at(i) == pInfo->pBetMoney){
				Sprite* pCheck = Sprite::createWithSpriteFrameName("check-box-bet.png");
				pStroke->addChild(pCheck);
				pCheck->setPosition(Vec2(pStroke->getContentSize().width / 2, pStroke->getContentSize().height / 2));
			}
		}
	}
	for (int i = 0; i < 2; i++){
		EGButtonSprite* button = (EGButtonSprite*)box->getChildByTag(4 + i);
		std::string pFrame = __String::createWithFormat(IMG_BUTTON_CHOOSE_SOLO, 1)->getCString();
		if (i == 1){
			pFrame = __String::createWithFormat(IMG_BUTTON_CHOOSE_TEAM, 1)->getCString();
		}
		button->setSpriteFrame(pFrame.c_str());
		button->removeAllChildrenWithCleanup(true);
	}
	EGButtonSprite* button = (EGButtonSprite*)box->getChildByTag(4 + pInfo->pTypeRoom);
	std::string pFrame = __String::createWithFormat(IMG_BUTTON_CHOOSE_SOLO, 2)->getCString();
	if (pInfo->pTypeRoom == 1){
		pFrame = __String::createWithFormat(IMG_BUTTON_CHOOSE_TEAM, 2)->getCString();
	}
	button->setSpriteFrame(pFrame.c_str());
	Sprite* pCheck = Sprite::createWithSpriteFrameName("check-choose-game.png");
	button->addChild(pCheck);
	pCheck->setPosition(Vec2(button->getContentSize().width - 10, button->getContentSize().height / 2));
	changeTypeRoom(pInfo->pTypeRoom, pInfo->pBetMoney);
}

ClientPlayerInfoEX* GameScene::getPlayerByName(std::string pName){
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	for (int i = pInfoRoom->mListPlayer->size() - 1; i >= 0; i--){
		ClientPlayerInfoEX *player = &pInfoRoom->mListPlayer->at(i);
		std::string pNameString = player->username;
		for (int i = 0; i < pNameString.length(); i++){
			pNameString[i] = std::tolower(pNameString[i]);
		}
		for (int i = 0; i < pName.length(); i++){
			pName[i] = std::tolower(pName[i]);
		}
		/*if (pNameString == pName){
			return player;
		}*/
		if (endsWith(pNameString, pName))
			return player;
	}
	return NULL;
}

bool GameScene::endsWith(std::string str, std::string suffix)
{
	if (str == suffix) return true;
	if (str.length() < suffix.length())
		return false;

	return str.substr(str.length() - suffix.length()) == suffix;
}

void GameScene::gift(Vec2 pSartPoint, Vec2 pEndPoint, float pMoneyF, bool pHide){
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;

	Label *label;
	if (pMoneyF > 0){
		unsigned int pMoney = pMoneyF;
		label = Label::createWithBMFont(FONT_BONUS_SCORE, __String::createWithFormat("+ %d", pMoney)->getCString());
	}
	else{
		unsigned int pMoney = pMoneyF;
		label = Label::createWithBMFont(FONT_BONUS_SCORE, __String::createWithFormat("- %d", pMoney)->getCString());
	}
	this->addChild(label);
	label->setPosition(pSartPoint);
	label->runAction(Sequence::create(MoveBy::create(1.5f, Vec2(0, 50)), CallFunc::create(CC_CALLBACK_0(Label::removeAllChildrenWithCleanup, label, true)), CallFunc::create(CC_CALLBACK_0(Label::removeFromParentAndCleanup, label, true)), NULL));

	const char* pFrame = __String::createWithFormat(IMG_COIN_INGAME, 1)->getCString();
	Vec2 pDestiny = pEndPoint;

	EGSprite *pCoin = EGSprite::createWithSpriteFrameName(pFrame);
	Animation* pArrayAnimation = Animation::create();
	for (int i = 0; i < 5; i++){
		pArrayAnimation->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_COIN_INGAME, i + 1)->getCString()));
	}
	ParticleFlower *mFlower = ParticleFlower::create();
	mFlower->setEmissionRate(20);
	mFlower->setStartColor(ccc4f(1, 1, 0, 1));
	mFlower->setStartColorVar(ccc4f(1, 1, 0, 1));
	mFlower->setEndColor(ccc4f(1, 1, 0, 1));
	mFlower->setEndColorVar(ccc4f(1, 1, 0, 1));
	mFlower->setLifeVar(0.5f);
	mFlower->setLife(0.5f);
	mFlower->setTexture(TextureCache::sharedTextureCache()->addImage(IMG_STAR));
	this->addChild(mFlower);

	float pDuration = ccpDistance(pSartPoint, pDestiny) / 200.0f;
	mFlower->runAction(Sequence::create(MoveTo::create(pDuration, pDestiny), DelayTime::create(0.5f), Hide::create(), CallFunc::create(CC_CALLBACK_0(ParticleFlower::removeAllChildrenWithCleanup, mFlower, true)), CallFunc::create(CC_CALLBACK_0(ParticleFlower::removeFromParentAndCleanup, mFlower, true)), NULL));
	mFlower->setStartColor(ccc4f(0, 0, 0, 1));
	mFlower->setPosition(pSartPoint);
	mFlower->setZOrder(1);
	pCoin->runAction(RepeatForever::create(Animate::create(pArrayAnimation)));
	if (pInfoRoom->mGameID == 2){
		pCoin->runAction(Sequence::create(MoveTo::create(pDuration, pDestiny), CallFunc::create(CC_CALLBACK_0(GameScene::refresh, this)), CallFunc::create(CC_CALLBACK_0(GameScene::growMoney, this, pMoneyF)), CallFunc::create(CC_CALLBACK_0(EGSprite::performRelease, pCoin)), NULL));
	}
	else{
		pCoin->runAction(Sequence::create(MoveTo::create(pDuration, pDestiny), CallFunc::create(CC_CALLBACK_0(GameScene::refresh, this)), CallFunc::create(CC_CALLBACK_0(EGSprite::performRelease, pCoin)), NULL));
	}
	this->addChild(pCoin);
	pCoin->setPosition(pSartPoint);
	pCoin->setScale(0.5f + (rand() % 2)* 0.1f);
	pCoin->setZOrder(1);
	if (pHide){
		label->setVisible(false);
	}
}

void GameScene::playSound(const char* pSoundEffect){
	if (UserDefault::getInstance()->getBoolForKey(KEY_SOUND, true)){
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(pSoundEffect);
	}
}

bool GameScene::checkRectCollide(Rect _rect1, Rect _rect2, float pRotation1, float pRotation2, bool iRotate){

	if (!iRotate){
		return _rect1.intersectsRect(_rect2);
	}
	else{
		PointArray *pArrayPoint1 = PointArray::create(4);
		PointArray *pArrayPoint2 = PointArray::create(4);
		Rect _rect;
		for (int j = 1; j >= 0; j--){
			_rect = _rect1;
			if (j == 1){
				_rect = _rect2;
			}
			for (int i = 3; i >= 0; i--){
				Point pPointGoc;
				switch (i)
				{
				case 0:pPointGoc = ccp(_rect.getMinX(), _rect.getMinY()); break;
				case 1:pPointGoc = ccp(_rect.getMaxX(), _rect.getMinY()); break;
				case 2:pPointGoc = ccp(_rect.getMaxX(), _rect.getMaxY()); break;
				case 3:pPointGoc = ccp(_rect.getMinX(), _rect.getMaxY()); break;
				}
				float pRotation = EGSupport::getDirectionByRotate(_rect.getMidX(), _rect.getMidY(), pPointGoc.x, pPointGoc.y);
				float pRadius1 = ccpDistance(ccp(_rect.getMidX(), _rect.getMidY()), ccp(_rect.getMinX(), _rect.getMinY()));
				if (j == 0){
					pArrayPoint1->addControlPoint(EGSupport::getPointInCircle(_rect.getMidX(), _rect.getMidY(), pRadius1, pRotation + pRotation1));
				}
				else{
					pArrayPoint2->addControlPoint(EGSupport::getPointInCircle(_rect.getMidX(), _rect.getMidY(), pRadius1, pRotation + pRotation2));
				}
			}
		}
		for (int i = pArrayPoint1->count() - 1; i >= 0; i--){
			for (int j = pArrayPoint2->count() - 1; j >= 0; j--){
				Point pPoint1a = pArrayPoint1->getControlPointAtIndex(i);
				int index1 = i - 1;
				if (index1 < 0){
					index1 = pArrayPoint1->count() - 1;
				}
				Point pPoint1b = pArrayPoint1->getControlPointAtIndex(index1);
				Point pPoint2a = pArrayPoint2->getControlPointAtIndex(j);
				int index2 = j - 1;
				if (index2 < 0){
					index2 = pArrayPoint2->count() - 1;
				}
				Point pPoint2b = pArrayPoint2->getControlPointAtIndex(index2);
				Point pPointCheck = ccpIntersectPoint(pPoint1a, pPoint1b, pPoint2a, pPoint2b);
				bool isOutLine = false;
				isOutLine = checkOutLine(pPoint1a, pPoint1b, pPointCheck);
				if (!isOutLine){
					isOutLine = checkOutLine(pPoint2a, pPoint2b, pPointCheck);
				}
				if (!isOutLine){
					return true;
				}
			}
		}
		return false;
	}
}
bool GameScene::checkOutLine(Point pPointStart, Point pPointEnd, Point pPointCheck){
	if (ccpDistance(pPointStart, pPointCheck) + ccpDistance(pPointCheck, pPointEnd) > ccpDistance(pPointEnd, pPointStart)){
		return true;
	}
	return false;
}
bool GameScene::checkContain(PointArray* pArrayPoint, Point pPointCheck){
	Vec2 pCenter = ccpIntersectPoint(pArrayPoint->getControlPointAtIndex(0), pArrayPoint->getControlPointAtIndex(2),
		pArrayPoint->getControlPointAtIndex(1), pArrayPoint->getControlPointAtIndex(3));
	if (ccpDistance(pCenter, pPointCheck) <= ccpDistance(pArrayPoint->getControlPointAtIndex(0), pArrayPoint->getControlPointAtIndex(2))){
		return true;
	}
	return false;
}

#include "FirstScene.h"

void GameScene::leftGame(){
	if (mDelayOutGame > 0){
		this->unscheduleAllSelectors();
		roomStruct * pRoomInfo = (roomStruct*)mInfoRoom;
		_eventDispatcher->removeAllEventListeners();
		//CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
		if (pRoomInfo->mGameID == 2){
			UserDefault::getInstance()->setStringForKey(KEY_MONEY, __String::createWithFormat("%d", getPlayerByName("")->balance)->getCString());
			UserDefault::getInstance()->setIntegerForKey(KEY_SO, mCountSo);
			Scene * homeScene = HomeSceneOff::scene();
			Director::getInstance()->replaceScene(homeScene);
			clearData();
		}
		else{

			if (getInfoMyselft() && pRoomInfo->mGameID == 0){
				GameInfo::mUserInfo.mBalance = getInfoMyselft()->balance;
			}
			stopAllActions();
			unscheduleAllSelectors();
			if (mRangeItem){
				delete mRangeItem->getUserData();
				mRangeItem->setUserData(NULL);
			}
			roomStruct * pRoomInfo = (roomStruct*)mInfoRoom;
			MpLeftTableMessageRequest * leftRoomRequest = new MpLeftTableMessageRequest();
			leftRoomRequest->setTokenId(GameInfo::mUserInfo.mTokenID);
			leftRoomRequest->addInt(MP_PARAM_TABLE_ID, ((roomStruct*)pRoomInfo)->mTableID);
			
			if (GameInfo::mUserInfo.mGameID == 0)
				MpClientManager::getInstance(1)->sendMessage(leftRoomRequest);
			else
				MpClientManager::getInstance(2)->sendMessage(leftRoomRequest);
			//MpClientManager::getInstance()->disconnect();
			clearData();

			/*std::string ip = GameInfo::mUserInfo.getIpNormal();
			int port = GameInfo::mUserInfo.getPortNormal();
			MpClientManager::getInstance(1)->connect(ip.c_str(), port, false);*/

			//Scene * chooseRoomScene = ChooseRoomScene::createScene(GameInfo::mUserInfo.mGameID);
			Scene* chooseRoomScene = MainScene::scene(0);
			Director::getInstance()->replaceScene(chooseRoomScene);

		}
	}
	else{
		outGame();
	}
}
void GameScene::outGame(){
	//CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	stopAllActions();
	this->unscheduleAllSelectors();
	_eventDispatcher->removeAllEventListeners();
	MpClientManager::getInstance()->close();

	clearData();

	Scene * chooseRoomScene = HomeScene::scene();
	Director::getInstance()->replaceScene(chooseRoomScene);
}


void GameScene::clearData(){
	roomStruct* pInfo = (roomStruct*)mInfoRoom;
	if (getInfoMyselft() && pInfo->mGameID == 0){
		GameInfo::mUserInfo.mBalance = getInfoMyselft()->balance;
	}
	stopAllActions();
	unscheduleAllSelectors();
	for (int i = mListBullet.size() - 1; i >= 0; i--){
		Sprite* bullet = mListBullet.at(i);
		bullet->stopAllActions();
		mListBullet.popBack();
	}
	for (int i = mListFish.size() - 1; i >= 0; i--){
		Node* bullet = mListFish.at(i);
		bullet->stopAllActions();
		mListFish.popBack();
	}
	if (mRangeItem){
		delete mRangeItem->getUserData();
		mRangeItem->setUserData(NULL);
	}
	roomStruct * pRoomInfo = (roomStruct*)mInfoRoom;
	if (pRoomInfo && pRoomInfo->mListPlayer){
		delete pRoomInfo->mListPlayer;
	}
	pRoomInfo->mListPlayer = NULL;
	pRoomInfo->pLoading = NULL;
}

void GameScene::reStart(){
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	if (!isStartGame){
		mMoneyTeam1 = 0;
		mMoneyTeam2 = 0;
		for (int i = pInfoRoom->mListPlayer->size() - 1; i >= 0; i--){
			ClientPlayerInfoEX* playerInfo = &pInfoRoom->mListPlayer->at(i);
			if (mTime == pInfoRoom->mDuration){
				playerInfo->balance = playerInfo->balanceDefault;

				playerInfo->score = 0;
				if (playerInfo->position < 2){
					mMoneyTeam1 = playerInfo->balance;
				}
				else{
					mMoneyTeam2 = playerInfo->balance;
				}
			}
			if (playerInfo->position < 4){
				mListGun.at(playerInfo->position)->unready();
				if (playerInfo->isReady){
					mListGun.at(playerInfo->position)->ready();
				}
			}
		}
		mButtonStartReady->setVisible(false);
		ClientPlayerInfoEX *myselft = getInfoMyselft();
		if (myselft){
			if (!myselft->host){
				if (!myselft->isReady){
					mButtonStartReady->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(IMG_BUTTON_READY));
					mButtonStartReady->setVisible(true);
				}
			}
			else{
				int pTeam1 = 0;
				int pTeam2 = 0;
				mButtonStartReady->setOpacity(155);
				mButtonStartReady->setVisible(true);
				mButtonStartReady->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(IMG_BUTTON_START));
				for (int i = pInfoRoom->mListPlayer->size() - 1; i >= 0; i--){
					ClientPlayerInfoEX playerInfo = pInfoRoom->mListPlayer->at(i);

					if ((playerInfo.isReady && pInfoRoom->pTypeRoom == 0)){
						pTeam1++;
					}
					else if ((playerInfo.isReady&& pInfoRoom->pTypeRoom == 1)){
						if (playerInfo.position / 2 == 0){
							pTeam1++;
						}
						else{
							pTeam2++;
						}
					}
				}
				if (pTeam2 + pTeam1 == pInfoRoom->mListPlayer->size() - 1 && pTeam2 + pTeam1 > 0){
					if (pInfoRoom->pTypeRoom == 0 || pTeam2 + pTeam1 == pInfoRoom->pMaxPlayer - 1){
						mButtonStartReady->setVisible(true);
						mButtonStartReady->setOpacity(255);
					}
				}
			}
		}
		refresh();
	}
	if (pInfoRoom->mGameID == 1){
		for (int i = pInfoRoom->mListPlayer->size() - 1; i >= 0; i--){
			ClientPlayerInfoEX* playerInfo = &pInfoRoom->mListPlayer->at(i);
			if (playerInfo->position < 2){
				mMoneyTeam1 = playerInfo->balance;
				pScoreTeam1 = playerInfo->score;
			}
			else{
				mMoneyTeam2 = playerInfo->balance;
				pScoreTeam2 = playerInfo->score;
			}
		}
	}
}
void GameScene::readyStartGame(){
	if (mButtonStartReady->getOpacity() == 255 && (!layerResultUp || layerResultUp->getOpacity() == 0)){
		roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
		WXmlReader* pXml = WXmlReader::create();
		if (GameInfo::mUserInfo.mBalance >= pInfoRoom->pBetMoney){
			MpPlayerReadyMessageRequest *readyRequest = new MpPlayerReadyMessageRequest();
			addComponentDefault(readyRequest);
			mButtonStartReady->setVisible(false);
		}
		else{
			std::string pStrMoney = __String::createWithFormat("%d", (int)pInfoRoom->pBetMoney)->getCString();
			showToast(__String::createWithFormat(pXml->getNodeTextByTagName("not_enough_money_to_ready").c_str(), EGSupport::addDotMoney(pStrMoney).c_str())->getCString(), 600, Vec2(400, 160));
		}
	}
}
void GameScene::startGame(){
	isStartGame = true;
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;

	for (int i = pInfoRoom->mListPlayer->size() - 1; i >= 0; i--){
		ClientPlayerInfoEX* playerInfo = &pInfoRoom->mListPlayer->at(i);
		if (playerInfo->position < 4){
			mListGun.at(playerInfo->position)->unready();
		}
	}
	for (int i = mListGun.size() - 1; i >= 0; i--){
		GunPlayer* gun = mListGun.at(i);
		gun->startGame(true);
	}
	if (pInfoRoom->mGameID == 1){
		mTime = pInfoRoom->mDuration;
		buttonOption->setVisible(false);
		buttonInvite->setVisible(false);
		Sprite * pTextStartGame = Sprite::createWithSpriteFrameName("text-start-game.png");
		this->addChild(pTextStartGame);
		pTextStartGame->setAnchorPoint(Vec2(0.5f, 0.4f));
		pTextStartGame->setZOrder(10);
		pTextStartGame->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
		//pTextStartGame->runAction(Sequence::create(DelayTime::create(2.75f), FadeOut::create(0.25f), NULL));
		pTextStartGame->runAction(Sequence::create(DelayTime::create(2.8f), ScaleTo::create(0.2f, 2, 0), CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParentAndCleanup, pTextStartGame, true)), NULL));
	}
}
void GameScene::updateForOffLineGame(float pSec){
	roomStruct* pRoomInfo = (roomStruct*)mInfoRoom;
	if (mDelayChangeMap >= 0){
		mDelayChangeMap -= pSec;
	}
	if (mDelayChangeMap < 0){
		pauseGameOff();
		CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
		isChangeMap = true;
		mDelayChangeMap = 300;
		mCountChange++;
	}
	if (mDelaySmallFishOff >= 0){
		mDelaySmallFishOff -= pSec;

	}
	if (mDelaySmallFishOff < 0){
		mDelaySmallFishOff = 2.5f + (0.1f* (rand() % 5));
		int TypeFish = -1;
		std::vector<ClientFishInfo> pListFish;
		if (true){
			ClientFishInfo pFishInfo1;

			pFishInfo1.direction = rand() % 4;
			mIDFish++;
			if (mIDFish > 1000){
				mIDFish = 1;
			}
			pFishInfo1.fishId = mIDFish;
			pFishInfo1.offsetX = 0;
			pFishInfo1.offsetY = 0;
			pFishInfo1.trajectory = 51 + rand() % 20;
			pFishInfo1.speed = rand() % 60;
			pFishInfo1.health = 0;
			int pTypeRand = 1 + rand() % 5;
			int pRnd = rand() % 100;
			if (pRnd < 50){
				pTypeRand = 1 + rand() % 2;
			}
			else if (pRnd < 80){
				pTypeRand = 3 + rand() % 2;
			}
			else{
				pTypeRand = 5;
			}
			int pQuantity = 5 + rand() % 8;
			if (pTypeRand == 5){
				pQuantity = 1 + rand() % 2;
			}
			pFishInfo1.trajectory = 1 + rand() % 30;

			for (int i = 0; i < pQuantity; i++){
				if (pTypeRand == 5){
					pFishInfo1.direction = rand() % 4;
				}
				mIDFish++;
				if (mIDFish > 1000){
					mIDFish = 1;
				}
				pFishInfo1.fishId = mIDFish;
				pFishInfo1.offsetX = rand() % 200;
				pFishInfo1.offsetY = rand() % 200;
				pFishInfo1.health = 15 + rand() % 300;
				pFishInfo1.fishType = pTypeRand;
				TypeFish = pTypeRand;
				pListFish.push_back(pFishInfo1);
			}
		}
		float pDelay = 0;
		int pType = pListFish.at(pListFish.size() - 1).fishType;
		for (int i = pListFish.size() - 1; i >= 0; i--){

			ClientFishInfo* pFishInfo = new ClientFishInfo(pListFish.at(i));
			if (pType != pFishInfo->fishType){
				pDelay += 0.1f + 0.1f* (rand() % 5);
			}

			CallFunc *call = CallFunc::create(CC_CALLBACK_0(GameScene::createFish, this, pFishInfo));
			runAction(Sequence::createWithTwoActions(DelayTime::create(pDelay), call));

		}
	}
	if (mDelayOffline >= 0){
		mDelayOffline -= pSec;
	}

	if (mDelayOffline < 0){
		mDelayOffline = 4 + rand() % 2;
		int TypeFish = -1;
		std::vector<ClientFishInfo> pListFish;
		for (int i = 0; i < 1; i++){
			int pRandTypeBig = rand() % 100;
			ClientFishInfo pFishInfo;
			pFishInfo.direction = rand() % 4;
			mIDFish++;
			if (mIDFish > 1000){
				mIDFish = 1;
			}
			pFishInfo.fishId = mIDFish;
			pFishInfo.offsetX = 0;
			pFishInfo.offsetY = 0;
			pFishInfo.trajectory = 51 + rand() % 20;
			pFishInfo.speed = rand() % 60;
			pFishInfo.health = 0;

			if (pRandTypeBig < 45){
				int pTypeRand = 6 + rand() % 9;
				pFishInfo.fishType = pTypeRand;
				pListFish.push_back(pFishInfo);
				TypeFish = pTypeRand;
			}
			else if (pRandTypeBig < 85){
				int pTypeRand = 11 + rand() % 3;
				pFishInfo.fishType = pTypeRand;
				pListFish.push_back(pFishInfo);
				TypeFish = pTypeRand;
			}
			else{
				int pTypeRand = 14 + rand() % 7;
				TypeFish = pTypeRand;
				pFishInfo.fishType = pTypeRand;
				pListFish.push_back(pFishInfo);
			}
		}
		int pCount = 1 + rand() % 2;
		float pDelay = 0;
		int pType = pListFish.at(pListFish.size() - 1).fishType;
		for (int i = pListFish.size() - 1; i >= 0; i--){
			ClientFishInfo* pFishInfo = new ClientFishInfo(pListFish.at(i));
			if (pType != pFishInfo->fishType){
				pDelay += 0.1f + 0.1f* (rand() % 5);
			}
			CallFunc *call = CallFunc::create(CC_CALLBACK_0(GameScene::createFish, this, pFishInfo));
			runAction(Sequence::createWithTwoActions(DelayTime::create(pDelay), call));
		}
	}
}
bool GameScene::checkExistFish(int pType){
	bool iExist = false;
	for (int i = mListFish.size() - 1; i >= 0; i--){
		Fish* pFish = mListFish.at(i);
		if (!pFish->isDead() && pFish->isVisible()){
			if (pFish->getType() == pType){
				iExist = true;
			}
		}
	}
	return iExist;
}
void GameScene::createFish(ClientFishInfo *pInfo){
	std::vector<Vec2> pList = JsonParser::getInstance()->bodyLineFormJson(Size(CLIENT_WIDTH, CLIENT_HEIGHT), Vec2(0, 0), __String::createWithFormat("map%d", pInfo->trajectory)->getCString());

	std::vector<Vec2*>* pFishWay = new std::vector<Vec2*>();
	for (int i = pList.size() - 1; i >= 0; i--){
		pFishWay->push_back(new Vec2(pList.at(i).x, pList.at(i).y));
		if (i > 0){
			Vec2 midPoint = ccpMidpoint(pList.at(i), pList.at(i - 1));
			pFishWay->push_back(new Vec2(midPoint.x, midPoint.y));
		}
	}
	if (pInfo->trajectory == 0){
		pFishWay->push_back(new Vec2(-80, 240));
		pFishWay->push_back(new Vec2(880, 240));
	}
	int pSideX = 2 * (pInfo->direction / 2) - 1;
	int pSideY = 2 * (pInfo->direction % 2) - 1;
	for (int i = pFishWay->size() - 1; i >= 0; i--){
		pFishWay->at(i)->x = 400 + (pFishWay->at(i)->x - 400)* pSideX;
		pFishWay->at(i)->y = 240 + (pFishWay->at(i)->y - 240)* pSideY;
	}
	int pOffsetX = pInfo->offsetX;
	int pOffsetY = pInfo->offsetY;
	if (pFishWay->at(pFishWay->size() - 1)->x < 0){
		pOffsetX = -pOffsetX;
	}
	if (pFishWay->at(pFishWay->size() - 1)->y < 0){
		pOffsetY = -pOffsetY;
	}
	if (pInfo->fishType == 0){
		pOffsetX = 0;
		pOffsetY = 0;
	}
	Vec2 pStartPoint = Vec2(pFishWay->at(pFishWay->size() - 1)->x + pOffsetX, pFishWay->at(pFishWay->size() - 1)->y + pOffsetY);
	Fish* pFish = obtainFish(pInfo->fishType);
	if (pInfo->isSpecial){
		pFish->upgrade();
	}
	pFish->setID(String::createWithFormat("%d", pInfo->fishId)->getCString());
	pFish->setZOrder(4);

	Vec2 pVecOffset = Vec2(pStartPoint.x - pFishWay->at(pFishWay->size() - 1)->x, pStartPoint.y - pFishWay->at(pFishWay->size() - 1)->y);
	for (int i = pFishWay->size() - 1; i >= 0; i--){
		pFishWay->at(i)->x += (pStartPoint.x - pFishWay->at(pFishWay->size() - 1)->x) / 2;
		pFishWay->at(i)->x += (pStartPoint.y - pFishWay->at(pFishWay->size() - 1)->y) / 2;
	}
	if (pFish->getType() < 6){
		pFish->setPower(pInfo->health);
	}
	pFish->setUserData(pFishWay);
	pFish->getObject()->setLimitSpeed(pFish->getObject()->getSpeed() + (pInfo->speed - 40)*1.5f);
	pFish->setPosition(pStartPoint);
	pFish->startMove();
	pFish->setStartPoint(pStartPoint);
	delete pInfo;
}
void GameScene::bonusMap(int pType){
	isBonus = true;
	int pTypeBig = 15 + rand() % 5;
	int pTypeSmall = 1 + rand() % 4;
	int pID = 0;
	for (int i = 0; i < 5; i++){
		Fish* pBigFish = obtainFish(pTypeBig);
		pBigFish->setID(__String::createWithFormat("%d", pID)->getCString());
		pBigFish->setZOrder(3);
		std::vector<Vec2*>* pFishWay = new std::vector<Vec2*>();
		Vec2 pStartPoint = Vec2(-100 - 300 * i, CLIENT_HEIGHT / 2);
		pFishWay->push_back(new Vec2(CLIENT_WIDTH + 210, CLIENT_HEIGHT / 2));
		pFishWay->push_back(new Vec2(pStartPoint.x + 1, pStartPoint.y));

		pBigFish->setUserData(pFishWay);
		pBigFish->setPosition(pStartPoint);
		pBigFish->setStartPoint(pStartPoint);
		pBigFish->startMove();

		pBigFish->getObject()->setCurrentSpeed(60);
		pID++;
		pTypeBig--;

		for (int j = 0; j < 24; j++){
			Fish* pSmallFish = obtainFish(pTypeSmall);
			pSmallFish->setID(__String::createWithFormat("%d", pID)->getCString());
			pSmallFish->setZOrder(3);
			std::vector<Vec2*>* pFishWay = new std::vector<Vec2*>();
			Vec2 pStartPointSmal = EGSupport::getPointInCircle(pStartPoint.x, pStartPoint.y, 100, j * 15);
			pFishWay->push_back(new Vec2(CLIENT_WIDTH + 200, pStartPointSmal.y));
			pFishWay->push_back(new Vec2(pStartPointSmal.x + 1, pStartPointSmal.y));
			pSmallFish->setUserData(pFishWay);
			pSmallFish->setPosition(pStartPointSmal);
			pSmallFish->setStartPoint(pStartPointSmal);
			pSmallFish->startMove();
			pSmallFish->getObject()->setCurrentSpeed(60);
			pID++;
		}
		pTypeSmall++;
	}
}
void GameScene::pauseGame(){
	CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
	roomStruct* pRoomInfo = (roomStruct*)mInfoRoom;
	if (pRoomInfo->mGameID < 2){
		EGScene::pauseGame();
		//outGame();
	}
	else{
		reloadMoneyOff();
	}
}
void GameScene::showBoxInvite(){
	//if (!isStartGame){
	//	mBoxInvite->showBox();
	//	//mBoxInvite->chooseTab(1);
	//}
}

void GameScene::showNotice(const std::string& content)
{
	mErrorCode_Game_New = content;
}

void GameScene::showLoading(bool bLoading)
{
	if (bLoading)
		mMainLoading->show();
	else
		mMainLoading->hide();
}

void GameScene::getSmsInfo()
{
	BoxTopUp* boxTopUp = mBoxTopUp;
	MpGetInfoPurchaseMessageRequest* pRequest = new MpGetInfoPurchaseMessageRequest();
	pRequest->setTokenId(GameInfo::mUserInfo.mTokenID.c_str());
	pRequest->setChargeID(GameInfo::mUserInfo.mChargeID);
	pRequest->setCountry(EGJniHelper::getCountry());
	std::string pOS = GameInfo::os_prefix;
	pRequest->setOS(pOS);
	pRequest->setProductCode(EGJniHelper::getPackage());
	MpClientManager::getInstance(0)->sendMessage(pRequest);
	MpClientManager::getInstance(GameInfo::mUserInfo.mGameID + 1);
}

void GameScene::recharge_clone(std::string pSerial, std::string pNumber, std::string pTelco) {

	mMainLoading->show();
	this->runAction(Sequence::createWithTwoActions(DelayTime::create(10), CallFunc::create(CC_CALLBACK_0(Loading::hide, mMainLoading))));

	RechargeStruct_game* pRechargeStruct = new RechargeStruct_game();
	pRechargeStruct->mTelco = pTelco;
	pRechargeStruct->mNumber = pNumber;
	pRechargeStruct->mSerrial = pSerial;
	pRechargeStruct->mUsername = GameInfo::mUserInfo.mUsername;
	pRechargeStruct->mLoading = mMainLoading;

	MpMessage* message = new MpMessage((uint32_t)MP_MSG_RECHARGE);
	message->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
	message->addInt(MP_PARAM_CHARGE_ID, GameInfo::mUserInfo.mChargeID);
	message->addString(MP_PARAM_TELCO_ID, pRechargeStruct->mTelco);
	message->addString(MP_SERRIAL, pRechargeStruct->mSerrial);
	message->addString(MP_PARAM_PASSWORD, pRechargeStruct->mNumber);
	for (int i = 0; i < pRechargeStruct->mUsername.length(); i++) {
		pRechargeStruct->mUsername[i] = std::tolower(pRechargeStruct->mUsername[i]);
	}
	message->addString(MP_PARAM_USERNAME, pRechargeStruct->mUsername);
	MpClientManager::getInstance(0)->sendMessage(message);
	MpClientManager::getInstance(GameInfo::mUserInfo.mGameID + 1);
	delete pRechargeStruct;
	pRechargeStruct = NULL;
}


void GameScene::setFireMoney(uint32_t pFireMoney){
	mFireMoney = pFireMoney;
	roomStruct* pInfoRoom = (roomStruct*)mInfoRoom;
	for (int i = pInfoRoom->mListPlayer->size() - 1; i >= 0; i--){
		ClientPlayerInfoEX *player = &pInfoRoom->mListPlayer->at(i);
		if (pInfoRoom->mGameID == 1){
			player->balanceDefault = mFireMoney;
		}
	}
}


void GameScene::earthQuake(int rate, int pAngle){
	if (rate > 0){

		mBackGround->stopAllActions();
		Vec2 pDestiny = EGSupport::getPointInCircle(400, 240, rate, pAngle);
		pAngle += 90 + rand() % 180;
		rate--;
		mBackGround->runAction(Sequence::create(MoveTo::create(0.05f, pDestiny), CallFunc::create(CC_CALLBACK_0(GameScene::earthQuake, this, rate, pAngle)), NULL));
	}
	else{
		mBackGround->setPosition(Vec2(400, 240));
	}
}
void GameScene::showBonusSpecial(int index, int pScore){
	if (!mBonusSpecialFish[index]){
		mBonusSpecialFish[index] = Sprite::create(__String::createWithFormat("effect_bonus_special%d.png", 1)->getCString());
		mMainLayer->addChild(mBonusSpecialFish[index]);
		mBonusSpecialFish[index]->setZOrder(3);
	}
	mBonusSpecialFish[index]->stopAllActions();
	mBonusSpecialFish[index]->setVisible(true);
	Animation* animation = Animation::create();
	for (int i = 0; i < 5; i++){
		animation->addSpriteFrameWithFile(__String::createWithFormat("effect_bonus_special%d.png", i + 1)->getCString());
	}
	animation->setDelayPerUnit(0.1f);
	mBonusSpecialFish[index]->runAction(Sequence::create(Repeat::create(Animate::create(animation), 10), CallFunc::create([=]{
		mBonusSpecialFish[index]->removeAllChildrenWithCleanup(true);
		mBonusSpecialFish[index]->setVisible(false);
	}), NULL));
	mBonusSpecialFish[index]->setPosition(Vec2((index / 2) * 800 + ((index / 2)*-2 + 1)*(220 + (index % 2) * 360), 100 + (index / 2) * 280));
	Label* pBonusScore = NULL;
	if (mBonusSpecialFish[index]->getChildrenCount() >0){
		pBonusScore = (Label*)mBonusSpecialFish[index]->getChildren().at(0);
		pBonusScore->setString(__String::createWithFormat("+%d", pBonusScore->getTag() + pScore)->getCString());
		pBonusScore->setTag(pBonusScore->getTag() + pScore);
	}
	else{
		pBonusScore = Label::createWithBMFont("font-num-room.fnt", "");
		mBonusSpecialFish[index]->addChild(pBonusScore);
		pBonusScore->setString(__String::createWithFormat("+%d", pScore)->getCString());
		pBonusScore->setTag(pScore);
	}
	pBonusScore->setPosition(Vec2(mBonusSpecialFish[index]->getContentSize() / 2) + Vec2(-10, 0));
	pBonusScore->setScale(0);
	pBonusScore->runAction(EaseElasticOut::create(ScaleTo::create(1, 1)));
	pBonusScore->setRotation(-20);
}
