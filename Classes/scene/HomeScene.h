#ifndef __HOMESCENE_H_INCLUDED__
#define __HOMESCENE_H_INCLUDED__

#include "cocos2d.h"
#include "EGButtonSprite.h"
#include "EGScene.h"
#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "extensions\cocos-ext.h"
#else
#include "cocos-ext.h"
#endif
#include "Loading.h"
#include "BoxNotice.h"
#include "WXmlReader.h"
#include "pthread_mobi.h"
#include "BoxChat.h"
#include "RippleSprite.h"
#include "cocos/network/HttpClient.h"
#include "object/BoxResetPass.h"

USING_NS_CC;
using namespace extension;

class HomeScene :public EGScene, public EditBoxDelegate {
public:
	static Scene* scene();
	
	void onBackPressed();
	void updateJoinTable(float pSec);
	void createGame();
	void onLoginFacebook(int pResult, std::string pAccessToken);
	void loginFacebook(std::string facebookID, std::string tokenFacebook);
	void facebookPress();
private:
	//CREATE_FUNC(HomeScene);
	bool isConnect;
	string _trandId = "";
	WXmlReader* mXml;
	RippleSprite* mBackground;
	Sprite /** mBackground,*/ *mTitle;
	EGButtonSprite* mBtn_Register, *mBtn_Login, *mBtn_Skip, *mBtn_BoxCheck, *mBtn_ForgetPass;
	EditBox* mEdt_Username, *mEdt_Passwords, *mEdt_RePasswords;
	Sprite* mCheck;
	Loading* mLoading;
	void goGameOffline();
	BoxNotice* mBoxNotice;
	EGButtonSprite* mBtnBack;
	EGButtonSprite* mBtnLoginFb;
	EGButtonSprite* mBtnMusic, *mBtnSound;
	BoxResetPass* boxResetPass;

	void initScene();
	void reconect();

	void checkBox();
	void login();
	void registerAccount();
	void forgetPasswords();
	void soundConfig();
	void musicConfig();
	void loginProcess(void* pVoid);

	void update(float dt);
	void updateBubble(float dt);
	virtual void editBoxReturn(EditBox* editBox);

	void requestGetOtp(std::string phone);
	void sendOtpToResetPass(std::string phone, std::string otp);
	void resendOTP();

	void executeResetPass(MpMessage* pMsg);
	void executeResendOTP(MpMessage* pMsg);
	void executeRequestOTP(MpMessage* pMsg);

	//for http request
	void requestDetectSub();
	void requestDectectSubComplete(cocos2d::network::HttpClient* sender, cocos2d::network::HttpResponse* response);


};

#endif