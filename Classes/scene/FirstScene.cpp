#include "FirstScene.h"
#include "GameInfo.h"
#include "HomeSceneOff.h"
#include "GameScene.h"
#include "EGJniHelper.h"
#include "SimpleAudioEngine.h"
#include "EGSupport.h"
#include "HomeScene.h"

#define SPEED_BOAT 500

Scene* FirstScene::scene() {
	FirstScene* pFirstScene = new FirstScene();
	pFirstScene->initScene();
	pFirstScene->autorelease();
	pFirstScene->registerBackPressed();

	return pFirstScene;
}

void FirstScene::initScene() {
	GameInfo::mUserInfo.stateScene = 0;
	mBackground = Sprite::create("background_choosetype.png");
	mBackground->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBackground);

	auto logo = Sprite::create("logo-home-off.png");
	logo->setPosition(Vec2(CLIENT_WIDTH/2, CLIENT_HEIGHT/2 + 50));
	mBackground->addChild(logo);

	mDarkBg = LayerColor::create(Color4B(0, 0, 0, 200));
	mBackground->addChild(mDarkBg);

	mLoadBar = Sprite::create("load_resources (5).png");
	mLoadBar->setPosition(Vec2(CLIENT_WIDTH / 2, mLoadBar->getContentSize().height / 2 + 30));
	mDarkBg->addChild(mLoadBar);

	mLoad = ProgressTimer::create(Sprite::create("load_resources (6).png"));
	mLoad->setPosition(Vec2(CLIENT_WIDTH / 2, mLoad->getContentSize().height / 2 + 30));
	mLoad->setMidpoint(Vec2(0, 0.5f));
	mLoad->setBarChangeRate(Vec2(1, 0));
	mLoad->setType(ProgressTimer::Type::BAR);
	mLoad->setPercentage(0);
	mDarkBg->addChild(mLoad);
	
	mLoadObject = Sprite::create("loading_1.png");
	mLoadObject->setScale(0.7f);
	mLoadObject->setPosition(Vec2(mLoadBar->getPositionX() - mLoadBar->getContentSize().width / 2,
		mLoadBar->getContentSize().height + mLoadObject->getContentSize().height-30));
	mDarkBg->addChild(mLoadObject);

	

	Vector<SpriteFrame*> _list;
	for (int i = 0; i < 4; i++){
		std::string filename = __String::createWithFormat("loading_%d.png", i + 1)->getCString();
		SpriteFrameCache *cache = SpriteFrameCache::getInstance();
		SpriteFrame *spriteFrame = cache->getSpriteFrameByName(filename);
		auto sprite = Sprite::create(filename);
		auto frame = SpriteFrame::create(filename, sprite->getTextureRect());
		cache->addSpriteFrame(frame, filename);
		Director::getInstance()->getTextureCache()->removeTextureForKey(filename);

		_list.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(filename));
	}

	Animate* pAnimation = Animate::create(Animation::createWithSpriteFrames(_list, 0.1f));

	mLoadObject->runAction(RepeatForever::create(pAnimation));

	/*float pWidth = mLoadBar->getPositionX() - mLoadBar->getContentSize().width / 2 + (mLoadBar->getContentSize().width / 100) * 20;
	float pTime = pWidth / SPEED_BOAT;
	mLoad->stopAllActions();
	mLoadObject->runAction(MoveTo::create(pTime, Vec2(pWidth, mLoadObject->getPositionY())));
	mLoad->runAction(ProgressTo::create(pTime, 20));
	mDarkBg->runAction(Sequence::createWithTwoActions(DelayTime::create(pTime), CallFunc::create(CC_CALLBACK_0(FirstScene::loadData, this))));*/
	loadData();
}

void FirstScene::loadData() {
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("icon.plist");

	/*ZipUtils::setPvrEncryptionKeyPart(0, 0xa9ddf293);
	ZipUtils::setPvrEncryptionKeyPart(1, 0x34d96c4a);
	ZipUtils::setPvrEncryptionKeyPart(2, 0x95ebcaef);
	ZipUtils::setPvrEncryptionKeyPart(3, 0x21e277d4);*/
	//SpriteFrameCache::getInstance()->addSpriteFramesWithFile("fish.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("update4.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("update1.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("update2.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("update3.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("effect.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bigobject.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("box.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("background.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("avatar.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("object.plist");


	float pWidth = mLoadBar->getPositionX() - mLoadBar->getContentSize().width / 2 + (mLoadBar->getContentSize().width / 100) * 70;
	float pTime = pWidth / SPEED_BOAT;
	//mLoadObject->stopAllActions();
	mLoad->stopAllActions();
	mLoadObject->runAction(MoveTo::create(pTime, Vec2(pWidth, mLoadObject->getPositionY())));
	mLoad->runAction(ProgressTo::create(pTime, 70));
	mDarkBg->runAction(Sequence::createWithTwoActions(DelayTime::create(pTime), CallFunc::create(CC_CALLBACK_0(FirstScene::loadMusic, this))));
}

void FirstScene::loadMusic() {
#if(CC_TARGET_PLATFORM != CC_PLATFORM_WP8 && CC_TARGET_PLATFORM != CC_PLATFORM_WINRT)
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("backmusic.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound-catch-seamaiden.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound-coin.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound-change-gun.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound-enter.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound-laser-shoot.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound-max-power.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound-shoot.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound-suport (1).mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound-suport (2).mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound-tip.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound-treasure.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("anchor_fall.mp3");
#endif

	float pWidth = mLoadBar->getPositionX() - mLoadBar->getContentSize().width / 2 + (mLoadBar->getContentSize().width / 100) * 100;
	float pTime = pWidth / SPEED_BOAT;
	//mLoadObject->stopAllActions();
	mLoad->stopAllActions();
	mLoadObject->runAction(MoveTo::create(pTime, Vec2(pWidth, mLoadObject->getPositionY())));
	mLoad->runAction(ProgressTo::create(pTime, 100));
	mDarkBg->runAction(Sequence::createWithTwoActions(DelayTime::create(pTime), CallFunc::create(CC_CALLBACK_0(FirstScene::loadDataDone, this))));
}

void FirstScene::loadDataDone() {
	this->runAction(Sequence::createWithTwoActions(DelayTime::create(0.5f), CallFunc::create([]{
		//Director::getInstance()->replaceScene(TransitionFade::create(0.5f, HomeSceneOff::scene()));
		Director::getInstance()->replaceScene(TransitionFade::create(0.5f, HomeScene::scene()));
	})));
}

void FirstScene::onBackPressed() {
}