#ifndef __MAINSCENE_H_INCLUDED__
#define __MAINSCENE_H_INCLUDED__

#include "cocos2d.h"
#include "EGSprite.h"
#include "EGButtonSprite.h"
#include "EGScene.h"
#include "BoxNotice.h"
#include "BoxTopUp.h"
#include "BoxTopUpCard.h"
#include "Loading.h"
#include "object/BoxActiveAccount.h"
#include "BoxInfoGame.h"
#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "extensions\cocos-ext.h"
#else
#include "cocos-ext.h"
#endif

//#include "BoxNews.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace extension;


class MainScene :public EGScene, public EditBoxDelegate{
public:
	static Scene* sceneWithRecharge();
	static Scene* scene(int _type); //0: nomarl 1: active require
	static Scene* sceneWithUpdateNotice();
	static std::vector<uint32_t>& getVectorMoney();
	static void setVectorMoney(std::vector<uint32_t> pVector);

	void excuteGetInfoMyselft(MpMessage* msg);
	void excuteGetBalance(MpMessage* pMsg);
	void excuteloadBetMoney(MpMessage* pMsg);
	void excuteChangeInfo(MpMessage* pMsg);
	void excuteGetSMSInfo(MpMessage* pMsg);
	void excuteGetSubInfo(MpMessage* pMsg);
	void excuteRecharge(MpMessage* pMsg);
	void excutegetRechargeRate(MpMessage* pMsg);
	void executeJoinTable(MpMessage* pMsg);
	void executeActiveAccount(MpMessage* pMsg);
	void executeResendOTP(MpMessage* pMsg);

	void onBackPressed();
	void refreshPlayerInfo();

private:
	void showNotice(const std::string& content);
	void showLoading(bool bLoading);

private:
	void getSmsInfo();
	void getSubInfo();
	void getVisaInfo();
	void getRechargeRate();
	void sendChangePass(std::string pOldPass, std::string pNewPass, std::string pReNewPass);
	void sendOtpToActiveAccount(std::string otp);
	void resendOTP();

private:
	LayerColor* mLayerBoxUpdateAcount;
	Sprite* mBackground;
	EGButtonSprite* mBtnAccInfo, *mBtnRecharge, *mBtnTop, *mBtnBonus, *mBtnShop, *mBtnMore;
	EGButtonSprite *mBtnFriend, *mBtnInvetory, *mBtnNews, *mBtnMail;
	EGButtonSprite* mBtnBack;
	BoxNotice* mBoxNotice;
	BoxTopUp* mBoxTopUp;
	BoxTopUpCard* mBoxTopUpCard;
	Loading* mLoading;
	EGButtonSprite* mGiftCode;
	BoxInfoGame * mBoxInfoGame;
	EditBox* mEdit_Name, *mEditPassWord1, *mEditPassWord2;
	//BoxNew* mBoxNew;
	BoxActiveAccount* mBoxActive;

	Sprite* mTitle;
	EGButtonSprite* mBtnPlayNormal, *mBtnPlayRank;
	EGButtonSprite* mBtnSmall, *mBtnNormal, *mBtnLarge, *mBtnHuge;

	Sprite* mSpriteAvatar;
	Label* mLblName, *mLblBalance;
	string _transId = "";

	//CREATE_FUNC(MainScene);
	void initScene(bool isNotice);

	void showTopUp();
	void showTopUpByCard();
	void showGiftcode();
	void sendGiftcode();

	void showBonus();
	void showMore();
	void gotoFriendScene();
	void gotoTopScene();
	void gotoShopScene();
	void gotoMailScene();

	void gotoNormalGame();
	void gotoRankGame();
	void gotoInventory();
	void gotoNews();
	void logout();
	void showInfo(std::string pUserName);
	Scene* createGame(void* pInfo);

	void update(float dt);
	void rechargeBySMS(int pIndex);
	void recharge(std::string pSerial, std::string pNumber, std::string pTelco);
	virtual void editBoxReturn(EditBox* editBox){
#if(CC_TARGET_PLATFORM == CC_PLATFORM_WP8 || CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
#else
		if (editBox == mEdit_Name) {
			mEditPassWord1->touchDownAction(NULL, cocos2d::ui::Widget::TouchEventType::CANCELED);
		}
		else if (editBox == mEditPassWord1) {
			if (mEditPassWord2->isVisible()) {
				mEditPassWord2->touchDownAction(NULL, cocos2d::ui::Widget::TouchEventType::CANCELED);
			}
		}
#endif
	};
};

#endif