#ifndef __LAYER_NOTICE_H_INCLUDED__
#define __LAYER_NOTICE_H_INCLUDED__
#include "cocos2d.h"
#include "./mpclient/protocol/MpMessage.h"

using namespace cocos2d;
#define NG_Home  0xFF + 1
#define NG_ALL  0xFFFF + 1
class LayerNotice : public Node {
public:
	static LayerNotice* createLayer();
	void initLayer();
	void showToast(std::string pToast);
	void showToast(std::string pToast, float pWidth, Vec2 pPosition);

	void showToastReal(std::string pToast, float pWidth, Vec2 pPosition, float pDuration);
	void showNextToast();
	void senMessage(mp::protocol::MpMessage* msg, bool isWatingResponse);
	void showToastReal(std::string pToast);
	void showToastReal(std::string pToast, float pWidth, Vec2 pPosition);
	void connect();
	void setCurrentState(int pState);
	void updateNotice(float delta);
	std::string mIP;
	int mPort;
	void resetTimeCount();
private:
	int mState;
	std::vector<std::string> mLstToast;
	float mDelayTime;
};

#endif