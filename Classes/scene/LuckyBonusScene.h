#ifndef __LUCKYBONUSSCENE_H_INCLUDED__
#define __LUCKYBONUSSCENE_H_INCLUDED__

#include "cocos2d.h"
#include "EGScene.h"
#include "EGButtonSprite.h"
#include "EGSprite.h"
#include "BoxNotice.h"
#include "WXmlReader.h"
#include "Loading.h"

USING_NS_CC;

class LuckyBonusScene : public EGScene {
public:
	static Scene* scene();

	void onBackPressed();
protected:
	int mCircleTurn;
	bool isDrawing;
private:
	WXmlReader* mXml;
	EGSprite* mBackground, *mLuckyBar, *mLuckyCirle, *mLuckyPrice, *mPriceReceive;
	EGButtonSprite* mBtnBack, *mBtnStartLucky;
	BoxNotice* mBoxNotice;
	Loading* mLoading;
	void startLucky();
	void runLucky();
	void initScene();
	void update(float dt);
};

#endif