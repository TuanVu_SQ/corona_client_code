#ifndef __CHOOSEROOMSCENE_H_INCLUDED__
#define __CHOOSEROOMSCENE_H_INCLUDED__
#include "cocos2d.h"
#include "GameInfo.h"
#include "EGButtonSprite.h"
#include "mpjointablemessageresponse.h"
#include "ChooseRoomLayer.h"
#include "BoxResize.h"
#include "WXmlReader.h"
#include "Loading.h"
#include "GameScene.h"
#include "EGScene.h"
#include "BoxNotice.h"

using namespace cocos2d;
struct roomStruct 
{
	Loading* pLoading;
	unsigned int pTypeRoom;
	unsigned int pMaxPlayer;
	unsigned int pBetMoney;
	unsigned int pBetMoneyDefault;
	unsigned int pMinMoney;
	std::string pRoomLevelstr;
	bool pHideRoom;
	int mIDRoom;
	int mRatio;
	uint32_t mTableID;
	int mGameID;
	int mBackGroundID;
	int mDuration;
	int mDurationReadyNormal;
	int mDurationReadyHost;
	bool iCreater;
	std::vector<ClientPlayerInfoEX>* mListPlayer;
	std::vector<ClientItemInfo> mListItemSp;
	std::vector<uint32_t>* mListBetMoney;
	std::vector<uint16_t> mListPrice;
	std::vector<uint16_t> mListXFactor;
	BoxNotice* mBoxNotice;
};

struct loadRoomStruct {

	std::string mRoomLevel;
	std::string mGameID;
	Sprite* pSpriteLoading;
	Loading* pLoading;
	Sprite* pButtonChoose;
	std::vector<ClientTableInfo>* mCacheListTable;
	std::vector<uint32_t>* mListBetMoney;
	std::vector<uint8_t>* mListRoom;
	int mRoomID;
};

class ChooseRoomScene : public EGScene 
{
public:
	static Scene *createScene(int pGameID);
	static Scene* createSceneFromGame(int pGameID);
	void initScene(int pGameID);
	void choosePlace(int index);
	void setEnableButtonChoosePlace(bool pBool);
	void update(float pSec);
	void nextStageChooseRoom();
	void loadListRoom();
	void updateStage(float pDuration);
	void backStage();	
	void updateJoinTable(float pSec);
	Scene* createGame(void* pInfo);
	void createTab(int pCount);
	void chooseRoom(int index);
	void loadListTable();
	void chooseTab(int index, int pCountTab);
	Label* addTextforButton(Sprite* pButton, std::string pString);
	void updateInfoTable(float pSec);
	void onBackPressed();
	void chooseHideRoom();
	void replay();

	virtual void pauseGame();
	virtual void resumeGame();
	void joinInvite();
	void playNow();
	void updateListBetMoney(std::vector<uint32_t> vtBetMoney);

private:
	void reloadTableRequest(float dt);

private:
	void executeLoadRoom(MpMessage* pMsg);
	void executeLoadTable(MpMessage* pMsg);
	void executeJoinTable(MpMessage* pMsg);

private:
	Loading* mLoadingMain;
	BoxNotice* mBoxNotice;
	bool m_isResume;
	
	Sprite* _boxMoneyGame;
	Sprite* mBarTitle;
	Layer* mMainLayer;
	LayerColor* mLayerPlace;
	ChooseRoomLayer* mLayerChooseRoom;
	std::vector<ClientTableInfo>* mCacheListRoom;
	std::vector<ClientPlayerInfoEX>* mPlayerList;
	std::vector<uint8_t>* mListRoom;
	std::string pRoomLevelstr;
	Sprite* mLoading;
	int mCurrentStage,mGameID,mRoomID,mCountTab;
	bool pTest;
	bool _isLoadTableContinue;
	std::vector<uint32_t>*  mListMoneyBet;
	
	void checkReadyCreateRoom();
	roomStruct * mInfoRoom;
	loadRoomStruct* roomLoadStruct;
};
#endif