#include "WHelper.h"
#include "WHelper-Android.h"
#include "md5.h"
#include "WSupport.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#endif
#pragma warning(push)
#pragma warning(disable: 4996)
#include "network/HttpClient.h"
#pragma warning(pop)
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#pragma GCC diagnostic pop
#endif
using namespace network;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
extern "C"
{
	JNIEXPORT void JNICALL Java_org_cocos2dx_callback_Cocos2dxCallback_callbackFacebook(JNIEnv* env, jobject thiz, jstring facebookId, jstring token) {
		std::string _facebookId = env->GetStringUTFChars(facebookId, NULL);
		std::string _facebookToken = env->GetStringUTFChars(token, NULL);
		WHelper::getInstance()->responeDataToLoginFacebook(_facebookId, _facebookToken);
	}
};
#endif

#define CLASS_NAME "org.cocos2dx.callback.Cocos2dxCallback"

WHelper::WHelper() {
	_deviceId = "TEST_DEVICE_ID";
	_cpid = "1";
	_country = "en";
	_package = "";
	_telco = "";
	_chargeID = "1";
	_phoneNumber = "";
	_hk = "";
	_hm = "";
	_hc = "";
	_pk = "";
	_tp = "";
}

WHelper::~WHelper() {

}

static WHelper* _whelper = nullptr;
WHelper* WHelper::getInstance() {
	if (_whelper == nullptr) _whelper = new WHelper();
	return _whelper;
}

void WHelper::loginFacebook() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	//WHelperAndroid::getInstance()->callFunctionToJava(CLASS_NAME, "loginFacebook");
#endif
}

void WHelper::responeDataToLoginFacebook(std::string facebookId, std::string tokenID) {
	if (facebookId.length() == 0 || tokenID.length() == 0) {
		log("facebookId,tokenID == null");
		return;
	}
	cocos2d::Director::getInstance()->getScheduler()->performFunctionInCocosThread([=]{
		if (_facebookCallback)
			_facebookCallback(facebookId, tokenID);
	});
}

void WHelper::setDeviceID(std::string value) {
	_deviceId = value;
}

void WHelper::setCountry(std::string value) {
	_country = value;
}

void WHelper::setPackage(std::string value) {
	_package = value;
}

void WHelper::setTelco(std::string value) {
	_telco = value;
}

void WHelper::setCPID(std::string value) {
	_cpid = value;
}

void WHelper::setChargeID(std::string value) {
	_chargeID = value;
}

void WHelper::setPhoneNumber(std::string value) {
	_phoneNumber = value;
}

void WHelper::setHK(std::string value) {
	_hk = value;
}

void WHelper::setHM(std::string value) {
	_hm = value;
}

void WHelper::setHC(std::string value) {
	_hc = value;
}

void WHelper::setPK(std::string value) {
	_pk = value;
}

void WHelper::setTP(std::string value) {
	_tp = value;
}

std::string WHelper::getDeviceID() {
	std::string value;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	value =  WHelperAndroid::getInstance()->getStringToJava(CLASS_NAME, "getDeviceInfo", WSupport::convertIntToString(JniCallback::DEVICESID));
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	value = WHelperIOS::getInstance()->getDeviceID();
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	value = WHelperWinRT::getInstance()->getDeviceID();
#endif
	if (value.length() == 0) {
		value = _deviceId;
	}
	return value;
}

std::string WHelper::getCountry() {
	std::string value;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	value =  WHelperAndroid::getInstance()->getStringToJava(CLASS_NAME, "getDeviceInfo", WSupport::convertIntToString(JniCallback::COUNTRY));
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	value = WHelperIOS::getInstance()->getCountry();
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	value = WHelperWinRT::getInstance()->getCountry();
#endif
	if (value.length() == 0) {
		value = _country;
	}
	return value;
}

std::string WHelper::getPackage() {
	std::string value;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	value =  WHelperAndroid::getInstance()->getStringToJava(CLASS_NAME, "getDeviceInfo", WSupport::convertIntToString(JniCallback::PACKAGE));
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	value = WHelperIOS::getInstance()->getPackage();
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	value = WHelperWinRT::getInstance()->getPackage();
#endif
	if (value.length() == 0) {
		value = _package;
	}
	log("value: %s", value.c_str());
	return value;
}

std::string WHelper::getTelco() {
	std::string value;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	value =  WHelperAndroid::getInstance()->getStringToJava(CLASS_NAME, "getDeviceInfo", WSupport::convertIntToString(JniCallback::TELCO));
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	value = WHelperIOS::getInstance()->getTelco();
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	value = WHelperWinRT::getInstance()->getTelco();
#endif
	if (value.length() == 0) {
		value = _telco;
	}
	return value;
}

std::string WHelper::getCPID() {
	std::string value;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	value =  WHelperAndroid::getInstance()->getStringToJava(CLASS_NAME, "getDeviceInfo", WSupport::convertIntToString(JniCallback::CPID));
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	value = WHelperIOS::getInstance()->getCPID();
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	value = WHelperWinRT::getInstance()->getCPID();
#endif
	if (value.length() == 0) {
		value = _cpid;
	}
	return value;
}

std::string WHelper::getChargeID() {
	std::string value;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	value =  WHelperAndroid::getInstance()->getStringToJava(CLASS_NAME, "getDeviceInfo", WSupport::convertIntToString(JniCallback::CHARGE_ID));
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	value = WHelperIOS::getInstance()->getChargeID();
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	value = WHelperWinRT::getInstance()->getChargeID();
#endif
	if (value.length() == 0) {
		value = _chargeID;
	}
	return value;
}

std::string WHelper::getPhoneNumber() {
	std::string value;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	value =  WHelperAndroid::getInstance()->getStringToJava(CLASS_NAME, "getDeviceInfo", WSupport::convertIntToString(JniCallback::PHONE_NUMBER));
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	value = WHelperIOS::getInstance()->getPhoneNumber();
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	value = WHelperWinRT::getInstance()->getPhoneNumber();
#endif
	if (value.length() == 0) {
		value = _phoneNumber;
	}
	return value;
}

std::string WHelper::getHK() {
	std::string value;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	value = WHelperAndroid::getInstance()->getStringToJava(CLASS_NAME, "getDeviceInfo", WSupport::convertIntToString(JniCallback::HK));
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)

#endif
	if (value.length() == 0) {
		value = _hk;
	}
	return value;
}

std::string WHelper::getHM() {
	std::string value;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	value =  WHelperAndroid::getInstance()->getStringToJava(CLASS_NAME, "getDeviceInfo", WSupport::convertIntToString(JniCallback::HM));
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)

#endif
	if (value.length() == 0) {
		value = _hm;
	}
	return value;
}

std::string WHelper::getHC() {
	std::string value;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	value =  WHelperAndroid::getInstance()->getStringToJava(CLASS_NAME, "getDeviceInfo", WSupport::convertIntToString(JniCallback::HC));
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)

#endif
	if (value.length() == 0) {
		value = _hc;
	}
	return value;
}

std::string WHelper::getPK() {
	std::string value;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	value =  WHelperAndroid::getInstance()->getStringToJava(CLASS_NAME, "getDeviceInfo", WSupport::convertIntToString(JniCallback::PK));
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)

#endif
	if (value.length() == 0) {
		value = _pk;
	}
	return value;
}

std::string WHelper::getTP() {
	std::string value;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	value =  WHelperAndroid::getInstance()->getStringToJava(CLASS_NAME, "getDeviceInfo", WSupport::convertIntToString(JniCallback::TP));
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)

#endif
	if (value.length() == 0) {
		value = _tp;
	}
	return value;
}

std::string WDeviceInfo::getDeviceID() {
	return WHelper::getInstance()->getDeviceID();
}

std::string WDeviceInfo::getCPID() {
	return WHelper::getInstance()->getCPID();
}

std::string WDeviceInfo::getCountryCode() {
	return WHelper::getInstance()->getCountry();
}

std::string WDeviceInfo::getPackageName() {
	return WHelper::getInstance()->getPackage();
}

std::string WDeviceInfo::getTelco() {
	return WHelper::getInstance()->getTelco();
}

std::string WDeviceInfo::getChargeID() {
	return WHelper::getInstance()->getChargeID();
}

std::string WDeviceInfo::getSecurityString(std::string tp) {
	std::string hk = WHelper::getInstance()->getHK();
	std::string hm = WHelper::getInstance()->getHM();
	std::string hc = WHelper::getInstance()->getHC();
	std::string pk = WHelper::getInstance()->getPK();
	std::string keyMd5 = "89f4f96a62c7a49b204921033b68f80c";

	std::string strSecurity;
	int timeStamp = atoi(tp.c_str());
	if (timeStamp % 2 == 0)
		strSecurity = keyMd5 + pk + hk + tp;
	else strSecurity = tp + hm + keyMd5 + hc;

	strSecurity = md5(strSecurity.c_str());
	return strSecurity;
}

std::string WHelper::getDeviceName() {
	std::string value;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	value =  WHelperAndroid::getInstance()->getStringToJava(CLASS_NAME, "getDeviceInfo", WSupport::convertIntToString(JniCallback::DEVICENAME));
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)

#endif
	return value;
}