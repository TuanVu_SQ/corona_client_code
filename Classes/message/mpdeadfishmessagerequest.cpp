#include "mpdeadfishmessagerequest.h"
#include "MessageType.h"

MpDeadFishMessageRequest::MpDeadFishMessageRequest() :MpMessageRequest(MP_DEAD_FISH)
{
}

void MpDeadFishMessageRequest::setListFishes(const std::vector<uint8_t> &vtFish)
{
	std::string listFish;
	for (int i = 0; i < vtFish.size(); i++){
		listFish.push_back(vtFish[i]);
	}
	this->addString(MP_PARAM_LIST_FISH, listFish);
}

bool MpDeadFishMessageRequest::getListFish(std::vector<uint8_t> &vtFish) const
{
    std::string data = this->getString(MP_PARAM_LIST_FISH);
    if(data == "") return true;
    for(size_t i = 0; i < data.size(); i++) vtFish.push_back((uint8_t)data[i]);
    return false;
}

void MpDeadFishMessageRequest::setTableId(uint32_t tableId)
{
	this->addInt(MP_PARAM_TABLE_ID, tableId);
}

uint32_t MpDeadFishMessageRequest::getTableId() const
{
	return getInt(MP_PARAM_TABLE_ID);
}
