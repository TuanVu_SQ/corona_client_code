#ifndef __MPGETINFOPURCHASEMESSAGERESPONSE_H_INCLUDED__
#define __MPGETINFOPURCHASEMESSAGERESPONSE_H_INCLUDED__

#include "mpmessageresponse.h"
#include "ClientPlayerInfo.h"
#include <vector>

class MpGetInfoPurchaseMessageResponse :public MpMessageResponse {
public:
	MpGetInfoPurchaseMessageResponse();

	/*void setServiceNumber(const std::string& pServiceNumber);
	std::string getServiceNumber() const;

	void setSyntax(const std::string& pSyntax);
	std::string getSyntax() const;*/

	bool getVtRecharge(std::vector<RechargeInfo> &vtRecharge) const;
	void setVtRecharge(const std::vector<RechargeInfo> &vtRecharge);

        bool getVtExchange(std::vector<ExchangeInfo> &vtExchange) const;
        void setVtExchange(const std::vector<ExchangeInfo> &vtExchange);

        bool getVtSubcribe(std::vector<SubcribeInfo> &vtSubcribe) const;
        void setVtSubcribe(const std::vector<SubcribeInfo> &vtSubcribe);

	bool getVtRechargeIAP(std::vector<RechargeVISAInfo> &vtRechargeIAP) const;
	void setVtRechargeIAP(const std::vector<RechargeVISAInfo> &vtRechargeIAP);

	void setEnable(uint8_t enable); // 1 = enable ; 0 = disable
	uint8_t getEnable() const; // 1 = enable ; 0 = disable
};

#endif // MPLOADTOPMESSAGERESPONSE_H
