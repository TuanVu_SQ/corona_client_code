#include "mpjointablemessagerequest.h"
#include "MessageType.h"
using std::string;

MpJoinTableMessageRequest::MpJoinTableMessageRequest() :MpMessageRequest(MP_MSG_JOIN_TABLE)
{}

MpJoinTableMessageRequest::MpJoinTableMessageRequest(uint32_t type) : MpMessageRequest(type)
{}

void MpJoinTableMessageRequest::setTableId(uint8_t roomId)
{
	addInt(MP_PARAM_TABLE_ID, roomId);
}

uint8_t MpJoinTableMessageRequest::getTableId() const
{
    string data = this->getString(MP_PARAM_TABLE_ID);
    if(data.length() == 0) return 0;
    return (uint8_t)*data.c_str();
}

//void MpJoinTableMessageRequest::setGameId(uint16_t gameId)
//{
//    gameId = htons(gameId);
//    addParam(m_pMsgHdr, MP_GAME_ID, (const char*) &gameId, 2);
//}

//uint16_t MpJoinTableMessageRequest::getGameId() const
//{
//    string data;
//    if(this->getString(MP_ROOMID,data) == false) return 0;
//    if(data.length() == 0) return 0;

//    uint16_t gameId = *((unsigned short*) data.c_str());
//    return ntohs(gameId);
//}

//void MpJoinTableMessageRequest::setGameLevel(uint8_t level)
//{
//    addParam(m_pMsgHdr, MP_GAME_LVL, &level, 1);
//}

//uint8_t MpJoinTableMessageRequest::getGameLevel() const
//{
//    string data;
//    if(this->getString(MP_GAME_LVL,data) == false) return 0;
//    if(data.length() == 0) return 0;
//    return *data.c_str();
//}
