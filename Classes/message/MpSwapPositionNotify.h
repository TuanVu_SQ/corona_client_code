#ifndef MPSWAPPOSITIONNOTIFY_H
#define MPSWAPPOSITIONNOTIFY_H

#include "MpSwapPositionRequest.h"
#include <vector>

class MpSwapPositionNotify : public MpMessage
{
public:
    MpSwapPositionNotify();
    void setListPlayerPosition(const std::vector<PlayerPosition> &vtPlayerPosition);
    bool getListPlayerPosition(std::vector<PlayerPosition> &vtPlayerPosition) const;
};
#endif // MPSWAPPOSITIONNOTIFY_H
