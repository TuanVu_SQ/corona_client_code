#include "mpgetinfosmsmessagerequest.h"
#include "MessageType.h"

MpGetInfoSMSMessageRequest::MpGetInfoSMSMessageRequest() : MpMessageRequest(MP_MSG_GETINFO_SMS)
{}

void MpGetInfoSMSMessageRequest::setChargeID(uint8_t pChargeID)
{
	this->addInt(MP_PARAM_CHARGE_ID, pChargeID);
}

void MpGetInfoSMSMessageRequest::setOS(const std::string &os)
{
    this->addString(MP_PARAM_OS, os);
}

void MpGetInfoSMSMessageRequest::setCountry(const std::string &country) 
{
	this->addString(MP_PARAM_COUNTRY, country);
}