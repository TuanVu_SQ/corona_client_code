#include "mpbuyitemmessagerequest.h"
#include "MessageType.h"

MpBuyItemMessageRequest::MpBuyItemMessageRequest() : MpMessageRequest(MP_MSG_BUY_ITEM)
{}

bool MpBuyItemMessageRequest::getItemID(uint8_t &pItemID) const 
{
	pItemID = getInt(MP_PARAM_ITEM_ID);
	return true;
}

void MpBuyItemMessageRequest::setItemID(uint8_t pItemID) 
{
	addInt(MP_PARAM_ITEM_ID, pItemID);	
}

bool MpBuyItemMessageRequest::getItemType(uint8_t &pItemType) const {
	pItemType = getInt(MP_TYPE);
	return true;
}

void MpBuyItemMessageRequest::setItemType(uint8_t pItemType) {
	this->addInt(MP_TYPE, pItemType);
}

bool MpBuyItemMessageRequest::getItemCounter(uint8_t &pItemCounter) const {
	pItemCounter = getInt(MP_QUANTITY);
	return true;
}

void MpBuyItemMessageRequest::setItemCounter(uint8_t pItemCounter)
{
	this->addInt(MP_QUANTITY, pItemCounter);
}

bool MpBuyItemMessageRequest::getExpireType(uint8_t &pExpireType) const {
	pExpireType = getInt(MP_EX_TYPE);
	return true;
}

void MpBuyItemMessageRequest::setExpireType(uint8_t pExpireType)
{
	this->addInt(MP_EX_TYPE, pExpireType);
}
