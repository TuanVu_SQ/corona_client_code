#ifndef MPSWAPPOSITIONRESPONSE_H
#define MPSWAPPOSITIONRESPONSE_H
#include "mpmessagerequest.h"
#include <vector>
#include "MpSwapPositionRequest.h"

class MpSwapPositionResponse :public MpMessageRequest
{
public:

    MpSwapPositionResponse();
    ~MpSwapPositionResponse();
    void setListPlayerPosition(const std::vector<PlayerPosition> &vtPlayerPosition);
    bool getListPlayerPosition(std::vector<PlayerPosition> &vtPlayerPosition) const;
};
#endif // MPSWAPPOSITIONRESPONSE_H
