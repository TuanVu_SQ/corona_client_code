#include "mploadtopmessageresponse.h"
#include "MessageType.h"
#include "MessageParam.h"
#include <string.h>

MpLoadTopMessageResponse::MpLoadTopMessageResponse() : MpMessageResponse(MP_MSG_GET_TOP_ACK)
{}

bool MpLoadTopMessageResponse::getTop(std::vector<TopInfo> &vtTop) const
{
    std::string data = this->getString(MP_PARAM_LISTTOP);
    if(data == "") return false;
    TopInfo topInfo;
    size_t len = 0;
    while (len < data.size())
    {
        topInfo.username = "";
        while(data[len] != 0)
        {
            topInfo.username += data[len];
            len++;
        }
        len++;      // trim 0
        topInfo.avatarId = (*(uint32_t*)(data.data() + len));
        topInfo.avatarId = ntohl(topInfo.avatarId);
        len += 4;

        topInfo.balance = *((uint32_t*)(data.data() + len));
        topInfo.balance = ntohl(topInfo.balance);
        len += 4;

        topInfo.level = *(data.data() + len);
        len++;

        topInfo.status = *(data.data() + len);
        len++;

        vtTop.push_back(topInfo);
    }
    return true;
}

void MpLoadTopMessageResponse::setTop(const std::vector<TopInfo> &vtTop)
{
    TopInfo topInfo;
    uint8_t buffer[10240];
    uint8_t *ptr = buffer;
    size_t len = 0;
    for(size_t i = 0; i < vtTop.size(); ++i)
    {
        if(len >= 10240 -10) return;
        topInfo = vtTop[i];
        // username
        memcpy(ptr + len, topInfo.username.c_str(), topInfo.username.size());
        len += topInfo.username.size();

        *(ptr + len) = 0;
        len++;

        topInfo.avatarId = htonl(topInfo.avatarId);
        *(uint32_t*)(ptr+len) = topInfo.avatarId;
        len += 4;

        topInfo.balance = htonl(topInfo.balance);
        *(uint32_t*)(ptr+len) = topInfo.balance;
        len += 4;

        *(ptr + len) = topInfo.level;
        len++;

        *(ptr + len) = topInfo.status;
        len++;
    }

    if(len > 0)
        this->addString(MP_PARAM_LISTTOP, std::string((const char *)buffer, len));
}
