#include "mpfirebulletmessagerequest.h"
#include "MessageType.h"

MpFireBulletMessageRequest::MpFireBulletMessageRequest() : MpMessageRequest(MP_MSG_FIRE_BULLET)
{}


void MpFireBulletMessageRequest::setBulletPosition(const MpPoint &point)
{
	uint32_t p = (uint32_t)(point.x << 16 | point.y);
	this->addInt(MP_PARAM_BULLET_POSITION, p);
}

bool MpFireBulletMessageRequest::getBulletPosition(MpPoint &point) const
{
	uint32_t n = getInt(MP_PARAM_BULLET_POSITION);
	point.x = (n >> 16);
	point.y = static_cast<uint16_t>(n & 0xFFFF);
	return true;
}

void MpFireBulletMessageRequest::setGunLevel(uint8_t level)
{
    this->addInt(MP_PARAM_GUN_LEVEL, level);
}

bool MpFireBulletMessageRequest::getGunLevel(uint8_t &level) const
{
	level = getInt(MP_PARAM_GUN_LEVEL);
    return true;
}

void MpFireBulletMessageRequest::setTableId(uint32_t tableId)
{
	this->addInt(MP_PARAM_TABLE_ID, tableId);
}

uint32_t MpFireBulletMessageRequest::getTableId() const
{
	return getInt(MP_PARAM_TABLE_ID);
}

