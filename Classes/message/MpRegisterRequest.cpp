#include "MpRegisterRequest.h"
#include "MessageType.h"

MpRegisterRequest::MpRegisterRequest() : MpMessageRequest(MP_MSG_REGISTER)
{}

const std::string MpRegisterRequest::getOS() const
{
    return this->getString(MP_PARAM_OS);
}

const std::string MpRegisterRequest::getPassword() const
{
    return this->getString(MP_PARAM_PASSWORD);
}

const std::string MpRegisterRequest::getCRC() const
{
    return this->getString(MP_PARAM_CRC);
}

const std::string MpRegisterRequest::getFullname() const
{
    return this->getString(MP_PARAM_FULLNAME);
}

const std::string MpRegisterRequest::getCpId() const
{
    return this->getString(MP_PARAM_CP_ID);
}

const std::string MpRegisterRequest::getUsername() const
{
    return this->getString(MP_PARAM_USERNAME);
}

void MpRegisterRequest::setOS(const std::string &os)
{
    this->addString(MP_PARAM_OS, os);
}

void MpRegisterRequest::setPassword(const std::string &password)
{
    this->addString(MP_PARAM_PASSWORD, password);
}

void MpRegisterRequest::setFullname(const std::string &fullname)
{
    this->addString(MP_PARAM_FULLNAME, fullname);
}

void MpRegisterRequest::setCpId(const std::string &cpId)
{
    this->addString(MP_PARAM_CP_ID, cpId);
}

void MpRegisterRequest::setUsername(const std::string &username)
{
    //this->addString(MP_PARAM_CP_ID, username);
    //uint32_t h = hash(username);
    //this->addInt(MP_PARAM_CRC, h);
}


