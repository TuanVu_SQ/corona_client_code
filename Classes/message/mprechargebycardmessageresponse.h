#ifndef __MPRECHARGEBYCARDMESSAGERESPONSE_H_INCLUDED__
#define __MPRECHARGEBYCARDMESSAGERESPONSE_H_INCLUDED__

#include "mpmessageresponse.h"
#include <vector>

class MpRechargeByCardMessageResponse :public MpMessageResponse {
public:
	MpRechargeByCardMessageResponse();
	bool getBalance(uint32_t &pBalance) const;
	void setBalance(uint32_t pBalance);
};

#endif // MPLOADTOPMESSAGERESPONSE_H
