#ifndef MPLISTFISHESNOTIFY_H
#define MPLISTFISHESNOTIFY_H
#include "ClientPlayerInfo.h"
#include "./mpclient/protocol/MpMessage.h"

class MpListFishesNotify : public mp::protocol::MpMessage
{
public:
    MpListFishesNotify();
    void setListFishes(const std::vector<ClientFishInfo> & vtFishes);
    bool getLishFishes(std::vector<ClientFishInfo> &vtFishes) const;
private:
};

#endif // MPLISTFISHESNOTIFY_H
