#include "mppingmessageresponse.h"
#include "MessageType.h"

MpPingMessageResponse::MpPingMessageResponse() : MpMessageResponse(MP_MSG_PING_ACK)
{}

void MpPingMessageResponse::setListEvent(const std::vector<uint8_t> &vtEvent)
{
	if (vtEvent.size() == 0) return;
	this->addString(MP_EVENTID, std::string((const char *)vtEvent[0], vtEvent.size()));
}

bool MpPingMessageResponse::getListEvent(std::vector<uint8_t> &vtEvent) const
{
	std::string data = this->getString(MP_EVENTID);
	if (data == "") return true;
	for (size_t i = 0; i < data.size(); i++) vtEvent.push_back((uint8_t)data[i]);
	return false;
}
