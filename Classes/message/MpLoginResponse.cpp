#include "MpLoginResponse.h"
#include "MessageType.h"

#if (MP_TARGET_PLATFORM == MP_PLATFORM_WIN32) || (MP_TARGET_PLATFORM == MP_PLATFORM_WINRT) || (MP_TARGET_PLATFORM == MP_PLATFORM_WP8)
	#include <Winsock2.h>
#else
	#include "netinet/in.h"
#endif

MpLoginResponse::MpLoginResponse() : MpMessageResponse(MP_MSG_LOGIN_ACK){}

const std::string MpLoginResponse::getTokenId() const
{
    return getString(MP_PARAM_TOKENID);    
}

const std::string MpLoginResponse::getFullname() const
{
    return getString(MP_PARAM_FULLNAME);    
}

const std::string MpLoginResponse::getEmail() const
{
    return getString(MP_PARAM_EMAIL);    
}

const std::vector<MpLoginResponse::GameAddress> MpLoginResponse::getGameAddress() const
{
	std::vector<MpLoginResponse::GameAddress> gas;
	GameAddress ga;
	std::string data = getString(MP_PARAM_GAME_SERVER_ADDR);
	if (data.size() < 7) return gas;
	const char *ptr = data.data();
	const char *end = data.data() + data.size();
	while (ptr < end)
	{
		ga.gameId = *ptr;
		++ptr;

		ga.port = *((uint16_t*)ptr);
		ga.port = ntohs(ga.port);
		ptr += 2;
		ga.domain = ptr;
		gas.push_back(ga);
		ptr += ga.domain.size() + 1;
	}
	return gas;
}

uint64_t MpLoginResponse::getBalance() const
{
    return getInt(MP_PARAM_BALANCE);
}

MpLoginResponse::Date MpLoginResponse::getBirthday() const
{
    std::string data = getString(MP_PARAM_BIRTHDAY);
    Date d;
    if(data.size() == 4)
    {
        d.from_string(data);
    }
    return d;
}

uint32_t MpLoginResponse::getAvatarId() const
{
    return getInt(MP_PARAM_AVATAR_ID);
}

uint32_t MpLoginResponse::getLevel() const
{
    return getInt(MP_PARAM_LEVEL);
}

uint32_t MpLoginResponse::getGender() const
{
    return getInt(MP_PARAM_GENDER);
}

void MpLoginResponse::setTokenId(const std::string &tokenId)
{
    this->addString(MP_PARAM_SESSION_ID, tokenId);
}

void MpLoginResponse::setFullname(const std::string &fullname)
{
    this->addString(MP_PARAM_FULLNAME, fullname);
}

void MpLoginResponse::setBirthday(const MpLoginResponse::Date &birthday)
{
    this->addString(MP_PARAM_BIRTHDAY, birthday.to_string());
}

void MpLoginResponse::setEmail(const std::string &email)
{
    this->addString(MP_PARAM_EMAIL, email);
}

void MpLoginResponse::setBalance(uint64_t balance)
{
    this->addInt(MP_PARAM_BALANCE, balance);
}

void MpLoginResponse::setAvatarId(uint32_t avatarId)
{
    this->addInt(MP_PARAM_AVATAR_ID, avatarId);
}

void MpLoginResponse::setGender(uint32_t gender)
{
    this->addInt(MP_PARAM_GENDER, gender);
}

void MpLoginResponse::setLevel(uint32_t level)
{
    this->addInt(MP_PARAM_LEVEL, level);
}


const std::string MpLoginResponse::GameAddress_s::to_string() const
{
    std::string data;
    data += (char) gameId;
    uint16_t tPort = htons(port);
    data.append((char*) &tPort, (char*)(&tPort + 2));
    data.append(domain);
    return data;
}

void MpLoginResponse::GameAddress_s::from_string(const std::string &g_addr)
{
    if(g_addr.size() < 7) return;
    const char *ptr = g_addr.data();
    gameId  = *((uint8_t *)ptr++);
    port    = ntohs(*((uint16_t *)ptr));
    ptr     += 2;

    domain = ptr;
}


const std::string MpLoginResponse::Date_s::to_string() const
{
    std::string data;
    data += (char) day;
    data += (char) month;
    uint16_t tYear = htons(year);
    data.append((char*) &tYear, (char*)(&tYear + 2));
    return data;
}

void MpLoginResponse::Date_s::from_string(const std::string &date)
{
    if(date.size() != 4) return;
    day     = date[0];
    month   = date[1];
    year    = ntohs(*((uint16_t *)date.data() + 2));
}
void MpLoginResponse::getListByTagRate(uint32_t  tag, std::vector<float>& listRate){
	std::string data = getString(tag);
	std::vector<uint16_t> numbers;
	{
		uint16_t n;
		for (size_t i = 0; i < data.size() - 1; i += 2)
		{
			n = *((const uint16_t*)(data.data() + i));
			n = ntohs(n);
			numbers.push_back(n);
		}
	}
	for (size_t i = 0; i < numbers.size(); i++){
		listRate[i] = numbers[i];
		listRate[i] = (listRate[i] / 100 - 100);
	}
}
