#include "mpdeadfishmessagenotify.h"
#include "MessageType.h"

#if (MP_TARGET_PLATFORM == MP_PLATFORM_WIN32) || (MP_TARGET_PLATFORM == MP_PLATFORM_WINRT) || (MP_TARGET_PLATFORM == MP_PLATFORM_WP8)
#include <Winsock2.h>
#else
#include "netinet/in.h"
#endif

MpDeadFishMessageNotify::MpDeadFishMessageNotify() :MpMessage(MP_DEAD_FISH_NTFY)
{
}

bool MpDeadFishMessageNotify::getUsername(std::string &username) const
{
	username = this->getString(MP_PARAM_USERNAME);
	if (username == ""){
		return false;
	}
	return true;
}

void MpDeadFishMessageNotify::setUsername(const std::string &username)
{
	this->addString(MP_PARAM_USERNAME, username);
}

void MpDeadFishMessageNotify::setListFist(const std::vector<uint8_t> &vtFish)
{
    if(vtFish.size()== 0) return;
	std::string listF(vtFish[0], vtFish.size());
	this->addString(MP_PARAM_LIST_FISH, listF);
}

bool MpDeadFishMessageNotify::getFishMoney(uint32_t &fishMoney) const
{
	fishMoney = getInt(MP_PARAM_FISH_MONEY);
    return true;
}

void MpDeadFishMessageNotify::setFishMoney(uint32_t fishMoney)
{
    fishMoney = htonl(fishMoney);
	this->addInt(MP_PARAM_FISH_MONEY, fishMoney);
}

bool MpDeadFishMessageNotify::getFireMoney(uint32_t &fireMoney) const
{
	fireMoney = getInt(MP_PARAM_FIRE_MONEY);
	return true;
}

void MpDeadFishMessageNotify::setFireMoney(uint32_t fireMoney)
{
    fireMoney = htonl(fireMoney);
	this->addInt(MP_PARAM_FIRE_MONEY, fireMoney);
}

bool MpDeadFishMessageNotify::getListFish(std::vector<uint8_t> &vtFish) const
{
	std::string data = this->getString(MP_PARAM_LIST_FISH);
	if (data == "") return true;
    for(size_t i = 0; i < data.size(); i++) vtFish.push_back((uint8_t)data[i]);
    return false;
}
