#include "mpsendluckymessagerequest.h"
#include "MessageType.h"

MpSendLuckyMessageRequest::MpSendLuckyMessageRequest() : MpMessageRequest(MP_MSG_LUCKY_SENDLUCKY)
{
    //this->m_pMsgHdr = makeMessage(MP_LUCKY_SENDLUCKY);
}

bool MpSendLuckyMessageRequest::getMoney(uint16_t &money) const
{
    money = getInt(MP_PARAM_BALANCE);
	return true;
}

void MpSendLuckyMessageRequest::setMoney(uint16_t Money)
{
    this->addInt(MP_PARAM_BALANCE, Money);
}
