#ifndef MPLOADROOMMESSAGERESPONSE_H
#define MPLOADROOMMESSAGERESPONSE_H
#include "mpmessageresponse.h"
using std::string;
#include <vector>

class MpLoadRoomMessageResponse : public MpMessageResponse
{
public:
    MpLoadRoomMessageResponse();
    //MpLoadRoomMessageResponse(const MsgHeader_t& msgHdr);
    virtual ~MpLoadRoomMessageResponse();
    bool getRoomStates(std::vector<uint8_t> & vtRooms) const;
    void setRoomStates(const std::vector<uint8_t> & m_vtRooms);

    bool getBetMoney(std::vector<uint32_t>& vtBetMoney) const;
    void setBetMoney(const std::vector<uint32_t>& vtBetMoney);    
};

#endif // MPLOADROOMMESSAGERESPONSE_H
