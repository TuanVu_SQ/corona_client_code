#ifndef MPLOGINRESPONSE_H
#define MPLOGINRESPONSE_H
#include "mpmessageresponse.h"
#include <vector>

class MpLoginResponse : public MpMessageResponse
{    
public:
    typedef struct GameAddress_s
    {
        uint8_t gameId;
        uint16_t port;
        std::string domain;
        GameAddress_s():gameId(0), port(0), domain(""){}
        const std::string to_string() const;
        void from_string(const std::string &g_addr);

    } GameAddress;

    typedef struct Date_s
    {
        uint8_t day;
        uint8_t month;
        uint16_t year;
        Date_s(): day(0), month(0), year(0){}
        const std::string to_string() const;
        void from_string(const std::string &date);
    } Date;

public:
    MpLoginResponse();

    const std::string getTokenId() const;
    const std::string getFullname() const;
    const std::string getEmail() const;
    const std::vector<GameAddress> getGameAddress() const;

    uint64_t getBalance() const;
    Date getBirthday() const;
    uint32_t getAvatarId() const;
    uint32_t getLevel() const;
    uint32_t getGender() const;


    void setTokenId(const std::string & tokenId);
    void setFullname(const std::string & fullname);
    void setBirthday(const Date & birthday);
    void setEmail(const std::string & email);
    void setBalance(uint64_t balance);
    void setAvatarId(uint32_t avatarId);
    void setGender(uint32_t gender);
    void setLevel(uint32_t level);
	void getListByTagRate(uint32_t  tag, std::vector<float> &numbers);
};

#endif // MPLOGINRESPONSE_H
