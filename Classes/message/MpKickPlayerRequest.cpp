#include "MpKickPlayerRequest.h"
#include "MessageType.h"

MpKickPlayerRequest::MpKickPlayerRequest() :MpMessageRequest(MP_MSG_KICK_PLAYER)
{
    //#define MP_MSG_KICK_PLAYER 0xD6
    //#define MP_MSG_KICK_PLAYER_NTFY 0xD8
}


void MpKickPlayerRequest::setUsername(const std::string &username)
{
    this->addString(MP_PARAM_USERNAME, username);
}

std::string MpKickPlayerRequest::getUsername() const
{
    return this->getString(MP_PARAM_USERNAME);
}
