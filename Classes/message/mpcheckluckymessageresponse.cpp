#include "mpcheckluckymessageresponse.h"
#include "MessageType.h"
#if (MP_TARGET_PLATFORM == MP_PLATFORM_WIN32) || (MP_TARGET_PLATFORM == MP_PLATFORM_WINRT) || (MP_TARGET_PLATFORM == MP_PLATFORM_WP8)
#include <Winsock2.h>
#else
#include "netinet/in.h"
#endif

MpCheckLuckyMessageResponse::MpCheckLuckyMessageResponse():MpMessageResponse(MP_MSG_LUCKY_CHECK_ACK){}


bool MpCheckLuckyMessageResponse::getResult(uint8_t &Status) const 
{
	Status = getInt(MP_RESULT);
	return true;
}

void MpCheckLuckyMessageResponse::setResult(uint8_t Status) 
{
	this->addInt(MP_RESULT, Status);
}

void MpCheckLuckyMessageResponse::setPercentageResult(uint16_t angle)
{
    this->addInt(MP_PARAM_LUCKY_PERCENT_RESULT, angle);
}

uint16_t MpCheckLuckyMessageResponse::getPercentageResult() const
{
	return getInt(MP_PARAM_LUCKY_PERCENT_RESULT);
}
