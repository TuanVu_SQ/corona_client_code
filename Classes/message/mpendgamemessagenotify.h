#ifndef MPENDGAMEMESSAGENOTIFY_H
#define MPENDGAMEMESSAGENOTIFY_H
#include "./mpclient/protocol/MpMessage.h"
#include "ClientPlayerInfo.h"
#include <vector>
class MpEndGameMessageNotify: public mp::protocol::MpMessage
{
public:
    MpEndGameMessageNotify();
    
	void setGameResult(const std::vector<GamePlayerResult> &vtRank);
	bool getGameResult(std::vector<GamePlayerResult> &vtRank) const;

    void setWinFeePercentage(unsigned char percentage);
    bool getWinFeePercentage(unsigned char &percentage) const;

};

#endif // MPENDGAMEMESSAGENOTIFY_H
