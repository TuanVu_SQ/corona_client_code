#include "MpGetAccountInfoResponse.h"
#include "MessageType.h"

MpGetAccountInfoResponse::MpGetAccountInfoResponse() : MpMessageResponse(MP_MSG_GET_ACC_INFO_ACK)
{}

void MpGetAccountInfoResponse::setAccountInfo(const MpGetAccountInfoResponse::AccountInfo &info)
{
	this->addString(MP_PARAM_USERNAME, info.username);
	this->addInt(MP_PARAM_BALANCE, info.balance);

	this->addInt(MP_PARAM_LEVEL, info.level);
	this->addInt(MP_PARAM_AVATAR_ID, info.avatarId);

	this->addInt(MP_WIN_COUNT, info.winCount);
	this->addInt(MP_LOSE_COUNT, info.loseCount);
	this->addInt(MP_PARAM_RANK, info.ranking);

	this->addInt(MP_IS_FRIEND, info.isFriend);
	this->addInt(MP_ACHIEVE1, info.achievement1);
	this->addInt(MP_ACHIEVE2, info.achievement2);
	this->addInt(MP_ACHIEVE3, info.achievement3);
}

bool MpGetAccountInfoResponse::getAccountInfo(MpGetAccountInfoResponse::AccountInfo &info) const
{
	info.username = this->getString(MP_PARAM_USERNAME);
	info.balance = this->getInt(MP_PARAM_BALANCE);

	info.level = this->getInt(MP_PARAM_LEVEL);
	info.avatarId = this->getInt(MP_PARAM_AVATAR_ID);

	info.winCount = this->getInt(MP_WIN_COUNT);
	info.loseCount = this->getInt(MP_LOSE_COUNT);
	info.ranking = this->getInt(MP_PARAM_RANK);

	info.isFriend = this->getInt(MP_IS_FRIEND);
	info.achievement1 = this->getInt(MP_ACHIEVE1);
	info.achievement2 = this->getInt(MP_ACHIEVE2);
	info.achievement3 = this->getInt(MP_ACHIEVE3);
	return true;
}
