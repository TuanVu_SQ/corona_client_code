#ifndef MPLOADTOPMESSAGERESPONSE_H
#define MPLOADTOPMESSAGERESPONSE_H
#include "mpmessageresponse.h"
#include "ClientPlayerInfo.h"
#include <vector>

class MpLoadTopMessageResponse:public MpMessageResponse
{
public:
    MpLoadTopMessageResponse();
    bool getTop(std::vector<TopInfo> &vtTop) const;
    void setTop(const std::vector<TopInfo> &vtTop);
};

#endif // MPLOADTOPMESSAGERESPONSE_H
