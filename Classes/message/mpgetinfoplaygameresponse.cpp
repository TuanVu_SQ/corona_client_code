#include "mpgetinfoplayergameresponse.h"
#include "MessageType.h"

MpGetInfoPlayerGameResponse::MpGetInfoPlayerGameResponse() :MpMessageResponse(MP_MSG_GET_ACC_INFO_ACK)
{
}

MpGetInfoPlayerGameResponse::~MpGetInfoPlayerGameResponse()
{

}
bool MpGetInfoPlayerGameResponse::getPlayerInfo(ClientPlayerInfoEX& pPlayer) const
{
	pPlayer.username = this->getString(MP_PARAM_USERNAME);
	pPlayer.balance = this->getInt(MP_PARAM_BALANCE);
	pPlayer.avatarId = this->getInt(MP_PARAM_AVATAR_ID);
	pPlayer.level = this->getInt(MP_PARAM_LEVEL);
	pPlayer.rank = this->getInt(MP_PARAM_RANK);
	pPlayer.winCount = this->getInt(MP_WIN_COUNT);
	pPlayer.loseCount = this->getInt(MP_LOSE_COUNT);
	pPlayer.acheive1 = this->getInt(MP_ACHIEVE1);
	pPlayer.acheive2 = this->getInt(MP_ACHIEVE2);
	pPlayer.acheive3 = this->getInt(MP_ACHIEVE3);
	pPlayer.isFriend = this->getInt(MP_IS_FRIEND);
	return true;
}