#include "mpfirebulletmessagenotify.h"

#include "MessageType.h"

MpFireBulletMessageNotify::MpFireBulletMessageNotify() : MpMessage(MP_MSG_FIRE_BULLET_NTFY)
{
}

MpFireBulletMessageNotify::MpFireBulletMessageNotify(const MpFireBulletMessageRequest &msgRequest) : MpMessage(MP_MSG_FIRE_BULLET_NTFY)
{
    //MsgHeader_t* pMsgHdr = msgRequest.getMsg();

    //assert(pMsgHdr);

    //for(int i = 0; i < pMsgHdr->msgData.list.count; i++)
    //{
    //    switch (pMsgHdr->msgData.list.array[i]->paramTag)
    //    {
    //    case MP_PARAM_BULLET_POSITION:
    //        //this->addString(MP_PARAM_BULLET_POSITION, pMsgHdr->msgData.list.array[i]->paramData->buf, pMsgHdr->msgData.list.array[i]->paramData->size);
    //        break;

    //    case MP_PARAM_GUN_LEVEL:
    //        //this->addComponent(MP_PARAM_GUN_LEVEL, pMsgHdr->msgData.list.array[i]->paramData->buf, pMsgHdr->msgData.list.array[i]->paramData->size);
    //        break;
    //    }
    //}
}

void MpFireBulletMessageNotify::setUsername(const std::string &username)
{
	this->addString(MP_PARAM_USERNAME, username);
}

bool MpFireBulletMessageNotify::getUsername(std::string &username) const
{
	username = this->getString(MP_PARAM_USERNAME);
	if (username == ""){
		return false;
	}
	return true; 
}

void MpFireBulletMessageNotify::setBulletPosition(const MpPoint &point)
{
	uint32_t p = (uint32_t)(point.x << 16 | point.y);
	this->addInt(MP_PARAM_BULLET_POSITION, p);
}

bool MpFireBulletMessageNotify::getBulletPosition(MpPoint &point) const
{
	uint32_t n = getInt(MP_PARAM_BULLET_POSITION);
	point.x = (n >> 16);
	point.y = static_cast<uint16_t>(n & 0xFFFF);
	return true;
}

void MpFireBulletMessageNotify::setGunLevel(uint8_t level)
{
	this->addInt(MP_PARAM_GUN_LEVEL, level);
}

bool MpFireBulletMessageNotify::getGunLevel(uint8_t &level) const
{
	level = getInt(MP_PARAM_GUN_LEVEL);
	return true;
}

void MpFireBulletMessageNotify::setFireMoney(uint32_t fireMoney)
{
    this->addInt(MP_PARAM_FIRE_MONEY, fireMoney);
}

bool MpFireBulletMessageNotify::getFireMoney(uint32_t &fireMoney) const
{
	fireMoney = this->getInt(MP_PARAM_FIRE_MONEY);
	return true;
}
