#ifndef MPKICKPLAYERREQUEST_H
#define MPKICKPLAYERREQUEST_H
#include "mpmessagerequest.h"
class MpKickPlayerRequest : public MpMessageRequest
{
public:
    MpKickPlayerRequest();
    void setUsername(const std::string& username);
    std::string getUsername() const;
};
#endif // MPKICKPLAYERREQUEST_H
