#include "MpSwapPositionNotify.h"
#include "MpSwapPositionRequest.h"
#include "MessageType.h"


MpSwapPositionNotify::MpSwapPositionNotify() :MpMessage(MP_MSG_SWAP_POSITION_NTFY)
{
}

void MpSwapPositionNotify::setListPlayerPosition(const std::vector<PlayerPosition> &vtPlayerPosition)
{
    std::string data;
    for(size_t i = 0; i < vtPlayerPosition.size(); ++i)
    {
        data += vtPlayerPosition[i].username;
        data += (char)(vtPlayerPosition[i].position);
    }

    this->addString(MP_PARAM_USERNAME, data);
}

bool MpSwapPositionNotify::getListPlayerPosition(std::vector<PlayerPosition> &vtPlayerPosition) const
{
	std::string data = this->getString(MP_PARAM_USERNAME);
	if (data == "") return false;
    if(data.size() < 7)return false;
    const char* ptr = data.data();
    const char* endData = data.data() + data.size();
    PlayerPosition pp;
    while(ptr < endData)
    {
        if(*ptr < 0xF)
        {
            pp.position = *ptr;
            vtPlayerPosition.push_back(pp);
            pp.username.clear();
            pp.position = 0xF;
        }else
        {
            pp.username += *ptr;
        }
        ++ptr;
    }
    return true;
}
