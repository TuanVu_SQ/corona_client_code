#ifndef __MPRECHARGEBYCARDMESSAGEREQUEST_H_INCLUDED__
#define __MPRECHARGEBYCARDMESSAGEREQUEST_H_INCLUDED__

#include "mpmessagerequest.h"
#include "MessageParam.h"

class MpRechargeByCardMessageRequest : public MpMessageRequest
{
public:
	MpRechargeByCardMessageRequest();
	
	void setSerial(const std::string& pSerial);
	std::string getSerial() const;

	void setCard(const std::string& pCard);
	std::string getCard() const;

	void setTelco(const std::string& pTelco);
	std::string getTelco() const;

	void setChargeID(uint8_t pChargeID);
	bool getChargeID(uint8_t &pChargeID) const;

	void setUsername(const std::string& pUsername);
	std::string getUsername() const;
};

#endif
