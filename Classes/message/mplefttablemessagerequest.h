#ifndef MPLEFTTABLEMESSAGEREQUEST_H
#define MPLEFTTABLEMESSAGEREQUEST_H
#include "MpMessageRequest.h"

class MpLeftTableMessageRequest : public MpMessageRequest
{
public:
    MpLeftTableMessageRequest();    
};

#endif // MPLEFTTABLEMESSAGEREQUEST_H
