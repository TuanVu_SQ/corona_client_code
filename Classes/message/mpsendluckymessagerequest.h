#ifndef __MPSENDLUCKYMESSAGEREQUEST_H_INCLUDED__
#define __MPSENDLUCKYMESSAGEREQUEST_H_INCLUDED__

#include "mpmessagerequest.h"
#include "MessageParam.h"

class MpSendLuckyMessageRequest : public MpMessageRequest
{
public:
	MpSendLuckyMessageRequest();
    bool getMoney(uint16_t &money) const;
	void setMoney(uint16_t Money);
};

#endif // MPLOADTOPMESSAGEREQUEST_H
