#ifndef MPSWAPPOSITIONINDICATION_H
#define MPSWAPPOSITIONINDICATION_H
#include "./mpclient/protocol/MpMessage.h"
#include "MpSwapPositionRequest.h"
#include <vector>
class MpSwapPositionIndication: public MpMessage
{
public:
    MpSwapPositionIndication();
    ~MpSwapPositionIndication();
    void setListPlayerPosition(const std::vector<PlayerPosition> &vtPlayerPosition);
    bool getListPlayerPosition(std::vector<PlayerPosition> &vtPlayerPosition) const;
};
#endif // MPSWAPPOSITIONINDICATION_H
