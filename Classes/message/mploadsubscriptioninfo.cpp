#include "mploadsubscriptioninfo.h"
#include "MessageType.h"
#ifdef _WIN32
#include <Winsock2.h>
#endif
#include <string.h>



std::string SubscriptionInfo::toString() const
{
    char buffer[1024];
    char *ptr = buffer;
    *(reinterpret_cast<uint32_t *>(ptr)) = htonl(ndays);
    ptr += 4;
    *(reinterpret_cast<uint32_t *>(ptr)) = htonl(htg);
    ptr += 4;
    *(reinterpret_cast<uint32_t *>(ptr)) = htonl(coin);
    ptr += 4;
    *(reinterpret_cast<uint32_t *>(ptr)) = htonl(promotion);
    ptr += 4;
    *(reinterpret_cast<uint32_t *>(ptr)) = htonl(status);
    ptr += 4;
    memcpy(ptr, name.c_str(), name.length());
    ptr += name.length();
    *(ptr++) = 0;
    return std::string(buffer,static_cast<size_t>(ptr - buffer));
}

size_t SubscriptionInfo::fromString(const char *buffer, size_t len)
{
    if(len < 21) return 0;
    const char *ptr = buffer;

    ndays = htonl(*reinterpret_cast<const uint32_t *>(ptr));
    ptr += 4;
    htg = htonl(*reinterpret_cast<const uint32_t *>(ptr));
    ptr += 4;
    coin = htonl(*reinterpret_cast<const uint32_t *>(ptr));
    ptr += 4;
    promotion = htonl(*reinterpret_cast<const uint32_t *>(ptr));
    ptr += 4;
    status = htonl(*reinterpret_cast<const uint32_t *>(ptr));
    ptr += 4;
    name = ptr;
    return 21 + name.size();
}

MpLoadSubscriptionInfoResponse::MpLoadSubscriptionInfoResponse() : MpMessageResponse(MP_MSG_GET_SUBSCRIPTION_ACK)
{}

void MpLoadSubscriptionInfoResponse::set(const std::vector<SubscriptionInfo> &vts)
{
    std::string s;
    for(auto const & si : vts) s += si.toString();
    this->addString(MP_PARAM_LIST_SUBSCRIPTION, s);
}

const std::vector<SubscriptionInfo> MpLoadSubscriptionInfoResponse::get() const 
{
    std::vector<SubscriptionInfo> vts;
    auto s = getString(MP_PARAM_LIST_SUBSCRIPTION);
    size_t consume = 0, tmp = 0;
    while (consume < s.length()) {
        SubscriptionInfo si;

        if((tmp = si.fromString(s.c_str() + consume, s.length() - consume)) == 0) break;
        consume += tmp;
        vts.push_back(si);
    }
    return vts;
}