#ifndef MPGETACCOUNTINFORESPONSE_H
#define MPGETACCOUNTINFORESPONSE_H
#include "mpmessageresponse.h"

class MpGetAccountInfoResponse : public MpMessageResponse
{
public:
/*
1.    Tên
2.    Tiền
3.    Cấp độ
4.    Avatar
5.    Số trận thắng
6.    Số trận thua
7.    Hạng
8.	Là bạn bè hay không?
9.	Danh sách huân chương chiến công ( chi tiết ở phần huân chương )
*/
    typedef struct AccountInfo_s
    {
        std::string username;
        unsigned int balance;
        unsigned char level;
        unsigned char avatarId;
        unsigned int winCount;
        unsigned int loseCount;
        unsigned int ranking;
        unsigned char isFriend;
        unsigned char achievement1;
        unsigned char achievement2;
        unsigned char achievement3;
    } AccountInfo;
public:
    MpGetAccountInfoResponse();
    void setAccountInfo(const AccountInfo &info);
    bool getAccountInfo(AccountInfo &info) const;
};

#endif // MPGETACCOUNTINFORESPONSE_H
