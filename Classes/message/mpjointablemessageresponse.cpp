#include "mpjointablemessageresponse.h"
#include "MessageType.h"

using std::string;

MpJoinTableMessageResponse::MpJoinTableMessageResponse() :MpMessageResponse(MP_MSG_JOIN_TABLE_ACK)
{}

MpJoinTableMessageResponse::~MpJoinTableMessageResponse()
{}

bool MpJoinTableMessageResponse::getListReadyPlayer(std::vector <ReadyPlayer>& vtListReady) const
{
    ReadyPlayer readyPlayer;
	string data = this->getString(MP_PARAM_LIST_READY_PLAYER);
	if (data == "") return false;
    if(data.data() == NULL) return false;

	size_t i = 0;
	while (i < data.length())
	{
        readyPlayer.position  = data[i];
		i++;
        readyPlayer.status = data[i];
		i++;

        vtListReady.push_back(readyPlayer);
	}

	return true;
}

void MpJoinTableMessageResponse::setListReadyPlayer(const std::vector<ReadyPlayer> &vtListReady)
{
	string buffer = "";
	for (size_t i = 0; i < vtListReady.size(); i++)
	{
        const ReadyPlayer& playerStatus = vtListReady.at(i);
        buffer += (uint8_t)playerStatus.position;
        buffer += (uint8_t)playerStatus.status;
	}
	this->addString(MP_PARAM_LIST_READY_PLAYER, buffer);
}
////Player binary:
//// 'username + 0'|level:1|balance:4|avatarId:1|position :1| host:1| itemId:1|disallowKick:1|
bool MpJoinTableMessageResponse::getListPlayer(std::vector<ClientPlayerInfo> &vtPlayer) const
{
    ClientPlayerInfo player;
    string data = this->getString(MP_LISTPLAYER);
    if(data == "") return false;
    size_t i = 0;
    while(i < data.length())
    {
        player.username = "";
                //username
        for(; i < data.length();i++)
        {
            if(data[i] != 0)
            {
                player.username += data[i];
            }else
            {
                break; //of for
            }
        }
        if(data.length() - i < 9)
            break; //Not enough data. Break of while

        i++;                             // level
        player.level = data[i];

        i++;                            // 1st byte of balance
        player.balance =*( uint32_t*) (data.c_str() + i);
        player.balance = ntohl(player.balance);

//        i+=4;                           // avatar id
//        player.avatarId = data[i];

        i+=4;                            // position
        player.position = ((uint8_t) data[i]);

        i++;                            //host[]
        player.host = data[i];

        i++;                            // itemId
        player.itemId = data[i];

        i++;                            // kick
        player.disallowKick = data[i];

        vtPlayer.push_back(player);
        i++;                            // nextPlayer
    }

    return true;
}

//// 'username + 0'|level:1|balance:4|avatarId:1|position :1| host:1| itemId:1|disallowKick:1|
/// 'username + 0'|level:1|balance:4|position :1| host:1| itemId:1|disallowKick:1|

void MpJoinTableMessageResponse::setListPlayer(const std::vector<ClientPlayerInfo> &vtPlayer)
{
    string buffer = "";
    uint32_t balance;
    for(size_t i = 0; i < vtPlayer.size(); i++)
    {
        const ClientPlayerInfo& player = vtPlayer[i];
        // username + 0
        buffer += player.username;
        buffer += (char)0;

        // level
        buffer += player.level;

        // balance
        balance = htonl(player.balance);
        buffer.append((const char*)&balance, 4);

        //position
        buffer += player.position;

        //host
        buffer += player.host;

        //itemId
        buffer += player.itemId;

        //disallowKick
        buffer += player.disallowKick;
    }
    this->addString(MP_LISTPLAYER, buffer);
}

bool MpJoinTableMessageResponse::getListItem(std::vector<ClientItemInfo> &vtItem) const
{
    ClientItemInfo itemInfo;
    string data = this->getString(MP_LISTITEMINVENTORY);

    if(data == "") return false;

    const char* ptr = data.c_str();

    while (ptr + sizeof(ClientItemInfo) <= data.c_str() + data.length())
    {
        itemInfo = *((const ClientItemInfo*) ptr);
        itemInfo.id = ntohl(itemInfo.id);
        itemInfo.quantity = ntohl(itemInfo.quantity);
#ifdef _USE_ITEM_TYPE
        itemInfo.type = ntohs(itemInfo.type);
#endif

        vtItem.push_back(itemInfo);
        ptr += sizeof(ClientItemInfo);
    }
    return true;
}

void MpJoinTableMessageResponse::setListItem(const std::vector<ClientItemInfo> &vtItem)
{
    ClientItemInfo itemInfo;
    size_t len = vtItem.size() * sizeof(ClientItemInfo);

    if(len == 0) return;

    uint8_t *buffer = new uint8_t[len];

    assert(buffer);

    uint8_t *ptr = buffer;

    for( size_t i = 0; i < vtItem.size(); i++)
    {
        itemInfo = vtItem[i];
        itemInfo.id = htonl(itemInfo.id);
        itemInfo.quantity = htonl(itemInfo.quantity);
#ifdef _USE_ITEM_TYPE
        itemInfo.type = htons(itemInfo.type);
#endif
        *((ClientItemInfo*) ptr) = itemInfo;
        ptr += sizeof(ClientItemInfo);
    }

    this->addString(MP_LISTITEMINVENTORY, std::string((const char *) buffer, len));

    if(buffer)
    {
        delete[] buffer;
        buffer = NULL;
    }
}

bool MpJoinTableMessageResponse::getListFishPrice(std::vector<uint16_t> &vtFishPrice) const
{
    string data = this->getString(MP_PARAM_LIST_FISH_PRICE);

    if (data == "") return false;

    const char* ptr = data.c_str();
    if(!ptr) return false;

    for (size_t i = 0; i < data.size(); i+=2)
    {
        uint16_t price = *((uint16_t*)ptr);
        price = ntohs(price);
        ptr += 2;
        vtFishPrice.push_back(price);
    }
    return true;
}

void MpJoinTableMessageResponse::setListFishPrice(const std::vector<uint16_t> &vtFishPrice)
{
    std::string data;
    for (size_t i = 0; i < vtFishPrice.size(); ++i)
    {
        uint16_t price = vtFishPrice.at(i);

        price = htons(price);
        string strTmp((const char*)&price, 2);
        data.append(strTmp);
    }
    this->addString(MP_PARAM_LIST_FISH_PRICE, data);
}


uint8_t MpJoinTableMessageResponse::getBackgroundId() const
{
	return getInt(MP_PARAM_BACKGROUND_ID);    
}

void MpJoinTableMessageResponse::setBackgroundId(uint8_t backgroundId)
{
    this->addInt(MP_PARAM_BACKGROUND_ID, backgroundId);
}

uint8_t MpJoinTableMessageResponse::getRatioLevel() const
{
	return getInt(MP_PARAM_RATIO_LEVEL);    
}

void MpJoinTableMessageResponse::setRatioLevel(uint8_t ratioLevel)
{
    this->addInt(MP_PARAM_RATIO_LEVEL, ratioLevel);
}

void MpJoinTableMessageResponse::setMinBetMoney(uint32_t minBetMoney)
{
    //#define MP_MIN_BETMONEY       0x15B
    this->addInt(MP_PARAM_MIN_BETMONEY, minBetMoney);
}

bool MpJoinTableMessageResponse::getMinBetMoney(uint32_t &minBetMoney) const
{
	minBetMoney = this->getInt(MP_PARAM_MIN_BETMONEY);
    return true;
}

void MpJoinTableMessageResponse::setDefaultBetMoney(uint32_t defaultBetMoney)
{
    //#define MP_DEF_BETMONEY       0x15C
    this->addInt(MP_PARAM_DEF_BETMONEY, defaultBetMoney);
}

bool MpJoinTableMessageResponse::getDefaultBetMoney(uint32_t &defaultBetMoney) const
{
	defaultBetMoney = this->getInt(MP_PARAM_DEF_BETMONEY);
    return true;
}
void MpJoinTableMessageResponse::setGameDuration(uint16_t duration)
{
	this->addInt(MP_PARAM_TIME, duration);
}

bool MpJoinTableMessageResponse::getGameDuration(uint16_t &duration) const
{
	duration = this->getInt(MP_PARAM_TIME);
	return true;
}
