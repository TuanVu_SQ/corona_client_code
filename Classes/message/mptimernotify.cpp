#include "mptimernotify.h"
#include "MessageType.h"

bool MpTimerNotify::getCurrentTime(time_t &t) const
{
	std::string data = this->getString(MP_PARAM_TIME);
	if (data == "") return false;
    t = *((const time_t*) data.data());
    return true;
}

MpTimerNotify::MpTimerNotify() : MpMessage(MP_MSG_TIMER_NTFY)
{}
