#ifndef __MPPINGMESSAGERESPONSE_H_INCLUDED__
#define __MPPINGMESSAGERESPONSE_H_INCLUDED__

#include "mpmessageresponse.h"
#include <vector>

class MpPingMessageResponse :public MpMessageResponse {
public:
	MpPingMessageResponse();
	
	void setListEvent(const std::vector<uint8_t> &vtEvent);
	bool getListEvent(std::vector<uint8_t> &vtEvent) const;

};

#endif // MPLOADTOPMESSAGERESPONSE_H
