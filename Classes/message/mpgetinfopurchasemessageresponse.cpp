#include "mpgetinfopurchasemessageresponse.h"
#include "MessageType.h"
#include "MessageParam.h"

MpGetInfoPurchaseMessageResponse::MpGetInfoPurchaseMessageResponse() : MpMessageResponse(MP_MSG_GETINFO_PURCHASE_ACK)
{
    //this->m_pMsgHdr = makeMessage(MP_GETINFO_PURCHASE_ACK);
}

//void MpGetInfoPurchaseMessageResponse::setServiceNumber(const std::string& pServiceNumber) {
//	this->addComponent(MP_SERVICE_NUMBER, (const uint8_t*)pServiceNumber.c_str(), pServiceNumber.length());
//}
//
//std::string MpGetInfoPurchaseMessageResponse::getServiceNumber() const {
//	std::string data = "";
//	this->getDataByTag(MP_SERVICE_NUMBER, data);
//	return data;
//}
//
//void MpGetInfoPurchaseMessageResponse::setSyntax(const std::string& pSyntax) {
//	this->addComponent(MP_SYNTAX, (const uint8_t*)pSyntax.c_str(), pSyntax.length());
//}
//
//std::string MpGetInfoPurchaseMessageResponse::getSyntax() const {
//	std::string data = "";
//	this->getDataByTag(MP_SYNTAX, data);
//	return data;
//}

bool MpGetInfoPurchaseMessageResponse::getVtRecharge(std::vector<RechargeInfo> &vtRecharge) const
{
	std::string data = this->getString(MP_VECTOR_RECHARGE);
	if (data == "") return false;
	RechargeInfo rechargeInfo;
	size_t len = 0;
	while (len < data.size())
	{
		rechargeInfo.id = (*(uint32_t*)(data.data() + len));
		rechargeInfo.id = ntohl(rechargeInfo.id);
		len += 4;

		rechargeInfo.syntax = "";
		while (data[len] != 0)
		{
			rechargeInfo.syntax += data[len];
			len++;
		}
		len++;

		rechargeInfo.serviceNumber = "";
		while (data[len] != 0)
		{
			rechargeInfo.serviceNumber += data[len];
			len++;
		}
		len++;

		rechargeInfo.serverUddp = "";
		while (data[len] != 0)
		{
			rechargeInfo.serverUddp += data[len];
			len++;
		}
		len++;

		rechargeInfo.telco = "";
		while (data[len] != 0)
		{
			rechargeInfo.telco += data[len];
			len++;
		}
		len++;

		rechargeInfo.money = (*(uint32_t*)(data.data() + len));
		rechargeInfo.money = ntohl(rechargeInfo.money);
		len += 4;

		rechargeInfo.balance = *((uint32_t*)(data.data() + len));
		rechargeInfo.balance = ntohl(rechargeInfo.balance);
		len += 4;

		rechargeInfo.percent = *((uint32_t*)(data.data() + len));
		rechargeInfo.percent = ntohl(rechargeInfo.percent);
		len += 4;

		vtRecharge.push_back(rechargeInfo);
	}
	return true;
}

void MpGetInfoPurchaseMessageResponse::setVtRecharge(const std::vector<RechargeInfo> &vtRecharge)
{
	RechargeInfo rechargeInfo;
	uint8_t buffer[10240];
	uint8_t *ptr = buffer;
	size_t len = 0;
	for (size_t i = 0; i < vtRecharge.size(); i++)
	{
		if (len >= 10240 - 10) return;
		rechargeInfo = vtRecharge[i];

		rechargeInfo.id = htonl(rechargeInfo.id);
		*(uint32_t*)(ptr + len) = rechargeInfo.id;
		len += 4;

		memcpy(ptr + len, rechargeInfo.syntax.c_str(), rechargeInfo.syntax.size());
		len += rechargeInfo.syntax.size();
		*(ptr + len) = 0;
		len++;

		memcpy(ptr + len, rechargeInfo.serviceNumber.c_str(), rechargeInfo.serviceNumber.size());
		len += rechargeInfo.serviceNumber.size();
		*(ptr + len) = 0;
		len++;

		memcpy(ptr + len, rechargeInfo.serverUddp.c_str(), rechargeInfo.serverUddp.size());
		len += rechargeInfo.serverUddp.size();
		*(ptr + len) = 0;
		len++;

		memcpy(ptr + len, rechargeInfo.telco.c_str(), rechargeInfo.telco.size());
		len += rechargeInfo.telco.size();
		*(ptr + len) = 0;
		len++;

		rechargeInfo.money = htonl(rechargeInfo.money);
		*(uint32_t*)(ptr + len) = rechargeInfo.money;
		len += 4;

		rechargeInfo.balance = htonl(rechargeInfo.balance);
		*(uint32_t*)(ptr + len) = rechargeInfo.balance;
		len += 4;

		rechargeInfo.percent = htonl(rechargeInfo.percent);
		*(uint32_t*)(ptr + len) = rechargeInfo.percent;
		len += 4;
	}

	if (len > 0)
		this->addString(MP_VECTOR_RECHARGE, std::string((const char*)buffer, len));
}

bool MpGetInfoPurchaseMessageResponse::getVtExchange(std::vector<ExchangeInfo> &vtExchange) const
{
	std::string data = this->getString(MP_PARAM_VT_EXCHANGE);
	if (data == "") return false;
	ExchangeInfo rechargeInfo;
	size_t len = 0;
	while (len < data.size())
	{
		rechargeInfo.id = (*(uint32_t*)(data.data() + len));
		rechargeInfo.id = ntohl(rechargeInfo.id);
		len += 4;

		rechargeInfo.syntax = "";
		while (data[len] != 0)
		{
			rechargeInfo.syntax += data[len];
			len++;
		}
		len++;

		rechargeInfo.serviceNumber = "";
		while (data[len] != 0)
		{
			rechargeInfo.serviceNumber += data[len];
			len++;
		}
		len++;

		rechargeInfo.serverUddp = "";
		while (data[len] != 0)
		{
			rechargeInfo.serverUddp += data[len];
			len++;
		}
		len++;

		rechargeInfo.telco = "";
		while (data[len] != 0)
		{
			rechargeInfo.telco += data[len];
			len++;
		}
		len++;

		rechargeInfo.money = (*(uint32_t*)(data.data() + len));
		rechargeInfo.money = ntohl(rechargeInfo.money);
		len += 4;

		rechargeInfo.balance = *((uint32_t*)(data.data() + len));
		rechargeInfo.balance = ntohl(rechargeInfo.balance);
		len += 4;

		rechargeInfo.percent = *((uint32_t*)(data.data() + len));
		rechargeInfo.percent = ntohl(rechargeInfo.percent);
		len += 4;

		vtExchange.push_back(rechargeInfo);
	}
	return true;
}

void MpGetInfoPurchaseMessageResponse::setVtExchange(const std::vector<ExchangeInfo> &vtExchange)
{
	ExchangeInfo exchangeInfo;
	uint8_t buffer[10240];
	uint8_t *ptr = buffer;
	size_t len = 0;
	for (size_t i = 0; i < vtExchange.size(); i++)
	{
		if (len >= 10240 - 10) return;
		exchangeInfo = vtExchange[i];

		exchangeInfo.id = htonl(exchangeInfo.id);
		*(uint32_t*)(ptr + len) = exchangeInfo.id;
		len += 4;

		memcpy(ptr + len, exchangeInfo.syntax.c_str(), exchangeInfo.syntax.size());
		len += exchangeInfo.syntax.size();
		*(ptr + len) = 0;
		len++;

		memcpy(ptr + len, exchangeInfo.serviceNumber.c_str(), exchangeInfo.serviceNumber.size());
		len += exchangeInfo.serviceNumber.size();
		*(ptr + len) = 0;
		len++;

		memcpy(ptr + len, exchangeInfo.serverUddp.c_str(), exchangeInfo.serverUddp.size());
		len += exchangeInfo.serverUddp.size();
		*(ptr + len) = 0;
		len++;

		memcpy(ptr + len, exchangeInfo.telco.c_str(), exchangeInfo.telco.size());
		len += exchangeInfo.telco.size();
		*(ptr + len) = 0;
		len++;

		exchangeInfo.money = htonl(exchangeInfo.money);
		*(uint32_t*)(ptr + len) = exchangeInfo.money;
		len += 4;

		exchangeInfo.balance = htonl(exchangeInfo.balance);
		*(uint32_t*)(ptr + len) = exchangeInfo.balance;
		len += 4;

		exchangeInfo.percent = htonl(exchangeInfo.percent);
		*(uint32_t*)(ptr + len) = exchangeInfo.percent;
		len += 4;
	}

	if (len > 0)
		this->addString(MP_PARAM_VT_EXCHANGE, std::string((const char*)buffer, len));
}

bool MpGetInfoPurchaseMessageResponse::getVtSubcribe(std::vector<SubcribeInfo> &vtSubcribe) const
{
	std::string data = this->getString(MP_PARAM_VT_SUBCRIBE);
	if (data == "") return false;
	SubcribeInfo subcribeInfo;
	size_t len = 0;
	while (len < data.size())
	{
		subcribeInfo.syntax = "";
		while (data[len] != 0)
		{
			subcribeInfo.syntax += data[len];
			len++;
		}
		len++;

		subcribeInfo.serviceNumber = "";
		while (data[len] != 0)
		{
			subcribeInfo.serviceNumber += data[len];
			len++;
		}
		len++;

		subcribeInfo.serverUddp = "";
		while (data[len] != 0)
		{
			subcribeInfo.serverUddp += data[len];
			len++;
		}
		len++;

		subcribeInfo.telco = "";
		while (data[len] != 0)
		{
			subcribeInfo.telco += data[len];
			len++;
		}
		len++;

		subcribeInfo.daily_money = (*(uint32_t*)(data.data() + len));
		subcribeInfo.daily_money = ntohl(subcribeInfo.daily_money);
		len += 4;

		subcribeInfo.day_valid = *((uint32_t*)(data.data() + len));
		subcribeInfo.day_valid = ntohl(subcribeInfo.day_valid);
		len += 4;

		subcribeInfo.balance = *((uint32_t*)(data.data() + len));
		subcribeInfo.balance = ntohl(subcribeInfo.balance);
		len += 4;

		vtSubcribe.push_back(subcribeInfo);
	}
	return true;
}

void MpGetInfoPurchaseMessageResponse::setVtSubcribe(const std::vector<SubcribeInfo> &vtSubcribe)
{
	SubcribeInfo subcribeInfo;
	uint8_t buffer[10240];
	uint8_t *ptr = buffer;
	size_t len = 0;
	for (size_t i = 0; i < vtSubcribe.size(); i++)
	{
		if (len >= 10240 - 10) return;
		subcribeInfo = vtSubcribe[i];

		memcpy(ptr + len, subcribeInfo.syntax.c_str(), subcribeInfo.syntax.size());
		len += subcribeInfo.syntax.size();
		*(ptr + len) = 0;
		len++;

		memcpy(ptr + len, subcribeInfo.serviceNumber.c_str(), subcribeInfo.serviceNumber.size());
		len += subcribeInfo.serviceNumber.size();
		*(ptr + len) = 0;
		len++;

		memcpy(ptr + len, subcribeInfo.serverUddp.c_str(), subcribeInfo.serverUddp.size());
		len += subcribeInfo.serverUddp.size();
		*(ptr + len) = 0;
		len++;

		memcpy(ptr + len, subcribeInfo.telco.c_str(), subcribeInfo.telco.size());
		len += subcribeInfo.telco.size();
		*(ptr + len) = 0;
		len++;

		subcribeInfo.daily_money = htonl(subcribeInfo.daily_money);
		*(uint32_t*)(ptr + len) = subcribeInfo.daily_money;
		len += 4;

		subcribeInfo.day_valid = htonl(subcribeInfo.day_valid);
		*(uint32_t*)(ptr + len) = subcribeInfo.day_valid;
		len += 4;
	}

	if (len > 0)
		this->addString(MP_PARAM_VT_SUBCRIBE, std::string((const char*)buffer, len));
}

bool MpGetInfoPurchaseMessageResponse::getVtRechargeIAP(std::vector<RechargeVISAInfo> &vtRechargeIAP) const {
    std::string data = this->getString(MP_PARAM_VECTOR_RECHARGE_IAP);
    if ( data == "") return false;
    RechargeVISAInfo rechargeInfo;
    size_t len = 0;
    while (len < data.size())
    {
        rechargeInfo.itemCode = "";
        while (data[len] != 0)
        {
            rechargeInfo.itemCode += data[len];
            len++;
        }
        len++;

        rechargeInfo.mBalance = (*(uint32_t*)(data.data() + len));
        rechargeInfo.mBalance = ntohl(rechargeInfo.mBalance);
        len += 4;

        rechargeInfo.mUSD = *((uint32_t*)(data.data() + len));
        rechargeInfo.mUSD = ntohl(rechargeInfo.mUSD);
        len += 4;

        rechargeInfo.mPercent = *((uint32_t*)(data.data() + len));
        rechargeInfo.mPercent = ntohl(rechargeInfo.mPercent);
        len += 4;

        vtRechargeIAP.push_back(rechargeInfo);
    }
    return true;
}

void MpGetInfoPurchaseMessageResponse::setVtRechargeIAP(const std::vector<RechargeVISAInfo> &vtRechargeIAP) {
    RechargeVISAInfo rechargeInfo;
    uint8_t buffer[10240];
    uint8_t *ptr = buffer;
    size_t len = 0;
    for (size_t i = 0; i < vtRechargeIAP.size(); i++)
    {
        if (len >= 10240 - 10) return;
        rechargeInfo = vtRechargeIAP[i];

        memcpy(ptr + len, rechargeInfo.itemCode.c_str(), rechargeInfo.itemCode.size());
        len += rechargeInfo.itemCode.size();

        *(ptr + len) = 0;
        len++;

        rechargeInfo.mBalance = htonl(rechargeInfo.mBalance);
        *(uint32_t*)(ptr + len) = rechargeInfo.mBalance;
        len += 4;

        rechargeInfo.mUSD = htonl(rechargeInfo.mUSD);
        *(uint32_t*)(ptr + len) = rechargeInfo.mUSD;
        len += 4;

        rechargeInfo.mPercent = htonl(rechargeInfo.mPercent);
        *(uint32_t*)(ptr + len) = rechargeInfo.mPercent;
        len += 4;
    }

    if (len > 0)
        this->addString(MP_VECTOR_RECHARGE, std::string((const char*)buffer, len));
}

void MpGetInfoPurchaseMessageResponse::setEnable(uint8_t enable)
{
    this->addInt(MP_PARAM_ENABLE, enable);
}

uint8_t MpGetInfoPurchaseMessageResponse::getEnable() const
{
    return getInt(MP_PARAM_ENABLE);
}
