#ifndef __MPGETINFOSMSMESSAGEREQUEST_H_INCLUDED__
#define __MPGETINFOSMSMESSAGEREQUEST_H_INCLUDED__

#include "mpmessagerequest.h"
#include "MessageParam.h"

class MpGetInfoSMSMessageRequest : public MpMessageRequest
{
public:
	MpGetInfoSMSMessageRequest();

	void setChargeID(uint8_t pChargeID);
	bool getChargeID(uint8_t &pChargeID) const;

	void setOS(const std::string& os);
	std::string getOS() const;

	void setCountry(const std::string& country);
	std::string getCountry() const;
};

#endif
