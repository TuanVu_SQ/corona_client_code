#ifndef __MPGETINFOSMSMESSAGERESPONSE_H_INCLUDED__
#define __MPGETINFOSMSMESSAGERESPONSE_H_INCLUDED__

#include "mpmessageresponse.h"
#include "ClientPlayerInfo.h"
#include <vector>

class MpGetInfoSMSMessageResponse :public MpMessageResponse {
public:
	MpGetInfoSMSMessageResponse();

	/*void setServiceNumber(const std::string& pServiceNumber);
	std::string getServiceNumber() const;

	void setSyntax(const std::string& pSyntax);
	std::string getSyntax() const;*/

	bool getVtRecharge(std::vector<RechargeInfo> &vtRecharge) const;
	void setVtRecharge(const std::vector<RechargeInfo> &vtRecharge);

	void setEnable(uint8_t enable); // 1 = enable ; 0 = disable
	uint8_t getEnable() const; // 1 = enable ; 0 = disable
};

#endif // MPLOADTOPMESSAGERESPONSE_H
