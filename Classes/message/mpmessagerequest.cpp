#include "mpmessagerequest.h"
#include "MessageParam.h"

using std::string;

MpMessageRequest::MpMessageRequest(uint32_t type) : MpMessage(type){}

MpMessageRequest::~MpMessageRequest(){}

bool MpMessageRequest::getTokenId(string &tokenId) const
{
    tokenId = this->getString(MP_PARAM_TOKENID);
	return true;
}

void MpMessageRequest::setTokenId(const std::string &tokenId)
{
	addString(MP_PARAM_TOKENID, tokenId);
}
