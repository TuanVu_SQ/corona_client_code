#ifndef MPMESSAGEREQUEST_H
#define MPMESSAGEREQUEST_H
#include "./mpclient/protocol/MpMessage.h"

using namespace mp;
using namespace protocol;

class MpMessageRequest : public MpMessage
{
public:

    MpMessageRequest(uint32_t type);
    virtual ~MpMessageRequest();

    bool getTokenId(std::string& tokenId) const;
    void setTokenId(const std::string& tokenId);

private:	
	MpMessageRequest() = delete;

private:

    std::string m_strTokenId;
};

#endif // MPMESSAGEREQUEST_H
