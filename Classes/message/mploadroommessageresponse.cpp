#include "mploadroommessageresponse.h"
#include "MessageType.h"
#include "MessageParam.h"


#if (MP_TARGET_PLATFORM == MP_PLATFORM_WIN32) || (MP_TARGET_PLATFORM == MP_PLATFORM_WINRT) || (MP_TARGET_PLATFORM == MP_PLATFORM_WP8)
#include <Winsock2.h>
#else
#include "netinet/in.h"
#endif

MpLoadRoomMessageResponse::MpLoadRoomMessageResponse() : MpMessageResponse(MP_LOAD_ROOM_ACK)
{
    //m_pMsgHdr = makeMessage(MP_LOAD_ROOM_ACK);
}
//
//MpLoadRoomMessageResponse::MpLoadRoomMessageResponse(const MsgHeader_t &msgHdr):MpMessageResponse(msgHdr)
//{
//}

MpLoadRoomMessageResponse::~MpLoadRoomMessageResponse()
{

}

bool MpLoadRoomMessageResponse::getRoomStates(std::vector<uint8_t> &vtRooms) const
{
	std::string p; getDataByTag(MP_LISTROOM, p);
    if(p == "") return false;
	size_t len = 0;
	while (len < p.size())
	{
		uint8_t pState = (*(uint8_t*)(p.data() + len));
		pState = ntohs(pState);
		len++;
		vtRooms.push_back(pState);
	}
    return true;
}

//void MpLoadRoomMessageResponse::setRoomStates(const std::vector<uint8_t> &m_vtRooms)
//{
//    std::vector<uint8_t> buffer;
//    unsigned char count = 0;
//    for(size_t i = 0; i < m_vtRooms.size(); i++)
//    {
//        if(m_vtRooms[i] > 0  || count == 0x7f)
//        {
//            if(count > 0)
//            {
//                buffer.push_back(0x80 + count);
//                count = 0;
//            }
//            buffer.push_back(m_vtRooms[i]);
//        }else
//        {
//            count++;
//        }
//    }
//    addParam(m_pMsgHdr, MP_LISTROOM, &m_vtRooms[0] , m_vtRooms.size());
//}

bool MpLoadRoomMessageResponse::getBetMoney(std::vector<uint32_t> &vtBetMoney) const
{
    string data ="";
    if(getDataByTag(MP_BET_MONEY, data)==false) return false;
    const uint8_t* buffer = (const uint8_t*)data.c_str();
    uint32_t betMoney = 0;
    for(size_t i = 0; i < data.length(); i+=4)
    {
        betMoney = *((uint32_t*)(buffer + i));
		betMoney = ntohl(betMoney);
        vtBetMoney.push_back(betMoney);
    }
    return true;
}

void MpLoadRoomMessageResponse::setBetMoney(const std::vector<uint32_t> &vtBetMoney)
{
    uint32_t len = vtBetMoney.size() << 2;
    uint8_t* buffer= new uint8_t[len];
    uint32_t betMoney = 0;
    for(size_t i = 0; i < vtBetMoney.size(); i++)
    {
        betMoney = vtBetMoney[i];
        betMoney = htonl(betMoney);
        *(uint32_t*)(buffer + i*4) = betMoney;
    }

    this->addComponent(MP_BET_MONEY,buffer, len );
	delete[] buffer;
}




