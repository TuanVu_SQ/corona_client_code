#include "MpSwapPositionResponse.h"
#include "MessageType.h"

MpSwapPositionResponse::MpSwapPositionResponse() :MpMessageRequest(MP_MSG_SWAP_POSITION_ACK)
{
}

void MpSwapPositionResponse::setListPlayerPosition(const std::vector<PlayerPosition> &vtPlayerPosition)
{
    std::string data;
    for(size_t i = 0; i < vtPlayerPosition.size(); ++i)
    {
        data += vtPlayerPosition[i].username;
        data += (char)(vtPlayerPosition[i].position);
    }

    this->addString(MP_PARAM_USERNAME, data);
}

bool MpSwapPositionResponse::getListPlayerPosition(std::vector<PlayerPosition> &vtPlayerPosition) const
{
	std::string data = this->getString(MP_PARAM_USERNAME);
    if(data.size() < 7)return false;
    const char* ptr = data.data();
    const char* endData = data.data() + data.size();
    PlayerPosition pp;
    while(ptr < endData)
    {
        if(*ptr < 0xF)
        {
            pp.position = *ptr;
            vtPlayerPosition.push_back(pp);
            pp.username.clear();
            pp.position = 0xF;
        }else
        {
            pp.username += *ptr;
        }
        ++ptr;
    }
    return true;
}

MpSwapPositionResponse::~MpSwapPositionResponse()
{

}
