#include "mprechargebycardmessageresponse.h"
#include "MessageType.h"
#include "MessageParam.h"

#if (MP_TARGET_PLATFORM == MP_PLATFORM_WIN32) || (MP_TARGET_PLATFORM == MP_PLATFORM_WINRT) || (MP_TARGET_PLATFORM == MP_PLATFORM_WP8)
#include <Winsock2.h>
#else
#include "netinet/in.h"
#endif

MpRechargeByCardMessageResponse::MpRechargeByCardMessageResponse(): MpMessageResponse(MP_MSG_RECHARGE)
{
    //this->m_pMsgHdr = makeMessage(MP_MSG_RECHARGE);
}

bool MpRechargeByCardMessageResponse::getBalance(uint32_t &Money) const
{
	Money = this->getInt(MP_PARAM_BALANCE);
	return true;
}

void MpRechargeByCardMessageResponse::setBalance(uint32_t Money) 
{
	this->addInt(MP_PARAM_BALANCE, Money);
}
