#include "mpgetinfopurchasemessagerequest.h"
#include "MessageType.h"

MpGetInfoPurchaseMessageRequest::MpGetInfoPurchaseMessageRequest() : MpMessageRequest(MP_MSG_GETINFO_PURCHASE)
{}

void MpGetInfoPurchaseMessageRequest::setChargeID(uint8_t pChargeID)
{
    this->addInt(MP_PARAM_CHARGE_ID, pChargeID);
}

bool MpGetInfoPurchaseMessageRequest::getChargeID(uint8_t &pChargeID) const
{
    pChargeID = getInt(MP_PARAM_CHARGE_ID);
    return true;
}

void MpGetInfoPurchaseMessageRequest::setOS(const std::string &os)
{
    this->addString(MP_PARAM_OS, os.c_str());
}

std::string MpGetInfoPurchaseMessageRequest::getOS() const
{
    return  this->getString(MP_PARAM_OS);
}

void MpGetInfoPurchaseMessageRequest::setCountry(const std::string &country)
{
    this->addString(MP_PARAM_COUNTRY, country.c_str());
}

std::string MpGetInfoPurchaseMessageRequest::getCountry() const
{
    return this->getString(MP_PARAM_COUNTRY);
}

void MpGetInfoPurchaseMessageRequest::setProductCode(const std::string &productCode)
{
    this->addString(MP_PARAM_PRODUCT_CODE, productCode);
}

std::string MpGetInfoPurchaseMessageRequest::getProductCode() const
{
    return this->getString(MP_PARAM_PRODUCT_CODE);
}
