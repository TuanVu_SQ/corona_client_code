#ifndef MPTIMERNOTIFY_H
#define MPTIMERNOTIFY_H

#include "./mpclient/protocol/MpMessage.h"
#include <time.h>

class MpTimerNotify: public mp::protocol::MpMessage
{
public:    
    bool getCurrentTime(time_t &t) const;
private:
    MpTimerNotify();
};

#endif // MPTIMERNOTIFY_H
