#include "mpchatmessagerequest.h"
#include "MessageType.h"

MpChatMessageRequest::MpChatMessageRequest() : MpMessageRequest(MP_MSG_CHAT_TEXT){}

MpChatMessageRequest::~MpChatMessageRequest() {}

void MpChatMessageRequest::setContent(const std::string& pContent) 
{
	this->addString(MP_PARAM_CHAT_CONTENT, pContent);
}

std::string MpChatMessageRequest::getContent() const 
{
	return this->getString(MP_PARAM_CHAT_CONTENT);
}

void MpChatMessageRequest::setUsername(const std::string& pUsername) 
{
	this->addString(MP_PARAM_USERNAME, pUsername);
}

std::string MpChatMessageRequest::getUsername() const 
{
	return this->getString(MP_PARAM_USERNAME);
}