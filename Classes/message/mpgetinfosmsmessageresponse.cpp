#include "mpgetinfosmsmessageresponse.h"
#include "MessageType.h"
#include "MessageParam.h"

MpGetInfoSMSMessageResponse::MpGetInfoSMSMessageResponse() : MpMessageResponse(MP_MSG_GETINFO_SMS_ACK)
{}

bool MpGetInfoSMSMessageResponse::getVtRecharge(std::vector<RechargeInfo> &vtRecharge) const
{
	std::string data = this->getString(MP_VECTOR_RECHARGE);
	if (data == "") return false;
	RechargeInfo rechargeInfo;
	size_t len = 0;
	while (len < data.size())
	{
		rechargeInfo.syntax = "";
		while (data[len] != 0)
		{
			rechargeInfo.syntax += data[len];
			len++;
		}
		len++;

		rechargeInfo.serviceNumber = "";
		while (data[len] != 0)
		{
			rechargeInfo.serviceNumber += data[len];
			len++;
		}
		len++;

		rechargeInfo.money = (*(uint32_t*)(data.data() + len));
		rechargeInfo.money = ntohl(rechargeInfo.money);
		len += 4;

		rechargeInfo.balance = *((uint32_t*)(data.data() + len));
		rechargeInfo.balance = ntohl(rechargeInfo.balance);
		len += 4;

		vtRecharge.push_back(rechargeInfo);
	}
	return true;
}

uint8_t MpGetInfoSMSMessageResponse::getEnable() const 
{
	return getInt(MP_PARAM_ENABLE);
}
