#ifndef MPLOGINREQUEST_H
#define MPLOGINREQUEST_H
#include "mpmessagerequest.h"

class MpLoginRequest : public MpMessageRequest
{
public:
    MpLoginRequest();
    MpLoginRequest(uint32_t tag);   // for facebook & guest login
    const std::string getVersion() const;
    const std::string getGameGroup() const;
    const std::string getOS() const;
    const std::string getPassword() const;
    const std::string getCRC() const;
    const std::string getFullname() const;
    const std::string getCpId() const;
    const std::string getUsername() const;

    void setVersion(const std::string &version);
    void setGameGroup(const std::string &gameGroup);
    void setOs(const std::string &os);
    void setPassword(const std::string &password); // md5
    void setFullname(const std::string &fullname);
    void setCpId(const std::string &cpId);
    void setUsername(const std::string &username);
	void setKey(const std::string &key);
};

#endif // MPLOGINREQUEST_H
