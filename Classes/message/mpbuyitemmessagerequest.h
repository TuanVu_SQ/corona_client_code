#ifndef __MPBUYITEMMESSAGEREQUEST_H_INCLUDED__
#define __MPBUYITEMMESSAGEREQUEST_H_INCLUDED__

#include "mpmessagerequest.h"
#include "MessageParam.h"

class MpBuyItemMessageRequest : public MpMessageRequest
{
public:
	MpBuyItemMessageRequest();
	
	bool getItemID(uint8_t &pItemID) const;
	void setItemID(uint8_t pItemID);

	bool getItemType(uint8_t &pItemType) const;
	void setItemType(uint8_t pItemType);

	bool getItemCounter(uint8_t &pItemCounter) const;
	void setItemCounter(uint8_t pItemCounter);

	bool getExpireType(uint8_t &pExpireType) const;
	void setExpireType(uint8_t pExpireType);
};

#endif // MPLOADTOPMESSAGEREQUEST_H
