#ifndef MPJOINTABLEMESSAGEREQUEST_H
#define MPJOINTABLEMESSAGEREQUEST_H
#include "mpmessagerequest.h"

class MpJoinTableMessageRequest : public MpMessageRequest
{
public:
    MpJoinTableMessageRequest();	
	MpJoinTableMessageRequest(uint32_t type);
    void setTableId(uint8_t roomId);
    uint8_t getTableId() const;
};

#endif // MPJOINTABLEMESSAGEREQUEST_H
