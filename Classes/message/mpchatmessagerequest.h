#ifndef __MPCHATMESSAGEREQUEST_H_INCLUDED__
#define __MPCHATMESSAGEREQUEST_H_INCLUDED__

#include "mpmessagerequest.h"

class MpChatMessageRequest : public MpMessageRequest
{
public:
	MpChatMessageRequest();
	virtual ~MpChatMessageRequest();

	void setContent(const std::string& pContent);
	std::string getContent() const;

	void setUsername(const std::string& pUsername);
	std::string getUsername() const;
};

#endif
