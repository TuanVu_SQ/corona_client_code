#ifndef __MPIAPINFOMESSAGEREQUEST_H_INCLUDED__
#define __MPIAPINFOMESSAGEREQUEST_H_INCLUDED__

#include "mpmessagerequest.h"
#include "MessageParam.h"

class MpIAPInfoMessageRequest : public MpMessageRequest
{
public:
	MpIAPInfoMessageRequest();

	void setProductCode(const std::string& pProductCode);
	std::string getProductCode() const;

	void setOS(const std::string& os);
	std::string getOS() const;

	void setItemCode(const std::string& itemCode);
	std::string getItemCode() const;

	void setUsername(const std::string& userName);
	std::string getUsername() const;

	void setDeviceID(const std::string& deviceID);
	std::string getDeviceID() const;
};

#endif
