#ifndef MPFIREBULLETMESSAGENOTIFY_H
#define MPFIREBULLETMESSAGENOTIFY_H
#include "./mpclient/protocol/MpClient.h"
#include "ClientPlayerInfo.h"
#include "mpfirebulletmessagerequest.h"

class MpFireBulletMessageNotify : public MpMessage
{
public:
    MpFireBulletMessageNotify();
    MpFireBulletMessageNotify(const MpFireBulletMessageRequest& msgRequest);
    void setUsername(const std::string& username);
    bool getUsername(std::string& username) const;
    void setBulletPosition(const MpPoint& point);
    bool getBulletPosition(MpPoint& point) const;
    void setGunLevel(uint8_t level);
    bool getGunLevel(uint8_t &level) const;

    void setFireMoney(uint32_t fireMoney);
    bool getFireMoney(uint32_t &fireMoney) const;
};

#endif // MPFIREBULLETMESSAGENOTIFY_H
