#ifndef MPDEADFISHMESSAGEREQUEST_H
#define MPDEADFISHMESSAGEREQUEST_H
#include "mpmessagerequest.h"
#include <vector>

class MpDeadFishMessageRequest: public MpMessageRequest
{
public:
    MpDeadFishMessageRequest();
    void setListFishes(const std::vector<uint8_t> &vtFish);
    bool getListFish(std::vector<uint8_t> &vtFish) const;
	void setTableId(uint32_t tableId);
	uint32_t getTableId() const;
};

#endif // MPDEADFISHMESSAGEREQUEST_H
