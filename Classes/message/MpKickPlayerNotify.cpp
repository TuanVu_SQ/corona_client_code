#include "MpKickPlayerNotify.h"
#include "MessageType.h"

MpKickPlayerNotify::MpKickPlayerNotify() :MpMessageResponse(MP_MSG_KICK_PLAYER_NTFY)
{}

void MpKickPlayerNotify::setUsername(const std::string &username)
{
    this->addString(MP_PARAM_USERNAME, username);
}

std::string  MpKickPlayerNotify::getUsername() const
{
    return this->getString(MP_PARAM_USERNAME);
}

