#ifndef MPDEADFISHMESSAGENOTIFY_H
#define MPDEADFISHMESSAGENOTIFY_H
#include <vector>
#include "./mpclient/protocol/MpClient.h"

class MpDeadFishMessageNotify : public mp::protocol::MpMessage
{
public:
    MpDeadFishMessageNotify();
    
    bool getUsername(std::string& username) const;
    void setUsername(const std::string& username);

    bool getListFish(std::vector<uint8_t> &vtFish) const;
    void setListFist(const std::vector<uint8_t> &vtFish);

	bool getFishMoney(uint32_t &fishMoney) const;
	void setFishMoney(uint32_t fishMoney);

    bool getFireMoney(uint32_t &fireMoney) const;
    void setFireMoney(uint32_t fireMoney);

};

#endif // MPDEADFISHMESSAGENOTIFY_H
