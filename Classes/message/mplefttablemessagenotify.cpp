#include "mplefttablemessagenotify.h"
#include "MessageType.h"

MpLeftTableMessageNotify::MpLeftTableMessageNotify() :MpMessage(MP_LEFT_TABLE_NTFY)
{
}

MpLeftTableMessageNotify::~MpLeftTableMessageNotify()
{

}

void MpLeftTableMessageNotify::setUsername(const std::string &username)
{

    this->addString(MP_PARAM_USERNAME, username);
}

bool MpLeftTableMessageNotify::getUsername(std::string &username) const
{
	username = this->getString(MP_PARAM_USERNAME);
	return true;
}

void MpLeftTableMessageNotify::setHostname(const std::string &username)
{
    this->addString(MP_PARAM_HOST_TABLE, username);
}

bool MpLeftTableMessageNotify::getHostname(std::string &hostname) const
{
	hostname = this->getString(MP_PARAM_HOST_TABLE);
	return true;
}
