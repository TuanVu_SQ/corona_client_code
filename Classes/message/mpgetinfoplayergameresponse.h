#ifndef MPGETINFOPLAYERMESSAGERESPONSE_H
#define MPGETINFOPLAYERMESSAGERESPONSE_H
#include "mpmessageresponse.h"
#include "ClientPlayerInfo.h"

//typedef struct ClientPlayerInfoEX_s{
//    uint8_t host;
//    uint8_t disallowKick;
//    uint8_t position;
//    uint8_t level;
//    uint8_t itemId;
//    uint32_t balance;
//    uint32_t balanceDefault;
//    uint32_t score;
//    std::string username;
//    bool isReady;
//    uint16_t winCount;
//    uint16_t loseCount;
//    uint32_t rank;
//    uint8_t avatarId;
//    ClientPlayerInfoEX_s()
//    {
//        username = "";
//        level = 0;
//        balance = 0;
//        position = 0;
//        score = 0;
//        host = 0;
//        disallowKick = 0;
//        itemId = 0;
//        balanceDefault = 0;
//        winCount = 0;
//        loseCount = 0;
//        rank = 987634;
//        avatarId = 0;
//    }
//    ClientPlayerInfoEX_s(const char* pUserName, uint8_t pLevel, uint32_t pBalance, uint8_t pPosition, uint8_t pBlockKick, uint8_t pItemId, bool pHost, bool pReady)
//    {
//        username = pUserName;
//        level = pLevel;
//        balance = pBalance;
//        position = pPosition;
//        host = pHost;
//        isReady = pReady;
//        itemId = pItemId;
//        disallowKick = pBlockKick;
//        balanceDefault = pBalance;
//        score = 0;
//    }
//}ClientPlayerInfoEX;

class MpGetInfoPlayerGameResponse : public MpMessageResponse{
public:
	MpGetInfoPlayerGameResponse();
	virtual ~MpGetInfoPlayerGameResponse();
	bool getPlayerInfo(ClientPlayerInfoEX & vtPlayer) const;
	void setPlayerInfo(ClientPlayerInfoEX & vtPlayer);
};

#endif 
