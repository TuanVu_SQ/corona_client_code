#ifndef __MPCHECKLUCKYMESSAGERESPONSE_H_INCLUDED__
#define __MPCHECKLUCKYMESSAGERESPONSE_H_INCLUDED__

#include "mpmessageresponse.h"
#include <vector>

class MpCheckLuckyMessageResponse :public MpMessageResponse {
public:
	MpCheckLuckyMessageResponse();
	
	bool getResult(uint8_t &pStatus) const;
	void setResult(uint8_t pStatus);

    void setPercentageResult(uint16_t pPercentageResult);
    uint16_t getPercentageResult() const;
};

#endif // MPLOADTOPMESSAGERESPONSE_H
