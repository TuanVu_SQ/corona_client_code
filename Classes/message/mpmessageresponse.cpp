#include "mpmessageresponse.h"
#include "MessageParam.h"
#ifdef _WIN32
#include <Winsock2.h>
#else
#include "netinet/in.h"
#endif

MpMessageResponse::MpMessageResponse(uint32_t tag):MpMessage(tag)
{}

MpMessageResponse::~MpMessageResponse()
{}

void MpMessageResponse::setErrorCode(uint32_t errorCode)
{
	addInt(MP_PARAM_ERROR_CODE, errorCode);
}

uint32_t MpMessageResponse::getErrorCode() const
{
	return getInt(MP_PARAM_ERROR_CODE);
}


void MpMessageResponse::setErrorDescription(const std::string& errorDescription)
{
    addString(MP_PARAM_ERROR_DESCRIPTION, errorDescription);
}
const std::string MpMessageResponse::getErrorDesciption() const
{
    return getString(MP_PARAM_ERROR_DESCRIPTION);
}
