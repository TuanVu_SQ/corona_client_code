#ifndef MPPLAYERREADYMESSAGEREQUEST_H
#define MPPLAYERREADYMESSAGEREQUEST_H
#include "mpmessagerequest.h"

class MpPlayerReadyMessageRequest : public MpMessageRequest
{
public:
    MpPlayerReadyMessageRequest();
};

#endif // MPPLAYERREADYMESSAGEREQUEST_H
