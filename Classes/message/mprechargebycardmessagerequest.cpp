#include "mprechargebycardmessagerequest.h"
#include "MessageType.h"

MpRechargeByCardMessageRequest::MpRechargeByCardMessageRequest() : MpMessageRequest(MP_MSG_RECHARGE)
{
    //this->m_pMsgHdr = makeMessage(MP_RECHARGE);
}

void MpRechargeByCardMessageRequest::setSerial(const std::string& pSerial)
{
	this->addString(MP_SERRIAL, pSerial);
}

std::string MpRechargeByCardMessageRequest::getSerial() const {
	return this->getString(MP_SERRIAL);
}

void MpRechargeByCardMessageRequest::setCard(const std::string& pCard) {
    this->addString(MP_PARAM_PASSWORD, pCard);
}

std::string MpRechargeByCardMessageRequest::getCard() const {
	return this->getString(MP_PARAM_PASSWORD);
}

void MpRechargeByCardMessageRequest::setTelco(const std::string& pTelco) {
	this->addString(MP_CARDTYPE, pTelco);
}

std::string MpRechargeByCardMessageRequest::getTelco() const {
	return this->getString(MP_CARDTYPE);
}

void MpRechargeByCardMessageRequest::setUsername(const std::string& pUsername) {
    this->addString(MP_PARAM_USERNAME, pUsername);
}

std::string MpRechargeByCardMessageRequest::getUsername() const
{
	return this->getString(MP_PARAM_USERNAME);
}

void MpRechargeByCardMessageRequest::setChargeID(uint8_t pChargeID)
{
	this->addInt(MP_PARAM_CHARGE_ID, pChargeID);
}

bool MpRechargeByCardMessageRequest::getChargeID(uint8_t &pChargeID) const
{
	pChargeID = this->getInt(MP_PARAM_CHARGE_ID);
	return true;
}
