#include "mpiapinfomessagerequest.h"
#include "MessageType.h"

MpIAPInfoMessageRequest::MpIAPInfoMessageRequest(): MpMessageRequest(MP_MSG_IAP_INFO)
{}

void MpIAPInfoMessageRequest::setOS(const std::string &os)
{
    this->addString(MP_PARAM_OS, os);
}

void MpIAPInfoMessageRequest::setProductCode(const std::string &productCode) 
{
	this->addString(MP_PARAM_PRODUCT_CODE, productCode);
}

void MpIAPInfoMessageRequest::setItemCode(const std::string &itemCode)
{
	this->addString(MP_PARAM_ITEM_CODE, itemCode);
}

void MpIAPInfoMessageRequest::setUsername(const std::string &username)
{
    this->addString(MP_PARAM_USERNAME, username);
}

void MpIAPInfoMessageRequest::setDeviceID(const std::string &deviceID)
{
    this->addString(MP_PARAM_DEVICE_ID, deviceID);
}
