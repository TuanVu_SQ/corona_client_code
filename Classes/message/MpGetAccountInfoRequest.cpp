#include "MpGetAccountInfoRequest.h"
#include "MessageType.h"
#include "MessageParam.h"

MpGetAccountInfoRequest::MpGetAccountInfoRequest() : MpMessageRequest(MP_MSG_GET_ACC_INFO)
{    
}

void MpGetAccountInfoRequest::setUsername(const std::string &username)
{
    this->addString(MP_PARAM_USERNAME, username);
}

bool MpGetAccountInfoRequest::getUsername(std::string &username) const
{
    username = this->getString(MP_PARAM_USERNAME);
	return true;
}
