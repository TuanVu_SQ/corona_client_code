#ifndef MPKICKPLAYERNOTIFY_H
#define MPKICKPLAYERNOTIFY_H
#include "mpmessageresponse.h"

/*
nErrCode = MP_PARAM_ITEM_ID : Co vat pham chong kick;
nErrCode = MP_PARAM_USERNAME : Username khong ton tai trong phong;
nErrCode = MP_HOST_TABLE: Khong phai chu phong, khong co quyen kick;
*/
class MpKickPlayerNotify : public MpMessageResponse
{
public:
    MpKickPlayerNotify();

    void setUsername(const std::string& username);
    std::string getUsername() const;

};
#endif // MPKICKPLAYERNOTIFY_H
