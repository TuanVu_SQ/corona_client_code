#ifndef MPLEFTTABLEMESSAGENOTIFY_H
#define MPLEFTTABLEMESSAGENOTIFY_H
#include "mpmessageresponse.h"
class MpLeftTableMessageNotify : public MpMessage
{
public:
    MpLeftTableMessageNotify();
	MpLeftTableMessageNotify(uint32_t pType) : MpMessage(pType){};
    virtual ~MpLeftTableMessageNotify();
    void setUsername(const std::string& username );
    bool getUsername(std::string& username ) const;

    void setHostname(const std::string& hostname);
    bool getHostname(std::string& hostname ) const;
};

#endif // MPLEFTTABLEMESSAGENOTIFY_H
