#ifndef MPGETACCOUNTINFOREQUEST_H
#define MPGETACCOUNTINFOREQUEST_H
#include "mpmessagerequest.h"


/*
1.    Tên
2.    Tiền
3.    Cấp độ
4.    Avatar
5.    Số trận thắng
6.    Số trận thua
7.    Hạng
8.	Là bạn bè hay không?
9.	Danh sách huân chương chiến công ( chi tiết ở phần huân chương )
 */

class MpGetAccountInfoRequest : public MpMessageRequest
{
public:
    MpGetAccountInfoRequest();
    void setUsername(const std::string& username);
    bool getUsername(std::string& username) const;
};

#endif // MPGETACCOUNTINFOREQUEST_H
