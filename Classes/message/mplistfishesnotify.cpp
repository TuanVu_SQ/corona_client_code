#include "mplistfishesnotify.h"
#include "MessageType.h"

MpListFishesNotify::MpListFishesNotify() : MpMessage(MP_LISH_FISH_NTFY)
{
}

void MpListFishesNotify::setListFishes(const std::vector<ClientFishInfo> &vtFishes)
{
    size_t len = vtFishes.size() * sizeof(ClientFishInfo);
    assert(len != 0);

    uint8_t *buffer = new uint8_t[len];
    uint8_t *ptr = buffer;

    ClientFishInfo cfi;
    for(size_t i = 0; i < vtFishes.size(); i++)
    {
        cfi = vtFishes[i];
        *((ClientFishInfo*)ptr) = cfi;
        ptr += sizeof(ClientFishInfo);
    }
    //this->addComponent(MP_LIST_FISH, buffer, len);
    delete[] buffer;
    buffer = NULL;
}

bool MpListFishesNotify::getLishFishes(std::vector<ClientFishInfo> &vtFishes) const
{
	std::string data = this->getString(MP_PARAM_LIST_FISH);
	if (data == "") return false;
    const uint8_t* ptr = (const uint8_t*)data.data();
    size_t len = 0;
    ClientFishInfo cfi;
    while (len <= data.size() - sizeof(ClientFishInfo))
    {
        cfi = *((ClientFishInfo*)(ptr + len));
		if (cfi.fishType == 254){
			cfi.fishType = 0;
		}
        vtFishes.push_back(cfi);
        len += sizeof(ClientFishInfo);
    }
    return true;
}
