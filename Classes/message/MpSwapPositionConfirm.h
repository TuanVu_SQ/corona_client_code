#ifndef MPSWAPPOSITIONCONFIRM_H
#define MPSWAPPOSITIONCONFIRM_H
#include "mpmessageresponse.h"

class MpSwapPositionConfirm : public MpMessageResponse
{
public:
    MpSwapPositionConfirm();
};
#endif // MPSWAPPOSITIONCONFIRM_H
