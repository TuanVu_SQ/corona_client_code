#ifndef MPMESSAGERESPONSE_H
#define MPMESSAGERESPONSE_H
#include "./mpclient/protocol/MpMessage.h"

using namespace mp;
using namespace protocol;
class MpMessageResponse : public MpMessage
{
public:

    MpMessageResponse(uint32_t tag);
    virtual ~MpMessageResponse();

    void setErrorCode(uint32_t errorCode);
    uint32_t getErrorCode() const;

    void setErrorDescription(const std::string& errorDescription);
    const std::string getErrorDesciption() const;	
		
private:
	MpMessageResponse();
};

#endif // MPMESSAGERESPONSE_H
