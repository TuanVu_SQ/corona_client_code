#ifndef MPREGISTERREQUEST_H
#define MPREGISTERREQUEST_H
#include "mpmessagerequest.h"

class MpRegisterRequest : public MpMessageRequest
{
public:
    MpRegisterRequest();
    MpRegisterRequest(uint32_t tag);

    const std::string getUsername() const;
    const std::string getPassword()const;
    const std::string getDeviceId() const;
    const std::string getCpId() const;        
    const std::string getOS() const;
    const std::string getFullname() const;
    const std::string getEmail() const;
    const std::string getCRC() const;

    void setUsername(const std::string & username);
    void setPassword(const std::string & password);
    void setDeviceId(const std::string & deviceId);
    void setCpId(const std::string & cpId);
    void setOS(const std::string & os);
    void setFullname(const std::string & os);
    void setEmail(const std::string & email);
};

#endif // MPREGISTERREQUEST_H
