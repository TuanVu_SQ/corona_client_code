#ifndef MPJOINTABLEMESSAGERESPONSE_H
#define MPJOINTABLEMESSAGERESPONSE_H
#include "mpmessageresponse.h"
#include "ClientPlayerInfo.h"
#include <vector>


typedef struct ReadyPlayer_s
{
	unsigned char position;
	unsigned char status;
}ReadyPlayer;

class MpJoinTableMessageResponse : public MpMessageResponse
{

public:
    MpJoinTableMessageResponse();    
	MpJoinTableMessageResponse(uint32_t pType) :MpMessageResponse(pType){};
    //Contructor Do nothing. It's made for Derived class reuse code.
    MpJoinTableMessageResponse(void*);

    virtual ~MpJoinTableMessageResponse();
    bool getListPlayer(std::vector<ClientPlayerInfo> & vtPlayer) const;
    void setListPlayer(const std::vector<ClientPlayerInfo> & vtPlayer);

    bool getListItem(std::vector <ClientItemInfo>& vtItem) const;
    void setListItem(const std::vector <ClientItemInfo>& vtItem);

    bool getListFishPrice(std::vector <uint16_t>& vtFishPrice) const;
    void setListFishPrice(const std::vector <uint16_t>& vtFishPrice);

    bool getListReadyPlayer(std::vector <ReadyPlayer>& vtListReady)const;
    void setListReadyPlayer(const std::vector <ReadyPlayer>& vtListReady);

    uint8_t getBackgroundId() const;
    void setBackgroundId(uint8_t backgroundId);

    uint8_t getRatioLevel() const;
    void setRatioLevel(uint8_t ratioLevel);

    //added for gameId = 1
    void setMinBetMoney(uint32_t minBetMoney);
    bool getMinBetMoney(uint32_t &minBetMoney) const;

    void setDefaultBetMoney(uint32_t defaultBetMoney);
    bool getDefaultBetMoney(uint32_t &defaultBetMoney) const;



	void setGameDuration(uint16_t duration);

	bool getGameDuration(uint16_t &duration) const;
};

#endif // MPJOINTABLEMESSAGERESPONSE_H
