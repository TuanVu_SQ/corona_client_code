#include "mpendgamemessagenotify.h"
#include "MessageType.h"
#include "ClientPlayerInfo.h"
#include <vector>
using std::string;

MpEndGameMessageNotify::MpEndGameMessageNotify() :MpMessage(MP_END_GAME_NTFY)
{
}

//void MpEndGameMessageNotify::setGameResult(const std::vector<GameRanker>& vtRank)
//{
//    // maximum player: 4.
//    // username is not include unicode character
//    std::string data;
//    char c[2] = {0,0};
//    for(size_t i = 0; i < vtRank.size(); i++)
//    {
//        GameRanker ranker = vtRank[i];
//        data.append(ranker.username);
//        c[0] = 0xF8 | (ranker.rank & 0x07);
//        data.append(c);
//    }
//    this->addComponent(MP_RESULT, (const uint8_t*) data.c_str(), data.size());
//}

//bool MpEndGameMessageNotify::getGameResult(std::vector<GameRanker>& vtRank) const
//{
//    std::string data;
//    char username[1024];
//    if(this->getString(MP_RESULT, data)==false) return false;
//    GameRanker ranker;
//    size_t len = data.size();
//    size_t i = 0;
//    size_t j = 0;
//    while(i < len)
//    {
//        if(((uint8_t) data[i] & 0xF8) == 0xF8)
//        {
//            username[j] = 0;
//            ranker.username = username;
//            ranker.rank = ((uint8_t) data[i] & 0x07);
//            j = 0;
//            vtRank.push_back(ranker);
//        }else
//        {
//            username[j] = data[i];
//            j++;
//        }
//        i++;
//    }
//    return true;
//}


void MpEndGameMessageNotify::setGameResult(const std::vector<GamePlayerResult>& vtRank)
{
    // maximum player: 4.
    // username is not include unicode character
	std::string buffer = "";
	uint32_t winMoney;
	for (size_t i = 0; i < vtRank.size(); i++)
	{
		const GamePlayerResult& player = vtRank[i];
		// username + 0
		buffer += player.Username;
		buffer += (char)0;

		// winMoney
		winMoney = htonl(player.WinMoney);
		buffer.append((const char*)&winMoney, 4);

		//Exp
		buffer += player.Exp;
	}
	//this->addComponent(MP_RESULT, (const uint8_t*)buffer.c_str(), buffer.length());
}

bool MpEndGameMessageNotify::getGameResult(std::vector<GamePlayerResult>& vtRank) const
{
	GamePlayerResult player;
	string data = this->getString(MP_RESULT);
	if (data == "") return false;
	size_t i = 0;
	while (i < data.length())
	{
		player.Username = "";
		//username
		for (; i < data.length(); i++)
		{
			if (data[i] != 0)
			{
				player.Username += data[i];
			}
			else
			{
				break; //of for
			}
		}
		if (data.length() - i < 6)
			break;

		i++;                          
		player.WinMoney = *(uint32_t*)(data.c_str() + i);
		player.WinMoney = ntohl(player.WinMoney);


		i += 4;                         
		player.Exp = (data[i]);
		vtRank.push_back(player);
		i += 1;
	}

    return true;
}

void MpEndGameMessageNotify::setWinFeePercentage(unsigned char percentage)
{   
	this->addInt(MP_PARAM_PERCENTAGE, percentage);
}

bool MpEndGameMessageNotify::getWinFeePercentage(unsigned char &percentage) const
{
	std::string data = this->getString(MP_PARAM_PERCENTAGE);
	if (data == "") return false;
    if(data.data() == NULL) return false;
    percentage = *data.data();
    return true;
}
