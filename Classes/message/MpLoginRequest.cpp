#include "MpLoginRequest.h"
#include "MessageType.h"
MpLoginRequest::MpLoginRequest() : MpMessageRequest(MP_MSG_LOGIN)
{}

MpLoginRequest::MpLoginRequest(uint32_t tag) : MpMessageRequest(tag)
{}

const std::string MpLoginRequest::getVersion() const
{
    return this->getString(MP_PARAM_VERSION);
}

const std::string MpLoginRequest::getGameGroup() const
{
    return this->getString(MP_PARAM_GAME_GROUP);
}

const std::string MpLoginRequest::getOS() const
{
    return this->getString(MP_PARAM_OS);
}

const std::string MpLoginRequest::getPassword() const
{
    return this->getString(MP_PARAM_PASSWORD);
}

const std::string MpLoginRequest::getCRC() const
{
    return this->getString(MP_PARAM_CRC);
}

const std::string MpLoginRequest::getFullname() const
{
    return this->getString(MP_PARAM_FULLNAME);
}

const std::string MpLoginRequest::getCpId() const
{
    return this->getString(MP_PARAM_CP_ID);
}

const std::string MpLoginRequest::getUsername() const
{
    return this->getString(MP_PARAM_USERNAME);
}

void MpLoginRequest::setVersion(const std::string &version)
{
    this->addString(MP_PARAM_VERSION, version);
}

void MpLoginRequest::setGameGroup(const std::string &gameGroup)
{
    this->addString(MP_PARAM_GAME_GROUP, gameGroup);
}

void MpLoginRequest::setOs(const std::string &os)
{
    this->addString(MP_PARAM_OS, os);
}

void MpLoginRequest::setPassword(const std::string &password)
{
    this->addString(MP_PARAM_PASSWORD, password);
}

void MpLoginRequest::setFullname(const std::string &fullname)
{
    this->addString(MP_PARAM_FULLNAME, fullname);
}

void MpLoginRequest::setCpId(const std::string &cpId)
{
    this->addString(MP_PARAM_CP_ID, cpId);
}

void MpLoginRequest::setUsername(const std::string &username)
{
    this->addString(MP_PARAM_USERNAME, username);
}
void MpLoginRequest::setKey(const std::string &key)
{
	if (key.size() > 0)
	{
		/*uint32_t h = hash(key);
		this->addInt(MP_PARAM_CRC, h);*/
	}
}
