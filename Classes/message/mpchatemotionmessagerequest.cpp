#include "mpchatemotionmessagerequest.h"
#include "MessageType.h"

//#include "FunctionGlobal.h"
//#include "KeyGlobal.h"

MpChatEmotionMessageRequest::MpChatEmotionMessageRequest() :MpMessageRequest(MP_MSG_CHAT_EMOTION){
}


MpChatEmotionMessageRequest::~MpChatEmotionMessageRequest() {}

void MpChatEmotionMessageRequest::setEmotionType(const std::string& pEmotionType) 
{
	this->addString(MP_PARAM_CHAT_EMOTION_TYPE, pEmotionType);
}

std::string MpChatEmotionMessageRequest::getEmotionType() const 
{
	return this->getString(MP_PARAM_CHAT_EMOTION_TYPE);
}

void MpChatEmotionMessageRequest::setEmotionID(uint8_t pEmotionID)
{
	this->addInt(MP_PARAM_CHAT_EMOTION_ID, pEmotionID);
}

uint8_t MpChatEmotionMessageRequest::getEmotionID() const
{
	return getInt(MP_PARAM_CHAT_EMOTION_ID);
}

void MpChatEmotionMessageRequest::setUsername(const std::string& pUsername)
{
	this->addString(MP_PARAM_USERNAME, pUsername);
}

std::string MpChatEmotionMessageRequest::getUsername() const
{
	return this->getString(MP_PARAM_USERNAME);
}
