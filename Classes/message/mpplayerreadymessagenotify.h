#ifndef MPPLAYERREADYMESSAGENOTIFY_H
#define MPPLAYERREADYMESSAGENOTIFY_H
#include "mplefttablemessagenotify.h"

class MpPlayerReadyMessageNotify : public MpLeftTableMessageNotify
{
public:
    MpPlayerReadyMessageNotify();
	MpPlayerReadyMessageNotify(uint32_t pType) :MpLeftTableMessageNotify(pType){};
};

#endif // MPPLAYERREADYMESSAGENOTIFY_H
