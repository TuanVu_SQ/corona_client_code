#include "MpSwapPositionRequest.h"
#include "MessageType.h"
#include <string>

MpSwapPositionRequest::MpSwapPositionRequest() :MpMessageRequest(MP_MSG_SWAP_POSITION_REQ)
{
}

MpSwapPositionRequest::~MpSwapPositionRequest()
{
}

void MpSwapPositionRequest::setPlayerPosition(const PlayerPosition &pp)
{
    std::string data;
    data += pp.username;
    data += (char)(pp.position);
	this->addString(MP_PARAM_USERNAME, data);
}

bool MpSwapPositionRequest::getPlayerPostition(PlayerPosition &pp) const
{
	std::string data = this->getString(MP_PARAM_USERNAME);
    if(data.size() < 7)return false;
    const char* ptr = data.data();
    const char* endData = data.data() + data.size();
    pp.position = 0xFF;
    while(ptr < endData)
    {
        if(*ptr < 0xF)
        {
            pp.position = *ptr;
            break;
        }else
        {
            pp.username += *ptr;
        }
        ++ptr;
    }
    if(pp.position == 0xFF) return false;
    return true;
}
