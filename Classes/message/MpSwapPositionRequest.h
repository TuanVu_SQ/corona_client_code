#ifndef MPSWAPPOSITIONREQUEST_H
#define MPSWAPPOSITIONREQUEST_H
#include "mpmessagerequest.h"

typedef struct PlayerPosition_s
{
    std::string username;
    unsigned char position;
}PlayerPosition;

class MpSwapPositionRequest: public MpMessageRequest
{
public:
    MpSwapPositionRequest();
    ~MpSwapPositionRequest();
    void setPlayerPosition(const PlayerPosition& pp);
    bool getPlayerPostition(PlayerPosition &pp) const;
};
#endif // MPSWAPPOSITIONREQUEST_H
