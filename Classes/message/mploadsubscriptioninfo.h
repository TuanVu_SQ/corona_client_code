﻿#pragma once
#include "mpmessagerequest.h"
#include "mpmessageresponse.h"
#include "common/ClientPlayerInfo.h"
#include <vector>


class MpLoadSubscriptionInfoResponse : public MpMessageResponse
{
public:
    MpLoadSubscriptionInfoResponse();
    void set(const std::vector<struct SubscriptionInfo> &);
    const std::vector<struct SubscriptionInfo> get() const;
};
