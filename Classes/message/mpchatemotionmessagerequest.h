#ifndef __MPCHATEMOTIONMESSAGEREQUEST_H_INCLUDED__
#define __MPCHATEMOTIONMESSAGEREQUEST_H_INCLUDED__

#include "mpmessagerequest.h"

class MpChatEmotionMessageRequest : public MpMessageRequest
{
public:
	MpChatEmotionMessageRequest();
	virtual ~MpChatEmotionMessageRequest();

	void setEmotionType(const std::string& pEmotionType);
	std::string getEmotionType() const;

	void setEmotionID(uint8_t pEmotionID);
	uint8_t getEmotionID() const;

	void setUsername(const std::string& pUsername);
	std::string getUsername() const;
};

#endif
