#ifndef MPFIREBULLETMESSAGEREQUEST_H
#define MPFIREBULLETMESSAGEREQUEST_H
#include "mpmessagerequest.h"

#include "ClientPlayerInfo.h"

class MpFireBulletMessageRequest : public MpMessageRequest
{    
public:
    MpFireBulletMessageRequest();    
    void setBulletPosition(const MpPoint& point);
    bool getBulletPosition(MpPoint& point) const;
    void setGunLevel(uint8_t level);
    bool getGunLevel(uint8_t &level) const;
	void setTableId(uint32_t tableId);
	uint32_t getTableId() const;
};

#endif // MPFIREBULLETMESSAGEREQUEST_H
