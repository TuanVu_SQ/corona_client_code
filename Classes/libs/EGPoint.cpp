#include "EGPoint.h"

EGPoint* EGPoint::create(float pX,float pY) {
	EGPoint *pEGPoint = new EGPoint();
	pEGPoint->init(pX,pY);

	return pEGPoint;
}

EGPoint* EGPoint::create(Point Point) {
	EGPoint *pEGPoint = new EGPoint();
	pEGPoint->init(Point.x, Point.y);

	return pEGPoint;
}

bool EGPoint::init(float pX,float pY) {
	mX = pX;
	mY = pY;

	return true;
}