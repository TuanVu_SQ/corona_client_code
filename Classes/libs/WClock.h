#pragma once

#include "WConfig.h"

#if !defined(_WIN32) && !defined(_WIN64) // Linux - Unix
#  include <sys/time.h>
typedef timeval sys_time_t;
inline void system_time(sys_time_t* t) {
	gettimeofday(t, NULL);
}
inline long long time_to_msec(const sys_time_t& t) {
	return t.tv_sec * 1000LL + t.tv_usec / 1000;
}
#else // Windows and MinGW
#  include <sys/timeb.h>
typedef _timeb sys_time_t;
inline void system_time(sys_time_t* t) 
{ 
	_ftime(t); 
}

inline long long time_to_msec(const sys_time_t& t) 
{
	return t.time * 1000LL + t.millitm;
}
#endif

struct Time 
{
	void restart() 
	{
		system_time(&t); 
	}

	uint64_t msec() const 
	{ 
		return time_to_msec(t); 
	}

	long long elapsed() const 
	{
		return (long long)(current_time().msec() - time_to_msec(t));
	}
	
	static Time current_time() 
	{
		Time t; 
		t.restart(); 
		return t; 	
	}
private:
	sys_time_t t;
};

class WClock {
public:
	static WClock* create();
	static void setTimeZoneCurrent(int timezoneCurrent);
	void start();
	int getDelay();
	double getDelayMilisecond();
	struct tm* getTimeNow();
	static const long long &getNowTimeMilisecond();
	static struct tm* convertIntToTime(int);
	static int getTimeZone();

private:
	Time _time;

public:
	WClock();
	~WClock();
};