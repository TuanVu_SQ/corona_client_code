#include "EGEffectActionManager.h"

ActionInterval* EGEffectActionManager::getEffectScaleBuble(bool isAppear,float times) {
	if(isAppear) {
		ScaleTo* pScale1 = ScaleTo::create(times*6/10,1.2f,1.2f);
		ScaleTo* pScale2 = ScaleTo::create(times*2/10,0.9f,0.9f);
		ScaleTo* pScale3 = ScaleTo::create(times*2/10,1,1);
		return Sequence::create(pScale1,pScale2,pScale3,NULL);
	} else {
		ScaleTo* pScale1 = ScaleTo::create(times*3/10,1.2f,1.2f);
		ScaleTo* pScale2 = ScaleTo::create(times*7/10,0,0);
		return Sequence::create(pScale1,pScale2,NULL);
	}
}

ActionInterval* EGEffectActionManager::getActionMoveToWithDelay(float timesDelay,float times,Point Point) {
	DelayTime *pDelaytime = DelayTime::create(timesDelay);
	MoveTo *pMoveAction = MoveTo::create(times,Point);
	return Sequence::create(pDelaytime,pMoveAction,NULL);
}

ActionInterval* EGEffectActionManager::getActionScaleToWithDelay(float timesDelay,float times,float pScaleX,float pScaleY) {
	DelayTime *pDelaytime = DelayTime::create(timesDelay);
	ScaleTo *pScaleAction = ScaleTo::create(times,pScaleX,pScaleY);
	return Sequence::create(pDelaytime,pScaleAction,NULL);
}

ActionInterval* EGEffectActionManager::getActionRotateToWithDelay(float timesDelay,float times,float pRotateX,float pRotateY) {
	DelayTime *pDelaytime = DelayTime::create(timesDelay);
	RotateTo *pRotateAction = RotateTo::create(times,pRotateX,pRotateY);
	return Sequence::create(pDelaytime,pRotateAction,NULL);
}