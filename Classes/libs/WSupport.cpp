#include "WSupport.h"

Animate* WSupport::createAnimateFrom(const char* lnk, int start, int end, float delay, bool showlog) {
	Vector<SpriteFrame*> _list;
	for (int id = start; id <= end; id++) {
		const char* txt = __String::createWithFormat(lnk, id)->getCString();
		if (!checkTextureCache(txt, showlog)) {
			if (showlog)
				log("Error 'createAnimateFrom': %s", txt);
			break;
		}
		_list.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(txt));
	}
	return Animate::create(Animation::createWithSpriteFrames(_list, delay));
}

bool WSupport::checkTextureCache(const char* link, bool showlog) {
	if (strlen(link) == 0) return false;
	SpriteFrameCache *cache = SpriteFrameCache::getInstance();
	SpriteFrame *spriteFrame = cache->getSpriteFrameByName(link);
	if (spriteFrame == nullptr) {
		auto sprite = Sprite::create(link);
		if (sprite == nullptr)  {
			if (showlog)
				log("Error 'checkTextureCache': %s", link);
			return false;
		}
		auto frame = SpriteFrame::create(link, sprite->getTextureRect());
		cache->addSpriteFrame(frame, link);
		Director::getInstance()->getTextureCache()->removeTextureForKey(link);
	}
	return true;
}

bool WSupport::checkTextureFile(const char* link, bool showlog) {
	if (strlen(link) == 0) return false;
	Sprite* spr = Sprite::create(link);
	if (spr) return true;
	if (showlog)
		log("Error 'checkTextureFile': %s", link);
	return false;
}

bool WSupport::isSpecialCharacterInString(std::string textInput, bool isNumber, std::string textBreak) {
	std::string text = textInput;
	std::transform(text.begin(), text.end(), text.begin(), ::tolower);
	std::vector<char> listChar;
	std::copy(textBreak.begin(), textBreak.end(), std::back_inserter(listChar));

	int i = 0;
	while (text[i]) {
		char& key = text[i];
		if (key < 'a' || key > 'z') {
			if (key >= '0' && key <= '9') {
				if (!isNumber) {
					return false;
				}
				else {
					i++;
					continue;
				}
			}
			std::vector<char>::iterator it = std::find(listChar.begin(), listChar.end(), key);
			if (it != listChar.end()) {
				i++;
				continue;
			}
			return true;
		}
		i++;
	}

	return false;
}

bool WSupport::equalVec2(Vec2 a, Vec2 b) {
	if ((int)a.x == (int)b.x && (int)a.y == (int)b.y) return true;
	return false;
}

bool WSupport::equalRect(cocos2d::Rect a, cocos2d::Rect b) {
	if ((int)a.getMaxX() == (int)b.getMaxX() && (int)a.getMinX() == (int)b.getMinX()
		&& (int)a.getMaxY() == (int)b.getMaxY() && (int)a.getMinY() == (int)b.getMinY()) return true;
	return false;
}

std::string WSupport::convertIntToString(uint32_t i) {
	static char str[20];
	sprintf(str, "%d", i);
	return (std::string)str;
}

std::string WSupport::convertMoneyAndAddDot(uint32_t i) {
	std::string money = convertIntToString(i);
	int pCounterDot = (money.length() - 1) / 3;
	int pIndex = money.length();
	std::string newStrMoney;
	while (pCounterDot > 0) {
		newStrMoney = "." + money.substr(pIndex - 3, 3) + newStrMoney;
		pCounterDot--;
		pIndex -= 3;
	}
	newStrMoney = money.substr(0, pIndex) + newStrMoney;
	return newStrMoney;
}

//support for 0 -> 999.999.999.999
std::string WSupport::convertMoneyAndAddText(uint32_t i) {
	std::string newString;
	uint32_t intOrigin = i;
	uint32_t intExcess = 0;
	uint8_t level = 0;
	while (intOrigin >= 10000) {
		intExcess = intOrigin % 1000;
		intOrigin /= 1000;
		level++;
	}
	newString += convertIntToString(intOrigin);
	if (intExcess >= 10) {
		std::string stringExecess = convertIntToString((uint32_t)(intExcess / 10));
		newString += "." + (stringExecess.length() == 1 ? "0" + stringExecess : stringExecess);
	}
	switch (level)
	{
	case 1:
		newString += "K";
		break;
	case 2:
		newString += "M";
		break;
	case 3:
		newString += "B";
		break;
	default:
		return convertIntToString(i);
	}
	return newString;
}

std::string WSupport::convertIntToTime(int i) {
	if (i <= 0) return "00:00";
	int minus = i / 60;
	int second = i % 60;
	std::string ss;
	std::string minusString = convertIntToString(minus);
	std::string secondString = convertIntToString(second);
	ss += (minusString.length() == 1) ? "0" : "" + minusString + ":";
	ss += (secondString.length() == 1) ? "0" : "" + secondString;
	return ss;
}

int WSupport::convertStringToInt(std::string text) {
	return atoi(text.c_str());
}

float WSupport::getDirectionByRotate(Vec2 first, Vec2 last) {
	return getDirectionByRotate(first.x, first.y, last.x, last.y);
}

float WSupport::getDirectionByRotate(float startX, float startY, float endX, float endY) {
	float pDistanceX = endX - startX;
	float pDistanceY = endY - startY;

	return atan2(pDistanceX, pDistanceY) * 180 / 3.14159265f;
}

Vec2 WSupport::getPointInCircle(Vec2 center, float pRadius, float pAngle) {
	return getPointInCircle(center.x, center.y, pRadius, pAngle);
}

Vec2 WSupport::getPointInCircle(float centerX, float centerY, float radius, float angle) {
	Vec2 newPoint;
	float constX = sin(angle* M_PI / 180);
	float constY = cos(angle* M_PI / 180);
	newPoint.x = centerX + radius* constX;
	newPoint.y = centerY + radius* constY;
	return newPoint;
}

float WSupport::countDistance(Vec2 vec2Start, Vec2 vec2End) {
	return countDistance(vec2Start.x, vec2Start.y, vec2End.x, vec2End.y);
}

float WSupport::countDistance(float fStartX, float fStartY, float fEndX, float fEndY) {
	return sqrt((fEndX - fStartX)*(fEndX - fStartX) + (fEndY - fStartY)*(fEndY - fStartY));
}

std::vector<int> WSupport::convertStringtoVectorInt(std::string origin) {
	std::vector<int> sequence;
	char COMMA = ',';

	origin += COMMA;
	std::istringstream stm(origin);

	int value;
	char delimiter;
	while (stm >> value >> delimiter && (delimiter == COMMA))
		sequence.push_back(value);

	return sequence;
}

std::string WSupport::convertVectorIntToString(std::vector<uint8_t> origin) {
	std::stringstream result;
	std::copy(origin.begin(), origin.end(), std::ostream_iterator<int>(result, ","));
	return result.str();
}

std::string WSupport::addSpaceInStdString(std::string origin, int maxLength) {
	if (maxLength <= 0) return origin;
	std::string stringReturn;
	int length = 0;
	while (origin.length() > 0) {
		char characterNow = origin[0];
		if (characterNow != ' ')
			length++;
		else length = 0;

		if (length >= maxLength) {
			stringReturn += characterNow;
			stringReturn += ' ';
			length = 0;
		}
		else {
			stringReturn += characterNow;
		}
		origin.erase(origin.begin() + 0);
	}
	return stringReturn;
}