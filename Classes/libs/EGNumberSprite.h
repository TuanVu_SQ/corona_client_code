#ifndef __EGNUMBERSPRITE_H_INCLUDED__
#define __EGNUMBERSPRITE_H_INCLUDED__

#include "cocos2d.h"
#include "EGSupport.h"

using namespace cocos2d;

class EGNumberSprite:public Node {
public:
	static EGNumberSprite* create(std::string fileName);
	static EGNumberSprite* createWithSpriteFrameName(std::string fileName);
	void setNumber(int pNumber);
	void setNumber(int pNumber,bool iDot);
	void addNumber(int pNumber);

	int getValues();

	void setPosition(Vec2 ccp);
	void setPositionX(float pX);
	void setPositionY(float pY);
	void setFileName(std::string fileName);
	Size getContentSize();
	void setOpacity(GLbyte pOpacity);
	void setOpacity_Values(GLbyte pOpacity);
	GLbyte getOpacity();
	void runAction(ActionInterval* pActionInterval);
	void stopAllActions();
	void setColor(Color3B pColor3b);
	void setColor_Values(Color3B pColor3b);
	void setMinLenght(int pMinLenght) {
		mMinLenght = pMinLenght;
	}
	void setDistanceSpace(float pDistanceSpace) {
		mDistanceSpace = pDistanceSpace;
	}
private:
	int mType;
	int mMinLenght;
	float mDistanceSpace;
	GLbyte mOpacity;
	int mNumber;
	std::vector<Sprite*> mListNumber;
	float mWidth,mHeight;
	std::string mFileName;

	bool init(std::string fileName);
	bool initWithSpriteFrameName(std::string fileName);
	void update(float dt);
};

#endif