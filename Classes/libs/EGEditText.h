#ifndef __EGEDITTEXT_H_INCLUDED__
#define __EGEDITTEXT_H_INCLUDED__

#include "cocos2d.h"

#include "EGSprite.h"

using namespace cocos2d;

class EGEditText :public EGSprite{
public:
	static EGEditText* createWithFile(const char*pPathImage ,const char*pFont,float pSizeFont);
	static EGEditText* createWithSpriteFrameName(const char*pPathImage ,const char*pFont,float pSizeFont);

	bool initWithFile(const char*pPathImage,const char*pFont,float pSizeFont);
	bool initWithSpriteFrameName(const char*pPathImage,const char*pFont,float pSizeFont);

	virtual bool TouchBegan(Touch*ptouch,Event*pevent);

	std::string getText();
	void setText(std::string pText);
	void setHint(std::string pHint);

	void setOpacity(GLubyte glubype);
	void setDimension(float pWidth,float pHeight);
	void setIsPasswords(bool isPasswords);
private:
	Sprite *mEditText;
	TextFieldTTF *mText;
	TextFieldTTF *mVirtualText;
	TextFieldTTF *mHint;
	int mDimension;
	bool mIsPasswords;

};
#endif
