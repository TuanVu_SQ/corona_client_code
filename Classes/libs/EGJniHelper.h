#ifndef __JNIHELPER_H_INCLUDED__
#define __JNIHELPER_H_INCLUDED__

#include "cocos2d.h"
#include "common/GameInfo.h"

#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) 
#include "platform\android\jni\JniHelper.h"
#define CLASS_NAME "bca/jni/EGJniHelper"

class EGJniHelper {
public:
	static void showDialogQuit() {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "quitGame", "()V")) {
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void showTopOnline() {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "showTopOnline", "()V")) {
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void showAchievement() {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "showAchievement", "()V")) {
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void unlockAchievement(const char* pUnlockCode) {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "unlockAchievement", "(Ljava/lang/String;)V")) {
			jstring unlockCode = methodInfo.env->NewStringUTF(pUnlockCode);
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, unlockCode);
			methodInfo.env->DeleteLocalRef(unlockCode);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static std::string _getStringToJava(const char *_className, const char* _methodName) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		JniMethodInfo methodInfo;
		const char* result;

		if (JniHelper::getStaticMethodInfo(methodInfo, _className, _methodName, "()Ljava/lang/String;")) {
			jstring returnString = (jstring) methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);
			result = methodInfo.env->GetStringUTFChars(returnString, NULL);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
		return result;
#endif
		return "";
	}
	static bool isOnline() {
		return _getStringToJava(CLASS_NAME, "isOnline") != "0";
	}
	static bool isPackageInstalled(const char* pPackageName) {
		return _getStringToJava(CLASS_NAME, "isPackageInstalled") != "0";	
	}
	static void insertTopScore(int pTopScore) {
		JniMethodInfo methodInfo;
		jint score = pTopScore;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "insertTopScore", "(I)V")) {
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, score);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static bool checkActive() {
		return _getStringToJava(CLASS_NAME, "checkIsActive") != "0";	
	}
	static bool checkShowTopUp() {
		return _getStringToJava(CLASS_NAME, "checkShowTopUp") != "0";
	}
	static bool checkShowTopUp1st() {
		return _getStringToJava(CLASS_NAME, "checkShowTopUp1st") != "0";
	}
	static void showAdmob() {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "showAdmob", "()V")) {
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void hideAdmob() {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "hideAdmob", "()V")) {
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void showPopUp() {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "showPopup", "()V")) {
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void showBigPopUp() {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "showBigPopup", "()V")) {
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void setGravityAdBotCenter() {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "setGravityAdBotCenter", "()V")) {
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void logOutFacebook() {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "logOutFacebook", "()V")) {
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void setGravityAdBotLeft() {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "setGravityAdBotLeft", "()V")) {
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void showToast(const char* pMessage) {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "showToast", "(Ljava/lang/String;)V")) {
			jstring message = methodInfo.env->NewStringUTF(pMessage);
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, message);
			methodInfo.env->DeleteLocalRef(message);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void moreGame() {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "moreGame", "()V")) {
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void openLink(const char* pLink) {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "openLink", "(Ljava/lang/String;)V")) {
			jstring link = methodInfo.env->NewStringUTF(pLink);
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, link);
			methodInfo.env->DeleteLocalRef(link);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void callSetupPlayService() {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "callSetupPlayService", "()V")) {
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void rateGame() {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "rateGame", "()V")) {
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void share() {
	}
	static const char* getDeviceID() {
		JniMethodInfo methodInfo;
		const char* result;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "getDeviceID", "()Ljava/lang/String;")) {
			jstring returnString = (jstring) methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);
			result = methodInfo.env->GetStringUTFChars(returnString, NULL);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);

		return result;
	}
	static const char* getCPID() {
		JniMethodInfo methodInfo;
		const char* result;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "getCPID", "()Ljava/lang/String;")) {
			jstring returnString = (jstring) methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);
			result = methodInfo.env->GetStringUTFChars(returnString, NULL);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);

		return result;
	}
	static const char* getRechargeID() {
		JniMethodInfo methodInfo;
		const char* result;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "getRechargeID", "()Ljava/lang/String;")) {
			jstring returnString = (jstring) methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);
			result = methodInfo.env->GetStringUTFChars(returnString, NULL);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);

		return result;
	}
	static void sendSMS(const char* pSyntax, const char* pServiceNumber) {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "sendSMS", "(Ljava/lang/String;Ljava/lang/String;)V")) {
			jstring syntax = methodInfo.env->NewStringUTF(pSyntax);
			jstring serviceNumber = methodInfo.env->NewStringUTF(pServiceNumber);
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, syntax, serviceNumber);
			methodInfo.env->DeleteLocalRef(syntax);
			methodInfo.env->DeleteLocalRef(serviceNumber);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static const char* getCountry() {
		JniMethodInfo methodInfo;
		const char* result;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "getCountry", "()Ljava/lang/String;")) {
			jstring returnString = (jstring) methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);
			result = methodInfo.env->GetStringUTFChars(returnString, NULL);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);

		return result;
	}
	static void showTopUp(int pTypeMoney, int pTypeCharging) {
		JniMethodInfo methodInfo;
		const char* functionName = "showTopUp";
		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, functionName, "(II)V")) {
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID,pTypeMoney,pTypeCharging);
		}
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void loginFacebook() {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "loginFacebook", "()V")) {
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static void purchaseItem(std::string pItemID, std::string pUsername) {
		JniMethodInfo methodInfo;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "purchaseItem", "(Ljava/lang/String;Ljava/lang/String;)V")) {
			jstring itemID = methodInfo.env->NewStringUTF(pItemID.c_str());
			jstring userName = methodInfo.env->NewStringUTF(pUsername.c_str());

			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, itemID, userName);
			methodInfo.env->DeleteLocalRef(itemID);
			methodInfo.env->DeleteLocalRef(userName);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	static const char* getPackage() {
		JniMethodInfo methodInfo;
		const char* result;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "getPackage", "()Ljava/lang/String;")) {
			jstring returnString = (jstring) methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);
			result = methodInfo.env->GetStringUTFChars(returnString, NULL);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);

		return result;
	}

	static const char* getTelco() {
		JniMethodInfo methodInfo;
		const char* result;

		if (JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "getTelco", "()Ljava/lang/String;")) {
			jstring returnString = (jstring) methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);
			result = methodInfo.env->GetStringUTFChars(returnString, NULL);
		}

		methodInfo.env->DeleteLocalRef(methodInfo.classID);

		return result;
	}

	static std::string getIP() {
		return GameInfo::loginServerAddress;
	}
	static std::string getIPRegister() {
		return GameInfo::loginServerAddress;
	}
};
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

void doSendSMS(const char* pSyntax, const char* pServiceNumber);
const char* getDeviceIdIOS();
void doPurchaseItem(std::string pItem, std::string pUsername);

class EGJniHelper {
public:
	static void logOutFacebook() {
	}
	static void showDialogQuit() {
	}
	static void showTopOnline() {
	}
	static void showAchievement() {
	}
	static void unlockAchievement(const char* pUnlockCode) {
	}
	static bool isOnline() {
		return false;
	}
	static bool isPackageInstalled(const char* pPackageName) {
		return true;
	}
	static bool checkActive() {
		return true;
	}
	static void showAdmob() {
	}
	static void hideAdmob() {
	}
	static void insertTopScore(int pTopScore) {
	}
	static void showPopUp() {
	}
	static void showBigPopUp() {
	}
	static void setGravityAdBotCenter() {
	}
	static void setGravityAdBotLeft() {
	}
	static void showToast(const char* pMessage) {
	}
	static void moreGame() {
	}
	static void openLink(const char* pLink) {
	}
	static void callSetupPlayService() {
	}
	static void rateGame() {
	}
	static void share() {
	}
	static const char* getDeviceID() {
		return getDeviceIdIOS();
	}
	static const char* getCPID() {
		return "1";
	}
	static const char* getRechargeID() {
		return "1";
	}
	static const char* getTelco() {
		return "viettel";
	}
	static void sendSMS(const char* pSyntax, const char* pServiceNumber) {
        doSendSMS(pSyntax, pServiceNumber);
	}
	static const char* getCountry() {
		return "vn";
	}
	static void showTopUp(int pTypeMoney, int pTypeCharging) {
	}
	static void loginFacebook() {
	}
	static void purchaseItem(std::string pItem, std::string pUsername) {
        doPurchaseItem(pItem, pUsername);
	}
	static const char* getPackage() {
		return "BCOL";
	}
	static std::string getIP() {
		return GameInfo::loginServerAddress;
	}
	static std::string getIPRegister() {
		return GameInfo::loginServerAddress;
	}
	static bool checkShowTopUp() {
		return true;
	}
	static bool checkShowTopUp1st() {
		return true;
	}
};
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include "cocos2d.h"
#include "EGScene.h"
#include "WXmlReader.h"

using namespace cocos2d;

class EGJniHelper {
public:
	static void logOutFacebook() {
	}
	static void showDialogQuit() {
	}
	static void showTopOnline() {
	}
	static void showAchievement() {
	}
	static void unlockAchievement(const char* pUnlockCode) {
	}
	static bool isOnline() {
		return true;
	}
	static bool isPackageInstalled(const char* pPackageName) {
		return false;
	}
	static bool checkActive() {
		return true;
	}
	static void showAdmob() {
	}
	static void hideAdmob() {
	}
	static void insertTopScore(int pTopScore) {
	}
	static void showPopUp() {
	}
	static void showBigPopUp() {
	}
	static void setGravityAdBotCenter() {
	}
	static void setGravityAdBotLeft() {
	}
	static void showToast(const char* pMessage) {
	}
	static void moreGame() {
	}
	static void openLink(const char* pLink) {
	}
	static void callSetupPlayService() {
	}
	static void rateGame() {
	}
	static void share() {
	}
	static string getDeviceID() {
		WXmlReader* xml = WXmlReader::create();
		xml->load("string.xml");
		return xml->getNodeTextByTagName("device_id");
	}
	static const char* getCPID() {
		return "1";
	}
	static const char* getTelco() {
		return "natcom";
	}
	static const char* getRechargeID() {
		return "1";
	}
	static void sendSMS(const char* pSyntax, const char* pServiceNumber) {
	}
	static const char* getCountry() {
		return "ht";
	}

	static void showTopUp(int pTypeMoney, int pTypeCharging) {
		EGScene* pScene = (EGScene*)Director::getInstance()->getRunningScene();
		if (pScene)
			pScene->onSMSResultCallback(0, 200000);
	}
	static void loginFacebook() {
	}
	static void purchaseItem(std::string pItem, std::string pUsername) {
	}
	static const char* getPackage() {
		return "vn.mgl.bcol";
	}
	static std::string getIP() {
		WXmlReader* xml = WXmlReader::create();
		xml->load("string.xml");
		string strIP = xml->getNodeTextByTagName("ip_server").c_str();
		return strIP;
	}
	static std::string getIPRegister() {
		WXmlReader* xml = WXmlReader::create();
		xml->load("string.xml");
		string strIP = xml->getNodeTextByTagName("GameInfo::loginServerAddress").c_str();
		return strIP;
	}
	static bool checkShowTopUp() {
		return true;
	}
	static bool checkShowTopUp1st() {
		return true;
	}
};
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8 || CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
//#include "AdControlDelegate.h"
//
//using namespace PhoneDirect3DXamlAppComponent::AdControlHelper;

class EGJniHelper {
public:
	static void logOutFacebook() {
	}
	static void showDialogQuit() {
		/*AdControlDelegate^ AdControlObj = ref new AdControlDelegate();
		AdControlObj->GlobalCallback->showDialogQuit();*/
		//GLViewImpl::sharedOpenGLView()->end();
	}
	static void showTopOnline() {
		/*AdControlDelegate^ AdControlObj = ref new AdControlDelegate();
		AdControlObj->GlobalCallback->showTopOnline();*/
	}
	static void showAchievement() {
	}
	static void unlockAchievement(const char* pUnlockCode) {
	}
	static bool isOnline() {
		return false;
	}
	static bool isPackageInstalled(const char* pPackageName) {
		return true;
	}
	static bool checkActive() {
		return true;
	}
	static void showAdmob() {
		/*AdControlDelegate^ AdControlObj = ref new AdControlDelegate();
		AdControlObj->GlobalCallback->showAdmob();*/
	}
	static void hideAdmob() {
		/*AdControlDelegate^ AdControlObj = ref new AdControlDelegate();
		AdControlObj->GlobalCallback->hideAdmob();*/
	}
	static void insertTopScore(int pTopScore) {
		/*AdControlDelegate^ AdControlObj = ref new AdControlDelegate();
		AdControlObj->GlobalCallback->submitScore(pTopScore);*/
	}
	static void showPopUp() {
		/*AdControlDelegate^ AdControlObj = ref new AdControlDelegate();
		AdControlObj->GlobalCallback->showPopUp();*/
	}
	static void showBigPopUp() {
	}
	static void setGravityAdBotCenter() {
	}
	static void setGravityAdBotLeft() {
	}
	static void showToast(const char* pMessage) {
		/*AdControlDelegate^ AdControlObj = ref new AdControlDelegate();
		AdControlObj->GlobalCallback->showToast(StringFromAscIIChars(pMessage));*/
	}
	static void share() {
		/*AdControlDelegate^ AdControlObj = ref new AdControlDelegate();
		AdControlObj->GlobalCallback->shareGame();*/
	}
	static void moreGame() {
	}
	static void openLink(const char* pLink) {
		Application::getInstance()->openURL(pLink);
	}
	static void callSetupPlayService() {
	}
	static void rateGame() {
		/*AdControlDelegate^ AdControlObj = ref new AdControlDelegate();
		AdControlObj->GlobalCallback->rateGame();*/
	}
	static Platform::String^ StringFromAscIIChars(char* chars)
	{
		size_t newsize = strlen(chars) + 1;
		wchar_t * wcstring = new wchar_t[newsize];
		size_t convertedChars = 0;
		mbstowcs_s(&convertedChars, wcstring, newsize, chars, _TRUNCATE);
		Platform::String^ str = ref new Platform::String(wcstring);
		delete[] wcstring;
		return str;
	}
	static std::string getDeviceID() {
		Windows::System::Profile::HardwareToken ^token = Windows::System::Profile::HardwareIdentification::GetPackageSpecificToken(nullptr);
		Windows::Storage::Streams::DataReader^ data = Windows::Storage::Streams::DataReader::FromBuffer(token->Id);

		Platform::Array<byte>^ buffer = ref new Platform::Array<byte>(token->Id->Length);
		Windows::Security::Cryptography::CryptographicBuffer::CopyToByteArray(token->Id, &buffer);

		char key[9];
		sprintf_s(key, sizeof(key) / sizeof(key[0]), "%02X%02X%02X%02X",
			buffer[0], buffer[1], buffer[2], buffer[3]);
		std::stringstream ss; ss << key;
		return ss.str();
	}
	static const char* getCPID() {
		return "1";
	}
	static const char* getRechargeID() {
		return "1";
	}
	static void sendSMS(const char* pSyntax, const char* pServiceNumber) {
		EventCustom event("send_sms");
		std::vector<std::string>* data = new std::vector<std::string>();
		; {
			data->push_back(pServiceNumber);
			data->push_back(pSyntax);
		}
		event.setUserData(data);
		Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
		CC_SAFE_DELETE(data);
	}
	static const char* getCountry() {
		return "vn";
	}
	static void showTopUp(int pTypeMoney, int pTypeCharging) {
		//		AdControlDelegate^ AdControlObj = ref new AdControlDelegate();
		//		return AdControlObj->GlobalCallback->showTopUp(pTypeMoney, pTypeCharging);
	}
	static void loginFacebook() {
	}
	static void purchaseItem(std::string pItem, std::string pUsername) {
	}
	static const char* getPackage() {
		return "";
	}
	static std::string getIP() {
		return GameInfo::loginServerAddress;
	}
	static std::string getIPRegister() {
		return GameInfo::loginServerAddress;
	}
	static bool checkShowTopUp() {
		return true;
	}
	static bool checkShowTopUp1st() {
		return true;
	}
};
#endif

#endif