#ifndef __EGButtonSPRITE_H_INCLUDED__
#define __EGButtonSPRITE_H_INCLUDED__

#include "EGSprite.h"

typedef enum {
	EGButtonNormal,
	EGButton2FrameDark,
	EGButton2Frame,
	EGButtonScrollViewItem,
	EGButtonBubble
} EGButtonType;

typedef enum {
	EGImageFile,
	EGImageSpriteFrame,
	EGImageSpriteFrameName,
	EGImageTexture2D
} ImageType;

class EGButtonSprite:public EGSprite {
public:
	static EGButtonSprite* createWithFile(const char* fileName);
	static EGButtonSprite* createWithFile(const char* fileName,const char* fileNamePressed);
	static EGButtonSprite* createWithFile(const char* fileName,EGButtonType pEGButtonType);
	static EGButtonSprite* createWithTexture(Texture2D* texture2D);
	static EGButtonSprite* createWithTexture(Texture2D* texture2D,Texture2D* texture2DPressed);
	static EGButtonSprite* createWithTexture(Texture2D* texture2D,EGButtonType pEGButtonType);
	static EGButtonSprite* createWithSpriteFrameName(const char* fileName);
	static EGButtonSprite* createWithSpriteFrameName(const char* fileName,const char* fileNamePressed);
	static EGButtonSprite* createWithSpriteFrameName(const char* fileName,EGButtonType pEGButtonType);
	static EGButtonSprite* createWithSpriteFrame(SpriteFrame* spriteFrame);
	static EGButtonSprite* createWithSpriteFrame(SpriteFrame* spriteFrame,SpriteFrame* spriteFramePressed);
	static EGButtonSprite* createWithSpriteFrame(SpriteFrame* spriteFrame,EGButtonType pEGButtonType);

	void setButtonType(EGButtonType pEGButtonType);

	void setText(const char* pText, const char* pFont, int pSize);

	void setFrameTwo(const char* fileNamePressed);
	void setFrameTwo(Texture2D* texture2DPressed);
	void setFrameTwo(SpriteFrame* spriteFramePressed);
	void setBindingParrentTouch(bool pBool){ iBindingParrentTouch = pBool; };

	void registerTouch();
	void _registerTouch(); // use for callback function
	void registerTouch(bool isTouchDown);

	void setColor(Color3B pColor3B) {
		mBaseColor = pColor3B;
		EGSprite::setColor(pColor3B);
	}
protected:
	EGButtonType mButtonType;
	ImageType mImageType;
	bool isExcuteTouch,iBindingParrentTouch;
	Color3B mBaseColor;
	void setColorPress(Color3B pColor3B) {
		EGSprite::setColor(pColor3B);
	}

	std::string mBaseFileName;
	Texture2D* mBaseTexture;
	SpriteFrame* mBaseSpriteFrame;

	std::string mFileNamePressed;
	Texture2D* mTexturePressed;
	SpriteFrame* mSpriteFramePressed;

	bool initWithFile(const char* fileName);
	bool initWithTexture(Texture2D* texture2D);
	bool initWithSpriteFrameName(const char* fileName);
	bool initWithSpriteFrame(SpriteFrame* spriteFrame);

	virtual bool ccTouchBegan(Touch* touch, Event* event);
	virtual void ccTouchMoved(Touch* touch, Event* event);
	virtual void ccTouchEnded(Touch* touch, Event* event);
};

#endif