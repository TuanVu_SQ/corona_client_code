#ifndef __EGGAMEPADLISTENER_H_INCLUDED__
#define __EGGAMEPADLISTENER_H_INCLUDED__

#include "cocos2d.h"

using namespace cocos2d;

class EGGamePadListener {
public:
	virtual void onControlMovement(float pRotation, float pDistance) {};
	virtual void onControlAction(Point pPointTouch) {};
};

#endif