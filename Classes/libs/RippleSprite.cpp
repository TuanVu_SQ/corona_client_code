#include "RippleSprite.h"

RippleSprite::RippleSprite()
	:m_texture(NULL),
	m_quadCountX(0),
	m_quadCountY(0),
	m_VerticesPrStrip(0),
	m_bufferSize(0),
	m_vertice(NULL),
	m_textureCoordinate(NULL),
	m_rippleCoordinate(NULL),
	m_edgeVertice(NULL),
	scaleRTT(0),
	mSize(cocos2d::Size::ZERO),
	runTime(0) {
	_timer = std::chrono::high_resolution_clock::now();
	srand(time(NULL));

	/*auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(RippleSprite::ccTouchBegan, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(RippleSprite::ccTouchMoved, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);*/
}

RippleSprite::~RippleSprite() {
	RIPPLE_DATA* runningRipple;
	free(m_vertice);
	free(m_textureCoordinate);
	free(m_rippleCoordinate);
	free(m_edgeVertice);
	for (int count = m_rippleList.size() - 1; count >= 0; count--) {
		runningRipple = m_rippleList.at(count);
		free(runningRipple);
	}
	m_texture->release();
}

RippleSprite* RippleSprite::create(std::string fileName) {
	auto rippleSprite = new RippleSprite();
	rippleSprite->initWithFile(fileName);
	rippleSprite->tesselate();
	//rippleSprite->autoRipple();
	rippleSprite->scheduleUpdate();
	rippleSprite->autorelease();
	return rippleSprite;
}

RippleSprite* RippleSprite::create(Node* node) {
	auto rippleSprite = new RippleSprite();
	rippleSprite->initWithScene(node);
	rippleSprite->tesselate();
	//rippleSprite->autoRipple();
	rippleSprite->scheduleUpdate();
	rippleSprite->autorelease();
	return rippleSprite;
}

bool RippleSprite::initWithFile(std::string fileName) {
	GLProgram* glProgram = ShaderCache::getInstance()->getGLProgram(GLProgram::SHADER_NAME_POSITION_TEXTURE);
	this->setGLProgram(glProgram);

	/*glProgram->initWithByteArrays(ccPositionTextureA8Color_vert, SHADER_HELPER_BLUR);
	glProgram->link();
	glProgram->updateUniforms();*/

	scaleRTT = 1.0f;
	//log("%s0", Director::getInstance()->getTextureCache()->getCachedTextureInfo().c_str());
	m_texture = createWithRenderTexture(fileName);
	m_texture->retain();
	m_vertice = NULL;
	m_textureCoordinate = NULL;
	m_quadCountX = RIPPLE_DEFAULT_QUAD_COUNT_X;
	m_quadCountY = RIPPLE_DEFAULT_QUAD_COUNT_Y;
	mSize = cocos2d::Size(m_texture->getContentSize().width / scaleRTT, m_texture->getContentSize().height / scaleRTT);

	return true;
}

bool RippleSprite::initWithScene(Node* node) {
	GLProgram* glProgram = ShaderCache::getInstance()->getGLProgram(GLProgram::SHADER_NAME_POSITION_TEXTURE);
	this->setGLProgram(glProgram);

	_parent = node;

	/*glProgram->initWithByteArrays(ccPositionTextureA8Color_vert, SHADER_HELPER_BLUR);
	glProgram->link();
	glProgram->updateUniforms();*/

	scaleRTT = 1.0f;
	m_texture = createWithRenderTexture(_parent);
	m_texture->retain();
	m_vertice = NULL;
	m_textureCoordinate = NULL;
	m_quadCountX = RIPPLE_DEFAULT_QUAD_COUNT_X;
	m_quadCountY = RIPPLE_DEFAULT_QUAD_COUNT_Y;
	mSize = cocos2d::Size(m_texture->getContentSize().width / scaleRTT, m_texture->getContentSize().height / scaleRTT);

	return true;
}

Texture2D* RippleSprite::createWithRenderTexture(std::string fileName) {
	auto texture = Director::getInstance()->getTextureCache()->addImage(fileName);
	//auto texture = SpriteFrameCache::getInstance()->getSpriteFrameByName(fileName);
	return texture;
}

Texture2D* RippleSprite::createWithRenderTexture(Node* parent) {

	cocos2d::Size mPhoneSize = Director::getInstance()->getVisibleSize();
	auto renderTexture = RenderTexture::create(mPhoneSize.width, mPhoneSize.height);
	renderTexture->begin();
	parent->visit();
	for (Node* node : parent->getChildren()) {
		node->visit();
	}
	renderTexture->end();
	renderTexture->getSprite()->getTexture()->setAntiAliasTexParameters();
	Director::getInstance()->getRenderer()->render();
	return renderTexture->getSprite()->getTexture();
}

void RippleSprite::setPosition(Vec2 pos) {
	Sprite::setPosition(pos - Vec2(m_texture->getContentSize() / 2) + Vec2(m_texture->getContentSize().width * this->getAnchorPoint().x,
		m_texture->getContentSize().height * this->getAnchorPoint().y));
}

Vec2 RippleSprite::getPosition() {
	return Vec2(Sprite::getPosition() - Vec2(m_texture->getContentSize() / 2) + Vec2(m_texture->getContentSize().width * this->getAnchorPoint().x,
		m_texture->getContentSize().height * this->getAnchorPoint().y));
}

cocos2d::Size RippleSprite::getContentSize() {
	return m_texture->getContentSize();
}

void RippleSprite::tesselate() {
	int vertexPos = 0;
	Vec2 normalized;
	free(m_vertice);
	free(m_textureCoordinate);
	free(m_rippleCoordinate);
	free(m_edgeVertice);
	m_VerticesPrStrip = 2 * (m_quadCountX + 1);
	m_bufferSize = m_VerticesPrStrip * m_quadCountY;
	m_vertice = (Vec2*)malloc(m_bufferSize * sizeof(Vec2));
	m_textureCoordinate = (Vec2*)malloc(m_bufferSize * sizeof(Vec2));
	m_rippleCoordinate = (Vec2*)malloc(m_bufferSize * sizeof(Vec2));
	m_edgeVertice = (bool*)malloc(m_bufferSize * sizeof(bool));
	vertexPos = 0;

	for (int y = 0; y < m_quadCountY; y++) {
		for (int x = 0; x < (m_quadCountX + 1); x++) {
			for (int yy = 0; yy < 2; yy++) {
				normalized.x = (float)x / (float)m_quadCountX;
				normalized.y = (float)(y + yy) / (float)m_quadCountY;
				m_vertice[vertexPos] = Vec2(normalized.x * m_texture->getContentSize().width / scaleRTT, normalized.y * m_texture->getContentSize().height / scaleRTT);
				m_textureCoordinate[vertexPos] = Vec2(normalized.x * m_texture->getMaxS(), m_texture->getMaxT() - normalized.y * m_texture->getMaxT());
				m_edgeVertice[vertexPos] = ((x == 0) || (x == m_quadCountX) || ((y == 0) && (yy == 0)) || ((y == (m_quadCountY - 1)) && (yy > 0)));
				vertexPos++;

			}
		}
	}
}

void RippleSprite::addRipple(Vec2 pos, RIPPLE_TYPE type, float strength)
{
	RIPPLE_DATA* newRipple = (RIPPLE_DATA*)malloc(sizeof(RIPPLE_DATA));
	newRipple->parent = true;
	for (int count = 0; count < 4; count++)
		newRipple->childCreated[count] = false;
	newRipple->rippleType = type;
	newRipple->center = pos;
	newRipple->centerCoordinate = Vec2(pos.x / m_texture->getContentSize().width * m_texture->getMaxS() / scaleRTT, m_texture->getMaxT()
		- (pos.y / m_texture->getContentSize().height * m_texture->getMaxT() / scaleRTT));
	newRipple->radius = RIPPLE_DEFAULT_RADIUS *strength / 2.0f;
	newRipple->strength = strength;
	newRipple->runtime = 0;
	newRipple->currentRadius = 0;
	newRipple->rippleCycle = RIPPLE_DEFAULT_RIPPLE_CYCLE;
	newRipple->lifespan = RIPPLE_DEFAULT_LIFESPAN;
	m_rippleList.push_back(newRipple);
}

void RippleSprite::addRippleChild(RIPPLE_DATA* parent, RIPPLE_CHILD type)
{
	Vec2 pos;
	RIPPLE_DATA* newRipple = (RIPPLE_DATA*)malloc(sizeof(RIPPLE_DATA));
	memcpy(newRipple, parent, sizeof(RIPPLE_DATA));
	newRipple->parent = false;

	switch (type) {
	case RIPPLE_CHILD_LEFT:
		pos = Vec2(-parent->center.x, parent->center.y);
		break;
	case RIPPLE_CHILD_TOP:
		pos = Vec2(parent->center.x, mSize.height + (mSize.height - parent->center.y));
		break;
	case RIPPLE_CHILD_RIGHT:
		pos = Vec2(mSize.width + (mSize.width - parent->center.x), parent->center.y);
		break;
	case RIPPLE_CHILD_BOTTOM:
	default:
		pos = Vec2(parent->center.x, -parent->center.y);
		break;
	}

	newRipple->center = pos;
	newRipple->centerCoordinate = Vec2(pos.x / m_texture->getContentSize().width * m_texture->getMaxS(),
		m_texture->getMaxT() - (pos.y / m_texture->getContentSize().height * m_texture->getMaxT()));
	newRipple->strength *= RIPPLE_CHILD_MODIFIER;
	parent->childCreated[type] = true;
	m_rippleList.push_back(newRipple);
}

void RippleSprite::update(float dt)
{
	//delete m_texture;
	//m_texture = createWithRenderTexture(_parent);
	//m_texture->retain();

	RIPPLE_DATA* ripple;
	Vec2 pos;
	float distance, correction;
	if (m_rippleList.size() == 0) return;
	memcpy(m_rippleCoordinate, m_textureCoordinate, m_bufferSize * sizeof(Vec2));

	for (int _count = (m_rippleList.size() - 1); _count >= 0; _count--) {
		ripple = m_rippleList[_count];
		for (int count = 0; count < m_bufferSize; count++) {
			if (m_edgeVertice[count] == false) {
				distance = ripple->center.distance(m_vertice[count]);
				if (distance <= ripple->currentRadius) {
					pos = m_rippleCoordinate[count];
					switch (ripple->rippleType) {

					case RIPPLE_TYPE_RUBBER:
						correction = sinf(2 * M_PI * ripple->runtime / ripple->rippleCycle);
						break;
					case RIPPLE_TYPE_GEL:
						correction = sinf(2 * M_PI * (ripple->currentRadius - distance) / ripple->radius * ripple->lifespan / ripple->rippleCycle);
						break;
					case RIPPLE_TYPE_WATER:
					default:
						correction = (ripple->radius * ripple->rippleCycle / ripple->lifespan) / (ripple->currentRadius - distance);
						if (correction > 1.0f) correction = 1.0f;
						correction *= correction;
						correction *= sinf(2 * M_PI * (ripple->currentRadius - distance) / ripple->radius * ripple->lifespan / ripple->rippleCycle);
						break;
					}
					
					correction *= 1 - (distance / ripple->currentRadius);
					correction *= 1 - (ripple->runtime / ripple->lifespan);
					correction *= RIPPLE_BASE_GAIN;
					correction *= ripple->strength;
					correction /= ripple->centerCoordinate.distance(pos);
					pos = pos + (pos - ripple->centerCoordinate) * correction;
					pos = pos.getClampPoint(cocos2d::Vec2::ZERO, Vec2(m_texture->getMaxS(), m_texture->getMaxT()));
					m_rippleCoordinate[count] = pos;
				}
			}
		}
		ripple->currentRadius = ripple->radius * ripple->runtime / ripple->lifespan;
		ripple->runtime += dt;
		if (ripple->runtime >= ripple->lifespan) {
			free(ripple);
			m_rippleList.erase(m_rippleList.begin() + _count);
		}
		else {
			if (ripple->parent) {
				if (!ripple->childCreated[RIPPLE_CHILD_LEFT] && ripple->currentRadius > ripple->center.x) {
					addRippleChild(ripple, RIPPLE_CHILD_LEFT);
				}

				if (!ripple->childCreated[RIPPLE_CHILD_TOP] && ripple->currentRadius > mSize.width - ripple->center.y) {
					addRippleChild(ripple, RIPPLE_CHILD_TOP);
				}

				if (!ripple->childCreated[RIPPLE_CHILD_RIGHT] && ripple->currentRadius > mSize.height - ripple->center.x) {
					addRippleChild(ripple, RIPPLE_CHILD_RIGHT);
				}

				if (!ripple->childCreated[RIPPLE_CHILD_BOTTOM] && ripple->currentRadius > ripple->center.y) {
					addRippleChild(ripple, RIPPLE_CHILD_BOTTOM);
				}
			}
		}
	}
}

void RippleSprite::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
	Node::draw(renderer, transform, flags);

	if (!this->isVisible()) return;
	_customCommand.init(_globalZOrder);
	_customCommand.func = CC_CALLBACK_0(RippleSprite::onDraw, this, transform, flags);
	renderer->addCommand(&_customCommand);
}

void RippleSprite::onDraw(const Mat4 &transform, uint32_t flags) {
	CC_NODE_DRAW_SETUP();
	GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POSITION | GL::VERTEX_ATTRIB_FLAG_TEX_COORD);
	GL::bindTexture2D(m_texture->getName());
	glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, 0, (void*)m_vertice);
	glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, 0, (m_rippleList.size() == 0) ? m_textureCoordinate : m_rippleCoordinate);
	for (int strip = 0; strip < m_quadCountY; strip++) {
		glDrawArrays(GL_TRIANGLE_STRIP, strip * m_VerticesPrStrip, m_VerticesPrStrip);
	}
}

bool RippleSprite::isPointInsideSprite(cocos2d::Vec2 pos)
{
	float maxX = m_texture->getContentSize().width / scaleRTT;
	float maxY = m_texture->getContentSize().height / scaleRTT;
	if (pos.x < 0 || pos.y < 0 ||
		pos.x > maxX || pos.y > maxY) {
		return false;
	}
	else {
		return true;
	}
}

void RippleSprite::autoRipple() {
	this->addRipple(Vec2((rand() % (int)(m_texture->getContentSize().width * 10 + 1)) / 10.0f,
		(rand() % (int)(m_texture->getContentSize().width * 10 + 1)) / 10.0f), RIPPLE_TYPE_WATER, (rand() % 10 + 11) / 10.0f);
	this->runAction(Sequence::createWithTwoActions(DelayTime::create((rand() % 10 + 1) / 10.0f), CallFunc::create(CC_CALLBACK_0(RippleSprite::autoRipple, this))));
}

bool RippleSprite::isTouchInsideSprite(cocos2d::Touch *touch) {
	return this->isPointInsideSprite(this->convertToNodeSpace(Director::getInstance()->convertToGL(touch->getLocationInView())));
}

bool RippleSprite::ccTouchBegan(Touch *pTouch, Event *pEvent) {
	if (!this->isTouchInsideSprite(pTouch)) return false;
	this->ccTouchMoved(pTouch, pEvent);
	return true;
}

void RippleSprite::ccTouchMoved(Touch *touch, Event *pEvent) {
	Vec2 pos = this->convertToNodeSpace(Director::getInstance()->convertToGL(touch->getLocationInView()));
	std::chrono::time_point<std::chrono::high_resolution_clock> _now = std::chrono::high_resolution_clock::now();
	std::chrono::duration<float> elapsed_seconds = _now - _timer;
	if (elapsed_seconds.count() > 0.1f) {
		this->addRipple(pos, RIPPLE_TYPE_WATER, 2.0f);
		_timer = std::chrono::high_resolution_clock::now();
	}
}

void RippleSprite::ccTouchEnded(Touch *pTouch, Event *pEvent) {

}