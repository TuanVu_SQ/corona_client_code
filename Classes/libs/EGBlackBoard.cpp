#include "EGBlackBoard.h"

EGBlackBoard* EGBlackBoard::create(float pOpacity, int pWidth, int pHeight) {
	EGBlackBoard *pEGBlackBoard = new EGBlackBoard();
	pEGBlackBoard->init(pOpacity, pWidth, pHeight);
	pEGBlackBoard->autorelease();

	return pEGBlackBoard;
}

void EGBlackBoard::onDraw(const Mat4 &transform, bool transformUpdated) {
	float pX = this->getPositionX();
	float pY = this->getPositionY();

	float pWidth = this->getContentSize().width;
	float pHeight = this->getContentSize().height;

	float pWidthScreen = mWidth;
	float pHeightScreen = mHeight;

	glLineWidth(1);
	ccDrawSolidRect(Vec2(-pX, -pY), Vec2(pWidthScreen, pHeightScreen), Color4F(0, 0, 0, mOpacity));

	Node::draw();
}

void EGBlackBoard::init(float pOpacity, int pWidth, int pHeight) {
	mWidth = pWidth;
	mHeight = pHeight;
	mOpacity = pOpacity;
	isRegisterTouch = false;
}

void EGBlackBoard::setOpacity(float pOpacity) {
	mOpacity = pOpacity;
}

void EGBlackBoard::registerTouch() {
	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(EGBlackBoard::ccTouchBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

bool EGBlackBoard::ccTouchBegan(Touch* touch, Event* event) {
	return isVisible();
}