#ifndef __EGSLIDER_H_INCLUDED__
#define __EGSLIDER_H_INCLUDED__

#include "cocos2d.h"
#include "EGButtonSprite.h"
#include "EGSprite.h"

using namespace cocos2d;

class EGSliderConfig:public EGSprite {
public:
	static EGSliderConfig* create(std::string fileSliderBar, std::string fileSlider, std::string fileSliderController);

	void setDoWhenDone(std::function<void()> pSelFunction);
	void setDoWhenControlling(std::function<void()> pSelFunction);
	float getPercent();
	void setPercent(float pPercent);
private:
	EGButtonSprite* mController;
	ProgressTimer* mSlider;
	CallFunc* mDoneFunc, *mActionMovingFunc;
	bool isPushingController;

	bool init(std::string fileSliderBar, std::string fileSlider, std::string fileSliderController);

	void onPushController();
	void onUpController();

	virtual bool ccTouchBegan(Touch* touch, Event* event);
	virtual void ccTouchMoved(Touch* touch, Event* event);
	virtual void ccTouchEnded(Touch* touch, Event* event);
};

#endif