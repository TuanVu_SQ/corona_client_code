#ifndef __EGEFFECTACTIONMANAGER_H_INCLUDED__
#define __EGEFFECTACTIONMANAGER_H_INCLUDED__

#include "cocos2d.h"

using namespace cocos2d;

class EGEffectActionManager {
public:
	static ActionInterval* getEffectScaleBuble(bool isAppear,float times);
	static ActionInterval* getActionMoveToWithDelay(float timesDelay,float times,Point Point);
	static ActionInterval* getActionScaleToWithDelay(float timesDelay,float times,float pScaleX,float pScaleY);
	static ActionInterval* getActionRotateToWithDelay(float timesDelay,float times,float pRotateX,float pRotateY);
};

#endif