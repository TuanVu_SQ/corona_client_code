#include "EGSprite.h"
#include "EGSupport.h"

EGSprite* EGSprite::createWithFile(const char* fileName) {
	EGSprite* pSprite = new EGSprite();
	pSprite->initWithFile(fileName);
	pSprite->autorelease();

	return pSprite;
}

EGSprite* EGSprite::createWithTexture(Texture2D* texture2D) {
	EGSprite* pSprite = new EGSprite();
	pSprite->initWithTexture(texture2D);
	pSprite->autorelease();

	return pSprite;
}

EGSprite* EGSprite::createWithSpriteFrameName(const char* fileName) {
	EGSprite* pSprite = new EGSprite();
	pSprite->initWithSpriteFrameName(fileName);
	pSprite->autorelease();

	return pSprite;
}

EGSprite* EGSprite::createWithSpriteFrame(SpriteFrame* spriteFrame) {
	EGSprite* pSprite = new EGSprite();
	pSprite->initWithSpriteFrame(spriteFrame);
	pSprite->autorelease();
	return pSprite;
}

bool EGSprite::initWithFile(std::string fileName) {
	Sprite::initWithFile(fileName.c_str());
	mFileName = fileName;
	mType = 0;
	prepare();

	return true;
}

bool EGSprite::initWithTexture(Texture2D *texture2D) {
	Sprite::initWithTexture(texture2D);
	mFileName = "";
	mType = 2;
	prepare();

	return true;
}
void EGSprite::setBindingRect(bool pBool){
	iBindingRect = pBool;
}
bool EGSprite::initWithSpriteFrameName(std::string fileName) {
	Sprite::initWithSpriteFrameName(fileName.c_str());
	mFileName = fileName;
	mType = 1;
	prepare();

	return true;
}

bool EGSprite::initWithSpriteFrame(SpriteFrame* spriteFrame) {
	Sprite::initWithSpriteFrame(spriteFrame);
	mFileName = "";
	mType = 3;
	prepare();

	return true;
}

// NOT TESTED
void EGSprite::setTags(int pIndexTag, float pValues) {
	if (pIndexTag >= mLstTag.size()) {
		for (int i = mLstTag.size(); i < pIndexTag; i++) {
			mLstTag.pushBack(__Float::create(-1));
		}
		mLstTag.pushBack(__Float::create(pValues));
	}
	else {
		mLstTag.insert(pIndexTag, __Float::create(pValues));
	}
}

// NOT TESTED
float EGSprite::getTags(int pIndexTag) {
	if (pIndexTag >= mLstTag.size()) {
		return -1;
	}
	return mLstTag.at(pIndexTag)->getValue();
}

void EGSprite::prepare() {
	isTouchRegistered = false;
	/*mTouchFunction = NULL;
	mUpFunction = NULL;*/
	mRect = rect();
	iBindingRect = true;
	isAutomaticRotate = false;
	isScaleAllChild = false;
	isAlwayReturnFalse = false;
}

std::string EGSprite::getFileName() {
	return mFileName;
}

void EGSprite::setTexture(const char* fileName) {
	std::string newFileName = fileName;

	mFileName = fileName;
	if (mType == 0) {
		Sprite::setTexture(fileName);
	}
	else if (mType == 1) {
		Sprite::setSpriteFrame(SpriteFrameCache::sharedSpriteFrameCache()->getInstance()->spriteFrameByName(fileName));
	}

	Sprite::updateBlendFunc();
}

void EGSprite::setTexture(Texture2D *pTexture) {
	Sprite::setTexture(pTexture);
}

void EGSprite::setTextureByRef(Ref* pRef) {
	Texture2D* pTexture2D = dynamic_cast<Texture2D*>(pRef);
	__String* pStrTexture = dynamic_cast<__String*>(pRef);
	if (pTexture2D) {
		setTexture(pTexture2D);
	}
	else if (pStrTexture) {
		setTexture(pStrTexture->getCString());
	}
}

void EGSprite::registerTouch() {
	registerTouch(true);
}

void EGSprite::registerTouch(bool isTouchDown) {
	if (!isTouchRegistered) {
		isTouchRegistered = true;

		auto listener = EventListenerTouchOneByOne::create();
		listener->onTouchBegan = CC_CALLBACK_2(EGSprite::ccTouchBegan, this);
		listener->setSwallowTouches(isTouchDown);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	}
}

void EGSprite::unregisterTouch() {
	if(isTouchRegistered) {
		isTouchRegistered = false;

		_eventDispatcher->removeEventListenersForTarget(this);
	}
}

bool EGSprite::isRegisteredTouch() {
	return isTouchRegistered;
}

void EGSprite::setOnTouch(std::function<void()> pTouchFunction) {
	mTouchFunction = pTouchFunction;
}

void EGSprite::setOnUp(std::function<void()> pUpFunction) {
	mUpFunction = pUpFunction;
}

Rect EGSprite::rect() {
    Size s = getContentSize();
	float pScaleX = this->getScaleX();
	float pScaleY = this->getScaleY();
	return Rect(-s.width*pScaleX*this->getAnchorPoint().x, -s.height*pScaleY*this->getAnchorPoint().y,
		s.width*pScaleX, s.height*pScaleY);
}

bool EGSprite::containsTouchLocation(Touch* touch) {
    return rect().containsPoint(convertTouchToNodeSpaceAR(touch));
}

bool EGSprite::ccTouchBegan(Touch* touch, Event* event) {
	if((containsTouchLocation(touch)||!iBindingRect) && isVisible() && this->getParent()->isVisible()) {
		if(this->getParent()->getParent() != NULL && this->getParent()->getParent()->isVisible()) {
			performClick();
			if (isAlwayReturnFalse){
				return false;
			}
			return true;
		}
	}
	return false;
}

void EGSprite::performClick() {
	CallFunc* pCallFunction;
	if(mTouchFunction) {
		pCallFunction = CallFunc::create(mTouchFunction);
		pCallFunction->execute();
	}
}

void EGSprite::performUp() {
	CallFunc* pCallFunction;
	if (mUpFunction) {
		pCallFunction = CallFunc::create(mUpFunction);
		pCallFunction->execute();
	}
}

void EGSprite::setVisibleByRef(Ref *pRef) {
	__Bool *p__BoolRef = dynamic_cast<__Bool*>(pRef);
	Sprite::setVisible(p__BoolRef->getValue());
}

void EGSprite::setAutomaticRotate(bool isAutomatic) {
	isAutomaticRotate = isAutomatic;
}

void EGSprite::setPosition(const Point& pos) {
	if (isAutomaticRotate) {
		float pRotate = EGSupport::getDirectionByRotate(this->getPositionX(), this->getPositionY(), pos.x, pos.y);
		this->setRotation(pRotate);
	}
	Sprite::setPosition(pos);
}

void EGSprite::setPositionByRef(Ref* pRef) {
	EGPoint* pPointRef = dynamic_cast<EGPoint*>(pRef);
	Sprite::setPosition(Vec2(pPointRef->getX(), pPointRef->getY()));
}

float EGSprite::getPositionX1() {
	return this->getPositionX() - this->getContentSize().width*this->getAnchorPoint().x;
}

float EGSprite::getPositionX2() {
	return this->getPositionX() + this->getContentSize().width*(1-this->getAnchorPoint().x);
}

float EGSprite::getPositionY1() {
	return this->getPositionY() - this->getContentSize().height*this->getAnchorPoint().y;
}

float EGSprite::getPositionY2() {
	return this->getPositionY() + this->getContentSize().height*(1-this->getAnchorPoint().y);
}

Point EGSprite::getBasePosition() {
	Point pBasePosition = this->getPosition();
	Node* pParent = this->getParent();
	pBasePosition.x += pParent->getPositionX() - pParent->getContentSize().width*pParent->getAnchorPoint().x;
	pBasePosition.y += pParent->getPositionY() - pParent->getContentSize().height*pParent->getAnchorPoint().y;
	do {
		pParent = pParent->getParent();
		if (pParent) {
			pBasePosition.x += pParent->getPositionX() - pParent->getContentSize().width*pParent->getAnchorPoint().x;
			pBasePosition.y += pParent->getPositionY() - pParent->getContentSize().height*pParent->getAnchorPoint().y;
		}
	} while (pParent != NULL);
	return pBasePosition;
}

void EGSprite::setFlipXByRef(Ref* pRef) {
	__Bool* p__Bool = dynamic_cast<__Bool*>(pRef);
	this->setFlippedX (p__Bool->getValue());
}

void EGSprite::setFlipYByRef(Ref* pRef) {
	__Bool* p__Bool = dynamic_cast<__Bool*>(pRef);
	this->setFlippedY(p__Bool->getValue());
}

void EGSprite::setScale(float pScale) {
	Sprite::setScale(pScale);
	if (isScaleAllChild) {
		this->getChildren();
		Vector<Node*> pVector = getChildren();
		for (int i = pVector.size() - 1; i >= 0; i--) {
			Sprite* pSprite = dynamic_cast<Sprite*>(pVector.at(i));
			if (pSprite) {
				pSprite->setScale(pScale);
			}
		}
	}
}

void EGSprite::setScaleX(float pScale) {
	Sprite::setScaleX(pScale);
	if (isScaleAllChild) {
		Vector<Node*> pVector = getChildren();
		for (int i = pVector.size() - 1; i >= 0; i--) {
			Sprite* pSprite = dynamic_cast<Sprite*>(pVector.at(i));
			if (pSprite) {
				pSprite->setScaleX(pScale);
			}
		}
	}
}

void EGSprite::setScaleY(float pScale) {
	Sprite::setScaleY(pScale);
	if (isScaleAllChild) {
		Vector<Node*> pVector = getChildren();
		for (int i = pVector.size() - 1; i >= 0; i--) {
			Sprite* pSprite = dynamic_cast<Sprite*>(pVector.at(i));
			if (pSprite) {
				pSprite->setScaleY(pScale);
			}
		}
	}
}

// Must same parrent or 2 parrent same position
bool EGSprite::colliWith(EGSprite* pEGSprite) {
	Size selfSize = getRect().size;
	Size friendSize = pEGSprite->getRect().size;

	Point selfPos = getPosition();
	Point friendPos = pEGSprite->getPosition();

	bool isColliX = false,isColliY = false;

	if(selfPos.x + selfSize.width/2 > friendPos.x - friendSize.width/2 &&
		selfPos.x + selfSize.width/2 < friendPos.x + friendSize.width/2) {
		isColliX = true;
	} else if (selfPos.x - selfSize.width/2 > friendPos.x - friendSize.width/2 &&
		selfPos.x - selfSize.width/2 < friendPos.x + friendSize.width/2) {
		isColliX = true;
	} 
	if (selfPos.y + selfSize.height/2 > friendPos.y - friendSize.height/2 &&
		selfPos.y + selfSize.height/2 < friendPos.y + friendSize.height/2) {
		isColliY = true;
	} else if (selfPos.y - selfSize.height/2 > friendPos.y - friendSize.height/2 &&
		selfPos.y - selfSize.height/2 < friendPos.y + friendSize.height/2) {
		isColliY = true;
	}
	return isColliX&&isColliY;
}

Rect EGSprite::getRect() {
	return mRect;
}

void EGSprite::setRect(Rect pRect) {
	mRect = pRect;
}

void EGSprite::performRelease() {
	this->removeAllChildrenWithCleanup(true);
	this->removeFromParentAndCleanup(true);
}