#ifndef __EGPOINT_H_INCLUDED__
#define __EGPOINT_H_INCLUDED__

#include "cocos2d.h"

using namespace cocos2d;

class EGPoint:public Sprite {
public:
	static EGPoint* create(float pX,float pY);
	static EGPoint* create(Point Point);

	float getX() {return mX;}
	float getY() {return mY;}

	Point getVec2() {
		return Vec2(mX, mY);
	}
private:
	float mX,mY;

	bool init(float pX,float pY);
};

#endif