#ifndef __EGBLACKBOARD_H_INCLUDED__
#define __EGBLACKBOARD_H_INCLUDED__

#include "cocos2d.h"

using namespace cocos2d;

class EGBlackBoard :public Node {
public:
	static EGBlackBoard* create(float pOpacity, int pWidth, int pHeight);

	void setOpacity(float pOpacity);
	float getOpacity() {
		return mOpacity;
	}
	void registerTouch();
private:
	float mOpacity;
	float mWidth, mHeight;
	bool isRegisterTouch;

	void onDraw(const Mat4 &transform, bool transformUpdated);
	void init(float pOpacity, int pWidth, int pHeight);

	virtual bool ccTouchBegan(Touch* touch, Event* event);
};

#endif