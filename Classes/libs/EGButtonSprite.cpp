#include "EGButtonSprite.h"
#include "EGSupport.h"

EGButtonSprite* EGButtonSprite::createWithFile(const char* fileName) {
	EGButtonSprite* pButtonSprite = new EGButtonSprite();
	pButtonSprite->initWithFile(fileName);
	pButtonSprite->autorelease();

	return pButtonSprite;
}

EGButtonSprite* EGButtonSprite::createWithFile(const char* fileName, const char* fileNamePressed) {
	EGButtonSprite* pButtonSprite = createWithFile(fileName);
	pButtonSprite->setButtonType(EGButton2Frame);
	pButtonSprite->setFrameTwo(fileNamePressed);

	return pButtonSprite;
}

EGButtonSprite* EGButtonSprite::createWithFile(const char* fileName, EGButtonType pEGButtonType) {
	EGButtonSprite* pButtonSprite = createWithFile(fileName);
	pButtonSprite->setButtonType(pEGButtonType);

	return pButtonSprite;
}

EGButtonSprite* EGButtonSprite::createWithTexture(Texture2D* texture2D) {
	EGButtonSprite* pButtonSprite = new EGButtonSprite();
	pButtonSprite->initWithTexture(texture2D);
	pButtonSprite->autorelease();

	return pButtonSprite;
}

EGButtonSprite* EGButtonSprite::createWithTexture(Texture2D* texture2D,Texture2D* texture2DPressed) {
	EGButtonSprite* pButtonSprite = createWithTexture(texture2D);
	pButtonSprite->setButtonType(EGButton2Frame);
	pButtonSprite->setFrameTwo(texture2DPressed);

	return pButtonSprite;
}

EGButtonSprite* EGButtonSprite::createWithTexture(Texture2D* texture2D,EGButtonType pEGButtonType) {
	EGButtonSprite* pButtonSprite = createWithTexture(texture2D);
	pButtonSprite->setButtonType(pEGButtonType);

	return pButtonSprite;
}

EGButtonSprite* EGButtonSprite::createWithSpriteFrameName(const char* fileName) {
	EGButtonSprite* pButtonSprite = new EGButtonSprite();
	pButtonSprite->initWithSpriteFrameName(fileName);
	pButtonSprite->autorelease();

	return pButtonSprite;
}

EGButtonSprite* EGButtonSprite::createWithSpriteFrameName(const char* fileName,const char* fileNamePressed) {
	EGButtonSprite* pButtonSprite = createWithSpriteFrameName(fileName);
	pButtonSprite->setButtonType(EGButton2Frame);
	pButtonSprite->setFrameTwo(fileNamePressed);

	return pButtonSprite;
}

EGButtonSprite* EGButtonSprite::createWithSpriteFrameName(const char* fileName,EGButtonType pEGButtonType) {
	EGButtonSprite* pButtonSprite = createWithSpriteFrameName(fileName);
	pButtonSprite->setButtonType(pEGButtonType);

	return pButtonSprite;
}

EGButtonSprite* EGButtonSprite::createWithSpriteFrame(SpriteFrame* spriteFrame) {
	EGButtonSprite* pButtonSprite = new EGButtonSprite();
	pButtonSprite->initWithSpriteFrame(spriteFrame);
	pButtonSprite->autorelease();

	return pButtonSprite;
}

EGButtonSprite* EGButtonSprite::createWithSpriteFrame(SpriteFrame* spriteFrame,EGButtonType pEGButtonType) {
	EGButtonSprite* pButtonSprite = createWithSpriteFrame(spriteFrame);
	pButtonSprite->setButtonType(pEGButtonType);

	return pButtonSprite;
}

EGButtonSprite* EGButtonSprite::createWithSpriteFrame(SpriteFrame* spriteFrame,SpriteFrame* spriteFramePressed) {
	EGButtonSprite* pButtonSprite = createWithSpriteFrame(spriteFrame);
	pButtonSprite->setButtonType(EGButton2Frame);
	pButtonSprite->setFrameTwo(spriteFramePressed);

	return pButtonSprite;
}

bool EGButtonSprite::initWithFile(const char* fileName) {
	mButtonType = EGButtonNormal;
	mImageType = EGImageFile;
	EGSprite::initWithFile(fileName);
	mBaseFileName = fileName;
	registerTouch(true);
	isExcuteTouch = true;
	mBaseColor = this->getColor();
	iBindingParrentTouch = true;
	return true;
}

bool EGButtonSprite::initWithTexture(Texture2D *texture2D) {
	mButtonType = EGButtonNormal;
	mImageType = EGImageTexture2D;
	EGSprite::initWithTexture(texture2D);
	mBaseTexture = texture2D;
	registerTouch(true);
	isExcuteTouch = true;
	mBaseColor = this->getColor();
	iBindingParrentTouch = true;
	return true;
}

bool EGButtonSprite::initWithSpriteFrameName(const char* fileName) {
	mButtonType = EGButtonNormal;
	mImageType = EGImageSpriteFrameName;
	EGSprite::initWithSpriteFrameName(fileName);
	mBaseFileName = fileName;
	registerTouch(true);
	isExcuteTouch = true;
	mBaseColor = this->getColor();
	iBindingParrentTouch = true;
	return true;
}

bool EGButtonSprite::initWithSpriteFrame(SpriteFrame* spriteFrame) {
	mButtonType = EGButtonNormal;
	mImageType = EGImageSpriteFrame;
	EGSprite::initWithSpriteFrame(spriteFrame);
	mBaseSpriteFrame = spriteFrame;
	registerTouch(true);
	isExcuteTouch = true;
	mBaseColor = this->getColor();
	iBindingParrentTouch = true;
	return true;
}

void EGButtonSprite::registerTouch() {
	if (!isTouchRegistered) {
		isTouchRegistered = true;

		auto listener = EventListenerTouchOneByOne::create();
		listener->onTouchBegan = CC_CALLBACK_2(EGButtonSprite::ccTouchBegan, this);
		listener->onTouchMoved = CC_CALLBACK_2(EGButtonSprite::ccTouchMoved, this);
		listener->onTouchEnded = CC_CALLBACK_2(EGButtonSprite::ccTouchEnded, this);
		listener->setSwallowTouches(true);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	}
}

void EGButtonSprite::_registerTouch() {
	registerTouch();
}

void EGButtonSprite::registerTouch(bool isTouchDown) {
	if (!isTouchRegistered) {
		isTouchRegistered = true;

		auto listener = EventListenerTouchOneByOne::create();
		listener->onTouchBegan = CC_CALLBACK_2(EGButtonSprite::ccTouchBegan, this);
		listener->onTouchMoved = CC_CALLBACK_2(EGButtonSprite::ccTouchMoved, this);
		listener->onTouchEnded = CC_CALLBACK_2(EGButtonSprite::ccTouchEnded, this);
		listener->setSwallowTouches(isTouchDown);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	}
}

void EGButtonSprite::setButtonType(EGButtonType pEGButtonType) {
	mButtonType = pEGButtonType;
	if (mButtonType == EGButtonScrollViewItem) {
		this->unregisterTouch();
		this->registerTouch(false);
	}
}

void EGButtonSprite::setFrameTwo(const char* fileName) {
	mFileNamePressed = fileName;
}

void EGButtonSprite::setFrameTwo(Texture2D* texture2D) {
	mTexturePressed = texture2D;
}

void EGButtonSprite::setFrameTwo(SpriteFrame* spriteFrame) {
	mSpriteFramePressed = spriteFrame;
}

void EGButtonSprite::setText(const char* pText, const char* pFont, int pSize) {
	this->removeAllChildrenWithCleanup(true);
	LabelTTF* pLabel = LabelTTF::create(pText, pFont, pSize);
	pLabel->setPosition(Vec2(this->getContentSize().width / 2, this->getContentSize().height / 2));
	this->addChild(pLabel);
}

bool EGButtonSprite::ccTouchBegan(Touch* touch, Event* event) {
	if(containsTouchLocation(touch) && isVisible() && this->getParent()->isVisible()) {
		if(this->getParent()->getParent() != NULL  ) {
			if (this->getParent()->getParent()->isVisible() || !iBindingParrentTouch ){
				isExcuteTouch = true;
				if (mButtonType == EGButtonNormal) {
					performClick();
				}
				else if (mButtonType == EGButton2FrameDark || mButtonType == EGButtonScrollViewItem) {
					this->setColorPress(Color3B(mBaseColor.r / 2, mBaseColor.g / 2, mBaseColor.b / 2));
				}
				else if (mButtonType == EGButton2Frame) {
					if (mImageType == EGImageFile) {
						this->setTexture(mFileNamePressed.c_str());
					}
					else if (mImageType == EGImageTexture2D) {
						Sprite::setTexture(mTexturePressed);
					}
					else if (mImageType == EGImageSpriteFrame) {
						this->setSpriteFrame(mSpriteFramePressed);
					}
					else if (mImageType == EGImageSpriteFrameName) {
						this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(mFileNamePressed.c_str()));
					}
				}
				else if (mButtonType == EGButtonBubble) {
					this->stopAllActions();
					this->runAction(ScaleTo::create(0.1f, 0.8f));
				}
				if (isAlwayReturnFalse&&mButtonType != EGButtonNormal){
					performClick();
					return false;
				}
				return true;
			}
		}
	}
	return false;
}

void EGButtonSprite::ccTouchMoved(Touch* touch, Event* event) {
	if (mButtonType == EGButtonScrollViewItem && isExcuteTouch) {
		Point startPoint = touch->getStartLocation();
		Point movePoint = touch->getLocation();
		float pDistanceMoved = EGSupport::countDistance(startPoint.x, startPoint.y, movePoint.x, movePoint.y);
		if (pDistanceMoved > 5) {
			isExcuteTouch = false;
			this->setColorPress(mBaseColor);
		}
	}
}

void EGButtonSprite::ccTouchEnded(Touch* touch, Event* event) {
	if (isExcuteTouch) {
		if (mButtonType == EGButton2FrameDark || mButtonType == EGButtonScrollViewItem) {
			this->setColorPress(mBaseColor);
		}
		else if (mButtonType == EGButton2Frame) {
			if (mImageType == EGImageFile) {
				this->setTexture(mBaseFileName.c_str());
			}
			else if (mImageType == EGImageTexture2D) {
				Sprite::setTexture(mBaseTexture);
			}
			else if (mImageType == EGImageSpriteFrame) {
				this->setSpriteFrame(mBaseSpriteFrame);
			}
			else if (mImageType == EGImageSpriteFrameName) {
				this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(mFileName.c_str()));
			}
		} else if (mButtonType == EGButtonBubble) {
			this->stopAllActions();
			this->runAction(ScaleTo::create(0.1f, 1));
		}
		if (mButtonType != EGButtonNormal && containsTouchLocation(touch)) {
			performClick();
		}
		performUp();
	}
}