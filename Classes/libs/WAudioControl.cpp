#include "WAudioControl.h"

WAudioControl::WAudioControl() : _vlmBackground(1.f), _vlmEffect(1.f), _newEngine(false), _enable(true) {
#if (COCOS2D_VERSION >= 0x00030400 && CC_TARGET_PLATFORM != CC_PLATFORM_WP8 && CC_TARGET_PLATFORM != CC_PLATFORM_WINRT)
	AudioEngine::lazyInit();
#endif
};

WAudioControl::~WAudioControl() {

};

WAudioControl* WAudioControl::getInstance() {
	static WAudioControl* audioControl = nullptr;
	if (audioControl == nullptr)
		audioControl = new WAudioControl();
	return audioControl;
}

void WAudioControl::setVolumeBackground(float volume) {
	_vlmBackground = volume;
	for (int id = static_cast<int>(_listBackgroundID.size()) - 1; id >= 0; id--) {
		libs::AudioInfo info = _listBackgroundID.at(id);
#if (COCOS2D_VERSION >= 0x00030400 && CC_TARGET_PLATFORM != CC_PLATFORM_WP8 && CC_TARGET_PLATFORM != CC_PLATFORM_WINRT)
		AudioEngine::setVolume(info._audioId, volume);
#endif
	}
	CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(_vlmBackground);
}

void WAudioControl::setVolumeEffect(float volume) {
	_vlmEffect = volume;
	for (int id = static_cast<int>(_listEffectID.size()) - 1; id >= 0; id--) {
		libs::AudioInfo info = _listEffectID.at(id);
#if (COCOS2D_VERSION >= 0x00030400 && CC_TARGET_PLATFORM != CC_PLATFORM_WP8 && CC_TARGET_PLATFORM != CC_PLATFORM_WINRT)
		AudioEngine::setVolume(info._audioId, volume);
#endif
	}
	CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(_vlmEffect);
}
void WAudioControl::enableNewEngine(bool enable) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    return;
#endif
#if (COCOS2D_VERSION >= 0x00030400 && CC_TARGET_PLATFORM != CC_PLATFORM_WP8 && CC_TARGET_PLATFORM != CC_PLATFORM_WINRT)
	_newEngine = enable;
	stopAll();
#endif
}
void WAudioControl::enableAudio(bool enable) {
	_enable = enable;
	stopAll();
}
void WAudioControl::playEffectSound(std::string effectSound, bool loop, std::function<void()> callback) {
	if (!getEnable()) return;
	if (_newEngine) {
#if (COCOS2D_VERSION >= 0x00030400 && CC_TARGET_PLATFORM != CC_PLATFORM_WP8 && CC_TARGET_PLATFORM != CC_PLATFORM_WINRT)
		int _audioID = AudioEngine::play2d(effectSound, loop, getVolumeEffect());
		//const char* pathSound = effectSound.c_str();
		if (loop) {
			_listEffectID.push_back(libs::AudioInfo(effectSound, _audioID, loop));
			stopEffectSound(effectSound);
		}
		AudioEngine::setFinishCallback(_audioID, [=](int id, const std::string& filePath){
			if (callback) callback();
		});
#endif
	}
	else {
		unsigned int _audioID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(effectSound.c_str(), loop);
		if (loop) {
			_listEffectID.push_back(libs::AudioInfo(effectSound, static_cast<int>(_audioID), loop));
			stopEffectSound(effectSound);
		}
	}
}
void WAudioControl::playBackgroundMusic(std::string backgroundMusic, bool loop, std::function<void()> callback) {
	if (!getEnable()) return;
	if (_newEngine) {
#if (COCOS2D_VERSION >= 0x00030400 && CC_TARGET_PLATFORM != CC_PLATFORM_WP8 && CC_TARGET_PLATFORM != CC_PLATFORM_WINRT)
		bool played = false;
		if (_listBackgroundID.size() > 0) {
			libs::AudioInfo info = _listBackgroundID.back();
			if (info._audioName.compare(backgroundMusic)) {
				AudioEngine::stop(info._audioId);
				_listBackgroundID.erase(_listBackgroundID.begin());
			}
			else {
				played = true;
			}
		}
		if (!played) {
			int _audioID = AudioEngine::play2d(backgroundMusic, loop, getVolumeBackground());
			_listBackgroundID.push_back(libs::AudioInfo(backgroundMusic, _audioID, loop));
			log("%s start", backgroundMusic.c_str());
			AudioEngine::setFinishCallback(_audioID, [&, callback](int id, const std::string& filePath){
				log("%s end", backgroundMusic.c_str());
				if (callback) callback();
			});
		}
#endif
	}
	else {
		bool played = false;
		if (_listBackgroundID.size() > 0) {
			libs::AudioInfo info = _listBackgroundID.back();
			if (info._audioName.compare(backgroundMusic)) {
				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
				_listBackgroundID.erase(_listBackgroundID.begin());
			}
			else {
				played = true;
			}
		}
		if (!played) {
			CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(backgroundMusic.c_str(), loop);
			_listBackgroundID.push_back(libs::AudioInfo(backgroundMusic, 0, loop));

		}
	}
}

void WAudioControl::stopEffectSound(std::string effectSound) {
	for (int i = static_cast<int>(_listEffectID.size()) - 1; i >= 0; i--) {
		libs::AudioInfo audioInfo = _listEffectID.at(i);
		if (!audioInfo._audioName.compare(effectSound)) {
			if (_newEngine) {
#if (COCOS2D_VERSION >= 0x00030400 && CC_TARGET_PLATFORM != CC_PLATFORM_WP8 && CC_TARGET_PLATFORM != CC_PLATFORM_WINRT)
				AudioEngine::stop(audioInfo._audioId);
#endif
			}
			else {
				CocosDenshion::SimpleAudioEngine::getInstance()->stopEffect(audioInfo._audioId);
			}
		}
	}
}

void WAudioControl::stopAll() {
	CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
#if (COCOS2D_VERSION >= 0x00030400 && CC_TARGET_PLATFORM != CC_PLATFORM_WP8 && CC_TARGET_PLATFORM != CC_PLATFORM_WINRT)
	AudioEngine::stopAll();
	_listBackgroundID.clear();
	_listEffectID.clear();
#endif
}

void WAudioControl::pauseAll() {
	if (_newEngine) {
#if (COCOS2D_VERSION >= 0x00030400 && CC_TARGET_PLATFORM != CC_PLATFORM_WP8 && CC_TARGET_PLATFORM != CC_PLATFORM_WINRT)
		AudioEngine::pauseAll();
#endif
	}
	else {
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}
}

void WAudioControl::resumeAll() {
	if (_newEngine) {
#if (COCOS2D_VERSION >= 0x00030400 && CC_TARGET_PLATFORM != CC_PLATFORM_WP8 && CC_TARGET_PLATFORM != CC_PLATFORM_WINRT)
		AudioEngine::resumeAll();
#endif
	}
	else {
		CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
	}
}

void WAudioControl::preloadBackgroundMusic(std::string backgroundMusic) {
	if (!_newEngine) {
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(backgroundMusic.c_str());
	}
}

void WAudioControl::preloadEffect(std::string effectSound) {
	if (!_newEngine) {
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect(effectSound.c_str());
	}
}

void WAudioControl::setLoopSound(std::string sound, bool loop) {
	if (_newEngine) {
#if (COCOS2D_VERSION >= 0x00030400 && CC_TARGET_PLATFORM != CC_PLATFORM_WP8 && CC_TARGET_PLATFORM != CC_PLATFORM_WINRT)
		int _audioId = _getAudioId(sound);
		if (_audioId != -1)
			AudioEngine::setLoop(_audioId, loop);
#endif
	}
}

float WAudioControl::getDurationSound(std::string sound) {
	if (_newEngine) {
#if (COCOS2D_VERSION >= 0x00030400 && CC_TARGET_PLATFORM != CC_PLATFORM_WP8 && CC_TARGET_PLATFORM != CC_PLATFORM_WINRT)
		int _audioId = _getAudioId(sound);
		if (_audioId != -1)
			return AudioEngine::getDuration(_audioId);
	}
	return AudioEngine::TIME_UNKNOWN;
#else
	}
#endif
	return 0;
}

int WAudioControl::_getAudioId(std::string sound) {
	int _id = -1;
	for (int i = static_cast<int>(_listEffectID.size()) - 1; i >= 0; i--) {
		libs::AudioInfo audioInfo = _listEffectID.at(i);
		if (!audioInfo._audioName.compare(sound)) {
			if (_newEngine) {
				_id = audioInfo._audioId;
				break;
			}
		}
	}
	if (_id == -1)
		for (int i = static_cast<int>(_listBackgroundID.size()) - 1; i >= 0; i--) {
			libs::AudioInfo audioInfo = _listBackgroundID.at(i);
			if (!audioInfo._audioName.compare(sound)) {
				if (_newEngine) {
					_id = audioInfo._audioId;
					break;
				}
			}
		}
	return _id;
}