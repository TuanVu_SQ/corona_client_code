#pragma once

#include <string>
#include <ctime>
#include "cocos2d.h"

#include "json/document.h"
#include "EGSupport.h"
USING_NS_CC;
class JsonParser {
	JsonParser(){}
	rapidjson::Document doc;
public:
	static JsonParser* getInstance();
	bool parseJsonFile(const std::string& pFile);
	bool parse(unsigned char* buffer, long length);
	void clearCache();
	std::vector<std::vector<Vec2>> bodyFormJson(Node* pNode, std::string name);
	std::vector<std::vector<Vec2>> bodyFormJson(Size pSize, Vec2 pAnChorPoint, float pScale, std::string name);
	std::vector<Vec2> bodyLineFormJson(Size pSize, Vec2 pAnChorPoint, std::string name);
	std::vector<Vec2> insertCircle(std::vector<Vec2> pVec, Vec2 pPointCircle, float pRadius);
	bool checkDagiacLoi(std::vector<Vec2> pArrayPoint);
	Vector<PhysicsShape*> cutPolygon(std::vector<Vec2> pArrayPoint);
};