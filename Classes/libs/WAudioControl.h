#pragma once

#include "WConfig.h"

namespace libs {
	class AudioInfo {
	public:
		AudioInfo(std::string _audioName, int _audioId, bool _loop) {
			this->_audioName = _audioName;
			this->_audioId = _audioId;
			this->_loop = _loop;
		}
		std::string _audioName;
		int _audioId;
		bool _loop;
	};
}

class WAudioControl {
public:
	static WAudioControl* getInstance();

public:
	void setVolumeBackground(float volume);
	void setVolumeEffect(float volume);
	void enableNewEngine(bool enable);
	void enableAudio(bool enable);
	void playEffectSound(std::string effectSound, bool loop = false, std::function<void()> callback = nullptr);
	void playBackgroundMusic(std::string backgroundMusic, bool loop = true, std::function<void()> callback = nullptr);
	void stopEffectSound(std::string effectSound);
	void stopAll();
	void pauseAll();
	void resumeAll();
	void preloadBackgroundMusic(std::string backgroundMusic);
	void preloadEffect(std::string effectSound);
	void setLoopSound(std::string sound, bool loop);
	float getDurationSound(std::string sound);

private:
	int _getAudioId(std::string sound);

private:
	CC_SYNTHESIZE_READONLY(float, _vlmBackground, VolumeBackground);
	CC_SYNTHESIZE_READONLY(float, _vlmEffect, VolumeEffect);
	CC_SYNTHESIZE_READONLY(bool, _newEngine, NewEngine);
	CC_SYNTHESIZE_READONLY(bool, _enable, Enable);

private:
	std::vector<libs::AudioInfo> _listBackgroundID;
	std::vector<libs::AudioInfo> _listEffectID;

private:
	WAudioControl();
	~WAudioControl();
};