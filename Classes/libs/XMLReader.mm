#include "WXmlReader.h"


string WXmlReader::getPathFileIOS()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    string cfilename=[resourcePath UTF8String];
    return cfilename + "/";
#endif
}
