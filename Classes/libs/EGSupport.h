#ifndef __EGSUPPORT_H_INCLUDED__
#define __EGSUPPORT_H_INCLUDED__

#include "cocos2d.h"

using namespace cocos2d;

class EGSupport {
public:
	static RenderTexture* createStroke(Sprite* label, int size, Color3B color, GLubyte opacity);
	static float countDistance(Vec2 pPointStart, Vec2 pPointEnd);
	static float countDistance(float startX, float startY, float endX, float endY);
	static float countDistanceEllipse(float startX, float startY, float endX, float endY, float height);
	static float getDirectionByRotate(float startX, float startY, float endX, float endY);
	static Point getNewVec2(float startX, float startY, float endX, float endY, float rangeChange);
	static Point getPointInCircle(float pCenterX, float pCenterY, float pRadius, float pAngle);
	static float getResultOfMath(std::string pStrMath);
	static std::string replaceString(std::string pBaseString, std::string pStrFind, std::string pStrReplace);
	static std::string addDotMoney(long long pMoney);
	static std::string addDotMoney(std::string pStrMoney);
	static Point getNewPoint(float startX, float startY, float endX, float endY, float rangeChange);
	static float absRotation(float pRotation);
	static std::string shortCutNumText(std::string pNumText, int pCountDisplay, bool isAddDot);
	static std::string convertMoneyAndAddDot(uint32_t i);
	static std::string convertIntToString(uint32_t i);
	static std::string addSpaceInStdString(std::string origin, int maxLength);
	static std::string convertMoneyAndAddText(uint32_t i);
};

#endif