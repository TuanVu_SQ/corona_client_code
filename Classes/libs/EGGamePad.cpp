#include "EGGamePad.h"
#include "EGSupport.h"

EGGamePad* EGGamePad::createWithFile(const char* fileNameBorder, const char* fileNameOracle) {
	return EGGamePad::createWithFile(fileNameBorder, fileNameOracle, 0.001f);
}

EGGamePad* EGGamePad::createWithFile(const char* fileNameBorder, const char* fileNameOracle, float pSensitivity) {
	EGGamePad* pEGGamePad = new EGGamePad();
	pEGGamePad->initWithFile(fileNameBorder, fileNameOracle, pSensitivity);
	pEGGamePad->autorelease();

	return pEGGamePad;
}

EGGamePad* EGGamePad::createWithTexture(Texture2D* texture2DBorder, Texture2D* texture2DOracle) {
	return EGGamePad::createWithTexture(texture2DBorder, texture2DOracle, 0.001f);
}

EGGamePad* EGGamePad::createWithTexture(Texture2D* texture2DBorder, Texture2D* texture2DOracle, float pSensitivity) {
	EGGamePad* pEGGamePad = new EGGamePad();
	pEGGamePad->initWithTexture(texture2DBorder, texture2DOracle, pSensitivity);
	pEGGamePad->autorelease();

	return pEGGamePad;
}

EGGamePad* EGGamePad::createWithSpriteFrameName(const char* fileNameBorder, const char* fileNameOracle) {
	return EGGamePad::createWithSpriteFrameName(fileNameBorder, fileNameOracle, 0.001f);
}

EGGamePad* EGGamePad::createWithSpriteFrameName(const char* fileNameBorder, const char* fileNameOracle, float pSensitivity) {
	EGGamePad* pEGGamePad = new EGGamePad();
	pEGGamePad->initWithSpriteFrameName(fileNameBorder, fileNameOracle, pSensitivity);
	pEGGamePad->autorelease();

	return pEGGamePad;
}

EGGamePad* EGGamePad::createWithSpriteFrame(SpriteFrame* spriteFrameBorder, SpriteFrame* spriteFrameOracle) {
	return EGGamePad::createWithSpriteFrame(spriteFrameBorder, spriteFrameOracle, 0.001f);
}

EGGamePad* EGGamePad::createWithSpriteFrame(SpriteFrame* spriteFrameBorder, SpriteFrame* spriteFrameOracle, float pSensitivity) {
	EGGamePad* pEGGamePad = new EGGamePad();
	pEGGamePad->initWithSpriteFrame(spriteFrameBorder, spriteFrameOracle, pSensitivity);
	pEGGamePad->autorelease();

	return pEGGamePad;
}

void EGGamePad::init(float pSensitivity) {
	mCircleBorder->setVisible(false);
	this->addChild(mCircleBorder);
	mOracle->setVisible(false);
	this->addChild(mOracle);
	mGamePadListener = NULL;
	mGamePadType = EGGamePadNormal;
	mWidthhGame = 800;
	mHeightGame = 480;

	auto listener = EventListenerTouchAllAtOnce::create();
	listener->onTouchesBegan = CC_CALLBACK_2(EGGamePad::onTouchesBegan, this);
	listener->onTouchesMoved = CC_CALLBACK_2(EGGamePad::onTouchesMoved, this);
	listener->onTouchesEnded = CC_CALLBACK_2(EGGamePad::onTouchesEnded, this);
	listener->onTouchesCancelled = CC_CALLBACK_2(EGGamePad::onTouchesCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	this->schedule(schedule_selector(EGGamePad::update), pSensitivity);
}

bool EGGamePad::initWithFile(const char* fileNameBorder, const char* fileNameOracle, float pSensitivity) {
	mCircleBorder = Sprite::createWithSpriteFrameName(fileNameBorder);
	mOracle = Sprite::createWithSpriteFrameName(fileNameOracle);
	init(pSensitivity);

	return true;
}

bool EGGamePad::initWithTexture(Texture2D* texture2DBorder, Texture2D* texture2DOracle, float pSensitivity) {
	mCircleBorder = Sprite::createWithTexture(texture2DBorder);
	mOracle = Sprite::createWithTexture(texture2DOracle);
	init(pSensitivity);

	return true;
}

bool EGGamePad::initWithSpriteFrameName(const char* fileNameBorder, const char* fileNameOracle, float pSensitivity) {
	mCircleBorder = Sprite::createWithSpriteFrameName(fileNameBorder);
	mOracle = Sprite::createWithSpriteFrameName(fileNameOracle);
	init(pSensitivity);

	return true;
}

bool EGGamePad::initWithSpriteFrame(SpriteFrame* spriteFrameBorder, SpriteFrame* spriteFrameOracle, float pSensitivity) {
	mCircleBorder = Sprite::createWithSpriteFrame(spriteFrameBorder);
	mOracle = Sprite::createWithSpriteFrame(spriteFrameOracle);
	init(pSensitivity);

	return true;
}

void EGGamePad::update(float dt) {
	if (mGamePadListener) {
		if (isTouchingMovement) {
			float pDistanceOracle = EGSupport::countDistance(mOracle->getPositionX(), mOracle->getPositionY(),
				mCircleBorder->getPositionX(), mCircleBorder->getPositionY());
			float pRotateOracle = EGSupport::getDirectionByRotate(mOracle->getPositionX(), mOracle->getPositionY(),
				mCircleBorder->getPositionX(), mCircleBorder->getPositionY());
			mGamePadListener->onControlMovement(pRotateOracle, (pDistanceOracle/(mCircleBorder->getContentSize().width/2))*100);
		}
	}
}

void EGGamePad::setGamepadType(EGGamePadType pEGGamePadType) {
	mGamePadType = pEGGamePadType;
}

void EGGamePad::setGamepadListener(EGGamePadListener *pGamepadListener) {
	mGamePadListener = pGamepadListener;
}

void EGGamePad::setGameRelolution(float pWidth, float pHeight) {
	mWidthhGame = pWidth;
	mHeightGame = pHeight;
}

void EGGamePad::onTouchesBegan(const std::vector<Touch*>& touches, cocos2d::Event  *event) {
	for (int i = 0; i < touches.size(); i++) {
		Touch* pTouch = touches.at(i);
		if (pTouch->getLocation().x < mWidthhGame / 2 && !mCircleBorder->isVisible()) {
			mCircleBorder->setPosition(pTouch->getLocation());
			mOracle->setPosition(pTouch->getLocation());
			mCircleBorder->setVisible(true);
			mOracle->setVisible(true);
			isTouchingMovement = true;
			mTouchMovementID = pTouch->getID();
		}
	}
}

void EGGamePad::onTouchesMoved(const std::vector<Touch*>& touches, cocos2d::Event  *event) {
	for (int i = 0; i < touches.size(); i++) {
		Touch* pTouch = touches.at(i);
		if (isTouchingMovement && pTouch->getID() == mTouchMovementID) {
			if (EGSupport::countDistance(pTouch->getLocation().x, pTouch->getLocation().y,
				mCircleBorder->getPositionX(), mCircleBorder->getPositionY()) < mCircleBorder->getContentSize().width / 2) {
				mOracle->setPosition(pTouch->getLocation());
			}
			else {
				mOracle->setPosition(EGSupport::getNewVec2(pTouch->getLocation().x, pTouch->getLocation().y,
					mCircleBorder->getPositionX(), mCircleBorder->getPositionY(), -mCircleBorder->getContentSize().width / 2));
			}
		}
	}
}

void EGGamePad::onTouchesEnded(const std::vector<Touch*>& touches, cocos2d::Event  *event) {
	for (int i = 0; i < touches.size(); i++) {
		Touch* pTouch = touches.at(i);
		if (isTouchingMovement && pTouch->getID() == mTouchMovementID) {
			mOracle->setPosition(mCircleBorder->getPosition());
			mCircleBorder->setVisible(false);
			mOracle->setVisible(false);
			isTouchingMovement = false;
		}
	}
}

void EGGamePad::onTouchesCancelled(const std::vector<Touch*>& touches, cocos2d::Event  *event) {
	onTouchesEnded(touches, event);
}