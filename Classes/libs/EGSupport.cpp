#include "EGSupport.h"
#include <math.h>

RenderTexture* EGSupport::createStroke(Sprite* label, int size, Color3B color, GLubyte opacity) {

	RenderTexture* rt = RenderTexture::create(
		label->getTexture()->getContentSize().width + size * 2,
		label->getTexture()->getContentSize().height + size * 2
		);

	Point originalPos = label->getPosition();
	Color3B originalColor = label->getColor();
	GLubyte originalOpacity = label->getOpacity();
	bool originalVisibility = label->isVisible();

	label->setColor(color);

	label->setOpacity(opacity);

	label->setVisible(true);

	BlendFunc originalBlend = label->getBlendFunc();

	BlendFunc bf = { GL_SRC_ALPHA, GL_ONE };

	label->setBlendFunc(bf);

	Point bottomLeft = Vec2(
		label->getTexture()->getContentSize().width * label->getAnchorPoint().x + size,
		label->getTexture()->getContentSize().height * label->getAnchorPoint().y + size);

	Point positionOffset = Vec2(
		-label->getTexture()->getContentSize().width / 2,
		-label->getTexture()->getContentSize().height / 2);

	Point position = ccpSub(originalPos, positionOffset);

	rt->begin();

	for (int i = 0; i<360; i += 15) {
		label->setPosition(
			Vec2(bottomLeft.x + sin(CC_DEGREES_TO_RADIANS(i))*size, bottomLeft.y + cos(CC_DEGREES_TO_RADIANS(i))*size)
			);
		label->visit();
	}
	rt->end();
	label->setPosition(originalPos);
	label->setColor(originalColor);
	label->setBlendFunc(originalBlend);
	label->setVisible(originalVisibility);
	label->setOpacity(originalOpacity);

	//rt->setPosition(0,0);
	rt->setPosition(label->getContentSize().width / 2, label->getContentSize().height / 2);

	return rt;
}
std::string EGSupport::addSpaceInStdString(std::string origin, int maxLength) {
	if (maxLength <= 0) return origin;
	std::string stringReturn;
	int length = 0;
	while (origin.length() > 0) {
		char characterNow = origin[0];
		if (characterNow != ' ')
			length++;
		else length = 0;

		if (length >= maxLength) {
			stringReturn += characterNow;
			stringReturn += ' ';
			length = 0;
		}
		else {
			stringReturn += characterNow;
		}
		origin.erase(origin.begin() + 0);
	}
	return stringReturn;
}

std::string EGSupport::convertIntToString(uint32_t i) {
	static char str[20];
	sprintf(str, "%d", i);
	return (std::string)str;
}

std::string EGSupport::convertMoneyAndAddDot(uint32_t i) {
	std::string money = convertIntToString(i);
	int pCounterDot = (money.length() - 1) / 3;
	int pIndex = money.length();
	std::string newStrMoney;
	while (pCounterDot > 0) {
		newStrMoney = "." + money.substr(pIndex - 3, 3) + newStrMoney;
		pCounterDot--;
		pIndex -= 3;
	}
	newStrMoney = money.substr(0, pIndex) + newStrMoney;
	return newStrMoney;
}


float EGSupport::countDistance(Vec2 pPointStart, Vec2 pPointEnd) {
	return countDistance(pPointStart.x, pPointStart.y, pPointEnd.x, pPointEnd.y);
}

float EGSupport::countDistance(float startX, float startY, float endX, float endY) {
	return sqrt((endX - startX)*(endX - startX) + (endY - startY)*(endY - startY));
}

float EGSupport::countDistanceEllipse(float startX, float startY, float endX, float endY, float height) {
	float radiusHozi = countDistance(startX, startY, endX, endY) / 2;
	float radiusAlong = height;

	return M_PI*(3 * (radiusHozi + radiusAlong) - sqrt((3 * radiusHozi + radiusAlong)*(radiusHozi + 3 * radiusAlong)));
}

float EGSupport::getDirectionByRotate(float startX, float startY, float endX, float endY) {
	float pDistanceX = endX - startX;
	float pDistanceY = endY - startY;

	return atan2f(pDistanceX, pDistanceY) * 180 / M_PI;
}

Point EGSupport::getNewVec2(float startX, float startY, float endX, float endY, float rangeChange) {
	Point newPoint;
	float pDistance = countDistance(startX, startY, endX, endY);

	if (pDistance != 0) {
		float rate = (pDistance + rangeChange) / pDistance;
		newPoint.x = startX + rate*(endX - startX);
		newPoint.y = startY + rate*(endY - startY);
	}
	else {
		newPoint.x = startX;
		newPoint.y = startY;
	}
	return newPoint;
}

Point EGSupport::getPointInCircle(float pCenterX, float pCenterY, float pRadius, float pAngle) {
	Point newPoint;

	float constX = sin(pAngle*M_PI / 180);
	float constY = cos(pAngle*M_PI / 180);

	newPoint.x = pCenterX + pRadius*constX;
	newPoint.y = pCenterY + pRadius*constY;

	return newPoint;
}

float EGSupport::getResultOfMath(std::string pStrMath) {
	pStrMath = replaceString(pStrMath, " ", "");
	bool step1 = true;
	while (step1) {
		int pSavePos = 0;
		bool isFoundStep1 = false;
		for (int i = 0; i < pStrMath.length(); i++) {
			std::string pSubStr = pStrMath.substr(i, 1);
			if (pSubStr == "(") {
				pSavePos = i;
				isFoundStep1 = true;
			}
			if (pSubStr == ")") {
				std::string pMiniMath = pStrMath.substr(pSavePos, (i + 1) - pSavePos);
				float pMiniResult = getResultOfMath(pMiniMath.substr(1, pMiniMath.length() - 1 - 1));
				std::stringstream ss; ss << pMiniResult;
				pStrMath = replaceString(pStrMath, pMiniMath, ss.str());
				break;
			}
		}
		step1 = isFoundStep1;
	}
	bool step2 = true;
	while (step2) {
		int pSavePoint = 0;
		int pSavePoint2 = 0;
		bool isFoundStep2 = false;
		std::string pFunc = "";
		for (int i = 0; i < pStrMath.length(); i++) {
			std::string pSubStr = pStrMath.substr(i, 1);
			if (pSubStr == "*" || pSubStr == "/" || pSubStr == "+" || pSubStr == "-" || i == pStrMath.length() - 1) {
				if (isFoundStep2) {
					bool returnI = false;
					if (i == pStrMath.length() - 1) {
						i++;
						returnI = true;
					}
					std::string pMiniMath = pStrMath.substr(pSavePoint, i - pSavePoint);
					float pNumber1 = atof(pStrMath.substr(pSavePoint, pSavePoint2 - pSavePoint).c_str());
					float pNumber2 = atof(pStrMath.substr(pSavePoint2 + 1, i - (pSavePoint2 + 1)).c_str());
					if (pFunc == "*") {
						std::stringstream ss; ss << pNumber1*pNumber2;
						pStrMath = replaceString(pStrMath, pMiniMath, ss.str());
					}
					else {
						std::stringstream ss; ss << pNumber1 / pNumber2;
						pStrMath = replaceString(pStrMath, pMiniMath, ss.str());
					}
					if (returnI) {
						i--;
					}
					break;
				}
				else if (pSubStr == "*" || pSubStr == "/") {
					pFunc = pSubStr;
					isFoundStep2 = true;
					pSavePoint2 = i;
				}
				else if (i != pStrMath.length() - 1){
					pSavePoint = i + 1;
				}
			}
		}
		step2 = isFoundStep2;
	}
	bool step3 = true;
	while (step3) {
		int pSavePoint = 0;
		int pSavePoint2 = 0;
		bool isFoundStep3 = false;
		std::string pFunc = "";
		for (int i = 0; i < pStrMath.length(); i++) {
			std::string pSubStr = pStrMath.substr(i, 1);
			if (pSubStr == "+" || pSubStr == "-" || i == pStrMath.length() - 1) {
				if (isFoundStep3) {
					bool returnI = false;
					if (i == pStrMath.length() - 1) {
						i++;
						returnI = true;
					}
					std::string pMiniMath = pStrMath.substr(0, i);
					float pNumber1 = atof(pStrMath.substr(0, pSavePoint).c_str());
					float pNumber2 = atof(pStrMath.substr(pSavePoint + 1, i - (pSavePoint + 1)).c_str());
					if (pFunc == "+") {
						std::stringstream ss; ss << pNumber1 + pNumber2;
						pStrMath = replaceString(pStrMath, pMiniMath, ss.str());
					}
					else {
						std::stringstream ss; ss << pNumber1 - pNumber2;
						pStrMath = replaceString(pStrMath, pMiniMath, ss.str());
					}
					if (returnI) {
						i--;
					}
					break;
				}
				else if (i != pStrMath.length() - 1){
					pSavePoint = i;
					isFoundStep3 = true;
					pFunc = pSubStr;
				}
			}
		}
		step3 = isFoundStep3;
	}

	return atof(pStrMath.c_str());
}

std::string EGSupport::replaceString(std::string pBaseString, std::string pStrFind, std::string pStrReplace) {
	int pTextFindLenght = pStrFind.length();
	int pTextBaseLenght = pBaseString.length();
	for (int i = 0; i <= pTextBaseLenght - pTextFindLenght; i++) {
		std::string pSubString = pBaseString.substr(i, pTextFindLenght);
		if (pSubString == pStrFind) {
			pBaseString = pBaseString.substr(0, i) + pStrReplace + pBaseString.substr(i + pTextFindLenght, pTextBaseLenght - (i + pTextFindLenght));
			pTextBaseLenght = pBaseString.length();
		}
	}
	return pBaseString;
}

std::string EGSupport::addDotMoney(long long pMoney) {
	std::stringstream ss; ss << pMoney;
	return addDotMoney(ss.str());
}

std::string EGSupport::addDotMoney(std::string pStrMoney) {
	bool pNegative = false;
	std::string pFirstChar = "-";
	if (pStrMoney.length() > 0){
		if (pStrMoney[0] == pFirstChar[0]){
			pNegative = true;
			pStrMoney.erase(pStrMoney.begin() + 0);
		}

		int pCounterDot = (pStrMoney.length() - 1) / 3;
		int pIndex = pStrMoney.length();
		std::string newStrMoney;
		while (pCounterDot > 0) {
			newStrMoney = "." + pStrMoney.substr(pIndex - 3, 3) + newStrMoney;
			pCounterDot--;
			pIndex -= 3;
		}
		newStrMoney = pStrMoney.substr(0, pIndex) + newStrMoney;
		if (pNegative){
			newStrMoney = "-" + newStrMoney;
		}
		return newStrMoney;
	}
	return pStrMoney;
}
Point EGSupport::getNewPoint(float startX, float startY, float endX, float endY, float rangeChange) {
	Point newPoint;
	float pDistance = ccpDistance(Vec2(startX, startY),Vec2( endX, endY));

	if (pDistance != 0) {
		float rate = rangeChange / pDistance;
		newPoint.x = startX + rate*(endX - startX);
		newPoint.y = startY + rate*(endY - startY);
	}
	else {
		newPoint.x = startX;
		newPoint.y = startY;
	}
	return newPoint;
}
float EGSupport::absRotation(float pRotation){
	while (pRotation < 0.0){
		pRotation += 360.0;
	}
	while (pRotation > 360.0){
		pRotation -= 360.0;
	}
	/*int _rotation = pRotation ;
	pRotation = (float)_rotation ;*/
	return pRotation;
}
std::string EGSupport::shortCutNumText(std::string pNumText, int pCountDisplay,bool isAddDot){
	std::string pDot = ".";
	for (int i = pNumText.length() - 1; i >= 0; i--){
		if (pNumText.at(i) == pDot.at(0)){
			pNumText.erase(pNumText.begin() + i);
		}
	}
	if (pNumText.length() <= pCountDisplay){
		if (isAddDot){
			pNumText = EGSupport::addDotMoney(pNumText);
		}
		return pNumText;
	}
	else{
		if (pNumText.length() > 9){
			pNumText = pNumText.substr(0, pNumText.length() - 9);
			if (isAddDot){
				pNumText = EGSupport::addDotMoney(pNumText);
			}
			pNumText = pNumText + "B";
		}
		else if (pNumText.length() > 6){
			pNumText = pNumText.substr(0, pNumText.length() - 6);
			if (isAddDot){
				pNumText = EGSupport::addDotMoney(pNumText);
			}
			pNumText = pNumText + "M";
		}
		else if (pNumText.length() > 3){
			pNumText = pNumText.substr(0, pNumText.length() - 3);
			if (isAddDot){
				pNumText = EGSupport::addDotMoney(pNumText);
			}
			pNumText = pNumText + "K";
		}
		return pNumText;
	}
}

//support for 0 -> 999.999.999.999
std::string EGSupport::convertMoneyAndAddText(uint32_t i) {
	std::string newString;
	uint32_t intOrigin = i;
	uint32_t intExcess = 0;
	uint8_t level = 0;
	while (intOrigin >= 10000) {
		intExcess = intOrigin % 1000;
		intOrigin /= 1000;
		level++;
	}
	newString += convertIntToString(intOrigin);
	if (intExcess >= 10) {
		std::string stringExecess = convertIntToString((uint32_t)(intExcess / 10));
		newString += "." + (stringExecess.length() == 1 ? "0" + stringExecess : stringExecess);
	}
	switch (level)
	{
	case 1:
		newString += "K";
		break;
	case 2:
		newString += "M";
		break;
	case 3:
		newString += "B";
		break;
	default:
		return convertIntToString(i);
	}
	return newString;
}