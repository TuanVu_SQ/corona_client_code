#include "EGEditText.h"

EGEditText* EGEditText::createWithFile(const char *pPathImage, const char *pFont, float pSizeFont) {
	EGEditText *pEditText = new EGEditText();
	pEditText->initWithFile(pPathImage,pFont,pSizeFont);
	pEditText->autorelease();

	return pEditText;
}

EGEditText* EGEditText::createWithSpriteFrameName(const char *pPathImage, const char *pFont, float pSizeFont) {
	EGEditText *pEditText = new EGEditText();
	pEditText->initWithSpriteFrameName(pPathImage,pFont,pSizeFont);
	pEditText->autorelease();

	return pEditText;
}

bool EGEditText::initWithSpriteFrameName(const char *pPathImage, const char *pFont,float pSizeFont) {
	EGSprite::initWithSpriteFrameName(pPathImage);

	mDimension = 0;

	mText = TextFieldTTF::textFieldWithPlaceHolder("", pFont, pSizeFont);
	mText->setPosition(Vec2(this->getContentSize().width/2, this->getContentSize().height/2));
	mText->setColor(Color3B::BLACK);
	this->addChild(mText);
	mText->setDimensions(this->getContentSize().width - 10 ,this->getContentSize().height - 10);

	mHint = TextFieldTTF::textFieldWithPlaceHolder("", pFont, pSizeFont);
	mHint->setPosition(Vec2(this->getContentSize().width/2, this->getContentSize().height/2));
	mHint->setColor(Color3B::GRAY);
	this->addChild(mHint);
	mHint->setDimensions(this->getContentSize().width - 10 ,this->getContentSize().height - 10);
	this->registerTouch();

	return true;
}

bool EGEditText::initWithFile(const char *pPathImage, const char *pFont,float pSizeFont) {
	EGSprite::initWithFile(pPathImage);

	mDimension = 0;
	mIsPasswords = false;

	mVirtualText = TextFieldTTF::textFieldWithPlaceHolder("", pFont, pSizeFont);

	mText = TextFieldTTF::textFieldWithPlaceHolder("", pFont, pSizeFont);
	mText->setPosition(Vec2(this->getContentSize().width/2, this->getContentSize().height/2));
	mText->setColor(Color3B::BLACK);
	this->addChild(mText);
	mText->setDimensions(this->getContentSize().width - 10 ,this->getContentSize().height - 10);
	
	mHint = TextFieldTTF::textFieldWithPlaceHolder("", pFont, pSizeFont);
	mHint->setPosition(Vec2(this->getContentSize().width/2, this->getContentSize().height/2));
	mHint->setColor(Color3B::GRAY);
	this->addChild(mHint);
	mHint->setDimensions(this->getContentSize().width - 10 ,this->getContentSize().height - 10);
	this->registerTouch();

	return true;
}

void EGEditText::setDimension(float pWidth, float pHeight) {
	mText->setDimensions(pWidth,pHeight);
	mHint->setDimensions(pWidth,pHeight);
}

void EGEditText::setHint(std::string pHint) {
	mHint->setString(pHint.c_str());
}

bool EGEditText::TouchBegan(Touch*touch,Event* pevent){
	if(containsTouchLocation(touch) && isVisible() && this->getParent()->isVisible()) {
		if(this->getParent()->getParent() != NULL && this->getParent()->getParent()->isVisible()) {
			mHint->setVisible(false);
			Director::sharedDirector()->getOpenGLView()->setIMEKeyboardState(false);
			mVirtualText->attachWithIME();
			return true;
		}
	} else {
		mVirtualText->setString("");
		Director::sharedDirector()->getOpenGLView()->setIMEKeyboardState(false);
	}

	return false;
}

std::string EGEditText::getText(){
	return mVirtualText->getString();
}

void EGEditText::setText(std::string pText){
	/*if (mIsPasswords) {
		std::string pText = "";
		for (int i = 0; i < pText.length; i++) {
			pText += "*";
		}
		mText->setString(pText.c_str());
	}
	else {
		mText->setString(pText.c_str());
	}*/
	mVirtualText->setString(pText.c_str());
}

void EGEditText::setOpacity(GLubyte glubyte) {
	EGSprite::setOpacity(glubyte);
	mText->setOpacity(glubyte);
}

void EGEditText::setIsPasswords(bool isPasswords) {
	mIsPasswords = isPasswords;
}