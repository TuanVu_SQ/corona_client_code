#ifndef __EGSCOREMANAGER_H_INCLUDED__
#define __EGSCOREMANAGER_H_INCLUDED__

#include "cocos2d.h"

using namespace cocos2d;

class EGScoreManager {
public:
	static EGScoreManager* create(const char* package,const char* game,int counter);
	void init(const char* package,const char* game,int counter);
	bool isTopScore(int point);
	void writeTop(const char *name,int point);
	std::string getTopScoreName(int index);
	int getTopScoreVec2(int index);
private:
	std::string package,game;
	int counter;
};

#endif