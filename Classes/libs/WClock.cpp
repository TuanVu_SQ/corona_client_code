#include "WClock.h"

long long _nowTime = 0;
int _timezoneCurrent = 0;

WClock::WClock() {
	_nowTime = getNowTimeMilisecond();
	_time = Time::current_time();
}

void WClock::setTimeZoneCurrent(int timezoneCurrent) {
	_timezoneCurrent = timezoneCurrent;
}

WClock::~WClock() {

}

WClock* WClock::create() {
	return new WClock();
}

void WClock::start() {
	_nowTime = getNowTimeMilisecond();
	_time = Time::current_time();
}

int WClock::getDelay() {
	return (int)(getDelayMilisecond());
}

double WClock::getDelayMilisecond() {
	return _time.elapsed() / 1000.f;
}

long long const & WClock::getNowTimeMilisecond()  {
	sys_time_t t;
	system_time(&t);
	_nowTime = time_to_msec(t);
	return _nowTime;
}

struct tm* WClock::convertIntToTime(int time) {
	time += _timezoneCurrent * 60 * 60L;
	time_t seconds(time);
	return gmtime(&seconds);
}

struct tm* WClock::getTimeNow() {
	time_t t = time(0);
	return localtime(&t);
}

int WClock::getTimeZone() {
	time_t zero = 24 * 60 * 60L;
	struct tm * timeptr;
	int gmtime_hours;

	/* get the local time for Jan 2, 1900 00:00 UTC */
	timeptr = localtime(&zero);
	gmtime_hours = timeptr->tm_hour;

	/* if the local time is the "day before" the UTC, subtract 24 hours
	from the hours to get the UTC offset */
	if (timeptr->tm_mday < 2)
		gmtime_hours -= 24;

	return gmtime_hours;
}
