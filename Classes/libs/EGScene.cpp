#include "EGScene.h"
#include "SimpleAudioEngine.h"
#include "mpiapinfomessagerequest.h"
#include "pthread_mobi.h"
#include "GameInfo.h"
#include "EGJniHelper.h"

EGScene::EGScene() {
	if (!GameInfo::mLayerNotice) {
		GameInfo::mLayerNotice = LayerNotice::createLayer();
		addChild(GameInfo::mLayerNotice);
	}
	else {
		GameInfo::mLayerNotice->retain();
		GameInfo::mLayerNotice->removeFromParentAndCleanup(true);
		addChild(GameInfo::mLayerNotice);
		GameInfo::mLayerNotice->release();
		GameInfo::mLayerNotice->schedule(schedule_selector(LayerNotice::updateNotice));
	}
}

EGScene* EGScene::create() {
	EGScene *pScene = new EGScene();
	pScene->init();
	pScene->autorelease();
	return pScene;
}

EGScene* EGScene::createWithPhysics() {
	EGScene *pScene = new EGScene();
	pScene->initWithPhysics();
	pScene->autorelease();

	return pScene;
}

void EGScene::addLayer(Layer* pLayer) {
	mLayer = pLayer;
	addChild(pLayer);
}

Layer* EGScene::getLayer() {
	return mLayer;
}

void EGScene::registerBackPressed() {
	auto listener = EventListenerKeyboard::create();
	listener->onKeyReleased = CC_CALLBACK_2(EGScene::onKeyReleased, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void EGScene::onKeyReleased(EventKeyboard::KeyCode keycode, Event* event) {
	if (keycode == EventKeyboard::KeyCode::KEY_ESCAPE) {
		onBackPressed();
	}
}

void EGScene::playSound(std::string effect) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(effect.c_str());
}

void EGScene::playSound(std::string effect, std::string pKey) {
	if (UserDefault::getInstance()->getBoolForKey(pKey.c_str(), true)) {
		playSound(effect);
	}
}

void EGScene::showToast(std::string pToast) {
	if (mLstToast.size() != 0)
		mLstToast.push_back(pToast);
	else {
		mLstToast.push_back(pToast);
		showToastReal(pToast);
	}
}
void EGScene::showToast(std::string pToast, float pWidth, Vec2 pPosition) {
	if (mLstToast.size() != 0)
		mLstToast.push_back(pToast);
	else {
		mLstToast.push_back(pToast);
		showToastReal(pToast, pWidth, pPosition);
	}
}

void EGScene::showNextToast() {
	mLstToast.erase(mLstToast.begin());
	if (mLstToast.size() != 0)
		showToastReal(mLstToast.at(0));
}
void EGScene::showToastReal(std::string pToast, float pWidth, Vec2 pPosition) {
	if (mLstToast.size() > 0) {
		LayerColor* pLayer = LayerColor::create(Color4B(0, 0, 0, 200), pWidth, 40);

		this->addChild(pLayer);

		Label* pLbl = Label::createWithBMFont("font_vn.fnt", pToast, TextHAlignment::CENTER, pWidth - 6);

		pLayer->addChild(pLbl);
		Label* pLblTest = Label::createWithBMFont("font_vn.fnt", pToast);
		if (pWidth > pLblTest->getContentSize().width) {
			pWidth = pLblTest->getContentSize().width + 20;
		}
		pLayer->setContentSize(Size(pWidth, pLbl->getContentSize().height + 6));
		pLbl->setPosition(Vec2(pLayer->getContentSize().width / 2, pLayer->getContentSize().height / 2));
		pLayer->setPosition(Vec2(pPosition.x - pLayer->getContentSize().width / 2, pPosition.y - pLayer->getContentSize().height / 2));
		float pXStart = 800 + pLbl->getContentSize().width / 2;
		float pXEnd = -pLbl->getContentSize().width / 2;
		float pDistances = pXStart - pXEnd;
		mLstToast.erase(mLstToast.begin() + 0);
		if (mLstToast.size() > 0) {
			pLbl->runAction(Sequence::createWithTwoActions(DelayTime::create(4), CallFunc::create([=] {
				showToastReal(mLstToast.at(0), pWidth, pPosition);
				pLbl->removeFromParentAndCleanup(true);
				pLayer->removeFromParentAndCleanup(true);
			})));
		}
		else {
			pLbl->runAction(Sequence::createWithTwoActions(DelayTime::create(4), CallFunc::create([=] {
				pLbl->removeFromParentAndCleanup(true);
				pLayer->removeFromParentAndCleanup(true);
			})));
		}
	}
}
void EGScene::showToastReal(std::string pToast, float pWidth, Vec2 pPosition, float pDuration) {

	LayerColor* pLayer = LayerColor::create(Color4B(0, 0, 0, 200), pWidth, 40);
	pLayer->setTag(999);
	this->addChild(pLayer);

	Label* pLbl = Label::createWithBMFont("font_vn.fnt", pToast, TextHAlignment::CENTER, pWidth - 6);

	pLayer->addChild(pLbl);
	Label* pLblTest = Label::createWithBMFont("font_vn.fnt", pToast);
	if (pWidth > pLblTest->getContentSize().width) {
		pWidth = pLblTest->getContentSize().width + 20;
	}
	pLayer->setContentSize(Size(pWidth, pLbl->getContentSize().height + 6));
	pLbl->setPosition(Vec2(pLayer->getContentSize().width / 2, pLayer->getContentSize().height / 2));
	pLayer->setPosition(Vec2(pPosition.x - pLayer->getContentSize().width / 2, pPosition.y - pLayer->getContentSize().height / 2));
	float pXStart = 800 + pLbl->getContentSize().width / 2;
	float pXEnd = -pLbl->getContentSize().width / 2;
	float pDistances = pXStart - pXEnd;

}
void EGScene::showToastReal(std::string pToast) {
	LayerColor* pLayer = LayerColor::create(Color4B(0, 0, 0, 200), 800, 40);
	pLayer->setPosition(Vec2(0, 480 - 40));
	this->addChild(pLayer);

	Label* pLbl = Label::createWithBMFont("font_vn.fnt", pToast);
	pLbl->setPosition(Vec2(800 + pLbl->getContentSize().width / 2, 20));
	pLayer->addChild(pLbl);

	float pXStart = 800 + pLbl->getContentSize().width / 2;
	float pXEnd = -pLbl->getContentSize().width / 2;
	float pDistances = pXStart - pXEnd;
	pLbl->runAction(Sequence::createWithTwoActions(MoveTo::create(pDistances / 100, Vec2(pXEnd, pLbl->getPositionY())), CallFunc::create([=] {
		showNextToast();
		pLbl->removeFromParentAndCleanup(true);
		pLayer->removeFromParentAndCleanup(true);
	})));
}

void EGScene::sendInfomationIAP(std::string productCode, std::string itemCode, std::string userName, std::string deviceID) {
	MpIAPInfoMessageRequest *pRequest = new MpIAPInfoMessageRequest;
	pRequest->setOS(GameInfo::os_prefix);
	pRequest->setUsername(userName);
	pRequest->setDeviceID(deviceID);
	pRequest->setProductCode(productCode);
	pRequest->setItemCode(itemCode);
	MpClientManager::getInstance()->sendMessage(pRequest);
}