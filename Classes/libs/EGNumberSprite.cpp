#include "EGNumberSprite.h"

EGNumberSprite* EGNumberSprite::create(std::string fileName) {
	EGNumberSprite* pEGNumberSprite = new EGNumberSprite();
	pEGNumberSprite->init(fileName);
	pEGNumberSprite->autorelease();

	return pEGNumberSprite;
}

EGNumberSprite* EGNumberSprite::createWithSpriteFrameName(std::string fileName) {
	EGNumberSprite* pEGNumberSprite = new EGNumberSprite();
	pEGNumberSprite->initWithSpriteFrameName(fileName);
	pEGNumberSprite->autorelease();

	return pEGNumberSprite;
}

bool EGNumberSprite::init(std::string fileName) {
	mFileName = fileName;
	mNumber = 0;
	mOpacity = 255;

	mMinLenght = 1;
	mDistanceSpace = 0;
	mType = 0;

	setNumber(0);

	return true;
}
bool EGNumberSprite::initWithSpriteFrameName(std::string fileName) {
	mFileName = fileName;
	mNumber = 0;
	mOpacity = 255;

	mMinLenght = 1;
	mDistanceSpace = 0;
	mType = 1;

	setNumber(0);

	return true;
}

void EGNumberSprite::addNumber(int pNumber) {
	setNumber(mNumber + pNumber);
}
void EGNumberSprite::setNumber(int pNumber,bool iDot) {
	mNumber = pNumber;
	if(mNumber < 0) {
		mNumber = 0;
	}
	std::stringstream ss;
	ss<<mNumber;
	std::string strNumber = ss.str();
	if (iDot){
		strNumber = EGSupport::addDotMoney(strNumber);
	}
	if (strNumber.length() < mMinLenght) {
		int moreNumber = strNumber.length() % mMinLenght;
		for (int i = 0; i < moreNumber; i++) {
			strNumber = "0" + strNumber;
		}
	}

	for(int i = mListNumber.size() - 1;i>=0;i--) {
		Sprite* pNumberSprite = dynamic_cast<Sprite*>(mListNumber.at(i));
		pNumberSprite->stopAllActions();
		pNumberSprite->removeFromParentAndCleanup(true);
	}
	mListNumber.clear();

	mWidth = 0;
	mHeight = 0;

	for(int i = 0;i<strNumber.length();i++) {
		std::string strNumSub = strNumber.substr(i, 1);
		if (strNumSub == "."){
			strNumSub = "10";
		}
		std::string strNumberFile = mFileName + strNumSub + ".png";

		Sprite* pNumberSprite;
		if (mType == 0) {
			pNumberSprite = Sprite::createWithSpriteFrameName(strNumberFile.c_str());
		}
		else {
			pNumberSprite = Sprite::createWithSpriteFrameName(strNumberFile.c_str());
		}
		pNumberSprite->setOpacity(mOpacity);
		pNumberSprite->setColor(this->getColor());
		this->addChild(pNumberSprite);
		if(mListNumber.size() != 0) {
			Sprite *preNumberSprite = dynamic_cast<Sprite*>(mListNumber.at(i-1));
			pNumberSprite->setPosition(ccp(preNumberSprite->getPositionX() + mDistanceSpace +
				preNumberSprite->getContentSize().width / 2 + pNumberSprite->getContentSize().width / 2, preNumberSprite->getPositionY()));
			mWidth += pNumberSprite->getContentSize().width + mDistanceSpace;
		} else {
			pNumberSprite->setPosition(Vec2(pNumberSprite->getContentSize().width / 2, pNumberSprite->getContentSize().height / 2));
			mWidth += pNumberSprite->getContentSize().width;
		}
		mListNumber.push_back(pNumberSprite);
		if(pNumberSprite->getContentSize().height > mHeight) {
			mHeight = pNumberSprite->getContentSize().height;
		}
	}
}
void EGNumberSprite::setNumber(int pNumber) {
	setNumber(pNumber, false);
}

void EGNumberSprite::update(float dt) {
	Sprite* pNumberSprite = dynamic_cast<Sprite*>(mListNumber.at(0));
	if (pNumberSprite) {
		mOpacity = pNumberSprite->getOpacity();
	}
}

void EGNumberSprite::setOpacity(GLbyte pOpacity) {
	mOpacity = pOpacity;
	for (int i = mListNumber.size() - 1; i >= 0; i--) {
		Sprite* pNumberSprite = dynamic_cast<Sprite*>(mListNumber.at(i));
		pNumberSprite->setOpacity(mOpacity);
	}
}

void EGNumberSprite::setOpacity_Values(GLbyte pOpacity) {
	mOpacity = pOpacity;
}

GLbyte EGNumberSprite::getOpacity() {
	return mOpacity;
}

void EGNumberSprite::stopAllActions() {
	for (int i = mListNumber.size() - 1; i >= 0; i--) {
		Sprite* pNumberSprite = mListNumber.at(i);
		pNumberSprite->stopAllActions();
	}
	Node::stopAllActions();
}

void EGNumberSprite::runAction(ActionInterval* pActionInterval) {
	for (int i = mListNumber.size() - 1; i >= 0; i--) {
		ActionInterval* pActionCopy = pActionInterval->clone();
		Sprite* pNumberSprite = mListNumber.at(i);
		pNumberSprite->runAction(pActionCopy);
	}
}

void EGNumberSprite::setPosition(Vec2 ccp) {
	ccp.x -= mWidth/2;
	ccp.y -= mHeight/2;
	Node::setPosition(ccp);
}

void EGNumberSprite::setPositionX(float pX) {
	pX -= mWidth / 2;
	Node::setPositionX(pX);
}

void EGNumberSprite::setPositionY(float pY) {
	pY -= mHeight / 2;
	Node::setPositionY(pY);
}

void EGNumberSprite::setColor(Color3B color3B) {
	for (int i = mListNumber.size() - 1; i >= 0; i--) {
		Sprite* pNumber = mListNumber.at(i);
		pNumber->setColor(color3B);
	}
	Node::setColor(color3B);
}

void EGNumberSprite::setColor_Values(Color3B color3B) {
	Node::setColor(color3B);
}

void EGNumberSprite::setFileName(std::string pFileName) {
	if (mFileName != pFileName) {
		mFileName = pFileName;
	}
}

int EGNumberSprite::getValues() {
	return mNumber;
}

Size EGNumberSprite::getContentSize() {
	return CCSizeMake(mWidth,mHeight);
}