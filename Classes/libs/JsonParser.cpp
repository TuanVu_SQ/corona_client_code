#include "JsonParser.h"

JsonParser* JsonParser::getInstance()
{
	static JsonParser* sg_ptr = nullptr;
	if (nullptr == sg_ptr)
	{
		sg_ptr = new JsonParser;
	}
	return sg_ptr;
}

bool JsonParser::parse(unsigned char *buffer, long length)
{
	bool result = false;
	std::string js((const char*)buffer, length);
	doc.Parse<0>(js.c_str());
	if (!doc.HasParseError())
	{
		result = true;
	}
	return result;
}

void JsonParser::clearCache()
{
	doc.SetNull();
}

bool JsonParser::parseJsonFile(const std::string& pFile)
{
	auto content = FileUtils::getInstance()->getDataFromFile(pFile);
	bool result = parse(content.getBytes(), content.getSize());
	return result;
}
std::vector<std::vector<Vec2>> JsonParser::bodyFormJson(cocos2d::Node *pNode, std::string name)
{
	return bodyFormJson(pNode->getContentSize(),pNode->getAnchorPoint(), 1, name);
}
std::vector<std::vector<Vec2>> JsonParser::bodyFormJson(Size pSize,Vec2 pAnChorPoint,float pScale, std::string name)
{
	std::vector<std::vector<Vec2>> pVec;
	rapidjson::Value &bodies = doc["rigidBodies"];
	if (bodies.IsArray())
	{
		for (int i = 0; i < (int)bodies.Size(); ++i)
		{
			std::string ptemp = bodies[i]["name"].GetString();
			if (!ptemp.compare(name))
			{
				rapidjson::Value &bd = bodies[i];
				if (bd.IsObject())
				{
					float width = pSize.width*pScale;
					float offx = -pAnChorPoint.x*pSize.width*pScale;
					float offy = -pAnChorPoint.y*pSize.height*pScale;

					Point origin(bd["origin"]["x"].GetDouble(), bd["origin"]["y"].GetDouble());
					rapidjson::Value &polygons = bd["polygons"];
					for (int i = 0; i < (int)polygons.Size(); ++i)
					{
						int pcount = polygons[i].Size();
						std::vector<Vec2> pVectorPoint;
						for (int pi = 0; pi < pcount; ++pi)
						{
							float pX = offx + width * polygons[i][pcount - 1 - pi]["x"].GetDouble();
							float pY = offy + width * polygons[i][pcount - 1 - pi]["y"].GetDouble();
							Vec2 pPoint = Vec2(pX, pY);
							pVectorPoint.push_back(pPoint);
						}
						pVec.push_back(pVectorPoint);
					}
				}
				else
				{
					CCLOG("body: %s not found!", name.c_str());
				}
				break;
			}
		}
	}
	return pVec;
}
std::vector<Vec2> JsonParser::bodyLineFormJson(Size pSize, Vec2 pAnChorPoint, std::string name)
{
	std::vector<Vec2> pVec;
	rapidjson::Value &bodies = doc["rigidBodies"];
	if (bodies.IsArray())
	{
		for (int i = 0; i < (int)bodies.Size(); ++i)
		{
			std::string ptemp = bodies[i]["name"].GetString();
			if (!ptemp.compare(name))
			{
				rapidjson::Value &bd = bodies[i];
				if (bd.IsObject())
				{
					float width = pSize.width;
					float offx = -pAnChorPoint.x*pSize.width;
					float offy = -pAnChorPoint.y*pSize.height;

					Point origin(bd["origin"]["x"].GetDouble(), bd["origin"]["y"].GetDouble());
					int a = 0;
					rapidjson::Value &polygons = bd["shapes"][a]["vertices"];
					int pSize = (int)polygons.Size();
					/*for (int i = 0; i < (int)polygons.Size(); ++i)
					{*/
						int pcount = pSize;
						for (int pi = 0; pi < pcount; ++pi)
						{
							float pX = offx + width * polygons[pi]["x"].GetDouble();
							float pY = offy + width * polygons[pi]["y"].GetDouble();
							pVec.push_back(Vec2(pX, pY));
						}
						return pVec;
				}
				else
				{
					CCLOG("body: %s not found!", name.c_str());
				}
				//insertCircle(body, Vec2(0, 0), 0);
				break;
			}
		}
	}
	return pVec;
}
int b = 0;
std::vector<Vec2> JsonParser::insertCircle(std::vector<Vec2> pVec, Vec2 pPointCircle, float pRadius){
	b++;
	if (b == 3){
		int a = 0;
	}
	float pPixel = 10;
	float pRotation = (pPixel * 180 / M_PI) / pRadius;
	std::vector<Vec2> pVecPointIntersect;
	std::vector<int> pVecIndexIntersect;
	for (int i = 360; i >= 0; i -= pRotation){
		Vec2 pPointA = EGSupport::getPointInCircle(pPointCircle.x, pPointCircle.y, pRadius, i);
		Vec2 pPointB = EGSupport::getPointInCircle(pPointCircle.x, pPointCircle.y, pRadius, i-pRotation);
		for (int j = pVec.size() - 1; j >= 1; j--){
			Vec2 pPointC =pVec.at(j);
			Vec2 pPointD =pVec.at(j-1);
			if (ccpSegmentIntersect(pPointA, pPointB, pPointC, pPointD)){
				Vec2 pPointInsec = ccpIntersectPoint(pPointA, pPointB, pPointC, pPointD);
				pVecPointIntersect.push_back(pPointInsec);
				pVec.emplace(pVec.begin()+ j , pPointInsec);
				pVecIndexIntersect.push_back(j);
			}
		}
	}
	for (int i = pVecIndexIntersect.size() -1; i >=0; i -= 2){
		int index = pVecIndexIntersect.at(i);
		int indexNext = pVecIndexIntersect.at(i-1);
		for (int j = index - 1; j > indexNext; j--){
			pVec.erase(pVec.begin() + j);
		}
	}
	int pCount = 0;
	for (int i = 0; i < pVecIndexIntersect.size(); i += 2){
		int index = i ;
		Vec2 pPointA = pVecPointIntersect.at(index);
		Vec2 pPointB = pVecPointIntersect.at(index + 1);
		float pRotationA = EGSupport::getDirectionByRotate(pPointCircle.x, pPointCircle.y, pPointA.x, pPointA.y);
		float pRotationB = EGSupport::getDirectionByRotate(pPointCircle.x, pPointCircle.y, pPointB.x, pPointB.y);
		if (pRotationA<0){
			pRotationA = 360 + pRotationA;
		}
		if (pRotationB<0){
			pRotationB = 360 + pRotationB;
		}
		float pLimitRotation = fabs(pRotationA - pRotationB);
		int abc = pVecIndexIntersect.at(i) + 1;
		for (int j = pRotationA- pRotation; j >pRotationB; j -= pRotation){			
			Vec2 pPointCircleA = EGSupport::getPointInCircle(pPointCircle.x, pPointCircle.y, pRadius, j);
			pVec.insert(pVec.begin() + abc, pPointCircleA);
			abc++;
		}
	}
	return pVec;
}
Vector<PhysicsShape*> JsonParser::cutPolygon(std::vector<Vec2> pArrayPoint){
	Vector<PhysicsShape*> mVecShape;
	for (int i = pArrayPoint.size() - 1; i >= 1; i--){
		std::vector<Vec2> pArrayPoint1 ;
		for (int j = i ; j >= 0; j--){
			Vec2 pPoint = pArrayPoint.at(j);
			pArrayPoint1.push_back(pPoint);
		}
		bool pBool = checkDagiacLoi(pArrayPoint1);
		if (pBool){
			Vec2* points = new Vec2[i];
			for (int g = i ; g >= 0; g--){
				if (g > 0 && g < i){
					pArrayPoint.erase(pArrayPoint.begin() + g);
				}
				points[g].x = pArrayPoint.at(g).x;
				points[g].y = pArrayPoint.at(g).y;
			}
			mVecShape.pushBack(PhysicsShapePolygon::create(points, i, PHYSICSBODY_MATERIAL_DEFAULT));
			i = pArrayPoint.size();
		}
	}
	return mVecShape;
}
bool JsonParser::checkDagiacLoi(std::vector<Vec2> pArrayPoint){
	if (pArrayPoint.size() == 3){
		return true;
	}
	if (pArrayPoint.size() > 3){
		for (int i = pArrayPoint.size() - 1; i >= 0; i--){
			int pCountNode = pArrayPoint.size() - 1;
			Point* pArrayPoint1 = new Point[2];
			int index = 0;
			int indexOut = i - 1;
			if (indexOut < 0){
				indexOut = pArrayPoint.size() - 1;
			}
		/*	for (int j = pArrayPoint.size() - 1; j >= 0; j--){			
				if (j != indexOut){
					Vec2 pPointIndex = pArrayPoint.at(j);
					pArrayPoint1[index] = Vec2(pPointIndex.x, pPointIndex.y);
					index++;
				}			
			}*/
			pArrayPoint1[0] = Vec2(0, 0);
			pArrayPoint1[1] = Vec2(0, 3);
			/*pArrayPoint1[2] = Vec2(3, 3);
			pArrayPoint1[3] = Vec2(4, 2);
			pArrayPoint1[4] = Vec2(3, 0);*/
		
			PhysicsShapePolygon *pShape = PhysicsShapePolygon::create(pArrayPoint1, 2, PHYSICSBODY_MATERIAL_DEFAULT);
			//PhysicsShapeEdgePolygon::create(pArrayPoint1, 5, PHYSICSBODY_MATERIAL_DEFAULT);
			Vec2 pPointOut = pArrayPoint.at(indexOut);
			if (pShape->containsPoint(pPointOut)){
				return false;
			}
		}
	}
	else{
		return false;
	}
	return true;
}