#pragma once

#include "WConfig.h"

class WSupport {
public:
	static Animate* createAnimateFrom(const char* lnk, int start, int end, float delay = 0.1f, bool showlog = true);
	static bool checkTextureCache(const char* link, bool showlog = true);
	static bool checkTextureFile(const char* link, bool showlog = true);
	static bool equalVec2(Vec2 a, Vec2 b);
	static bool equalRect(cocos2d::Rect a, cocos2d::Rect b);
	static bool isSpecialCharacterInString(std::string textInput, bool isNumber = true, std::string textBreak = "");
	static std::string convertIntToString(uint32_t i);
	static std::string convertMoneyAndAddDot(uint32_t i);
	static std::string convertIntToTime(int i);
	static std::string convertMoneyAndAddText(uint32_t i);
	static int convertStringToInt(std::string text);
	static float getDirectionByRotate(Vec2 first, Vec2 last);
	static float getDirectionByRotate(float startX, float startY, float endX, float endY);
	static Vec2 getPointInCircle(Vec2 center, float pRadius, float pAngle);
	static Vec2 getPointInCircle(float centerX, float centerY, float radius, float angle);
	static float countDistance(Vec2 vec2Start, Vec2 vec2End);
	static float countDistance(float fStartX, float fStartY, float fEndX, float fEndY);
	static std::vector<int> convertStringtoVectorInt(std::string);
	static std::string convertVectorIntToString(std::vector<uint8_t>);
	static std::string addSpaceInStdString(std::string origin, int maxLength);
};

class WCover {
public:
	virtual std::vector<cocos2d::Rect> getRect() = 0;
	virtual cocos2d::Rect getOnlyRect() = 0;
	virtual bool intersectsRect(std::vector<cocos2d::Rect> rect);
	virtual bool intersectsRect(cocos2d::Rect rect);
};