// Created by LiemLT - MobiPlus Inc.

#ifndef __WXmlReader_H_INCLUDED__
#define __WXmlReader_H_INCLUDED__

#include <iostream>
#include "extensions\cocos-ext.h"
#include "tinyxml2\tinyxml2.h"

using namespace std;
using namespace tinyxml2;

class WXmlReader {
public:
    static WXmlReader* create(){
		WXmlReader* xml = new WXmlReader();
		return xml;	
	}
    
	void load(const char* pUrl) {
		const char* mUrl = pUrl;

		bool hasError = doc.LoadFile(mUrl);

		if (!hasError)
		{
			rootNode = doc.FirstChildElement();
		}
		mNodeName = "";
		mTagName = "";
	}

	void load(const char* pUrl, const char* pNodeName, const char* pTagName) {
		load(pUrl);
		mNodeName = pNodeName;
		mTagName = pTagName;
	}

	const char* getNodeTextByTagName(const char* pNodeName, const char* pTagName, const char* pTagValue) {
		if(rootNode!=NULL){
			tinyxml2::XMLElement* child = rootNode->FirstChildElement(pNodeName);
			while(child!=NULL)
			{
				const char *pTextName = child->Attribute(pTagName);
        
				if(pTextName!=NULL) {
					if(strcmp (pTextName,pTagValue) == 0) {
						return child->GetText();
					}
				}
        
				child = child->NextSiblingElement();
			}
		}
		return NULL;
	}

	const char* getNodeTextByTagName(const char* pTagValues) {
		return getNodeTextByTagName(mNodeName, mTagName, pTagValues);
	}

private:
    tinyxml2::XMLDocument doc;
    tinyxml2::XMLElement* rootNode;

protected:
	const char* mNodeName, *mTagName;
    string getPathFileIOS();
};

#endif