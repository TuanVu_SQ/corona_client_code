#include "EGSliderConfig.h"

EGSliderConfig* EGSliderConfig::create(std::string fileSliderBar, std::string fileSlider, std::string fileSliderController) {
	EGSliderConfig* pEGSliderConfig = new EGSliderConfig();
	pEGSliderConfig->init(fileSliderBar, fileSlider, fileSliderController);
	pEGSliderConfig->autorelease();

	return pEGSliderConfig;
}

bool EGSliderConfig::init(std::string fileSliderBar, std::string fileSlider, std::string fileSliderController) {
	EGSprite::initWithFile(fileSliderBar);

	isPushingController = false;
	mActionMovingFunc = NULL;
	mDoneFunc = NULL;

	mSlider = ProgressTimer::create(Sprite::createWithSpriteFrameName(fileSlider.c_str()));
	mSlider->setBarChangeRate(Vec2(1, 0));
	mSlider->setMidpoint(Vec2(0, 0.5f));
	mSlider->setType(ProgressTimer::Type::BAR);
	mSlider->setPercentage(100);
	mSlider->setPosition(Vec2(this->getContentSize().width/2, this->getContentSize().height/2));
	this->addChild(mSlider);

	registerTouch();

	mController = EGButtonSprite::createWithSpriteFrameName(fileSliderController.c_str());
	mController->unregisterTouch();
	mController->registerTouch(false);
	mController->setOnTouch(CC_CALLBACK_0(EGSliderConfig::onPushController, this));
	mController->setOnUp(CC_CALLBACK_0(EGSliderConfig::onUpController, this));
	mController->setPosition(Vec2(this->getContentSize().width, this->getContentSize().height/2));
	this->addChild(mController);

	return true;
}

void EGSliderConfig::onPushController() {
	isPushingController = true;
}

void EGSliderConfig::onUpController() {
	isPushingController = false;
}

bool EGSliderConfig::ccTouchBegan(Touch* touch, Event* event) {
	return isPushingController;
}

void EGSliderConfig::ccTouchMoved(Touch* touch, Event* event) {
	float newPointXController = touch->getLocation().x - this->getContentSize().width / 2 + mController->getContentSize().width / 2;
	if (newPointXController < 0) {
		newPointXController = 0;
	}
	else if (newPointXController > this->getContentSize().width) {
		newPointXController = this->getContentSize().width;
	}
	mSlider->setPercentage(newPointXController / this->getContentSize().width * 100);
	mController->setPositionX(newPointXController);
	if (mActionMovingFunc) {
		mActionMovingFunc->execute();
	}
}

void EGSliderConfig::ccTouchEnded(Touch* touch, Event* event) {
	if (mDoneFunc) {
		mDoneFunc->execute();
	}
}

void EGSliderConfig::setDoWhenDone(std::function<void()> pSelFunction) {
	mDoneFunc = CallFunc::create(pSelFunction);
	mDoneFunc->retain();
}

void EGSliderConfig::setDoWhenControlling(std::function<void()> pSelFunction) {
	mDoneFunc = CallFunc::create(pSelFunction);
	mActionMovingFunc->retain();
}

void EGSliderConfig::setPercent(float pPercent) {
	float newPointXController = this->getContentSize().width*pPercent/100;
	if (newPointXController < 0) {
		newPointXController = 0;
	}
	else if (newPointXController > this->getContentSize().width) {
		newPointXController = this->getContentSize().width;
	}
	mSlider->setPercentage(newPointXController / this->getContentSize().width * 100);
	mController->setPositionX(newPointXController);
}

float EGSliderConfig::getPercent() {
	return mSlider->getPercentage();
}