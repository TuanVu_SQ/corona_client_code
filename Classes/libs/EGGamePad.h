#ifndef __EGGAMEPAD_H_INCLUDED__
#define __EGGAMEPAD_H_INCLUDED__

#include "cocos2d.h"
#include "EGGamePadListener.h"

using namespace cocos2d;

typedef enum {
	EGGamePadNormal,
	EGGamePad2WayOnly,
	EGGamePad2WayOnlyStatic
} EGGamePadType;

class EGGamePad :public Node {
public:
	static EGGamePad* createWithFile(const char* fileNameBorder, const char* fileNameOracle);
	static EGGamePad* createWithFile(const char* fileNameBorder, const char* fileNameOracle, float pSensitivity);
	static EGGamePad* createWithTexture(Texture2D* texture2DBorder, Texture2D* texture2DOracle);
	static EGGamePad* createWithTexture(Texture2D* texture2DBorder, Texture2D* texture2DOracle, float pSensitivity);
	static EGGamePad* createWithSpriteFrameName(const char* fileNameBorder, const char* fileNameOracle);
	static EGGamePad* createWithSpriteFrameName(const char* fileNameBorder, const char* fileNameOracle, float pSensitivity);
	static EGGamePad* createWithSpriteFrame(SpriteFrame* spriteFrameBorder, SpriteFrame* spriteFrameOracle);
	static EGGamePad* createWithSpriteFrame(SpriteFrame* spriteFrameBorder, SpriteFrame* spriteFrameOracle, float pSensitivity);

	void setGamepadType(EGGamePadType pEGGamePadType);
	void setGamepadListener(EGGamePadListener *pGamepadListener);
	void setGameRelolution(float pWidth, float pHeight);
private:
	Sprite* mCircleBorder, *mOracle;
	EGGamePadListener *mGamePadListener;
	EGGamePadType mGamePadType;
	float mWidthhGame, mHeightGame;
	int mTouchMovementID, mTouchActionID;
	bool isTouchingMovement, isTouchingAction;

	void init(float pSensitivity);
	bool initWithFile(const char* fileNameBorder, const char* fileNameOracle, float pSensitivity);
	bool initWithTexture(Texture2D* texture2DBorder, Texture2D* texture2DOracle, float pSensitivity);
	bool initWithSpriteFrameName(const char* fileNameBorder, const char* fileNameOracle, float pSensitivity);
	bool initWithSpriteFrame(SpriteFrame* spriteFrameBorder, SpriteFrame* spriteFrameOracle, float pSensitivity);
	void update(float dt);

	virtual void onTouchesBegan(const std::vector<Touch*>& touches, cocos2d::Event  *event);
	virtual void onTouchesMoved(const std::vector<Touch*>& touches, cocos2d::Event  *event);
	virtual void onTouchesEnded(const std::vector<Touch*>& touches, cocos2d::Event  *event);
	virtual void onTouchesCancelled(const std::vector<Touch*>& touches, cocos2d::Event  *event);
};

#endif