#ifndef __EGSCENE_H_INCLUDED__
#define __EGSCENE_H_INCLUDED__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "common/GameInfo.h"
#include "MpClientManager.h"
#include "MpManager.h"

using namespace cocos2d;

class EGScene :public Scene {
public:
	EGScene();
	static EGScene* create();
	static EGScene* createWithPhysics();
	void addLayer(Layer *pLayer);
	Layer* getLayer();
	void registerBackPressed();

	virtual void onBackPressed() {};
	virtual void pauseGame() {};
	virtual void resumeGame() {};
	virtual void playSound(std::string effect);
	virtual void playSound(std::string effect, std::string pKey);

	virtual void onSMSResultCallback(int pType, int pMoney) {};
	virtual void onLoginFacebook(int pResult, std::string pAccessToken) {};

	void showToast(std::string pToast);
	void showToast(std::string pToast, float pWidth, Vec2 pPosition);
	virtual bool isExitGame() { return true; };

	void showToastReal(std::string pToast, float pWidth, Vec2 pPosition, float pDuration);
	void sendInfomationIAP(std::string productCode, std::string itemCode, std::string userName, std::string deviceID);
private:
	CC_SYNTHESIZE(std::string, _errorCode, ErrorCode);
	Layer* mLayer;
	std::vector<std::string> mLstToast;
	void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event);

	void showNextToast();
	void showToastReal(std::string pToast);
	void showToastReal(std::string pToast, float pWidth, Vec2 pPosition);
};

#endif