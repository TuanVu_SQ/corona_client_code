#pragma once

#include "cocos2d.h"

using namespace cocos2d;
using namespace std;

#define RIPPLE_DEFAULT_QUAD_COUNT_X             60
#define RIPPLE_DEFAULT_QUAD_COUNT_Y             40
#define RIPPLE_BASE_GAIN                        0.1f
#define RIPPLE_DEFAULT_RADIUS                   200
#define RIPPLE_DEFAULT_RIPPLE_CYCLE             0.15f
#define RIPPLE_DEFAULT_LIFESPAN                 1.4f
#define RIPPLE_CHILD_MODIFIER                   0.25f

typedef enum {
	RIPPLE_TYPE_RUBBER,
	RIPPLE_TYPE_GEL,
	RIPPLE_TYPE_WATER,
} RIPPLE_TYPE;

typedef enum {
	RIPPLE_CHILD_LEFT,
	RIPPLE_CHILD_TOP,
	RIPPLE_CHILD_RIGHT,
	RIPPLE_CHILD_BOTTOM,
	RIPPLE_CHILD_COUNT
} RIPPLE_CHILD;

typedef struct {
	bool                    parent;
	bool                    childCreated[4];
	RIPPLE_TYPE             rippleType;
	Vec2                    center;
	Vec2                    centerCoordinate;
	float                   radius;
	float                   strength;
	float                   runtime;
	float                   currentRadius;
	float                   rippleCycle;
	float                   lifespan;
} RIPPLE_DATA;

class RippleSprite : public Sprite {
public:
	static RippleSprite* create(std::string);
	static RippleSprite* create(Node*);
	void setPosition(Vec2 pos);
	Vec2 getPosition();
	void autoRipple();
	cocos2d::Size getContentSize();
	void addRipple(Vec2 pos, RIPPLE_TYPE type, float strength);
private:
	bool initWithFile(std::string);
	bool initWithScene(Node*);

	void tesselate();
	void update(float);
	
	void addRippleChild(RIPPLE_DATA* parent, RIPPLE_CHILD type);
	bool isPointInsideSprite(Vec2 pos);
	bool isTouchInsideSprite(Touch* pTouch);
	virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags) override;
	void onDraw(const Mat4 &transform, uint32_t flags);

private:
	Texture2D* createWithRenderTexture(std::string);
	Texture2D* createWithRenderTexture(Node*);

private:
	virtual bool ccTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void ccTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void ccTouchEnded(Touch *pTouch, Event *pEvent);

private:
	CustomCommand _customCommand;
	Node* _parent;
	//ripple
	Texture2D*              m_texture;
	int                     m_quadCountX;
	int                     m_quadCountY;
	int                     m_VerticesPrStrip;
	int                     m_bufferSize;
	Vec2*					m_vertice;
	Vec2*					m_textureCoordinate;
	Vec2*					m_rippleCoordinate;
	bool*                   m_edgeVertice;
	std::vector<RIPPLE_DATA*>    m_rippleList;

private:
	cocos2d::Size mSize;
	float scaleRTT;
	float runTime;
	std::chrono::time_point<std::chrono::high_resolution_clock> _timer;

private:
	RippleSprite();
	~RippleSprite();
};