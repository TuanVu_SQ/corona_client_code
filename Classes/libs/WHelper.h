#pragma once

#include "cocos2d.h"

using namespace cocos2d;
using namespace std;

typedef enum JniCallback {
	DEVICESID = 0,
	CPID,
	COUNTRY,
	PACKAGE,
	TELCO,
	CHARGE_ID,
	PHONE_NUMBER,
	HK,
	HM,
	HC,
	PK,
	TP,
	DEVICENAME
} JniCallback_s;

class WHelper {
public:
	static WHelper* getInstance();
	void loginFacebook();
	void responeDataToLoginFacebook(std::string facebookId, std::string tokenID);
	std::function<void(std::string facebookId, std::string tokenID)> _facebookCallback;

public:
	std::string getDeviceID();
	std::string getCountry();
	std::string getPackage();
	std::string getTelco();
	std::string getCPID();
	std::string getChargeID();
	std::string getPhoneNumber();
	std::string getHK();
	std::string getHM();
	std::string getHC();
	std::string getPK();
	std::string getTP();
	std::string getDeviceName();
	void setDeviceID(std::string);
	void setCountry(std::string);
	void setPackage(std::string);
	void setTelco(std::string);
	void setCPID(std::string);
	void setChargeID(std::string);
	void setPhoneNumber(std::string);
	void setHK(std::string);
	void setHM(std::string);
	void setHC(std::string);
	void setPK(std::string);
	void setTP(std::string);

private:
	std::string _deviceId;
	std::string _country;
	std::string _package;
	std::string _telco;
	std::string _cpid;
	std::string _chargeID;
	std::string _phoneNumber;
	std::string _hk;
	std::string _hm;
	std::string _hc;
	std::string _pk;
	std::string _tp;

private:
	WHelper();
	~WHelper();
};

class WDeviceInfo {
public:
	static std::string getDeviceID();
	static std::string getCPID();
	static std::string getCountryCode();
	static std::string getPackageName();
	static std::string getTelco();
	static std::string getChargeID();

public:
	static std::string getSecurityString(std::string tp);
};