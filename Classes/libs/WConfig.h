#pragma once

#include <ctime>
#include <chrono>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdint.h>
#include <stdlib.h>
#include <cstring>
#include <cstdio>
#include <stdio.h>
#include <iomanip>
#include <cctype>
#include <thread>
#include <sys/stat.h>
#include <iterator>
#include <numeric>
#include <ctype.h>
#include <algorithm>
#include <functional>

#define STRINGIFY(A)  #A

#include "cocos2d.h"
USING_NS_CC;

#include "audio/include/SimpleAudioEngine.h"
using namespace CocosDenshion;

#include "extensions/cocos-ext.h"
using namespace extension;

#if (COCOS2D_VERSION >= 0x00030400 && CC_TARGET_PLATFORM != CC_PLATFORM_WP8 && CC_TARGET_PLATFORM != CC_PLATFORM_WINRT)
#include "audio/include/AudioEngine.h"
using namespace cocos2d::experimental;
#endif

using namespace std;

#include "cocos/ui/CocosGUI.h"

#include <tinyxml2/tinyxml2.h>
using namespace tinyxml2;

#include "ui/UIEditBox/UIEditBox.h"

#ifdef _WIN32
#include <winsock2.h>
#include <WS2tcpip.h>
#include <windows.h>
#define SLEEP(x) Sleep(x*1000)
#else
#include <unistd.h>
#define SLEEP(x) sleep(x)
#endif

#define CC_CALLBACK_4(__selector__,__target__, ...) std::bind(&__selector__,__target__, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, ##__VA_ARGS__)
#define CC_CALLBACK_5(__selector__,__target__, ...) std::bind(&__selector__,__target__, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, ##__VA_ARGS__)
