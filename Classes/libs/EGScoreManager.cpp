#include "EGScoreManager.h"

EGScoreManager* EGScoreManager::create(const char* package,const char* game,int counter) {
	EGScoreManager *pEGScoreManager = new EGScoreManager();
	pEGScoreManager->init(package,game,counter);

	return pEGScoreManager;
}

void EGScoreManager::init(const char* ppackage,const char* pgame,int pcounter) {
	package = ppackage;
	game = pgame;
	counter = pcounter;
}

bool EGScoreManager::isTopScore(int point) {
	for(int i = 1;i <=counter;i++) {
		std::string packageStr = package;
		std::string gameStr = game;
		std::stringstream ss;
		ss<<i;
		std::string key = packageStr + gameStr + "score" + ss.str();
		int highScore  = UserDefault::getInstance()->getIntegerForKey(key.c_str(),0);
		if(point > highScore) {
			return true;
		}
	}
	return false;
}

void EGScoreManager::writeTop(const char *name,int point) {
	std::string saveName;
	int savePoint;
	bool savedScore = false;
	for(int i = 1;i <=counter;i++) {
		std::stringstream ss;
		ss<<i;
		std::string keyScore = package + game + "score" + ss.str();
		std::string keyName = package + game + "name" + ss.str();

		int curPoint = UserDefault::getInstance()->getIntegerForKey(keyScore.c_str(),0);

		if(!savedScore && point > curPoint) {
			savePoint = curPoint;
			saveName = UserDefault::getInstance()->getStringForKey(keyName.c_str(),"####");
			UserDefault::getInstance()->setStringForKey(keyName.c_str(),name);
			UserDefault::getInstance()->setIntegerForKey(keyScore.c_str(),point);
			savedScore = true;
		}else if(savedScore) {
			std::string saveName_ = saveName;
			int savePoint_ = savePoint;
			savePoint = curPoint;
			saveName = UserDefault::getInstance()->getStringForKey(keyName.c_str(),"####");
			UserDefault::getInstance()->setStringForKey(keyName.c_str(),saveName_);
			UserDefault::getInstance()->setIntegerForKey(keyScore.c_str(),savePoint_);
		}
	}
}

std::string EGScoreManager::getTopScoreName(int index) {
	std::stringstream ss;
	ss<<index;
	std::string keyName = package + game + "name" + ss.str();
	return UserDefault::getInstance()->getStringForKey(keyName.c_str(),"####");
}

int EGScoreManager::getTopScoreVec2(int index) {
	std::stringstream ss;
	ss<<index;
	std::string keyScore = package + game + "score" + ss.str();

	int point = UserDefault::getInstance()->getIntegerForKey(keyScore.c_str(),0);
	return point;
}