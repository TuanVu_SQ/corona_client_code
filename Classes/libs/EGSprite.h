#ifndef __EGSprite_H_INCLUDED__
#define __EGSprite_H_INCLUDED__

#include "cocos2d.h"
#include "EGPoint.h"

using namespace cocos2d;

class EGSprite:public Sprite {
public:
	static EGSprite* createWithFile(const char* fileName);
	static EGSprite* createWithTexture(Texture2D* texture2D);
	static EGSprite* createWithSpriteFrameName(const char* fileName);
	static EGSprite* createWithSpriteFrame(SpriteFrame* spriteFrame);

	void registerTouch();
	void registerTouch(bool isTouchDown);
	void unregisterTouch();
	bool isRegisteredTouch();

	void setOnTouch(std::function<void()> pTouchFunction);
	void setOnUp(std::function<void()> pUpFunction);

	std::string getFileName();
	void setTexture(const char* fileName);
	void setTexture(Texture2D* pTexture);
	void setTextureByRef(Ref* pRef);

	void performClick();
	void performUp();
	void setVisibleByRef(Ref* pRef);
	void setPosition(const Point& pos);
	void setPositionByRef(Ref* pRef);
	void setBindingRect(bool pBool);

	void setFlipXByRef(Ref* pRef);
	void setFlipYByRef(Ref* pRef);

	void setScale(float pScale);
	void setScaleX(float pScale);
	void setScaleY(float pScale);

	float getPositionX1();
	float getPositionX2();
	float getPositionY1();
	float getPositionY2();
	Point getBasePosition();

	bool colliWith(EGSprite* pEGSprite);
	void setRect(Rect pRect);
	Rect getRect();

	void setAutomaticRotate(bool isAutomatic);

	void performRelease();

	void setTags(int pIndexTag, float pValues);
	float getTags(int pIndexTag);
	void setAlwayReturnFalse(bool pBool){ isAlwayReturnFalse = pBool; };
protected:
	int mType;
	Vector<__Float*> mLstTag;
	bool isAutomaticRotate;
	bool isScaleAllChild,isAlwayReturnFalse;
	bool iBindingRect;
	std::string mFileName;
	bool isTouchRegistered;
	/*CallFunc* mTouchAction, *mUpAction;
	__CCCallFuncO* mTouchActionRef, *mUpActionRef;*/
	std::function<void()> mTouchFunction, mUpFunction;
	Rect mRect;
	
	bool initWithFile(std::string fileName);
	bool initWithTexture(Texture2D* texture2D);
	bool initWithSpriteFrameName(std::string fileName);
	bool initWithSpriteFrame(SpriteFrame* spriteFrame);

	void prepare();

	Rect rect();
	bool containsTouchLocation(Touch* touch);
	virtual bool ccTouchBegan(Touch* touch, Event* event);
};

#endif