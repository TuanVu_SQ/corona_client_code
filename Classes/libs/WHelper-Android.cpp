#include "WHelper-Android.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "android/jni/JniHelper.h"
#endif

WHelperAndroid* WHelperAndroid::getInstance() {
	static WHelperAndroid* helper = nullptr;
	if (helper == nullptr) {
		helper = new WHelperAndroid();
	}
	return helper;
}

// Java native core
//===============================================================================================
//===============================================================================================
//===============================================================================================

std::string WHelperAndroid::getStringToJava(const char *_className, const char* _methodName) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	return _getStringToJava(_className, _methodName);
#endif
	return "";
}

std::string WHelperAndroid::getStringToJava(const char *_className, const char* _methodName, std::string _pString)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	return _getStringToJava(_className, _methodName, _pString);
#endif
	return "";
}

void WHelperAndroid::callFunctionToJava(const char *_className, const char* _methodName)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	_callFunctionToJava(_className, _methodName);
#endif
}

void WHelperAndroid::callFunctionToJava(const char *_className, const char* _methodName, std::string _pString)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	_callFunctionToJava(_className, _methodName, _pString);
#endif
}

void WHelperAndroid::callFunctionToJava(const char *_className, const char* _methodName, bool _pBool)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	_callFunctionToJava(_className, _methodName, _pBool);
#endif
}

void WHelperAndroid::callFunctionToJava(const char *_className, const char* _methodName, int _values1, int _values2)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	_callFunctionToJava(_className, _methodName, _values1, _values2);
#endif
}

void WHelperAndroid::callFunctionToJava(const char *_className, const char* _methodName, std::string _values1, std::string _values2, std::string _values3)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	_callFunctionToJava(_className, _methodName, _values1, _values2, _values3);
#endif
}

const char* WHelperAndroid::_getStringToJava(const char *_className, const char* _methodName) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniMethodInfo methodInfo;
	const char* result;

	if (JniHelper::getStaticMethodInfo(methodInfo, _className, _methodName, "()Ljava/lang/String;")) {
		jstring returnString = (jstring) methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);
		result = methodInfo.env->GetStringUTFChars(returnString, NULL);
	}

	methodInfo.env->DeleteLocalRef(methodInfo.classID);
	return result;
#endif
	return NULL;
}

const char* WHelperAndroid::_getStringToJava(const char *_className, const char* _methodName, std::string _pString) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniMethodInfo methodInfo;
	const char* result;
	if (JniHelper::getStaticMethodInfo(methodInfo, _className, _methodName, "(Ljava/lang/String;)Ljava/lang/String;")) {
		jstring argContent = methodInfo.env->NewStringUTF(_pString.c_str());
		jstring returnString = (jstring)methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID, argContent);
		result = methodInfo.env->GetStringUTFChars(returnString, NULL);
		methodInfo.env->DeleteLocalRef(argContent);
	}
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
	return result;
#endif
	return NULL;
}

void WHelperAndroid::_callFunctionToJava(const char *_className, const char* _methodName) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo, _className, _methodName, "()V")) {
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
	}
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
#endif
}

void WHelperAndroid::_callFunctionToJava(const char *_className, const char* _methodName, std::string _pString) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo, _className, _methodName, "(Ljava/lang/String;)V")) {
		jstring argContent = methodInfo.env->NewStringUTF(_pString.c_str());
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, argContent);

		methodInfo.env->DeleteLocalRef(argContent);
	}
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
#endif
}

void WHelperAndroid::_callFunctionToJava(const char *_className, const char* _methodName, bool _pBool) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo, _className, _methodName, "(Z)V")) {
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, _pBool);
	}
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
#endif
}

void WHelperAndroid::_callFunctionToJava(const char *_className, const char* _methodName, int _values1, int _values2) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo, _className, _methodName, "(II)V")) {
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, _values1, _values2);
	}
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
#endif
}

void WHelperAndroid::_callFunctionToJava(const char *_className, const char* _methodName, std::string _values1, std::string _values2, std::string _values3) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo, _className, _methodName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V")) {
		jstring argContent1 = methodInfo.env->NewStringUTF(_values1.c_str());
		jstring argContent2 = methodInfo.env->NewStringUTF(_values2.c_str());
		jstring argContent3 = methodInfo.env->NewStringUTF(_values3.c_str());
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, argContent1, argContent2, argContent3);
		methodInfo.env->DeleteLocalRef(argContent1);
		methodInfo.env->DeleteLocalRef(argContent2);
		methodInfo.env->DeleteLocalRef(argContent3);
	}
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
#endif
}