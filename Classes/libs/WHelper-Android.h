#pragma once

#include "cocos2d.h"
using namespace cocos2d;

class WHelperAndroid {
public:
	static WHelperAndroid* getInstance();

public:
	static std::string getStringToJava(const char *_className, const char* _methodName);
	static std::string getStringToJava(const char *_className, const char* _methodName, std::string _pString);
	static void callFunctionToJava(const char *_className, const char* _methodName);
	static void callFunctionToJava(const char *_className, const char* _methodName, std::string _pString);
	static void callFunctionToJava(const char *_className, const char* _methodName, bool _pBool);
	static void callFunctionToJava(const char *_className, const char* _methodName, int _values1, int _values2);
	static void callFunctionToJava(const char *_className, const char* _methodName, std::string _values1, std::string _values2, std::string _values3);

private:
	static const char* _getStringToJava(const char *_className, const char* _methodName);
	static const char* _getStringToJava(const char *_className, const char* _methodName, std::string _pString);
	static void _callFunctionToJava(const char *_className, const char* _methodName);
	static void _callFunctionToJava(const char *_className, const char* _methodName, std::string _pString);
	static void _callFunctionToJava(const char *_className, const char* _methodName, bool _pBool);
	static void _callFunctionToJava(const char *_className, const char* _methodName, int _values1, int _values2);
	static void _callFunctionToJava(const char *_className, const char* _methodName, std::string _values1, std::string _values2, std::string _values3);
};