#ifndef _WXMLREADER_H_INCLUDED_
#define _WXMLREADER_H_INCLUDED_

#include <iostream>
#include <fstream>
#include "cocos2d.h"
#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "tinyxml2/tinyxml2.h"
#else
#include "tinyxml2.h"
#endif
#include "GameInfo.h"

using namespace cocos2d;
using namespace std;
using namespace tinyxml2;

class WXmlReader {
public:
	static WXmlReader* create(){
		static WXmlReader* reader = nullptr;
		if (reader == nullptr) {
			reader = new WXmlReader();
		}
		return reader;
	}
	void load(const char* pUrl) {
		const char* mUrl = pUrl;
		bool hasError = false;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		stringstream ss; ss << pUrl;
		string ios = getPathFileIOS() + ss.str();
		mUrl = ios.c_str();
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT) || (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
		stringstream ss; ss << pUrl;
		string winrt = "Assets/Resources/" + ss.str();
		mUrl = winrt.c_str();
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)  || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT) || (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		std::string path = FileUtils::sharedFileUtils()->fullPathForFilename(mUrl);
		unsigned char * charbuffer = FileUtils::sharedFileUtils()->getFileData(path.c_str(), "rb", 0);
		stringstream ss; ss << charbuffer;
		doc.Parse((const char*)charbuffer);
#else
		std::ifstream file(mUrl);
		std::string str;
		std::string file_contents;
		while (std::getline(file, str)) {
			file_contents += str;
			file_contents.push_back('\n');
		}
		doc.Parse(file_contents.c_str());
#endif
#else
		int isError = doc.LoadFile(mUrl);
		if (isError != tinyxml2::XMLError::XML_NO_ERROR) {
			hasError = true;
		}
#endif
		if (!hasError) {
			rootNode = doc.FirstChildElement();
		}
	}
	string getNodeTextByTagName(string pTagValue, string pNodeName = "string", string pTagName = "name") {
		if (ends_with(pTagValue, "256")){
			pTagValue = TEXT_ERR_TIMEOUT;
		}
		if (rootNode != NULL){
			for (tinyxml2::XMLElement *text = rootNode->FirstChildElement(pNodeName.c_str()); text;
				text = text->NextSiblingElement()) {
				if (strcmp(text->Attribute(pTagName.c_str()), pTagValue.c_str()) == 0){
					return text->GetText();
				}
			}
		}
		std::string pReturnErr = getNodeTextByTagName(TEXT_EER_NOTFOUND_EXCEPTION);
		pReturnErr += pTagValue;
		return pReturnErr;
	}
	inline bool ends_with(std::string const & value, std::string const & ending)
	{
		if (ending.size() > value.size()) return false;
		return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
	}

private:
	tinyxml2::XMLDocument doc;
	tinyxml2::XMLElement* rootNode;

protected:
	WXmlReader(){
		rootNode = NULL;
	}
	~WXmlReader(){
		free(rootNode);
	}
	string getPathFileIOS();
};

#endif