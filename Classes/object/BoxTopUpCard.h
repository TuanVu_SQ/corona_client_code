#ifndef __BOXTOPUPCARD_H_INCLUDED__
#define __BOXTOPUPCARD_H_INCLUDED__

#include "cocos2d.h"
#include "EGButtonSprite.h"
#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "extensions\cocos-ext.h"
#else
#include "cocos-ext.h"
#endif

USING_NS_CC;
USING_NS_CC_EXT;

class BoxTopUpCard : public LayerColor {
public:
	static BoxTopUpCard* create(std::function<void(std::string, std::string, std::string, std::string)> pFunctionRechargeByCard);

	void setVisible(bool isVisible);

	EditBox *mEdt_Serial, *mEdt_CardNumber, *mEdt_Username;
protected:
	std::string mTelcoCode;
private:
	Sprite* mBox;
	EGButtonSprite** mBtnTelco;
	Sprite** mCardEnable;
	EGButtonSprite* mBtnRecharge, *mBtnClose;
	std::function<void(std::string pSerial, std::string pNumber, std::string pTelco, std::string)> mFunctionRechargeByCard;

	void initObject(std::function<void(std::string, std::string, std::string, std::string)> pFunctionRechargeByCard);
	void chooseTelco(int pIndex);
	void recharge();
	void close();

	bool touchBegan(Touch* touch, Event* event) {
		return this->isVisible();
	}
};

#endif