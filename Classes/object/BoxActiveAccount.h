#ifndef __BOXACTIVE_H_INCLUDED__
#define __BOXACTIVE_H_INCLUDED__

#include "cocos2d.h"
#include "EGButtonSprite.h"
#include "EGSprite.h"
#include "ClientPlayerInfo.h"
#include "libs/WXmlReader.h"
#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "extensions\cocos-ext.h"
#else
#include "cocos-ext.h"
#endif

USING_NS_CC;
USING_NS_CC_EXT;

class BoxActiveAccount :public EGSprite {
public:
	Label* mLbl_Title3, *mLbl_Title4_2, *mLbl_Des;
	EGButtonSprite* mBtnAccept, *mBtnReject, *mBtnSkip, *mBtnResendOtp, *btnClose;
	RechargeInfo m_StructInfo;
	WXmlReader* pXml;


public:
	EditBox *edtInput;

	static BoxActiveAccount* create();
	void showDescription(std::string content);
	void successActive();

private:
	void initObject();

private:
	CC_SYNTHESIZE(std::function<void(std::string)>, _funcSendOtp, FuncSendOTP);
	CC_SYNTHESIZE(std::function<void()>, _funcResendOtp, FuncResendOTP);

private:
	void btnAcceptClicked();
	void btnResendClicked();


	virtual bool ccTouchBegan(Touch* touch, Event* event) {
		return this->isVisible();
	}
};

#endif