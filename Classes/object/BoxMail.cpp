#include "BoxMail.h"
#include "EGSupport.h"

BoxMail* BoxMail::createBoxMail(){
	BoxMail* box = new BoxMail();
	box->initBoxMail();
	box->autorelease();
	return box;
}

void BoxMail::initBoxMail(){
	
	LayerColor::initWithColor(Color4B(0, 0, 0, 220));

	background = EGSprite::createWithFile("background_write_mail.png");
	background->setPosition(Vec2(CLIENT_WIDTH/2, CLIENT_HEIGHT/2 + 30));
	this->addChild(background, 1);

	_xmlReader = WXmlReader::create();
	_xmlReader->load(KEY_XML_FILE);

	_editTitle = NULL;
	_editContent = NULL;

	auto sprContent = EGSprite::createWithFile("edt_mail_big.png");
	sprContent->setPosition(Vec2(background->getContentSize().width / 2, sprContent->getContentSize().height / 2 + 40));
	background->addChild(sprContent, 1);

	_lbContent = Label::createWithBMFont("font.fnt", "Noi dung");
	_lbContent->setAnchorPoint(Vec2(0, 1));
	_lbContent->setScale(0.4f);
	_lbContent->setPosition(Vec2(10, sprContent->getContentSize().height - 10));
	_lbContent->setWidth(sprContent->getContentSize().width - 20);
	sprContent->addChild(_lbContent);

	_editContent = EditBox::create(Size(633, 124), cocos2d::ui::Scale9Sprite::create("edt_mail_big1.png"));
	_editContent->setInputFlag(EditBox::InputFlag::SENSITIVE);
	_editContent->setInputMode(EditBox::InputMode::SINGLE_LINE);
	_editContent->setPosition(sprContent->getPosition());
	_editContent->setFont("fonts/arial.ttf", 18);	
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	_editContent->setDelegate(this);
#endif
	_editContent->setMaxLength(250);
	_editContent->setTag(1);
	_editContent->setFontColor(Color3B::WHITE);
	_editContent->setPlaceholderFontColor(Color3B(125, 125, 125));
	//_editContent->setInputMode(EditBox::InputMode::ANY);
	background->addChild(_editContent, -1);

	auto sprTitle = EGSprite::createWithFile("edt_mail_small.png");
	sprTitle->setPosition(Vec2(_editContent->getPosition()) + Vec2(0, sprContent->getContentSize().height / 2 + sprTitle->getContentSize().height - 15));
	background->addChild(sprTitle, 1);

	auto lbTitle_l = Label::createWithBMFont("font.fnt", _xmlReader->getNodeTextByTagName("txt_mail_title"));
	lbTitle_l->setAnchorPoint(Vec2(0, 0.5f));
	lbTitle_l->setColor(Color3B::YELLOW);
	lbTitle_l->setScale(0.4f);
	lbTitle_l->setPosition(Vec2(10, sprTitle->getContentSize().height/2));
	sprTitle->addChild(lbTitle_l);

	_lbTitleMail = Label::createWithBMFont("font.fnt", "abc...");
	_lbTitleMail->setAnchorPoint(Vec2(0, 0.5f));
	_lbTitleMail->setScale(0.4f);
	_lbTitleMail->setPosition(Vec2(lbTitle_l->getContentSize().width*lbTitle_l->getScale() + 20, sprTitle->getContentSize().height/2));
	sprTitle->addChild(_lbTitleMail);

	_editTitle = EditBox::create(Size(637, 53), cocos2d::ui::Scale9Sprite::create("edt_mail_small1.png"));
	_editTitle->setInputFlag(EditBox::InputFlag::SENSITIVE);
	_editTitle->setInputMode(EditBox::InputMode::SINGLE_LINE);
	_editTitle->setPosition(sprTitle->getPosition());
	_editTitle->setFont("fonts/arial.ttf", 18);
	_editTitle->setMaxLength(50);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	_editTitle->setDelegate(this);
#endif
	_editTitle->setTag(1);
	_editTitle->setFontColor(Color3B::WHITE);
	_editTitle->setPlaceholderFontColor(Color3B(125, 125, 125));
	//_editTitle->setInputMode(EditBox::InputMode::ANY);
	background->addChild(_editTitle, -1);

	auto sprFromTo = EGSprite::createWithFile("edt_mail_small.png");
	sprFromTo->setPosition(Vec2(sprTitle->getPosition()) + Vec2(0, sprFromTo->getContentSize().height + 10));
	background->addChild(sprFromTo, 1);

	_lbUsername_Left = Label::createWithBMFont("font.fnt", _xmlReader->getNodeTextByTagName("txt_mail_to"));
	_lbUsername_Left->setAnchorPoint(Vec2(0, 0.5f));
	_lbUsername_Left->setColor(Color3B::YELLOW);
	_lbUsername_Left->setScale(0.4f);
	_lbUsername_Left->setPosition(Vec2(10, sprFromTo->getContentSize().height / 2));
	sprFromTo->addChild(_lbUsername_Left);

	_lbUsername = Label::createWithBMFont("font.fnt", "TuanVu");
	_lbUsername->setAnchorPoint(Vec2(0, 0.5f));
	_lbUsername->setScale(0.4f);
	_lbUsername->setPosition(Vec2(_lbUsername_Left->getContentSize().width*_lbUsername_Left->getScale() + 20, sprFromTo->getContentSize().height / 2));
	sprFromTo->addChild(_lbUsername);

	_editName = EditBox::create(Size(637, 53), cocos2d::ui::Scale9Sprite::create("edt_mail_small1.png"));
	_editName->setInputFlag(EditBox::InputFlag::SENSITIVE);
	_editName->setInputMode(EditBox::InputMode::SINGLE_LINE);
	_editName->setPosition(sprFromTo->getPosition());
	_editName->setFont("fonts/arial.ttf", 18);
	_editName->setMaxLength(50);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	_editName->setDelegate(this);
#endif
	_editName->setTag(1);
	_editName->setFontColor(Color3B::WHITE);
	_editName->setPlaceholderFontColor(Color3B(125, 125, 125));
	//_editName->setInputMode(EditBox::InputMode::ANY);
	background->addChild(_editName, -1);

	//create buttons
	_btnCenter = EGButtonSprite::createWithFile("btn_box_mail_delete.png", EGButton2FrameDark);
	_btnCenter->setPosition(Vec2(background->getContentSize().width / 2, -_btnCenter->getContentSize().height/2 - 5));
	background->addChild(_btnCenter);
	_btnCenter->setOnTouch(CC_CALLBACK_0(BoxMail::btnCenterClicked, this));

	_btnLeft = EGButtonSprite::createWithFile("btn_box_mail_reply.png", EGButton2FrameDark);
	_btnLeft->setPosition(Vec2(background->getContentSize().width / 2 - _btnLeft->getContentSize().width - 30, -_btnCenter->getContentSize().height / 2 - 5));
	background->addChild(_btnLeft);
	_btnLeft->setOnTouch(CC_CALLBACK_0(BoxMail::btnLeftClicked, this));

	_btnRight = EGButtonSprite::createWithFile("btn_box_mail_close.png", EGButton2FrameDark);
	_btnRight->setPosition(Vec2(background->getContentSize().width / 2 + _btnRight->getContentSize().width + 30, -_btnCenter->getContentSize().height / 2 - 5));
	background->addChild(_btnRight);
	_btnRight->setOnTouch(CC_CALLBACK_0(BoxMail::btnRightClicked, this));

	
	EventListenerTouchOneByOne *_eventTouch = EventListenerTouchOneByOne::create();
	_eventTouch->onTouchBegan = CC_CALLBACK_2(BoxMail::onTouchBegan, this);
	_eventTouch->onTouchMoved = CC_CALLBACK_2(BoxMail::onTouchMoved, this);
	_eventTouch->onTouchEnded = CC_CALLBACK_2(BoxMail::onTouchEnded, this);
	_eventTouch->onTouchCancelled = CC_CALLBACK_2(BoxMail::onTouchEnded, this);
	_eventTouch->setSwallowTouches(true);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(_eventTouch, this);

	this->schedule(schedule_selector(BoxMail::updateText), 0.3f);
}

void BoxMail::setBoxType(int pType){ // 0: inbox, 1: outbox, 2: new
	_type = pType;
	if (pType != 2){
		background->setTexture("background_view_mail.png");
		if (pType == 0)
			_lbUsername_Left->setString(_xmlReader->getNodeTextByTagName("txt_mail_from").c_str());
		else
			_lbUsername_Left->setString(_xmlReader->getNodeTextByTagName("txt_mail_to").c_str());
		_editName->setEnabled(false);
		_editTitle->setEnabled(false);
		_editContent->setEnabled(false);
	}
	else{
		background->setTexture("background_write_mail.png");
		_lbUsername_Left->setString(_xmlReader->getNodeTextByTagName("txt_mail_to").c_str());
		_editName->setEnabled(true);
		_editTitle->setEnabled(true);
		_editContent->setEnabled(true);
		_btnCenter->setVisible(false);
		_btnLeft->setTexture("btn_box_mail_send.png");
	}
}

void BoxMail::showMessage(uint8_t type, StructMail pInfo){
	if (type == 0){ // inbox
		_btnLeft->setTexture("btn_box_mail_reply.png");
		_btnCenter->setVisible(true);
		_lbUsername->setString(pInfo.sender);
		_strUserName = pInfo.sender;
	}
	else{
		_btnCenter->setVisible(false);
		_lbUsername->setString(pInfo.receiver);
		_strUserName = pInfo.receiver;
		_btnLeft->setTexture("btn_box_mail_delete.png");
	}
	std::string  pContent = EGSupport::addSpaceInStdString(pInfo.content, 8);
	_lbTitleMail->setString(pInfo.subject);
	_lbContent->setString(pContent);
	_structMail = pInfo;
}

void BoxMail::updateText(float pSec){
	if (_type == 2){
		_lbUsername->setString(_editName->getText());
		_lbTitleMail->setString(_editTitle->getText());
		_lbContent->setString(_editContent->getText());
	}
}
void BoxMail::editBoxReturn(EditBox* editBox){
	if (editBox == _editTitle){
		_lbTitleMail->setString(editBox->getText());
	}
	else if (editBox == _editContent){
		_lbContent->setString(EGSupport::addSpaceInStdString(editBox->getText(), 8));
	}
	else if (editBox == _editName){
		_lbUsername->setString(editBox->getText());
	}
}
std::string BoxMail::getUserName(){
	return _strUserName;
}
std::string BoxMail::getTitle(){
	return _lbTitleMail->getString();
}
std::string BoxMail::getContent(){
	return _lbContent->getString();
}
bool BoxMail::onTouchBegan(Touch*, Event*){
	return (this->isVisible());
}

void BoxMail::btnLeftClicked()
{
	if (_type == 0){ //is reply
		_type = 2;
		this->setBoxType(2);
		_editName->setText(_structMail.sender.c_str());
		_editName->setEnabled(false);
		_btnCenter->setVisible(false);
		_btnLeft->setTexture("btn_box_mail_send.png");
	}
	else if(_type == 1){ //delete mail
		if (_funcDeleteMail)
			_funcDeleteMail(_structMail);
	}
	else if (_type == 2){ // send mail
		std::string name = _editName->getText();
		std::string title = _editTitle->getText();
		std::string content = _editContent->getText();

		if (!name.compare("")){
			_funcshowNotice("txt_mail_enter_name");
		}
		else if (!title.compare("")){
			_funcshowNotice("txt_mail_enter_title");
		}
		else if (!content.compare("")){
			_funcshowNotice("txt_mail_enter_content");
		}
		else if (checkExistsSpecialCharcter(name) == false){
			_funcshowNotice("txt_mail_enter_name_have_special_character");
		}
		else{
			//tolower name
			for (uint32_t i = 0; i < name.size(); ++i){
				name[i] = std::tolower(name[i]);
			}
			int x = 1;
			//send
			if (_funcSendMail)
				_funcSendMail(name, title, content);
			//resetEditText();
		}
	}
}

void BoxMail::btnCenterClicked()
{
	if (_funcDeleteMail)
		_funcDeleteMail(_structMail);
}

void BoxMail::btnRightClicked()
{
	this->setVisible(false);
	this->setPosition(CLIENT_WIDTH, 0);
	resetEditText();
	if (_funcCallBack)
		_funcCallBack();
}

void BoxMail::resetEditText()
{
	_editName->setText("");
	_editTitle->setText("");
	_editContent->setText("");
	_lbUsername->setString("");
	_lbTitleMail->setString("");
	_lbContent->setString("");
}

bool BoxMail::checkExistsSpecialCharcter(const std::string& strInput)
{
	for (uint32_t i = 0; i < strInput.size(); ++i)
	{
		if (strInput[i] == ':'){
			return false;
		}
	}
	return true;
}