#ifndef __ACTIONMOVE_H_INCLUDED__
#define __ACTIONMOVE_H_INCLUDED__
#include "cocos2d.h"
#include "EGSupport.h"

using namespace cocos2d;

class ActionMove : public Ref {
public:
	static ActionMove* create(){
		ActionMove *_action = new ActionMove();
		_action->init();
		_action->autorelease();
		return _action;
	}
	void init(){
		prepare();
	}
	// =0 pX & pY 
	//= 1 pX
	//=2 pY
	static ActionMove* createMoveTo(float pSpeed, Vec2 pPositionTo, int pSide);
	void initAction(float pSpeed, Vec2 pPositionTo, int pSide);

	static ActionMove* createMoveBy(float pSpeed, Vec2 pPositionBy);
	void initActionMoveBy(float pSpeed, Vec2 pPositionBy);

	static ActionMove* createMoveCircle(float pSpeed, Vec2 pCenter, float pRadius, float pRotation, int pSide);
	static ActionMove* createMoveCircle(float pSpeed, Vec2 pCenter, float pRotation, int pSide);
	void initAction(float pSpeed, Vec2 pCenter, float pRadius, float pRotation, int pSide);

	static ActionMove* createDelayTime(float pDelayTime);
	static ActionMove* createDelayTimeShield(float pDelayTime);
	void initAction(float pDelayTime,bool pShield);

	static ActionMove* createMoveParaBol(float pDuration, float pHeight, float pWidth);
	void initAction( float pDuration, float pHeight, float pWidth);

	static ActionMove* createMoveGravity(float pDuration,Vec2 pVecStartSpeed, Vec2 pVecDamping);
	void initAction(float pDuration, Vec2 pVecStartSpeed, Vec2 pVecDamping);

	static ActionMove* createMoveSin(float pSpeed, float pWidthSong, float pHeightSong, float pCountSong);
	void initAction(float pSpeed, float pWidthSong, float pHeightSong, float pCountSong);

	ActionMove* setRepeat(int pTimeRepeat,bool pBoolReset);
	ActionMove* setRepeat(int pTimeRepeat);

	void update(float pSec);
	void setStartPosition(Vec2 pPosition){ mStartPosition = pPosition; mCurrentPosition = mStartPosition; };

	void prepare();
	void setScaleTime(float pScale){
		mScaleTime = pScale; 
		iScaleTime = 1;
	};
	float getScaleTime(){ return mScaleTime; };
	bool isComplete();
	Vec2 getCurrentPosition();
	void clearAll();
	Vec2 getStartPosition(){ return mStartPosition; };
	Vector<ActionMove*> getListAction(){ return mArrayAction; };
	void addAction(ActionMove* pAction){ mArrayAction.pushBack(pAction);}
	void setSpeed(float pSpeed);
	ActionMove* setGiatoc(float pGiatoc){ mGiatocAction = pGiatoc; iGiatoc = 1; return this; }
	ActionMove* setSyncronize(bool pBool){ iSyncro = pBool; return this; };
	Vec2 getTypeEnemy(){ return mTypeEnemy; };
	void setTypeEnemy(Vec2 pType){ mTypeEnemy = pType; };
	void nextAction();
	ActionMove* setComplete(std::function<void()> pFunction){ 
		mFuntionOnComplete = pFunction; 
		for (int i = mArrayAction.size() - 1; i >= 0; i--){
			mArrayAction.at(i)->setComplete(pFunction);
		}
		return this; }
	ActionMove* getActionByIndex(int pIndex){ return mArrayAction.at(pIndex); };
	void forceComplete(){
		iComplete = true; 
	};
	void setDelayTime(float pDelay){ mDelayTime = pDelay; }
	void pauseAction(){ iPause = true; };
	void resumeAction(){
		iPause = false;
	};
	bool isPause(){ return iPause; };
	bool isShield();
	int getTypeAction(){ return mType; };
private:
	int mType,mSide,mCurrentRepeat,mTimeReapet,mCurrentAction,mSideMoveTo,iScaleTime,mSideSin,iGiatoc;
	Vec2 mTypeEnemy;
	Vec2 mStartPosition,mCurrentPosition,mDestinyPosition,mCenterCircle,mMoveBy,mVecStartSpeed,mVecDamping,mCurrentVecSpeed;
	float mSpeed, mDestinyRotation, mCurrentRotation, mRadius, mCurrentMove, mDelayTime, mCurrentDelay, mDuration, mCurrentParabolY, mGiatocAction, mCurrenParabolX, mGiatoc, mScaleTime, mCountSin, mCurrentSpeed, mCurrentMoveX, mCurrentMoveY;
	float mHeightParabol, mWidthParabol;
	bool iComplete,iSyncro,iPause,iReset,iShield;
	Vector<ActionMove*> mArrayAction;
	std::function<void()> mFuntionOnComplete;
};
#endif