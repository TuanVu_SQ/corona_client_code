#ifndef __BOXTOPUP_H_INCLUDED__
#define __BOXTOPUP_H_INCLUDED__

#include "cocos2d.h"
#include "EGButtonSprite.h"
#include "ClientPlayerInfo.h"
#include "ChargeItem.h"
#include "object/BoxConfirmSms.h"
#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "extensions\cocos-ext.h"
#else
#include "cocos-ext.h"
#endif

USING_NS_CC;
USING_NS_CC_EXT;

class BoxTopUp : public LayerColor, public TableViewDataSource, public TableViewDelegate {
public:
	static BoxTopUp* create(std::function<void()> pFunctionCallCard, std::function<void(int)> pFunctionRecharge);

	void setInfo(std::vector<RechargeInfo> pLstRecharge, bool isEnableRecharge = true);
	void setInfoVISA(std::vector<RechargeVISAInfo> pLstRechargeVISA);

	void close();
    void setVisible(bool isVisible);
protected:
	bool isLoaded;
	std::function<void(int)> mFunctionRecharge;
private:
    std::vector<EGButtonSprite*> mLstButton;
	Sprite* mBox;
	EGSprite* mp_sprTab1, *mp_sprTab2, *cardComboBg;
	EGButtonSprite* mp_btnsprTabRechargeVN, *mp_btnsprTabRechargeVISA;
	EGButtonSprite** mBtnRecharge, *mBtnCallBoxRechargeByCard, **mBtnRechargeSend;
	EGButtonSprite* _btnSms, *_btnCard, *_btnVisa;
	EGSprite** mp_sprSMSBonus;
	Label** mp_lblSMSBonus, *_lbTelcoNoteUp, *_lbTelcoNoteDown;
	EGButtonSprite* mBtnClose, *_btnReCharge;
	TableView* mTableView;
	WXmlReader* pXml;

	//add by vu (new)
	BoxConfirmSms* boxConfirmSms;
	std::vector<RechargeInfo> _listSmsBuffer;

	void showSmsView(uint8_t tag, std::string telco);
	std::vector<EGButtonSprite*> _listTabTelco;
	//end

	void initObject(std::function<void()> pFunctionCallCard);
	void recharge(std::string pSyntax, std::string pServiceNumber);

	bool touchBegan(Touch* touch, Event* event) {
		return this->isVisible();
	}
	void purchaseItem(std::string pItemID);

	virtual void scrollViewDidScroll(ScrollView* view) {};
	virtual void scrollViewDidZoom(ScrollView* view) {};
	virtual void tableCellTouched(TableView* table, TableViewCell* cell) {};
	virtual Size tableCellSizeForIndex(TableView *table, ssize_t idx);
	virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(TableView *table);

public:
	void reloadData(uint8_t type); //0:sms, 1: visa
	void showNotice(const std::string strNotice);
	void showLoading(bool bLoading);
	void showRechargePromotion(uint8_t telcoId);
	//void loadRate(uint8_t telcoId);
	void showBoxConfirmSms(RechargeInfo smsInfo);
	void showBoxConfirmSub(SubcribeInfo_s subInfo);
	void showBoxConfirmExchange(ExchangeInfo exchangeInfo);
	void showBoxLockRecharge();
	BoxConfirmSms* GetBoxSms();

private:
	Node* _nodeCard, *_nodeNotCard;
	EditBox *_edtCardNumber, *_edtSerialNumber;

	std::vector<EGSprite*> listCardBg;
	std::vector<Label*> _listLabelCardCombo;

	uint8_t _currentCardType;

public:
	uint8_t _currentTab, _tab;
	void btnSmsClicked();
	void btnCardClicked();
	void btnVisaClicked();
	void createTabsSmsInfo();
	void showSubPackage();

private:
	void cardClicked(uint8_t cardIdx);
	void btnReChargeClicked();

	bool isNumber(const char * strNumber);

private:
	CC_SYNTHESIZE(std::vector<RechargeInfo>, _listSmsInfo, LstRechargeSMS);
	CC_SYNTHESIZE(std::vector<StructTelcoInfo>, _listSmsInfoTelco, LstSmsInfoTelco);
	//CC_SYNTHESIZE(std::vector<SubscriptionInfo>, _listSubInfo, LstSubInfo);
	CC_SYNTHESIZE(std::vector<SubcribeInfo_s>, _listSubInfo, LstSubInfo);
	CC_SYNTHESIZE(std::vector<RechargeVISAInfo>, mLstRechargeVISA, LstRechargeVISA);
	CC_SYNTHESIZE(std::vector<ExchangeInfo_s>, mLstExchangeInfo, LstExchangeInfo);
	CC_SYNTHESIZE(std::vector<ScratchCard>, mLstRate, LstRate);

	CC_SYNTHESIZE(std::function<void(const std::string&)>, _funcShowNotice, FuncShowNotice);
	CC_SYNTHESIZE(std::function<void(bool)>, _funcShowLoading, FuncShowLoading);
	CC_SYNTHESIZE(std::function<void()>, _funcGetSmsInfo, FuncGetSmsInfo);
	CC_SYNTHESIZE(std::function<void()>, _funcGetSubInfo, FuncGetSubInfo);
	CC_SYNTHESIZE(std::function<void()>, _funcGetRechargeRate, FuncGetRechargeRate);
	CC_SYNTHESIZE(std::function<void(std::string, std::string, std::string)>, _funcRecharge, FuncRecharge);

	CC_SYNTHESIZE(std::function<void()>, _funcChargeSms, FuncChargeSms);
	CC_SYNTHESIZE(std::function<void()>, _funcExchange, FuncExchange);
};

#endif