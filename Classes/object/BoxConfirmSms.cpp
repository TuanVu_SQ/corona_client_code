#include "BoxConfirmSms.h"
#include "GameInfo.h"
#include "libs/EGJniHelper.h"
#include "libs/EGSupport.h"
#include "MessageType.h"
#include "MpClientManager.h"

BoxConfirmSms* BoxConfirmSms::create() {
	BoxConfirmSms* pBoxConfirmSms = new BoxConfirmSms();
	pBoxConfirmSms->initObject();
	pBoxConfirmSms->autorelease();

	return pBoxConfirmSms;
}

void BoxConfirmSms::initObject() {
	EGSprite::initWithSpriteFrameName(IMG_BOX_NOTICE);

	LayerColor* pLayer = LayerColor::create(Color4B(0, 0, 0, 200));
	pLayer->setPosition(Vec2(this->getContentSize().width / 2 - CLIENT_WIDTH / 2, this->getContentSize().height / 2 - CLIENT_HEIGHT / 2));
	this->addChild(pLayer, -1);

	pXml = WXmlReader::create();
	pXml->load(KEY_XML_FILE);

	_nodeSms = Node::create();
	_nodeSms->setPosition(0, 0);
	this->addChild(_nodeSms);

	_nodeInGame = Node::create();
	_nodeInGame->setPosition(CLIENT_WIDTH, 0);
	this->addChild(_nodeInGame);

	btnChargeSms = EGButtonSprite::createWithFile("kv_banthuong_on_button.png");
	btnChargeSms->setPosition(Vec2(this->getContentSize().width / 2 - btnChargeSms->getContentSize().width/2 - 20,  this->getContentSize().height - btnChargeSms->getContentSize().height + 10));
	this->addChild(btnChargeSms);
	btnChargeSms->setOnTouch(CC_CALLBACK_0(BoxConfirmSms::showSmsNode, this));

	auto lb1 = Label::createWithBMFont("font-info-game.fnt", "Chaje via SMS");
	lb1->setScale(0.7f);
	lb1->setPosition(btnChargeSms->getContentSize() / 2);
	btnChargeSms->addChild(lb1);

	btnChargeInGame = EGButtonSprite::createWithFile("kv_banthuong_off_button.png");
	btnChargeInGame->setPosition(Vec2(this->getContentSize().width / 2 + btnChargeInGame->getContentSize().width / 2 + 20, this->getContentSize().height - btnChargeInGame->getContentSize().height + 10));
	this->addChild(btnChargeInGame);
	btnChargeInGame->setOnTouch(CC_CALLBACK_0(BoxConfirmSms::showIngameNode, this));

	auto lb2 = Label::createWithBMFont("font-info-game.fnt", "Vit chaje");
	lb2->setScale(0.7f);
	lb2->setPosition(btnChargeInGame->getContentSize() / 2);
	btnChargeInGame->addChild(lb2);

	//node ingame
	lb_ingame_confirm = Label::createWithBMFont("font_black.fnt", "");
	lb_ingame_confirm->setScale(1.2f);
	lb_ingame_confirm->setPosition(Vec2(this->getContentSize().width/2, this->getContentSize().height/2 + 50));
	lb_ingame_confirm->setColor(Color3B::BLACK);
	_nodeInGame->addChild(lb_ingame_confirm);

	edtInput = cocos2d::ui::EditBox::create(Size(150, 42), Scale9Sprite::create("editbox_small.png"));
	edtInput->setVisible(false);
	edtInput->setInputFlag(cocos2d::ui::EditBox::InputFlag::SENSITIVE);
	edtInput->setInputMode(EditBox::InputMode::NUMERIC);
	edtInput->setPosition(Vec2(this->getContentSize().width / 2, this->getContentSize().height / 2 - 10));
	edtInput->setFont("fonts/arial.ttf", 18);
	edtInput->setMaxLength(15);
	edtInput->setPlaceholderFont("fonts/arial.ttf", 18);
	edtInput->setFontColor(Color3B::WHITE);
	edtInput->setPlaceHolder(pXml->getNodeTextByTagName("txt_enter_otp").c_str());
	edtInput->setPlaceholderFontColor(Color3B(135, 206, 235));
	_nodeInGame->addChild(edtInput);

	lbErrorChargeInGame = Label::createWithBMFont("font.fnt", "error");
	lbErrorChargeInGame->setVisible(false);
	lbErrorChargeInGame->setScale(0.5f);
	lbErrorChargeInGame->setPosition(Vec2(this->getContentSize().width / 2, edtInput->getPositionY() - 45));
	lbErrorChargeInGame->setColor(Color3B::ORANGE);
	_nodeInGame->addChild(lbErrorChargeInGame);

	auto btnCharge = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_GREEN, EGButton2FrameDark);
	btnCharge->setPosition(Vec2(this->getContentSize().width / 4, btnCharge->getContentSize().height / 2 + 20));
	btnCharge->setText(pXml->getNodeTextByTagName("accept").c_str(), FONT, 20);
	_nodeInGame->addChild(btnCharge);
	btnCharge->setOnTouch(CC_CALLBACK_0(BoxConfirmSms::doRechargeInGame, this));

	auto btnClose = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_GREEN, EGButton2FrameDark);
	btnClose->setPosition(Vec2(3 * this->getContentSize().width / 4, btnClose->getContentSize().height / 2 + 20));
	btnClose->setText(pXml->getNodeTextByTagName("skip").c_str(), FONT, 20);
	_nodeInGame->addChild(btnClose);
	btnClose->setOnTouch(CC_CALLBACK_0(BoxConfirmSms::btnIgnoreClicked, this));


	//node sms
	auto mLbl_Title1 = Label::createWithBMFont("font_white.fnt", "sms");
	mLbl_Title1->setVisible(false);
	mLbl_Title1->setScale(1.3f);
	mLbl_Title1->setPosition(Vec2(this->getContentSize().width / 2, this->getContentSize().height - mLbl_Title1->getContentSize().height - 25));
	mLbl_Title1->setColor(Color3B::BLUE);
	_nodeSms->addChild(mLbl_Title1);

	auto mLbl_Title2 = Label::createWithBMFont("font_black.fnt", pXml->getNodeTextByTagName("txt_box_confirm_title_2"));
	mLbl_Title2->setScale(1.2f);
	mLbl_Title2->setPosition(Vec2(mLbl_Title1->getPosition()) + Vec2(0, -60));
	mLbl_Title2->setColor(Color3B::BLACK);
	_nodeSms->addChild(mLbl_Title2);

	mLbl_Title3 = Label::createWithBMFont("font_white.fnt", "");
	mLbl_Title3->setScale(1.1f);
	mLbl_Title3->setPosition(Vec2(mLbl_Title2->getPosition()) + Vec2(0, -30));
	mLbl_Title3->setColor(Color3B::ORANGE);
	_nodeSms->addChild(mLbl_Title3);

	auto mLbl_Title4_1 = Label::createWithBMFont("font_black.fnt", pXml->getNodeTextByTagName("txt_box_confirm_title_4_1"));
	mLbl_Title4_1->setColor(Color3B::BLACK);
	mLbl_Title4_1->setScale(1.2f);
	mLbl_Title4_1->setPosition(Vec2(mLbl_Title3->getPosition()) + Vec2(-70, -30));
	_nodeSms->addChild(mLbl_Title4_1);

	mLbl_Title4_2 = Label::createWithBMFont("font_white.fnt", "");
	mLbl_Title4_2->setScale(1.2f);
	mLbl_Title4_2->setPosition(Vec2(mLbl_Title4_1->getPosition()) + Vec2(94, 0));
	mLbl_Title4_2->setColor(Color3B::ORANGE);
	_nodeSms->addChild(mLbl_Title4_2);

	auto mLbl_Title4_3 = Label::createWithBMFont("font_black.fnt", pXml->getNodeTextByTagName("txt_box_confirm_title_4_3"));
	mLbl_Title4_3->setColor(Color3B::BLACK);
	mLbl_Title4_3->setScale(1.2f);
	mLbl_Title4_3->setPosition(Vec2(mLbl_Title4_2->getPosition()) + Vec2(70, 0));
	_nodeSms->addChild(mLbl_Title4_3);

	mLbl_Title5 = Label::createWithBMFont("font_black.fnt", "");
	mLbl_Title5->setColor(Color3B::BLACK);
	mLbl_Title5->setScale(1.2f);
	mLbl_Title5->setPosition(Vec2(mLbl_Title3->getPositionX(), mLbl_Title4_3->getPositionY() - 30));
	_nodeSms->addChild(mLbl_Title5);


	mBtnAccept = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_GREEN, EGButton2FrameDark);
	mBtnAccept->setPosition(Vec2(this->getContentSize().width/4, mBtnAccept->getContentSize().height/2 + 20));
	mBtnAccept->setText(pXml->getNodeTextByTagName("accept").c_str(), FONT, 20);
	_nodeSms->addChild(mBtnAccept);
	mBtnAccept->setOnTouch(CC_CALLBACK_0(BoxConfirmSms::btnAcceptClicked, this));

	mBtnSkip = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_GREEN, EGButton2FrameDark);
	mBtnSkip->setPosition(Vec2(3 * this->getContentSize().width / 4, mBtnSkip->getContentSize().height / 2 + 20));
	mBtnSkip->setText(pXml->getNodeTextByTagName("skip").c_str(), FONT, 20);
	_nodeSms->addChild(mBtnSkip);
	mBtnSkip->setOnTouch(CC_CALLBACK_0(BoxConfirmSms::btnIgnoreClicked, this));


	this->setVisible(false);
	this->registerTouch();
}

void BoxConfirmSms::btnAcceptClicked()
{
	string syntax = "";
	string serviceNumber = "";
	if (_type == 1){
		syntax = m_StructInfo.syntax;
		serviceNumber = m_StructInfo.serviceNumber;
	}
	else if (_type == 2){
		syntax = m_SubInfo.syntax;
		serviceNumber = m_SubInfo.serverUddp;
	}
	else{
		syntax = m_ExchangeInfo.syntax;
		serviceNumber = m_ExchangeInfo.serviceNumber;
	}

	EGJniHelper::sendSMS(syntax.c_str(), serviceNumber.c_str());
	this->setVisible(false);
	this->setPosition(Vec2(1300, 240));
}

void BoxConfirmSms::btnIgnoreClicked()
{
	this->setVisible(false);
	this->setPosition(Vec2(1300, 240));
}


void BoxConfirmSms::doRechargeInGame()
{
	/*this->setVisible(false);
	this->setPosition(Vec2(1300, 240));*/
	lbErrorChargeInGame->setVisible(false);
	if (_type == 1){ //recharge
		if (_chargeStep == 1){
			MpMessage *messag = new MpMessage((uint32_t)MP_MSG_CHARGE_REQ);
			messag->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
			messag->addUInt32(MP_PARAM_TAG_1, m_StructInfo.id);
			MpClientManager::getInstance()->sendMessage(messag, true);
		}
		else{
			string otp = edtInput->getText();
			if (otp == ""){
				lbErrorChargeInGame->setString(pXml->getNodeTextByTagName("txt_box_repass_err_enter_otp"));
				lbErrorChargeInGame->setVisible(true);
			}
			else{
				MpMessage *messag = new MpMessage((uint32_t)MP_MSG_CHARGE_VERIFY_REQ);
				messag->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
				messag->addString(MP_PARAM_TAG_1, otp);
				messag->addString(MP_PARAM_TAG_2, _transId);
				MpClientManager::getInstance()->sendMessage(messag, true);
			}

		}
	}
	else{ //exchange
		if (_chargeStep == 1){
			MpMessage *messag = new MpMessage((uint32_t)MP_MSG_EXCHANGE_REQ);
			messag->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
			messag->addUInt32(MP_PARAM_TAG_1, m_ExchangeInfo.id);
			MpClientManager::getInstance()->sendMessage(messag, true);
		}
		else{
			string otp = edtInput->getText();
			if (otp == ""){
				lbErrorChargeInGame->setString(pXml->getNodeTextByTagName("txt_box_repass_err_enter_otp"));
				lbErrorChargeInGame->setVisible(true);
			}
			else{
				MpMessage *messag = new MpMessage((uint32_t)MP_MSG_EXCHANGE_VERIFY_REQ);
				messag->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
				messag->addString(MP_PARAM_TAG_1, otp);
				messag->addString(MP_PARAM_TAG_2, _transId);
				messag->addUInt32(MP_PARAM_TAG_3, m_ExchangeInfo.id);
				MpClientManager::getInstance()->sendMessage(messag, true);
			}

		}
	}
	
}

void BoxConfirmSms::showSmsNode()
{
	btnChargeInGame->setTexture("kv_banthuong_off_button.png");
	btnChargeSms->setTexture("kv_banthuong_on_button.png");

	_nodeSms->setPosition(Vec2(0, 0));
	_nodeSms->setVisible(true);

	_nodeInGame->setPosition(Vec2(1300, 0));
	_nodeInGame->setVisible(false);
	_chargeType = 1;

}

void BoxConfirmSms::ShowOtpToCharge(string transId)
{
	_transId = transId;
	_nodeSms->setPosition(Vec2(1300, 0));
	_nodeSms->setVisible(false);

	_nodeInGame->setPosition(Vec2(0, 0));
	_nodeInGame->setVisible(true);

	edtInput->setVisible(true);
	lb_ingame_confirm->setString(pXml->getNodeTextByTagName("txt_enter_otp_to_active"));
	_chargeStep = 2;
}

void BoxConfirmSms::showIngameNode()
{
	btnChargeInGame->setTexture("kv_banthuong_on_button.png");
	btnChargeSms->setTexture("kv_banthuong_off_button.png");

	uint32_t balance = 0;
	uint32_t money = 0;
	if (_type == 1){
		balance = m_StructInfo.balance;
		money = m_StructInfo.money;
	}
	else if (_type == 2){
		balance = m_SubInfo.balance;
		money = m_SubInfo.daily_money;
	}
	else if (_type == 3){
		balance = m_ExchangeInfo.balance;
		money = m_ExchangeInfo.money;
	}

	_nodeInGame->setPosition(Vec2(0, 0));
	_nodeInGame->setVisible(true);

	_nodeSms->setPosition(Vec2(1300, 0));
	_nodeSms->setVisible(false);
	_chargeType = 2;
}

void BoxConfirmSms::showInfo(RechargeInfo info)
{
	btnChargeInGame->setVisible(true);
	btnChargeSms->setVisible(true);
	m_StructInfo = info;
	_type = 1;

	mLbl_Title3->setString(info.syntax);
	mLbl_Title4_2->setString(info.serviceNumber);
	mLbl_Title5->setString(__String::createWithFormat(pXml->getNodeTextByTagName("txt_box_confirm_title_5").c_str(), EGSupport::convertMoneyAndAddDot(info.money).c_str())->getCString());

	this->setVisible(true);
	this->setPosition(Vec2(400, 240));

	lb_ingame_confirm->setString(__String::createWithFormat(pXml->getNodeTextByTagName("txt_recharge_confirm").c_str(), EGSupport::convertMoneyAndAddDot(m_StructInfo.balance).c_str(), EGSupport::convertMoneyAndAddDot(m_StructInfo.money).c_str())->getCString());
}

void BoxConfirmSms::showSubConfirm(SubcribeInfo_s info)
{
	showSmsNode();
	btnChargeInGame->setVisible(false);
	btnChargeSms->setVisible(false);

	m_SubInfo = info;
	_type = 2;

	mLbl_Title3->setString(info.syntax);
	mLbl_Title4_2->setString(info.serviceNumber);
	mLbl_Title5->setString(__String::createWithFormat(pXml->getNodeTextByTagName("txt_box_confirm_title_5").c_str(), EGSupport::convertMoneyAndAddDot(info.daily_money).c_str())->getCString());

	this->setVisible(true);
	this->setPosition(Vec2(400, 240));

	lb_ingame_confirm->setString(__String::createWithFormat(pXml->getNodeTextByTagName("txt_recharge_confirm").c_str(), EGSupport::convertMoneyAndAddDot(m_SubInfo.balance).c_str(), EGSupport::convertMoneyAndAddDot(m_SubInfo.daily_money).c_str())->getCString());
}

void BoxConfirmSms::showExchangeConfirm(ExchangeInfo info)
{
	btnChargeInGame->setVisible(true);
	btnChargeSms->setVisible(true);
	m_ExchangeInfo = info;
	_type = 3;

	mLbl_Title3->setString(info.syntax);
	mLbl_Title4_2->setString(info.serviceNumber);
	mLbl_Title5->setString(__String::createWithFormat(pXml->getNodeTextByTagName("txt_box_confirm_title_5").c_str(), EGSupport::convertMoneyAndAddDot(info.money).c_str())->getCString());

	this->setVisible(true);
	this->setPosition(Vec2(400, 240));

	lb_ingame_confirm->setString(__String::createWithFormat(pXml->getNodeTextByTagName("txt_exchange_confirm").c_str(), EGSupport::convertMoneyAndAddDot(m_ExchangeInfo.balance).c_str(), EGSupport::convertMoneyAndAddDot(m_ExchangeInfo.money).c_str())->getCString());
}

void BoxConfirmSms::showDescription(std::string content){
	if (content.length() > 0){
		lbErrorChargeInGame->setString(content);
		lbErrorChargeInGame->setVisible(true);
	}
	else
		lbErrorChargeInGame->setVisible(false);
}

void BoxConfirmSms::Reset(){
	_nodeSms->setPosition(Vec2(0, 0));
	_nodeSms->setVisible(true);

	_nodeInGame->setPosition(Vec2(1300, 0));
	_nodeInGame->setVisible(false);

	edtInput->setVisible(false);
	lbErrorChargeInGame->setVisible(false);
	_chargeStep = 1;
}
