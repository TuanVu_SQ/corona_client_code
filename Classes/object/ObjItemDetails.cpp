#include "ObjItemDetails.h"
#include "GameInfo.h"
#include "ItemInfo.h"
#include "WXmlReader.h"
#include "GunInfo.h"
#include "EGSupport.h"
#include "ShopScene.h"
#include "AvatarInfo.h"
#include "MainScene.h"

#include "mpbuyitemmessagerequest.h"
#include "mpbuyitemmessageresponse.h"
#include "pthread_mobi.h"

#include "BoxNoticeManager.h"
#include "EGJniHelper.h"

struct ItemStruct {
public:
	int mItemID, mCounter, mItemType;
	int mExType;
};
void pthread_onBuy(void* _void) {
	ItemStruct* pItemStruct = (ItemStruct*)_void;

	MpBuyItemMessageRequest* pRequest = new MpBuyItemMessageRequest();
	pRequest->setItemID(pItemStruct->mItemID);
	pRequest->setItemCounter(pItemStruct->mCounter);
	pRequest->setItemType(pItemStruct->mItemType);
	pRequest->setExpireType(pItemStruct->mExType);
	pRequest->setTokenId(GameInfo::mUserInfo.mTokenID.c_str());
	MpClientManager::getInstance()->sendMessage(pRequest);
	delete pItemStruct;
	pItemStruct = NULL;
}

ObjItemDetails* ObjItemDetails::create(bool pIsShop, Loading* pLoading, BoxNotice *pBoxNotice, std::function<void()> pFuncReload) {
	ObjItemDetails* pObjItemDetails = new ObjItemDetails();
	pObjItemDetails->mFuncReload = pFuncReload;
	pObjItemDetails->initObject(pIsShop, pLoading, pBoxNotice);
	pObjItemDetails->autorelease();
	return pObjItemDetails;
}

ObjItemDetails* ObjItemDetails::create(bool pIsShop, Loading* pLoading, BoxNotice *pBoxNotice) {
	ObjItemDetails* pObjItemDetails = new ObjItemDetails();
	pObjItemDetails->initObject(pIsShop, pLoading, pBoxNotice);
	pObjItemDetails->autorelease();
	return pObjItemDetails;
}

void ObjItemDetails::initObject(bool pIsShop, Loading* pLoading, BoxNotice *pBoxNotice) {
	EGSprite::initWithSpriteFrameName(IMG_BOX_DETAILS);

	mIsShop = pIsShop;
	mBoxNotice = pBoxNotice;
	mLoading = pLoading;
	mXml = WXmlReader::create();
	mXml->load(KEY_XML_FILE);
	mLblCounter = NULL;
	mLblCounter_Values = NULL;
	mLblPrice = NULL;
	mLblPrice_Values = NULL;

	this->setVisible(false);
	this->registerTouch();
	scheduleUpdate();
}

void ObjItemDetails::show(int pItemID, int pExType, int pType, Sprite* pItem, int pQuantity) {
	mCounter = 1;
	mItemType = pType;
	mItemID = pItemID;
	mExType = pExType;
	mItem = pItem;
	WXmlReader* pXml = WXmlReader::create();
	pXml->load(KEY_XML_FILE);

	this->removeAllChildrenWithCleanup(true);
	LayerColor* layer = LayerColor::create(Color4B(0, 0, 0, 155));
	this->addChild(layer);
	layer->setZOrder(-1);
	layer->setContentSize(Size(CLIENT_WIDTH, CLIENT_HEIGHT));
	layer->setPosition(Vec2(-this->getPositionX() + this->getContentSize().width / 2, -this->getPositionY() + this->getContentSize().height / 2));
	//EGSprite* pTitle = EGSprite::createWithSpriteFrameName(IMG_TITLE_CARTSHOP);
	EGSprite* pTitle = EGSprite::createWithFile("bg_box_avatar_detail.png");
	pTitle->setPosition(Vec2(this->getContentSize().width / 2, this->getContentSize().height));
	this->addChild(pTitle);

	EGSprite* pBorder = EGSprite::createWithSpriteFrameName(IMG_BORDER_ITEM_DETAILS);
	pBorder->setPosition(Vec2(pBorder->getContentSize().width / 2 + 30, this->getContentSize().height / 2));
	this->addChild(pBorder);

	std::string pFileName;
	std::string pNameItem;
	switch (pType){
	case 0:pFileName = __String::createWithFormat("avatar (%d).png", pItemID)->getCString(); pNameItem = AvatarInfo::getName(pItemID); break;
	case 1:pFileName = __String::createWithFormat("be%d.png", pItemID - 100)->getCString(); pNameItem = GunInfo::getName(pItemID);  break;
	case 2:pFileName = __String::createWithFormat("%d.png", pItemID)->getCString(); pNameItem = ItemInfo::getName(pItemID);  break;
	}

	if (pType == 2) {
		Label* pTitleItem = Label::createWithBMFont("font_white.fnt", pNameItem.c_str());
		pTitleItem->setPosition(Vec2(pBorder->getContentSize().width / 2, pBorder->getContentSize().height - pTitleItem->getContentSize().height / 2 - 5));
		pBorder->addChild(pTitleItem);
	}
	else {
		Label* pTitleItem = Label::createWithBMFont("font_black.fnt", pNameItem.c_str());
		pTitleItem->setPosition(Vec2(pBorder->getContentSize().width / 2, pTitleItem->getContentSize().height / 2 + 15));
		pBorder->addChild(pTitleItem);
	}

	Sprite* pImage = Sprite::createWithSpriteFrameName(pFileName.c_str());

	if (pType == 1) {
		/*Sprite* pImage_Gun = Sprite::createWithSpriteFrameName(__String::createWithFormat("s%d.png", pItemID - 100)->getCString());
		pImage_Gun->setPosition(Vec2(, 0));*/
		Sprite* pImage_Number = Sprite::createWithSpriteFrameName(__String::createWithFormat("s%da.png", pItemID - 100)->getCString());
		pImage_Number->setPosition(Vec2(pImage->getContentSize().width / 2, pImage->getContentSize().height - pImage_Number->getContentSize().height / 2));
		pImage->addChild(pImage_Number, 1);

		Sprite* pImage_Button_Prev = Sprite::createWithSpriteFrameName(__String::createWithFormat("d%d.png", pItemID - 100)->getCString());
		pImage_Button_Prev->setPosition(Vec2(pImage_Button_Prev->getContentSize().width / 2, pImage_Button_Prev->getContentSize().height / 2));
		pImage->addChild(pImage_Button_Prev);

		Sprite* pImage_Button_Next = Sprite::createWithSpriteFrameName(__String::createWithFormat("c%d.png", pItemID - 100)->getCString());
		pImage_Button_Next->setPosition(Vec2(pImage->getContentSize().width - pImage_Button_Next->getContentSize().width / 2, pImage_Button_Next->getContentSize().height / 2));
		pImage->addChild(pImage_Button_Next);

		std::string strFile = __String::createWithFormat("ww%d.png", 1 + (pExType - 1) * 3)->getCString();
		if (!mIsShop) {
			strFile = "ww7.png";
		}
		Sprite* pImage_RealNumber = Sprite::createWithSpriteFrameName(strFile);
		pImage_RealNumber->setPosition(Vec2(pImage_Number->getContentSize().width / 2, pImage_Number->getContentSize().height / 2));
		pImage_Number->addChild(pImage_RealNumber);

		Sprite* pImage_Gun = Sprite::createWithSpriteFrameName(__String::createWithFormat("s%d.png", pItemID - 100)->getCString());
		pImage_Gun->setAnchorPoint(Vec2(0.5f, 0));
		if (mIsShop) {
			pImage_Gun->setScale(1 - (3 - pExType)*0.1f);
		}
		pImage_Gun->setPosition(Vec2(pImage_Number->getPositionX(), pImage_Number->getPositionY() - pImage_Number->getContentSize().height / 2 + 10));
		pImage->addChild(pImage_Gun);

		pImage->setScale(0.7f);
	}
	pImage->setPosition(Vec2(pBorder->getContentSize().width / 2, pImage->getContentSize().height*pImage->getScaleY() / 2 + 52));
	pBorder->addChild(pImage);

	float pWidthLeft = (this->getContentSize().width - pBorder->getContentSize().width - 60);

	if (mIsShop) {
		EGButtonSprite* pBtnAdd, *pBtnMinus, *pBtnBuy;

		/*pBtnAdd = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_SHOP_ADD, EGButton2FrameDark);
		pBtnAdd->setOnTouch(CC_CALLBACK_0(ObjItemDetails::changeItemCounter, this, 1));
		pBtnAdd->setPosition(Vec2(2 * pBorder->getContentSize().width / 3 + 20, pBtnAdd->getContentSize().height / 2 + 10));
		pBtnAdd->setVisible(pType != 1 && pType != 0);
		pBorder->addChild(pBtnAdd);

		pBtnMinus = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_SHOP_MINUS, EGButton2FrameDark);
		pBtnMinus->setOnTouch(CC_CALLBACK_0(ObjItemDetails::changeItemCounter, this, -1));
		pBtnMinus->setPosition(Vec2(pBorder->getContentSize().width / 3 - 20, pBtnAdd->getPositionY()));
		pBtnMinus->setVisible(pType != 1 && pType != 0);
		pBorder->addChild(pBtnMinus);*/


		pBtnBuy = EGButtonSprite::createWithFile("btn_use_box_avatar.png", EGButton2FrameDark);
		pBtnBuy->setOnTouch(CC_CALLBACK_0(ObjItemDetails::onBuy, this));
		pBtnBuy->setScale(0.7f);
		pBtnBuy->setPosition(Vec2(this->getContentSize().width - pWidthLeft + pWidthLeft / 3 - pBtnBuy->getContentSize().width / 2 + 25,
			pBorder->getPositionY1() + pBtnBuy->getContentSize().height / 2 - 8));
		this->addChild(pBtnBuy);
	}

	if (pType == 1) {
		std::string pTextSpeed = pXml->getNodeTextByTagName(TEXT_SPEED);
		pTextSpeed += ": ";
		Label* pLblSpeed = Label::createWithBMFont("font_white.fnt", pTextSpeed);
		pLblSpeed->setPosition(Vec2(pBorder->getPositionX2() + pLblSpeed->getContentSize().width / 2 + 50,
			pBorder->getPositionY2() - pLblSpeed->getContentSize().height / 2 - 20));
		this->addChild(pLblSpeed);

		std::string pTextExp = pXml->getNodeTextByTagName(TEXT_EXP);
		pTextExp += ": ";
		Label* pLblExp = Label::createWithBMFont("font_white.fnt", pTextExp);
		pLblExp->setPosition(Vec2(pBorder->getPositionX2() + pLblExp->getContentSize().width / 2 + 50,
			pLblSpeed->getPositionY() - pLblSpeed->getContentSize().height / 2 - pLblExp->getContentSize().height / 2 - 10));
		this->addChild(pLblExp);

		std::stringstream ssSpeed;
		ssSpeed << GunInfo::getSpeed(pItemID);
		std::string pStrSpeed = ssSpeed.str();
		pStrSpeed = "+" + pStrSpeed + "%";
		Label* pLblSpeed_Values = Label::createWithBMFont("font_white.fnt", pStrSpeed);
		pLblSpeed_Values->setColor(Color3B::GREEN);
		pLblSpeed_Values->setPosition(Vec2(pLblSpeed->getPositionX() + pLblSpeed->getContentSize().width / 2 + pLblSpeed_Values->getContentSize().width / 2 + 10, pLblSpeed->getPositionY()));
		this->addChild(pLblSpeed_Values);

		std::stringstream ssExp;
		ssExp << GunInfo::getExp(pItemID);
		std::string pStrExp = ssExp.str();
		pStrExp = "+" + pStrExp + "%";
		Label* pLblExp_Values = Label::createWithBMFont("font_white.fnt", pStrExp);
		pLblExp_Values->setColor(Color3B::GREEN);
		pLblExp_Values->setPosition(Vec2(pLblExp->getPositionX() + pLblExp->getContentSize().width / 2 + pLblExp_Values->getContentSize().width / 2 + 10, pLblExp->getPositionY()));
		this->addChild(pLblExp_Values);

		if (mIsShop) {
			switch (pExType) {
			case 1:
				mRealPrice = GunInfo::getPrice(pItemID) * 1;
				break;
			case 2:
				mRealPrice = GunInfo::getPrice(pItemID) * 4;
				break;
			case 3:
				mRealPrice = GunInfo::getPrice(pItemID) * 20;
				break;
			}

			std::string pTextPrice;
			if (mIsShop)
				pTextPrice = pXml->getNodeTextByTagName(TEXT_PRICE);
			else
				pTextPrice = pXml->getNodeTextByTagName(TEXT_PRICE_SALE);
			pTextPrice += ": ";
			mLblPrice = Label::createWithBMFont("font_white.fnt", pTextPrice);
			mLblPrice->setPosition(Vec2(pBorder->getPositionX2() + mLblPrice->getContentSize().width / 2 + 50,
				pLblExp->getPositionY() - pLblExp->getContentSize().height / 2 - mLblPrice->getContentSize().height / 2 - 10));
			this->addChild(mLblPrice);

			std::string pStrPrice = __String::createWithFormat("%u", mRealPrice)->getCString();
			pStrPrice = EGSupport::addDotMoney(pStrPrice);
			mLblPrice_Values = Label::createWithBMFont("font_white.fnt", pStrPrice);
			mLblPrice_Values->setPosition(Vec2(mLblPrice->getPositionX() + mLblPrice->getContentSize().width / 2 + mLblPrice_Values->getContentSize().width / 2 + 10, mLblPrice->getPositionY()));
			this->addChild(mLblPrice_Values);
		}
		else {
			mRealPrice = GunInfo::getPrice(pItemID) * 20;

			if (pItem->getTag() == 1) {
				mBtnUse = EGButtonSprite::createWithSpriteFrameName("btnusing.png", EGButton2FrameDark);
				mBtnUse->unregisterTouch();
				mBtnUse->setPosition(Vec2(this->getContentSize().width - pWidthLeft + pWidthLeft / 3 - mBtnUse->getContentSize().width / 2,
					pBorder->getPositionY1() + mBtnUse->getContentSize().height / 2));
				this->addChild(mBtnUse);
			}
			else {
				mBtnUse = EGButtonSprite::createWithSpriteFrameName("btnuse.png", EGButton2FrameDark);
				mBtnUse->setOnTouch(CC_CALLBACK_0(ObjItemDetails::onUse, this, pItemID, mItemType));
				mBtnUse->setPosition(Vec2(this->getContentSize().width - pWidthLeft + pWidthLeft / 3 - mBtnUse->getContentSize().width / 2,
					pBorder->getPositionY1() + mBtnUse->getContentSize().height / 2));
				this->addChild(mBtnUse);
			}

			std::string pTextDeadline = pXml->getNodeTextByTagName(TEXT_DEADLINE);
			pTextDeadline += ": ";
			if (pExType != 0) {
				pTextDeadline += __String::createWithFormat("%d", pExType)->getCString();
				pTextDeadline += pXml->getNodeTextByTagName("days");
			}
			else {
				pTextDeadline += pXml->getNodeTextByTagName("unlimited_time");
			}
			Label* pLblExpired = Label::createWithBMFont("font_white.fnt", pTextDeadline);
			pLblExpired->setPosition(Vec2(pBorder->getPositionX2() + pLblExpired->getContentSize().width / 2 + 50,
				pLblExp->getPositionY() - pLblExp->getContentSize().height / 2 - pLblExpired->getContentSize().height / 2 - 10));
			this->addChild(pLblExpired);
		}

		if (mIsShop) {
			std::string pTextDeadline = pXml->getNodeTextByTagName(TEXT_DEADLINE);
			pTextDeadline += ": ";
			switch (pExType) {
			case 1:
				pTextDeadline += pXml->getNodeTextByTagName("7days");
				break;
			case 2:
				pTextDeadline += pXml->getNodeTextByTagName("28days");
				break;
			case 3:
				pTextDeadline += pXml->getNodeTextByTagName("xxdays");
				break;
			}
			Label* pLblExpired = Label::createWithBMFont("font_white.fnt", pTextDeadline);
			pLblExpired->setPosition(Vec2(pBorder->getPositionX2() + pLblExpired->getContentSize().width / 2 + 50,
				mLblPrice->getPositionY() - mLblPrice->getContentSize().height / 2 - pLblExpired->getContentSize().height / 2 - 10));
			this->addChild(pLblExpired);

			std::string pTextCounter = pXml->getNodeTextByTagName(TEXT_COUNTER);
			pTextCounter += ": ";
			mLblCounter = Label::createWithBMFont("font_white.fnt", pTextCounter);
			mLblCounter->setPosition(Vec2(pBorder->getPositionX2() + mLblCounter->getContentSize().width / 2 + 50,
				pLblExpired->getPositionY() - pLblExpired->getContentSize().height / 2 - mLblCounter->getContentSize().height / 2 - 10));
			this->addChild(mLblCounter);

			mLblCounter_Values = Label::createWithBMFont("font_white.fnt", "1");
			mLblCounter_Values->setPosition(Vec2(mLblCounter->getPositionX() + mLblCounter->getContentSize().width / 2 + mLblCounter_Values->getContentSize().width / 2 + 10, mLblCounter->getPositionY()));
			this->addChild(mLblCounter_Values);
		}
	}
	else if (pType == 2) {
		std::string pTextPrice;
		if (mIsShop)
			pTextPrice = pXml->getNodeTextByTagName(TEXT_PRICE);
		else
			pTextPrice = pXml->getNodeTextByTagName(TEXT_PRICE_SALE);
		pTextPrice += ": ";
		mLblPrice = Label::createWithBMFont("font_white.fnt", pTextPrice);
		mLblPrice->setPosition(Vec2(pBorder->getPositionX2() + mLblPrice->getContentSize().width / 2 + 50,
			pBorder->getPositionY2() - mLblPrice->getContentSize().height / 2 - 20));
		this->addChild(mLblPrice);

		mRealPrice = ItemInfo::getPrice(pItemID);
		std::string pStrPrice = __String::createWithFormat("%u", mRealPrice)->getCString();
		pStrPrice = EGSupport::addDotMoney(pStrPrice);
		mLblPrice_Values = Label::createWithBMFont("font_white.fnt", pStrPrice);
		mLblPrice_Values->setPosition(Vec2(mLblPrice->getPositionX() + mLblPrice->getContentSize().width / 2 + mLblPrice_Values->getContentSize().width / 2 + 10, mLblPrice->getPositionY()));
		this->addChild(mLblPrice_Values);

		std::string pTextCounter = pXml->getNodeTextByTagName(TEXT_COUNTER);
		pTextCounter += ": ";
		mLblCounter = Label::createWithBMFont("font_white.fnt", pTextCounter);
		mLblCounter->setPosition(Vec2(pBorder->getPositionX2() + mLblCounter->getContentSize().width / 2 + 50,
			mLblPrice->getPositionY() - mLblPrice->getContentSize().height / 2 - mLblCounter->getContentSize().height / 2 - 10));
		this->addChild(mLblCounter);

		mLblCounter_Values = Label::createWithBMFont("font_white.fnt", __String::createWithFormat("%d", pQuantity)->getCString());
		mLblCounter_Values->setPosition(Vec2(mLblCounter->getPositionX() + mLblCounter->getContentSize().width / 2 + mLblCounter_Values->getContentSize().width / 2 + 10, mLblCounter->getPositionY()));
		this->addChild(mLblCounter_Values);

		std::string pTextDeadline = pXml->getNodeTextByTagName(TEXT_DEADLINE);
		pTextDeadline += ": ";
		if (mIsShop){
			pTextDeadline += pXml->getNodeTextByTagName("7days");
		}
		else{
			pTextDeadline += __String::createWithFormat("%d", pExType)->getCString();
			pTextDeadline += pXml->getNodeTextByTagName("days");
		}
		Label* pLblExpired = Label::createWithBMFont("font_white.fnt", pTextDeadline);
		pLblExpired->setPosition(Vec2(pBorder->getPositionX2() + pLblExpired->getContentSize().width / 2 + 50,
			mLblCounter->getPositionY() - mLblCounter->getContentSize().height / 2 - pLblExpired->getContentSize().height / 2 - 10));
		this->addChild(pLblExpired);
	}
	else if (pType == 0) {
		if (mIsShop) {
			std::string pTextPrice;
			if (mIsShop)
				pTextPrice = pXml->getNodeTextByTagName(TEXT_PRICE);
			else
				pTextPrice = pXml->getNodeTextByTagName(TEXT_PRICE_SALE);
			pTextPrice += ": ";
			Label *mLblPrice = Label::createWithBMFont("font_white.fnt", AvatarInfo::getName(pItemID));
			mLblPrice->setPosition(Vec2(pBorder->getPositionX2() + mLblPrice->getContentSize().width / 2 + 40,
				pBorder->getPositionY2() - mLblPrice->getContentSize().height / 2 - 20));
			this->addChild(mLblPrice);

			/*	mRealPrice = AvatarInfo::getPrice(pItemID);
				std::string pStrPrice = __String::createWithFormat("%u", mRealPrice)->getCString();
				pStrPrice = EGSupport::addDotMoney(pStrPrice);
				Label *mLblPrice_Values = Label::createWithBMFont("font_white.fnt", pStrPrice);
				mLblPrice_Values->setPosition(Vec2(mLblPrice->getPositionX() + mLblPrice->getContentSize().width / 2 + mLblPrice_Values->getContentSize().width / 2 + 10, mLblPrice->getPositionY()));
				this->addChild(mLblPrice_Values);*/

			/*std::string pTextDeadline = pXml->getNodeTextByTagName(TEXT_DEADLINE);
			pTextDeadline += ": ";
			pTextDeadline += pXml->getNodeTextByTagName("7days");*/
			/*Label* pLblExpired = Label::createWithBMFont("font_white.fnt", pTextDeadline);
			pLblExpired->setPosition(Vec2(pBorder->getPositionX2() + pLblExpired->getContentSize().width / 2 + 50,
			mLblPrice->getPositionY() - mLblPrice->getContentSize().height / 2 - pLblExpired->getContentSize().height / 2 - 10));
			this->addChild(pLblExpired);*/
		}
		else {
			std::string pTextDeadline = pXml->getNodeTextByTagName(TEXT_DEADLINE);
			pTextDeadline += ": ";
			pTextDeadline += __String::createWithFormat("%d", pExType)->getCString();
			pTextDeadline += pXml->getNodeTextByTagName("days");
			Label* pLblExpired = Label::createWithBMFont("font_white.fnt", pTextDeadline);
			pLblExpired->setPosition(Vec2(pBorder->getPositionX2() + pLblExpired->getContentSize().width / 2 + 50,
				pBorder->getPositionY2() - pLblExpired->getContentSize().height / 2 - 20));
			this->addChild(pLblExpired);

			if (pItem->getTag() == 1) {
				mBtnUse = EGButtonSprite::createWithSpriteFrameName("btnusing.png", EGButton2FrameDark);
				mBtnUse->unregisterTouch();
				mBtnUse->setPosition(Vec2(this->getContentSize().width - pWidthLeft + pWidthLeft / 3 - mBtnUse->getContentSize().width / 2,
					pBorder->getPositionY1() + mBtnUse->getContentSize().height / 2));
				this->addChild(mBtnUse);
			}
			else {
				mBtnUse = EGButtonSprite::createWithSpriteFrameName("btnuse.png", EGButton2FrameDark);
				mBtnUse->setOnTouch(CC_CALLBACK_0(ObjItemDetails::onUse, this, pItemID, mItemType));
				mBtnUse->setPosition(Vec2(this->getContentSize().width - pWidthLeft + pWidthLeft / 3 - mBtnUse->getContentSize().width / 2,
					pBorder->getPositionY1() + mBtnUse->getContentSize().height / 2));
				this->addChild(mBtnUse);
			}
		}
	}

	EGButtonSprite* pBtnClose = EGButtonSprite::createWithSpriteFrameName(IMG_X);
	pBtnClose->setOnTouch(CC_CALLBACK_0(EGSprite::setVisible, this, false));
	pBtnClose->setPosition(Vec2(this->getContentSize().width, this->getContentSize().height));
	this->addChild(pBtnClose);

	this->setVisible(true);
}


void ObjItemDetails::changeItemCounter(int pIndex) {
	if (mCounter + pIndex <= 0 || mCounter + pIndex >= 100)
		return;
	mCounter += pIndex;

	std::stringstream ssCounter;
	ssCounter << mCounter;
	mLblCounter_Values->setString(ssCounter.str());
	mLblCounter_Values->setPosition(Vec2(mLblCounter->getPositionX() + mLblCounter->getContentSize().width / 2 + mLblCounter_Values->getContentSize().width / 2 + 10, mLblCounter->getPositionY()));

	std::stringstream ssPrice;
	ssPrice << mCounter*mRealPrice;
	std::string pStrPrice = ssPrice.str();
	pStrPrice = EGSupport::addDotMoney(pStrPrice);
	mLblPrice_Values->setString(pStrPrice);
	mLblPrice_Values->setPosition(Vec2(mLblPrice->getPositionX() + mLblPrice->getContentSize().width / 2 + mLblPrice_Values->getContentSize().width / 2 + 10, mLblPrice->getPositionY()));
}

void ObjItemDetails::onBuy() {
	mLoading->show();

	ItemStruct* pItemStruct = new ItemStruct();
	pItemStruct->mItemID = mItemID;
	pItemStruct->mCounter = mCounter;
	pItemStruct->mItemType = mItemType;
	pItemStruct->mExType = mExType;
	mTypeBuy = mItemType;
	 pthread_onBuy( pItemStruct);
}

void pthread_onSale(void* _void) {
	ItemStruct* pItemStruct = (ItemStruct*)_void;


	MpMessage* pRequest = new MpMessage(MP_SALE_ITEM);
	std::stringstream ssType;
	ssType << pItemStruct->mItemType;
	std::stringstream ssItemID;
	ssItemID << pItemStruct->mItemID;
	pRequest->addString(MP_PARAM_TOKENID, GameInfo::mUserInfo.mTokenID);
	pRequest->addString(MP_PARAM_ITEM_ID, ssItemID.str());
	pRequest->addInt(MP_QUANTITY, pItemStruct->mCounter);
	MpClientManager::getInstance()->sendMessage(pRequest);



	delete pItemStruct;
	pItemStruct = NULL;

	pthread_exit(NULL);
}

void ObjItemDetails::onSale() {
	ItemStruct* pItemStruct = new ItemStruct();
	pItemStruct->mItemID = mItemID;
	pItemStruct->mCounter = mCounter;
	pItemStruct->mItemType = mItemType;

	 pthread_onSale(pItemStruct);
}

void ObjItemDetails::update(float dt) {

	if (mErrorcode_ObjItemDetails != "") {
		mLoading->hide();
		this->setVisible(false);
		if (mErrorcode_ObjItemDetails == TEXT_SUCCESS_BUYITEM) {
			//GameInfo::mUserInfo.mBalance -= mRealPrice*mCounter;
			BoxNoticeManager::getBoxNotice(mBoxNotice,
				mXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				mXml->getNodeTextByTagName("success_buyitem_avatar").c_str());
			mBoxNotice->setVisible(true);
			if (mFuncReload)
				mFuncReload();
		}
		else if (mErrorcode_ObjItemDetails == "134267"){
			BoxNoticeManager::getBoxConfirm(mBoxNotice,
				mXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				mXml->getNodeTextByTagName(mErrorcode_ObjItemDetails.c_str()).c_str(), [=]{
				Scene *_scene = MainScene::sceneWithRecharge();
				Director::getInstance()->replaceScene(_scene);
			});
			mBoxNotice->setVisible(true);
		}
		else {
			BoxNoticeManager::getBoxNotice(mBoxNotice,
				mXml->getNodeTextByTagName(TEXT_NOTICE).c_str(),
				mXml->getNodeTextByTagName(mErrorcode_ObjItemDetails.c_str()).c_str());
			mBoxNotice->setVisible(true);
		}
		mErrorcode_ObjItemDetails = "";
	}
}

void ObjItemDetails::onUse(int pItemID, int pItemType)
{
}