#ifndef __GUNPLAYER_H_INCLUDED__
#define __GUNPLAYER_H_INCLUDED__
#include "cocos2d.h"
#include "EGSupport.h"
#include "UserInfo.h"
#include "GameInfo.h"
#include "EGButtonSprite.h"
#include "ClientPlayerInfo.h"
#include "SimpleAudioEngine.h"
#include "BoxChatDisplay.h"
using namespace cocos2d;

class GunPlayer : public  Node {
public :
	static GunPlayer* createGun(int pTypeGame);
	void initGun();
	void fireTo(Vec2 pPointFire);
	void updateType();
	void setType(int pType);
	int getTypeGun();
	void setUserInfo(ClientPlayerInfoEX* info);
	void flipY(bool pBool);
	void refresh(){ setUserInfo(mUserInfo); };
	Rect rect();
	void setOnTouch(std::function<void()> pTouchFunction);
	void setEnableChoose(bool pBool);
	Vec2 getPointFire();
	void ready();
	void active(bool pBool);
	bool isActive(){ return iActive; };
	void unready();
	void changeGun(int pSide);
	void visibeChangeGun(bool pBool);
	void visibeInfo(bool pVisible);
	int getCurrentGun();
	void setCrrentGun(int pType);
	void open(bool pOpen);

	void showChatText(std::string pText);
	void showChatEmo(int index);
	void showInfoMoney(bool pBool){ iconLvel->setVisible(pBool); mLabelLevel->setVisible(pBool); };
	ClientPlayerInfoEX *getUserInf(){ return mUserInfo; };
	void countTime(float pPercent);
	void stopCount(){ pStroke->setVisible(false); }
	void startGame(bool pBool);
	void setButtonXMoney(EGButtonSprite* buttonX){ buttonXMoney = buttonX; }
	int mXFactor;
private:
	int mTypeGame;
	int mTypeGun, mTypeBase,mCurrentGun,mSide;
	
	float mTouchY;
	float mPerCent;
	bool isStartGame;
	EGSprite* buttonSwap;
	Sprite* boxName;
	Sprite* iconMoney, *iconLvel,*mNumSprite;
	Sprite* boxNum;
	Label *mLabelName,*mLabelMoney,*mLabelLevel;
	Sprite* mBoxInfoSprite, *mBigBase, *mSmalBase, *mGun, *boxChoosePosition,* pLabelChoosePosition,*mSpriteReady,*mHostIcon;
	Node* mArrowNode;
	EGButtonSprite *buttonNext,*buttonPrevious,*buttonXMoney;
	ClientPlayerInfoEX* mUserInfo;
	virtual bool ccTouchBegan(Touch* touch, Event* event);
	virtual void ccTouchMove(Touch* touch, Event* event);
	bool containsTouchLocation(Touch* touch),iEnabledTouch,iVisibleInfo,iOpen,iBegan,iActive;
	std::function<void()> mTouchFunction;
	BoxChatDisplay * mBoxChat;
	Sprite* pStroke;
	ProgressTimer *progress;

};

#endif