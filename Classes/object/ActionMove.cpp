#include "ActionMove.h"

ActionMove* ActionMove::createMoveTo(float pSpeed, Vec2 pPositionTo,int pSide){
	ActionMove* action = new ActionMove();
	action->initAction(pSpeed, pPositionTo, pSide);
	return action;
}
void ActionMove::initAction(float pSpeed, Vec2 pPosition, int pSide){
	mSpeed = pSpeed;
	mDestinyPosition = pPosition;
	mSideMoveTo = pSide;
	mType = 1;
	iPause = false;
	prepare();
}
ActionMove* ActionMove::createDelayTime(float pDelayTime){
	ActionMove* action = new ActionMove();
	action->initAction(pDelayTime,false);
	return action;
}
ActionMove* ActionMove::createDelayTimeShield(float pDelayTime){
	ActionMove* action = new ActionMove();
	action->initAction(pDelayTime, true);
	return action;
}
void ActionMove::initAction(float pDelayTime,bool pShield){
	mDelayTime = pDelayTime;
	mType = 4;
	iPause = false;
	prepare();
	iShield = pShield;
}
ActionMove* ActionMove::createMoveCircle(float pSpeed, Vec2 pCenter, float pRadius, float pRotation, int pSide){
	ActionMove* action = new ActionMove();
	action->initAction(pSpeed, pCenter, pRadius, pRotation, pSide);
	return action;
}
ActionMove* ActionMove::createMoveCircle(float pSpeed, Vec2 pCenter, float pRotation, int pSide){
	ActionMove* action = new ActionMove();
	action->initAction(pSpeed, pCenter, -1, pRotation, pSide);
	return action;
}
void ActionMove::initAction(float pSpeed, Vec2 pCenter, float pRadius, float pRotation, int pSide){
	mSpeed = pSpeed;
	mCenterCircle = pCenter;
	mRadius = pRadius;
	mDestinyRotation = pRotation;
	mSide = pSide;
	mType = 3;
	iPause = false;
	prepare();
}

ActionMove* ActionMove::createMoveParaBol(float pDuration, float pHeight, float pWidth){
	ActionMove* action = new ActionMove();
	action->initAction(pDuration, pHeight, pWidth);
	return action;
}
void ActionMove::initAction( float pDuration, float pHeight, float pWidth){
	mHeightParabol = pHeight;
	mWidthParabol = pWidth;
	mDuration = pDuration;
	mType = 5;
	iPause = false;
	prepare();
	
}
ActionMove* ActionMove::createMoveBy(float pSpeed, Vec2 pPosition){
	ActionMove* action = new ActionMove();
	action->initActionMoveBy(pSpeed, pPosition);
	return action;
}
void ActionMove::initActionMoveBy(float pSpeed, Vec2 pPosition){
	mSpeed = pSpeed;
	mMoveBy = pPosition;
	mType = 2;
	iPause = false;
	prepare();
}
ActionMove* ActionMove::createMoveGravity(float pDuration, Vec2 pVecStartSpeed, Vec2 pVecDamping){
	ActionMove* action = new ActionMove();
	action->initAction(pDuration, pVecStartSpeed, pVecDamping);
	return action;
}
void ActionMove::initAction(float pDuration, Vec2 pVecStartSpeed, Vec2 pVecDamping){
	mVecStartSpeed = pVecStartSpeed;
	mDuration = pDuration;
	mVecDamping = pVecDamping;
	mType = 6;
	iPause = false;
	prepare();
}
ActionMove* ActionMove::createMoveSin(float pSpeed, float pWidthSong, float pHeightSong, float pCountSong){
	ActionMove* action = new ActionMove();
	action->initAction(pSpeed, pWidthSong, pHeightSong, pCountSong);
	return action;
}
void ActionMove::initAction(float pSpeed, float pWidthSong, float pHeightSong, float pCountSong){
	mCountSin = pCountSong;
	mSpeed = pSpeed;
	mWidthParabol = pWidthSong;
	mHeightParabol = pHeightSong;
	mSideSin = mHeightParabol / fabsf(mHeightParabol);
	mType = 7;
	iPause = false;
	prepare();
}
ActionMove* ActionMove::setRepeat(int pTimeRepeatn){
	return setRepeat(pTimeRepeatn, false);
}
ActionMove* ActionMove::setRepeat(int pTimeRepeatn,bool pBoolreset){
	mTimeReapet = pTimeRepeatn;
	iReset = pBoolreset;
	return this;
}
void ActionMove::update(float pSec){
	pSec = pSec* mScaleTime;
	if (!iPause){
		if (iComplete && !isComplete()){
			if (mCurrentAction == -1){
				if (mArrayAction.size() > 0){
					mArrayAction.at(0)->setStartPosition(getCurrentPosition());
				}
				mCurrentAction = 0;
			}
			ActionMove* _action = mArrayAction.at(mCurrentAction);
			_action->update(pSec);
			if (_action->isComplete()){
				int pCurrentAction = mCurrentAction + 1;
				Vec2 _startPosition = getCurrentPosition();
				mCurrentAction++;
				if (mCurrentAction >= mArrayAction.size()){
					mCurrentRepeat++;
					int pCurrentRepeat = mCurrentRepeat;
					if (mCurrentRepeat < mTimeReapet){
						mCurrentAction = -1;
						if (!iReset){
							setStartPosition(_startPosition);
						}
						else{
							setStartPosition(mStartPosition);
						}
						prepare();
						mCurrentRepeat = pCurrentRepeat;
					}
					else{
						mCurrentAction--;
					}
				}
				else{
					mArrayAction.at(mCurrentAction)->setStartPosition(_startPosition);
				}
			}
		}
		else{
			bool pCheckComplete = false;
			if (!iComplete){
				pCheckComplete = true;
			}
			if (mRadius == -1){
				mSpeed = (EGSupport::countDistance(mStartPosition.x, mStartPosition.y, mCenterCircle.x, mCenterCircle.y)*M_PI*mDestinyRotation /180)/ mSpeed;
				mCurrentSpeed = mSpeed;
			}
			float pSpace = pSec* mCurrentSpeed;
			mCurrentSpeed += mGiatocAction*pSec;
			if (mType == 1){
				Vec2 pDestiny = mDestinyPosition;
				if (mSideMoveTo == 1){
					pDestiny.y = mCurrentPosition.y;
				}
				else if (mSideMoveTo == 2){
					pDestiny.x = mCurrentPosition.x;
				}
				
				if (EGSupport::countDistance(mCurrentPosition.x, mCurrentPosition.y, pDestiny.x, pDestiny.y) > pSpace){
					mCurrentPosition = EGSupport::getNewPoint(mCurrentPosition.x, mCurrentPosition.y, pDestiny.x, pDestiny.y, pSpace);
				}
				else{
					mCurrentPosition = pDestiny;
					iComplete = true;
				}
			}
			else if (mType == 2){
				float pDistance = sqrtf((mMoveBy.x*mMoveBy.x) + (mMoveBy.y*mMoveBy.y));
				if (mCurrentMove + pSpace <= pDistance){
					mCurrentMove += pSpace;
				}
				else{
					pSpace = pDistance - mCurrentMove;
					mCurrentMove = pDistance;
				}
				mCurrentPosition = EGSupport::getNewPoint(mCurrentPosition.x, mCurrentPosition.y, mCurrentPosition.x + mMoveBy.x, mCurrentPosition.y + mMoveBy.y, pSpace);
				if (mCurrentMove >= pDistance){
					iComplete = true;
				}

			}
			else if (mType == 3){
				if (mRadius == -1){
					mRadius = EGSupport::countDistance(mStartPosition.x, mStartPosition.y, mCenterCircle.x, mCenterCircle.y);
				}
				float pCurrentRotation = EGSupport::getDirectionByRotate(mCenterCircle.x, mCenterCircle.y, mCurrentPosition.x, mCurrentPosition.y);
				float pSpaceRotation = pSpace * 180 / (mRadius*M_PI);
				if (mCurrentRotation + pSpaceRotation <= mDestinyRotation){
					mCurrentRotation += pSpaceRotation;
				}
				else{
					pSpaceRotation = mDestinyRotation - mCurrentRotation;
					mCurrentRotation = mDestinyRotation;
				}
				mCurrentPosition = EGSupport::getPointInCircle(mCenterCircle.x, mCenterCircle.y, mRadius, pCurrentRotation + pSpaceRotation*mSide);
				if (mCurrentRotation >= mDestinyRotation){
					iComplete = true;
				}
			}
			else if (mType == 4){
				mCurrentDelay += pSec;
				if (mCurrentDelay >= mDelayTime){
					iComplete = true;
				}
			}
			else if (mType == 5){
				float pSpeed = sqrtf(mCurrenParabolX*mCurrenParabolX + mCurrentParabolY*mCurrentParabolY);
				pSpace = pSec* pSpeed;
				mCurrentPosition = EGSupport::getNewPoint(mCurrentPosition.x, mCurrentPosition.y, mCurrentPosition.x + mCurrenParabolX, mCurrentPosition.y + mCurrentParabolY, pSpace);
				mCurrentParabolY += mGiatoc*pSec;
				mCurrentMove += pSec;
				if (mCurrentMove >= mDuration){
					iComplete = true;
				}
			}
			else if (mType == 6){
				mCurrentVecSpeed = Vec2(mCurrentVecSpeed.x + mVecDamping.x*pSec, mCurrentVecSpeed.y + mVecDamping.y*pSec);
				mCurrentPosition = Vec2(mCurrentPosition.x + mCurrentVecSpeed.x*pSec, mCurrentPosition.y + mCurrentVecSpeed.y*pSec);
				mCurrentDelay += pSec;
				if (mCurrentDelay >= mDuration){
					iComplete = true;
				}
			}
			else if (mType == 7){
				float pDuration = mWidthParabol / mCurrentSpeed;
				float pSpeedY = fabsf(mHeightParabol) / pDuration;
				float pSideX = mCountSin / fabsf(mCountSin);
				mCurrentMoveX += mCurrentSpeed*pSec;
				mCurrentMoveY += pSpeedY*pSec;
				if (mCurrentMoveY >= fabsf(mHeightParabol)){
					mSideSin = -mSideSin;
					mCurrentMoveY = 0;
				}
				mCurrentPosition = Vec2(mCurrentPosition.x + mCurrentSpeed*pSec*pSideX, mCurrentPosition.y + pSpeedY*pSec*mSideSin);
				if (mCurrentMoveX >= fabsf(mCountSin)*mWidthParabol){
					iComplete = true;
				}
			}

			if (iComplete&& pCheckComplete){
				CallFunc::create(mFuntionOnComplete)->execute();
			}
		}
		if (!iSyncro){
			for (int i = mArrayAction.size() - 1; i >= 0; i--){
				mArrayAction.at(i)->update(pSec);
			}
		}
	}	
}
Vec2 ActionMove::getCurrentPosition(){
	if (mCurrentAction >=0 && mCurrentAction < mArrayAction.size()){
		return mArrayAction.at(mCurrentAction)->getCurrentPosition();
	}
	return mCurrentPosition;
}
bool ActionMove::isShield(){
	return iShield;
}

void ActionMove::prepare(){
	for (int i = mArrayAction.size() - 1; i >= 0; i--){
		mArrayAction.at(i)->prepare();
	}
	mCurrentRepeat = 0;
	mCurrentAction = -1;
	mCurrentRotation = 0;
	mCurrentMove = 0;
	mCurrentDelay = 0;
	if (iScaleTime != 1){
		mScaleTime = 1;
	}
	if (iGiatoc != 1){
		mGiatocAction = 0;
	}
	mCurrentMoveX = 0;
	mCurrentMoveY = 0;
	mCurrentSpeed = mSpeed;
	mCurrentVecSpeed = mVecStartSpeed;
	if (mType == 5){		
		mCurrenParabolX = mWidthParabol / mDuration;
		mGiatoc = -2*mHeightParabol/(mDuration*mDuration/4);
		mCurrentParabolY = mGiatoc*mDuration / 2;
		if (mHeightParabol < 0){
			mCurrentParabolY = -mCurrentParabolY;
		}
	}
	iComplete = false;
	iSyncro = true;
	iShield = false;
}
void ActionMove::clearAll(){
	for (int i = mArrayAction.size() - 1; i >= 0; i--){
		mArrayAction.at(i)->clearAll();
	}
	autorelease();
}
bool ActionMove::isComplete(){
	for (int i = mArrayAction.size() - 1; i >= 0; i--){
		if (!mArrayAction.at(i)->isComplete())
		{
			return false;
		}
	}
	if (mCurrentRepeat < mTimeReapet){
		return false;
	}
	return iComplete;
}
void ActionMove::setSpeed(float pSpeed){
	mSpeed = pSpeed;
}
void ActionMove::nextAction(){
	if (!iComplete){
		iComplete = true;
	}
	else{
		if (mCurrentAction >= 0 && mCurrentAction < mArrayAction.size()){
			mArrayAction.at(mCurrentAction)->forceComplete();
		}
	}
}