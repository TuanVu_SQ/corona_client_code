#pragma once

#include "ClientPlayerInfo.h"
#include "EGButtonSprite.h"
#include "EGSprite.h"
#include "WXmlReader.h"
#include "WClock.h"

class MailView : public EGButtonSprite {
public:
	static MailView* createMailView();
	void initMailView();
	void setInfoMaill(uint8_t type, StructMail pMailInFo);
	StructMail getInfo();
	void changeReadState();
private:
	Label * _title, *_username, *_datetime, *_labelName;
	EGSprite* pMailStatus;
	StructMail mMailInfo;
	WXmlReader* _mXmlreader;
};