#include "BoxChat.h"
#include "GameInfo.h"
#include "WXmlReader.h"

BoxChat* BoxChat::create(std::string pUsername, std::function<void(std::string pContent)> pFuncOnSendChat, std::function<void(std::string pEmotionType, int pEmotionID)> pFuncOnSendEmotion) {
	BoxChat* pBoxChat = new BoxChat();
	pBoxChat->mUsername = pUsername;
	pBoxChat->mFuncOnSendEmotion = pFuncOnSendEmotion;
	pBoxChat->mFuncOnSendChat = pFuncOnSendChat;
	pBoxChat->initObject();
	pBoxChat->autorelease();

	return pBoxChat;
}

void BoxChat::initObject() {
	isOpen = false;
	mBox = EGSprite::createWithSpriteFrameName("chat_box.png");
	mBox->registerTouch(true);
	this->addChild(mBox);

	mBtnController = EGButtonSprite::createWithSpriteFrameName("chat_btn_out.png", EGButton2FrameDark);
	mBtnController->setPosition(Vec2(mBox->getContentSize().width + mBtnController->getContentSize().width / 2
		, mBox->getContentSize().height / 2));
	mBtnController->setOnTouch(CC_CALLBACK_0(BoxChat::onOpen, this));;
	mBox->addChild(mBtnController);

	mBox->setPosition(Vec2(-mBox->getContentSize().width/2, CLIENT_HEIGHT / 2));

	mBtnMenu = new EGButtonSprite*[30];
	
	for (int i = 0; i < 3; i++) {
		std::string fileName = "";
		if (i == 0)
			fileName = __String::createWithFormat("chat_button (%d).png", i + 1)->getCString();
		else
			fileName = __String::createWithFormat("chat_button_off (%d).png", i + 1)->getCString();	
		mBtnMenu[i] = EGButtonSprite::createWithSpriteFrameName(fileName.c_str());
		mBtnMenu[i]->setOnTouch(CC_CALLBACK_0(BoxChat::onChooseWindows, this, i));
		mBtnMenu[i]->setPosition(Vec2(mBtnMenu[i]->getContentSize().width / 2 + 10,
			mBox->getContentSize().height - mBtnMenu[i]->getContentSize().height / 2 - 10 - (mBtnMenu[i]->getContentSize().height + 30)*i));
		mBox->addChild(mBtnMenu[i]);
	}

	mBtnSend = EGButtonSprite::createWithSpriteFrameName("chat_btn_send.png");
	mBtnSend->setPosition(Vec2(mBox->getContentSize().width - mBtnSend->getContentSize().width / 2 - 5, mBtnSend->getContentSize().height / 2 + 10));
	mBtnSend->setOnTouch(CC_CALLBACK_0(BoxChat::onSend, this));
	mBox->addChild(mBtnSend);

	Scale9Sprite* pEdtSprite = Scale9Sprite::createWithSpriteFrameName("chat_edittext.png");
	mEditBox = EditBox::create(Size(pEdtSprite->getContentSize().width, pEdtSprite->getContentSize().height), pEdtSprite);
	mEditBox->setPosition(Vec2(mBtnSend->getPositionX1() - mEditBox->getContentSize().width / 2, mBtnSend->getPositionY()));
	mEditBox->setFontName(FONT);
	mEditBox->setFontSize(20);
	mEditBox->setInputFlag(EditBox::InputFlag::SENSITIVE);
	mEditBox->setFontColor(Color3B::BLACK);
	mEditBox->setMaxLength(REQ_MAXLENGHT_CHAT);
	//mEditBox->setReturnType(EditBox::KeyboardReturnType::DONE);
	mEditBox->setInputMode(EditBox::InputMode::SINGLE_LINE);
	//mEditBox->setOnEnterCallback(CC_CALLBACK_0(BoxChat::onSend, this));
	//mEditBox->setReturnType
	mEditBox->setDelegate(this);
	mBox->addChild(mEditBox);

	mNode = NULL;

	mTableSize = Size(mBox->getContentSize().width - (mBtnSend->getPositionX1() - mEditBox->getContentSize().width - 20) - 30,
		mBox->getContentSize().height - 20 - mBtnMenu[2]->getPositionY1());
	mTableView = TableView::create(this, mTableSize);
	mTableView->setDirection(ScrollView::Direction::VERTICAL);
	mTableView->setPosition(Vec2(mEditBox->getPositionX() - mEditBox->getContentSize().width / 2, mBtnMenu[2]->getPositionY1()));
	mBox->addChild(mTableView);

	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(BoxChat::ccTouchBeganFake, this);
	listener->setSwallowTouches(true);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void BoxChat::editBoxReturn(EditBox* editBox) {
	onSend();
}

bool BoxChat::ccTouchBeganFake(Touch* touch, Event* event) {
	if (isOpen) {
		onClose();
	}
	return isOpen;
}

void BoxChat::onSend() {
	std::string pContent = mEditBox->getText();
	if (pContent == "")
		return;
	mEditBox->setText("");
	onChatText(pContent);
}

void BoxChat::onChatText(std::string pContent) {
	if (mFuncOnSendChat)
		mFuncOnSendChat(pContent);
	updateChat(mUsername, pContent);
}

void BoxChat::onChatEmotion(std::string pEmotionType, int pEmotionID) {
	if (mFuncOnSendEmotion)
		mFuncOnSendEmotion(pEmotionType, pEmotionID);
	updateChatWithEmotion(mUsername, pEmotionType, pEmotionID);
}

void BoxChat::setOnOpen(std::function<void()> pFuncOnOpen) {
	mFuncOnOpen = pFuncOnOpen;
}

void BoxChat::setOnClose(std::function<void()> pFuncOnClose) {
	mFuncOnClose = pFuncOnClose;
}

void BoxChat::updateChat(std::string pName, std::string pContent) {
	/* CREATE CHAT CONTENT */
	Node* pNodeChat = Node::create();
	float pChatHeight;

	std::string pChatContent = pName + ": " + pContent;
	
	Label* pLblName = Label::createWithBMFont(FONT_CHAT_VN, pChatContent, TextHAlignment::LEFT, mTableSize.width - 10);
	pLblName->setColor(Color3B(255, 255, 255));
	if (mUsername != pName) {
		for (int i = 0; i < pLblName->getString().length(); i++) {
			if (i <= pName.length()) {
				pLblName->getLetter(i)->setColor(Color3B(200, 100, 100));
			}
			else {
				break;
			}
		}
	}
	else {
		for (int i = 0; i < pLblName->getString().length(); i++) {
			if (i <= pName.length()) {
				pLblName->getLetter(i)->setColor(Color3B(255, 0, 0));
			}
			else {
				break;
			}
		}
	}
	pLblName->setPosition(Vec2(pLblName->getContentSize().width / 2, pLblName->getContentSize().height / 2));

	pNodeChat->addChild(pLblName);
	mNode[2]->addChild(pNodeChat);

	pChatHeight = pLblName->getContentSize().height + 5;

	mLstChatHeight.push_back(pChatHeight);
	mLstChatObject.push_back(pNodeChat);

	mBtnMenu[2]->performClick();
}

void BoxChat::updateChatWithEmotion(std::string pName, std::string pEmotionType, int pEmotionID) {
	/* CREATE CHAT EMOTION CONTENT */
	Node* pNodeChat = Node::create();
	float pChatHeight;
	
	std::string pChatContent = pName + ": ";
	Label* pLblName = Label::createWithBMFont(FONT_CHAT_VN, pChatContent);
	if (mUsername != pName) {
		pLblName->setColor(Color3B(200, 100, 100));
	}
	else {
		pLblName->setColor(Color3B(255, 0, 0));
	}
	pLblName->setPosition(Vec2(pLblName->getContentSize().width / 2, pLblName->getContentSize().height / 2));

	std::string fileName = __String::createWithFormat("%s_%d (1).png", pEmotionType.c_str(), pEmotionID + 1)->getCString();
	EGSprite* pEmotion = EGSprite::createWithSpriteFrameName(fileName.c_str());
	pEmotion->setTags(0, pEmotionID);
	// NOTICE : THIS CASE NO SET TAG FOR NAME OF EMOTION, IF UPDATE MORE EMOTION MUST SET HERE
	pEmotion->setPosition(Vec2(pLblName->getPositionX() + pLblName->getContentSize().width / 2 + pEmotion->getContentSize().width / 2, pEmotion->getContentSize().height / 2));
	
	pNodeChat->addChild(pEmotion);
	pNodeChat->addChild(pLblName);
	mNode[2]->addChild(pNodeChat);

	pChatHeight = pEmotion->getContentSize().height + 5;

	mLstChatHeight.push_back(pChatHeight);
	mLstChatObject.push_back(pNodeChat);

	mBtnMenu[2]->performClick();
}

void BoxChat::onChooseWindows(int pIndex) {
	for (int i = 0; i < 3; i++) {
		if (i != pIndex) {
			mBtnMenu[i]->setTexture(__String::createWithFormat("chat_button_off (%d).png", i + 1)->getCString());
			mNode[i]->setVisible(false);
		}
		else {
			mBtnMenu[i]->setTexture(__String::createWithFormat("chat_button (%d).png", i + 1)->getCString());
			mNode[i]->setVisible(true);
		}
	}
	mTableView->reloadData();

	if (pIndex == 1) {
		for (int j = 0; j < mNode[1]->getChildrenCount(); j++) {
			EGButtonSprite* pEmotion = (EGButtonSprite*)mNode[1]->getChildren().at(j);

			Animation* pAnimation = Animation::create();
			pAnimation->setDelayPerUnit(0.1f);

			for (int g = 0; g < EMOTION_COUNTER[j]; g++) {
				std::string fileNameAnimation = __String::createWithFormat("a_%d (%d).png", j + 1, g + 1)->getCString();
				pAnimation->addSpriteFrame(SpriteFrameCache::sharedSpriteFrameCache()->getInstance()->spriteFrameByName(fileNameAnimation));
			}

			pEmotion->runAction(RepeatForever::create(Animate::create(pAnimation)));
		}
	} else if (pIndex == 2) {
		float pHeight = 0;
		float pOldY = 0;
		for (int i = mLstChatObject.size() - 1; i >= 0; i--) {
			pHeight += mLstChatHeight.at(i);

			Node* pNodeChat = mLstChatObject.at(i);
			pNodeChat->setPosition(Vec2(0, pOldY));
			pOldY += mLstChatHeight.at(i);
			for (int j = 0; j < pNodeChat->getChildrenCount(); j++) {
				EGSprite* pEmotion = dynamic_cast<EGSprite*>(pNodeChat->getChildren().at(j));
				if (pEmotion) {
					int pEmotionID = pEmotion->getTags(0);

					Animation* pAnimation = Animation::create();
					pAnimation->setDelayPerUnit(0.1f);

					for (int g = 0; g < EMOTION_COUNTER[pEmotionID]; g++) {
						std::string fileNameAnimation = __String::createWithFormat("a_%d (%d).png", pEmotionID + 1, g + 1)->getCString();
						pAnimation->addSpriteFrame(SpriteFrameCache::sharedSpriteFrameCache()->getInstance()->spriteFrameByName(fileNameAnimation));
					}

					pEmotion->runAction(RepeatForever::create(Animate::create(pAnimation)));
				}
			}
		}
		float pContentOffSetHeight = pHeight - mTableSize.height;
		if (pContentOffSetHeight > 0)
			mTableView->setContentOffset(Vec2(mTableView->getContentOffset().x, mTableView->getContentOffset().y + pContentOffSetHeight));
	}
}

void BoxChat::onOpen() {
	mBtnMenu[2]->performClick();
	isOpen = true;
	mBtnController->unregisterTouch();
	mBox->stopAllActions();
	mBox->runAction(Sequence::createWithTwoActions(MoveTo::create(0.25f, Vec2(mBox->getContentSize().width / 2, mBox->getPositionY())),
		CallFunc::create([=] {
		mBtnController->setTexture("chat_btn_in.png");
		mBtnController->registerTouch();
	})));
	if (mFuncOnOpen)
		mFuncOnOpen();
	mBtnController->setOnTouch(CC_CALLBACK_0(BoxChat::onClose, this));;
}

void BoxChat::onClose() {
	isOpen = false;
	mBtnController->unregisterTouch();
	mBox->stopAllActions();
	mBox->runAction(Sequence::createWithTwoActions(MoveTo::create(0.25f,
		Vec2(-mBox->getContentSize().width / 2, mBox->getPositionY())),
		CallFunc::create([=] {
		mBtnController->setTexture("chat_btn_out.png");
		mBtnController->registerTouch();
	})));
	if (mFuncOnClose)
		mFuncOnClose();
	mBtnController->setOnTouch(CC_CALLBACK_0(BoxChat::onOpen, this));;
}

Size BoxChat::tableCellSizeForIndex(TableView *table, ssize_t idx) {
	if (mNode) {
		for (int i = 0; i < 3; i++) {
			if (mNode[i]->isVisible()) {
				switch (i) {
				case 0:
					return Size(200, 59 * REQ_QUICKCHAT_COUNTER);
					break;
				case 1:
					return Size(200, 606);
					break;
				case 2:
					float pHeight = 0;
					for (int i = 0; i < mLstChatHeight.size(); i++) {
						pHeight += mLstChatHeight.at(i);
					}

					return Size(200, pHeight);
					break;
				}
			}
		}
	}
	else {
		return Size(0, 0);
	}
}

TableViewCell* BoxChat::tableCellAtIndex(TableView *table, ssize_t idx) {
	TableViewCell* pCell = table->dequeueCell();
	if (!pCell) {
		pCell = TableViewCell::create();
		mTableViewCell = pCell;

		mNode = new Node*[3];

		for (int i = 0; i < 3; i++) {
			mNode[i] = Node::create();
			mTableViewCell->addChild(mNode[i]);
			if (i == 1) {
				for (int j = 0; j < REQ_EMOTION_COUNTER; j++) {
					std::string fileName = __String::createWithFormat("a_%d (1).png", j + 1)->getCString();
					EGButtonSprite* pEmotion = EGButtonSprite::createWithSpriteFrameName(fileName.c_str(), EGButtonScrollViewItem);
					pEmotion->setOnTouch(CC_CALLBACK_0(BoxChat::onChatEmotion, this, "a", j));
					pEmotion->setPosition(Vec2(pEmotion->getContentSize().width / 2 + 10 + (pEmotion->getContentSize().width + 35)*(j % 4),
						pEmotion->getContentSize().height / 2 + 10 + (pEmotion->getContentSize().height + 20)*(j / 4)));
					mNode[i]->addChild(pEmotion);
				}
			}
			else if (i == 0) {
				WXmlReader* pXml = WXmlReader::create();
				pXml->load("string.xml");

				for (int j = 0; j < REQ_QUICKCHAT_COUNTER; j++) {
					std::string pQuickChat = pXml->getNodeTextByTagName(__String::createWithFormat("quickchat_%d", j + 1)->getCString());

					EGButtonSprite* pBoxQuickChat = EGButtonSprite::createWithSpriteFrameName("chat_boxquick.png", EGButtonScrollViewItem);
					pBoxQuickChat->setOnTouch(CC_CALLBACK_0(BoxChat::onChatText, this, pQuickChat));
					pBoxQuickChat->setPosition(Vec2(pBoxQuickChat->getContentSize().width / 2 + 10,
						pBoxQuickChat->getContentSize().height / 2 + 10 + (pBoxQuickChat->getContentSize().height + 10)*(j)));
					mNode[i]->addChild(pBoxQuickChat);

					Label* pLbl = Label::createWithBMFont(FONT_CHAT_VN, pQuickChat, TextHAlignment::LEFT, pBoxQuickChat->getContentSize().width - 30);
					pLbl->setPosition(Vec2(pLbl->getContentSize().width/2 + 15, pBoxQuickChat->getContentSize().height / 2));
					pBoxQuickChat->addChild(pLbl);
				}
			}
		}
	}
	return pCell;
}

ssize_t BoxChat::numberOfCellsInTableView(TableView *table) {
	return 1;
}