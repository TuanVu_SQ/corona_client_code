#include "BoxResetPass.h"
#include "GameInfo.h"
#include "libs/EGJniHelper.h"
#include "libs/EGSupport.h"


BoxResetPass* BoxResetPass::create() {
	BoxResetPass* pBoxResetPass = new BoxResetPass();
	pBoxResetPass->initObject();
	pBoxResetPass->autorelease();

	return pBoxResetPass;
}

void BoxResetPass::initObject() {
	EGSprite::initWithSpriteFrameName(IMG_BOX_NOTICE);

	LayerColor* pLayer = LayerColor::create(Color4B(0, 0, 0, 200));
	pLayer->setPosition(Vec2(this->getContentSize().width / 2 - CLIENT_WIDTH / 2, this->getContentSize().height / 2 - CLIENT_HEIGHT / 2));
	this->addChild(pLayer, -1);

	pXml = WXmlReader::create();
	pXml->load(KEY_XML_FILE);

	//auto mLbl_Title1 = Label::createWithBMFont("font_white.fnt", pXml->getNodeTextByTagName("txt_enter_otp_to_active"));
	auto mLbl_Title1 = Label::createWithBMFont("font-info-game.fnt", "REYAJISTE MODPAS OU");
	mLbl_Title1->setScale(1.2f);
	mLbl_Title1->setPosition(Vec2(this->getContentSize().width / 2, this->getContentSize().height - mLbl_Title1->getContentSize().height - 15));
	mLbl_Title1->setColor(Color3B::ORANGE);
	this->addChild(mLbl_Title1);

	edtInputPhone = cocos2d::ui::EditBox::create(Size(222, 42), Scale9Sprite::create("editbox_small.png"));
	edtInputPhone->setInputFlag(cocos2d::ui::EditBox::InputFlag::SENSITIVE);
	edtInputPhone->setInputMode(EditBox::InputMode::NUMERIC);
	edtInputPhone->setPosition(Vec2(this->getContentSize().width / 2, this->getContentSize().height / 2));
	edtInputPhone->setFont("fonts/arial.ttf", 18);
	edtInputPhone->setMaxLength(15);
	edtInputPhone->setPlaceholderFont("fonts/arial.ttf", 18);
	edtInputPhone->setFontColor(Color3B::WHITE);
	edtInputPhone->setPlaceHolder(pXml->getNodeTextByTagName("txt_box_repass_enter_phone_holder").c_str());
	edtInputPhone->setPlaceholderFontColor(Color3B(135, 206, 235));
	this->addChild(edtInputPhone, 2);

	mLbl_Title2 = Label::createWithBMFont("font_black.fnt", pXml->getNodeTextByTagName("txt_box_repass_enter_phone"));
	mLbl_Title2->setScale(1.1f);
	mLbl_Title2->setPosition(Vec2(mLbl_Title1->getPosition()) + Vec2(0, -60));
	mLbl_Title2->setColor(Color3B::RED);
	this->addChild(mLbl_Title2);

	mLbl_Des = Label::createWithBMFont("font.fnt", "");
	mLbl_Des->setPosition(Vec2(edtInputPhone->getPosition()) + Vec2(0, -45));
	mLbl_Des->setScale(0.4f);
	mLbl_Des->setColor(Color3B::RED);
	this->addChild(mLbl_Des);

	mBtnAccept = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_GREEN, EGButton2FrameDark);
	mBtnAccept->setPosition(Vec2(this->getContentSize().width / 2 + mBtnAccept->getContentSize().width/2 + 10, mBtnAccept->getContentSize().height / 2 + 20));
	mBtnAccept->setText(pXml->getNodeTextByTagName("accept").c_str(), FONT, 20);
	this->addChild(mBtnAccept);
	mBtnAccept->setOnTouch(CC_CALLBACK_0(BoxResetPass::btnAcceptClicked, this));
	mBtnAccept->setVisible(false);

	mBtnResendOtp = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_GREEN, EGButton2FrameDark);
	mBtnResendOtp->setPosition(Vec2(this->getContentSize().width / 2 - mBtnResendOtp->getContentSize().width/2 - 10, mBtnAccept->getContentSize().height / 2 + 20));
	mBtnResendOtp->setText("Resend otp", FONT, 20);
	this->addChild(mBtnResendOtp);
	mBtnResendOtp->setVisible(false);
	mBtnResendOtp->setOnTouch(CC_CALLBACK_0(BoxResetPass::btnResendClicked, this));

	mBtnSendPhonenumber = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_GREEN, EGButton2FrameDark);
	mBtnSendPhonenumber->setPosition(Vec2(this->getContentSize().width / 2, mBtnAccept->getContentSize().height / 2 + 20));
	mBtnSendPhonenumber->setText("Voye", FONT, 20);
	this->addChild(mBtnSendPhonenumber);
	mBtnSendPhonenumber->setOnTouch([=]{
		std::string phone = edtInputPhone->getText();
		if (phone.length() == 0){
			showDescription(pXml->getNodeTextByTagName("txt_box_repass_err_enter_phone"));
			return;
		} 
		if (_funcSendPhoneNumber)
			_funcSendPhoneNumber(phone);
		phoneEntered = phone;
	});
	//mBtnSendPhonenumber->setVisible(false);

	auto btnClose = EGButtonSprite::createWithFile("btn_close.png");
	this->addChild(btnClose);
	btnClose->setPosition(Vec2(this->getContentSize().width - 10, this->getContentSize().height - 10));
	btnClose->setOnTouch([=]{
		this->setVisible(false);
		this->setPosition(Vec2(0, 1000));
	});


	this->setVisible(false);
	this->registerTouch();
}

void BoxResetPass::btnAcceptClicked()
{
	std::string otp = edtInputPhone->getText();
	if (otp.length() == 0){
		showDescription(pXml->getNodeTextByTagName("txt_box_repass_err_enter_otp"));
		return;
	}
	if (_funcSendOtp)
		_funcSendOtp(phoneEntered, edtInputPhone->getText());

	/*this->setVisible(false);
	this->setPosition(Vec2(1300, 240));*/
}

void BoxResetPass::btnResendClicked()
{
	if (_funcResendOtp)
		_funcResendOtp();
}

void BoxResetPass::showOtpConfirm(){
	edtInputPhone->setText("");
	edtInputPhone->setPlaceHolder(pXml->getNodeTextByTagName("txt_enter_otp").c_str());
	mLbl_Title2->setString(pXml->getNodeTextByTagName("txt_enter_otp_to_active").c_str());
	showDescription("");
	mBtnAccept->setVisible(true);
	mBtnResendOtp->setVisible(true);
	mBtnSendPhonenumber->setVisible(false);
}

void BoxResetPass::ChangePassSuccess(){
	mBtnAccept->setVisible(false);
	mBtnResendOtp->setVisible(false);
	mBtnSendPhonenumber->setVisible(false);
}

void BoxResetPass::showDescription(std::string content){
	if (content.length() > 0){
		mLbl_Des->setString(content);
		mLbl_Des->setVisible(true);
	}
	else
		mLbl_Des->setVisible(false);
}
