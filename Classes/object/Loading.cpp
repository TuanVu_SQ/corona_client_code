#include "Loading.h"
#include "GameInfo.h"

Loading* Loading::create() {
	Loading* pLoading = new Loading();
	pLoading->initObject();
	pLoading->autorelease();

	return pLoading;
}

void Loading::initObject() {
	EGSprite::initWithFile("loading_1.png");

	pLayer = LayerColor::create(Color4B(0, 0, 0, 200));
	pLayer->setPosition(Vec2(this->getContentSize().width / 2 - CLIENT_WIDTH / 2, this->getContentSize().height / 2 - CLIENT_HEIGHT / 2));
	this->addChild(pLayer, -1);

	this->registerTouch();
	this->setVisible(false);
	mSize = Size(CLIENT_WIDTH, CLIENT_HEIGHT);
}

void Loading::show() {
	if (!this->isVisible()) {
		this->setVisible(true);
		this->stopAllActions();

		/*Animation *pAnimation = Animation::create();
		pAnimation->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_LOADSPRITE, 1)->getCString()));
		pAnimation->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_LOADSPRITE, 2)->getCString()));
		pAnimation->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_LOADSPRITE, 3)->getCString()));
		pAnimation->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_LOADSPRITE, 4)->getCString()));
		pAnimation->setDelayPerUnit(0.2f);

		this->runAction(RepeatForever::create(Animate::create(pAnimation)));*/

		Vector<SpriteFrame*> _list;
		for (int i = 0; i < 4; i++){
			std::string filename = __String::createWithFormat("loading_%d.png", i + 1)->getCString();
			SpriteFrameCache *cache = SpriteFrameCache::getInstance();
			SpriteFrame *spriteFrame = cache->getSpriteFrameByName(filename);
			auto sprite = Sprite::create(filename);
			auto frame = SpriteFrame::create(filename, sprite->getTextureRect());
			cache->addSpriteFrame(frame, filename);
			Director::getInstance()->getTextureCache()->removeTextureForKey(filename);

			_list.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(filename));
		}

		Animate* pAnimation = Animate::create(Animation::createWithSpriteFrames(_list, 0.1f));

		this->runAction(RepeatForever::create(pAnimation));
	}
}

void Loading::hide() {
	this->stopAllActions();
	this->setVisible(false);
}
void Loading::setSize(Size pSize){
	mSize = pSize;
	pLayer->setContentSize(mSize);
	pLayer->setPosition(Vec2(this->getContentSize().width / 2 - mSize.width / 2, this->getContentSize().height / 2 - mSize.height/2));
}