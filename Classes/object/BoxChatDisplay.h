#ifndef __BOXCHATDISPLAY_H_INCLUDED__
#define __BOXCHATDISPLAY_H_INCLUDED__

#include "cocos2d.h"
#include "EGSprite.h"

USING_NS_CC;

#define BOXCHATDISPLAY_TYPE_TOPDOWN 1
#define BOXCHATDISPLAY_TYPE_BOTUP 2

class BoxChatDisplay : public Node {
public:
	static BoxChatDisplay* create(int pType);

	void chatWithEmotion(int pEmotionID);
	void chatWithText(std::string pContent);

	void setVisible(bool isVisible);
private:
	int mType;

	EGSprite* mBoxDisplayTop, *mBoxDisplayMiddle, *mBoxDisplayBottom;
	Label* mLabelContentChat;
	EGSprite* mEmotionChat;

	void initObject();
};

#endif