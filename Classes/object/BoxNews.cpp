#include "BoxNews.h"
#include "GameInfo.h"

BoxNew* BoxNew::create() {
	BoxNew* pBoxNew = new BoxNew();
	pBoxNew->initObject();
	pBoxNew->autorelease();

	return pBoxNew;
}

void BoxNew::initObject() {
	LayerColor::initWithColor(Color4B(0, 0, 0, 200));

	mBox = EGSprite::createWithFile("box_news.png");
	mBox->setPosition(Vec2(CLIENT_WIDTH / 2 + 20, CLIENT_HEIGHT / 2));
	this->addChild(mBox);

#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	float pWdith = 500;
	float pHeight = 250;
	mWebView = cocos2d::experimental::ui::WebView::create();
	mWebView->setPosition(Vec2(mBox->getContentSize().width / 2, pHeight / 2 + 50));
	mWebView->setContentSize(Size(pWdith, pHeight));
	mWebView->setScalesPageToFit(true);
	mBox->addChild(mWebView);
#endif

	mImage = EGSprite::createWithFile("image_news.png");
	mImage->setPosition(Vec2(0, mBox->getContentSize().height / 2));
	mBox->addChild(mImage);

	mBtnClose = EGButtonSprite::createWithFile("close.png", EGButton2FrameDark);
	mBtnClose->setOnTouch([=] {
		this->setVisible(false);
	});
	mBtnClose->setPosition(Vec2(mBox->getContentSize().width - 15, mBox->getContentSize().height - 50));
	mBox->addChild(mBtnClose);

	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(BoxNew::ccTouchBeganFake, this);
	listener->setSwallowTouches(true);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	setVisible(false);
}

bool BoxNew::ccTouchBeganFake(Touch* touch, Event* event) {
	return isVisible();
}

void BoxNew::show() {
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	mWebView->loadURL("http://www.google.com");
#endif
}