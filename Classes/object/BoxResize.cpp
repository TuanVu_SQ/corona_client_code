#include "BoxResize.h"

BoxResize* BoxResize::createBoxResize(const char* pFrameTop, const char* pFrameMid, const char* pFrameBot){
	BoxResize* box = new BoxResize();
	box->initBoxResize(pFrameTop, pFrameMid, pFrameBot);
	box->autorelease();
	return box;
}
void BoxResize::initBoxResize(const char* pFrameTop, const char* pFrameMid, const char* pFrameBot){
	Node::init();
	mBoxBody = Sprite::createWithSpriteFrameName(pFrameMid);
	this->addChild(mBoxBody);
	mBoxTop = Sprite::createWithSpriteFrameName(pFrameTop);
	this->addChild(mBoxTop);
	mBoxBot = Sprite::createWithSpriteFrameName(pFrameBot);
	this->addChild(mBoxBot);
	closeBox();
	mDistanceItem = 60;
	iPause = true;
	scheduleUpdate();
	mOffsetY = 0;
	mHeight = -1;
}
void BoxResize::setDistance(float pDistance){
	mDistanceItem = pDistance;
}
void BoxResize::setSize(float pHeight){
	mBoxTop->setPosition(Vec2(0, pHeight*(1 - mBoxBody->getAnchorPoint().y) + mBoxTop->getContentSize().height / 2 ));
	mBoxBot->setPosition(Vec2(0, -pHeight*(mBoxBody->getAnchorPoint().y)  - mBoxBot->getContentSize().height / 2 ));
	mBoxBot->setFlipY(true);
}
void BoxResize::addButton(EGButtonSprite* pButton,int pKey ){
	pButton->setTag(pKey );
	mArrayButton.pushBack(pButton);
	this->addChild(pButton);
}
void BoxResize::visibleObject(int pKey, bool pVisible){
	for (int i = 0; i < mArrayButton.size(); i++){
		if (mArrayButton.at(i)->getTag() == pKey){
			mArrayButton.at(i)->setVisible(pVisible);
		}
	}
}
void BoxResize::clearAllButton(){
	for (int i = mArrayButton.size()-1; i >= 0; i--){
		mArrayButton.at(i)->removeAllChildrenWithCleanup(true);
		mArrayButton.at(i)->removeFromParentAndCleanup(true);
	}
	mArrayButton.clear();
}
void BoxResize::setAnchor(Vec2 pPoint){
	mBoxBody->setAnchorPoint(pPoint);
}
void BoxResize::showBox(){
	float pHeight = 0;
	for (int i = 0; i < mArrayButton.size(); i++){
		if (mArrayButton.at(i)->isVisible()){
			pHeight += mDistanceItem;
		}
	}
	if (mHeight > 0){
		pHeight = mHeight;
	}
	int index = 0;
	for (int i = mArrayButton.size() - 1; i >= 0; i--){
		EGButtonSprite* button = mArrayButton.at(mArrayButton.size() -1 -i);
		button->stopAllActions();
		if (button->isVisible()){
			float pY = pHeight * (1.0f - mBoxBody->getAnchorPoint().y) + mOffsetY - mDistanceItem / 2 - index * mDistanceItem;
			float pX = 0;
			button->runAction(MoveTo::create(0.2f, Vec2(pX, pY)));
			index++;
		}
	}
	float pScale = pHeight / mBoxBody->getContentSize().height;
	if (pScale < 1){
		pScale = 1;
	}
	mBoxBody->stopAllActions();
	mBoxBody->runAction(Sequence::create(ScaleTo::create(0.2f, 1, pScale), CallFunc::create(CC_CALLBACK_0(BoxResize::reload, this)), CallFunc::create(CC_CALLBACK_0(BoxResize::pauseChedule, this)), NULL));
	iPause = false;
	iOpen = true;
}
void BoxResize::visibleStroke(bool pVisible){
	if (!pVisible){
		mBoxBody->setOpacity(0);
		mBoxBot->setOpacity(0);
		mBoxTop->setOpacity(0);
	}
	else{
		mBoxBody->setOpacity(255);
		mBoxBot->setOpacity(255);
		mBoxTop->setOpacity(255);
	}
}
void BoxResize::closeBox(){
	for (int i = mArrayButton.size() - 1; i >= 0; i--){
		int index = mArrayButton.size() - 1 - i;
		EGButtonSprite* button = mArrayButton.at(index);
		button->stopAllActions();
		button->runAction(Sequence::create(MoveTo::create(0.2f, Vec2(0, 0)),NULL));
	}
	float pScale = 1;
	mBoxBody->stopAllActions();
	mBoxBody->runAction(Sequence::create(ScaleTo::create(0.2f, 1, pScale), CallFunc::create(CC_CALLBACK_0(BoxResize::reload, this)), CallFunc::create(CC_CALLBACK_0(BoxResize::pauseChedule, this)), NULL));
	iPause = false;
	iOpen = false;
}
void BoxResize::update(float pSec){
	if (!iPause){
		setSize(mBoxBody->getScaleY()*mBoxBody->getContentSize().height);
	}
}
bool BoxResize::isOpen(){
	return iOpen;
}
void BoxResize::reload(){
	setSize(mBoxBody->getScaleY()*mBoxBody->getContentSize().height);
}
