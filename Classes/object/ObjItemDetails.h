#ifndef __OBJITEMDETAILS_H_INCLUDED__
#define __OBJITEMDETAILS_H_INCLUDED__

#include "cocos2d.h"
#include "EGSprite.h"
#include "EGButtonSprite.h"

#include "BoxNotice.h"
#include "Loading.h"
#include "BoxNotice.h"

#include "WXmlReader.h"

USING_NS_CC;



class ObjItemDetails :public EGSprite {
public:
	 int mTypeBuy ;
	 std::string mErrorcode_ObjItemDetails;
	static ObjItemDetails* create(bool pIsShop, Loading* pLoading, BoxNotice *pBoxNotice, std::function<void()> pReloadFunc);
	static ObjItemDetails* create(bool pIsShop, Loading* pLoading, BoxNotice *pBoxNotice);

	void initInventory(std::function<void()> pFunctionUseItem) {
		mFunctionUseItem = pFunctionUseItem;
	}
	void show(int pItemID, int pExType, int pType, Sprite* pItem, int pQuantity);
	uint32_t getItemId(){
		return mItemID;
	}
private:
	std::function<void()> mFunctionUseItem;
	std::function<void()> mFuncReload;
	bool mIsShop;
	CustomCommand _customCommand;
	Label* mLblCounter, *mLblPrice;
	Label* mLblCounter_Values, *mLblPrice_Values;
	Label* mTitle;
	uint32_t mRealPrice, mCounter, mItemID, mItemType, mExType;
	Loading* mLoading;
	BoxNotice* mBoxNotice;
	EGButtonSprite* mBtnUse;
	WXmlReader *mXml;
	Sprite* mItem;
	

	void initObject(bool pIsShop, Loading* pLoading, BoxNotice *pBoxNotice);

	void changeItemCounter(int pIndex);
	virtual bool ccTouchBegan(Touch* touch, Event* event) {
		return this->isVisible();
	}
	void onUse(int pItemID, int pItemType);
	void onBuy();
	void onSale();

	void update(float dt);
};

#endif