#include "BoxNotice.h"
#include "GameInfo.h"

BoxNotice* BoxNotice::create() {
	BoxNotice* pBoxNotice = new BoxNotice();
	pBoxNotice->initObject();
	pBoxNotice->autorelease();

	return pBoxNotice;
}

void BoxNotice::initObject() {
	EGSprite::initWithSpriteFrameName(IMG_BOX_NOTICE);

	LayerColor* pLayer = LayerColor::create(Color4B(0, 0, 0, 200));
	pLayer->setPosition(Vec2(this->getContentSize().width / 2 - CLIENT_WIDTH / 2, this->getContentSize().height / 2 - CLIENT_HEIGHT / 2));
	this->addChild(pLayer, -1);

	mLbl_Title = Label::createWithBMFont("font_black.fnt", "");
	mLbl_Title->setColor(Color3B::BLACK);
	this->addChild(mLbl_Title);

	mLbl_Content = Label::createWithBMFont("font_black.fnt", "", TextHAlignment::CENTER, this->getContentSize().width - 100);
	mLbl_Content->setColor(Color3B::BLACK);
	this->addChild(mLbl_Content);

	mEdt_Input = EditBox::create(Size(312, 53), Scale9Sprite::createWithSpriteFrameName(IMG_EDITTEXT));
	mEdt_Input->setVisible(false);
	mEdt_Input->setFontName(FONT);
	mEdt_Input->setFontSize(20);
	//mEdt_Input->setMargins(20, 20);
	mEdt_Input->setInputFlag(EditBox::InputFlag::SENSITIVE);
	mEdt_Input->setFontColor(Color3B::WHITE);
	mEdt_Input->setPlaceholderFontColor(Color3B::GRAY);
	mEdt_Input->setMaxLength(REQ_MAXLENGHT_USERNAME);
	mEdt_Input->setReturnType(EditBox::KeyboardReturnType::DONE);
	this->addChild(mEdt_Input);

	mBtnAccept = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_GREEN, EGButton2FrameDark);
	mBtnAccept->setPosition(Vec2(this->getContentSize().width/4, mBtnAccept->getContentSize().height/2 + 20));
	mBtnAccept->setVisible(false);
	this->addChild(mBtnAccept);

	mBtnReject = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_GREEN, EGButton2FrameDark);
	mBtnReject->setPosition(Vec2(2 * this->getContentSize().width / 4, mBtnReject->getContentSize().height / 2 + 20));
	mBtnReject->setVisible(false);
	this->addChild(mBtnReject);

	mBtnSkip = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_GREEN, EGButton2FrameDark);
	mBtnSkip->setPosition(Vec2(3 * this->getContentSize().width / 4, mBtnSkip->getContentSize().height / 2 + 20));
	mBtnSkip->setVisible(false);
	this->addChild(mBtnSkip);

	mEdt_Input->setPosition(Vec2(this->getContentSize().width / 2, mBtnAccept->getPositionY2() + mEdt_Input->getContentSize().height / 2 + 20));

	this->setVisible(false);
	this->registerTouch();
}