#include "Fish.h"

Fish* Fish::createFish(const char* pFile){
	Fish* _fish = new Fish();
	_fish->initFish(pFile);
	_fish->autorelease();
	return _fish;
}
void Fish::initFish(const char* pFile){
	mFishFake = NULL;
	mObject = NULL;
	mSpecialSprite = NULL;
	Node::init();
	mFileName = pFile;
	mFishBody = Sprite::create();
	this->addChild(mFishBody);
	mFishBody->setScale(0.5f);
	reset();
}
void Fish::setType(int pType){
	if (pType > 10)
		mFishBody->setScale(mFishBody->getScale() + (pType-10)*0.1f/2);
	if (pType != 0){
		/*mFishBody->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(mFileName.c_str(), pType, 1)->getCString()));*/
		mFishBody->setTexture(__String::createWithFormat(mFileName.c_str(), pType, 1)->getCString());
	}
	else{
		mFishBody->setTexture(Director::getInstance()->getTextureCache()->addImage(__String::createWithFormat(mFileName.c_str(), pType, 1)->getCString()));
	}
	mType = pType;
	if (mObject){
		delete mObject;
	}
	mObject = FISHOBJECT_LIST[mType]->copy();
	mFishBody->runAction(RepeatForever::create(Sequence::create(DelayTime::create(rand()%20*0.1f), ScaleTo::create(0.8f, mFishBody->getScale() - 0.06f), ScaleTo::create(0.8f, mFishBody->getScale() + 0.06f), NULL)));
}
void Fish::scaleSpeed(float pScale, float pTime){
	mScaleSpeed = 0;
	mTimeScale = pTime/2;
	mScalePerSec = pScale*2 / pTime;
	//mFishBody->getScheduler()->setTimeScale(1 + pScale / 2);
}
int Fish::getType(){
	return mType;
}
void Fish::reset(){
	mFishBody->setOpacity(255);
	mFishBody->stopAllActions();
	mFishBody->setScale(0.5f);
	mID = "";
	_dameGun = 0;
	mDameMage = 0;
	mCurrentTime = 0;
	mScaleSpeed = 0;
	mTimeScale = 0;
	mScalePerSec = 0;
	iDead = false;
	iPause = false;
	iDestroy = false;
	iTrap = false;
	mListAnimation = NULL;
	mPower = 0;
	mCurrentPower = 0;
	if (mFishFake){
		for (int i = mFishFake->listWay.size() - 1; i >= 0; i--){
			FishPoint * pPoint = mFishFake->listWay.at(i);
			mFishFake->listWay.pop_back();
			delete pPoint;
		}
		if (this->getUserData()){
			std::vector<Vec2*>* pListMove = (std::vector<Vec2*>*)this->getUserData();
			for (int i = pListMove->size() - 1; i >= 0; i--){
				Vec2 * pPoint = pListMove->at(i);
				pListMove->pop_back();
				delete pPoint;
			}
			delete pListMove;
			this->setUserData(NULL);
		}
	}
	mFishFake = new FishFake();
}
void Fish::swim(){
		/*mListAnimation = Animation::create();
		mListAnimation->retain();
		for (int i = 0; i < mObject->getCountFrameSwim(); i++){
			if (mType != 0){
				mListAnimation->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(mFileName.c_str(), mType, i + 1)->getCString()));
			}
			else{
				mListAnimation->addSpriteFrameWithFileName((__String::createWithFormat(mFileName.c_str(), mType, i + 1)->getCString()));
			}
		}
		mListAnimation->setDelayPerUnit(0);
		mListAnimation->setLoops(0);*/
}
void Fish::startMove(){
	swim();
	std::vector<Vec2*>* pListMove = (std::vector<Vec2*>*)this->getUserData();
	setRotation(EGSupport::getDirectionByRotate(this->getPositionX(), this->getPositionY(), pListMove->at(pListMove->size() - 1)->x, pListMove->at(pListMove->size() - 1)->y) - 90);
	mFishFake->rotation = getRotation() + 90;
}

void Fish::move(float pSec){
	if (!iPause && this->getUserData()){
		do{
			drawWay(0.01f);
		} while (mFishFake && mFishFake->currentTime  <mCurrentTime + pSec&&this->getUserData());
		//mListAnimation->setDelayPerUnit(mListAnimation->getDelayPerUnit() + pSec*(1+mScaleSpeed*6));
		/*if (mListAnimation->getDelayPerUnit() >0.2f){
			int pFrameIndex = mListAnimation->getLoops();
			mFishBody->setSpriteFrame(mListAnimation->getFrames().at(pFrameIndex)->getSpriteFrame());
			pFrameIndex++;
			if (pFrameIndex >= mListAnimation->getFrames().size()){
				pFrameIndex = 0;
			}
			mListAnimation->setLoops(pFrameIndex);
			mListAnimation->setDelayPerUnit(0);
		}*/
		
		float pDistance = pSec*mObject->getSpeed();
		if (mPower>0){
			if (mScaleSpeed <= 0){
				mCurrentPower -= pDistance;
			}
			if (mCurrentPower <= 0){
				if (mTypeGame == 2){
					if (getType() > 4){
						scaleSpeed(0.5f + 0.1f*(rand() % 5), 2.5f + 0.1f*(rand() % 5));
					}
					else{
						if (rand() % 2 == 0){
							scaleSpeed(0.5f + 0.1f*(rand() % 10), 2.5f + 0.1f*(rand() % 10));
						}
					}
				}
				else{
					int pID = atoi(mID.c_str()) % 10;
					scaleSpeed(1.0f + 0.1f*(pID), 2.5f + 0.1f*(pID )*1.25f);
				}
				mCurrentPower = mPower;
			}
		}
		mCurrentTime += pSec;	
		while (mFishFake->listWay.size() > 0){
			FishPoint* pPoint = mFishFake->listWay.at(0);
			if (pPoint->time + 0.01f > mCurrentTime){
				break;
			}
			else{
				mFishFake->listWay.erase(mFishFake->listWay.begin());
				this->setPosition(pPoint->position);
				this->setRotation(pPoint->rotation - 90);
				delete pPoint;
			}
		}
		float pMaxDistance = std::max(mFishBody->getContentSize().width / 2, mFishBody->getContentSize().height / 2);
		if (fabsf(400 - this->getPosition().x) > 400 + pMaxDistance || fabsf(240 - this->getPosition().y) > 240 + pMaxDistance){
			if (iDestroy&& isVisible()){
				destroy();
			}
		}
	}
}
void Fish::trap(Vec2 pPosition){
	std::vector<Vec2*>* pListMove = (std::vector<Vec2*>*)this->getUserData();
	mListMoveTrap.clear();
	for (int i = 0; i < pListMove->size(); i++){
		mListMoveTrap.push_back(*pListMove->at(i));
	}
	for (int i = pListMove->size() - 1; i >= 0; i--){
		Vec2* pPoint = pListMove->at(i);
		pListMove->erase(pListMove->begin() + i);
		delete pPoint;
	}
	pListMove->push_back(new Vec2(pPosition));
	iTrap = true;

}
void Fish::unTrap(){
	iTrap = false;
	std::vector<Vec2*>* pListMove = (std::vector<Vec2*>*)this->getUserData();
	if (pListMove){
		for (int i = pListMove->size() - 1; i >= 0; i--){
			Vec2* pPoint = pListMove->at(i);
			pListMove->erase(pListMove->begin() + i);
			delete pPoint;
		}
		for (int i = 0; i < mListMoveTrap.size(); i++){
			pListMove->push_back(new Vec2(mListMoveTrap.at(i)));
		}
	}
}
Rect Fish::rect(){
	return Rect(this->getPositionX() - mFishBody->getContentSize().width / 2, this->getPositionY() - mFishBody->getContentSize().height / 2, mFishBody->getContentSize().width, mFishBody->getContentSize().height);
}

void Fish::dead(){
	iDead = true;
	iPause = true;
	mFishBody->stopAllActions();
	int pStartFrame = 1;
	int pLength = 1;
	pStartFrame = mObject->getCountFrameSwim();
	pLength = mObject->getCountFrameDead();
	/*Animation* _animationDead = Animation::create();
	for (int i = 0; i < pLength; i++){
		int indexFrame = pStartFrame + i;
		if (mType != 0){
			_animationDead->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(mFileName.c_str(), mType, indexFrame)->getCString()));
		}
		else{
			_animationDead->addSpriteFrameWithFileName((__String::createWithFormat(mFileName.c_str(), mType, indexFrame)->getCString()));
		}
	}
	_animationDead->setDelayPerUnit(0.1f);
	mFishBody->runAction(Repeat::create(Animate::create(_animationDead), 3));*/
	mFishBody->runAction(Sequence::create(DelayTime::create(0.4f), FadeOut::create(0.3f), CallFunc::create(CC_CALLBACK_0(Fish::destroy, this)), NULL));
}
bool Fish::isDead(){
	return iDead;
}
void Fish::hit(float pDame){
	mDameMage += pDame;
}
void Fish::setDame(float pDame){
	mDameMage = pDame;
}
float Fish::getDame(){
	return mDameMage;
}
void Fish::stun(float pDelay){
	stopAllActions();
	runAction(Sequence::createWithTwoActions(DelayTime::create(pDelay), CallFunc::create(CC_CALLBACK_0(Fish::resume, this))));
	iPause = true;
}
void Fish::drawWay(float pSec){
	if (!iPause && this->getUserData()){
		std::vector<Vec2*>* pListMove = (std::vector<Vec2*>*)this->getUserData();
		if (pListMove->size() > 0){
			float pSpeed = mObject->getSpeed();
			if (mTimeScale >0  || mScaleSpeed >0){
				pSpeed += pSpeed*mScaleSpeed;
				mScaleSpeed += mScalePerSec*pSec;
				mTimeScale -= pSec;
				if (mTimeScale <= 0){
					if (mScalePerSec > 0){
						mScalePerSec = -mScalePerSec;
					}
				}
			}
			else{
				mScaleSpeed = 0;
				mTimeScale = 0;
				mScalePerSec = 0;
			}
			float pCurrentRotation = mFishFake->rotation;
			pCurrentRotation = EGSupport::absRotation(pCurrentRotation);
			Vec2 pVec = ccpForAngle(pCurrentRotation*M_PI / 180);
			pVec = Vec2(pVec.y, pVec.x);
			float pAngular = 40;
			float pRadius = pSpeed * 180 / (M_PI * pAngular);
			if (isTrap()){
				pAngular += 40 + rand() % 20;
			}

			Vec2* pPointTo = pListMove->at(pListMove->size() - 1);
			float pRotationTo = 90 - ccpToAngle(Vec2(pPointTo->x - mFishFake->position.x, pPointTo->y - mFishFake->position.y)) * 180 / M_PI;

			pRotationTo = EGSupport::absRotation(pRotationTo);

			int pSide = -1;
			if (pRotationTo - pCurrentRotation > 0){
				pSide = 1;
			}
			if (fabsf(pRotationTo - pCurrentRotation) > 180){
				pSide *= -1;
			}

			Vec2 pCenter;
			if (pSide > 0){
				pCenter = EGSupport::getNewPoint(mFishFake->position.x, mFishFake->position.y, mFishFake->position.x + pVec.y, mFishFake->position.y - pVec.x, pRadius);
			}
			else{
				pCenter = EGSupport::getNewPoint(mFishFake->position.x, mFishFake->position.y, mFishFake->position.x - pVec.y, mFishFake->position.y + pVec.x, pRadius);
			}
			if (ccpDistance(pCenter, *pPointTo) < pRadius){
				if (pListMove->size() <= 1){
					timerDestroy();
				}
			}

			if (fabsf(pRotationTo - pCurrentRotation) < pSec * pAngular){
				if (ccpDistance(*pPointTo, mFishFake->position) < pSec* pSpeed){
					if (fabsf(pRotationTo - pCurrentRotation) < 0.0002f){
						mFishFake->position = *pPointTo;
					}
					if (pListMove->size() > 1){
						Vec2* pVec = pListMove->at(pListMove->size() - 1);
						pListMove->pop_back();
						delete pVec;
					}
				}
				mFishFake->rotation = pRotationTo;
			}
			else{
				bool isReset = false;
				for (int i = pListMove->size() - 1; i >= 0; i--){
					Vec2* pPoint = pListMove->at(i);
					if (ccpDistance(pCenter, *pPoint) >= pRadius){
						break;
					}
					else{
						if (pListMove->size() > 1){
							isReset = true;
							Vec2* pVec = pListMove->at(pListMove->size() - 1);
							pListMove->pop_back();
							delete pVec;
						}
					}
				}
				if (isReset){
					drawWay(pSec);
					return;
				}
				mFishFake->rotation = mFishFake->rotation + pSec*pSide * pAngular;
			}
			pCurrentRotation = mFishFake->rotation;
			pCurrentRotation = EGSupport::absRotation(pCurrentRotation);
			pVec = ccpForAngle(pCurrentRotation*M_PI / 180);
			pVec = Vec2(pVec.y, pVec.x);
			mFishFake->currentTime += pSec;
			mFishFake->position = EGSupport::getNewPoint(mFishFake->position.x, mFishFake->position.y, mFishFake->position.x + pVec.x, mFishFake->position.y + pVec.y, pSec* pSpeed);
			mFishFake->listWay.push_back(new FishPoint(mFishFake->position, mFishFake->rotation, mFishFake->currentTime));
			if (pListMove->size() == 0){
				this->setUserData(NULL);
				delete pListMove;
			}
		}
	}
}
void Fish::upgrade(){
	if (mObject){
		mObject->setSpecial(true);
	}
	if (!mSpecialSprite){
		mSpecialSprite = Sprite::create("special_fish1.png");
		this->addChild(mSpecialSprite);
		mSpecialSprite->setZOrder(-1);
		mSpecialSprite->runAction(RepeatForever::create(RotateBy::create(3, 360)));
		mSpecialSprite->setScale(1.1f);
	}
	int index = 1;
	if (mType > 18){
		index = 3;
	}
	else if (mType > 16){
		index = 2;
	}
	mSpecialSprite->setTexture(__String::createWithFormat("special_fish%d.png", index)->getCString());
	mSpecialSprite->setPositionX(10);
}