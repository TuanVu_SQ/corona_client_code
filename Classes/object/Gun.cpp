#include "Gun.h"
#include "BoxResize.h"

GunPlayer* GunPlayer::createGun(int pTypeGame){
	GunPlayer* pGun = new GunPlayer();
	pGun->mTypeGame = pTypeGame;
	pGun->initGun();
	pGun->autorelease();
	return pGun;
}
void GunPlayer::initGun(){
	Node::init();
	mTypeBase = 1;
	mTypeGun = 7;
	mCurrentGun = 7;
	mSide = 1;
	mXFactor = 1;
	mHostIcon = NULL;
	mUserInfo = NULL;
	iVisibleInfo = true;
	iEnabledTouch = true;
	iBegan = false;
	iActive = true;
	iOpen = true;
	isStartGame = false;
	mPerCent = 0;
	mBoxChat = NULL;
	buttonXMoney = NULL;
	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(GunPlayer::ccTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(GunPlayer::ccTouchMove, this);
	listener->setSwallowTouches(true);
	//_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	mBigBase = Sprite::create("big-base-onl.png");
	if (mTypeGame == 2){
		mBigBase = EGButtonSprite::createWithFile("big-base-onl.png");
	}
	this->addChild(mBigBase);
	mBigBase->setAnchorPoint(Vec2(0.5f, 0));
	mBigBase->setPosition(Vec2(0,-mSide*mBigBase->getContentSize().height));
	mBigBase->setZOrder(1);
	if (mTypeGame == 1){
		pStroke = Sprite::create("stroke-time-ready.png");
		this->addChild(pStroke);
		pStroke->setPositionY(-90);
		pStroke->setRotation(-48);
		progress = ProgressTimer::create(Sprite::create("bar-time-ready.png"));
		pStroke->addChild(progress);
		progress->setPosition(Vec2(pStroke->getContentSize().width / 2, pStroke->getContentSize().height / 2));
		progress->setType(ProgressTimerType::RADIAL);
		progress->setPercentage(100);
	}
	buttonNext = EGButtonSprite::createWithFile("next-onl.png");
	if (mTypeGame == 2){
		buttonNext = EGButtonSprite::createWithFile("next-onl.png");
	}
	mBigBase->addChild(buttonNext);
	buttonNext->setVisible(false);
	buttonNext->setOnTouch(CC_CALLBACK_0(GunPlayer::changeGun, this,1));
	buttonNext->setPosition(Vec2(mBigBase->getContentSize().width - buttonNext->getContentSize().width / 2, buttonNext->getContentSize().height / 2));
	buttonNext->setButtonType(EGButton2FrameDark);
	buttonPrevious = EGButtonSprite::createWithFile("previous-onl.png");
	if (mTypeGame == 2){
		buttonPrevious = EGButtonSprite::createWithFile("previous-onl.png");
	}
	buttonPrevious->setVisible(false);
	buttonPrevious->setOnTouch(CC_CALLBACK_0(GunPlayer::changeGun, this, -1));
	mBigBase->addChild(buttonPrevious);
	buttonPrevious->setPosition(Vec2( buttonPrevious->getContentSize().width / 2, buttonPrevious->getContentSize().height / 2));
	buttonPrevious->setButtonType(EGButton2FrameDark);
	mSmalBase = Sprite::create("small-base-onl.png");
	if (mTypeGame == 2){
		mSmalBase = EGButtonSprite::createWithFile("small-base-onl.png");
	}
	mSmalBase->setAnchorPoint(Vec2(0.5f, 0));
	mBigBase->addChild(mSmalBase);
	mSmalBase->setPosition(Vec2(mBigBase->getContentSize().width / 2, 0));
	mGun = Sprite::create("gun-onl (1).png");
	if (mTypeGame == 2){
		mGun = EGButtonSprite::createWithFile("gun-onl (1).png");
	}
	mSmalBase->addChild(mGun);
	mGun->setVisible(false);
	mGun->setPosition(Vec2(mSmalBase->getContentSize().width / 2, 32));
	mBoxInfoSprite = Sprite::createWithSpriteFrameName(IMG_BOX_INFO_PLAYER_GAME);
	mGun->setTag(2);
	mBoxInfoSprite->setVisible(false);
	mBoxInfoSprite->setAnchorPoint(Vec2(1, 0));
	this->addChild(mBoxInfoSprite);
	boxChoosePosition = Sprite::createWithSpriteFrameName(__String::createWithFormat(IMG_BOX_CHOOSEPOSITION,1)->getCString());
	this->addChild(boxChoosePosition);
	boxChoosePosition->setAnchorPoint(Vec2(0.5f, 0));
	Animation *animation = Animation::create();
	for (int i = 1; i < 3; i++){
		animation->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat(IMG_BOX_CHOOSEPOSITION, i)->getCString()));
	}
	animation->setDelayPerUnit(0.2f);
	boxChoosePosition->runAction(RepeatForever::create(Animate::create(animation)));
	pLabelChoosePosition = Sprite::createWithSpriteFrameName(IMG_TEXT_CHOOSEPOSITION);
	pLabelChoosePosition->setPosition(Vec2(boxChoosePosition->getContentSize().width / 2, 28));
	boxChoosePosition->addChild(pLabelChoosePosition);
	boxName = Sprite::createWithSpriteFrameName(IMG_BOX_NAME_GAME);
	mBigBase->addChild(boxName);
	boxName->setPosition(Vec2(mBigBase->getContentSize().width / 2, boxName->getContentSize().height/2));

	mLabelName = Label::createWithBMFont(FONT_NAME_GAME,"asdasd");
	boxName->addChild(mLabelName);
	mLabelName->setAlignment(kCCTextAlignmentCenter);
	mLabelName->setPosition(Vec2(boxName->getContentSize().width / 2, boxName->getContentSize().height / 2));
	mLabelName->setVisible(false);

	if (mTypeGame == 1){
		iconMoney = Sprite::create(IMG_ICON_MONEY_GAME);
	}
	else{
		iconMoney = Sprite::create(IMG_ICON_LEVEL_GAME);
	}
	this->addChild(iconMoney);
	iconMoney->setPosition(Vec2(-160, 35));
	mLabelMoney = Label::createWithBMFont(FONT_MONEY_GAME, "");
	mLabelMoney->setString("1000000");
	iconMoney->addChild(mLabelMoney);
	iconMoney->setVisible(false);
	mLabelMoney->setPositionY(iconMoney->getContentSize().height / 2);

	mSpriteReady = Sprite::createWithSpriteFrameName(IMG_LABEL_READY);
	this->addChild(mSpriteReady);
	mSpriteReady->setZOrder(10);
	mSpriteReady->setPositionY(70);
	mSpriteReady->setVisible(false);

	iconLvel = Sprite::create(IMG_ICON_LEVEL_GAME);
	this->addChild(iconLvel);
	iconLvel->setPosition(Vec2(-160, 15));
	mLabelLevel = Label::createWithBMFont(FONT_MONEY_GAME, "");
	mLabelLevel->setString("10");
	iconLvel->addChild(mLabelLevel);
	iconLvel->setVisible(false);
	mLabelLevel->setPositionY(iconLvel->getContentSize().height / 2);

	buttonSwap = EGSprite::createWithSpriteFrameName("button-swap (1).png");
	this->addChild(buttonSwap);
	buttonSwap->setAnchorPoint(Vec2(0.5f, 0));
	buttonSwap->setPositionY(3);
	buttonSwap->registerTouch(true);
	Animation* pAni = Animation::create();
	for (int i = 2; i >=0; i--){
		pAni->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat("button-swap (%d).png", i + 1)->getCString()));
	}
	pAni->setDelayPerUnit(0.2f);
	buttonSwap->runAction(RepeatForever::create(Animate::create(pAni)));
	
	//buttonSwap->setButtonType(EGButton2FrameDark);

	mArrowNode = Node::create();
	boxChoosePosition->addChild(mArrowNode);
	mArrowNode->setPosition(Vec2(boxChoosePosition->getContentSize().width / 2, boxChoosePosition->getContentSize().height - 5));
	Sprite* pArrowTop = Sprite::createWithSpriteFrameName(IMG_ARROW_TOP);
	mArrowNode->addChild(pArrowTop);
	Sprite* pArrowBot = Sprite::createWithSpriteFrameName(IMG_ARROW_BOT);
	mArrowNode->addChild(pArrowBot);
	pArrowBot->setPosition(Vec2(0, -pArrowTop->getContentSize().height / 2 - pArrowBot->getContentSize().height / 2));
	pArrowBot->runAction(RepeatForever::create(Sequence::createWithTwoActions(MoveBy::create(0.3f, Vec2(0, -5)), MoveBy::create(0.3f, Vec2(0, 5)))));
	pArrowTop->runAction(RepeatForever::create(Sequence::createWithTwoActions(MoveBy::create(0.3f, Vec2(0, 5)), MoveBy::create(0.3f, Vec2(0, -5)))));
	mArrowNode->setVisible(false);

	 boxNum = Sprite::create();
	mSmalBase->addChild(boxNum);
	boxNum->setPosition(Vec2(mSmalBase->getContentSize().width / 2, 25));
	boxNum->setPositionY(boxNum->getPositionY() + 12);
	 mNumSprite = Sprite::create();
	boxNum->addChild(mNumSprite);
	mNumSprite->setPosition(Vec2(boxNum->getContentSize().width / 2, boxNum->getContentSize().height / 2));
	boxNum->runAction(Hide::create());
	updateType();
	open(false);
}
void GunPlayer::startGame(bool pBool){
	isStartGame = pBool;
	if (!pBool){
		Animation* animation = Animation::create();
		for (int i = 0; i < 3; i++){
			animation->addSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat("button-swap (%d).png", i + 1)->getCString()));
		}
		animation->setDelayPerUnit(0.2f);
		buttonSwap->runAction(RepeatForever::create(Animate::create(animation)));
	}
	else{
		buttonSwap->stopAllActions();
		buttonSwap->setTexture("button-swap (0).png");
	}
}
void GunPlayer::countTime(float pPercent){
	pStroke->setVisible(true);
	progress->setPercentage((pPercent * 100 / 360) * 100);
	mPerCent = pPercent;
}
void GunPlayer::active(bool pBool){
	iActive = pBool;
	mGun->stopAllActions();
	mGun->setRotation(0);

}
void GunPlayer::setType(int pType){
	mTypeGun = pType;
	mTypeBase = pType;
	updateType();
}
void GunPlayer::unready(){
	mSpriteReady->setVisible(false);
}
void GunPlayer::ready(){
	mSpriteReady->setVisible(true);
}
void GunPlayer::visibeChangeGun(bool pBool){
	buttonNext->setVisible(pBool);
	buttonPrevious->setVisible(pBool);
}
void GunPlayer::updateType(){
	int pCurrentGun = mCurrentGun;
	if (mUserInfo){
		pCurrentGun = mUserInfo->gunType;
	}
	if (mTypeGame != 2){
		mBigBase->setTexture(Director::getInstance()->getTextureCache()->addImage("big-base-onl.png"));
		mSmalBase->setTexture(Director::getInstance()->getTextureCache()->addImage("small-base-onl.png"));
		mGun->setTexture(Director::getInstance()->getTextureCache()->addImage(String::createWithFormat("gun-onl (%d).png", pCurrentGun)->getCString()));
		buttonNext->setTexture(Director::getInstance()->getTextureCache()->addImage("next-onl.png"));
		buttonPrevious->setTexture(Director::getInstance()->getTextureCache()->addImage("previous-onl.png"));
	}
	else{
		mBigBase->setTexture(Director::getInstance()->getTextureCache()->addImage("big-base-onl.png"));
		mSmalBase->setTexture(Director::getInstance()->getTextureCache()->addImage("small-base-onl.png"));
		mGun->setTexture(Director::getInstance()->getTextureCache()->addImage(String::createWithFormat("gun-onl (%d).png", pCurrentGun)->getCString()));
		boxNum->setVisible(false);
		buttonNext->setTexture(Director::getInstance()->getTextureCache()->addImage("next-onl.png"));
		buttonPrevious->setTexture(Director::getInstance()->getTextureCache()->addImage("previous-onl.png"));
	}
	//boxNum->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(String::createWithFormat("s%da.png", mTypeGun)->getCString()));
	//mNumSprite->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(String::createWithFormat("ww%d.png", mUserInfo->gunType )->getCString()));
	mGun->setAnchorPoint(Vec2(0.5f, 20 / mGun->getContentSize().height));
	if (mTypeGame != 2){
		/*mGun->setScale(0.85f + 0.015f*mUserInfo->gunType );
		boxNum->setScale(0.85f + 0.015f*mUserInfo->gunType );*/
	}
	else{
		mGun->setScale(1);
		boxNum->setScale(1);
	}
}
void GunPlayer::fireTo(Vec2 pPointFire){
	if (iActive){
		Sprite* pGun = (Sprite*)mSmalBase->getChildByTag(2);
		float pRotation = EGSupport::getDirectionByRotate(this->getPositionX(), this->getPositionY() + 25 * mSide, pPointFire.x, pPointFire.y);
		if (mSide == -1){
			pRotation += 180;
		}
		pGun->stopAllActions();
		pGun->runAction(RotateTo::create(0.1f, pRotation));
	}
}
void GunPlayer::setUserInfo(ClientPlayerInfoEX *info){
	if (info){
		if (info->username == GameInfo::mUserInfo.mFullname){
			mLabelName->setBMFontFilePath(FONT_NAME_GAME1);
			if (buttonXMoney){
				buttonXMoney->setTexture("rate_battle_box1.png");
				buttonXMoney->registerTouch();
				buttonXMoney->getChildByTag(1)->setVisible(true);
			}
		}
		else{
			mLabelName->setBMFontFilePath(FONT_NAME_GAME);
			if (buttonXMoney){
				buttonXMoney->setTexture("rate_battle_box2.png");
				buttonXMoney->unregisterTouch();
				buttonXMoney->getChildByTag(1)->setVisible(false);
			}
		}
		if (info->host){
			if (!mHostIcon){
				mHostIcon = Sprite::createWithSpriteFrameName("icon-host.png");
				this->addChild(mHostIcon);
			}
		
			mHostIcon->setVisible(true);
			//mHostIcon->setAnchorPoint(Vec2(0.5f, 0));
			mHostIcon->setPosition(Vec2(-mBoxInfoSprite->getContentSize().width+ 76 + mHostIcon->getContentSize().width / 2, mSide* mBoxInfoSprite->getContentSize().height / 2));
			if (!iVisibleInfo){
				mHostIcon->setPosition(Vec2(-120, mSide* mHostIcon->getContentSize().height / 2));
				mHostIcon->setZOrder(10);
			}
			//mHostIcon->setScale(5);
		}
		else{
			if (mHostIcon){
				mHostIcon->setVisible(false);
			}
		}
		mLabelName->setString(info->username);
		mGun->setVisible(true);
		mGun->setTexture(Director::getInstance()->getTextureCache()->addImage(String::createWithFormat("gun-onl (%d).png", info->gunType)->getCString()));
		mLabelName->setVisible(true);
		mBigBase->setVisible(true);
		mBigBase->stopAllActions();
		if (!mUserInfo || mBigBase->getPositionY() != 0){
			mBigBase->runAction(MoveTo::create(0.2f, Vec2(0, 0)));
		}
		mBoxInfoSprite->setVisible(iVisibleInfo);
		mLabelMoney->setString(__String::createWithFormat("%d", info->balance)->getCString());
		std::string pScoreString = __String::createWithFormat("%d", info->score)->getCString();
		if (pScoreString.length() > 6){
			pScoreString = pScoreString.substr(0, pScoreString.length() - 3) + "K+";
		}
		mLabelLevel->setString(pScoreString);
		boxName->setVisible(true);
		if (this->getPositionY() > CLIENT_HEIGHT / 2) {
			if (!iconLvel->isFlippedY()) {
				iconLvel->setPosition(Vec2(-160, -15));
				iconMoney->setPosition(Vec2(-160, -35));
				iconLvel->setFlippedY(true);
				iconMoney->setFlippedY(true);
			}
		}
		mLabelMoney->setPositionX(mLabelMoney->getParent()->getContentSize().width + mLabelMoney->getContentSize().width / 2);
		mLabelLevel->setPositionX(mLabelLevel->getParent()->getContentSize().width + mLabelLevel->getContentSize().width / 2);
		if (buttonXMoney){
			buttonXMoney->setVisible(true);
		}
		/*if (mTypeGame == 0){
			if (iVisibleInfo){
				((BoxResize*)buttonXMoney->getChildByTag(1))->visibleObject(info->indexX, false);
				((Sprite*)buttonXMoney->getChildByTag(10))->setTexture(__String::createWithFormat("rate_battle_label%d.png", info->indexX)->getCString());
			}
		}*/
		if (mTypeGame == 1){
			mLabelLevel->getParent()->setVisible(iVisibleInfo);
			if (buttonXMoney){
				buttonXMoney->setVisible(false);
			}
			
		}
		if (mTypeGame == 2 || mTypeGame == 0){
			boxNum->setVisible(false);
			boxName->setVisible(false);
			buttonXMoney->setVisible(false);
		}
		mLabelMoney->getParent()->setVisible(iVisibleInfo);
		setType(info->itemId-100);
		setEnableChoose(false);
	}
	else{
		if (buttonXMoney){
			buttonXMoney->setVisible(false);
		}
		if (mTypeGame == 1){
			pStroke->setVisible(false);
		}
		if (mHostIcon){
			mHostIcon->setVisible(false);
		}
		mBoxInfoSprite->setVisible(false);
		mBigBase->setVisible(false);
		mSpriteReady->setVisible(false);
		setEnableChoose(true);
		mGun->setVisible(false);
		mLabelLevel->getParent()->setVisible(false);
		mLabelMoney->getParent()->setVisible(false);
	}
	mUserInfo = info;
	updateType();
}
void GunPlayer::visibeInfo(bool pVisible){
	iVisibleInfo = pVisible;
}
int GunPlayer::getTypeGun(){
	return mTypeGun;
}
Vec2 GunPlayer::getPointFire(){
	return Vec2(this->getPositionX(), this->getPositionY() + 32*mSide);
}
void GunPlayer::flipY(bool pBool){
	if (pBool){
		mSide = -1;
		boxChoosePosition->setRotation(180);
		mBigBase->setRotation(180);
		mBoxInfoSprite->setAnchorPoint(Vec2(1, 1));
		mLabelName->setRotation(180);
		mBigBase->setPosition(Vec2(0, -mSide*mBigBase->getContentSize().height));
		pLabelChoosePosition->setRotation(180);
		buttonSwap->setAnchorPoint(Vec2(0.5f, 1));
		buttonSwap->setPosition(Vec2(0, - 3));
		if (mTypeGame == 1){
			pStroke->setRotation(180 - 48);
			pStroke->setPositionY(90);
		}
		mLabelMoney->getParent()->setPositionY(mLabelMoney->getParent()->getPositionY() - mBoxInfoSprite->getContentSize().height);
		mLabelLevel->getParent()->setPositionY(mLabelLevel->getParent()->getPositionY() - mBoxInfoSprite->getContentSize().height);
		mSpriteReady->setPositionY(-70);
		open(false);
		if (!mBoxChat){
			mBoxChat = BoxChatDisplay::create(BOXCHATDISPLAY_TYPE_TOPDOWN);
			this->addChild(mBoxChat);
			mBoxChat->setZOrder(5);
		}
	}
	else{
		if (!mBoxChat){
			mBoxChat = BoxChatDisplay::create(BOXCHATDISPLAY_TYPE_BOTUP);
			this->addChild(mBoxChat);
			mBoxChat->setZOrder(5);
		}
	}
}
void GunPlayer::showChatText(std::string pText){
	mBoxChat->chatWithText(pText);
}
void GunPlayer::showChatEmo(int index){
	mBoxChat->chatWithEmotion(index);
}
Rect GunPlayer::rect(){
	Rect rect = Rect( - 160,- ((mSide*-1 + 1) / 2) * 80, 320, 80);
	return rect;
}
void GunPlayer::setOnTouch(std::function<void()> pTouchFunction) {
	buttonSwap->setOnTouch(pTouchFunction);
	buttonSwap->setAlwayReturnFalse(true);
}
bool GunPlayer::containsTouchLocation(Touch* touch) {
	return rect().containsPoint(convertTouchToNodeSpaceAR(touch));
}
bool GunPlayer::ccTouchBegan(Touch* touch, Event* event) {
	if (containsTouchLocation(touch) && isVisible() && this->getParent()->isVisible()&& iEnabledTouch) {
		if (this->getParent()->getParent() != NULL && this->getParent()->getParent()->isVisible()) {
			mTouchY = touch->getLocation().y;
			iBegan = true;
			return true;
		}
	}
	return false;
}
void GunPlayer::ccTouchMove(Touch* touch, Event* event) {
	if (abs(touch->getLocation().y - mTouchY) > 50 && iBegan){
		iBegan = false;
		open(!iOpen);
	}
}
void GunPlayer::open(bool pBool){
	iOpen = pBool;
	boxChoosePosition->stopAllActions();
	if (iOpen){
		boxChoosePosition->runAction(MoveTo::create(0.25f, Vec2(0, 0)));
	}
	else{
		boxChoosePosition->runAction(MoveTo::create(0.25f, Vec2(0, -60*mSide)));
	}
}
void GunPlayer::setEnableChoose(bool pBool){
	iEnabledTouch = pBool;
	if (mTypeGame != 2){
		buttonSwap->setOpacity((pBool)* 255);
		if (mTypeGame == 0){
			buttonSwap->setVisible(false);
		}
	}else
	if (mTypeGame == 2){
		boxChoosePosition->runAction(Hide::create());
		boxChoosePosition->setOpacity(122);
		buttonSwap->setVisible(false);
	}
	/*boxChoosePosition->runAction(Hide::create());
	boxChoosePosition->setOpacity(122);*/
}
void GunPlayer::changeGun(int pSide){
	if (UserDefault::getInstance()->getBoolForKey(KEY_SOUND, true)) {
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(SOUND_CHANGE_GUN);
	}

	mUserInfo->gunType  += pSide;
	if (mUserInfo->gunType  < 1){
		if (mTypeGame == 2){
			mUserInfo->gunType  = 10;
			int pUpgradeDame = 0;
			int pUpgradeSpeed = 0;
			do{
				mUserInfo->gunType --;
				int pDefault = 0;
				if (mUserInfo->gunType  >= 1 && mUserInfo->gunType  <= 7){
					pDefault = 1;
				}
				pUpgradeDame = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_DAME_GUN, mUserInfo->gunType )->getCString(), pDefault);
				pUpgradeSpeed = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_SPEED_GUN, mUserInfo->gunType )->getCString(), pDefault);
			} while (pUpgradeDame == 0 && pUpgradeSpeed == 0);
		}
		else{
			mUserInfo->gunType  = 7;
		}
	}
	if (mUserInfo->gunType >7) {
		if (mTypeGame != 2) {
			mUserInfo->gunType  = 7;
		}
		else {
			int pUpgradeDame = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_DAME_GUN, mUserInfo->gunType )->getCString(), 0);
			int pUpgradeSpeed = UserDefault::getInstance()->getIntegerForKey(__String::createWithFormat(KEY_SPEED_GUN, mUserInfo->gunType )->getCString(), 0);
			if (pUpgradeDame > 0 || pUpgradeSpeed > 0){
			}
			else{
				mUserInfo->gunType  = 1;
			}
		}
	}
	mCurrentGun= mUserInfo->gunType ;
	updateType();
}

int GunPlayer::getCurrentGun(){ 
	if (mUserInfo){
		return mUserInfo->gunType;
	}
	return mCurrentGun; 
}
void GunPlayer::setCrrentGun(int pType)
{
	if (mUserInfo){
		mUserInfo->gunType = pType;
	}
	mCurrentGun = pType;
}