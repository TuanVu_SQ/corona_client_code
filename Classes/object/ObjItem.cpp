#include "ObjItem.h"
#include "GameInfo.h"
#include "GunInfo.h"
#include "ItemInfo.h"
#include "AvatarInfo.h"

ObjItem* ObjItem::create(unsigned int pItemID, int pExType, int pType, bool isShop) {
	ObjItem* pObjItem = new ObjItem();
	pObjItem->initObject(pItemID, pExType, pType, isShop);
	pObjItem->autorelease();

	return pObjItem;
}

void ObjItem::initObject(unsigned int pItemID, int pExType, int pType, bool isShop) {
	if (isShop)
		EGButtonSprite::initWithSpriteFrameName(IMG_BORDER_ITEM);
	else
		EGButtonSprite::initWithSpriteFrameName(IMG_BORDER_ITEM_INVENTORY);
		
	this->setButtonType(EGButton2FrameDark);

	std::string pFileName;
	std::string pName;
	switch (pType){
	case 0:
		pFileName = __String::createWithFormat("avatar (%d).png", pItemID)->getCString();
		pName = AvatarInfo::getName(pItemID); 
		break;
	case 1:
		if (isShop) 
			pFileName = __String::createWithFormat("be%d.png", pItemID - 100)->getCString();
		else
			pFileName = __String::createWithFormat("be%d.png", pItemID - 100)->getCString();
		pName = GunInfo::getName(pItemID);
		break;
	case 2:
		pFileName = __String::createWithFormat("%d.png", pItemID)->getCString();
		pName = ItemInfo::getName(pItemID);
		break;
	}
	Sprite* pImage = Sprite::createWithSpriteFrameName(pFileName.c_str());
	if (pType == 1) {
		/*Sprite* pImage_Gun = Sprite::createWithSpriteFrameName(__String::createWithFormat("s%d.png", pItemID - 100)->getCString());
		pImage_Gun->setPosition(Vec2(, 0));*/
		Sprite* pImage_Number = Sprite::createWithSpriteFrameName(__String::createWithFormat("s%da.png", pItemID - 100)->getCString());
		pImage_Number->setPosition(Vec2(pImage->getContentSize().width / 2, pImage->getContentSize().height - pImage_Number->getContentSize().height / 2));
		pImage->addChild(pImage_Number, 1);

		Sprite* pImage_Button_Prev = Sprite::createWithSpriteFrameName(__String::createWithFormat("d%d.png", pItemID - 100)->getCString());
		pImage_Button_Prev->setPosition(Vec2(pImage_Button_Prev->getContentSize().width / 2, pImage_Button_Prev->getContentSize().height / 2));
		pImage->addChild(pImage_Button_Prev);

		Sprite* pImage_Button_Next = Sprite::createWithSpriteFrameName(__String::createWithFormat("c%d.png", pItemID - 100)->getCString());
		pImage_Button_Next->setPosition(Vec2(pImage->getContentSize().width - pImage_Button_Next->getContentSize().width / 2, pImage_Button_Next->getContentSize().height / 2));
		pImage->addChild(pImage_Button_Next);

		std::string strFile = __String::createWithFormat("ww%d.png", 1 + (pExType - 1) * 3)->getCString();
		if (!isShop) {
			strFile = "ww7.png";
		}
		Sprite* pImage_RealNumber = Sprite::createWithSpriteFrameName(strFile);
		pImage_RealNumber->setPosition(Vec2(pImage_Number->getContentSize().width / 2, pImage_Number->getContentSize().height / 2));
		pImage_Number->addChild(pImage_RealNumber);

		Sprite* pImage_Gun = Sprite::createWithSpriteFrameName(__String::createWithFormat("s%d.png", pItemID - 100)->getCString());
		pImage_Gun->setAnchorPoint(Vec2(0.5f, 0));
		if (isShop) {
			pImage_Gun->setScale(1 - (3 - pExType)*0.1f);
		}
		pImage_Gun->setPosition(Vec2(pImage_Number->getPositionX(), pImage_Number->getPositionY() - pImage_Number->getContentSize().height / 2 + 10));
		pImage->addChild(pImage_Gun);
		
		pImage->setScale(0.7f);
	}
	pImage->setPosition(Vec2(this->getContentSize().width / 2, pImage->getContentSize().height*pImage->getScaleY() / 2 + 13));
	this->addChild(pImage);

	if (!isShop) {
		pImage->setScale(0.45f);
		pImage->setPosition(Vec2(this->getContentSize().width / 2, pImage->getContentSize().height*pImage->getScaleY() / 2 + 26));
	}
	if (pType == 2 || pType == 0) {
		pImage->setPositionY(pImage->getPositionY() + 30);
	}

	if (!isShop) {
		//pName += __String::createWithFormat(" (%d)", pQuantity)->getCString();
	}

	std::string pFont = "font_black.fnt";
	if (isShop) {
		//pFont = "font_shop.fnt";
	}
	Label* pTitle = Label::createWithBMFont(pFont.c_str(), pName);
	if (isShop) {
		pTitle->setPosition(Vec2(this->getContentSize().width / 2,
			this->getContentSize().height - pTitle->getContentSize().height));
		//pTitle->setColor(Color3B::BLACK);
	}
	else {
		pTitle->setScale(0.8f);
		pTitle->setPosition(Vec2(this->getContentSize().width / 2,15));
		pTitle->setColor(Color3B::WHITE);
	}
	this->addChild(pTitle);
}