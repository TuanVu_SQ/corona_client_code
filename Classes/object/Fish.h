#ifndef __FISH_H_INCLUDED__
#define __FISH_H_INCLUDED__
#include "cocos2d.h"
#include "EGSupport.h"
#include "ActionMove.h"
#include "FishObject.h"

using namespace cocos2d;
static FishObject* FISHOBJECT_LIST[] = {
FishObject::createFishObject(100, 0, 60, 12, 0),
FishObject::createFishObject(120, 1, 110, 12, 6), 
FishObject::createFishObject(110, 3, 100, 12, 3), 
FishObject::createFishObject(70, 6, 90, 12, 3),
FishObject::createFishObject(90, 10, 100, 10, 3),
FishObject::createFishObject(60, 12, 90, 6, 4),
FishObject::createFishObject(60, 15, 80, 11, 6),
FishObject::createFishObject(10, 20, 60, 12, 4),
FishObject::createFishObject(10, 22, 60, 8, 4),
FishObject::createFishObject(100, 30, 50, 12, 0),
FishObject::createFishObject(100, 32, 50, 8, 0),
FishObject::createFishObject(100, 40, 40, 19, 0),
FishObject::createFishObject(100, 42, 40, 12, 0),
FishObject::createFishObject(100, 50, 50, 8, 4),
FishObject::createFishObject(100, 52, 50, 12, 4),
FishObject::createFishObject(100, 100, 60, 12, 4),
FishObject::createFishObject(100, 110, 60, 8, 4),
FishObject::createFishObject(100, 200, 60, 12, 4), 
FishObject::createFishObject(100, 210, 60, 8, 4),
FishObject::createFishObject(100, 250, 60, 15, 0), 
FishObject::createFishObject(100, 280, 60, 12, 0) };
typedef struct FishPoint_s
{
	Vec2 position;
	float rotation;
	float time;
	FishPoint_s(Vec2 pPoint, float pRotation, float pTime){ position = pPoint; time = pTime; rotation = pRotation; }
} FishPoint;
typedef struct FishFake_s
{
	Vec2 position;
	float rotation;
	float currentTime;
	std::vector<FishPoint*> listWay;
	FishFake_s(){ currentTime = 0; rotation = 0; };
} FishFake;

class Fish : public Node {
public:
	static Fish* createFish(const char *pFile);
	void initFish(const char *pFile);
	void setType(int pType);
	int getType();
	void startMove();
	void swim();
	void dead();
	void upgrade();
	void setDame(float pDame);
	float getDame();
	void scaleSpeed(float pScale, float pTime);
	void timerDestroy(){
		iDestroy = true;
	}
	void destroy(){
		iDead = true;
		setVisible(false);
		if (mSpecialSprite){
			mSpecialSprite->removeFromParentAndCleanup(true);
			mSpecialSprite = NULL;
		}
		if (mObject){
			delete mObject;
			mObject = NULL;			
		}
		if (mFishFake){
			for (int i = mFishFake->listWay.size() - 1; i >= 0; i--){
				FishPoint * pPoint = mFishFake->listWay.at(i);
				mFishFake->listWay.pop_back();
				delete pPoint;
			}
			if (this->getUserData()){
				std::vector<Vec2*>* pListMove = (std::vector<Vec2*>*)this->getUserData();
				for (int i = pListMove->size() - 1; i >= 0; i--){
					Vec2 * pPoint = pListMove->at(i);
					pListMove->pop_back();
					delete pPoint;
				}
				delete pListMove;
				this->setUserData(NULL);
			}
		}
		mFishFake = NULL;
	}
	bool isDestroy(){ return iDestroy; }
	void stun(float pDelay);
	void resume(){ iPause = false; }
	Size getContentSizeBody(){ return mFishBody->getContentSize(); }
	void hit(float pDame);
	void move(float pSec);
	void drawWay(float pSec);
	float getDameMage(){ return mDameMage; }
	void reset();
	Rect rect();
	bool isDead();
	Sprite* getBody(){ return mFishBody; }
	bool isTrap(){ return iTrap; }
	void unTrap();
	void setPower(float pPower){ mPower = pPower; mCurrentPower = pPower; }
	void setCurrentPower(float pPower){
		mCurrentPower = pPower;
	}
	void trap(Vec2 pPosition);
	void setStartPoint(Vec2 pStartPoint){ mFishFake->position = pStartPoint; this->setPosition(pStartPoint);	 };
	void setID(const char* pID){ 
		mID = pID;
	}
	std::string getId(){ return mID; }
	FishObject* getObject(){ return mObject; }
	void setTypeGame(int pType){ mTypeGame = pType; };
private:
	CC_SYNTHESIZE(float, _dameGun, DameGun);
	std::vector<Vec2> mListMoveTrap;
	FishObject *mObject;
	FishFake *mFishFake;
	Sprite* mFishBody,*mSpecialSprite;
	Vec2 mStartPoint;
	Animation* mListAnimation;
	int mType;
	int mTypeGame;
	float mCurrentTime, mScaleSpeed, mTimeScale,mScalePerSec;
	std::string mID;
	float mDameMage;
	float mPower,mCurrentPower;
	bool iDead,iDestroy,iTrap;
	std::string mFileName;
	bool iPause;
};
#endif