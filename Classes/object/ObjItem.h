#ifndef __OBJITEM_H_INCLUDED__
#define __OBJITEM_H_INCLUDED__

#include "cocos2d.h"
#include "EGButtonSprite.h"
#include "EGSprite.h"

USING_NS_CC;

class ObjItem :public EGButtonSprite {
public:
	static ObjItem* create(unsigned int pItemID,int pExType, int pType, bool isShop);
private:
	void initObject(unsigned int pItemID, int pExType, int pType, bool isShop);
};

#endif