#ifndef __BOXNEW_H_INCLUDED__
#define __BOXNEW_H_INCLUDED__

#include "cocos2d.h"
#include "EGSprite.h"
#include "EGButtonSprite.h"
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "UIWebView.h"
#else
#include "ui\UIWebView.h"
#endif

USING_NS_CC;

class BoxNew: public LayerColor {
public:
	static BoxNew* create();

	void show();
private:
	EGSprite* mBox, *mImage;
	EGButtonSprite* mBtnClose;
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	experimental::ui::WebView *mWebView;
#endif

	void initObject();

	virtual bool ccTouchBeganFake(Touch* touch, Event* event);
};

#endif