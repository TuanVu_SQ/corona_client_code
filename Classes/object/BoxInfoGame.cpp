#include "BoxInfoGame.h"
#include "EGButtonSprite.h"
#include "WXmlReader.h"
#include "EGSupport.h"

BoxInfoGame* BoxInfoGame::createBoxInfo(){
	BoxInfoGame* box = new BoxInfoGame();
	box->autorelease();
	box->initBoxInfo();
	return box;
}
void BoxInfoGame::initBoxInfo(){

	WXmlReader *xml = WXmlReader::create();
	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(BoxInfoGame::ccTouchBegan, this);
	listener->setSwallowTouches(true);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	Size size = Director::getInstance()->getWinSize();
	Sprite* box = Sprite::createWithSpriteFrameName(IMG_BOX_INFO_GAME);
	box->setPosition(Vec2(size.width / 2, size.height / 2));
	box->setTag(1);
	this->addChild(box);
	buttonkick = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_KICK);
	box->addChild(buttonkick);
	buttonkick->setButtonType(EGButton2FrameDark);
	buttonkick->setPosition(Vec2(box->getContentSize().width -70 - buttonkick->getContentSize().width / 2, 63));

	buttonSwap = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_CHANGE_POSITION);
	box->addChild(buttonSwap);
	buttonSwap->setButtonType(EGButton2FrameDark);
	buttonSwap->setPosition(Vec2(box->getContentSize().width / 2, 63));

	buttonFriend = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_ADD_FRIEND_GAME);
	box->addChild(buttonFriend);
	buttonFriend->setButtonType(EGButton2FrameDark);
	buttonFriend->setPosition(Vec2(70 + buttonFriend->getContentSize().width / 2, 63));
	mAvatarSprite = Sprite::create();
	box->addChild(mAvatarSprite);
	mAvatarSprite->setPosition(Vec2(93, 277));

	mNameLabel = Label::createWithBMFont("font.fnt", "");
	mNameLabel->setPosition(Vec2(mAvatarSprite->getPositionX(), mAvatarSprite->getPositionY() - 40));
	box->addChild(mNameLabel);
	mNameLabel->setScale(0.5f);

	mLabelLevel = Label::createWithBMFont("font-info-money-game.fnt", "100");
	mLabelLevel->setPosition(Vec2(342, 236));
	box->addChild(mLabelLevel);
	mLabelLevel->setAnchorPoint(Vec2(0, 0.5f));

	mLabelWinCount = Label::createWithBMFont("font-info-money-game.fnt", "100");
	mLabelWinCount->setPosition(Vec2(130,204));
	box->addChild(mLabelWinCount);
	mLabelWinCount->setAnchorPoint(Vec2(0, 0.5f));

	mLabelLoseCount = Label::createWithBMFont("font-info-money-game.fnt", "100");
	mLabelLoseCount->setPosition(Vec2(329,204));
	box->addChild(mLabelLoseCount);
	mLabelLoseCount->setAnchorPoint(Vec2(0, 0.5f));

	mLabelRank = Label::createWithBMFont("font-info-money-game.fnt", "212");
	mLabelRank->setPosition(Vec2(517,204));
	box->addChild(mLabelRank);
	mLabelRank->setAnchorPoint(Vec2(0, 0.5f));

	mLabelMoney = Label::createWithBMFont("font-info-money-game.fnt", "1000000");
	mLabelMoney->setPosition(Vec2(225,274));
	box->addChild(mLabelMoney);
	mLabelMoney->setAnchorPoint(Vec2(0, 0.5f));

	EGButtonSprite* buttonClose = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_CLOSE_CR);
	buttonClose->setButtonType(EGButton2FrameDark);
	box->addChild(buttonClose);
	buttonClose->setPosition(Vec2(box->getContentSize().width - 5, box->getContentSize().height - 5));
	buttonClose->setOnTouch(CC_CALLBACK_0(BoxInfoGame::closeBox, this));

	for (int i = 0; i < 3; i++){
		EGSprite* pAchieSprite = EGSprite::createWithSpriteFrameName(__String::createWithFormat("achieve%d (%d).png", 1, 1)->getCString());
		box->addChild(pAchieSprite);
		pAchieSprite->setPosition(Vec2(box->getContentSize().width / 2 - 110 + 110 * i, 150));
		pAchieSprite->setScale(1.2f);
		pAchieSprite->setTag(i + 1);
		pAchieSprite->registerTouch(true);
	}

	buttonChangePass = EGButtonSprite::createWithFile("button_change_pass.png");
	box->addChild(buttonChangePass);
	buttonChangePass->setPosition(Vec2(box->getContentSize().width / 2, 70));
	buttonChangePass->registerTouch(true);

	//box->setVisible(false);
	buttonChangePass->setButtonType(EGButton2FrameDark);

	EGSprite* boxChangePass = EGSprite::createWithFile("box_change_password.png");
	boxChangePass->setVisible(false);
	boxChangePass->setTag(2);
	this->addChild(boxChangePass);
	boxChangePass->setPosition(box->getPosition());

	Scale9Sprite* pEdtSprite_Password= Scale9Sprite::create("edit_box_password.png");
	mEdit_OldPass = EditBox::create(pEdtSprite_Password->getContentSize(), pEdtSprite_Password);
	mEdit_OldPass->setPosition(Vec2(193 + pEdtSprite_Password->getContentSize().width / 2, 242));
	mEdit_OldPass->setFontName(FONT);
	mEdit_OldPass->setFontSize(20);
	//mEdt_Username->setMargins(20, 20);
	mEdit_OldPass->setInputFlag(EditBox::InputFlag::PASSWORD);
	mEdit_OldPass->setInputMode(EditBox::InputMode::SINGLE_LINE);
	mEdit_OldPass->setFontColor(Color3B::WHITE);
	mEdit_OldPass->setPlaceholderFontColor(Color3B::GRAY);
	mEdit_OldPass->setMaxLength(REQ_MAXLENGHT_USERNAME);
	mEdit_OldPass->setReturnType(EditBox::KeyboardReturnType::DONE);
	//mEdit_OldPass->setDelegate(this);
	boxChangePass->addChild(mEdit_OldPass);

	Scale9Sprite* pEdtSprite_NewPassword = Scale9Sprite::create("edit_box_password.png");
	mEdit_NewPass = EditBox::create(pEdtSprite_NewPassword->getContentSize(), pEdtSprite_NewPassword);
	mEdit_NewPass->setPosition(Vec2(193 + pEdtSprite_NewPassword->getContentSize().width / 2, 189));
	mEdit_NewPass->setFontName(FONT);
	mEdit_NewPass->setFontSize(20);
	//mEdt_Username->setMargins(20, 20);
	mEdit_NewPass->setInputFlag(EditBox::InputFlag::PASSWORD);
	mEdit_NewPass->setInputMode(EditBox::InputMode::SINGLE_LINE);
	mEdit_NewPass->setFontColor(Color3B::WHITE);
	mEdit_NewPass->setPlaceholderFontColor(Color3B::GRAY);
	mEdit_NewPass->setMaxLength(REQ_MAXLENGHT_USERNAME);
	mEdit_NewPass->setReturnType(EditBox::KeyboardReturnType::DONE);
	//mEdit_OldPass->setDelegate(this);
	boxChangePass->addChild(mEdit_NewPass);

	Scale9Sprite* pEdtSprite_NewPassword1 = Scale9Sprite::create("edit_box_password.png");
	mEdit_NewPass1 = EditBox::create(pEdtSprite_NewPassword1->getContentSize(), pEdtSprite_NewPassword1);
	mEdit_NewPass1->setPosition(Vec2(193 + pEdtSprite_NewPassword1->getContentSize().width / 2, 135));
	mEdit_NewPass1->setFontName(FONT);
	mEdit_NewPass1->setFontSize(20);
	//mEdt_Username->setMargins(20, 20);
	mEdit_NewPass1->setInputFlag(EditBox::InputFlag::PASSWORD);
	mEdit_NewPass1->setInputMode(EditBox::InputMode::SINGLE_LINE);
	mEdit_NewPass1->setFontColor(Color3B::WHITE);
	mEdit_NewPass1->setPlaceholderFontColor(Color3B::GRAY);
	mEdit_NewPass1->setMaxLength(REQ_MAXLENGHT_USERNAME);
	mEdit_NewPass1->setReturnType(EditBox::KeyboardReturnType::DONE);
	//mEdit_OldPass->setDelegate(this);
	boxChangePass->addChild(mEdit_NewPass1);

	EGButtonSprite* buttonAccept = EGButtonSprite::createWithFile("button_accept_change_pass.png");
	boxChangePass->addChild(buttonAccept);
	buttonAccept->setPosition(Vec2(boxChangePass->getContentSize().width / 2, 70));
	buttonAccept->setButtonType(EGButton2FrameDark);
	buttonAccept->registerTouch(true);
	buttonAccept->setOnTouch([=]{ _funcChangePass(mEdit_OldPass->getText(), mEdit_NewPass->getText(), mEdit_NewPass1->getText()); });

	buttonChangePass->setOnTouch([=]{
		box->setVisible(false);
		boxChangePass->setVisible(true);
	});

	EGButtonSprite* buttonClose1 = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_CLOSE_CR);
	buttonClose1->setButtonType(EGButton2FrameDark);
	boxChangePass->addChild(buttonClose1);
	buttonClose1->setPosition(Vec2(boxChangePass->getContentSize().width - 5, boxChangePass->getContentSize().height - 5));
	buttonClose1->setOnTouch(CC_CALLBACK_0(BoxInfoGame::closeBox, this));
}
std::string toLower(std::string pString){
	for (int i = 0; i < pString.length(); i++){
		pString[i] = std::tolower(pString[i]);
	}
	return pString;
}
void BoxInfoGame::showBox(ClientPlayerInfoEX* pInfo, ClientPlayerInfoEX* pMyselft){
	this->setVisible(true);
	Sprite* box = (Sprite*)this->getChildByTag(1);
	box->setVisible(true);
	mLabelWinCount->setString("");
	mLabelLoseCount->setString("");
	mLabelRank->setString("");
	mLabelMoney->setString("");
	mLabelLevel->setString("");
	if (pInfo ){
		mNameLabel->setString(pInfo->username);
		mNameLabel->setColor(Color3B::GREEN);
		mNameLabel->setScale(0.5f);
		mLabelWinCount->setString(__String::createWithFormat("%d", pInfo->winCount)->getCString());
		mLabelLoseCount->setString(__String::createWithFormat("%d", pInfo->loseCount)->getCString());
		mLabelRank->setString(__String::createWithFormat("%d", pInfo->rank)->getCString());
		std::string pBalance = __String::createWithFormat("%d", pInfo->balance)->getCString();
		mLabelMoney->setString(EGSupport::addDotMoney(pBalance).c_str());
		mLabelLevel->setString(__String::createWithFormat("%d", pInfo->level)->getCString());
		for (int i = 0; i < 3; i++){	
			EGSprite* pAchieSprite = (EGSprite*)box->getChildByTag(i + 1);
			uint8_t  pIndex = 1;
			if (i == 0){ pIndex = pInfo->acheive1; }
			if (i == 1){ pIndex = pInfo->acheive2; }
			if (i == 2){ pIndex = pInfo->acheive3; }
			pAchieSprite->setOnTouch(CC_CALLBACK_0(BoxInfoGame::showInfoAchieve,this, i, pIndex));
			if (pIndex == 0){
				pAchieSprite->setColor(Color3B( 0, 0, 0));
				pIndex = 1;
			}
			else{
				pAchieSprite->setColor(Color3B(255, 255, 255));
			}
			pAchieSprite->setTexture(__String::createWithFormat("achieve%d (%d).png", i + 1, (int)pIndex)->getCString());
			
		}
		mLabelLevel->setColor(Color3B::YELLOW);
		if (pMyselft && !pMyselft->host){
			buttonkick->setOpacity(155);
			buttonkick->unregisterTouch();
		}
		else{
			buttonkick->setOpacity(255);
			buttonkick->registerTouch(true);
		}
		if (pInfo->isFriend){
			buttonFriend->unregisterTouch();
			buttonFriend->setOpacity(155);
		}
		else{
			buttonFriend->registerTouch(true);
			buttonFriend->setOpacity(255);
		}
		buttonSwap->setOpacity(255);
		buttonSwap->registerTouch(true);
		if (pMyselft && (toLower(pInfo->username) == toLower(pMyselft->username) || pMyselft->position <0 || pMyselft->position >3)){
			buttonkick->setOpacity(155);
			buttonkick->unregisterTouch();
			buttonFriend->setOpacity(155);
			buttonFriend->unregisterTouch();
			buttonSwap->setOpacity(155);
			buttonSwap->unregisterTouch();
		}
		else if (!pMyselft){
			buttonkick->setOpacity(155);
			buttonkick->unregisterTouch();
			buttonSwap->setOpacity(155);
			buttonSwap->unregisterTouch();
		}
		if (pInfo->avatarId > 0){
			mAvatarSprite->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat("avatar (%d).png", pInfo->avatarId)->getCString()));
		}
		if (toLower(pInfo->username) == toLower(GameInfo::mUserInfo.mUsername) && pInfo->username != ""){
			buttonChangePass->setVisible(true);
		}
		else{
			buttonChangePass->setVisible(false);
		}
	}
}
void BoxInfoGame::setType(int pType){
	if (pType == 2){
		buttonkick->setVisible(false);
		buttonFriend->setVisible(false);
		buttonSwap->setVisible(false);
	}
	else{
		buttonChangePass->setVisible(false);
	}
}
bool BoxInfoGame::ccTouchBegan(Touch* touch, Event* event) {
	if (isVisible()) {
		return true;
	}
	return false;
}
void BoxInfoGame::closeBox(){
	this->setVisible(false);
	this->getChildByTag(1)->setVisible(false);
	this->getChildByTag(2)->setVisible(false);
	/*mEdit_NewPass1->setText("");
	mEdit_NewPass->setText("");
	mEdit_OldPass->setText("");*/
}
void BoxInfoGame::setOnTouchAddFriend(std::function<void()> pTouchFunction){
	buttonFriend->setOnTouch(pTouchFunction);
}
void BoxInfoGame::setOnTouchKickPlayer(std::function<void()> pTouchFunction){
	buttonkick->setOnTouch(pTouchFunction);
}
void BoxInfoGame::setOnTouchChangePosition(std::function<void()> pTouchFunction){
	buttonSwap->setOnTouch(pTouchFunction);
}
void BoxInfoGame::showToastReal(std::string pToast, float pWidth, Vec2 pPosition) {
	LayerColor* pLayer = (LayerColor*)getChildByTag(20);
	if (pLayer){
		pLayer->removeAllChildrenWithCleanup(true);
		pLayer->removeFromParentAndCleanup(true);
		pLayer = NULL;
	}
		 pLayer = LayerColor::create(Color4B(0, 0, 0, 200), pWidth, 40);

		this->addChild(pLayer);
		pLayer->setTag(20);
		Label* pLbl = Label::createWithBMFont("font_vn.fnt", pToast, TextHAlignment::CENTER, pWidth - 6);

		pLayer->addChild(pLbl);
		Label* pLblTest = Label::createWithBMFont("font_vn.fnt", pToast);
		if (pWidth > pLblTest->getContentSize().width){
			pWidth = pLblTest->getContentSize().width + 20;
		}
		pLayer->setContentSize(Size(pWidth, pLbl->getContentSize().height + 6));
		pLbl->setPosition(Vec2(pLayer->getContentSize().width / 2, pLayer->getContentSize().height / 2));
		pLayer->setPosition(Vec2(pPosition.x - pLayer->getContentSize().width / 2, pPosition.y - pLayer->getContentSize().height / 2));
		float pXStart = 800 + pLbl->getContentSize().width / 2;
		float pXEnd = -pLbl->getContentSize().width / 2;
		float pDistances = pXStart - pXEnd;
			pLbl->runAction(Sequence::createWithTwoActions(DelayTime::create(4), CallFunc::create([=] {\
				pLbl->removeFromParentAndCleanup(true);
				pLayer->removeFromParentAndCleanup(true);
			})));

}
void BoxInfoGame::showInfoAchieve(int index, int pLevel){
	WXmlReader* pXml = WXmlReader::create();
	std::string pStr = pXml->getNodeTextByTagName(__String::createWithFormat("achieve_%d", index)->getCString());
	if (pLevel > 0){
		pStr += pXml->getNodeTextByTagName(__String::createWithFormat("achieve_%d_%d", index + 1, pLevel)->getCString());
	}
	else{
		pStr += pXml->getNodeTextByTagName(__String::createWithFormat("achieve_%d_%d", 0, 0)->getCString());
	}
	showToastReal(pStr, 600, Vec2(400, 400));
}