#include "BoxChatDisplay.h"
#include "GameInfo.h"

BoxChatDisplay* BoxChatDisplay::create(int pType) {
	BoxChatDisplay* pBoxChatDisplay = new BoxChatDisplay();
	pBoxChatDisplay->mType = pType;
	pBoxChatDisplay->initObject();
	pBoxChatDisplay->autorelease();

	return pBoxChatDisplay;
}

void BoxChatDisplay::initObject() {
	mLabelContentChat = NULL;
	mBoxDisplayBottom = NULL;
	mBoxDisplayTop = NULL;
	mBoxDisplayMiddle = NULL;
	mEmotionChat = NULL;
}

void BoxChatDisplay::setVisible(bool isVisible) {
	if (mLabelContentChat)
		mLabelContentChat->setVisible(isVisible);
	if (mBoxDisplayTop)
		mBoxDisplayTop->setVisible(isVisible);
	if (mBoxDisplayMiddle)
		mBoxDisplayMiddle->setVisible(isVisible);
	if (mBoxDisplayBottom)
		mBoxDisplayBottom->setVisible(isVisible);
	if (mEmotionChat)
		mEmotionChat->setVisible(isVisible);
}

void BoxChatDisplay::chatWithEmotion(int pEmotionID) {
	this->stopAllActions();
	this->setScale(0);
	this->runAction(ScaleTo::create(0.25f, 1));
	this->runAction(Sequence::createWithTwoActions(DelayTime::create(5), ScaleTo::create(0.25f, 0)));

	if (mLabelContentChat)
		mLabelContentChat->setVisible(false);
	if (mBoxDisplayTop)
		mBoxDisplayTop->setVisible(false);
	if (mBoxDisplayMiddle)
		mBoxDisplayMiddle->setVisible(false);
	if (mBoxDisplayBottom)
		mBoxDisplayBottom->setVisible(false);

	std::string fileName = __String::createWithFormat("a_%d (1).png", pEmotionID + 1)->getCString();
	if (mEmotionChat) {
		mEmotionChat->stopAllActions();
		mEmotionChat->setTexture(fileName.c_str());
		mEmotionChat->setVisible(true);
	}
	else {
		mEmotionChat = EGSprite::createWithSpriteFrameName(fileName.c_str());
		if (mType == BOXCHATDISPLAY_TYPE_BOTUP)
			mEmotionChat->setPosition(Vec2(0, mEmotionChat->getContentSize().height / 2));
		else
			mEmotionChat->setPosition(Vec2(0, -mEmotionChat->getContentSize().height / 2));
		this->addChild(mEmotionChat);
	}

	Animation* pAnimation = Animation::create();
	pAnimation->setDelayPerUnit(0.1f);

	for (int g = 0; g < EMOTION_COUNTER[pEmotionID]; g++) {
		std::string fileNameAnimation = __String::createWithFormat("a_%d (%d).png", pEmotionID + 1, g + 1)->getCString();
		pAnimation->addSpriteFrame(SpriteFrameCache::sharedSpriteFrameCache()->getInstance()->spriteFrameByName(fileNameAnimation));
	}

	mEmotionChat->runAction(RepeatForever::create(Animate::create(pAnimation)));
}

void BoxChatDisplay::chatWithText(std::string pContent) {
	this->stopAllActions();
	this->setScale(0);
	this->runAction(ScaleTo::create(0.25f, 1));
	this->runAction(Sequence::createWithTwoActions(DelayTime::create(5), ScaleTo::create(0.25f, 0)));

	if (mEmotionChat) {
		mEmotionChat->stopAllActions();
		mEmotionChat->setVisible(false);
	}

	if (mLabelContentChat) {
		mLabelContentChat->setString(pContent);
		mLabelContentChat->setVisible(true);
	}
	else {
		mLabelContentChat = Label::createWithBMFont(FONT_CHAT_VN, pContent, TextHAlignment::LEFT, 175);
		this->addChild(mLabelContentChat, 1);
	}

	if (mBoxDisplayBottom) {
		mBoxDisplayBottom->setVisible(true);
	}
	else {
		mBoxDisplayBottom = EGSprite::createWithSpriteFrameName("chat_dis_box1.png");
		if (mType == BOXCHATDISPLAY_TYPE_BOTUP) {
			mBoxDisplayBottom->setPosition(Vec2(0, mBoxDisplayBottom->getContentSize().height / 2));
		}
		else {
			mBoxDisplayBottom->setPosition(Vec2(0, -mBoxDisplayBottom->getContentSize().height / 2));
		}
		mBoxDisplayBottom->setFlippedY(mType == BOXCHATDISPLAY_TYPE_BOTUP);
		this->addChild(mBoxDisplayBottom);
	}

	if (mBoxDisplayMiddle) {
		mBoxDisplayMiddle->setVisible(true);
	}
	else {
		mBoxDisplayMiddle = EGSprite::createWithSpriteFrameName("chat_dis_box2.png");
		this->addChild(mBoxDisplayMiddle);
	}

	float pHeightDisplayMiddle = mBoxDisplayMiddle->getContentSize().height;
	float pHeihgtContentChat = mLabelContentChat->getContentSize().height;

	mBoxDisplayMiddle->setScaleY(pHeihgtContentChat / pHeightDisplayMiddle);

	if (mType == BOXCHATDISPLAY_TYPE_BOTUP) {
		mBoxDisplayMiddle->setPosition(Vec2(0, mBoxDisplayBottom->getPositionY2() + mBoxDisplayMiddle->getContentSize().height*mBoxDisplayMiddle->getScaleY() / 2));
	}
	else {
		mBoxDisplayMiddle->setPosition(Vec2(0, mBoxDisplayBottom->getPositionY1() - mBoxDisplayMiddle->getContentSize().height*mBoxDisplayMiddle->getScaleY() / 2));
	}
	mLabelContentChat->setPosition(mBoxDisplayMiddle->getPosition());

	if (mBoxDisplayTop) {
		mBoxDisplayTop->setVisible(true);
	}
	else {
		mBoxDisplayTop = EGSprite::createWithSpriteFrameName("chat_dis_box1.png");
		mBoxDisplayTop->setFlippedY(mType == BOXCHATDISPLAY_TYPE_TOPDOWN);
		this->addChild(mBoxDisplayTop);
	}

	if (mType == BOXCHATDISPLAY_TYPE_BOTUP) {
		mBoxDisplayTop->setPosition(Vec2(0, mBoxDisplayMiddle->getPositionY()
			+ mBoxDisplayMiddle->getContentSize().height *mBoxDisplayMiddle->getScaleY() / 2
			+ mBoxDisplayTop->getContentSize().height / 2));
	}
	else {
		mBoxDisplayTop->setPosition(Vec2(0, mBoxDisplayMiddle->getPositionY()
			- mBoxDisplayMiddle->getContentSize().height *mBoxDisplayMiddle->getScaleY() / 2
			- mBoxDisplayTop->getContentSize().height / 2));
	}
}