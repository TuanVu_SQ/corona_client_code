#ifndef __BOXNOTICEMANAGER_H_INCLUDED__
#define __BOXNOTICEMANAGER_H_INCLUDED__

#include "BoxNotice.h"
#include "WXmlReader.h"
#include "GameInfo.h"
#include "HomeScene.h"

class BoxNoticeManager {
public:
	static void getBoxNotice(BoxNotice* pBoxNotice, std::string pTitle, std::string pContent) {
		if (pBoxNotice) {
			WXmlReader* xml = WXmlReader::create();
			xml->load(KEY_XML_FILE);

			pBoxNotice->mLbl_Title->setString(pTitle);
			pBoxNotice->mLbl_Title->setPosition(Vec2(pBoxNotice->getContentSize().width / 2,
				pBoxNotice->getContentSize().height - pBoxNotice->mLbl_Title->getContentSize().height / 2 - 50));

			if (pContent == "")
				pContent = xml->getNodeTextByTagName(TEXT_EER_NOTFOUND_EXCEPTION).c_str();
			else if (pContent == xml->getNodeTextByTagName(TEXT_ERR_TIMEOUT))
				pBoxNotice->mBtnSkip->setOnTouch([=] {
					Director::getInstance()->replaceScene(TransitionFade::create(0.5f, HomeScene::scene()));
				});
			else
				pBoxNotice->mBtnSkip->setOnTouch(CC_CALLBACK_0(BoxNotice::setVisible, pBoxNotice, false));
			//pContent = "Test";
			pBoxNotice->mLbl_Content->setString(pContent);
			pBoxNotice->mLbl_Content->setPosition(Vec2(pBoxNotice->getContentSize().width / 2,
				pBoxNotice->mLbl_Title->getPositionY() - pBoxNotice->mLbl_Title->getContentSize().height / 2
				- pBoxNotice->mLbl_Content->getContentSize().height / 2 - 20));

			pBoxNotice->mBtnAccept->setVisible(false);

			pBoxNotice->mBtnSkip->setVisible(true);
			pBoxNotice->mBtnSkip->setText(xml->getNodeTextByTagName(TEXT_CLOSE).c_str(), FONT, 20);
			//pBoxNotice->mBtnSkip->setText("Test", FONT, 20);
			//pBoxNotice->mBtnSkip->setOnTouch(CC_CALLBACK_0(BoxNotice::setVisible, pBoxNotice, false));

			pBoxNotice->mBtnReject->setVisible(false);
			pBoxNotice->mEdt_Input->setVisible(false);
		}
	}

	static void getBoxNotice(BoxNotice* pBoxNotice, std::string pTitle, std::string pContent, std::function<void()> pAcceptFunc) {
		if (pBoxNotice) {
			WXmlReader* xml = WXmlReader::create();
			xml->load(KEY_XML_FILE);

			pBoxNotice->mLbl_Title->setString(pTitle);
			pBoxNotice->mLbl_Title->setPosition(Vec2(pBoxNotice->getContentSize().width / 2,
				pBoxNotice->getContentSize().height - pBoxNotice->mLbl_Title->getContentSize().height / 2 - 50));

			if (pContent == "")
				pContent = xml->getNodeTextByTagName(TEXT_EER_NOTFOUND_EXCEPTION).c_str();
			else if (pContent == xml->getNodeTextByTagName(TEXT_ERR_TIMEOUT))
				pBoxNotice->mBtnSkip->setOnTouch([=] {
				Director::getInstance()->replaceScene(TransitionFade::create(0.5f, HomeScene::scene()));
			});
			else
				pBoxNotice->mBtnSkip->setOnTouch(pAcceptFunc);
			//pContent = "Test";
			pBoxNotice->mLbl_Content->setString(pContent);
			pBoxNotice->mLbl_Content->setPosition(Vec2(pBoxNotice->getContentSize().width / 2,
				pBoxNotice->mLbl_Title->getPositionY() - pBoxNotice->mLbl_Title->getContentSize().height / 2
				- pBoxNotice->mLbl_Content->getContentSize().height / 2 - 20));

			pBoxNotice->mBtnAccept->setVisible(false);

			pBoxNotice->mBtnSkip->setVisible(true);
			pBoxNotice->mBtnSkip->setText(xml->getNodeTextByTagName(TEXT_CLOSE).c_str(), FONT, 20);
			//pBoxNotice->mBtnSkip->setText("Test", FONT, 20);
			//pBoxNotice->mBtnSkip->setOnTouch(pAcceptFunc);

			pBoxNotice->mBtnReject->setVisible(false);

			pBoxNotice->mEdt_Input->setVisible(false);
		}
	}

	static void getBoxNotice(BoxNotice* pBoxNotice, std::string pTitle, std::string pContent, std::function<void()> pAcceptFunc, std::function<void()> pCloseFunc) {
		if (pBoxNotice) {
			WXmlReader* xml = WXmlReader::create();
			xml->load(KEY_XML_FILE);

			pBoxNotice->mLbl_Title->setString(pTitle);
			pBoxNotice->mLbl_Title->setPosition(Vec2(pBoxNotice->getContentSize().width / 2,
				pBoxNotice->getContentSize().height - pBoxNotice->mLbl_Title->getContentSize().height / 2 - 50));

			if (pContent == "")
				pContent = xml->getNodeTextByTagName(TEXT_EER_NOTFOUND_EXCEPTION).c_str();
			else if (pContent == xml->getNodeTextByTagName(TEXT_ERR_TIMEOUT))
				pBoxNotice->mBtnSkip->setOnTouch([=] {
				Director::getInstance()->replaceScene(TransitionFade::create(0.5f, HomeScene::scene()));
			});
			else
				pBoxNotice->mBtnSkip->setOnTouch(pAcceptFunc);
			//pContent = "Test";
			pBoxNotice->mLbl_Content->setString(pContent);
			pBoxNotice->mLbl_Content->setPosition(Vec2(pBoxNotice->getContentSize().width / 2,
				pBoxNotice->mLbl_Title->getPositionY() - pBoxNotice->mLbl_Title->getContentSize().height / 2
				- pBoxNotice->mLbl_Content->getContentSize().height / 2 - 20));

			pBoxNotice->mBtnAccept->setVisible(false);

			pBoxNotice->mBtnSkip->setVisible(true);
			pBoxNotice->mBtnSkip->setText(xml->getNodeTextByTagName(TEXT_CLOSE).c_str(), FONT, 20);
			//pBoxNotice->mBtnSkip->setText("Test", FONT, 20);
			pBoxNotice->mBtnSkip->setOnTouch(pCloseFunc);

			pBoxNotice->mBtnReject->setVisible(false);

			pBoxNotice->mEdt_Input->setVisible(false);
		}
	}

	static void getBoxConfirm(BoxNotice* pBoxNotice, std::string pTitle, std::string pContent, std::function<void()> pAcceptFunc) {
		if (pBoxNotice) {
			WXmlReader* xml = WXmlReader::create();
			xml->load(KEY_XML_FILE);

			pBoxNotice->mLbl_Title->setString(pTitle);
			pBoxNotice->mLbl_Title->setPosition(Vec2(pBoxNotice->getContentSize().width / 2,
				pBoxNotice->getContentSize().height - pBoxNotice->mLbl_Title->getContentSize().height / 2 - 50));

			if (pContent == "")
				pContent = xml->getNodeTextByTagName(TEXT_EER_NOTFOUND_EXCEPTION).c_str();
			//pContent = "Test";
			pBoxNotice->mLbl_Content->setString(pContent);
			pBoxNotice->mLbl_Content->setPosition(Vec2(pBoxNotice->getContentSize().width / 2,
				pBoxNotice->mLbl_Title->getPositionY() - pBoxNotice->mLbl_Title->getContentSize().height / 2
				- pBoxNotice->mLbl_Content->getContentSize().height / 2 - 20));

			pBoxNotice->mBtnAccept->setOnTouch(pAcceptFunc);
			pBoxNotice->mBtnAccept->setVisible(true);
			pBoxNotice->mBtnAccept->setText(xml->getNodeTextByTagName("accept").c_str(), FONT, 20);

			pBoxNotice->mBtnSkip->setVisible(true);
			pBoxNotice->mBtnSkip->setText(xml->getNodeTextByTagName("skip").c_str(), FONT, 20);
			//pBoxNotice->mBtnSkip->setText("Test", FONT, 20);
			pBoxNotice->mBtnSkip->setOnTouch(CC_CALLBACK_0(BoxNotice::setVisible, pBoxNotice, false));

			pBoxNotice->mBtnReject->setVisible(false);

			pBoxNotice->mEdt_Input->setVisible(false);
		}
	}

	static void getBoxConfirm(BoxNotice* pBoxNotice, std::string pTitle, std::string pContent, std::function<void()> pAcceptFunc, std::function<void()> pRejectFunc) {
		if (pBoxNotice) {
			WXmlReader* xml = WXmlReader::create();
			xml->load(KEY_XML_FILE);

			pBoxNotice->mLbl_Title->setString(pTitle);
			pBoxNotice->mLbl_Title->setPosition(Vec2(pBoxNotice->getContentSize().width / 2,
				pBoxNotice->getContentSize().height - pBoxNotice->mLbl_Title->getContentSize().height / 2 - 50));

			if (pContent == "")
				pContent = xml->getNodeTextByTagName(TEXT_EER_NOTFOUND_EXCEPTION).c_str();
			//pContent = "Test";
			pBoxNotice->mLbl_Content->setString(pContent);
			pBoxNotice->mLbl_Content->setPosition(Vec2(pBoxNotice->getContentSize().width / 2,
				pBoxNotice->mLbl_Title->getPositionY() - pBoxNotice->mLbl_Title->getContentSize().height / 2
				- pBoxNotice->mLbl_Content->getContentSize().height / 2 - 20));

			pBoxNotice->mBtnAccept->setOnTouch(pAcceptFunc);
			pBoxNotice->mBtnAccept->setVisible(true);
			pBoxNotice->mBtnAccept->setText(xml->getNodeTextByTagName("accept").c_str(), FONT, 20);

			pBoxNotice->mBtnSkip->setVisible(true);
			pBoxNotice->mBtnSkip->setText(xml->getNodeTextByTagName("skip").c_str(), FONT, 20);
			//pBoxNotice->mBtnSkip->setText("Test", FONT, 20);
			pBoxNotice->mBtnSkip->setOnTouch(pRejectFunc);

			pBoxNotice->mBtnReject->setVisible(false);

			pBoxNotice->mEdt_Input->setVisible(false);
		}
	}

	static void getBoxInput(BoxNotice* pBoxNotice, std::string pTitle, std::string pContent,
		std::string pTextHolder, std::string pTextDefault, std::string pTextAccept, std::function<void()> pAcceptFunc) {
		if (pBoxNotice) {
			WXmlReader* xml = WXmlReader::create();
			xml->load(KEY_XML_FILE);

			pBoxNotice->mLbl_Title->setString(pTitle);
			pBoxNotice->mLbl_Title->setPosition(Vec2(pBoxNotice->getContentSize().width / 2,
				pBoxNotice->getContentSize().height - pBoxNotice->mLbl_Title->getContentSize().height / 2 - 50));

			pBoxNotice->mLbl_Content->setString(pContent);
			pBoxNotice->mLbl_Content->setPosition(Vec2(pBoxNotice->getContentSize().width / 2,
				pBoxNotice->mLbl_Title->getPositionY() - pBoxNotice->mLbl_Title->getContentSize().height / 2
				- pBoxNotice->mLbl_Content->getContentSize().height / 2 - 20));

			pBoxNotice->mBtnAccept->setVisible(true);
			pBoxNotice->mBtnAccept->setText(pTextAccept.c_str(), FONT, 20);
			pBoxNotice->mBtnAccept->setOnTouch(pAcceptFunc);

			pBoxNotice->mBtnReject->setVisible(false);

			pBoxNotice->mBtnSkip->setVisible(true);
			pBoxNotice->mBtnSkip->setText(xml->getNodeTextByTagName(TEXT_CLOSE).c_str(), FONT, 20);
			pBoxNotice->mBtnSkip->setOnTouch(CC_CALLBACK_0(BoxNotice::setVisible, pBoxNotice, false));

			pBoxNotice->mEdt_Input->setVisible(true);
			pBoxNotice->mEdt_Input->setPlaceHolder(pTextHolder.c_str());
			pBoxNotice->mEdt_Input->setText(pTextDefault.c_str());
		}
	}
};

#endif