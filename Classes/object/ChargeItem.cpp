#include "ChargeItem.h"
#include "libs/EGSupport.h"

ChargeItem::ChargeItem() {
	_itemType = 1;
	_numberChip = 0;
	_numberCoin = 0;
	_isChip = false;
}

ChargeItem::~ChargeItem() {

}

ChargeItem* ChargeItem::create() {
	ChargeItem* pChargeItem = new ChargeItem();
	pChargeItem->initItem();
	pChargeItem->autorelease();
	return pChargeItem;
}


void ChargeItem::initItem() {
	EGSprite::init();

	pXml = WXmlReader::create();
	pXml->load(KEY_XML_FILE);

	_background = EGButtonSprite::createWithFile("vs1.png");
	_background->setButtonType(EGButtonType::EGButtonScrollViewItem);
	_background->setPosition(this->getContentSize()/2);
	_background->setScale(0.9f);
	this->addChild(_background);
	_background->setOnTouch(CC_CALLBACK_0(ChargeItem::itemClicked, this));

	/*_bigCoin = EGSprite::createWithFile("vs1.png");
	_bigCoin->setPosition(Vec2(_background->getContentSize().width / 2, _background->getContentSize().height / 2 + 20));
	_background->addChild(_bigCoin);*/

	_lbMoneyInGame = Label::createWithBMFont("font.fnt", "0");
	_lbMoneyInGame->setScale(0.4f);
	_lbMoneyInGame->setPosition(Vec2(_background->getContentSize().width / 2 + 5, _lbMoneyInGame->getContentSize().height / 2 * _lbMoneyInGame->getScale() + 15));
	_lbMoneyInGame->setColor(Color3B::YELLOW);
	_background->addChild(_lbMoneyInGame);

	moneyBg = EGSprite::createWithFile("img_charge_item_money_bg.png");
	moneyBg->setScale(0.9f);
	moneyBg->setPosition(Vec2(0, _background->getPositionY1() - moneyBg->getContentSize().height / 2 + 5));
	this->addChild(moneyBg);

	_lbMoneyReal = Label::createWithBMFont("font.fnt", "0");
	_lbMoneyReal->setScale(0.4f);
	_lbMoneyReal->setColor(Color3B::YELLOW);
	_lbMoneyReal->setPosition(Vec2(moneyBg->getContentSize() / 2));
	moneyBg->addChild(_lbMoneyReal, 1);

	_salebg = EGSprite::createWithFile("img_charge_sale.png");
	_salebg->setPosition(_background->getContentSize()/2);
	_background->addChild(_salebg);

	_lbSale = Label::createWithBMFont("font.fnt", "0");
	_lbSale->setScale(0.4f);
	_lbSale->setRotation(45);
	_lbSale->setPosition(Vec2(_salebg->getContentSize().width - 20, _salebg->getContentSize().height -20));
	_salebg->addChild(_lbSale);
	
	_smsBg = EGButtonSprite::createWithFile("vs6.png");
	_smsBg->setVisible(false);
	_smsBg->setButtonType(EGButtonType::EGButtonScrollViewItem);
	_smsBg->setFlippedX(true);
	_smsBg->setPosition(_background->getContentSize() / 2);
	_background->addChild(_smsBg);
	_smsBg->setOnTouch(CC_CALLBACK_0(ChargeItem::sendSmsClicked, this));

	_lbServiceNumber = Label::createWithBMFont("font.fnt", "0");
	_lbServiceNumber->setScaleY(0.6f);
	_lbServiceNumber->setScaleX(-0.6f);
	_lbServiceNumber->setPosition(Vec2(_smsBg->getContentSize().width / 2, _lbServiceNumber->getContentSize().height * _lbServiceNumber->getScaleY()/2 ) + Vec2(0, 10));
	_smsBg->addChild(_lbServiceNumber);
}

void ChargeItem::setSmsInfo(uint8_t order, bool isChip, RechargeInfo structSms)
{
	_smsInfo = structSms;
	_itemType = 1; 
	std::string bigCoinPath = "";
	order++;
	if (order > 5)
		order = 5;
	bigCoinPath = __String::createWithFormat("vs%d.png", order)->getCString();
	_background->setTexture(bigCoinPath.c_str());

	_lbMoneyInGame->setString(EGSupport::convertMoneyAndAddDot(structSms.balance));
	_lbMoneyReal->setString(EGSupport::convertMoneyAndAddDot(structSms.money) + " " + pXml->getNodeTextByTagName("txt_charge_item_vnd"));

	if (structSms.percent > 0){
		_salebg->setVisible(true);
		_lbSale->setString(__String::createWithFormat("+%d%%", structSms.percent)->getCString());
	}
	else

		_salebg->setVisible(false);
	_lbServiceNumber->setString(structSms.serviceNumber);
}

void ChargeItem::setSubInfo(uint8_t order, bool isChip, SubcribeInfo_s structSub)
{
	_subInfo = structSub;
	_itemType = 2;
	std::string bigCoinPath = "";
	order++;
	if (order > 5)
		order = 5;
	bigCoinPath = __String::createWithFormat("vs%d.png", order)->getCString();
	_background->setTexture(bigCoinPath.c_str());

	_lbMoneyInGame->setString(EGSupport::convertMoneyAndAddDot(structSub.balance));
	_lbMoneyReal->setString(EGSupport::convertMoneyAndAddDot(structSub.daily_money) + " " + pXml->getNodeTextByTagName("txt_charge_item_vnd"));

	/*if (_subInfo.percent > 0){
		_salebg->setVisible(true);
		_lbSale->setString(__String::createWithFormat("+%d%%", structSms.percent)->getCString());
	}
	else
		_salebg->setVisible(false);*/
	_salebg->setVisible(false);
	_lbServiceNumber->setString(structSub.serviceNumber);
}

void ChargeItem::setExchangeInfo(uint8_t order, bool isChip, ExchangeInfo_s structSub)
{
	_exchangeInfo = structSub;
	_itemType = 3;
	std::string bigCoinPath = "";
	order++;
	if (order > 5)
		order = 5;
	bigCoinPath = __String::createWithFormat("vs%d.png", order)->getCString();
	_background->setTexture(bigCoinPath.c_str());

	_lbMoneyInGame->setString(EGSupport::convertMoneyAndAddDot(structSub.balance));
	_lbMoneyReal->setString(EGSupport::convertMoneyAndAddDot(structSub.money) + " " + pXml->getNodeTextByTagName("txt_charge_item_vnd"));

	/*if (_subInfo.percent > 0){
	_salebg->setVisible(true);
	_lbSale->setString(__String::createWithFormat("+%d%%", structSms.percent)->getCString());
	}
	else
	_salebg->setVisible(false);*/
	_salebg->setVisible(false);
	_lbServiceNumber->setString(structSub.serviceNumber);
}

void ChargeItem::setVisaInfo(uint8_t order, bool isChip, RechargeVISAInfo structVisa)
{
    _visaInfo = structVisa;
	_itemType = 2;
	std::string bigCoinPath = "";
	order++;
	if (order > 5)
		order = 5;
	bigCoinPath = __String::createWithFormat("vs%d.png", order)->getCString();
	_background->setTexture(bigCoinPath.c_str());

	_lbMoneyInGame->setString(EGSupport::convertMoneyAndAddDot(structVisa.mBalance));
	_lbMoneyReal->setString(__String::createWithFormat("%.2f $", ((float)structVisa.mUSD / 100))->getCString());

	if (structVisa.mPercent > 0){
		_salebg->setVisible(true);
		_lbSale->setString(__String::createWithFormat("+%d%%", structVisa.mPercent)->getCString());
	}
	else
		_salebg->setVisible(false);
}


void ChargeItem::itemClicked()
{
	if (_itemType == 1) // sms
	{
		//EGJniHelper::sendSMS(_smsInfo.syntax.c_str(), _smsInfo.serviceNumber.c_str());
        /*_background->unregisterTouch();
		_background->runAction(RotateBy::create(0.3f, Vec3(0, 180, 0)));
		_background->runAction(Sequence::createWithTwoActions(DelayTime::create(0.15f), CallFunc::create([=]{
            _smsBg->setVisible(true);
        })));

		_background->runAction(Sequence::createWithTwoActions(DelayTime::create(4), RotateBy::create(0.3f, Vec3(0, 180, 0))));
		_background->runAction(Sequence::createWithTwoActions(DelayTime::create(4.15f), CallFunc::create([=]{
            _smsBg->setVisible(false);
        })));
		_background->runAction(Sequence::createWithTwoActions(DelayTime::create(4.3f), CallFunc::create([=]{
            _background->registerTouch();
        })));*/


		//show box notice
		if (_funcShowSms)
			_funcShowSms(_smsInfo);
	}
	else if (_itemType == 2){
		if (_funcShowSub)
			_funcShowSub(_subInfo);
	}
	else { // for visa
		if (_funcShowExchange)
			_funcShowExchange(_exchangeInfo);
		//call library
        //EGJniHelper::showInapp();
	}
}

void ChargeItem::sendSmsClicked()
{
	//call library
	//EGJniHelper::sendSMS(_smsInfo.syntax.c_str(), _smsInfo.serviceNumber.c_str());
}

void ChargeItem::itemUnClicked()
{
	_smsBg->setVisible(false);
	_smsBg->setFlippedX(true);
	_background->runAction(RotateTo::create(0.01f, Vec3( 0, 0, 0)));
	_background->unregisterTouch();
}

