#pragma once

#include "ClientPlayerInfo.h"
#include "EGSprite.h"
#include "EGButtonSprite.h"
#include "cocos2d.h"
#include "WXmlReader.h"
#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "extensions\cocos-ext.h"
#else
#include "cocos-ext.h"
#endif
//#include "ui/CocosGUI.h" 

USING_NS_CC;
USING_NS_CC_EXT;

class BoxMail : public LayerColor, public EditBoxDelegate {
public:
	static BoxMail* createBoxMail();

	void showMessage(uint8_t type, StructMail pInfo);
	std::string getUserName();
	std::string getTitle();
	std::string getContent();
	void setBoxType(int pType);
	void updateText(float pSec);
	void resetEditText();

private:
	void initBoxMail();
	bool checkExistsSpecialCharcter(const std::string& strInput);
	

private:
	void btnLeftClicked();
	void btnCenterClicked(); // delete mail
	void btnRightClicked();


private:
	CC_SYNTHESIZE(int, _type, TypeBox);
	CC_SYNTHESIZE(std::function<void()>, _funcCallBack, FuncCallBack);
	CC_SYNTHESIZE(std::function<void(StructMail)>, _funcDeleteMail, FuncDeleteMail);
	CC_SYNTHESIZE(std::function<void(std::string, std::string, std::string)>, _funcSendMail, FuncSendMail);
	CC_SYNTHESIZE(std::function<void(const std::string&)>, _funcshowNotice, FuncShowNotice);

private:
	virtual void editBoxTextChanged(EditBox* editBox, const std::string& text){};
	virtual void editBoxReturn(EditBox* editBox);
	virtual bool onTouchBegan(Touch*, Event*);
	virtual void onTouchMoved(Touch*, Event*){};
	virtual void onTouchEnded(Touch*, Event*){};
	Label* _lbContent, *_lbUsername, *_lbTitleMail, *_lbUsername_Left;
	Label* _labelUserName;
	EditBox  *_editTitle, *_editContent, *_editName;
	EGSprite* background;
	LayerColor* _blackScene;
	std::string _strUserName;
	WXmlReader* _xmlReader;
	EGButtonSprite* _btnLeft, *_btnCenter, *_btnRight;
	StructMail _structMail;;
};