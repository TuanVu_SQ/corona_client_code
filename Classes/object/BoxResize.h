#ifndef __BOXRESIZE_H_INCLUDED__
#define __BOXRESIZE_H_INCLUDED__
#include "cocos2d.h"
#include "EGButtonSprite.h"

using namespace cocos2d;

class BoxResize : public Node {
public:
	static BoxResize* createBoxResize(const char* pFrameTop, const char* pFrameMid, const char* pFrameBot);
	void initBoxResize(const char* pFrameTop, const char* pFrameMid, const char* pFrameBot);
	void setSize(float pHeight);
	void addButton(EGButtonSprite* pButton,  int pKey = 0);
	void visibleObject(int pKey, bool pVisible);
	void visibleStroke(bool pVisible);
	void clearAllButton();
	void showBox();
	void update(float pSec);
	void closeBox();
	void setAnchor(Vec2 pPoint);
	bool isOpen();
	void setDistance(float pDistance);
	void pauseChedule(){ iPause = true; }
	void reload();
	void setOffsetY(float pOffsetY){ mOffsetY = pOffsetY; };
	void setHeight(float pHeight){ mHeight = pHeight; };
private:
	bool iOpen,iPause;
	float mDistanceItem,mOffsetY,mHeight;
	Sprite* mBoxBody, *mBoxTop,* mBoxBot;
	Vector<EGButtonSprite*> mArrayButton;
};
#endif

