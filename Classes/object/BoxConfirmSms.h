#ifndef __BOXCONFIRM_H_INCLUDED__
#define __BOXCONFIRM_H_INCLUDED__

#include "MessageParam.h"
#include "cocos2d.h"
#include "EGButtonSprite.h"
#include "EGSprite.h"
#include "ClientPlayerInfo.h"
#include "libs/WXmlReader.h"
#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "extensions\cocos-ext.h"
#else
#include "cocos-ext.h"
#endif

USING_NS_CC;
USING_NS_CC_EXT;

class BoxConfirmSms :public EGSprite {
public:
	Label* mLbl_Title3, *mLbl_Title4_2, *mLbl_Title5, *lb_ingame_confirm, *lbErrorChargeInGame;
	EGButtonSprite* mBtnAccept, *mBtnReject, *mBtnSkip, *btnChargeSms, *btnChargeInGame;
	RechargeInfo m_StructInfo;
	SubcribeInfo_s m_SubInfo;
	ExchangeInfo m_ExchangeInfo;
	WXmlReader* pXml;
	

	static BoxConfirmSms* create();
	void showInfo(RechargeInfo info);
	void showSubConfirm(SubcribeInfo_s info);
	void showExchangeConfirm(ExchangeInfo info);
	void ShowOtpToCharge(string transId);
	void showDescription(std::string content);
	void Reset();
	void showSmsNode();
	void showIngameNode();

public:
	EditBox* edtInput;


private:
	int _type = 0;
	int _chargeType = 0; //0:sms 1:ingame
	int _chargeStep = 1; //1:request 2:confirm
	string _transId = "";

private:
	Node *_nodeSms, *_nodeInGame;

private:
	void initObject();

private:
	void btnAcceptClicked();
	void btnIgnoreClicked();
	void doRechargeInGame();

	

	virtual bool ccTouchBegan(Touch* touch, Event* event) {
		return this->isVisible();
	}
};

#endif