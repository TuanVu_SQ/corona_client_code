#include "BoxInfo.h"
#include "GameInfo.h"
#include "WXmlReader.h"

BoxInfo* BoxInfo::create(bool isInGame) {
	BoxInfo* pBoxInfo = new BoxInfo();
	pBoxInfo->initObject(isInGame);
	pBoxInfo->autorelease();

	return pBoxInfo;
}

void BoxInfo::initObject(bool isInGame) {
	EGSprite::initWithSpriteFrameName(IMG_BOX_INFO);

	mTitle = Sprite::createWithSpriteFrameName(IMG_TITLE_INFO);
	mTitle->setPosition(Vec2(mTitle->getContentSize().width / 2 + 20, this->getContentSize().height - mTitle->getContentSize().height / 2 - 30));
	this->addChild(mTitle);

	EGButtonSprite* pBtnClose = EGButtonSprite::createWithSpriteFrameName(IMG_X, EGButton2FrameDark);
	pBtnClose->setOnTouch(CC_CALLBACK_0(BoxInfo::setVisible, this, false));
	pBtnClose->setPosition(Vec2(this->getContentSize().width, this->getContentSize().height));
	this->addChild(pBtnClose);

	mAvatar = NULL;

	this->registerTouch();
	this->setVisible(false);
}

void BoxInfo::showInfo(UserInfo pUserInfo, bool isFriend) {
	WXmlReader* pXml = WXmlReader::create();
	pXml->load(KEY_XML_FILE);

	if (!mAvatar) {
		EGSprite* pSpriteAvatarBorder = EGSprite::createWithSpriteFrameName(IMG_AVATAR_BORDER);
		pSpriteAvatarBorder->setPosition(Vec2(pSpriteAvatarBorder->getContentSize().width / 2 + 20, this->getContentSize().height / 2 - 20));
		this->addChild(pSpriteAvatarBorder);

		mAvatar = Sprite::createWithSpriteFrameName(__String::createWithFormat("avatar (%d).png", pUserInfo.mAvatarID)->getCString());
		mAvatar->setPosition(pSpriteAvatarBorder->getPosition());
		this->addChild(mAvatar);

		std::string pStrUsername = pXml->getNodeTextByTagName(TEXT_USERNAME);
		pStrUsername += ": ";
		mLblUsername = LabelTTF::create(pStrUsername, FONT, 15);
		mLblUsername->setPosition(Vec2(pSpriteAvatarBorder->getPositionX2() + mLblUsername->getContentSize().width / 2 + 10,
			pSpriteAvatarBorder->getPositionY2() - mLblUsername->getContentSize().height / 2 - 10));
		this->addChild(mLblUsername);

		std::string pStrFullname = pXml->getNodeTextByTagName(TEXT_FULLNAME);
		pStrFullname += ": ";
		mLblFullname = LabelTTF::create(pStrFullname, FONT, 15);
		mLblFullname->setPosition(Vec2(pSpriteAvatarBorder->getPositionX2() + mLblFullname->getContentSize().width / 2 + 10,
			mLblUsername->getPositionY() - 23));
		this->addChild(mLblFullname);

		std::string pStrGender = pXml->getNodeTextByTagName(TEXT_GENDER);
		pStrGender += ": ";
		mLblGender = LabelTTF::create(pStrGender, FONT, 15);
		mLblGender->setPosition(Vec2(pSpriteAvatarBorder->getPositionX2() + mLblGender->getContentSize().width / 2 + 10,
			mLblFullname->getPositionY() - 23));
		this->addChild(mLblGender);

		std::string pStrLevel = pXml->getNodeTextByTagName(TEXT_LEVEL);
		pStrLevel += ": ";
		mLblLevel = LabelTTF::create(pStrLevel, FONT, 15);
		mLblLevel->setPosition(Vec2(pSpriteAvatarBorder->getPositionX2() + mLblLevel->getContentSize().width / 2 + 10,
			mLblGender->getPositionY() - 23));
		this->addChild(mLblLevel);

		std::string pStrBalance = pXml->getNodeTextByTagName(TEXT_BALANCE);
		pStrBalance += ": ";
		mLblBalance = LabelTTF::create(pStrBalance, FONT, 15);
		mLblBalance->setPosition(Vec2(pSpriteAvatarBorder->getPositionX2() + mLblBalance->getContentSize().width / 2 + 10,
			mLblLevel->getPositionY() - 23));
		this->addChild(mLblBalance);

		std::string pStrBirthday = pXml->getNodeTextByTagName(TEXT_BIRTHDAY);
		pStrBirthday += ": ";
		mLblBirthday = LabelTTF::create(pStrBirthday, FONT, 15);
		mLblBirthday->setPosition(Vec2(pSpriteAvatarBorder->getPositionX2() + mLblBirthday->getContentSize().width / 2 + 10,
			mLblBalance->getPositionY() - 23));
		this->addChild(mLblBirthday);

		std::string pStrEmail = pXml->getNodeTextByTagName(TEXT_EMAIL);
		pStrEmail += ": ";
		mLblEmail = LabelTTF::create(pStrEmail, FONT, 15);
		mLblEmail->setPosition(Vec2(pSpriteAvatarBorder->getPositionX2() + mLblEmail->getContentSize().width / 2 + 10,
			mLblBirthday->getPositionY() - 23));
		this->addChild(mLblEmail);

		std::string pStrStatus = pXml->getNodeTextByTagName(TEXT_STATUS);
		pStrStatus += ": ";
		mLblStatus = LabelTTF::create(pStrStatus, FONT, 15);
		mLblStatus->setPosition(Vec2(pSpriteAvatarBorder->getPositionX2() + mLblStatus->getContentSize().width / 2 + 10,
			mLblEmail->getPositionY() - 23));
		this->addChild(mLblStatus);

		mLblUsername_Values = LabelTTF::create("", FONT, 15);
		this->addChild(mLblUsername_Values);
		mLblFullname_Values = LabelTTF::create("", FONT, 15);
		this->addChild(mLblFullname_Values);
		mLblGender_Values = LabelTTF::create("", FONT, 15);
		this->addChild(mLblGender_Values);
		mLblBalance_Values = LabelTTF::create("", FONT, 15);
		this->addChild(mLblBalance_Values);
		mLblEmail_Values = LabelTTF::create("", FONT, 15);
		this->addChild(mLblEmail_Values);
		mLblStatus_Values = LabelTTF::create("", FONT, 15);
		this->addChild(mLblStatus_Values);
	}
	else {
		mAvatar->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName(__String::createWithFormat("avatar_%d.png", pUserInfo.mAvatarID)->getCString()));
	}

	mLblUsername_Values->setString(pUserInfo.mUsername);
	mLblUsername_Values->setPosition(Vec2(300 + mLblUsername_Values->getContentSize().width / 2, mLblUsername->getPositionY()));

	mLblFullname_Values->setString(pUserInfo.mFullname);
	mLblFullname_Values->setPosition(Vec2(300 + mLblFullname_Values->getContentSize().width / 2, mLblFullname->getPositionY()));

	std::string pStrGender = pXml->getNodeTextByTagName(TEXT_FEMALE);
	if (pUserInfo.mGender == 1) {
		pStrGender = pXml->getNodeTextByTagName(TEXT_MALE);
	}
	mLblGender_Values->setString(pStrGender);
	mLblGender_Values->setPosition(Vec2(300 + mLblGender_Values->getContentSize().width / 2, mLblGender->getPositionY()));

	std::stringstream ssBalance; ssBalance << pUserInfo.mBalance;
	mLblBalance_Values->setString(ssBalance.str());
	mLblBalance_Values->setPosition(Vec2(300 + mLblBalance_Values->getContentSize().width / 2, mLblBalance->getPositionY()));

	mLblEmail_Values->setString(pUserInfo.mEmail);
	mLblEmail_Values->setPosition(Vec2(300 + mLblEmail_Values->getContentSize().width / 2, mLblEmail->getPositionY()));

	this->setVisible(true);
}

void BoxInfo::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags) {
	_customCommand.init(_globalZOrder);
	_customCommand.func = CC_CALLBACK_0(BoxInfo::onDraw, this, transform, flags);
	renderer->addCommand(&_customCommand);

	Sprite::draw(renderer, transform, flags);
}

void BoxInfo::onDraw(const Mat4 &transform, uint32_t flags) {
	Director* director = Director::getInstance();
	director->pushMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
	director->loadMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW, transform);

	DrawPrimitives::drawSolidRect(Vec2(-this->getPositionX(), -this->getPositionY()), Vec2(CLIENT_WIDTH, CLIENT_HEIGHT), Color4F(0, 0, 0, 0.9f));
}