#include "MailView.h"
#include "WClock.h"

MailView* MailView::createMailView(){
	auto sp = new MailView();
	sp->initMailView();
	sp->autorelease();
	return sp;
}
void MailView::initMailView(){
	initWithFile("view_mail_length.png");
	setOpacity(200);
	_mXmlreader = WXmlReader::create();
	_mXmlreader->load(KEY_XML_FILE);
	_labelName = Label::createWithBMFont("font.fnt", _mXmlreader->getNodeTextByTagName("txt_mail_to").c_str());
	this->addChild(_labelName);
	_labelName->setAnchorPoint(Vec2(0, 0.5f));
	_labelName->setScale(0.4f);
	_labelName->setPosition(Vec2(3, getContentSize().height / 2 + _labelName->getContentSize().height*_labelName->getScale() / 2 + 2));
	_labelName->setColor(Color3B::YELLOW);

	_username = Label::createWithBMFont("font.fnt", "Son Cho");
	addChild(_username);
	_username->setScale(0.4f);

	Label* _labelTitle = Label::createWithBMFont("font.fnt", _mXmlreader->getNodeTextByTagName("txt_mail_title").c_str());
	this->addChild(_labelTitle);
	_labelTitle->setAnchorPoint(Vec2(0, 0.5f));
	_labelTitle->setScale(0.4f);
	_labelTitle->setPosition(Vec2(3, getContentSize().height / 2 - _labelTitle->getContentSize().height*_labelName->getScale() / 2 - 2));
	_labelTitle->setColor(Color3B::YELLOW);

	_title = Label::createWithBMFont("font.fnt", "Mr.SH");
	addChild(_title);
	_title->setScale(0.4f);

	pMailStatus = EGSprite::createWithFile("status_mail0.png");
	this->addChild(pMailStatus);
	pMailStatus->setPosition(Vec2(getContentSize().width - 50, getContentSize().height / 2));

	_datetime = Label::createWithBMFont("font.fnt", "26/10/1992 \n 69:96");
	_datetime->setScale(0.4f);
	_datetime->setAlignment(TextHAlignment::CENTER);
	addChild(_datetime);
	_datetime->setPosition(Vec2(getContentSize().width - 150, getContentSize().height / 2));
}
void MailView::setInfoMaill(uint8_t type, StructMail pStructMail){
	if (type == 2){
		this->removeAllChildrenWithCleanup(true);
		auto lb = Label::createWithBMFont("font.fnt", _mXmlreader->getNodeTextByTagName("txt_mail_load_new_page"));
		lb->setScale(0.45f);
		lb->setColor(Color3B::YELLOW);
		lb->setPosition(this->getContentSize()/2);
		this->addChild(lb);
		return;
	}
	std::string pUserName = "";
	if (type == 1){
		_labelName->setString(_mXmlreader->getNodeTextByTagName("txt_mail_to").c_str());
		pUserName = pStructMail.receiver;
	}
	else{
		_labelName->setString(_mXmlreader->getNodeTextByTagName("txt_mail_from").c_str());
		pUserName = pStructMail.sender;
	}

	std::string pTitle = pStructMail.subject;
	uint8_t bonusTime = WClock::getTimeZone();
	tm *time = WClock::convertIntToTime(pStructMail.created + bonusTime*60*60);
	std::string pHour = __String::createWithFormat("%d", time->tm_hour)->getCString();
	if (time->tm_hour < 10){
		pHour = "0" + pHour;
	}
	std::string min = __String::createWithFormat("%d", time->tm_min)->getCString();
	if (time->tm_min < 10){
		min = "0" + min;
	}
	std::string pTime = __String::createWithFormat("%d/%d/%d \n %s:%s", time->tm_mday, time->tm_mon + 1, time->tm_year + 1900, pHour.c_str(), min.c_str())->getCString();
	_username->setString(pUserName);
	_title->setString(pTitle);
	_username->setPosition(Vec2(100 + _username->getContentSize().width* _username->getScale() / 2, getContentSize().height / 2 + _username->getContentSize().height*_username->getScale() / 2 + 2));
	_title->setPosition(Vec2(100 + _title->getContentSize().width* _title->getScale() / 2, getContentSize().height / 2 - _title->getContentSize().height*_title->getScale() / 2 - 2));
	_datetime->setString(pTime);
	if (type == 0){
		if (pStructMail.status == 0) 
			pMailStatus->setTexture("status_mail0.png");
		else 
			pMailStatus->setTexture("status_mail1.png");
	}
	else
		pMailStatus->setTexture("status_mail1.png");
	mMailInfo = pStructMail;
}
StructMail MailView::getInfo(){
	return mMailInfo;
}

void MailView::changeReadState()
{
	pMailStatus->setTexture("status_mail1.png");
}