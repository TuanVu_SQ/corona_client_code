#ifndef __BOXINFO_H_INCLUDED__
#define __BOXINFO_H_INCLUDED__

#include "cocos2d.h"
#include "UserInfo.h"
#include "EGSprite.h"
#include "EGButtonSprite.h"

USING_NS_CC;

class BoxInfo :public EGSprite {
public:
	static BoxInfo* create(bool isInGame);

	void showInfo(UserInfo pUserInfo, bool isFriend);
private:
	CustomCommand _customCommand;

	Sprite* mTitle;
	Sprite* mAvatar;

	LabelTTF *mLblUsername, *mLblFullname, *mLblEmail, *mLblBirthday, *mLblGender, *mLblLevel, *mLblBalance, *mLblStatus;
	LabelTTF* mLblUsername_Values, *mLblFullname_Values, *mLblEmail_Values, *mLblGender_Values, *mLblLevel_Values, *mLblBalance_Values, *mLblStatus_Values;

	void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags);
	void onDraw(const Mat4 &transform, uint32_t flags);

	bool mIsInGame;
	void initObject(bool isInGame);

	virtual bool ccTouchBegan(Touch* touch, Event* event) {
		return this->isVisible();
	}
};

#endif