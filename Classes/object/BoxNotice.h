#ifndef __BOXNOTICE_H_INCLUDED__
#define __BOXNOTICE_H_INCLUDED__

#include "cocos2d.h"
#include "EGButtonSprite.h"
#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "extensions\cocos-ext.h"
#else
#include "cocos-ext.h"
#endif
#include "EGSprite.h"

USING_NS_CC;
using namespace extension;

class BoxNotice :public EGSprite {
public:
	Label* mLbl_Title, *mLbl_Content;
	EGButtonSprite* mBtnAccept, *mBtnReject, *mBtnSkip;
	EditBox* mEdt_Input;

	static BoxNotice* create();

	void setTitle(const char* pTitle);
	void setContent(const char* pContent);
	void setButtonAccept(const char* pTextButtonAccept, std::function<void()> pFuncAccept);
	void setButtonReject(const char* pTextButtonReject, std::function<void()> pFuncReject);
	void setButtonSkip(const char* pTextButtonSkip, std::function<void> pFuncSkip);
	void clearAll();
private:
	void initObject();

	virtual bool ccTouchBegan(Touch* touch, Event* event) {
		return this->isVisible();
	}
};

#endif