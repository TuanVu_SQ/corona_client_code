#pragma once

//#include "m2w_libs/WLibrary.h"
//#include "message/StructInfo.h"
//#include "layer/TopLayer.h"
//#include "m2w_libs/helper/WHelper.h"
#include "libs/EGSprite.h"
#include "libs/EGButtonSprite.h"
#include "ClientPlayerInfo.h"
#include "libs/WXmlReader.h"
#include "libs/EGJniHelper.h"

class ChargeItem : public EGSprite {

public:
	static ChargeItem* create();

private:
	void initItem();

public:
	void setSmsInfo(uint8_t order, bool isChip, RechargeInfo structSms);
	void setSubInfo(uint8_t order, bool isChip, SubcribeInfo_s structSms);
	void setExchangeInfo(uint8_t order, bool isChip, ExchangeInfo_s structSms);
	void setVisaInfo(uint8_t order, bool isChip, RechargeVISAInfo structVisa);

	void itemClicked();
	void itemUnClicked();
	void sendSmsClicked();

private:
	CC_SYNTHESIZE(std::function<void(uint32_t, uint32_t)>, _funcExchange, FuncExchange);
	CC_SYNTHESIZE(std::function<void()>, _funcReCharge, FuncReCharge);
	CC_SYNTHESIZE(std::function<void(std::string, std::string, std::string)>, _funcShowSyntax, FuncShowSyntax);
	CC_SYNTHESIZE(std::function<void()>, _funcRoolBack, FuncRoolBack);
	CC_SYNTHESIZE(std::function<void(RechargeInfo)>, _funcShowSms, FuncShowSms);
	CC_SYNTHESIZE(std::function<void(SubcribeInfo_s)>, _funcShowSub, FuncShowSub);
	CC_SYNTHESIZE(std::function<void(ExchangeInfo)>, _funcShowExchange, FuncShowExchange);

private:
	uint8_t _itemType; //1:coin, 2:chip
	uint32_t _numberChip, _numberCoin;
	WXmlReader* pXml;
	bool _isChip;
	EGSprite *_coinIcon, *_iconCoinBottom, *_bigCoin, *_salebg, *moneyBg;
	Label* _lbMoneyInGame, *_lbMoneyReal, *_lbSale, *_lbServiceNumber;
	EGButtonSprite* _smsBg, *_background;

	RechargeInfo _smsInfo;
	RechargeVISAInfo _visaInfo;
	SubcribeInfo_s _subInfo;
	ExchangeInfo_s _exchangeInfo;

private:
	ChargeItem();
	~ChargeItem();
};