#ifndef __BOXADS_H_INCLUDED__
#define __BOXADS_H_INCLUDED__

#include "cocos2d.h"
#include "EGButtonSprite.h"
#include "EGSprite.h"

USING_NS_CC;

class BoxAds :public EGSprite {

private:
	EGButtonSprite* _sprBanner;

public:
	static BoxAds* create();
	void showBoxAds();

private:
	void initObject();
	bool touchBegan(Touch* touch, Event* event) {
		return this->isVisible();
	}

};

#endif