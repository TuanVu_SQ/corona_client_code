#include "BoxActiveAccount.h"
#include "GameInfo.h"
#include "libs/EGJniHelper.h"
#include "libs/EGSupport.h"
#include "scene/HomeScene.h"


BoxActiveAccount* BoxActiveAccount::create() {
	BoxActiveAccount* pBoxActiveAccount = new BoxActiveAccount();
	pBoxActiveAccount->initObject();
	pBoxActiveAccount->autorelease();

	return pBoxActiveAccount;
}

void BoxActiveAccount::initObject() {
	EGSprite::initWithSpriteFrameName(IMG_BOX_NOTICE);

	LayerColor* pLayer = LayerColor::create(Color4B(0, 0, 0, 200));
	pLayer->setPosition(Vec2(this->getContentSize().width / 2 - CLIENT_WIDTH / 2, this->getContentSize().height / 2 - CLIENT_HEIGHT / 2));
	this->addChild(pLayer, -1);

	pXml = WXmlReader::create();
	pXml->load(KEY_XML_FILE);

	//auto mLbl_Title1 = Label::createWithBMFont("font_white.fnt", pXml->getNodeTextByTagName("txt_enter_otp_to_active"));
	auto mLbl_Title1 = Label::createWithBMFont("font-info-game.fnt", "VERIFIKASYON KONT");
	mLbl_Title1->setScale(1.2f);
	mLbl_Title1->setPosition(Vec2(this->getContentSize().width / 2, this->getContentSize().height - mLbl_Title1->getContentSize().height - 15));
	mLbl_Title1->setColor(Color3B::ORANGE);
	this->addChild(mLbl_Title1);

	edtInput = cocos2d::ui::EditBox::create(Size(150, 42), Scale9Sprite::create("editbox_small.png"));
	edtInput->setInputFlag(cocos2d::ui::EditBox::InputFlag::SENSITIVE);
	edtInput->setInputMode(EditBox::InputMode::NUMERIC);
	edtInput->setPosition(Vec2(this->getContentSize().width / 2, this->getContentSize().height / 2));
	edtInput->setFont("fonts/arial.ttf", 18);
	edtInput->setMaxLength(15);
	edtInput->setPlaceholderFont("fonts/arial.ttf", 18);
	edtInput->setFontColor(Color3B::WHITE);
	edtInput->setPlaceHolder(pXml->getNodeTextByTagName("txt_enter_otp").c_str());
	edtInput->setPlaceholderFontColor(Color3B(135, 206, 235));
	this->addChild(edtInput,2);

	auto mLbl_Title2 = Label::createWithBMFont("font_black.fnt", pXml->getNodeTextByTagName("txt_enter_otp_to_active"));
	mLbl_Title2->setScale(1.1f);
	mLbl_Title2->setPosition(Vec2(mLbl_Title1->getPosition()) + Vec2(0, -60));
	mLbl_Title2->setColor(Color3B::RED);
	this->addChild(mLbl_Title2);

	mLbl_Des = Label::createWithBMFont("font.fnt", "");
	mLbl_Des->setPosition(Vec2(edtInput->getPosition()) + Vec2(0, -45));
	mLbl_Des->setScale(0.4f);
	mLbl_Des->setColor(Color3B::RED);
	this->addChild(mLbl_Des);

	mBtnAccept = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_GREEN, EGButton2FrameDark);
	mBtnAccept->setPosition(Vec2(this->getContentSize().width / 2 + mBtnAccept->getContentSize().width/2 + 10, mBtnAccept->getContentSize().height / 2 + 20));
	mBtnAccept->setText(pXml->getNodeTextByTagName("accept").c_str(), FONT, 20);
	this->addChild(mBtnAccept);
	mBtnAccept->setOnTouch(CC_CALLBACK_0(BoxActiveAccount::btnAcceptClicked, this));

	mBtnResendOtp = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_GREEN, EGButton2FrameDark);
	mBtnResendOtp->setPosition(Vec2(this->getContentSize().width / 2 - mBtnResendOtp->getContentSize().width/2 - 10, mBtnAccept->getContentSize().height / 2 + 20));
	mBtnResendOtp->setText("Resend otp", FONT, 20);
	this->addChild(mBtnResendOtp);
	mBtnResendOtp->setOnTouch(CC_CALLBACK_0(BoxActiveAccount::btnResendClicked, this));

	btnClose = EGButtonSprite::createWithFile("btn_close.png");
	this->addChild(btnClose);
	btnClose->setVisible(false);
	btnClose->setPosition(Vec2(this->getContentSize().width - 10, this->getContentSize().height - 10));
	btnClose->setOnTouch([=]{
		this->setVisible(false);
		this->setPosition(Vec2(0, 1000));

		Scene* pScene = HomeScene::scene();
		TransitionFade* pTrans = TransitionFade::create(0.1f, pScene);
		Director::getInstance()->replaceScene(pTrans);
	});


	this->setVisible(false);
	this->registerTouch();
}

void BoxActiveAccount::btnAcceptClicked()
{
	std::string otp = edtInput->getText();
	if (otp.length() == 0) return;
	if (_funcSendOtp)
		_funcSendOtp(edtInput->getText());

	/*this->setVisible(false);
	this->setPosition(Vec2(1300, 240));*/
}

void BoxActiveAccount::btnResendClicked()
{
	if (_funcResendOtp)
		_funcResendOtp();
}

void BoxActiveAccount::showDescription(std::string content){
	if (content.length() > 0){
		mLbl_Des->setString(content);
		mLbl_Des->setVisible(true);
	}
	else
		mLbl_Des->setVisible(false);
}

void BoxActiveAccount::successActive(){
	mBtnResendOtp->setVisible(false);
	mBtnAccept->setVisible(false);
	btnClose->setVisible(true);
}
