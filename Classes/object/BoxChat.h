#ifndef __BOXCHAT_H_INCLUDED__
#define __BOXCHAT_H_INCLUDED__

#include "cocos2d.h"
#include "EGSprite.h"
#include "EGButtonSprite.h"
#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "extensions\cocos-ext.h"
#else
#include "cocos-ext.h"
#endif

USING_NS_CC;
USING_NS_CC_EXT;

class BoxChat : public Node, public TableViewDataSource, public TableViewDelegate, public EditBoxDelegate {
public:
	static BoxChat* create(std::string pUsername, std::function<void(std::string pContent)> pFuncOnSendChat, std::function<void(std::string pEmotionType, int pEmotionID)> pFuncOnSendEmotion);

	void setOnOpen(std::function<void()> pFuncOnOpen);
	void setOnClose(std::function<void()> pFuncOnClose);
	void updateChat(std::string pName, std::string pContent);
	void updateChatWithEmotion(std::string pName, std::string pEmotionType, int pEmotionID);
private:
	EGSprite* mBox;
	EGButtonSprite** mBtnMenu;
	EGButtonSprite* mBtnSend;
	EditBox* mEditBox;
	TableView* mTableView;
	TableViewCell* mTableViewCell;
	EGButtonSprite* mBtnController;
	Node** mNode;
	std::function<void()> mFuncOnOpen, mFuncOnClose;
	std::function<void(std::string pContent)> mFuncOnSendChat;
	std::function<void(std::string pEmotionType, int pEmotionID)> mFuncOnSendEmotion;
	bool isOpen;
	std::string mUsername;
	Size mTableSize;
	std::vector<float> mLstChatHeight;
	std::vector<Node*> mLstChatObject;

	void initObject();

	void onOpen();
	void onClose();
	void onSend();
	void onChatText(std::string pContent);
	void onChatEmotion(std::string pEmotionType, int pEmotionID);
	void onChooseWindows(int pIndex);

	virtual void scrollViewDidScroll(ScrollView* view) {};
	virtual void scrollViewDidZoom(ScrollView* view) {};
	virtual void tableCellTouched(TableView* table, TableViewCell* cell) {};
	virtual Size tableCellSizeForIndex(TableView *table, ssize_t idx);
	virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	virtual bool ccTouchBeganFake(Touch* touch, Event* event);
	virtual void editBoxReturn(EditBox* editBox);
};

#endif