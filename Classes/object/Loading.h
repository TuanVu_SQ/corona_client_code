#ifndef __LOADING_H_INCLUDED__
#define __LOADING_H_INCLUDED__

#include "cocos2d.h"
#include "EGSprite.h"

USING_NS_CC;

class Loading :public EGSprite {
public:
	static Loading* create();
	void setSize(Size pSize);
	void show();
	void hide();
private:
	void initObject();
	LayerColor* pLayer;
	virtual bool ccTouchBegan(Touch* touch, Event* event) {
		Size s = getContentSize();
		Rect rect =  Rect( - mSize.width/2,  - mSize.height/2,mSize.width,mSize.height+ 100);
		if (rect.containsPoint(convertTouchToNodeSpaceAR(touch)) && this->isVisible()){
			return true;
		}
		return false;
	}
	Size mSize;
};

#endif