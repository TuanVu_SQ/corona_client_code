#ifndef __BOXINFOGAME_H_INCLUDED__
#define __BOXINFOGAME_H_INCLUDED__
#include "cocos2d.h"
#include "GameInfo.h"
#include "ClientPlayerInfo.h"
#include "EGButtonSprite.h"
#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#include "extensions\cocos-ext.h"
#else
#include "cocos-ext.h"
#endif

using namespace extension;
using namespace cocos2d;

class BoxInfoGame :public Node {
public:
	static BoxInfoGame* createBoxInfo();
	void initBoxInfo();
	void showBox(ClientPlayerInfoEX * pInfo,  ClientPlayerInfoEX* pMyselft);
	void closeBox();
	void setOnTouchAddFriend(std::function<void()> pTouchFunction);
	void setOnTouchKickPlayer(std::function<void()> pTouchFunction);
	void setOnTouchChangePosition(std::function<void()> pTouchFunction);
	void setType(int pType);
	void showToastReal(std::string pToast, float pWidth, Vec2 pPosition);
	void showInfoAchieve(int index,int pLevel);
private:
	CC_SYNTHESIZE(std::function<void(std::string, std::string, std::string)>, _funcChangePass, FuncChangePass);
	EGButtonSprite* buttonChangePass;
	virtual bool ccTouchBegan(Touch* touch, Event* event);
	EditBox* mEdit_OldPass, *mEdit_NewPass, *mEdit_NewPass1;
	EGButtonSprite * buttonkick, *buttonSwap, *buttonFriend;
	Sprite * mAvatarSprite;
	Label* mNameLabel, *mLabelWinCount, *mLabelLoseCount, *mLabelRank,*mLabelMoney,* mLabelLevel;
};
#endif