#ifndef __OBJFRIEND_H_INCLUDED__
#define __OBJFRIEND_H_INCLUDED__

#include "cocos2d.h"
#include "EGButtonSprite.h"
#include "libs\EGButtonSprite.h"

USING_NS_CC;

class ObjFriend :public EGButtonSprite {
public:
	static ObjFriend* create(unsigned int pAvatarID, std::string pTxtName,
		unsigned long pBalance, unsigned int pLevel, int pStatus, std::function<void()> pShowInfoFunc, bool isPlayHard = false);
	static ObjFriend* create(unsigned int pAvatarID, std::string pTxtName,
		std::function<void()> pAcceptFunc, std::function<void()> pDenyFunc, std::function<void()> pShowInfoFunc);
	void setTypeTop(int pTypeTop);
	void setOntouchInvite(std::function<void()> pInviteFunc){
		EGButtonSprite* button =(EGButtonSprite*) this->getChildByTag(101);
		if (button){
			button->setOnTouch(pInviteFunc);
		}
	}
	void visibleButtonInvite(bool pBool);
	std::string getUserName(){ return mUserName; }
private:
	void initObject(unsigned int pAvatarID, std::string pTxtName,
		unsigned long pBalance, unsigned int pLevel, int pStatus, std::function<void()> pShowInfoFunc, bool isPlayHard);
	void initObject(unsigned int pAvatarID, std::string pTxtName,
		std::function<void()> pAcceptFunc, std::function<void()> pDenyFunc, std::function<void()> pShowInfoFunc);
	std::string mUserName;

	std::string itos(int n);
};

#endif