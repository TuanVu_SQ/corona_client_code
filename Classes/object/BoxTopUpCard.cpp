#include "BoxTopUpCard.h"
#include "GameInfo.h"
#include "WXmlReader.h"

BoxTopUpCard* BoxTopUpCard::create(std::function<void(std::string, std::string, std::string, std::string)> pFunctionRechargeByCard) {
	BoxTopUpCard* pBoxTopUpCard = new BoxTopUpCard();
	pBoxTopUpCard->initObject(pFunctionRechargeByCard);
	pBoxTopUpCard->autorelease();

	return pBoxTopUpCard;
}

void BoxTopUpCard::initObject(std::function<void(std::string, std::string, std::string, std::string)> pFunctionRechargeByCard) {
	mFunctionRechargeByCard = pFunctionRechargeByCard;
	initWithColor(Color4B(0, 0, 0, 200));

	mBox = Sprite::createWithSpriteFrameName("card_box.png");
	mBox->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBox);

	mBtnTelco = new EGButtonSprite*[3];
	mCardEnable = new Sprite*[3];
	for (int i = 0; i < 3; i++) {
		std::string pFileName = "";
		std::string pFileNameEnable = "card_disable.png";
		switch (i) {
		case 0:
			pFileName = "card_vms.png";
			pFileNameEnable = "card_enable.png";
			break;
		case 1:pFileName = "card_vnp.png"; break;
		case 2:pFileName = "card_vtt.png"; break;
		}
		mBtnTelco[i] = EGButtonSprite::createWithSpriteFrameName(pFileName.c_str(), EGButton2FrameDark);
		mBtnTelco[i]->setOnTouch(CC_CALLBACK_0(BoxTopUpCard::chooseTelco, this, i));
		mBtnTelco[i]->setPosition(Vec2(mBtnTelco[i]->getContentSize().width / 2 + 5,
			mBox->getContentSize().height - (mBtnTelco[i]->getContentSize().height + 6)*(i + 0.5f) - 45));
		mBox->addChild(mBtnTelco[i]);

		mCardEnable[i] = Sprite::createWithSpriteFrameName(pFileNameEnable);
		mCardEnable[i]->setPosition(Vec2(mBtnTelco[i]->getPositionX2() + mCardEnable[i]->getContentSize().width / 2 + 5, mBtnTelco[i]->getPositionY()));
		mBox->addChild(mCardEnable[i]);
	}

	mTelcoCode = "VMS";

	Scale9Sprite* pEdtSprite_Serial = Scale9Sprite::createWithSpriteFrameName("card_input.png");
	mEdt_Serial = EditBox::create(Size(228, 45), pEdtSprite_Serial);
	mEdt_Serial->setPosition(Vec2(350, 269));
	mEdt_Serial->setFontName(FONT);
	mEdt_Serial->setFontSize(20);
	//mEdt_Username->setMargins(20, 20);
	mEdt_Serial->setInputFlag(EditBox::InputFlag::SENSITIVE);
	mEdt_Serial->setFontColor(Color3B::BLACK);
	mEdt_Serial->setPlaceholderFontColor(Color3B::GRAY);
	mEdt_Serial->setReturnType(EditBox::KeyboardReturnType::DONE);
	mEdt_Serial->setMaxLength(20);
	mBox->addChild(mEdt_Serial);

	Scale9Sprite* pEdtSprite_CardNumber = Scale9Sprite::createWithSpriteFrameName("card_input.png");
	mEdt_CardNumber = EditBox::create(Size(228, 45), pEdtSprite_CardNumber);
	mEdt_CardNumber->setPosition(Vec2(350, 219));
	mEdt_CardNumber->setFontName(FONT);
	mEdt_CardNumber->setFontSize(20);
	//mEdt_Username->setMargins(20, 20);
	mEdt_CardNumber->setInputFlag(EditBox::InputFlag::SENSITIVE);
	mEdt_CardNumber->setInputMode(EditBox::InputMode::NUMERIC);
	mEdt_CardNumber->setFontColor(Color3B::BLACK);
	mEdt_CardNumber->setPlaceholderFontColor(Color3B::GRAY);
	mEdt_CardNumber->setReturnType(EditBox::KeyboardReturnType::DONE);
	mEdt_CardNumber->setMaxLength(20);
	mBox->addChild(mEdt_CardNumber);

	Scale9Sprite* pEdtSprite_Username = Scale9Sprite::createWithSpriteFrameName("card_input.png");
	mEdt_Username = EditBox::create(Size(228, 45), pEdtSprite_Username);
	mEdt_Username->setPosition(Vec2(350, 169));
	mEdt_Username->setFontName(FONT);
	mEdt_Username->setFontSize(20);
	//mEdt_Username->setMargins(20, 20);
	mEdt_Username->setInputFlag(EditBox::InputFlag::SENSITIVE);
	mEdt_Username->setFontColor(Color3B::BLACK);
	mEdt_Username->setPlaceholderFontColor(Color3B::GRAY);
	mEdt_Username->setMaxLength(REQ_MAXLENGHT_USERNAME);
	mEdt_Username->setReturnType(EditBox::KeyboardReturnType::DONE);
	mBox->addChild(mEdt_Username);

	WXmlReader* pXml = WXmlReader::create();
	pXml->load(KEY_XML_FILE);
	Label* pInfo = Label::createWithBMFont("font_white.fnt", pXml->getNodeTextByTagName("content_card"), TextHAlignment::CENTER, mBox->getContentSize().width - 50);
	pInfo->setPosition(Vec2(mBox->getContentSize().width / 2, 100));
	mBox->addChild(pInfo);

	mBtnRecharge = EGButtonSprite::createWithSpriteFrameName("card_submit.png", EGButton2FrameDark);
	mBtnRecharge->setOnTouch(CC_CALLBACK_0(BoxTopUpCard::recharge, this));
	mBtnRecharge->setPosition(Vec2(mBox->getContentSize().width / 3 - 50, mBtnRecharge->getContentSize().height / 2 + 10));
	mBox->addChild(mBtnRecharge);

	mBtnClose = EGButtonSprite::createWithSpriteFrameName("card_cancel.png", EGButton2FrameDark);
	mBtnClose->setOnTouch(CC_CALLBACK_0(BoxTopUpCard::close, this));
	mBtnClose->setPosition(Vec2(2 * mBox->getContentSize().width / 3 + 50, mBtnRecharge->getPositionY()));
	mBox->addChild(mBtnClose);

	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(BoxTopUpCard::touchBegan, this);
	listener->setSwallowTouches(true);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void BoxTopUpCard::recharge() {
	std::string pSerial = mEdt_Serial->getText();
	std::string pNumber = mEdt_CardNumber->getText();
	std::string pUsername = mEdt_Username->getText();
	mEdt_CardNumber->setText("");
	mEdt_Serial->setText("");

	if (mFunctionRechargeByCard) {
		mFunctionRechargeByCard(pSerial, pNumber, mTelcoCode, pUsername);
	}
}

void BoxTopUpCard::chooseTelco(int pIndex) {
	for (int i = 0; i < 3; i++) {
		mCardEnable[i]->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName("card_disable.png"));
	}
	mCardEnable[pIndex]->setSpriteFrame(SpriteFrameCache::getInstance()->spriteFrameByName("card_enable.png"));

	switch (pIndex) {
	case 0:
		mTelcoCode = "2";
		break;
	case 1:
		mTelcoCode = "3";
		break;
	case 2:
		mTelcoCode = "1";
		break;
	}
}

void BoxTopUpCard::setVisible(bool isVisible) {
	mEdt_CardNumber->setText("");
	mEdt_Serial->setText("");
	mEdt_Username->setText(GameInfo::mUserInfo.mUsername.c_str());
	mEdt_Username->setVisible(true);
	LayerColor::setVisible(isVisible);
}

void BoxTopUpCard::close() {
	this->setVisible(false);
}