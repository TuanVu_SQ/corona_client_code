#include "ObjFriend.h"
#include "GameInfo.h"
#include "WXmlReader.h"
#include "EGSupport.h"

ObjFriend* ObjFriend::create(unsigned int pAvatarID, std::string pTxtName,
	unsigned long pBalance, unsigned int pLevel, int pStatus, std::function<void()> pShowInfoFunc, bool isPlayHard) {
	ObjFriend* pObjFriend = new ObjFriend();
	pObjFriend->initObject(pAvatarID, pTxtName, pBalance, pLevel, pStatus, pShowInfoFunc, isPlayHard);
	pObjFriend->autorelease();

	return pObjFriend;
}

ObjFriend* ObjFriend::create(unsigned int pAvatarID, std::string pTxtName,
	std::function<void()> pAcceptFunc, std::function<void()> pDenyFunc, std::function<void()> pShowInfoFunc) {
	ObjFriend* pObjFriend = new ObjFriend();
	pObjFriend->initObject(pAvatarID, pTxtName, pAcceptFunc, pDenyFunc, pShowInfoFunc);
	pObjFriend->autorelease();

	return pObjFriend;
}

void ObjFriend::initObject(unsigned int pAvatarID, std::string pTxtName,
	unsigned long pBalance, unsigned int pLevel, int pStatus, std::function<void()> pShowInfoFunc, bool isPlayHard) {

	WXmlReader* pXml = WXmlReader::create();
	pXml->load(KEY_XML_FILE);
	mUserName = pTxtName;
	EGButtonSprite::initWithSpriteFrameName(IMG_FRIENDBG);

	Sprite* pAvatar = Sprite::createWithSpriteFrameName(__String::createWithFormat("avatar (%d).png", pAvatarID)->getCString());
	pAvatar->setPosition(Vec2(pAvatar->getContentSize().width / 2 + 20, this->getContentSize().height / 2));
	this->addChild(pAvatar);

	LabelTTF* pLblName = LabelTTF::create(pTxtName, FONT, 20);
	pLblName->setPosition(Vec2(pAvatar->getPositionX() + pAvatar->getContentSize().width / 2
		+ pLblName->getContentSize().width / 2 + 10, this->getContentSize().height / 2));
	this->addChild(pLblName);

	EGSprite* pImgCoin = EGSprite::createWithSpriteFrameName(IMG_COIN);
	pImgCoin->setTag(100);
	pImgCoin->setPosition(Vec2(220, this->getContentSize().height / 2));
	this->addChild(pImgCoin);

	LabelTTF* pLblBalance = LabelTTF::create(EGSupport::addDotMoney(__String::createWithFormat("%u", pBalance)->getCString()), FONT, 20);
	pLblBalance->setPosition(Vec2(pImgCoin->getPositionX() + pImgCoin->getContentSize().width / 2 + 10
		+ pLblBalance->getContentSize().width / 2, this->getContentSize().height / 2));
	this->addChild(pLblBalance);
	pLblBalance->setTag(101);

	if (isPlayHard){
		uint32_t _nTimeRemain = pBalance;
		uint8_t day = _nTimeRemain / (60 * 60 * 24);
		if (day == 0){
			uint8_t hour = _nTimeRemain / (60 * 60);
			uint8_t minute = (_nTimeRemain - hour * 60 * 60) / 60;
			uint8_t sec = (_nTimeRemain - hour * 60 * 60) % 60;
			std::string text = __String::createWithFormat("%s:%s:%s", itos(hour).c_str(), itos(minute).c_str(), itos(sec).c_str())->getCString();
			pLblBalance->setString(text);
		}
		else{
			uint8_t hour = (_nTimeRemain - (60 * 60 * 24 * day)) / (60 * 60);
			uint8_t minute = (_nTimeRemain - (60 * 60 * 24 * day) - hour * 60 * 60) / 60;
			uint8_t sec = (_nTimeRemain - (60 * 60 * 24 * day) - hour * 60 * 60) % 60;
			std::string text = __String::createWithFormat("%s %s, %s:%s:%s", itos(day).c_str(), pXml->getNodeTextByTagName("day").c_str(), itos(hour).c_str(), itos(minute).c_str(), itos(sec).c_str())->getCString();
			pLblBalance->setString(text);
		}
	}

	std::string pStrLevel = pXml->getNodeTextByTagName(TEXT_LEVEL);
	pStrLevel += " ";
	pStrLevel += __String::createWithFormat("%d", pLevel)->getCString();
	LabelTTF* pLblLevel = LabelTTF::create(pStrLevel.c_str(), FONT, 20);
	pLblLevel->setPosition(Vec2(440, this->getContentSize().height / 2));
	this->addChild(pLblLevel);

	Sprite* pSprStatus;
	if (pStatus == 0) {
		pSprStatus = Sprite::createWithSpriteFrameName(IMG_STATUS_OFFLINE);
	}
	else {
		pSprStatus = Sprite::createWithSpriteFrameName(IMG_STATUS_ONLINE);
	}
	pSprStatus->setPosition(Vec2(550, this->getContentSize().height / 2));
	this->addChild(pSprStatus);

	EGButtonSprite* pBtnDetails = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_DETAILS, EGButton2FrameDark);
	pBtnDetails->setOnTouch(pShowInfoFunc);
	pBtnDetails->setPosition(Vec2(this->getContentSize().width - pBtnDetails->getContentSize().width / 2, this->getContentSize().height / 2));
	pBtnDetails->setVisible(false);
	this->addChild(pBtnDetails);
}

void ObjFriend::initObject(unsigned int pAvatarID, std::string pTxtName,
	std::function<void()> pAcceptFunc, std::function<void()> pDenyFunc, std::function<void()> pShowInfoFunc) {
	WXmlReader* pXml = WXmlReader::create();
	pXml->load(KEY_XML_FILE);
	mUserName = pTxtName;
	EGButtonSprite::initWithSpriteFrameName(IMG_FRIENDBG);

	Sprite* pAvatar = Sprite::createWithSpriteFrameName(__String::createWithFormat("avatar (%d).png", pAvatarID)->getCString());
	pAvatar->setPosition(Vec2(pAvatar->getContentSize().width / 2 + 20, this->getContentSize().height / 2));
	this->addChild(pAvatar);

	LabelTTF* pLblName = LabelTTF::create(pTxtName, FONT, 20);
	pLblName->setPosition(Vec2(pAvatar->getPositionX() + pAvatar->getContentSize().width / 2
		+ pLblName->getContentSize().width / 2 + 10, this->getContentSize().height / 2));
	this->addChild(pLblName);

	EGButtonSprite* pBtnDetails = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_DETAILS, EGButton2FrameDark);
	pBtnDetails->setOnTouch(pShowInfoFunc);
	pBtnDetails->setPosition(Vec2(this->getContentSize().width - pBtnDetails->getContentSize().width / 2, this->getContentSize().height / 2));
	this->addChild(pBtnDetails);

	EGButtonSprite* pBtnDeny = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_REJECT, EGButton2FrameDark);
	pBtnDeny->setOnTouch(pDenyFunc);
	pBtnDeny->setPosition(Vec2(pBtnDetails->getPositionX1() - pBtnDeny->getContentSize().width / 2 - 10, pBtnDetails->getPositionY()));
	this->addChild(pBtnDeny);

	EGButtonSprite* pBtnAccept = EGButtonSprite::createWithSpriteFrameName(IMG_BUTTON_ACCEPT, EGButton2FrameDark);
	pBtnAccept->setOnTouch(pAcceptFunc);
	pBtnAccept->setPosition(Vec2(pBtnDeny->getPositionX1() - pBtnAccept->getContentSize().width / 2 - 10, pBtnDetails->getPositionY()));
	this->addChild(pBtnAccept);
}

void ObjFriend::setTypeTop(int pTypeTop) {
	if (pTypeTop == 3) {
		EGButtonSprite *button = ((EGButtonSprite*)this->getChildByTag(101));
		if (!button){
			button = EGButtonSprite::createWithSpriteFrameName("button-invite.png");
			this->addChild(button);
			button->setTag(101);
			button->setButtonType(EGButton2FrameDark);
		}
		button->setPosition(Vec2(this->getContentSize().width - 50 - button->getContentSize().width / 2, this->getContentSize().height / 2));
	}
	else if (pTypeTop == 2) {
		//((Sprite*)this->getChildByTag(100))->setTexture("icon-fish (18).png");
		((Sprite*)this->getChildByTag(100))->setVisible(false);
		//std::string value = ((LabelTTF*)this->getChildByTag(101))->getString();
	}
	else {
		((EGSprite*)this->getChildByTag(100))->setTexture(IMG_COIN);
	}
}
void ObjFriend::visibleButtonInvite(bool pBool){
	EGButtonSprite *button = ((EGButtonSprite*)this->getChildByTag(101));
	if (button){
		button->setVisible(false);
	}
}

std::string ObjFriend::itos(int n)
{
	const int max_size = std::numeric_limits<int>::digits10 + 1 /*sign*/ + 1 /*0-terminator*/;
	char buffer[max_size] = { 0 };
	sprintf(buffer, "%02d", n);
	return std::string(buffer);
}
