#include "BoxAds.h"
#include "GameInfo.h"
#include "libs/EGJniHelper.h"
#include "libs/EGSupport.h"


BoxAds* BoxAds::create() {
	BoxAds* pBoxAds = new BoxAds();
	pBoxAds->initObject();
	pBoxAds->autorelease();

	return pBoxAds;
}

void BoxAds::initObject() {

	LayerColor* pLayer = LayerColor::create(Color4B(0, 0, 0, 200));
	pLayer->setPosition(Vec2(0, 0));
	this->addChild(pLayer, -1);

	_sprBanner = EGButtonSprite::createWithFile("banner_ads.jpg");
	this->addChild(_sprBanner);
	_sprBanner->setPosition(Vec2(CLIENT_WIDTH / 2, 1000));

	auto btnClose = EGButtonSprite::createWithFile("btn_close.png");
	_sprBanner->addChild(btnClose);
	btnClose->setPosition(Vec2(_sprBanner->getContentSize().width - 5, _sprBanner->getContentSize().height - 5));
	btnClose->setOnTouch([=]{
		this->setVisible(false);
		this->setPosition(Vec2(0, 1000));
	});

	this->setVisible(false);
	this->registerTouch();

	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(BoxAds::touchBegan, this);
	listener->setSwallowTouches(true);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void BoxAds::showBoxAds(){
	this->setVisible(true);
	_sprBanner->runAction(EaseElasticInOut::create(MoveTo::create(1, Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2))));
}