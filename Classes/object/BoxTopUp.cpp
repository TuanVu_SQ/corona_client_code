#include "BoxTopUp.h"
#include "GameInfo.h"
#include "WXmlReader.h"
#include "EGSupport.h"
#include "EGJniHelper.h"

struct {
	bool operator() (RechargeInfo i, RechargeInfo j) {
		return i.money < j.money;
	}
} _sortSmsInfo;

BoxTopUp* BoxTopUp::create(std::function<void()> pFunctionCallCard, std::function<void(int)> pFunctionRecharge) {
	BoxTopUp* pBoxTopUp = new BoxTopUp();
	pBoxTopUp->mFunctionRecharge = pFunctionRecharge;
	pBoxTopUp->initObject(pFunctionCallCard);
	pBoxTopUp->autorelease();

	return pBoxTopUp;
}

void BoxTopUp::initObject(std::function<void()> pFunctionCallCard) {
	isLoaded = false;
	_currentCardType = 0;
	_currentTab = 5;
	initWithColor(Color4B(0, 0, 0, 200));

	pXml = WXmlReader::create();
	pXml->load(KEY_XML_FILE);

	mBox = Sprite::create("box_purchase.png");
	mBox->setPosition(Vec2(CLIENT_WIDTH / 2, CLIENT_HEIGHT / 2));
	this->addChild(mBox);

	boxConfirmSms = BoxConfirmSms::create();
	boxConfirmSms->setPosition(Vec2(1300, 240));
	boxConfirmSms->setVisible(false);
	this->addChild(boxConfirmSms, 10);

	/*mp_sprTab1 = EGSprite::createWithSpriteFrameName("box_purchase_rect.png");
	mp_sprTab1->setPosition(Vec2(mBox->getContentSize().width / 2, mBox->getContentSize().height / 2));
	mBox->addChild(mp_sprTab1);

	mp_sprTab2 = EGSprite::createWithSpriteFrameName("box_purchase_rect2.png");
	mp_sprTab2->setPosition(Vec2(mBox->getContentSize().width / 2, mBox->getContentSize().height / 2));
	mp_sprTab2->setVisible(false);
	mBox->addChild(mp_sprTab2);

	mp_btnsprTabRechargeVN = EGButtonSprite::createWithSpriteFrameName("nap_the1.png");
	mp_btnsprTabRechargeVN->setPosition(Vec2(144, 267));
	mp_btnsprTabRechargeVN->setOnTouch([=] {
		mp_sprTab1->setVisible(true);
		mp_sprTab2->setVisible(false);
		mp_btnsprTabRechargeVN->setTexture("nap_the1.png");
		mp_btnsprTabRechargeVISA->setTexture("nap_visa2.png");
	});
	mBox->addChild(mp_btnsprTabRechargeVN);

	mp_btnsprTabRechargeVISA = EGButtonSprite::createWithSpriteFrameName("nap_visa2.png");
	mp_btnsprTabRechargeVISA->setPosition(Vec2(144, 120));
	mp_btnsprTabRechargeVISA->setOnTouch([=] {
		mp_sprTab1->setVisible(false);
		mp_sprTab2->setVisible(true);
		mp_btnsprTabRechargeVN->setTexture("nap_the2.png");
		mp_btnsprTabRechargeVISA->setTexture("nap_visa1.png");
	});
	mBox->addChild(mp_btnsprTabRechargeVISA);

	mBtnRecharge = new EGButtonSprite*[2];
	mBtnRechargeSend = new EGButtonSprite*[2];
	mp_sprSMSBonus = new EGSprite*[2];
	mp_lblSMSBonus = new Label*[2];
	float pPositionX = 0;
	for (int i = 0; i < 2; i++) {
		mBtnRecharge[i] = EGButtonSprite::createWithSpriteFrameName("rect_purchase_info.png", EGButton2FrameDark);
		mBtnRecharge[i]->setPosition(Vec2(250 + mBtnRecharge[i]->getContentSize().width / 2,
			mBox->getContentSize().height - (i + 1)*mBox->getContentSize().height / 4 - 50));
		mp_sprTab1->addChild(mBtnRecharge[i]);

		mBtnRechargeSend[i] = EGButtonSprite::createWithSpriteFrameName("btnSend_purchase.png", EGButton2FrameDark);
		mBtnRechargeSend[i]->setPosition(Vec2(mBtnRecharge[i]->getPositionX2() +
			mBtnRechargeSend[i]->getContentSize().width / 2 + 5, mBtnRecharge[i]->getPositionY()));
		pPositionX = mBtnRechargeSend[i]->getPositionX();
		mp_sprTab1->addChild(mBtnRechargeSend[i]);

		mp_sprSMSBonus[i] = EGSprite::createWithSpriteFrameName("sms_bonus.png");
		mp_sprSMSBonus[i]->setVisible(false);
		mp_sprSMSBonus[i]->setPosition(mBtnRecharge[i]->getPosition());
		mp_sprTab1->addChild(mp_sprSMSBonus[i]);

		mp_lblSMSBonus[i] = Label::createWithBMFont("font_visa.fnt", "100%", TextHAlignment::CENTER);
		mp_lblSMSBonus[i]->setVisible(false);
		mp_lblSMSBonus[i]->setColor(Color3B(255, 255, 0));
		mp_lblSMSBonus[i]->setRotation(45);
		mp_lblSMSBonus[i]->setPosition(Vec2(321, 67));
		mp_sprSMSBonus[i]->addChild(mp_lblSMSBonus[i]);
	}

	mBtnCallBoxRechargeByCard = EGButtonSprite::createWithSpriteFrameName("btncard.png", EGButton2FrameDark);
	mBtnCallBoxRechargeByCard->setOnTouch(pFunctionCallCard);
	mBtnCallBoxRechargeByCard->setPosition(Vec2(pPositionX + 5,
		mBtnCallBoxRechargeByCard->getContentSize().height / 2 + 65));
	mp_sprTab1->addChild(mBtnCallBoxRechargeByCard);

	Sprite* pImgInfo = Sprite::createWithSpriteFrameName("cardinfo.png");
	pImgInfo->setPosition(Vec2(mBtnCallBoxRechargeByCard->getPositionX1() - pImgInfo->getContentSize().width / 2 - 10, mBtnCallBoxRechargeByCard->getPositionY()));
	mp_sprTab1->addChild(pImgInfo);*/

	_nodeCard = Node::create();
	_nodeCard->setPosition(CLIENT_WIDTH, 50);
	mBox->addChild(_nodeCard);

	auto fishSexy = EGSprite::createWithFile("img_sexy_fish.png");
	fishSexy->setPosition(Vec2(fishSexy->getContentSize().width / 2 + 10, fishSexy->getContentSize().height / 2 - 30));
	_nodeCard->addChild(fishSexy);

	_edtCardNumber = cocos2d::ui::EditBox::create(Size(257, 42), Scale9Sprite::create("editbox_small.png"));
	_edtCardNumber->setInputFlag(cocos2d::ui::EditBox::InputFlag::SENSITIVE);
	_edtCardNumber->setInputMode(EditBox::InputMode::NUMERIC);
	_edtCardNumber->setTag(2);
	_edtCardNumber->setPosition(Vec2(CLIENT_WIDTH / 2 + _edtCardNumber->getContentSize().width / 2 + 100, 98));
	_edtCardNumber->setFont("fonts/arial.ttf", 18);
	_edtCardNumber->setMaxLength(15);
	_edtCardNumber->setPlaceholderFont("fonts/arial.ttf", 18);
	_edtCardNumber->setFontColor(Color3B::WHITE);
	_edtCardNumber->setPlaceHolder(pXml->getNodeTextByTagName("txt_charge_edt_card_number").c_str());
	_edtCardNumber->setPlaceholderFontColor(Color3B(135, 206, 235));
	_nodeCard->addChild(_edtCardNumber);

	_edtSerialNumber = EditBox::create(Size(257, 42), Scale9Sprite::create("editbox_small.png"));
	_edtSerialNumber->setInputFlag(EditBox::InputFlag::SENSITIVE);
	_edtSerialNumber->setInputMode(EditBox::InputMode::SINGLE_LINE);
	_edtSerialNumber->setTag(1);
	_edtSerialNumber->setPosition(Vec2(_edtCardNumber->getPositionX(), _edtCardNumber->getPositionY() + _edtSerialNumber->getContentSize().height + 10));
	_edtSerialNumber->setFont("fonts/arial.ttf", 18);
	_edtSerialNumber->setMaxLength(15);
	_edtSerialNumber->setPlaceholderFont("fonts/arial.ttf", 18);
	_edtSerialNumber->setFontColor(Color3B::WHITE);
	_edtSerialNumber->setPlaceHolder(pXml->getNodeTextByTagName("txt_charge_edt_serial_number").c_str());
	_edtSerialNumber->setPlaceholderFontColor(Color3B(135, 206, 235));
	_nodeCard->addChild(_edtSerialNumber);

	for (uint8_t i = 1; i < 4; ++i){
		std::string fileName = __String::createWithFormat("img_logo_card%d.png", i)->getCString();
		auto logoImg = EGButtonSprite::createWithFile(fileName.c_str());
		logoImg->setPosition(Vec2(210 + (float)(CLIENT_WIDTH / 8.35f)*(i + (i - 1)), CLIENT_HEIGHT - 250));
		logoImg->setOnTouch(CC_CALLBACK_0(BoxTopUp::cardClicked, this, i));
		_nodeCard->addChild(logoImg);

		auto cardBg = EGSprite::createWithFile("img_logo_cardbg.png");
		cardBg->setPosition(logoImg->getPosition());
		_nodeCard->addChild(cardBg);
		cardBg->setTag(i);
		i == 1 ? cardBg->setVisible(true) : cardBg->setVisible(false);
		listCardBg.push_back(cardBg);
	}

	cardComboBg = EGSprite::createWithFile("img_charge_bg_combo_card.png");
	cardComboBg->setAnchorPoint(Vec2(0, 0.5f));
	cardComboBg->setPosition(Vec2(listCardBg[0]->getPositionX1(), 80));
	_nodeCard->addChild(cardComboBg);

	auto lbComboBgTitle = Label::createWithBMFont("font.fnt", pXml->getNodeTextByTagName("txt_charge_combo_box_title"));
	lbComboBgTitle->setPosition(Vec2(cardComboBg->getPosition()) + Vec2(cardComboBg->getContentSize().width/2, cardComboBg->getContentSize().height / 2 - 20));
	lbComboBgTitle->setScale(0.4f);
	lbComboBgTitle->setColor(Color3B::GREEN);
	_nodeCard->addChild(lbComboBgTitle);

	if (mLstRate.size() > 0)
		cardClicked(1);

	_btnReCharge = EGButtonSprite::createWithFile("img_btn_recharging.png");
	_btnReCharge->setButtonType(EGButtonType::EGButton2FrameDark);
	_btnReCharge->setPosition(Vec2(_edtCardNumber->getPositionX(), cardComboBg->getPositionY() - cardComboBg->getContentSize().height/2 + _btnReCharge->getContentSize().height / 2));
	_nodeCard->addChild(_btnReCharge);
	_btnReCharge->setOnTouch(CC_CALLBACK_0(BoxTopUp::btnReChargeClicked, this));

	_nodeNotCard = Node::create();
	_nodeNotCard->setPosition(0, 50);
	mBox->addChild(_nodeNotCard);

	mTableView = TableView::create(this, Size(740, 250));
	mTableView->setPosition(Vec2(30, 10));
	mTableView->setDirection(ScrollView::Direction::HORIZONTAL);
	mTableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	mTableView->setDelegate(this);
	_nodeNotCard->addChild(mTableView);
	//mTableView->reloadData();

	_btnSms = EGButtonSprite::createWithFile("kv_banthuong_off_button.png");
	_btnSms->setPosition(Vec2(CLIENT_WIDTH/2 - _btnSms->getContentSize().width - 50, mBox->getContentSize().height  - 110));
	_btnSms->setOnTouch(CC_CALLBACK_0(BoxTopUp::btnSmsClicked, this));
	mBox->addChild(_btnSms);

	auto lb1 = Label::createWithBMFont(FONT_BMF_NAME, "SMS");
	lb1->setScale(0.8f);
	lb1->setPosition(_btnSms->getContentSize() / 2);
	_btnSms->addChild(lb1);

	_btnCard = EGButtonSprite::createWithFile("kv_banthuong_on_button.png");
	_btnCard->setPosition(Vec2(CLIENT_WIDTH / 2, _btnSms->getPositionY()));
	_btnCard->setOnTouch(CC_CALLBACK_0(BoxTopUp::btnCardClicked, this));
	mBox->addChild(_btnCard);

	auto lb2 = Label::createWithBMFont(FONT_BMF_NAME, "USSD");
	lb2->setScale(0.8f);
	lb2->setPosition(_btnCard->getContentSize() / 2);
	_btnCard->addChild(lb2);
	
	_btnVisa = EGButtonSprite::createWithFile("kv_banthuong_off_button.png");
	_btnVisa->setPosition(Vec2(CLIENT_WIDTH / 2 + _btnSms->getContentSize().width + 50 , _btnSms->getPositionY()));
	_btnVisa->setOnTouch(CC_CALLBACK_0(BoxTopUp::btnVisaClicked, this));
	mBox->addChild(_btnVisa);

	auto lb3 = Label::createWithBMFont(FONT_BMF_NAME, "ECHANJ");
	lb3->setScale(0.8f);
	lb3->setPosition(_btnVisa->getContentSize() / 2);
	_btnVisa->addChild(lb3);

	mBtnClose = EGButtonSprite::createWithSpriteFrameName("image_x.png", EGButton2FrameDark);
	mBtnClose->setOnTouch(CC_CALLBACK_0(BoxTopUp::close, this));
	mBtnClose->setPosition(Vec2(mBox->getContentSize().width - mBtnClose->getContentSize().width / 2, mBox->getContentSize().height - mBtnClose->getContentSize().height / 2));
	mBox->addChild(mBtnClose);

	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(BoxTopUp::touchBegan, this);
	listener->setSwallowTouches(true);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

}

void BoxTopUp::setInfo(std::vector<RechargeInfo> pLstRecharge, bool isEnableRecharge) {
	/*if (!isLoaded) {
		isLoaded = true;

		WXmlReader* pXml = WXmlReader::create();
		pXml->load(KEY_XML_FILE);

		for (int i = 0; i < pLstRecharge.size(); i++) {
			RechargeInfo rechargeInfo = pLstRecharge.at(i);
			std::string pNoticeText = "";
			pNoticeText += pXml->getNodeTextByTagName("recharge_syntax1");
			pNoticeText += (" " + rechargeInfo.syntax + " " + GROUP_ID + " ");
			pNoticeText += (GameInfo::mUserInfo.mUsername + " ") + "\n";

			pNoticeText += (pXml->getNodeTextByTagName("recharge_syntax3") + " ");
			pNoticeText += EGSupport::addDotMoney(rechargeInfo.balance);

			pNoticeText += (" " + pXml->getNodeTextByTagName("recharge_syntax4") + " ");
			pNoticeText += EGSupport::addDotMoney(rechargeInfo.money) + " " + pXml->getNodeTextByTagName("recharge_syntax5");

			Label* lblNotice = Label::createWithBMFont("font_black.fnt", pNoticeText.c_str(), TextHAlignment::CENTER, mBtnRecharge[i]->getContentSize().width - 20);
			lblNotice->setPosition(Vec2(mBtnRecharge[i]->getContentSize().width / 2, mBtnRecharge[i]->getContentSize().height / 2));
			mBtnRecharge[i]->addChild(lblNotice);

			std::string pNoticeText2 = "";
			pNoticeText2 += pXml->getNodeTextByTagName("recharge_syntax2") + "\n" + rechargeInfo.serviceNumber;
			Label* lblNotice2 = Label::createWithBMFont("font_black.fnt", pNoticeText2, TextHAlignment::CENTER);
			lblNotice2->setScale(1.3f);
			lblNotice2->setPosition(Vec2(mBtnRechargeSend[i]->getContentSize().width / 2, mBtnRechargeSend[i]->getContentSize().height / 2));
			mBtnRechargeSend[i]->addChild(lblNotice2);

			if (rechargeInfo.percent > 0) {
				mp_sprSMSBonus[i]->setVisible(true);
				mp_lblSMSBonus[i]->setVisible(true);
				mp_lblSMSBonus[i]->setString(__String::createWithFormat("%d%", rechargeInfo.percent)->getCString());
			}
			else {
				mp_sprSMSBonus[i]->setVisible(false);
				mp_lblSMSBonus[i]->setVisible(false);
			}

			if (isEnableRecharge) {
				mBtnRecharge[i]->setOnTouch(CC_CALLBACK_0(BoxTopUp::recharge, this, rechargeInfo.syntax + " " + GROUP_ID + " " + GameInfo::mUserInfo.mUsername, rechargeInfo.serviceNumber));
				mBtnRechargeSend[i]->setOnTouch(CC_CALLBACK_0(BoxTopUp::recharge, this, rechargeInfo.syntax + " " + GROUP_ID + " " + GameInfo::mUserInfo.mUsername, rechargeInfo.serviceNumber));
			}
		}
	}
    
    if (!isEnableRecharge) {
        mBtnCallBoxRechargeByCard->setVisible(false);
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        mp_btnsprTabRechargeVISA->performClick();
        mp_btnsprTabRechargeVN->setVisible(false);
#endif
    } else {
        mp_btnsprTabRechargeVN->performClick();
    }*/
}

void BoxTopUp::setInfoVISA(std::vector<RechargeVISAInfo> pLstRechageVISA) {
	/*mLstRechargeVISA = pLstRechageVISA;
	mTableView->reloadData();*/
}

void BoxTopUp::close() {
	_edtCardNumber->setText("");
	_edtSerialNumber->setText("");
	this->setVisible(false);
	this->setPosition(CLIENT_WIDTH, 0);
}

void BoxTopUp::setVisible(bool isVisible) {
	
    LayerColor::setVisible(isVisible);
	if (isVisible)
		this->setPosition(0, 0);
	else
		this->setPosition(CLIENT_WIDTH, 0);
}

void BoxTopUp::recharge(std::string pSyntax, std::string pServiceNumber) { // i == 0 -> 10k ; i == 1 -> 15k
	EGJniHelper::sendSMS(pSyntax.c_str(), pServiceNumber.c_str());
	mFunctionRecharge(0);
}

Size BoxTopUp::tableCellSizeForIndex(TableView *table, ssize_t idx) {
	return Size(150, 250);
}

TableViewCell* BoxTopUp::tableCellAtIndex(TableView *table, ssize_t idx) {
	TableViewCell* tableViewCell = table->dequeueCell();
	if (tableViewCell) {
		tableViewCell->removeAllChildrenWithCleanup(true);
		tableViewCell->removeFromParentAndCleanup(true);
	}
	tableViewCell = new TableViewCell();

	auto sprite = ChargeItem::create();
	//sprite->setScale(0.93f);
	sprite->setPosition(Vec2(70, 115));
	sprite->setRotation(0);
	sprite->setTag(1);
	if (_currentTab == 1){ // sms
		sprite->setSmsInfo(idx, true, _listSmsBuffer[idx]);
		sprite->setFuncShowSms(CC_CALLBACK_1(BoxTopUp::showBoxConfirmSms, this));
	}
	else if (_currentTab == 2){ // subs
		sprite->setSubInfo(idx, true, _listSubInfo[idx]);
		sprite->setFuncShowSub(CC_CALLBACK_1(BoxTopUp::showBoxConfirmSub, this));
	}
	else if (_currentTab == 3)//exchange
	{
		sprite->setExchangeInfo(idx, true, mLstExchangeInfo[idx]);
		sprite->setFuncShowExchange(CC_CALLBACK_1(BoxTopUp::showBoxConfirmExchange, this));
		//sprite->setVisaInfo(idx, true, mLstRechargeVISA[idx]);
	}
		
	tableViewCell->addChild(sprite);
	
	return tableViewCell;
}

void BoxTopUp::purchaseItem(std::string pItemID) {
	if (isVisible() && mp_sprTab2->isVisible())
		EGJniHelper::purchaseItem(pItemID.c_str(), GameInfo::mUserInfo.mUsername);
}

ssize_t BoxTopUp::numberOfCellsInTableView(TableView *table) {
	if (_currentTab == 3)
		return mLstExchangeInfo.size();
	else if (_currentTab == 1)
		return _listSmsBuffer.size();
	return _listSubInfo.size();
}

void BoxTopUp::btnSmsClicked()
{
	_tab = 1;
	//if (_currentTab == 1) return;
	_currentTab = 1;

	//if (_listTabTelco.size() > 0){
	//	for (uint8_t i = 0; i < _listTabTelco.size(); ++i)
	//		_listTabTelco[i]->setVisible(true);
	//	/*_lbTelcoNoteUp->setVisible(true);
	//	_lbTelcoNoteDown->setVisible(true);*/
	//}

	_btnSms->setTexture("kv_banthuong_on_button.png");
	_btnCard->setTexture("kv_banthuong_off_button.png");
	_btnVisa->setTexture("kv_banthuong_off_button.png");

	_nodeNotCard->setPosition(Vec2(0, 50));
	_nodeCard->setPosition(Vec2(CLIENT_WIDTH, 0));
	
	if (_listSmsBuffer.size() > 0)
	{
		if (_listTabTelco.size() > 0)
			_listTabTelco[0]->setVisible(true);
		mTableView->reloadData();
	}
	else{
		if (_funcGetSmsInfo)
			_funcGetSmsInfo();
	}
	

	//if (_listSmsBuffer.size() > 0)
	//	mTableView->reloadData();
	//if (_listSmsInfo.size() > 0)
	//	createTabsSmsInfo();
	//else{
	//	//request get list sms
	//	if (_funcGetSmsInfo)
	//		_funcGetSmsInfo();
	//}
}

void BoxTopUp::btnCardClicked()
{
	_tab = 2;
	if (_currentTab == 2) return;
	_currentTab = 2;
	_btnSms->setTexture("kv_banthuong_off_button.png");
	_btnCard->setTexture("kv_banthuong_on_button.png");
	_btnVisa->setTexture("kv_banthuong_off_button.png");

	/*_nodeCard->setPosition(Vec2(0, 50));
	_nodeNotCard->setPosition(Vec2(CLIENT_WIDTH, 0));*/
	if (_listSubInfo.size() > 0){
		if (_listTabTelco.size() > 0){
			for (uint8_t i = 0; i < _listTabTelco.size(); ++i)
				_listTabTelco[i]->setVisible(false);
			_lbTelcoNoteUp->setVisible(false);
			_lbTelcoNoteDown->setVisible(false);
		}
		mTableView->reloadData();
	}
	else{
		if (_funcGetSmsInfo)
			_funcGetSmsInfo();
	}
	
	/*if (_funcGetSubInfo)
		_funcGetSubInfo();*/
	_nodeNotCard->setPosition(Vec2(0, 50));
	_nodeCard->setPosition(Vec2(CLIENT_WIDTH, 0));
}

void BoxTopUp::btnVisaClicked()
{
	_tab = 3;
	if (_currentTab == 3) return;
	_currentTab = 3;

	if (_listTabTelco.size() > 0){
		for (uint8_t i = 0; i < _listTabTelco.size(); ++i)
			_listTabTelco[i]->setVisible(false);
		_lbTelcoNoteUp->setVisible(false);
		_lbTelcoNoteDown->setVisible(false);
	}

	_btnSms->setTexture("kv_banthuong_off_button.png");
	_btnCard->setTexture("kv_banthuong_off_button.png");
	_btnVisa->setTexture("kv_banthuong_on_button.png");

	/*_nodeNotCard->setPosition(Vec2(0, 50));
	_nodeCard->setPosition(Vec2(CLIENT_WIDTH, 0));*/

	//mTableView->reloadData();

	if (mLstExchangeInfo.size() > 0)
		mTableView->reloadData();
	else{
		//request get list iap
		if (_funcGetSmsInfo)
			_funcGetSmsInfo();
	}
}

void BoxTopUp::btnReChargeClicked()
{
	std::string cardNumber = _edtCardNumber->getText();
	std::string serial = _edtSerialNumber->getText();

	if (serial.length() < 8){
		if (_funcShowNotice)
			_funcShowNotice("txt_charge_serial_number_invalid");
		return;
	}
	if (cardNumber.length() < 8){
		if (_funcShowNotice)
			_funcShowNotice("txt_charge_card_number_invalid");
		return;
	}
	if (!isNumber(cardNumber.c_str())){
		if (_funcShowNotice)
			_funcShowNotice("txt_charge_card_number_invalid2");
		return;
	}

	std::string telCo = "";
	if (listCardBg[0]->isVisible())
		telCo = "1";
	else if (listCardBg[1]->isVisible())
		telCo = "2";
	else if (listCardBg[2]->isVisible())
		telCo = "3";

	//send recharge request message
	if (_funcRecharge)
		_funcRecharge(serial, cardNumber, telCo);

	_edtCardNumber->setText("");
	_edtSerialNumber->setText("");
}

void BoxTopUp::cardClicked(uint8_t cardIdx)
{
	_currentCardType = cardIdx;
	for (size_t i = 0; i < listCardBg.size(); ++i)
	{
		if (listCardBg[i]->getTag() == cardIdx)
			listCardBg[i]->setVisible(true);
		else
			listCardBg[i]->setVisible(false);
	}
	this->showRechargePromotion(cardIdx);
}

bool BoxTopUp::isNumber(const char * strNumber)
{
	bool state = true;
	for (int i = 0; i < (int)strlen(strNumber); ++i)
	{
		if (!(strNumber[i] >= '0' && strNumber[i] <= '9'))
		{
			state = false;
			break;
		}
	}
	return state;
}

void BoxTopUp::showRechargePromotion(uint8_t telcoId)
{
	/*cardComboBg->removeAllChildrenWithCleanup(true);
	uint8_t idx = 0;
	for (size_t i = 0; i < mLstRate.size(); ++i)
	{
		if (mLstRate[i].telcoId != telcoId)
			continue;
		++idx;
		auto lbInfo = Label::createWithBMFont("font.fnt", "");
		std::string content = "";
		if (mLstRate[i].promotionPercent > 0)
			content = __String::createWithFormat(pXml->getNodeTextByTagName("txt_recharge_have_promotion").c_str(), EGSupport::convertMoneyAndAddDot(mLstRate[i].value).c_str(), EGSupport::convertMoneyAndAddDot(mLstRate[i].balance).c_str(), __String::createWithFormat("%d", mLstRate[i].promotionPercent)->getCString())->getCString();
		else
			content = __String::createWithFormat(pXml->getNodeTextByTagName("txt_recharge_rate_no_promotion").c_str(), EGSupport::convertMoneyAndAddDot(mLstRate[i].value).c_str(), EGSupport::convertMoneyAndAddDot(mLstRate[i].balance).c_str())->getCString();	
		lbInfo->setString(content);
		lbInfo->setScale(0.35f);
		lbInfo->setColor(Color3B(135, 206, 235));
		lbInfo->setPosition(Vec2(cardComboBg->getContentSize().width/2, cardComboBg->getContentSize().height - 22 - 23 * idx));
		cardComboBg->addChild(lbInfo, 20);
		
	}*/
	/*btnSmsClicked();
	_btnSms->setVisible(false);
	_btnCard->setVisible(false);
	_btnVisa->setVisible(false);*/
}

void BoxTopUp::showNotice(const std::string strNotice)
{
	if (_funcShowNotice)
		_funcShowNotice(strNotice);
}

void BoxTopUp::showLoading(bool bLoading)
{
	if (_funcShowLoading)
		_funcShowLoading(bLoading);
}

void BoxTopUp::reloadData(uint8_t type)
{
	/*if (type == 0)
		btnSmsClicked();
	else if (type == 1)
		btnVisaClicked();*/
	mTableView->reloadData();
}

void BoxTopUp::showBoxConfirmSms(RechargeInfo smsInfo)
{
	boxConfirmSms->Reset();
	boxConfirmSms->showSmsNode();
	boxConfirmSms->showInfo(smsInfo);
}

void BoxTopUp::showBoxConfirmSub(SubcribeInfo_s subInfo)
{
	boxConfirmSms->showSubConfirm(subInfo);
}

void BoxTopUp::showBoxConfirmExchange(ExchangeInfo exchangeInfo)
{
	boxConfirmSms->Reset();
	boxConfirmSms->showSmsNode();
	boxConfirmSms->showExchangeConfirm(exchangeInfo);
}

void BoxTopUp::createTabsSmsInfo()
{
	//_nodeNotCard->getChildByTag(10)->setVisible(false);
	/*if (_listSmsInfoTelco.size() == 0){
		_numberOfCell = 0;
		_tableView->reloadData();
		return;
	}*/
	if (_listTabTelco.size() > 0) return;
	uint8_t idx = 0;
	for (uint8_t i = 0; i < _listSmsInfoTelco.size(); ++i)
	{
		auto tab_bg = EGButtonSprite::createWithFile("icon_tab_bg_notpress_chargescene.png");
		tab_bg->setTag(i);
		tab_bg->setPosition(Vec2((i + 1)*tab_bg->getContentSize().width*0.9f - 60 + 13, 260));
		tab_bg->setUserData((void*)_listSmsInfoTelco[i].telcoName.c_str());
		_nodeNotCard->addChild(tab_bg);
		tab_bg->setOnTouch(CC_CALLBACK_0(BoxTopUp::showSmsView, this, i, _listSmsInfoTelco[i].telcoName));
		_listTabTelco.push_back(tab_bg);

		auto icon_tab_bg = EGSprite::createWithFile("icon_not_press_chargescene.png");
		icon_tab_bg->setPosition(Vec2(icon_tab_bg->getContentSize().width, tab_bg->getContentSize().height / 2));
		icon_tab_bg->setTag(1);
		tab_bg->addChild(icon_tab_bg);

		auto lb_telco = Label::createWithBMFont("font_white.fnt", _listSmsInfoTelco[i].telcoName);
		lb_telco->setTag(2);
		lb_telco->setScale(0.9f);
		lb_telco->setPosition(Vec2(tab_bg->getContentSize() / 2) + Vec2(10, 0));
		tab_bg->addChild(lb_telco);

		tab_bg->setScale(0.85f);
		std::string telco = EGJniHelper::getTelco();
		for (uint32_t i = 0; i < telco.size(); ++i){
			telco[i] = std::tolower(telco[i]);
		}
		if (telco != "viettel" && telco != "vinaphone" && telco != "mobifone" && telco != "natcom")
			telco = "other";
		/*if (!_listSmsInfoTelco[i].telcoName.compare(telco))
			idx = i;*/
		idx = 0;
	}

	_lbTelcoNoteUp = Label::createWithBMFont("font-arial-shop.fnt", "Note");
	_lbTelcoNoteUp->setColor(Color3B::WHITE);
	_lbTelcoNoteUp->setScale(0.75f);
	_lbTelcoNoteUp->setPosition(Vec2(800 / 2, 14));
	_nodeNotCard->addChild(_lbTelcoNoteUp);
	_lbTelcoNoteUp->setVisible(false);

	_lbTelcoNoteDown = Label::createWithBMFont("font-arial-shop.fnt", "Note down");
	_lbTelcoNoteDown->setColor(Color3B::WHITE);
	_lbTelcoNoteDown->setScale(0.75f);
	_lbTelcoNoteDown->setPosition(Vec2(800 / 2, -6));
	_nodeNotCard->addChild(_lbTelcoNoteDown);
	_lbTelcoNoteDown->setVisible(false);

	std::string telco = EGJniHelper::getTelco();
	for (uint32_t i = 0; i < telco.size(); ++i){
		telco[i] = std::tolower(telco[i]);
	}
	showSmsView(idx, telco);

}

void BoxTopUp::showSmsView(uint8_t tag, std::string telco)
{
	//_strTelco = telco;
	telco = "natcom";
	for (uint8_t i = 0; i < _listTabTelco.size(); ++i)
	{
		if (_listTabTelco[i]->getTag() == tag){
			_listTabTelco[i]->setTexture("icon_tab_bg_pressed_chargescene.png");
			((EGSprite*)_listTabTelco[i]->getChildByTag(1))->setTexture("icon_pressed_chargescene.png");
			((Label*)_listTabTelco[i]->getChildByTag(2))->setColor(Color3B::YELLOW);
		}
		else{
			_listTabTelco[i]->setTexture("icon_tab_bg_notpress_chargescene.png");
			((EGSprite*)_listTabTelco[i]->getChildByTag(1))->setTexture("icon_not_press_chargescene.png");
			((Label*)_listTabTelco[i]->getChildByTag(2))->setColor(Color3B::WHITE);
		}
	}

	_listSmsBuffer.clear();
	/*for (uint8_t i = 0; i < _listSmsInfo.size(); ++i)
	{
		if (!_listSmsInfo[i].telco.compare(telco))
			_listSmsBuffer.push_back(_listSmsInfo[i]);
	}*/
	_listSmsBuffer = _listSmsInfo;
	std::sort(_listSmsBuffer.begin(), _listSmsBuffer.end(), _sortSmsInfo);

	/*for (uint8_t i = 0; i < _listSmsInfoTelco.size(); ++i)
	{
		if (!_listSmsInfoTelco[i].telcoName.compare(telco)){
			_lbTelcoNoteUp->setString(__String::createWithFormat(pXml->getNodeTextByTagName("txt_recharge_note_up_1").c_str(), _listSmsInfoTelco[i].telcoName.c_str(), EGSupport::convertMoneyAndAddText(_listSmsInfoTelco[i].maxBalance).c_str())->getCString());
			break;
		}
	}
	_lbTelcoNoteDown->setString(pXml->getNodeTextByTagName("txt_recharge_note_down_1"));*/

	mTableView->reloadData();
}

void BoxTopUp::showSubPackage(){
	/*_listTabTelco[0]->setVisible(false);
	mTableView->reloadData();*/
	btnCardClicked();
}

BoxConfirmSms* BoxTopUp::GetBoxSms(){
	return boxConfirmSms;
}

void BoxTopUp::showBoxLockRecharge()
{
	/*_btnSms->unregisterTouch();
	_btnCard->unregisterTouch();
	_btnSms->setColor(Color3B(_btnSms->getColor().r / 2, _btnSms->getColor().g / 2, _btnSms->getColor().b / 2));
	_btnCard->setColor(Color3B(_btnCard->getColor().r / 2, _btnCard->getColor().g / 2, _btnCard->getColor().b / 2));*/
	_btnSms->setVisible(false);
	_btnCard->setVisible(false);
	btnVisaClicked();
}