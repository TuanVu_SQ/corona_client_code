#ifndef __AVATARINFO_H_INCLUDED__
#define __AVATARINFO_H_INCLUDED__

#include "cocos2d.h"

USING_NS_CC;

class AvatarInfo {
public:
	static std::string getName(int pItemID) {
		switch (pItemID) {
		case 2: return "Mosa"; break;
		case 3: return "Yunno"; break;
		case 4: return "Captain Smoker"; break;
		case 5: return "Sasaki"; break;
		case 6: return "Harry Porter"; break;
		case 7: return "Captain ACE"; break;

		case 8: return "Ada"; break;
		case 9: return "Alice"; break;
		case 10: return "Andy"; break;
		case 11: return "Alesssa"; break;
		case 12: return "Arrow"; break;
		case 13: return "Black Widow"; break;

		case 14: return "Bat Cute"; break;
		case 15: return "Jarven"; break;
		case 16: return "Jack Sparrow"; break;
		case 17: return "Cat Women"; break;
		case 18: return "Pupu"; break;
		case 19: return "Chunli"; break;

		case 20: return "Michonne"; break;
		case 21: return "Daryl Dixon"; break;
		case 22: return "Turtle Ninja"; break;
		case 23: return "Valimir"; break;
		case 24: return "Ghost"; break;
		case 25: return "Sphinx"; break;

		case 26: return "Guan Yu"; break;
		case 27: return "Zhao Yun"; break;
		case 28: return "Centaur"; break;
		case 29: return "Ichigo Hollow"; break;
		case 30: return "Robo"; break;
		case 31: return "RoboAndroid"; break;

		case 32: return "Phoenix Woman"; break;
		case 33: return "Police Zombie"; break;
		case 34: return "Kuro"; break;
		case 35: return "Akali"; break;
		case 36: return "Kuzan \"Girl\""; break;
		case 37: return "Jinchuuriki X"; break;

		case 38: return "Oar"; break;
		case 39: return "Uchiha Madara"; break;
		case 40: return "Magneto"; break;
		case 41: return "Goku"; break;
		case 42: return "Naruto"; break;
		case 43: return "Hokage IV"; break;

		case 44: return "Bruce Lee"; break;
		case 45: return "Butcher"; break;
		case 46: return "Gold Roger"; break;
		case 47: return "Joker"; break;
		case 48: return "Uchiha Sasuke"; break;
		case 49: return "Twister Fate"; break;

		case 50: return "Vegeta"; break;
		}
		return "";
	}
	static float getPrice(int pItemID) {
		/*switch (pItemID) {
		case 0: return 1000;
		case 1: return 500;
		}*/
		return 10000;
	}
};

#endif