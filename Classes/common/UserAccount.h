#ifndef __USERACCOUNT_H_INLCUDED__
#define __USERACCOUNT_H_INCLUDED__

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "string.h"

class UserAccount {
public:
    UserAccount() {
        mUsername = "";
        mFullname = "";
        mLinkAvatar = "";
        mStatus = "";
        mBirthdays = "";
        mEmail = "";
        mRoomInfo = "";
        mRoomID = "";
        mRoomIP = "";
        mRoomPort = "";
        mLevel = 1;
        mGender = 0;
        mBalance = 0;
        mCPID = "";
        mDeviceID = "";
        mData = "";
    }
    virtual ~UserAccount() {
        mUsername = NULL;
        delete mUsername;
        mFullname = NULL;
        delete mFullname;
        mLinkAvatar = NULL;
        delete mLinkAvatar;
        mStatus = NULL;
        delete mStatus;
        mBirthdays = NULL;
        delete mBirthdays;
        mEmail = NULL;
        delete mEmail;
        mRoomInfo = NULL;
        delete mRoomInfo;
        mRoomID = NULL;
        delete mRoomID;
        mRoomIP = NULL;
        delete mRoomIP;
        mRoomPort = NULL;
        delete mRoomPort;
        mLevel = NULL;
        mGender = NULL;
        mBalance = NULL;
        mCPID = NULL;
        delete mCPID;
        mDeviceID = NULL;
        delete mDeviceID;
        mData = NULL;
        delete mData;
    }

    // GET

    const char* getUsername() {
        return mUsername;
    }

    const char* getFullname() {
        return mFullname;
    }

    const char* getLinkAvatar() {
        return mLinkAvatar;
    }

    const char* getStatus() {
        return mStatus;
    }

    const char* getBirthdays() {
        return mBirthdays;
    }

    const char* getEmail() {
        return mEmail;
    }

    const char* getRoomInfo() {
        return mRoomInfo;
    }

    const char* getRoomID() {
        return mRoomID;
    }

    const char* getRoomIP() {
        return mRoomIP;
    }

    const char* getRoomPort() {
        return mRoomPort;
    }

    int getLevel() {
        return mLevel;
    }

    int getGender() {
        return mGender;
    }

    long getBalance() {
        return mBalance;
    }

    const char* getCPID() {
        return mCPID;
    }

    const char* getDeviceID() {
        return mDeviceID;
    }

    const char* getPasswords() {
        return mPasswords;
    }

    // SET

    void setUsername(const char* pUsername) {
        mUsername = pUsername;
    }

    void setFullname(const char* pFullname) {
        mFullname = pFullname;
    }

    void setLinkAvatar(const char* pLinkAvatar) {
        mLinkAvatar = pLinkAvatar;
    }

    void setStatus(const char* pStatus) {
        mStatus = pStatus;
    }

    void setBirthdays(const char* pBirthdays) {
        mBirthdays = pBirthdays;
    }

    void setEmail(const char* pEmail) {
        mEmail = pEmail;
    }

    void setRoomInfo(const char* pRoomInfo) {
        mRoomInfo = pRoomInfo;
    }

    void setRoomID(const char* pRoomID) {
        mRoomID = pRoomID;
    }

    void setRoomIP(const char* pRoomIP) {
        mRoomIP = mRoomIP;
    }

    void setRoomPort(const char* pRoomPort) {
        mRoomPort = pRoomPort;
    }

    void setLevel(int pLevel) {
        mLevel = pLevel;
    }

    void setGender(int pGender) {
        mGender = pGender;
    }

    void setBalance(long pBalance) {
        mBalance = pBalance;
    }

    void setDeviceID(const char* pDeviceID) {
        mDeviceID = pDeviceID;
    }

    void setCPID(const char* pCPID) {
        mCPID = pCPID;
    }

    void setPasswords(const char* pPasswords) {
        mPasswords = pPasswords;
    }

    void initData(const char* pData) {
        mData = pData;
        std::string pStrData = pData;
        int index = 0,startChar = 0;
        for(int i = 0;i<pStrData.length();i++) {
            std::string pStrDataSub = pStrData.substr(i,i + 1);
            if(pStrDataSub == "|") {
                std::string pStrValues = pStrData.substr(startChar,i);
                switch (index) {
                case 0:
                    setUsername(pStrValues.c_str());break;
                case 1:
                    setPasswords(pStrValues.c_str());break;
                case 2:
                    setFullname(pStrValues.c_str());break;
                case 3:
                    setLinkAvatar(pStrValues.c_str());break;
                case 4:
                    setStatus(pStrValues.c_str());break;
                case 5:
                    setBirthdays(pStrValues.c_str());break;
                case 6:
                    setEmail(pStrValues.c_str());break;
                case 7:
                    setRoomID(pStrValues.c_str());break;
                case 8:
                    setRoomInfo(pStrValues.c_str());break;
                case 9:
                    setRoomIP(pStrValues.c_str());break;
                case 10:
                    setRoomPort(pStrValues.c_str());break;
                case 11:
                    setLevel(atoi(pStrValues.c_str()));break;
                case 12:
                    setGender(atoi(pStrValues.c_str()));break;
                case 13:
                    setBalance(atol(pStrValues.c_str()));break;
                case 14:
                    setCPID(pStrValues.c_str());break;
                case 15:
                    setDeviceID(pStrValues.c_str());break;
                default:
                    break;
                }
                index++;
                startChar = i + 1;
            }
        }
        // analyze data here
    }

    const char* getData() { // use after insert full data
        std::string pData = pData + mUsername + "|" + mPasswords + "|" + mFullname + "|" + mLinkAvatar + "|" + mStatus + "|" + mBirthdays + "|"
                + mEmail + "|" + mRoomID + "|" + mRoomInfo + "|" + mRoomIP + "|" + mRoomPort + "|" + (const char*)mLevel + "|" + (const char*)mGender + "|"
                + (const char*)mBalance + "|" + mCPID + "|" + mDeviceID;
        mData = pData.c_str();
        return mData;
    }

protected:
    const char* mUsername, *mFullname, *mLinkAvatar;
    const char* mPasswords;
    const char* mBirthdays, *mEmail;
    const char* mRoomInfo, *mRoomID, *mRoomIP, *mRoomPort;
    int mLevel, mGender;
    long mBalance;
    const char* mStatus,*mCPID,*mDeviceID;
    const char* mData;
};

#endif // #ifndef
