#ifndef __USERINFO_H_INLCUDED__
#define __USERINFO_H_INLCUDED__

#include "ClientPlayerInfo.h"
//#include "tcpclient.h"
#include "pthread_mobi.h"
#include "MpLoginResponse.h"



class UserInfo {
public:
	std::string mUsername, mFullname, mEmail,mPassWord,mDeviceID,mCPID;
	std::string mTokenID;
	uint32_t mBalance;
	uint8_t isBonus;
	bool iFromGame, iStage;
	unsigned int mLevel, mGender, mAvatarID;
	MpLoginResponse::Date mBirthday;
	std::string mServiceNumber, mSyntax;
	uint8_t mChargeID;
	std::vector<RechargeInfo> mLstRechargeInfo;
	std::vector<RechargeVISAInfo> mLstRechageVISAInfo;
	std::vector<SubscriptionInfo> mLstSubInfo;

	std::vector<RechargeInfo_s> mLstSmsInfo_new;
	//std::vector<SubscriptionInfo> mLstSub_new;
	std::vector<SubcribeInfo_s> mLstSub_new;
	std::vector<ExchangeInfo> mLstExchange;

	bool mIsKeepAlive;
	//std::vector<TcpClient**> mLstTcpTimeout;
	std::vector<MpLoginResponse::GameAddress> lstGameDomain;
	pthread_mutex_t pthreadlock;
	std::string mIP[2];
	uint16_t mPort[2];
	int mGameID;
	int mRoomID;
	int mLevelRoom, mTabChoose;
	std::string mRoomLevel;
	int mTableID;
	bool isReConnect = false;
	std::vector<uint32_t>* mListBetMoney;
	bool isCanRecharge;
	bool isReset = false;
	std::string mUrlUpdate;
	int stateScene;
	MpLoginResponse::GameAddress domainReconect;
	uint8_t isCanEnableSms;
	CC_SYNTHESIZE(std::string, ipNormal, IpNormal);
	CC_SYNTHESIZE(int, portNormal, PortNormal);

	/*size_t getSize() {
	size_t size;
	if (mUsername) {
	size += sizeof(mUsername);
	}
	if (mFullname) {
	size += sizeof(mFullname);
	}
	if (mEmail) {
	size += sizeof(mEmail);
	}
	if (mRoomID) {
	size += sizeof(mRoomID);
	}
	if (mRoomIP) {
	size += sizeof(mRoomIP);
	}
	if (mRoomPort) {
	size += sizeof(mRoomPort);
	}
	if (mTokenID) {
	size += sizeof(mTokenID);
	}
	if (mBalance) {
	size += sizeof(mBalance);
	}
	if (mLevel) {
	size += sizeof(mLevel);
	}
	if (mGender) {
	size += sizeof(mGender);
	}
	if (mAvatarID) {
	size += sizeof(mAvatarID);
	}
	if (mBirthday) {
	size += sizeof(mBirthday);
	}
	}*/
};

#endif