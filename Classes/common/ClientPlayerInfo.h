#ifndef CLIENTPLAYERINFO_H
#define CLIENTPLAYERINFO_H
#include <string>
#include <stdint.h>
#include "cocos2d.h"
#include "mpclient/platform/PlatformDefine.h"

#if (MP_TARGET_PLATFORM == MP_PLATFORM_WIN32) || (MP_TARGET_PLATFORM == MP_PLATFORM_WINRT) || (MP_TARGET_PLATFORM == MP_PLATFORM_WP8)
#include <Winsock2.h>
#else
#include "netinet/in.h"
#endif

typedef struct ClientPlayerInfoEX_s{
	uint32_t balance;
	uint32_t balanceDefault;
	uint32_t score;
	uint32_t winCount;
	uint32_t loseCount;
	uint32_t rank;
	uint8_t host;
	uint8_t disallowKick;
	uint8_t position;
	uint8_t level;
	uint8_t itemId;
	uint8_t avatarId;	
	uint8_t acheive1;
	uint8_t acheive2;
	uint8_t acheive3;
	uint8_t indexX = 1;
	uint8_t gunType;
	std::string username;
	bool isReady;
	bool isFriend;
	float durationReady;
	ClientPlayerInfoEX_s()
	{
		username = "";
		level = 0;
		balance = 0;
		position = 0;
		score = 0;
		host = 0;
		disallowKick = 0;
		itemId = 0;
		balanceDefault = 0;
		winCount = 0;
		loseCount = 0;
		rank = 987634;
		avatarId = 0;
		indexX = 1;
		isFriend = false;
		isReady = false;
		acheive1 = 1;
		acheive2 = 1;
		acheive3 = 1;
		durationReady = 0;
		gunType = 7;
	}
	ClientPlayerInfoEX_s(const char* pUserName, uint8_t pLevel, uint32_t pBalance, uint8_t pPosition, uint8_t pBlockKick, uint8_t pItemId, bool pHost, bool pReady, uint8_t pGunType)
	{
		username = pUserName;
		level = pLevel;
		balance = pBalance;
		position = pPosition;
		host = pHost;
		isReady = pReady;
		itemId = pItemId;
		disallowKick = pBlockKick;
		balanceDefault = pBalance;
		gunType = pGunType;
		score = 0;
		acheive1 = 1;
		acheive2 = 1;
		acheive3 = 1;
		isFriend = false;
		isReady = false;
		durationReady = 0;
	}
}ClientPlayerInfoEX;
typedef struct GameRanker_s
{
	std::string username;
	uint8_t rank;
}GameRanker;

typedef struct RechargeInfo_s {
	uint32_t id;
	std::string syntax;
	std::string serviceNumber;
	std::string serverUddp;
	std::string telco;
	uint32_t money;
	uint32_t balance;
	uint32_t percent;
} RechargeInfo;

typedef struct RechargeVISAInfo_s {
	std::string itemCode;
	int mBalance;
	int mUSD;
	int mPercent;
} RechargeVISAInfo;

struct SubscriptionInfo
{
	std::string name;   //Daily/ Monthy
	uint32_t ndays;
	uint32_t htg;       //
	uint32_t coin;
	uint32_t promotion;
	uint32_t status;    // Active/ InActive
	std::string toString() const;
	size_t fromString(const char* buffer, size_t len);
};

typedef struct ExchangeInfo_s {
	uint32_t id;
	std::string syntax;
	std::string serviceNumber;
	std::string serverUddp;
	std::string telco;
	uint32_t money;
	uint32_t balance;
	uint32_t percent;
} ExchangeInfo;

typedef struct SubcribeInfo_s {
	std::string syntax;
	std::string serviceNumber;
	std::string serverUddp;
	std::string telco;
	uint32_t daily_money;
	uint32_t day_valid;
	uint32_t balance;
} SubcribeInfo;


typedef struct ClientPlayerInfo_s
{
	uint8_t host;
	uint8_t disallowKick;
	uint8_t position;
	uint8_t level;
	uint8_t itemId;
	uint32_t balance;
	std::string username;

	ClientPlayerInfo_s()
	{
		username = "";
		level = 0;
		balance = 0;
		position = 0;
		host = 0;
		disallowKick = 0;
		itemId = 0;
	}
} ClientPlayerInfo;
typedef struct GamePlayerResult_s
{
	std::string Username;
	int WinMoney;
	uint16_t Exp;
	GamePlayerResult_s(const std::string &username, int winMoney, int exp) : Username(username), WinMoney(winMoney), Exp(exp){}
	GamePlayerResult_s(){}
}GamePlayerResult;
typedef struct ClientTableInfo_s
{
	uint8_t maxPlayer;
	uint32_t betMoney;
	uint8_t totalPlayer;
	uint8_t team1;
	uint8_t id;

	ClientTableInfo_s()
	{
		maxPlayer = 4;
		betMoney = 0;
		totalPlayer = 0;
		team1 = 0;
		id = 0;
	}
} ClientTableInfo;

typedef struct ClientItemInfo_s
{
	uint32_t id;
	uint32_t quantity;
	//uint16_t type;

	ClientItemInfo_s()
	{
		id = 0;
		quantity = 0;
		//type = 0;
	}
} ClientItemInfo;

typedef struct MpPoint_s
{
	uint16_t x;
	uint16_t y;
	MpPoint_s(uint16_t pX, uint16_t pY)
	{
		x = pX;
		y = pY;
		//type = 0;
	}
} MpPoint;

typedef struct ClientFishInfo_s
{
	uint8_t quantiy;
	uint8_t fishType;
	uint8_t trajectory;
	uint8_t direction;
	uint16_t time;

	uint8_t fishId;
	uint8_t speed;
	uint8_t offsetX;
	uint8_t offsetY;
	uint8_t health;
	bool isSpecial = false;


	static std::string to_string(const std::vector<struct ClientFishInfo_s> & vtFish)
	{
		std::string data;
		struct ClientFishInfo_s cfi;

		size_t i = 0;
		uint8_t n = 0;
		while (i < vtFish.size())
		{
			cfi = vtFish[i];
			data += (char)cfi.quantiy;
			data += (char)cfi.fishType;
			data += (char)cfi.trajectory;
			data += static_cast<char>(cfi.direction << 6 | ((cfi.time >> 8) && 0x3F));
			data += static_cast<char>(cfi.time & 0xFF);
			data += cfi.fishId;
			data += cfi.speed;
			data += cfi.offsetX;
			data += cfi.offsetY;
			data += cfi.health;
			if (cfi.quantiy > 1)
			{
				n = i + cfi.quantiy;
				n = (n > vtFish.size() ? vtFish.size() : n);
				size_t j = 0;
				for (j = i + 1; j < n; ++j)
				{
					cfi = vtFish[j];
					data += cfi.fishId;
					data += cfi.speed;
					data += cfi.offsetX;
					data += cfi.offsetY;
					data += cfi.health;
				}
				i = j;
			}
			else
			{
				++i;
			}

		}
		return data;
	}

	static const std::vector<struct ClientFishInfo_s> from_string(const std::string &data)
	{
		std::vector<struct ClientFishInfo_s> vtFish;
		struct ClientFishInfo_s cfi;
		const uint8_t *ptr = (const uint8_t *)data.data();
		const uint8_t *end = ptr + data.size();
		uint8_t n = 0;

		while (ptr < end - 10)
		{
			cfi.quantiy = *ptr++;
			cfi.fishType = *ptr++;
			cfi.trajectory = *ptr++;
			n = *ptr++;
			cfi.direction = (n >> 6) & 0x3;
			cfi.time = ((n & 0x3F) << 8) | *ptr++;
			cfi.fishId = *ptr++;
			cfi.speed = *ptr++;
			cfi.offsetX = *ptr++;
			cfi.offsetY = *ptr++;
			cfi.health = *ptr++;
			vtFish.push_back(cfi);

			n = cfi.quantiy - 1;
			if (n > 0 && ptr < end - 5)
			{
				for (size_t j = 0; j < n; ++j)
				{
					cfi.fishId = *ptr++;
					cfi.speed = *ptr++;
					cfi.offsetX = *ptr++;
					cfi.offsetY = *ptr++;
					cfi.health = *ptr++;
					vtFish.push_back(cfi);
				}
			}
		}
		return vtFish;
	}

} ClientFishInfo;

typedef struct TopInfo_s
{
	uint32_t avatarId;
	uint32_t balance;
	uint8_t level;
	uint8_t status;
	std::string username;
}TopInfo;

typedef struct ScratchCard_s
{
	uint32_t value;
	uint32_t balance;
	uint8_t promotionPercent;
	uint8_t telcoId;

	static const std::string to_string(const std::vector<struct ScratchCard_s> &cards)
	{
		struct ScratchCard_s card;
		size_t len = sizeof(struct ScratchCard_s) * cards.size();
		uint8_t *pBuffer = new (std::nothrow) uint8_t[len];
		//assert(pBuffer);

		for (size_t i = 0; i < cards.size(); ++i)
		{
			card = cards[i];
			*((uint32_t*)pBuffer) = htonl(card.value);
			pBuffer += 4;

			*((uint32_t*)pBuffer) = htonl(card.balance);
			pBuffer += 4;

			*pBuffer = card.promotionPercent;
			++pBuffer;

			*pBuffer = card.telcoId;
			++pBuffer;
		}

		if (len && pBuffer)
		{
			std::string data((const char*)pBuffer, len);
			delete pBuffer;
			return data;
		}
		return "";
	}

	static const std::vector<struct ScratchCard_s> from_string(const std::string data)
	{
		std::vector<struct ScratchCard_s> cards;
		struct ScratchCard_s card;
		const uint8_t *pBuffer = (const uint8_t*)data.data();
		//assert(pBuffer);

		for (size_t i = 0; i < data.size(); i += 10)
		{
			//card = cards[i];
			card.value = ntohl(*((uint32_t *)pBuffer));
			pBuffer += 4;

			card.balance = ntohl(*((uint32_t *)pBuffer));
			pBuffer += 4;

			card.promotionPercent = *pBuffer;
			++pBuffer;

			card.telcoId = *pBuffer;
			++pBuffer;

			cards.push_back(card);
		}

		return cards;
	}
} ScratchCard;

typedef struct StructMail
{
	uint32_t created;
	uint8_t status;
	std::string sender;
	std::string receiver;
	std::string subject;
	std::string content;

	static const std::string to_string(const std::vector<struct StructMail> &emails)
	{
		uint8_t buffer[10240];
		uint8_t *ptr = buffer;
		uint8_t *end = buffer + 10240;
		for (auto e : emails)
		{
			if (ptr >= end - 10) break;
			e.created = htonl(e.created);
			*((uint32_t*)ptr) = e.created;
			ptr += 4;

			*ptr++ = e.status;

			if (ptr >= end - e.sender.size()) break;

			memcpy(ptr, e.sender.data(), e.sender.size());
			ptr += e.sender.size();
			*ptr++ = (uint8_t)0;

			if (ptr >= end - e.receiver.size()) break;
			memcpy(ptr, e.receiver.data(), e.receiver.size());
			ptr += e.receiver.size();
			*ptr++ = (uint8_t)0;

			if (ptr >= end - e.subject.size()) break;
			memcpy(ptr, e.subject.data(), e.subject.size());
			ptr += e.subject.size();
			*ptr++ = (uint8_t)0;

			if (ptr >= end - e.content.size()) break;
			memcpy(ptr, e.content.data(), e.content.size());
			ptr += e.content.size();
			*ptr++ = (uint8_t)0;
		}
		std::string data;
		data.append(buffer, ptr);
		return data;
	}

	static const std::vector<struct StructMail> from_string(const std::string &data)
	{
		struct StructMail e;
		std::vector<struct StructMail> lstMail;
		const uint8_t *ptr = (const uint8_t*)data.data();
		const uint8_t *end = ptr + data.size();

		while (ptr < end - 4)
		{
			e.created = *((uint32_t*)ptr);
			e.created = ntohl(e.created);
			ptr += 4;

			e.status = *ptr++;
			e.sender = (const char*)ptr;
			ptr += e.sender.size() + 1;

			e.receiver = (const char*)ptr;
			ptr += e.receiver.size() + 1;
			e.subject = (const char*)ptr;
			ptr += e.subject.size() + 1;
			e.content = (const char*)ptr;
			ptr += e.content.size() + 1;

			lstMail.push_back(e);
		}
		return lstMail;
	}
} StructMail_s;

typedef struct StructTelcoInfo
{
	std::string telcoName;
	uint32_t maxBalance;
}StructTelcoInfo_s;

#endif // CLIENTPLAYERINFO_H
