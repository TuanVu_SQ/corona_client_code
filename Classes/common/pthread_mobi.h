#ifndef __PTHREAD_MOBI_H_INCLUDED__
#define __PTHREAD_MOBI_H_INCLUDED__

#include "cocos2d.h"

USING_NS_CC;

#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	#include "pthread.h"
#else
	#if(CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
		#include "../../proj.win8.1-universal/pthread.h"
	#endif
#endif

#endif