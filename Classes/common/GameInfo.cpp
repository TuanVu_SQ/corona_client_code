#include "GameInfo.h"

std::vector<float> GameInfo::DAME_GUN = { 25, 30, 35, 45, 55, 65, 70, 75, 80 };
 std::vector<float> GameInfo::DAME_GROW_BY_GUN = { 5, 10, 15, 20, 30, 35, 45, 50, 60 };
 std::vector<float> GameInfo::DAME_GROW_BY_FISH = {0, 1, 0.9f, 0.85f, 0.8f, 0.75f, 0.7f, 0.6f, 0.5f, 0.5f, 0.4f, 0.4f, 0.35f, 0.35f, 0.25f, 0.2f, 0.15f, 0.1f, 0.05f, 0, 0 };
 std::vector<float> GameInfo::GROW_RATE_DEAD_BY_GUN = { 0.05f, 0.15f, 0.2f, 0.3f, 0.4f, 0.55f, 0.7f, 0.85f, 1 };
 std::vector<float> GameInfo::GROW_RATE_DEAD = {0, 30, 20, 15, 15, 15, 10, 10, 5, 5, 5, 5, 3, 5, 5, 0.4f, 0.4f, 0.3f, 0.3f, 0.2f, 0.2f };
 std::vector<float> GameInfo::FISH_RATE_DEAD = { 0, 50, 40, 30, 20, 5, 2, 0, 0, -1, -2, 0, -4, -4, -5, -0.2f, -0.2f, -0.3f, -0.4f, -0.5f, -1 };
 LayerNotice* GameInfo::mLayerNotice = NULL;

 std::string GameInfo::registerServerAddress = "165.232.150.185";
 std::string GameInfo::loginServerAddress = "165.232.150.185";
 //std::string GameInfo::registerServerAddress = "fishingmoney.natcom.com.ht";
 //std::string GameInfo::loginServerAddress = "fishingmoney.natcom.com.ht";
 uint16_t GameInfo::registerServerPort = 8003;
 uint16_t GameInfo::loginServerPort = 8003;

#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
 std::string GameInfo::os_prefix = "a";
#endif
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
 std::string GameInfo::os_prefix = "i";
#endif
#if(CC_TARGET_PLATFORM == CC_PLATFORM_WP8 || CC_TARGET_PLATFORM == CC_PLATFORM_WINRT || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
std::string GameInfo::os_prefix = "a";
#endif