﻿#ifndef __GUNINFO_H_INCLUDED__
#define __GUNINFO_H_INCLUDED__

#include "cocos2d.h"

USING_NS_CC;

class GunInfo {
public:
	static std::string getName(int pItemID) {
		pItemID -= 100;
		switch (pItemID) {
		case 1: return "";
		case 2: return "Pirates Gun";
		case 3: return "Red Hawk Gun ";
		case 4: return "Target Gun";
		case 5: return "Secret Gun";
		case 6: return "Violet Gun";
		case 7: return "Black Hawk Gun";
		case 8: return "Pinky Gun";
		case 9: return "Spiderman Gun";
		case 10: return "Bee Gun";
		case 11: return "Black Royal Gun";
		case 12: return "Prenium Gun";
		case 13: return "High Red Gun";
		}
		return "";
	}
	static float getSpeed(int pItemID) {
		pItemID -= 100;
		switch (pItemID) {
		case 1: return 0;
		case 2: return 1;
		case 3: return 2;
		case 4: return 3;
		case 5: return 4;
		case 6: return 5;
		case 7: return 6;
		case 8: return 7;
		case 9: return 8;
		case 10: return 9;
		case 11: return 10;
		case 12: return 11;
		case 13: return 12;
		}
		return 0;
	}
	static float getExp(int pItemID) {
		pItemID -= 100;
		switch (pItemID) {
		case 1: return 0;
		case 2: return 10;
		case 3: return 10;
		case 4: return 10;
		case 5: return 10;
		case 6: return 10;
		case 7: return 20;
		case 8: return 20;
		case 9: return 20;
		case 10: return 20;
		case 11: return 30;
		case 12: return 30;
		case 13: return 40;
		}
		return 0;
	}
	static float getPrice(int pItemID) {
		pItemID -= 100;
		switch (pItemID) {
		case 1: return 0;
		case 2: return 10000;
		case 3: return 15000;
		case 4: return 20000;
		case 5: return 50000;
		case 6: return 100000;
		case 7: return 200000;
		case 8: return 400000;
		case 9: return 1000000;
		case 10: return 2000000;
		case 11: return 4000000;
		case 12: return 5000000;
		case 13: return 10000000;
		}
		return 0;
	}
};

#endif