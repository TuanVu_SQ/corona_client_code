#ifndef __ITEMINFO_H_INCLUDED__
#define __ITEMINFO_H_INCLUDED__

#include "cocos2d.h"

USING_NS_CC;

class ItemInfo {
public:
	static std::string getName(int pItemID) {
		pItemID -= 200;
		switch (pItemID) {
		case 3: return "Block Kick";
		}
		return "";
	}
	static float getPrice(int pItemID) {
		pItemID -= 200;
		switch (pItemID) {
		case 3: return 50000;
		}
		return 0;
	}
};

#endif