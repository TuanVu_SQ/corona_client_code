#ifndef __FISHOBJECT_H_INCLUDED__
#define __FISHOBJECT_H_INCLUDED__
#include "cocos2d.h"

using namespace cocos2d;

class FishObject {
public:
	static FishObject* createFishObject(int pHealth, int pMoney, float pSpeed, int pCountFrameSwim, int pCountFrameDead){
		FishObject * object = new FishObject();
		object->initFishObject(pHealth, pMoney, pSpeed, pCountFrameSwim, pCountFrameDead);
		return object;

	}
	void initFishObject(int pHealth, int pMoney, float pSpeed, int pCountFrame, int pCountFrameDead){
		mHealth = 0;
		mMoney = pMoney;
		mCountFrameSwim = pCountFrame;
		mCountFrameDead = pCountFrameDead;
		mSpeed = pSpeed;
		mCurrentHeath = 0;
		mCurrentSpeed = mSpeed;
		iSpecial = false;
	}
	int getHeathLimit(){
		return mCurrentHeath;
	}
	int getMoney(){
		return mMoney;
	}
	int getCountFrameSwim(){
		return mCountFrameSwim;
	}
	int getCountFrameDead(){
		return mCountFrameDead;
	}
	float getSpeed(){
		return mCurrentSpeed;
	}
	void setLimitSpeed(float pSpeed){
		mSpeed = pSpeed;
	}
	void setCurrentSpeed(float pSpeed){
		mCurrentSpeed = pSpeed;
	}
	void setPrice(uint16_t pPrice){
		mMoney = pPrice;
	}
	void setSpecial(bool pBool){
		iSpecial = pBool;
	}
	bool isSpecial(){
		return iSpecial;
	}
	FishObject* copy(){
		FishObject * object = new FishObject();
		object->initFishObject(mHealth, mMoney, mSpeed, mCountFrameSwim, mCountFrameDead);
		return object;
	}
	void setHeath(float pHeath){
		mCurrentHeath = pHeath;
	}
	int getHeath(){
		return mCurrentHeath;
	}
	void reset(){
		/*mCurrentHeath = mHealth;
		mCurrentSpeed =  mSpeed;*/
	}
private:
	int mHealth, mMoney, mCountFrameSwim,mCountFrameDead;
	float mSpeed;
	int mCurrentHeath;
	float mCurrentSpeed;
	bool iSpecial;
};
#endif