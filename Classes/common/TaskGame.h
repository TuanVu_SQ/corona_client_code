#include "cocos2d.h"
#include <math.h>

using namespace cocos2d;

class TaskTarget : public Ref{
public:
	static TaskTarget *createTask(int pQuantity, int pID, int pTypeTask){
		TaskTarget *task = new TaskTarget();
		task->initTask(pQuantity, pID, pTypeTask);
		return task;
	}
	void initTask(int pQuantity, int pID, int pTypeTask){
		mQuantity = pQuantity;
		mID = pID;
		mTypeTask = pTypeTask;
		mCurrentQuanity = 0;
	}
	int getID(){
		return mID;
	}
	int getQuantity(){
		return mQuantity;
	}
	void setCurrentQuantity(int pQuantity){
		mCurrentQuanity = pQuantity;
	}
	int getCurrentQuantity(){
		return mCurrentQuanity;
	}
	int getType(){
		return mTypeTask;
	}
private :
	int mQuantity, mID,mTypeTask,mCurrentQuanity;
};

class TaskGame :public Ref {
private:
	bool isXo;
	Vector<TaskTarget*>* mArrayTask;
	int mBonus;
public:
	bool isCoin(){
		return isXo;
	}
	Vector<TaskTarget*>* getArrayTask(){
		return mArrayTask;
	}
	int getBonus(){
		return mBonus;
	}
	static TaskGame* createTask(int pBonus, bool iCoin, Vector<TaskTarget*> *pArrayTask){
		TaskGame *task = new TaskGame();
		task->initTask(pBonus, iCoin, pArrayTask);
		return task;
	}
	void initTask(int pBonus, bool iCoin, Vector<TaskTarget*> *pArrayTask){
		mBonus = pBonus;
		isXo = iCoin;
		mArrayTask = pArrayTask;
	}
	static TaskGame* getTask(int pTarget){
		int i = pTarget % 11;
		int j = 1 + pTarget / 11;
		if (j > 20){
			j = 20;
		}
		if (i == 0){
			TaskTarget* target_01 = TaskTarget::createTask(j * 60, 1, 1);
			Vector<TaskTarget*> *pArrayTast = new Vector<TaskTarget*>();
			pArrayTast->pushBack(target_01);
			TaskGame* fishTask_01 = TaskGame::createTask(1, false,
				pArrayTast);

			return fishTask_01;
		}
		else if (i == 1){
			TaskTarget* target_01 = TaskTarget::createTask(j * 30, 3, 1);
			Vector<TaskTarget*> *pArrayTast = new Vector<TaskTarget*>();
			pArrayTast->pushBack(target_01);
			TaskGame* fishTask_02 = TaskGame::createTask(j * 100, true,
				pArrayTast);

			return fishTask_02;
		}
		else if (i == 2){
			TaskTarget* target_01 = TaskTarget::createTask(j * 3, 9, 1);
			TaskTarget* target_02 = TaskTarget::createTask(1, 3, 2);
			Vector<TaskTarget*> *pArrayTast = new Vector<TaskTarget*>();
			pArrayTast->pushBack(target_01);
			pArrayTast->pushBack(target_02);
			TaskGame* fishTask_03 = TaskGame::createTask(j * 1, false,
				pArrayTast);

			return fishTask_03;
		}
		else if (i == 3){
			TaskTarget* target_01 = TaskTarget::createTask(j * 40, 2, 1);
			TaskTarget* target_02 = TaskTarget::createTask(1, 2, 2);
			Vector<TaskTarget*> *pArrayTast = new Vector<TaskTarget*>();
			pArrayTast->pushBack(target_01);
			pArrayTast->pushBack(target_02);
			TaskGame* fishTask_04 = TaskGame::createTask(j * 100, true,
				pArrayTast);

			return fishTask_04;
		}
		else if (i == 4){
			TaskTarget* target_01 = TaskTarget::createTask(j * 1, 13, 1);
			TaskTarget* target_02 = TaskTarget::createTask(1, 1, 2);
			Vector<TaskTarget*> *pArrayTast = new Vector<TaskTarget*>();
			pArrayTast->pushBack(target_01);
			pArrayTast->pushBack(target_02);
			TaskGame* fishTask_05 = TaskGame::createTask(j * 1, false,
				pArrayTast);

			return fishTask_05;
		}
		else if (i == 5){
			TaskTarget* target_01 = TaskTarget::createTask(j * 1, 12, 1);
			TaskTarget* target_02 = TaskTarget::createTask(1, 1, 3);
			Vector<TaskTarget*> *pArrayTast = new Vector<TaskTarget*>();
			pArrayTast->pushBack(target_01);
			pArrayTast->pushBack(target_02);
			TaskGame* fishTask_06 = TaskGame::createTask(j * 1, false,
				pArrayTast);

			return fishTask_06;
		}
		else if (i == 6){
			TaskTarget* target_01 = TaskTarget::createTask(j * 5, 8, 1);
			TaskTarget* target_02 = TaskTarget::createTask(j * 10, 7, 1);
			Vector<TaskTarget*> *pArrayTast = new Vector<TaskTarget*>();
			pArrayTast->pushBack(target_01);
			pArrayTast->pushBack(target_02);
			TaskGame* fishTask_07 = TaskGame::createTask(j * 200, true,
				pArrayTast);


			return fishTask_07;
		}
		else if (i == 7){
			TaskTarget* target_01 = TaskTarget::createTask(j * 50, 4, 1);
			TaskTarget* target_02 = TaskTarget::createTask(j * 300, 1, 4);
			Vector<TaskTarget*> *pArrayTast = new Vector<TaskTarget*>();
			pArrayTast->pushBack(target_01);
			pArrayTast->pushBack(target_02);
			TaskGame* fishTask_08 = TaskGame::createTask(j * 1, false,
				pArrayTast);

			return fishTask_08;
		}
		else if (i == 8){
			TaskTarget* target_01 = TaskTarget::createTask(j * 30, 5, 1);
			TaskTarget* target_02 = TaskTarget::createTask(j * 30, 6, 1);
			Vector<TaskTarget*> *pArrayTast = new Vector<TaskTarget*>();
			pArrayTast->pushBack(target_01);
			pArrayTast->pushBack(target_02);
			TaskGame* fishTask_09 = TaskGame::createTask(j * 1, false,
				pArrayTast);

			return fishTask_09;
		}
		else if (i == 9){
			TaskTarget* target_01 = TaskTarget::createTask(j * 2, 13, 1);
			TaskTarget* target_02 = TaskTarget::createTask(j * 2, 12, 1);
			Vector<TaskTarget*> *pArrayTast = new Vector<TaskTarget*>();
			pArrayTast->pushBack(target_01);
			pArrayTast->pushBack(target_02);
			TaskGame* fishTask_10 = TaskGame::createTask(-1 + j * 500, true,
				pArrayTast);

			return fishTask_10;
		}
		else if (i == 10){
			TaskTarget* target_01 = TaskTarget::createTask(j * 10, 10, 1);
			TaskTarget* target_02 = TaskTarget::createTask(j * 5, 11, 1);
			Vector<TaskTarget*> *pArrayTast = new Vector<TaskTarget*>();
			pArrayTast->pushBack(target_01);
			pArrayTast->pushBack(target_02);
			TaskGame* fishTask_11 = TaskGame::createTask(j * 1, false,
				pArrayTast);

			return fishTask_11;
		}
		else{
			return NULL;
		}

	}
};