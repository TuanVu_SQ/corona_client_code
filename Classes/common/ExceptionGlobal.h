#ifndef EXCEPTIONGLOBAL_H
#define EXCEPTIONGLOBAL_H

#define EXCEPTION_REGISTER_USER_EXIST 100257

#define EXCEPTION_LOGIN_NEED_UPDATE (100 + 3) * 10^1 + 1
#define EXCEPTION_LOGIN_CAN_UPDATE (100 + 3) * 10^1 + 2
#define EXCEPTION_LOGIN_PASSWORDS_WRONG 102257

#define EXCEPTION_CHANGEPASS_PASSWORDS_WRONG (100 + 7) * 10^1 + 1

#define EXCEPTION_BUYITEM_NOT_ENOUGHT_MONEY (100 + 23) * 10^1 + 1
#define EXCEPTION_BUYITEM_NOT_ENOUGHT_LEVEL (100 + 23) * 10^1 + 2
#define EXCEPTION_BUYITEM_FRIEND_NOT_ENOUGHT_LEVEL (100 + 23) * 10^1 + 3
#define EXCEPTION_BUYITEM_FRIEND_NOTVALID (100 + 23) * 10^1 + 4

#define EXCEPTION_SELLITEM_NOT_OWN_ITEM (100 + 25) * 10^1 + 1
#define EXCEPTION_SELLITEM_NOT_ENOUGHT_ITEM (100 + 25) * 10^1 + 2
#define EXCEPTION_SELLITEM_CANT_SALE (100 + 25) * 10^1 + 3

#define EXCEPTION_ADDFRIEND_USERNAME_INVALID (100 + 43) * 10^1 + 1
#define EXCEPTION_ADDFRIEND_ALREADY_FRIEND (100 + 43) * 10^1 + 2

#define EXCEPTION_CLAIMREWARD_NOT_COMPLETED (100 + 83) * 10^1 + 1
#define EXCEPTION_CLAIMREWARD_CLAIMED (100 + 83) * 10^1 + 2

#endif // EXCEPTIONGLOBAL_H
