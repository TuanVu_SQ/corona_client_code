#include "AppDelegate.h"
#include "SimpleAudioEngine.h"

#include "GameInfo.h"

#include "EGScene.h"

#include "HomeScene.h"
#include "MainScene.h"
#include "LogoScene.h"

#include "pthread_mobi.h"

//#include "tcpclient.h"
#include "mppingmessagerequest.h"
#include "mppingmessageresponse.h"

#include "EGJniHelper.h"

#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "unistd.h"
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
extern "C"
{
	void GetJStringContent(JNIEnv *AEnv, jstring AStr, std::string &ARes) {
		if (!AStr) {
			ARes.clear();
			return;
		}

		const char *s = AEnv->GetStringUTFChars(AStr, NULL);
		ARes = s;
		AEnv->ReleaseStringUTFChars(AStr, s);
	}

	JNIEXPORT void JNICALL Java_bca_jni_EGJniHelper_onPaymentCompleted(JNIEnv* env, jobject thiz, jint pType, jint pMoney) {
		EGScene* scene = (EGScene*)Director::getInstance()->getRunningScene();
		if (scene) {
			scene->onSMSResultCallback((int)pType, (int)pMoney);
		}
	}

	JNIEXPORT void JNICALL Java_gvbox_jnihelper_JniHelper_onLoginFacebook(JNIEnv* env, jobject thiz, jint pResult, jstring pAccessToken) {
		EGScene* scene = (EGScene*)Director::getInstance()->getRunningScene();
		if (scene) {
			std::string strAccessToken;
			GetJStringContent(env,pAccessToken,strAccessToken);
			scene->onLoginFacebook((int)pResult, strAccessToken);
		}
	}
};
#endif

USING_NS_CC;

#include "libs/WAudioControl.h"
AppDelegate::AppDelegate() {	
	WAudioControl::getInstance()->setVolumeBackground(!UserDefault::getInstance()->getBoolForKey(KEY_MUSIC, true) ? 0 : 1);
	WAudioControl::getInstance()->setVolumeEffect(!UserDefault::getInstance()->getBoolForKey(KEY_SOUND, true) ? 0 : 1);
}

AppDelegate::~AppDelegate() 
{
}

//void* pthread_tcpmanager(void* _void)
//{
//	while (true) {
//#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
//		Sleep(1000); 
//#elif(CC_TARGET_PLATFORM == CC_PLATFORM_WP8 || CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
//		std::this_thread::sleep_for(std::chrono::seconds(1));
//#else
//		sleep(1);
//#endif
//		for (int i = GameInfo::mUserInfo.mLstTcpTimeout.size() - 1; i >= 0; i--) {
//			TcpClient** client = GameInfo::mUserInfo.mLstTcpTimeout.at(i);
//			if (*client == NULL) {
//				GameInfo::mUserInfo.mLstTcpTimeout.erase(GameInfo::mUserInfo.mLstTcpTimeout.begin() + i);
//				continue;
//			}
//			if ((*client)->isInGame || (*client)->isJoinedGame) {
//				GameInfo::mUserInfo.mLstTcpTimeout.erase(GameInfo::mUserInfo.mLstTcpTimeout.begin() + i);
//				continue;
//			}
//			(*client)->mTimeoutSecond--;
//
//			pthread_mutex_lock(&GameInfo::mUserInfo.pthreadlock);
//			if (GameInfo::mUserInfo.isReset) {
//				GameInfo::mUserInfo.isReset = false;
//				break;
//			}
//			if ((*client)->mTimeoutSecond <= 0 && (*client)->mConnecting) {
//				(*client)->setReasonKill(KILL_BY_TIME_OUT);
//				(*client)->close();
//			}
//
//			if (!(*client)->mConnecting) {
//				if (!(*client)->isJoinedGame)
//				{
//					delete (*client);
//					(*client) = NULL;
//					client = NULL;
//				}
//				GameInfo::mUserInfo.mLstTcpTimeout.erase(GameInfo::mUserInfo.mLstTcpTimeout.begin() + i);
//			}
//			pthread_mutex_unlock(&GameInfo::mUserInfo.pthreadlock);
//		}
//	}
//
//	pthread_exit(0);
//	return NULL;
//}

#include "cry_is.h"

bool AppDelegate::applicationDidFinishLaunching() {
	std::string key = "2b7e151628aed2a6abf7158809cf4f3c";
	std::string value = "6bc1bee22e409f96e93d7e117393172a";
	CCLOG("result: %s", Cry_IS::Converting(value, key).c_str());


	// initialize director
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	if (!glview) {
#if (CC_TARGET_PLATFORM != CC_PLATFORM_WP8 || CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
		glview = GLViewImpl::create("Ban Ca Online");
		glview->setFrameSize(CLIENT_WIDTH, CLIENT_HEIGHT);
#endif
		director->setOpenGLView(glview);
	}
	glview->setDesignResolutionSize(CLIENT_WIDTH, CLIENT_HEIGHT, ResolutionPolicy::EXACT_FIT);
	srand(time(0));

    // turn on display FPS
    //director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

	GameInfo::mUserInfo.mIsKeepAlive = false;
	pthread_mutex_init(&GameInfo::mUserInfo.pthreadlock, NULL);
	/*pthread_t t1;
	pthread_create(&t1, NULL, pthread_ping, NULL);*/
	/*pthread_t t2;
	pthread_create(&t2, NULL, pthread_tcpmanager, NULL);*/

    // create a scene. it's an autorelease object
	auto scene = LogoScene::scene();

    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();

	EGScene* pScene = (EGScene*)Director::getInstance()->getRunningScene();
	if (pScene) {
		pScene->pauseGame();
	}
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
//	if (pScene) {
//		pScene->pauseGame();
//	}
//#endif
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
	CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();

	EGScene* pScene = (EGScene*)Director::getInstance()->getRunningScene();
	if (pScene) {
		pScene->resumeGame();
	}
}
